<?php

namespace App\Jobs;

//add namespace on it
use Mail;
use App\Mail\EmailVerification;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendVerificationEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $user;

    public function __construct($user)
    {
        //add this too
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */

    //configure this too
    public function handle()
    {
        //add this too
        $email = new EmailVerification($this->user);
        Mail::to('ethics.secretariat@pchrd.dost.gov.ph')->send($email);
    }
}