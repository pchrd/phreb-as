<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Phrebform_attached_letter extends Model
{
    protected $fillable = ['pfal_recdetails_id', 'pfal_pf_id', 'pfal_pft_id', 'pfal_users_id', 'pfal_body', 'pfal_status'];

    public function phrebforms(){
    	return $this->belongsTo(Phrebforms::class, 'pfal_pf_id');
    }

    public function phrebforms_templates(){
    	return $this->belongsTo(Phrebforms_template::class, 'pfal_pft_id');
    }

    public function users(){
    	return $this->belongsTo(User::class, 'pfal_users_id');
    }
}
