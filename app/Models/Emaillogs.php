<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Emaillogs extends Model{
    protected $fillable = ['e_sender_user_id','e_recdetails_id','e_from','e_to','e_cc','e_bcc','e_subject','e_body','e_status','e_sentorfailed','e_created_at'];
    public $timestamps = false;

    public function recdetails(){
    	return $this->belongsTo(Recdetails::class, 'e_recdetails_id');
    }

    public function users(){
    	return $this->belongsTo(User::class, 'e_sender_user_id');
    }
}