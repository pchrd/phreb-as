<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Awardingletter extends Model{
    protected $fillable = ['a_letter_recdetails_id','a_letter_saveduser_id','a_letter_doctypes', 'a_letter_years', 'a_letter_body','a_letter_datesend','a_letter_status'];

    public function recdetails(){
    	return $this->belongsTo(Recdetails::class, 'a_letter_recdetails_id');
    }

    public function users(){
    	return $this->belongsTo(User::class, 'a_letter_saveduser_id');
    }

    public function documenttypes(){
    	return $this->belongsTo(DocumentTypes::class, 'a_letter_doctypes');
    }

    public function applicationlevel(){
        return $this->belongsTo(Applicationlevel::class, 'a_letter_recdetails_id', 'recdetails_id');
    }
}