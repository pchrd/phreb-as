<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SendOrderpayment extends Model
{
    protected $fillable = ['or_sender_id', 'or_recdetails_id', 'or_serialno', 'or_file', 'or_file_generated', 'or_datecreated_at'];
    public $timestamps = false;

    public function recdetails(){
    	return $this->belongsTo(Recdetails::class, 'or_recdetails_id');
    }

    public function users(){
    	return $this->belongsTo(User::class, 'or_sender_id');
    }
}
