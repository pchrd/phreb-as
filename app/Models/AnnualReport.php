<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AnnualReport extends Model
{
    protected $guarded = [];

    public function recdetails(){
    	return $this->belongsTo(Recdetails::class, 'ar_recdetails_id');
    }

    public function documents(){
    	return $this->belongsTo(Recdocuments::class, 'ar_recdocuments_id');
    }

    public function annualreporttitles(){
    	return $this->belongsTo(AnnualReportTitle::class, 'ar_title_id');
    }

    public function applicationlevel(){
    	return $this->belongsTo(Applicationlevel::class, 'ar_recdetails_id');
    }
}
