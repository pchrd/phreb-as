<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sendtoaccreditor extends Model{
    //
    use SoftDeletes;

    public $timestamps = false;
     protected $primaryKey = 'sendacc_tbl_id'; 

    protected $fillable=['a_recdetails_id', 'a_sender_user_id', 'a_accreditors_user_id', 'sendaccreditors_status', 'sendaccreditors_confirm_decline', 'a_datesubmitted', 'a_dateofexpiry', 'a_reminder_status', 'deleted_at'];

    public function recdetails(){
    	return $this->belongsTo(Recdetails::class, 'a_recdetails_id')->join('applicationlevel', 'applicationlevel.recdetails_id', 'recdetails.id');
    }

    public function users(){
    	return $this->belongsTo(User::class, 'a_accreditors_user_id');
    }

    public function applicationlevel(){
        return $this->belongsTo(Applicationlevel::class, 'a_recdetails_id', 'recdetails_id');
    }
}