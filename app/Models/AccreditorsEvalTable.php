<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccreditorsEvalTable extends Model
{	
	protected $table = 'accreditors_eval_tables';
	protected $fillable = ['accreditors_evaluations_id', 'criteria_id', 'acc_rate', 'field1', 'field2'];
	public $timestamps = false;
	protected $casts = ['accreditors_evaluations_id' => 'json'];

    public function accreditors_evaluation(){
    	return $this->belongsTo(AccreditorsEvaluation::class, 'accreditors_evaluations_id');
    }

    public function accreditors_criteria(){
    	return $this->belongsTo(AccreditorsEvaluationCriteria::class, 'criteria_id');
    }
}
