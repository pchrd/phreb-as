<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Esig extends Model
{
    protected $fillable = ['esig_user_id', 'esig_addedby', 'esig_file', 'esig_file_generated', 'esig_datecreated_at'];
    public $timestamps = false;

    public function role_users(){
    	return $this->belongsTo(Role_user::class, 'user_id',  'esig_user_id');
    }
}
