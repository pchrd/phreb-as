<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IPaddresses extends Model
{
    protected $table = 'ipaddresses';
}