<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class AnnualReportTitle extends Model{
	
    protected $fillable = ['ar_name', 'ar_desc', 'ar_start_year', 'ar_end_year', 'ar_sobrang_field'];
    
    public function annualreporttitles(){
    	return $this->hasMany(AnnualReport::class, 'ar_title_id');
    }
}
