<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Trainingeventsmanagement extends Model
{
    protected $fillable = ['te_orientation_id', 'te_trainee_id', 'te_addedby_id', 'te_role_event_id', 'te_affiliation_id', 'status'];

    public function trainees(){
    	return $this->belongsTo(Trainingeventstrainees::class, 'te_trainee_id');
    }

    public function eventsorientations(){
    	return $this->belongsTo(Trainingeventsorientation::class, 'te_orientation_id');
    }

    public function addedby_users(){
    	return $this->belongsTo(User::class, 'te_addedby_id');
    }

    public function roleinevents(){
        return $this->belongsTo(Accreditation_role::class, 'te_role_event_id');
    }

    public function affiliationsGetCurrentForViewAttendees(){
        return $this->belongsTo(Trainingeventstrainees_affiliations::class, 'te_affiliation_id');
    }
}
