<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Recdocuments_emailattachment extends Model
{
    protected $fillable = ['re_submissionstatuses_id', 're_recdetails_id', 're_user_id', 're_file_name', 're_file_generated', 're_file_extension', 're_file_size'];

    public function recdetails(){
    	return $this->belongsTo(Recdetails::class, 're_recdetails_id');
    }
}
