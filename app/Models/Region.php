<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    //for belongs in Recdetails model
    public function recdetails(){
        return $this->belongsToMany(Recdetails::class,'applicationlevel');
    }

    //for belongs in User Model
    public function users(){
        return $this->belongsToMany(User::class,'applicationlevel');
    }

    //for belongs to level
    public function levels(){
    	return $this->belongsToMany(Level::class,'applicationlevel');
    }

    public function applicationlevel(){
        return $this->hasMany(Applicationlevel::class,'region_id');
    }

    public $timestamps = false;
}
