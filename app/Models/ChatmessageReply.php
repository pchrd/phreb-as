<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChatmessageReply extends Model
{
    protected $fillable = ['cmr_replytomessage_id', 'cmr_message_id', 'cmr_messageStatus'];

    public function messages(){
    	return $this->belongsTo(Chatmessage::class, 'cmr_message_id');
    }

    public function messagesGetReply(){
    	return $this->belongsTo(Chatmessage::class, 'cmr_replytomessage_id');
    }

    public function messageFiles(){
    	return $this->hasMany(ChatmessageFile::class, 'cmf_message_id', 'cmr_message_id');
    }
}
