<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StatementRecs extends Model
{
    protected $fillable=['statement_recdetails_id', 'statement_statement_id'];

    public function statement(){
    	return $this->belongsTo(Statement::class, 'statement_statement_id');
    }
}
