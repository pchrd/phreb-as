<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Phrebform_assessment_statuses extends Model
{
    protected $fillable = ['pfas_status_pfid', 'pfas_status_sender_id', 'pfas_status_status_id'];

    public function statuses(){
    	return $this->belongsTo(Status::class, 'pfas_status_status_id');
    }

    public function users(){
    	return $this->belongsTo(User::class, 'pfas_status_sender_id');
    }

     public function phrebforms(){
    	return $this->belongsTo(Phrebforms::class, 'pfas_status_pfid');
    }

    public function phrebform_emailmessages(){
        return $this->hasMany(Phrebform_emailmessage::class, 'pfem_pfid', 'pfas_status_pfid');
    }

    public function sendtofinalassessment(){
        return $this->hasMany(Phrebform_assessment_finalsubmission::class, 'pfa_fs_pfid', 'pfas_status_pfid');
    }
}
