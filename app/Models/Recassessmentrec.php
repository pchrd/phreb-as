<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Recassessmentrec extends Model{
    protected $fillable = ['ra_rec_ra_id','ra_rec_recdetails_id','ra_rec_sender_id','ra_rec_csachair_id','ra_rec_status','ra_rec_datecreated_at','ra_rec_ap_dateexpiry','ra_rec_ap_reminder_status', 'ra_rec_ap_reminder_status_deadline', 'ra_rec_ce_dateexpiry','ra_rec_ce_reminder_status', 'ra_rec_ce_reminder_status_deadline'];
    
    public $timestamps = false;

    public function users_sender(){
    	return $this->belongsTo(User::class, 'ra_rec_sender_id');
    }

    public function users_csachair(){
        return $this->belongsTo(User::class, 'ra_rec_csachair_id');
    }

    public function recname(){
    	return $this->belongsTo(Recdetails::class, 'ra_rec_recdetails_id');
    }

    public function recchairs(){
    	return $this->belongsTo(Recchair::class, 'ra_rec_recdetails_id');
    }

    public function recassessment(){
    	return $this->belongsTo(Recassessment::class, 'ra_rec_ra_id');
    }

    public function applicationlevel(){
    	return $this->belongsTo(Applicationlevel::class, 'ra_rec_recdetails_id', 'recdetails_id'); 
    }
}