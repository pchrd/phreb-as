<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Recdocuments extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function documenttypes(){
    	return $this->belongsTo(DocumentTypes::class, 'recdocuments_document_types_id');
    }

    public function users(){
    	return $this->belongsTo(User::class, 'recdocuments_user_id');
    }

    public function recdetails(){
    	return $this->belongsTo(Recdetails::class, 'recdocuments_recdetails_id');
    }

    public function applicationlevel(){
    	return $this->belongsTo(Applicationlevel::class, 'recdocuments_recdetails_id', 'recdetails_id');
    }

    public function annualreports(){
    	return $this->belongsTo(Applicationlevel::class, 'ar_recdocuments_id');
    }

    public function submissionstatuses(){
        return $this->belongsTo(Submissionstatuses::class, 'recdocuments_submission_statuses_id');
    }

    public function recdocumentsTypesID(){
        return $this->hasMany(DocumentTypes::class, 'recdocuments_document_types_id');
    }

}
