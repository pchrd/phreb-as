<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB; 

class Submissionstatuses extends Model
{	
	protected $fillable=['submissionstatuses_recdetails_id', 'submissionstatuses_user_id', 'submissionstatuses_statuses_id', 'submissionstatuses_emailmessage', 'submissionstatuses_datecreated', 'submissionstatuses_rec_response', 'submissionstatuses_emailsend', 'submissionstatuses_doctypes_id', 'submissionstatuses_date_accreditation', 'submissionstatuses_date_expiry', 'submissionstatuses_level'];

    protected $casts = [
        'submissionstatuses_doctypes_id' => 'array'
    ];

    public function recdetails(){
        return $this->belongsTo(Recdetails::class, 'submissionstatuses_recdetails_id');
    }

    public function status(){
    	return $this->belongsTo(Status::class, 'submissionstatuses_statuses_id');
    }

    public function users(){
    	return $this->belongsTo(User::class, 'submissionstatuses_user_id');
    }

    public function emailattachments(){
        return $this->hasMany(Recdocuments_emailattachment::class, 're_submissionstatuses_id');
    }

    // added for resubmit requirements
    public function recdocuments(){
        return $this->hasMany(Recdocuments::class, 'recdocuments_submission_statuses_id')->join('document_types', 'document_types.id', 'recdocuments_document_types_id')->select('document_types_name', DB::raw('document_types.id as docID'), DB::raw('COUNT(*) as totalFiles'))->groupBy('document_types_name');
    }

    // added for duedate table
    public function submissionstatuses_duedate(){
        return $this->hasOne(Submissionstatuses_duedate::class, 'submissionstatuses_id');
    }

    // added to show files submitted by RECs
    public function showsubmitted_documents(){
        return $this->hasMany(Recdocuments::class, 'recdocuments_submission_statuses_id');
    }

    public function history_levels(){
        return $this->belongsTo(Level::class, 'submissionstatuses_level');
    }

    public $timestamps = false;
}