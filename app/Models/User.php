<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;


use App\Notifications\MailResetPasswordToken;

class User extends Authenticatable
{
    use Notifiable;
    
    //add this too for softdeleting user records
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'email_token','verified', 'recname', 'applyingforlevel', 'messagetosecretariat', 'verifiedby'
    ];

     //add this to avoid the updated_at
    // public $timestamps = false;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    //for softdeletes
    protected $dates = ['deleted_at'];



    //for belongs in Role model
    public function roles(){
        return $this->belongsToMany(Role::class,'role_users');
    }

     //----------------------

    public function role_users(){
        return $this->hasOne(Role_user::class, 'user_id');
    }

    public function recassessmentscsas(){
        return $this->hasMany(Recassessmentscsa::class, 'ra_csa_sender_id', 'ra_csa_csachair_id');
    }

     public function recassessmentrec(){
        return $this->hasMany(Recassessmentrec::class, 'ra_rec_sender_id', 'ra_rec_csachair_id');
    }

    public function recdetails(){
        return $this->hasOne(\App\Models\Recdetails::class,'user_id');
    }

    public function sendorderpayment(){
        return $this->hasMany(SendOrderpayment::class, 'or_sender_id');
    }

    public function recactiopnplan(){
        return $this->hasMany(Recactionplan::class, 'ap_sender_id');
    }

    public function recactionplancsas(){
        return $this->hasMany(Recactionplancsa::class, 'ap_csa_sender_id', 'ap_csa_csachair_id', 'ap_csa_rec_id');
    }

    public function sendtoaccreditors(){
        return $this->hasMany(Sendtoaccreditor::class, 'a_accreditors_user_id');
    }

    public function recassessments(){
        return $this->hasMany(Recassessment::class, 'ra_user_id');
    }

    public function recdocuments(){
        return $this->hasMany(Recdocuments::class, 'recdocuments_user_id');
    }

    public function awardingletter(){
        return $this->hasMany(Awardingletter::class, 'a_letter_saveduser_id');
    }

    public function emaillogs(){
        return $this->hasMany(Emaillogs::class, 'e_sender_user_id');
    }

    public function applicationlevel(){
        return $this->hasMany(Applicationlevel::class, 'recdetails_id');
    }

    //----------------------
   

    //based on the authserviceprovider.php
    public function hasAccess(array $permissions){
        foreach($this->roles as $role ){
            if($role->hasAccess($permissions)){
                return true;
            }
        }
        return false;
    }

    public function inRole($roleSlug){
        return $this->roles()->where('slug', $roleSlug)->count()==1;
    }
    
    //Send a password reset email to the user || add this too
    public function sendPasswordResetNotification($token){
        $this->notify(new MailResetPasswordToken($token));
    }

    public function accreditors_evaluation(){
        return $this->hasMany(AccreditorsEvaluation::class, 'evaluatedby_user_id');
    }

    public function phrebforms(){
        return $this->hasMany(Phrebforms::class, 'pf_user_id');
    }

}