<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sendemailreminder extends Model
{
    protected $fillable = ['ser_recdetails_id', 'ser_emailsent_date'];

    public function recdetails(){
    	return $this->belongsTo(Recdetails::class, 'ser_recdetails_id');
    }
}
