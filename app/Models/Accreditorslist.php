<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Accreditorslist extends Model{
    
	protected $table = 'accreditorslists';

    protected $fillable = ['accreditors_name', 'accreditors_status', 'accreditors_designation', 'accreditors_gender', 'accreditors_specialization', 'accreditors_office', 'accreditors_address', 'accreditors_contactnumber', 'accreditors_email'];

    public function accreditors_evaluations(){
    	return $this->hasMany(AccreditorsEvaluation::class, 'accreditors_id');
    }
}
