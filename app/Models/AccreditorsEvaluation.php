<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccreditorsEvaluation extends Model{
    
    protected $fillable = ['accreditors_id', 'evaluatedby_user_id', 'title_award', 'for_rec', 'for_level'];

    public function accreditors_eval_tables(){
    	return $this->belongsToMany(AccreditorsEvalTable::class, 'accreditors_eval_tables', 'accreditors_evaluations_id', 'criteria_id');
    }

    public function users(){
    	return $this->belongsTo(User::class, 'evaluatedby_user_id');
    }

    public function accreditors(){
    	return $this->belongsTo(Accreditorslist::class, 'accreditors_id');
    }
    public function accreditors_eval_tables_1(){
    	return $this->belongsTo(AccreditorsEvalTable::class, 'accreditors_evaluations_id');
    }
}