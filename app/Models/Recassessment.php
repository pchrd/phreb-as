<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Recassessment extends Model{
	protected $fillable  = ['ra_user_id','ra_recdetails_id','ra_documents_types_id','ra_check','ra_recdocuments_file','ra_recdocuments_file_generated', 'ra_recdocuments_file_extension', 'ra_dateuploaded'];
    public $timestamps = false;

    public function ra_rec(){
    	return $this->hasMany(Recassessmentrec::class, 'ra_rec_ra_id');
    }

    public function recassessmentscsas(){
    	return $this->hasMany(Recassessmentscsa::class, 'ra_csa_ra_id');
    }

    public function users(){
    	return $this->belongsTo(User::class, 'ra_user_id');
    }

    public function recdetails(){
    	return $this->belongsTo(Recdetails::class, 'ra_recdetails_id');
    }

    public function applicationlevel(){
        return $this->belongsTo(Applicationlevel::class, 'ra_recdetails_id', 'recdetails_id');
    }

    //->join('applicationlevel', 'applicationlevel.recdetails_id', 'recdetails.id')
}
