<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Phrebform_assessment_rec extends Model
{
    protected $fillable = ['pfar_recdetails_id', 'pfar_pf_id', 'pfar_sender_id', 'pfar_status'];

    public function recdetails(){
    	return $this->belongsTo(Recdetails::class, 'pfar_recdetails_id');
    }

    public function phrebforms(){
    	return $this->belongsTo(Phrebforms::class, 'pfar_pf_id');
    }

    public function sentby(){
    	return $this->belongsTo(User::class, 'pfar_sender_id');
    }

    public function getAttachedActionPlan(){
    	return $this->hasMany(Phrebform_actionplan::class, 'ap_recassessment_id');
    }

    public function phrebform_actionplanfromrecs(){
   		return $this->hasMany(Phrebform_actionplanfromrec::class, 'pfap_rec_recass_id');
   	}

}