<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Recassessmentscsa extends Model{	
	protected $fillable  = ['ra_csa_ra_id','ra_csa_sender_id','ra_csa_csachair_id', 'ra_csa_recdetails_id','ra_csa_status','ra_csa_datecreated_at','ra_csa_dateexpiry','ra_csa_reminder_status'];
    public $timestamps = false;

    public function users_sender(){
    	return $this->belongsTo(User::class, 'ra_csa_sender_id');
    }

    public function users_csachair(){
    	return $this->belongsTo(User::class, 'ra_csa_csachair_id');
    }

    public function recassessments(){
    	return $this->belongsTo(Recassessment::class, 'ra_csa_ra_id');
    }

    public function applicationlevel(){
        return $this->belongsTo(Applicationlevel::class, 'ra_csa_recdetails_id', 'recdetails_id');
    }
}