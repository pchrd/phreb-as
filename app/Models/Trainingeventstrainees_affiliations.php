<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Trainingeventstrainees_affiliations extends Model
{
    protected $guarded = [''];

    public function acc_roles(){
    	return $this->belongsTo(Accreditation_role::class, 'trainees_recrole');
    }
}
