<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Phrebform_actionplanfromrec extends Model
{
	protected $fillable = ['pfap_rec_pfid', 'pfap_rec_recass_id', 'pfap_rec_recdetails_id', 'pfap_rec_sendby', 'pfap_rec_status'];

    public function phrebforms(){
    	return $this->belongsTo(Phrebforms::class, 'pfap_rec_pfid');
    }

    public function phrebform_actionplanfromrecstatuses(){
    
    	return $this->hasOne(Phrebform_actionplanfromrecsstatus::class, 'pf_apfr_id', 'id');
    }

	    	public function getCurrentStatus(){
	    		return $this->phrebform_actionplanfromrecstatuses()->orderBy('id', 'desc')->latest()->first();
	    	}
}
