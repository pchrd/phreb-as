<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Applicationlevel extends Model{
     protected $table = 'applicationlevel';
     protected $guarded = []; 

     public function recdetails(){
        return $this->belongsTo(Recdetails::class, 'recdetails_id');
     }

     public function status(){
        return $this->belongsTo(Status::class, 'statuses_id');
     }

     public function levels(){
    	return $this->belongsTo(Level::class, 'level_id');
     }

     public function regions(){
     	return $this->belongsTo(Region::class, 'region_id');
     }

     public function recactionplans(){
     	return $this->hasMany(Recactionplan::class, 'recdetails_id');
     }

     public function awardingletter(){
     	return $this->hasMany(Awardingletter::class, 'a_letter_recdetails_id');
     }

     public function sendtoaccreditors(){
     	return $this->hasMany(Sendtoaccreditor::class, 'a_recdetails_id', 'recdetails_id');
     }

     public function recassessments(){
     	return $this->hasMany(Recassessment::class, 'ra_recdetails_id');
     }

     public function recassessmentrec(){
     	return $this->hasMany(Recassessmentrec::class, 'ra_rec_recdetails_id');
     }

     public function recdocuments(){
     	return $this->hasMany(Recdocuments::class, 'recdocuments_recdetails_id');
     }

     public function annualreports(){
     	return $this->hasMany(AnnualReport::class, 'ar_recdetails_id');
     }

     // public function users(){
     // 	return $this->belongsTo(User::class, 'recdetails_id');
     // }


}