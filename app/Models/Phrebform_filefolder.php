<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Phrebform_filefolder extends Model
{
    protected $fillable = ["pfff_name", "pfff_recdetails_id", "pfff_pf_id", "pfff_user_id"];

    public function phrebform_files(){
    	return $this->hasMany(Phrebform_file::class, 'pf_files_pfff_id');
    }

    public function users(){
    	return $this->belongsTo(User::class, 'pfff_user_id');
    }

    public function phrebforms(){
    	return $this->hasOne(Phrebforms::class, 'pfff_pf_id');
    }

}
