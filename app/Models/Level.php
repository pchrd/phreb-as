<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Level extends Model
{
    //add protected
     protected $fillable=['level_name','level_description'];

     //disable updated or created at
    public $timestamps = false;

     //for belongs in Recdetails model
    public function recdetails(){
        return $this->belongsTo(\App\Models\Recdetails::class);
    }

   public function applicationlevel(){
        return $this->hasOne(Applicationlevel::class, 'level_id');
   }

}