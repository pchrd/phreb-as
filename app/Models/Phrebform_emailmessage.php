<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Phrebform_emailmessage extends Model
{
    protected $fillable = ['pfem_pfid', 'pfem_sender_id', 'pfem_pfid_status_id', 'pfem_emailmessage', 'pfem_status'];
}
