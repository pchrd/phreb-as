<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Phrebform_remarks extends Model
{
    protected $fillable = ['pf_remarks_pfid', 'pf_remarks_recdetails_id', 'pf_remarks_user_id', 'pf_remarks_body', 'pf_remarks_status', 'pf_remarks_seen'];

    public function phrebforms(){
    	return $this->belongsTo(Phrebforms::class, 'pf_remarks_pfid');
    }

    public function users(){
    	return $this->belongsTo(User::class, 'pf_remarks_user_id');
    }
}
