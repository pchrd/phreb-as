<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Status extends Model{
    //add this to avoid the updated_at or created_at
    public $timestamps = false;

    public function recdetails(){
    	return $this->hasOne(Recdetails::class);
    }

    public function applicationlevel(){
    	return $this->hasOne(Applicationlevel::class, 'statuses_id');
    }

}