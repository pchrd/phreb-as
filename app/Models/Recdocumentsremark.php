<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Recdocumentsremark extends Model
{
      protected $fillable=['remarks_user_id', 'remarks_recdetails_id', 'remarks_recdocuments_types_id', 'remarks_message_recdocuments'];

      public function remarks(){
      	return $this->hasMany('App\Models\Recdocuments');
      }
}