<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentTypes extends Model
{
    protected $guarded = [''];
    
    //disabled timestamps
    public $timestamps = false;

    public function recactionplan(){
    	return $this->hasMany(Recactionplan::class, 'ap_document_types');
    }

    public function recdocuments(){
    	return $this->hasMany(Recdocuments::class, 'recdocuments_document_types_id');
    }

    public function awardingletter(){
    	return $this->hasMany(Awardingletter::class, 'a_letter_doctypes');
    }

    public function existing_arps(){
        return $this->hasMany(Existing_arps::class);
    }
}
