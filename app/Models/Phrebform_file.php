<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Phrebform_file extends Model
{
    protected $fillable = [ "pf_files_pfid", "pf_files_pfff_id", "pf_files_addedby", "pf_files_originalfilename", "pf_files_originalfileextension", "pf_files_originalfilesize", "pf_files_uniqid"];

    public function phrebforms(){
    	return $this->belongsTo(Phrebforms::class, 'pf_files_pfid');
    }

    public function users(){
    	return $this->hasOne(User::class, 'pf_files_addedby');
    }

    public function phrebform_file_folders(){
    	return $this->belongsTo(Phrebform_filefolder::class, 'pf_files_pfff_id');
    }

}
