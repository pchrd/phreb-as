<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Reporting extends Model{

   protected $fillable = ['report_user_id', 'report_start_date', 'report_end_date', 'report_status'];

}