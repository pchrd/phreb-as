<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Phrebform_actionplanaccstatuses extends Model
{
     protected $fillable = ['pf_acas_pfid', 'pf_acas_status_id', 'pf_acas_to', 'pf_acas_addedby_id', 'pf_acas_status', 'mark_as_done_eval', 'deadline'];

     public function statuses(){
     	return $this->belongsTo(Status::class, 'pf_acas_status_id');
     }

     public function users1(){
     	return $this->belongsTo(User::class, 'pf_acas_to');
     }

     public function users2(){
     	return $this->belongsTo(User::class, 'pf_acas_to');
     }
}
