<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Phrebforms_template extends Model
{	
	 public $timestamps = false; 

     public function phrebforms(){
    	return $this->hasMany(Phrebforms::class, 'pf_pfid');
    }
}
