<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Phrebform_actionplanfromrecsstatus extends Model
{
	protected $table = 'phrebform_actionplanfromrecsstatuses';
	
    protected $fillable = ['pf_apfr_id', 'pf_apfr_pfid', 'pf_apfr_status_id', 'pf_apfr_user_id'];

    public function statuses(){
    	return $this->belongsTo(Status::class, 'pf_apfr_status_id');
    }

    public function users(){
    	return $this->belongsTo(User::class, 'pf_apfr_user_id');
    }
}