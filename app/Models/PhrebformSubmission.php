<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PhrebformSubmission extends Model
{
    protected $fillable = ['ps_recdetails_id', 'ps_phrebforms_id', 'ps_pfid', 'ps_sender_id', 'ps_csachair_id', 'ps_link', 'ps_status'];

    public function recdetails(){
    	return $this->belongsTo(Recdetails::class, 'ps_recdetails_id');
    }

    public function phrebformsTemplate(){
    	return $this->belongsTo(Phrebforms_template::class, 'ps_phrebforms_id');
    }

    public function sender(){
    	return $this->belongsTo(User::class, 'ps_sender_id');
    }

    public function csachair(){
    	return $this->belongsTo(User::class, 'ps_csachair_id');
    }
}
