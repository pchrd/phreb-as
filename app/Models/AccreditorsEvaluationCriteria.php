<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccreditorsEvaluationCriteria extends Model
{
    protected $fillable = ['criteria_name', 'criteria_description', 'criteria_percentage', 'criteria_field1', 'criteria_field2', 'criteria_field3'];

    public function accreditors_eval_tables(){
    	return $this->hasMany(AccreditorsEvaluationCriteria::class, 'criteria_id');
    }
}
