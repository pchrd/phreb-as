<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chatmessage extends Model
{
    protected $fillable = ['send_user_id','messageFrom','messageSubject','messages','seen_status','message_status', 'bcrypted_id'];

    public function users(){
    	return $this->belongsTo(User::class, 'send_user_id');
    }

    public function messageTo(){
    	return $this->hasMany(ChatmessageTo::class, 'message_id');
    }

    public function message_id(){
    	return $this->hasOne(ChatmessageTo::class, 'message_id');
    }

    public function messageFiles(){
    	return $this->hasMany(ChatmessageFile::class, 'cmf_message_id');
    }

    public function messageReply(){
    	return $this->hasMany(ChatmessageReply::class, 'cmr_message_id');
    }

    public function messageGetReply(){
    	return $this->hasMany(ChatmessageReply::class, 'cmr_replytomessage_id')->orderBy('created_at', 'desc');
    }

}
