<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Trainingeventsorientation extends Model
{
    protected $fillable = ['training_type_name', 'institution_name', 'rec_name', 'training_start_date', 'training_end_date', 'user_id'];

    public function users(){
    	return $this->belongsTo(User::class, 'user_id');
    }

    public function reclists(){
    	return $this->belongsTo(Reclist::class, 'rec_name');
    }

}
