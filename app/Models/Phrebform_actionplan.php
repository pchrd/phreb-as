<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Phrebform_actionplan extends Model implements Auditable{

	use \OwenIt\Auditing\Auditable;

   	protected $fillable = ['ap_recassessment_id', 'ap_pf_id', 'ap_pft_id', 'ap_pfap_id', 'ap_body', 'ap_body', 'ap_remarks', 'ap_createdby', 'ap_status', 'ap_extrafield1'];

   	public function phrebforms(){
   		return $this->belongsTo(Phrebforms::class, 'ap_pfap_id');
   	}

   	public function phrebforms_templates(){
   		return $this->belongsTo(Phrebforms_template::class, 'ap_pft_id');
   	}

   	public function phrebformrecass(){
   		return $this->hasOne(Phrebform_assessment_rec::class, 'ap_recassessment_id');
   	}


}
