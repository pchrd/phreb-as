<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Recactionplancsa extends Model
{
    protected $fillable = ['ap_csa_ap_id','ap_csa_recdetails_id','ap_csa_sender_id','ap_csa_csachair_id','ap_csa_rec_id','ap_csa_status','ap_csa_datecreated_at'];
    public $timestamps = false;

    public function recdetails(){
    	return $this->belongsTo(Recdetails::class, 'ap_csa_recdetails_id');
    }

    public function recactionplan(){
    	return $this->belongsTo(Recactionplan::class, 'ap_csa_ap_id');
    }

    public function users(){
    	return $this->belongsTo(User::class, 'ap_csa_sender_id');
    }

    public function users_csachair(){
    	return $this->belongsTo(User::class, 'ap_csa_csachair_id');
    }

     public function users_rec(){
    	return $this->belongsTo(User::class, 'ap_csa_rec_id');
    }

}