<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use App\Models\User;
use Auth;

class Phrebforms extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

	protected $fillable = ['pf_recdetails_id', 'pf_pfid', 'pf_user_id' ,'pf_body1', 'pf_comments', 'pf_comments_date', 'pf_status'];

    public function recdetails(){
    	return $this->belongsTo(Recdetails::class, 'pf_recdetails_id');
    }

    public function recdetail(){
    	return $this->belongsTo(Recdetails::class, 'pf_recdetails_id');
    }

    public function phrebforms_template(){
    	return $this->belongsTo(Phrebforms_template::class, 'pf_pfid');
    }

    public function users(){
        return $this->belongsTo(User::class, 'pf_user_id');
    }

    public function phrebform_assessment_recs(){
        return $this->hasOne(Phrebform_assessment_rec::class, 'pfar_pf_id');
    }

    public function phrebformActionplan(){
        return $this->hasOne(Phrebform_actionplan::class, 'ap_pfap_id');
    }

    public function getAttachedActionPlan(){
        return $this->hasMany(Phrebform_actionplan::class, 'ap_pf_id');
    }

    public function phrebform_filefolders(){
        return $this->hasMany(Phrebform_filefolder::class, 'pfff_pf_id')->orderby('id', 'desc');
    }

    public function phrebform_apsubmission_recs(){
        return $this->hasOne(Phrebform_actionplanfromrec::class, 'pfap_rec_pfid');
    }

    public function phrebform_actionplanfromrecstatuses(){
        return $this->hasMany(Phrebform_actionplanfromrecsstatus::class, 'pf_apfr_pfid');
    }

    public function phrebform_remarks(){
        $user = User::find(Auth::user()->id);

        if($user->role_users->role_id == 2 OR $user->role_users->role_id == 4 OR $user->role_users->role_id == 5){
            return $this->hasMany(Phrebform_remarks::class, 'pf_remarks_pfid')->orderBy('id', 'desc');
        }else{
            return $this->hasMany(Phrebform_remarks::class, 'pf_remarks_pfid')->orderBy('id', 'desc')->where('pf_remarks_status', '!=', 'null');
        }
    }

    public function phrebformAssessmentStatus(){
        return $this->hasMany(Phrebform_assessment_statuses::class, 'pfas_status_pfid');
    }

    public function phrebformAssessmentStatus_getLatest(){
        return $this->phrebformAssessmentStatus()->latest()->first();
    }

    public function sendtoaccreditors(){
        return $this->hasMany(Sendtoaccreditor::class, 'a_recdetails_id', 'pf_recdetails_id');
    }

    public function phrebform_finalsubmission(){
        return $this->hasMany(Phrebform_assessment_finalsubmission::class, 'pfa_fs_pfid');
    }

    public function phrebform_attached_letter(){
        return $this->hasOne(Phrebform_attached_letter::class, 'pfal_pf_id')->first();
    }

    public function phrebform_actionplanaccstatuses(){
        return $this->hasMany(Phrebform_actionplanaccstatuses::class, 'pf_acas_pfid');
    }

}
