<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Recchair extends Model
{
	 // /add protected inorder to fill the database fields
     protected $fillable=['rec_chairname', 'rec_chairemail', 'rec_chairmobile'];

     public $timestamps = false;

     //for belongs in Recdetails model
    public function recdetails(){
        return $this->belongsToMany(App\Models\Recdetails::class);
    }

}