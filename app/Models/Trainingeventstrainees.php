<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Trainingeventstrainees extends Model
{
    protected $fillable = ['trainees_lname', 'trainees_fname', 'trainees_mname', 'trainees_gender', 'trainees_certificatename'];

    public function acc_recroles(){
    	return $this->belongsTo(Accreditation_role::class, 'trainees_recrole');
    }

    public function affiliations(){
    	return $this->hasMany(Trainingeventstrainees_affiliations::class, 'trainees_id', 'id')->orderby('id', 'desc');
    }

    public function affiliationsLatestOnly(){
        return $this->hasOne(Trainingeventstrainees_affiliations::class, 'trainees_id')->latest();
    }

    public function traineesAttendance(){
    	return $this->hasMany(Trainingeventsmanagement::class, 'te_trainee_id', 'id')->orderby('id', 'desc');
    }
}
