<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChatmessageFile extends Model
{
    protected $fillable = ['cmf_message_id', 'cmf_filename', 'cmf_fileextensionname', 'cmf_filesize', 'cmf_filegenerated_id'];

     public function messages(){
    	return $this->belongsTo(Chatmessage::class, 'cmf_message_id');
    }
}