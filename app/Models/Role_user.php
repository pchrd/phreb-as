<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role_user extends Model{
 
    public function users(){
    	return $this->belongsTo(User::class, 'user_id');
    }

    public function roles(){
    	return $this->belongsTo(Role::class, 'role_id');
    }

    public function levels(){
    	return $this->belongsTo(Level::class, 'user_assigned_level_id', 'id');
    }

    public function esig(){
    	return $this->hasOne(Esig::class, 'esig_user_id')->orderBy('id');
    }

     public function assignedRegions(){
        return $this->belongsTo(Region::class, 'user_assigned_region_id');
    }
}
