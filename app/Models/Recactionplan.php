<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Recactionplan extends Model
{
    protected $fillable = ['ap_recdetails_id','ap_document_types','ap_sender_id','ap_recactionplan_name','ap_recactionplan_uniqid','ap_status', 'ap_submittedto_rec'];

    public function recdetails(){
    	return $this->belongsTo(Recdetails::class, 'ap_recdetails_id');
    }

    public function documenttypes(){
    	return $this->belongsTo(DocumentTypes::class, 'ap_document_types');
    }

    public function users(){
    	return $this->belongsTo(User::class, 'ap_sender_id');
    }

    public function recactionplancsas(){
    	return $this->hasMany(Recactionplancsa::class, 'ap_csa_ap_id');
    }

    public function applicationlevel(){
        return $this->hasOne(Applicationlevel::class, 'recdetails_id', 'ap_recdetails_id');
    }
}