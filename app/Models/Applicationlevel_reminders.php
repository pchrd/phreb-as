<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Applicationlevel_reminders extends Model{
    protected $guarded = [''];
    public $timestamps = false;

    public function recdetails(){
    	return $this->belongsToMany(Recdetails::class);
    }
}
