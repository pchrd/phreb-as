<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Submissionstatuses_duedate extends Model
{
    protected $fillable = ['submissionstatuses_id', 'submissionstatuses_duedate', 'submissionstatuses_user_id'];
}
