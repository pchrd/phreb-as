<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChatmessageTo extends Model
{
    protected $fillable = ['message_id','messageTo_id','messageStatus'];

    public function users(){
    	return $this->belongsTo(User::class, 'messageTo_id');
    }
}
