<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //add protected
     protected $fillable=['role_name','slug','permissions'];

     public $timestamps = false;

     //for belongs to user model
      public function users(){
    	  return $this->belongsToMany(App\Models\User::class,'role_users');
      }

       //for belongs to recdetails model
      public function recdetails(){
        return $this->belongsToMany(App\Models\Recdetails::class,'recdetails');
      }

      public function role_users(){
        return $this->belongsTo(Role_user::class, 'role_id');
      }

      public function hasAccess(array $permissions){
      	foreach($permissions as $permission){
      		if($this->hasPermission($permission)){
      			return true;
      		}
      	}
      	return false;
      }

      protected function hasPermission(string $permission){
      	$permissions = json_decode($this->permissions, true);
      	return $permissions[$permission]??false;
      }
}