<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Recdetails extends Model
{
    public $timestamps = false;

    //add protected inorder to fill the database fields
     protected $fillable=['selectlevel', 'selectregions', 'user_id', 'rec_apptype_id', 'rec_existing_record', 'rec_name', 'rec_institution', 'rec_address', 'rec_email', 'rec_telno', 'rec_faxno', 'rec_contactperson', 'rec_contactposition', 'rec_contactmobileno', 'rec_contactemail', 'rec_dateestablished'];

      //for belongs in level model
    public function levels(){
        return $this->belongsToMany(\App\Models\Level::class,'applicationlevel')->withTimestamps();
    }

      //for belongs in User Model
    public function users(){
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }

    public function applicationlevel(){
        return $this->hasOne(Applicationlevel::class, 'recdetails_id');
    }

        //for belongs in Recchair Model
    public function recchairs(){
    	return $this->hasOne(\App\Models\Recchair::class, 'recdetails_id');
    }

    public function roles(){
        return $this->belongsToMany(\App\Models\Role::class, 'role_users');
    }

    public function submissionsstatuses(){
        return $this->hasMany(Submissionstatuses::class, 'submissionstatuses_recdetails_id')->orderBy('submissionstatuses_datecreated', 'desc');
    }


    public function recassessmentrecs(){
        return $this->hasMany(Recassessmentrec::class, 'ra_rec_recdetails_id');
    }

    public function sendorderpayment(){
        return $this->hasMany(SendOrderpayment::class, 'or_recdetails_id');
    }

    public function recactionplan(){
        return $this->hasMany(Recactionplan::class, 'ap_recdetails_id');
    }

    public function status(){
        return $this->belongsTo(Status::class, 'rec_apptype_id');
    }

    public function recactionplancsas(){
        return $this->hasMany(Recactionplancsa::class, 'ap_csa_recdetails_id');
    }

    public function sendaccreditors(){
        return $this->hasMany(Sendtoaccreditor::class, 'a_recdetails_id');
    }

    public function recassessments(){
        return $this->hasMany(Recassessment::class, 'ra_recdetails_id');
    }

    public function awardingletter(){
        return $this->hasMany(Awardingletter::class, 'a_letter_recdetails_id');
    }

    public function emaillogs(){
        return $this->hasMany(Emaillogs::class, 'e_recdetails_id');
    }

    public function recassessmentcsas(){
        return $this->hasMany(Recassessmentcsa::class, 'ra_csa_recdetails_id');
    }

    public function recdocuments(){
        return $this->hasMany(Recdocuments::class, 'recdocuments_recdetails_id');
    }

    public function recdocument_typesID(){
        return $this->hasMany(DocumentTypes::class, 'recdetails_id');
    }

    public function annualreports(){
        return $this->hasMany(AnnualReport::class, 'ar_recdetails_id');
    }

    public function existing_recs(){
        return $this->hasOne(Existing_recs::class, 'recdetails_id');
    }

    public function existing_arps(){
        return $this->hasOne(Existing_arps::class, 'recdetails_id')->orderBy('existing_arps.foryear', 'desc')->get();
    }

    public function applicationlevel_reminders(){
        return $this->hasOne(Applicationlevel_reminders::class, 'recdetails_id');
    }

    public function phrebforms(){
        return $this->belongsToMany(Phrebforms::class, 'phrebforms', 'pf_recdetails_id', 'pf_pfid', 'pf_body')->withTimestamps();
    }

    public function phrebform(){
        return $this->hasMany(Phrebforms::class, 'pf_recdetails_id');
    }

    // policy statement
    public function statement_recdetails(){
        return $this->belongsToMany(StatementRecs::class, 'statement_recs', 'statement_recdetails_id', 'statement_statement_id');
    }

    public function recdetails_statements(){
        return $this->hasMany(StatementRecs::class, 'statement_recdetails_id');
    }

}