<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Existing_arps extends Model{

	protected $guarded = [''];

   	public function recdetails(){
    	return $this->belongsTo(Recdetails::class);
    }

    public function documenttypes(){
    	return $this->belongsTo(DocumentTypes::class, 'selectarps');
    }
}
