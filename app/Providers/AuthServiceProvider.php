<?php
namespace App\Providers;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(){
        $this->registerPolicies();
        //add this as custom 
        $this->registerPostPolicies();
    }

    // methods for gates
    public function registerPostPolicies(){
        
        //only admin and secretariat can do see radio buttons and others
        Gate::define('can-see-pending-inc-complete', function($user){
            return $user->hasAccess(['overall-functions']) or $user->hasAccess(['incharge-functions']);
        });

        //only admin can access this page
        Gate::define('only-admin-can-access-page', function($user){
            return $user->hasAccess(['overall-functions']);
        });

        //only secreataria can see this
        Gate::define('secretariat-access', function($user){
            return $user->hasAccess(['incharge-functions']);
        });

        //only REC can see this
        Gate::define('rec-access', function($user){
            return $user->hasAccess(['rec-functions']);
        });

        //only csa-chair can access/see
        Gate::define('csachair-access', function($user){
            return $user->hasAccess(['csachair-functions']);
        });

         //only accreditors can see this
        Gate::define('accreditors-access', function($user){
            return $user->hasAccess(['accreditor-functions']);
        });

        //accreditors and secretariat
        Gate::define('accreditors_and_secretariat', function($user){
            return $user->hasAccess(['accreditor-functions']) or $user->hasAccess(['incharge-functions']);
        });

        Gate::define('accreditors_and_csachair', function($user){
            return $user->hasAccess(['accreditor-functions']) or $user->hasAccess(['csachair-functions']);
        });

        Gate::define('accreditors_and_csachair_secretariat', function($user){
            return $user->hasAccess(['accreditor-functions']) or $user->hasAccess(['csachair-functions']) or $user->hasAccess(['incharge-functions']);
        });

        Gate::define('rec_and_secretariat', function($user){
            return $user->hasAccess(['rec-functions']) or $user->hasAccess(['incharge-functions']);
        });

        Gate::define('secretariat_and_admin', function($user){
            return $user->hasAccess(['incharge-functions']) or $user->hasAccess(['overall-functions']);
        });

        Gate::define('csachair_and_secretariat', function($user){
            return $user->hasAccess(['csachair-functions']) or $user->hasAccess(['incharge-functions']);
        });

        Gate::define('rec_secretariat_accreditors_csachair', function($user){
            return $user->hasAccess(['csachair-functions']) or $user->hasAccess(['incharge-functions']) or $user->hasAccess(['accreditor-functions']) or $user->hasAccess(['rec-functions']);
        });

        Gate::define('rec_secretariat_accreditors', function($user){
            return $user->hasAccess(['rec-functions']) or $user->hasAccess(['incharge-functions']) or $user->hasAccess(['accreditor-functions']);
        });
    }
}