<?php
namespace App\Providers;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Models\User;
use App\Models\Recdetails;
use App\Models\Chatmessage;
use App\Models\Recassessmentrec;
use App\Models\Recassessmentscsa;
use App\Models\EmailTemplate;
use App\Models\EmailLogs;
use App\Models\Sendtoaccreditor;
use Auth;
use Mail;
use Carbon\Carbon;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {   
        $_SERVER["SERVER_NAME"] = "ethics-accreditation.healthresearch.ph";

        //add this to migrate tbl in cmd
        Schema::defaultStringLength(191);
        
        // $users = User::where('id', Auth::id())->first();
        
        // Mail::raw('This is Group Sample Test Email Only from the server. ', function($message){
        //     $message->to('rgbahala@pchrd.dost.gov.ph');
        //     $message->subject('PHREB Accreditation Sample Test Email Only');
        //  });

        // if (Mail::failures()) {
        //     return;
        // }
       

        // // otherwise everything is okay ...
        // return redirect()->back();

        
        //share the counting of messages and notifs
        // view()->composer('layouts.myApp', function($view){
        //     if(auth::user() == true){
        //         $countmessages = Chatmessage::where('chatmessages.reciever_user_id', auth::user()->id)->orWhere('chatmessages.send_user_id', auth::user()->id)->count();
        //         $view->with('countmessages', $countmessages);

        //         $notifsmessage = Chatmessage::where('chatmessages.reciever_user_id', auth::id())->where('seen_status',null)->get();
        //          $view->with('notifsmessage', $notifsmessage);

        //         $countmessages_notifs = Chatmessage::where('chatmessages.seen_status', '=', null)->where('chatmessages.reciever_user_id', auth::user()->id)->count();
        //         $view->with('countmessages_notifs', $countmessages_notifs);
        //     }
        // });

            // //Remind to submit Annual Report and Protocol Summary

            // $emails = Recdetails::get();
            // foreach($emails as $get_details1){

            //     // Mail::raw('Please Submit Your Annual Report. Thank you!', function($message) use ($get_details1){
            //     //     $message->from('us@example.com', 'Laravel');
            //     //     $message->to([$get_details1->rec_email]);
            //     // });

            //      // Mail::send('email.email_template', compact('body', 'get_details1'), function($message) use ($get_details1, $body){
            //      //        $message->to($get_details1->users->email);
            //      //        $message->subject('Reminders for Submission of Assessment Form for '.$get_details1->recdetails->rec_name);

            //      //        $sender = 4;
            //      //        $recdetails_id = $get_details1->applicationlevel->recdetails_id;
            //      //        $from = '';
            //      //        $to = $get_details1->users->email;
            //      //        $cc = '';
            //      //        $bcc = '';
            //      //        $subject = 'Reminders for Submission of Assessment Form for '.$get_details1->recdetails->rec_name;
            //      //        $body = $body;
            //      //        emaillogs($sender, $recdetails_id, $from, $to, $cc, $bcc, $subject, $body);

            //      //    });
            // }


            //Inital Emailing of Annual Report & Protocol Summary 7 months after the end of year
            // if(Carbon::today()->startOfYear()->addMonths(7) <= Carbon::today()->EndOfYear() or carbon::today()->EndOfYear()){

            //      foreach($emails as $get_details1){

            //      $emailTemplate = EmailTemplate::find(23);
            //      $body = str_replace(['$rec_institution','$rec_name','$level','$year','$deadline'], [$get_details1->rec_institution, $get_details1->rec_name,  $get_details1->applicationlevel->levels->level_name, Carbon::today()->format('Y'), Carbon::today()->EndOfYear()->addMonths(2)->format('F j, Y')], $emailTemplate->email_body);


            //      Mail::send('email.email_template1', compact('body', 'get_details1'), function($message) use ($get_details1, $body, $emailTemplate){
            //             $message->to($get_details1->rec_email);
            //             $message->cc([$get_details1->rec_contactemail, $get_details1->recchairs->rec_chairemail]);
            //             $message->subject($emailTemplate->email_subject.' for '.$get_details1->rec_name.'-'.$get_details1->rec_institution);

            //             $sender = 1;
            //             $recdetails_id = $get_details1->applicationlevel->recdetails_id;
            //             $cc = '';
            //             $from = $get_details1->rec_contactemail.', '.$get_details1->recchairs->rec_chairemail;
            //             $to = $get_details1->rec_email;
            //             $bcc = 'phrebaccreditationsystem@gmail.com';
            //             $subject = $emailTemplate->email_subject.' for '.$get_details1->rec_name.'-'.$get_details1->rec_institution;
            //             $body = $body;
            //             emaillogs($sender, $recdetails_id, $from, $to, $cc, $bcc, $subject, $body);

            //         });
            //     }
            // }


            // //notify rec to submit action plan and compliance evidences
            // $today = Carbon::today();
            // $currentdate = Carbon::today()->addDays(7);
            // $rec_actionplan = Recassessmentrec::whereIn('ra_rec_ap_dateexpiry', [$currentdate])->orWhereIn('ra_rec_ap_dateexpiry', [$today]);
            // $rec_actionplan_dl = $rec_actionplan->join('applicationlevel', 'applicationlevel.recdetails_id', 'recassessmentrecs.ra_rec_recdetails_id')->get();
            
            // foreach($rec_actionplan_dl as $get_details1){

            //     if($get_details1->ra_rec_ap_dateexpiry <= $currentdate and $get_details1->ra_rec_ap_reminder_status == null){
            //         $this->notifyrec_ap($get_details1);
            //         $rec_actionplan->update(['ra_rec_ap_reminder_status' => Carbon::now()]);
            //     }elseif($get_details1->ra_rec_ap_dateexpiry <= $today and $get_details1->ra_rec_ap_reminder_status_deadline == null){
            //         $this->notifyrec_ap($get_details1);
            //         $rec_actionplan->update(['ra_rec_ap_reminder_status_deadline' => Carbon::now()]);
            //     }elseif($get_details1->ra_rec_ce_dateexpiry <= Carbon::today()->addMonths(4) and $get_details1->ra_rec_ce_reminder_status == null){
            //         $this->notifyrec_ce($get_details1);
            //         $rec_actionplan->update(['ra_rec_ce_reminder_status' => Carbon::now()]);
            //     }elseif($get_details1->ra_rec_ce_dateexpiry <= Carbon::today() and $get_details1->ra_rec_ce_reminder_status_deadline == null){
            //         $this->notifyrec_ce($get_details1);
            //         $rec_actionplan->update(['ra_rec_ce_reminder_status_deadline' => Carbon::now()]);
            //     }
            // }
            
            // //notify accreditor's deadline to submit rec-assessment
            // $accreditor = Sendtoaccreditor::leftjoin('applicationlevel', 'applicationlevel.recdetails_id', 'sendtoaccreditors.a_recdetails_id')->whereIn('a_dateofexpiry', [$currentdate])->orWhereIn('a_dateofexpiry', [$today]);
            // $accreditors = $accreditor->get();

            // foreach ($accreditors as $get_details1){
            //     if($get_details1->a_dateofexpiry <= $currentdate AND $get_details1->a_reminder_status == null){
            //        $this->notifyacc_ass($get_details1);
            //        $accreditor->update(['a_reminder_status' => Carbon::now()]);
            //     }elseif($get_details1->a_dateofexpiry <= $today AND $get_details1->a_reminder_status_deadline ==null){
            //        $this->notifyacc_ass($get_details1);
            //        $accreditor->update(['a_reminder_status_deadline' => Carbon::now()]);
            //     }
            // }

            // //notify the csa-chair to submit their assessment
            // $csachair = Recassessmentscsa::whereIn('ra_csa_dateexpiry', [$currentdate])->orWhereIn('ra_csa_dateexpiry', [$today]);
            // $csachairs = $csachair->get();
            
            // foreach($csachairs as $get_details1){
            //     if($get_details1->ra_csa_dateexpiry <= $currentdate AND $get_details1->ra_csa_reminder_status == null){
            //             $this->notifycsachair_ass($get_details1);
            //             $csachair->update(['ra_csa_reminder_status' => carbon::now()]);
            //         }elseif($get_details1->ra_csa_dateexpiry <= $today AND $get_details1->ra_csa_reminder_status_deadline == null){
            //             $this->notifycsachair_ass($get_details1);
            //             $csachair->update(['ra_csa_reminder_status_deadline' => carbon::now()]);
            //         }
            // }



        //notify REC to send annual report & Protocol Summary per year//
        // $arps = Recdetails::all();
        // dd($arps->applicationlevel->levels->level_name);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
//-----------------------------------------------------------------------------------//
    public function notifyacc_ass($get_details1){
         $expirydate = \Carbon\Carbon::createFromFormat('Y-m-d',$get_details1->a_dateofexpiry)->format('F j , Y');
            $emailTemplate = EmailTemplate::find(9);
            $body = str_replace(['$name','$level_id','$rec_name','$expirydate'], [$get_details1->users->name, $get_details1->recdetails->level_id, $get_details1->recdetails->rec_name, $expirydate], $emailTemplate->email_body);

                Mail::send('email.email_template', compact('body', 'get_details1'), function($message) use ($get_details1, $body){
                    $message->to($get_details1->users->email);
                    $message->subject('Reminders for Submission of Assessment Form for '.$get_details1->recdetails->rec_name);

                    $sender = 4;
                    $recdetails_id = $get_details1->applicationlevel->recdetails_id;
                    $from = '';
                    $to = $get_details1->users->email;
                    $cc = '';
                    $bcc = '';
                    $subject = 'Reminders for Submission of Assessment Form for '.$get_details1->recdetails->rec_name;
                    $body = $body;
                    emaillogs($sender, $recdetails_id, $from, $to, $cc, $bcc, $subject, $body);

                });
    }

    public function notifycsachair_ass($get_details1){
         $deadline = \Carbon\Carbon::createFromFormat('Y-m-d',$get_details1->ra_csa_dateexpiry)->format('F j, Y');
                $emailTemplate = EmailTemplate::find(10);
                $body = str_replace(['$name','$rec_name','$level_id','$status_name','$deadline'], [$get_details1->users_csachair->name, $get_details1->recassessments->recdetails->rec_name, $get_details1->recassessments->recdetails->level_id, $get_details1->recassessments->recdetails->status->status_name, $deadline], $emailTemplate->email_body);

                    Mail::send('email.email_template', compact('body', 'get_details1'), function($message) use ($get_details1, $body){
                        $message->to($get_details1->users_csachair->email);
                        $message->cc([$get_details1->recassessments->users->email]);
                        $message->subject('Deadline of Submission of Final Assessment for '.$get_details1->recassessments->recdetails->rec_name);

                    $sender = 4;
                    $recdetails_id = $get_details1->recassessments->ra_recdetails_id;
                    $from = '';
                    $to = $get_details1->users_csachair->email;
                    $cc = $get_details1->recassessments->users->email;
                    $bcc = '';
                    $subject = 'Deadline of Submission of Final Assessment for '.$get_details1->recassessments->recdetails->rec_name;
                    $body = $body;
                    emaillogs($sender, $recdetails_id, $from, $to, $cc, $bcc, $subject, $body);
                });
    }

//---------------------------------------------------------------------------------------------------//
    public function notifyrec_ap($get_details1){
        $expirydate = \Carbon\Carbon::createFromFormat('Y-m-d',$get_details1->ra_rec_ap_dateexpiry)->format('F j , Y');

        $emailTemplate = EmailTemplate::find(11);
        $body = str_replace(['$rec_chairname','$deadline'], [$get_details1->recname->recchairs->rec_chairname, $expirydate], $emailTemplate->email_body);

        Mail::send('email.email_template', compact('body', 'get_details1'), function($message) use ($get_details1, $body){
            $message->to($get_details1->recname->recchairs->rec_chairemail);
            $message->subject($get_details1->recname->rec_name.'-Deadline of REC Action Plan');
            $message->cc([$get_details1->recname->rec_contactemail, $get_details1->recname->rec_email, $get_details1->recname->users->email]);

            $sender = 4;
            $recdetails_id = $get_details1->recname->applicationlevel->recdetails_id;
            $from = $get_details1->recname->rec_name.'- Deadline of REC Action Plan';
            $to = $get_details1->recname->recchairs->rec_chairemail;
            $cc = $get_details1->recname->rec_contactemail.', '.$get_details1->recname->rec_email.', '.$get_details1->recname->users->email;
            $bcc = '';
            $subject = $get_details1->recname->rec_name.'-Deadline of REC Action Plan';
            $body = $body;
            emaillogs($sender, $recdetails_id, $from, $to, $cc, $bcc, $subject, $body);

                        // $emaillogs = Emaillogs::create([
                        //     'e_sender_user_id' => 1,
                        //     'e_recdetails_id' => $get_details1->recdetails_id,
                        //     'e_from' => $get_details1->recname->rec_name.'-Deadline of REC Action Plan',
                        //     'e_to' => $get_details1->recname->recchairs->rec_chairemail,
                        //     'e_cc' => $get_details1->recname->rec_contactemail.', '.$get_details1->recname->rec_email.', '.$get_details1->recname->users->email,
                        //     'e_bcc' => '',
                        //     'e_subject' => $get_details1->recname->rec_name.'-Deadline of REC Action Plan',
                        //     'e_body' => $body,
                        //     'e_created_at' => Carbon::now()
                        // ]);
        });
    }

    public function notifyrec_ce($get_details1){
       $expirydate_ce = \Carbon\Carbon::createFromFormat('Y-m-d',$get_details1->ra_rec_ce_dateexpiry)->format('F j , Y');

       $emailTemplate = EmailTemplate::find(12);
       $body = str_replace(['$rec_chairname','$deadline'], [$get_details1->recchairs->rec_chairname, $expirydate_ce], $emailTemplate->email_body);

       Mail::send('email.email_template', compact('body', 'get_details1'), function($message) use ($get_details1, $body){
        $message->to($get_details1->recname->recchairs->rec_chairemail);
        $message->subject($get_details1->recname->rec_name.'-Deadline of REC Compliance Evidences');
        $message->cc([$get_details1->recname->rec_contactemail, $get_details1->recname->rec_email, $get_details1->recname->users->email]);

        $sender = 4;
        $recdetails_id = $get_details1->recdetails_id;
        $from = $get_details1->recname->rec_name.'-Deadline of REC Compliance Evidences';
        $to = $get_details1->recname->recchairs->rec_chairemail;
        $cc = $get_details1->recname->rec_contactemail.', '.$get_details1->recname->rec_email.', '.$get_details1->recname->users->email;
        $bcc = '';
        $subject = $get_details1->recname->rec_name.'-Deadline of REC Compliance Evidences';
        $body = $body;
        emaillogs($sender, $recdetails_id, $from, $to, $cc, $bcc, $subject, $body);

                        // $emaillogs = Emaillogs::create([
                        //     'e_sender_user_id' => 1,
                        //     'e_recdetails_id' => $get_details1->recdetails_id,
                        //     'e_from' => $get_details1->recname->rec_name.'-Deadline of REC Compliance Evidences',
                        //     'e_to' => $get_details1->recname->recchairs->rec_chairemail,
                        //     'e_cc' => $get_details1->recname->rec_contactemail.', '.$get_details1->recname->rec_email.', '.$get_details1->recname->users->email,
                        //     'e_bcc' => '',
                        //     'e_subject' => $get_details1->recname->rec_name.'-Deadline of REC Compliance Evidences',
                        //     'e_body' => $body,
                        //     'e_created_at' => Carbon::now()
                        // ]);
    });
    }

}