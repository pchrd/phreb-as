<?php 
use App\Models\Emaillogs;
use App\Models\Recdetails;
use App\Models\EmailTemplate;
use Carbon\Carbon;
use App\Models\Phrebforms_template;

//emailogsManagement
if(!function_exists('emaillogs, emailTemplate')){

	 function emaillogs($sender, $recdetails_id, $from, $to, $cc, $bcc, $subject, $body){
        $emaillogs = Emaillogs::create([
           'e_sender_user_id' => $sender,
           'e_recdetails_id' => $recdetails_id,
           'e_from' => $from,
           'e_to' => $to,
           'e_cc' => $cc,
           'e_bcc' => $bcc,
           'e_subject' => $subject,
           'e_body' => $body,
           'e_created_at' => Carbon::now()
        ]);
    }

    //reminder for acc-deadline (General)//
    function emailTemplate(){
      $emails = Recdetails::get();

        foreach($emails as $get_details1){

            $emailTemplate = EmailTemplate::find(25);
            $body = str_replace(['$rec_institution','$rec_name','$level','$date_accreditation_expiry'], [$get_details1->rec_institution, $get_details1->rec_name,  $get_details1->applicationlevel->levels->level_name, \Carbon\Carbon::createFromFormat('Y-m-d', $get_details1->applicationlevel->date_accreditation_expiry)->format('F j, Y')], $emailTemplate->email_body);

            //3rd - 1st reminder
            if($get_details1->applicationlevel->date_accreditation_expiry <= Carbon::today() AND $get_details1->applicationlevel_reminders->deadline_reminder_3rd == null){

            Mail::send('email.email_template1', compact('body', 'get_details1'), function($message) use ($get_details1, $body, $emailTemplate){
              $message->to('phrebaccreditationsystem@gmail.com');
              $message->cc([$get_details1->rec_email, $get_details1->rec_contactemail, $get_details1->recchairs->rec_chairemail]);
              $message->subject($emailTemplate->email_subject.' for '.$get_details1->rec_name.'-'.$get_details1->rec_institution);
              });
                emailogs_Save($get_details1, $emailTemplate, $body);
                $get_details1->applicationlevel()->update(['statuses_id' => 17]);
                $get_details1->applicationlevel_reminders()->update(['deadline_reminder_3rd' => Carbon::now()]);

            //2nd
            }elseif(Carbon::createFromFormat('Y-m-d', $get_details1->applicationlevel->date_accreditation_expiry)->addMonths(-7) <= Carbon::today() AND $get_details1->applicationlevel_reminders->deadline_reminder_2nd == null){

              Mail::send('email.email_template1', compact('body', 'get_details1'), function($message) use ($get_details1, $body, $emailTemplate){
              $message->to('phrebaccreditationsystem@gmail.com');
              $message->cc([$get_details1->rec_email, $get_details1->rec_contactemail, $get_details1->recchairs->rec_chairemail]);
              $message->subject($emailTemplate->email_subject.' for '.$get_details1->rec_name.'-'.$get_details1->rec_institution);
              });
                emailogs_Save($get_details1, $emailTemplate, $body);
                $get_details1->applicationlevel_reminders()->update(['deadline_reminder_2nd' => Carbon::now()]);
            
            //1st
            }elseif(Carbon::createFromFormat('Y-m-d', $get_details1->applicationlevel->date_accreditation_expiry)->addMonths(-2) <= Carbon::today() AND $get_details1->applicationlevel_reminders->deadline_reminder_1st == null){

              Mail::send('email.email_template1', compact('body', 'get_details1'), function($message) use ($get_details1, $body, $emailTemplate){
              $message->to('phrebaccreditationsystem@gmail.com');
              $message->cc([$get_details1->rec_email, $get_details1->rec_contactemail, $get_details1->recchairs->rec_chairemail]);
              $message->subject($emailTemplate->email_subject.' for '.$get_details1->rec_name.'-'.$get_details1->rec_institution);
              });
                emailogs_Save($get_details1, $emailTemplate, $body);
                $get_details1->applicationlevel_reminders()->update(['deadline_reminder_1st' => Carbon::now()]);

            }
                
        }
    }


    //reminder for arps deadline//
    function emailTemplate1(){
      $emails = Recdetails::get();

        foreach($emails as $get_details1){
            $emailTemplate = EmailTemplate::find(23);
            $body = str_replace(['$rec_institution','$rec_name','$level','$year','$deadline'], [$get_details1->rec_institution, $get_details1->rec_name,  $get_details1->applicationlevel->levels->level_name, Carbon::today()->format('Y'), Carbon::today()->EndOfYear()->addMonths(2)->format('F j, Y')], $emailTemplate->email_body);

            Mail::send('email.email_template1', compact('body', 'get_details1'), function($message) use ($get_details1, $body, $emailTemplate){
                $message->to([$get_details1->rec_email, $get_details1->rec_contactemail, $get_details1->recchairs->rec_chairemail]);
                $message->subject($emailTemplate->email_subject.' for '.$get_details1->rec_name.'-'.$get_details1->rec_institution);
                $message->cc('phrebaccreditationsystem@gmail.com');

                $sender = 1;
                $recdetails_id = $get_details1->applicationlevel->recdetails_id;
                $cc = 'phrebaccreditationsystem@gmail.com';
                $from = $get_details1->rec_contactemail.', '.$get_details1->recchairs->rec_chairemail;
                $to = $get_details1->rec_email;
                $bcc = '';
                $subject = $emailTemplate->email_subject.' for '.$get_details1->rec_name.'-'.$get_details1->rec_institution;
                $body = $body;
                emaillogs($sender, $recdetails_id, $from, $to, $cc, $bcc, $subject, $body);
            });            
        }
    }

    //reminder for acc-deadline (Specific)
    function emailTemplate2($id){
      $emails = Recdetails::find($id);

      $emailTemplate = EmailTemplate::find(24);
      $body = str_replace(['$rec_institution','$rec_name','$level','$date_accreditation_expiry','$secname'], [$emails->rec_institution, $emails->rec_name,  $emails->applicationlevel->levels->level_name, \Carbon\Carbon::createFromFormat('Y-m-d',$emails->applicationlevel->date_accreditation_expiry)->format('F j, Y'), auth::user()->name], $emailTemplate->email_body);

      Mail::send('email.email_template1', compact('body'), function($message) use ($emails, $body, $emailTemplate){
            $message->to($emails->rec_email);
            $message->cc([$emails->rec_contactemail, $emails->recchairs->rec_chairemail]);
            $message->subject($emailTemplate->email_subject.' for '.$emails->rec_name.'-'.$emails->rec_institution);

            $sender = 1;
            $recdetails_id = $emails->applicationlevel->recdetails_id;
            $cc = '';
            $from = auth::user()->email;
            $to = $emails->rec_email;
            $bcc = 'phrebaccreditationsystem@gmail.com';
            $subject = $emailTemplate->email_subject.' for '.$emails->rec_name.'-'.$emails->rec_institution;
            $body = $body;
            emaillogs($sender, $recdetails_id, $from, $to, $cc, $bcc, $subject, $body);
          });
      return redirect()->back()->withSuccess($emailTemplate->email_subject.' is Successfully Sent!');
    }


    //for if else in save in emaillogs
    function emailogs_Save($get_details1, $emailTemplate, $body){
        $sender = 1;
        $recdetails_id = $get_details1->applicationlevel->recdetails_id;
        $cc = $get_details1->rec_email.', '.$get_details1->rec_contactemail.', '.$get_details1->recchairs->rec_chairemail;
        $from = 'phrebaccreditationsystem@gmail.com';
        $to = $get_details1->rec_email;
        $bcc = 'phrebaccreditationsystem@gmail.com';
        $subject = $emailTemplate->email_subject.' for '.$get_details1->rec_name.'-'.$get_details1->rec_institution;
        $body = $body;
        emaillogs($sender, $recdetails_id, $from, $to, $cc, $bcc, $subject, $body);
    }


    //helper for PHREB FORMS---------------------------
    function getPhrebformTempID($id){
      $phrebformID = Phrebforms_template::find($id);
      return $phrebformID;
    }

}