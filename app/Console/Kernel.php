<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // add the ff. 
        Commands\emailUser::class,
        Commands\sample::class,
        Commands\emailremindersexpired::class,
        Commands\Sendemail::class,
        Commands\StatusEmail::class,
        Commands\GenerateReport::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('email:a_dateofexpiry')->dailyAt('00:01');
        $schedule->command('email:user')->weeklyOn(4, '10:30');
        $schedule->command('command:sample')->everyMinute();
        $schedule->command('command:expiryreminder')->monthlyOn(28, '15:00');
        $schedule->command('command:sendemailreminder')->everyMinute();  
        $schedule->command('command:statusemail')->everyMinute(); 
        $schedule->command('command:generatereport')->weeklyOn(4, '10:30');
        
        // $schedule->command('command:generatereport')->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
