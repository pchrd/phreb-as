<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\EmailTemplate;
use App\Models\Recdetails;
use App\Models\Sendemailreminder;
use Carbon;
use Mail;


class emailremindersexpired extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:expiryreminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reminder PHREB Accreditation Expiry Date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {   
        // email reminders 
        // $rec = Recdetails::with('applicationlevel')->whereHas('applicationlevel', function($query){

        //     $query->whereBetween('date_accreditation_expiry', [\Carbon\Carbon::now(), \Carbon\Carbon::now()->addMonths(6)])
        //     ->orWhere('date_accreditation_expiry', '<=', \Carbon\Carbon::now())
        //     ->where('statuses_id', 4)
        //     ->whereIn('level_id', [1,3]);

        // })->get();

        $rec = Recdetails::join('applicationlevel', 'applicationlevel.recdetails_id', 'recdetails.id')
        ->whereBetween('date_accreditation_expiry', [\Carbon\Carbon::now(), \Carbon\Carbon::now()->addMonths(6)])
        // ->orWhere('date_accreditation_expiry', '<=', \Carbon\Carbon::now())
        ->where('statuses_id', 4)
        ->whereIn('level_id', [1, 2, 3])
        ->get();

        // $rec = Recdetails::join('applicationlevel', 'applicationlevel.recdetails_id', 'recdetails.id')
        // ->where('statuses_id', 4)
        // ->whereIn('level_id', [1])
        // ->whereBetween('date_accreditation_expiry', [\Carbon\Carbon::now(), \Carbon\Carbon::now()->addMonths(6)])
        // // ->where('date_accreditation_expiry', '<=', \Carbon\Carbon::now())
        // ->get();



        // $emailTemplate = EmailTemplate::find(25);

        foreach($rec as $id => $recs){

        //     $body = str_replace(['$rec_institution','$rec_name','$level','$date_accreditation_expiry', '$days', '$secname'], [$recs->rec_institution, $recs->rec_name,  $recs->applicationlevel->levels->level_name, \Carbon\Carbon::createFromFormat('Y-m-d',$recs->applicationlevel->date_accreditation_expiry)->format('F j, Y') ?? '---'], $emailTemplate->email_body);

        //     // $body = str_replace(['$rec_institution','$rec_name','$level','$date_accreditation_expiry','$secname'], [$emails->rec_institution, $emails->rec_name,  $emails->applicationlevel->levels->level_name, \Carbon\Carbon::createFromFormat('Y-m-d',$emails->applicationlevel->date_accreditation_expiry)->format('F j, Y'), auth::user()->name], $emailTemplate->email_body);

        //     Mail::send('email.email_template1', compact('body'), function($message) use ($emailTemplate, $recs){
        //         // $message->cc($recs->users->email ?? '');
        //         // $message->to([$recs->rec_email, $recs->rec_contactemail, $recs->recchairs->rec_chairemail]);
        //         $message->bcc(['jrcambonga@pchrd.dost.gov.ph']);
        //         $message->subject($emailTemplate->email_subject.' - '. $recs->rec_institution.' - '.$recs->rec_name.' | '.$recs->applicationlevel->levels->level_name);

        //     });

            $add = Sendemailreminder::create([
                'ser_recdetails_id' => $recs->id,
            ]);

        }

       
    }
}
