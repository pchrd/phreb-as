<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\EmailTemplate;
use App\Models\Recdetails;
use App\Models\Sendemailreminder;
use Carbon\Carbon;
use Mail;

class Sendemail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:sendemailreminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reminder PHREB Accreditation Expiry Date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sendreminders = Sendemailreminder::where('ser_emailsent_date', null)->get(); 

        $emailTemplate = EmailTemplate::find(25);

        foreach ($sendreminders as $key => $sendreminder) {
            

            $body = str_replace(['$rec_institution','$rec_name','$level','$date_accreditation_expiry', '$days', '$secname'], [$sendreminder->recdetails->rec_institution, $sendreminder->recdetails->rec_name,  $sendreminder->recdetails->applicationlevel->levels->level_name, \Carbon\Carbon::createFromFormat('Y-m-d',$sendreminder->recdetails->applicationlevel->date_accreditation_expiry)->format('F j, Y') ?? '---'], $emailTemplate->email_body);


            Mail::send('email.email_template1', compact('body'), function($message) use ($emailTemplate, $sendreminder){
                
                $message->cc($sendreminder->recdetails->users->email ?? '');
                $message->to([$sendreminder->recdetails->rec_email, $sendreminder->recdetails->rec_contactemail, $sendreminder->recdetails->recchairs->rec_chairemail]);
                $message->bcc(['ethics.secretariat@pchrd.dost.gov.ph']);
                $message->replyTo('ethics.secretariat@pchrd.dost.gov.ph');

                $message->subject($emailTemplate->email_subject.' '.$sendreminder->recdetails->applicationlevel->levels->level_name.' - '.$sendreminder->recdetails->rec_institution.' - '.$sendreminder->recdetails->rec_name);

            });

            $update = $sendreminder->update(['ser_emailsent_date' => Carbon::now()]);
        }
    }

}
