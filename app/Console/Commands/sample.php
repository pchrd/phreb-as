<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
// use Mail;

class sample extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:sample';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sample Email Only';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Mail::raw('Hello good day! this is only a sample email from Accreditation (in Every Minute Mode)', function ($message) {
        //     $message->to(['jrcambonga@pchrd.dost.gov.ph', 'rgbahala@pchrd.dost.gov.ph'])->subject('Test: Sample Email Only');
        // });
    }
}
