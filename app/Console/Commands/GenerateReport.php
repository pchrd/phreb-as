<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Recdetails;
use App\Models\Applicationlevel;
use App\Models\EmailTemplate;
use App\Models\Level;
use App\Models\Pdfgeneratereport;
use Carbon\Carbon;
use PDF;
use Mail;
use Auth;
use DB;

class GenerateReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:generatereport';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PHREB Accreditation Yearly Report';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {   
        $start = Carbon::createFromDate(Carbon::now()->year, 1, 1)->copy()->startOfYear()->format('Y-m-d');
        // $start = "2020-01-01";
        $end   = Carbon::now()->format('Y-m-d');

        $numberofrecs = 0;

        // $recs = Recdetails::join('applicationlevel', 'applicationlevel.recdetails_id', 'recdetails.id')->select([
            
        //     // DB::raw('rec_name as rec_name'),
        //     DB::raw('recdetails.id as rec_id'),
        //     DB::raw('count(recdetails.id) as recs'),
        //     DB::raw('MONTHNAME(date_accreditation) as months'),
        //     DB::raw('MONTH(date_accreditation) as monthname'),
        //     DB::raw('year(date_accreditation) as year'),

        //     // DB::raw('DATE_FORMAT(applicationlevel.date_accreditation, "%d-%b-%Y") as formatted_date'),
        //     // DB::raw('GROUP_CONCAT(DISTINCT rec_institution SEPARATOR "<br><br>") as "rec_institution"'),

        //     DB::raw('GROUP_CONCAT("[L", level_id, "]: ", rec_institution, " - ", rec_name, " <small style=\'color: red\'>(", DATE_FORMAT(applicationlevel.date_accreditation, "%b %d, %Y"), ")</small>" SEPARATOR "<br><br>") as "rec_institution"'),
        //     DB::raw('GROUP_CONCAT(level_id SEPARATOR "<br><br>") as "level"'),
        // ])
        // ->whereBetween('date_accreditation', ["$start","$end"])
        // ->where('statuses_id', 4)
        // ->orderBy('monthname', 'asc')
        // // ->distinct('months')
        // ->groupBy(['months', 'year'])
        // ->get();

        $recs = Recdetails::with('applicationlevel')->whereHas('applicationlevel', function($query) use($start, $end){
            $query->whereBetween('date_accreditation', [$start, $end])->where('statuses_id', 4);
        })->get();

        $mapped_recs = $recs->map(function ($query, $key) {
            return [
                'rec_id' => $query->id,
                'rec_name' => $query->rec_institution . ' ' . $query->rec_name,
                'rec_level' => $query->applicationlevel->levels->id,
                'month_year' => Carbon::parse($query->applicationlevel->date_accreditation)->format('F Y'),
                'month_number' => Carbon::parse($query->applicationlevel->date_accreditation)->month,
                'date_accreditation' => Carbon::createfromFormat('Y-m-d', $query->applicationlevel->date_accreditation)->format('F j, Y'),
                'date_accreditation_expiry' => Carbon::createfromFormat('Y-m-d', $query->applicationlevel->date_accreditation_expiry)->format('F j, Y'),
                'rec_count' => 1,
            ];
        })->sortBy('month_number')->groupBy('month_year')->map(function ($group) {
            return [
                'recs' => $group,
                'count' => $group->count(), // Total count for each month group
            ];
        });

        $grand_total = $mapped_recs->sum('count');

        // $levels = Level::join('applicationlevel', 'applicationlevel.level_id', 'levels.id')->whereBetween('date_accreditation', ["$start","$end"])->where('statuses_id', 4)->select(DB::raw("COUNT(*) as counts, level_name"))->groupBy('level_name')->get();

        // $institution = Recdetails::with('applicationlevel')->whereHas('applicationlevel' , function($query){
        //     $query->where('statuses_id', 4);
        // })->get();

        $emailTemplate = EmailTemplate::find(42);

        // foreach ($recs as $key => $rec) {
        //     # code...
      
        //     $months[] = $rec->months;
        //     $recnames[] = $rec->rec_institution;

        //     $month_val = implode("<br><br>", $months);
        //     $rec_val = implode("<br>", $recnames);

        // }

        // $body = str_replace(['$year', '$month', '$recname', '$level', '$dateaccredited'], [Carbon::now()->year, $month_val, $rec_val, 'level', 'dateaccredited'], $emailTemplate->email_body);

        // $uniqID = uniqid();

        // $addpdffile = Pdfgeneratereport::create([
        //     'pdf_uniqid' => $uniqID
        // ]);

        // $pdf = PDF::loadview('email.email_template3', compact('recs','levels'))->save(storage_path('/app/public/generatedreports/'.$uniqID.'.pdf'));

        $email = \Mail::send('email.email_template2', compact('mapped_recs', 'grand_total'), function($message) use ($emailTemplate) {

                $message->to(['ethics.secretariat@pchrd.dost.gov.ph'])->subject(Carbon::now()->year.' '.$emailTemplate->email_subject.' as of '.Carbon::now()->format('F j, Y'));
        });

       
    }
}
