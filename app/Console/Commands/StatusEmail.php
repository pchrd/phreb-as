<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\EmailTemplate;
use App\Models\Submissionstatuses;
use App\Models\Recdetails;
use Carbon\Carbon;
use Storage;
use Mail;
use Auth;

class StatusEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:statusemail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PHREB Accreditation Status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $statuses = Submissionstatuses::where('submissionstatuses_emailsend', null)->get();

        foreach ($statuses as $key => $status) {
            
            if($status->submissionstatuses_statuses_id == 3){

                $emailTemplate = EmailTemplate::find(2);
            
            }elseif($status->submissionstatuses_statuses_id == 9){

                $emailTemplate = EmailTemplate::find(3);
            
            }elseif($status->submissionstatuses_statuses_id == 2){

                $emailTemplate = EmailTemplate::find(4);
            
            }elseif($status->submissionstatuses_statuses_id == 4){

                $emailTemplate = EmailTemplate::find(20);
            
            }elseif($status->submissionstatuses_statuses_id == 27){

                $emailTemplate = EmailTemplate::find(43);
            
            }elseif($status->submissionstatuses_statuses_id == 19 OR $status->submissionstatuses_statuses_id == 11 OR $status->submissionstatuses_statuses_id == 5 OR $status->submissionstatuses_statuses_id == 17 OR $status->submissionstatuses_statuses_id == 21 OR $status->submissionstatuses_statuses_id == 22 OR $status->submissionstatuses_statuses_id == 23 OR $status->submissionstatuses_statuses_id == 24 OR $status->submissionstatuses_statuses_id == 25 OR $status->submissionstatuses_statuses_id == 26){

                $emailTemplate = EmailTemplate::find(5);
            }

                $get_details1 = Recdetails::find($status->submissionstatuses_recdetails_id);

                $find = ['$rec_name', '$rec_institution', '$level_name', '$status_name', '$deadline', '$fee', '$sender', '$date_accreditation', '$emailmessage'];

                $status->submissionstatuses_duedate == true ? $duedate = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $status->submissionstatuses_duedate->submissionstatuses_duedate)->format('F j, Y') : $duedate = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', \Carbon\Carbon::now()->addDays(7))->format('F j, Y'); 

                $replace = [$status->recdetails->rec_name, $status->recdetails->rec_institution, $status->recdetails->applicationlevel->levels->level_name, $status->status->status_name, $duedate, $status->recdetails->applicationlevel->level_id == 3 ? '5,000.00' : '3,000.00', $status->users->name, Carbon::today()->format('F j, Y'), $status->submissionstatuses_emailmessage];

                $body = str_replace($find, $replace, $emailTemplate->email_body);

                Mail::send('email.email_template', compact('body', 'status', 'emailTemplate', 'get_details1'), function($message) use ($body, $status){

                 $message->cc('ethics.secretariat@pchrd.dost.gov.ph');
                 $message->to([$status->recdetails->users->email, $status->recdetails->rec_email, $status->recdetails->rec_contactemail,$status->recdetails->recchairs->rec_chairemail]);
                 $message->bcc($status->users->email);
                    // $message->to('jrcambonga@pchrd.dost.gov.ph');
                 $message->replyTo('ethics.secretariat@pchrd.dost.gov.ph');

                 $message->subject('['.$status->recdetails->applicationlevel->levels->level_name.']: '.$status->recdetails->rec_institution.' - '.$status->recdetails->rec_name.' | PHREB Secretariat Assessment on REC Application');

                 if($status->emailattachments == true){

                     foreach($status->emailattachments as $files){

                         $path = Storage::disk('public')->path('documentsEmailattachments/'.$files->re_user_id.'/'.$files->re_recdetails_id.'/'.$files->re_file_generated.'-'.$files->re_file_name);

                         // $message->attach($path, ['as' => $files->getClientOriginalName(), 'mime' => $files->getMimeType()]);

                         $message->attach($path, ['as' => $files->re_file_name]);
                     }
                     
                 }  

                });

            $status->update(['submissionstatuses_emailsend' => Carbon::now()]);
        }

    }
}
