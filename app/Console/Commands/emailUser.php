<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\EmailTemplate;
use auth;
use Mail;

class emailUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reminder for Weekly Updating of Accredited/Re-accredited RECs in the website';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle()
    {
        $emailTemplate = EmailTemplate::find(39);
        $body = $emailTemplate->email_body;

        Mail::send('email.email_template1', compact('body'), function($message) use ($emailTemplate){
            $message->to(['accreditation-level1@pchrd.dost.gov.ph', 'accreditation-level2@pchrd.dost.gov.ph', 'accreditation-level3@pchrd.dost.gov.ph', 'remb1.ethics@pchrd.dost.gov.ph', 'remb6.ethics@pchrd.dost.gov.ph', 'remb11.ethics@pchrd.dost.gov.ph', 'jrcambonga@pchrd.dost.gov.ph']);
            $message->subject($emailTemplate->email_subject);
        });
    }


}
