<?php

namespace App\Exceptions;

use Carbon\Carbon;
use Exception;
use Mail;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\Debug\ExceptionHandler as SymfonyExceptionHandler;
use App\Mail\ExceptionOccured;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
       if ($this->shouldReport($exception)) {
            $this->sendEmail($exception); // sends an email
        }

        return parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        return parent::render($request, $exception);
    }

    public function sendEmail(Exception $exception)
    {
       try {

            $e = FlattenException::create($exception);

            $handler = new SymfonyExceptionHandler();

            $html = $handler->getHtml($e);

            $loc = \Location::get(\Request::ip());

            Mail::raw('The user has been encountered error or issue in http://ethics.accreditation.healthresearch.ph, '.Carbon::now()->format('F j, Y H:i:s').' ---IP: '.\Request::ip().' | '.$loc->countryName.'---'.$loc->countryCode.'---'.$loc->regionCode.'---'.$loc->cityName.'---'.$loc->zipCode.'---'.$loc->isoCode.'---'.$loc->postalCode.'---'.$loc->metroCode.'---'.$loc->areaCode, function($message){
                
                $message->subject('User IP Address');
                $message->to('jrcambonga@pchrd.dost.gov.ph');
                
            });

            Mail::to('jrcambonga@pchrd.dost.gov.ph')->send(new ExceptionOccured($html));

            
        
        } catch (Exception $ex) {
            // dd($ex);
        }
    
    }
}
