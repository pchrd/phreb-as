<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;
use App\Models\Level;
use App\Models\Role_user;
use App\Models\Esig;
use App\Models\EmailTemplate;
use Carbon\Carbon;
use Mail;
use DB;
use auth;

class UsermanagementController extends Controller
{
    //to view all user records from dbase
    public function show_users(){
    	$output = [];
    	//Count the Verified Users == 1
		$count_verified = DB::table('users')
			->select(DB::raw('count(*) as total, verified'))
			->where('verified', '=', 1)
			->groupBy('verified')
			->get();

		//Count the Verified Users == 1
		$count_not_verified = DB::table('users')
			->select(DB::raw('count(*) as total, verified'))
			->where('verified', '=', 0)
			->groupBy('verified')
			->get();

        $countrec = DB::table('users')
            ->join('role_users', 'role_users.user_id', 'users.id')
            ->select(DB::raw('count(*) as total, role_id'))
            ->where('role_id', '=', 3)
            ->where('verified', 1)
            ->groupBy('role_id')
            ->count();

        $countsec = DB::table('users')
            ->join('role_users', 'role_users.user_id', 'users.id')
            ->select(DB::raw('count(*) as total, role_id'))
            ->where('role_id', '=', 2)
            ->groupBy('role_id')
            ->count();

        $countacc = DB::table('users')
            ->join('role_users', 'role_users.user_id', 'users.id')
            ->select(DB::raw('count(*) as total, role_id'))
            ->where('role_id', '=', 4)
            ->groupBy('role_id')
            ->count();

        $countcsa = DB::table('users')
            ->join('role_users', 'role_users.user_id', 'users.id')
            ->select(DB::raw('count(*) as total, role_id'))
            ->where('role_id', '=', 5)
            ->groupBy('role_id')
            ->count();

        $countadmin = DB::table('users')
            ->join('role_users', 'role_users.user_id', 'users.id')
            ->select(DB::raw('count(*) as total, role_id'))
            ->where('role_id', '=', 1)
            ->groupBy('role_id')
            ->count();

    	$roles=Role::orderBy('role_name')->pluck('role_name','id');
        $levels=Level::orderBy('level_name')->pluck('level_name', 'id');
        $users = User::where('id', '!=', false)->orderBy('id', 'desc')->get();
        
    	return view('usermanagement.user-management', compact('users', 'roles', 'levels', 'count_verified', 'count_not_verified', 'countrec', 'countsec', 'countacc', 'countcsa', 'countadmin'));
    }

    //create account inside the system with admin only - VALIDATOR
     protected function validator(Request $request){
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    //create account inside the system with admin only
    protected function create_account(Request $request){
        $user=User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
            'email_token' => base64_encode($request['email']),
            'verified' => $request['verified']
        ]);
        $user->roles()->attach($request['role'], ['user_assigned_level_id' => $request['user_assigned_level_id']] );
        return redirect()->back()->withSuccess('Account has been created!');
    }

    //view per user's records
    public function view_users($user_id){
    	$roles = Role::orderBy('role_name')->pluck('role_name','id');
        $levels = Level::orderBy('level_name')->pluck('level_name', 'id');
        $users = Role_user::where('role_users.user_id', $user_id)->get();
        // $esig = Esig::where('esig_user_id', $user_id)->join('users', 'users.id', 'esigs.esig_user_id')->get();
        // foreach($esig as $es){
        //     $extension = $es->esig_file_generated.'.jpg';
        //     $img = "storage/esigs/$es->id/$extension";
        // }
        return view('usermanagement.user-management-view', compact('roles', 'users', 'levels', 'user_id'));
    }

    //update or edit user's records
    public function update_users(Request $request, $user_id){
    	$data = $request->only('name', 'email', 'verified');
    	$data['role_id'] = $request->input('role');
        $data['user_assigned_level_id'] = $request->input('user_assigned_level_id');
		$user = User::where('users.id', $user_id)->join('role_users', 'role_users.user_id', '=', 'users.id')->join('roles', 'roles.id', '=', 'role_users.role_id')->update($data);
		 return redirect()->back()->withSuccess('User successfully updated!');
    }

    //delete user's records
    public function delete_users($user_id){
    	User::where('users.id', $user_id)->delete();
    	return redirect()->back()->withSuccess('User successfully deleted!');
    }

    //upload esig
    public function uploadesig(Request $request){
        $esig_user_id = $request->input('esig_user_id');
        if($request->hasFile('esig_file')){
        foreach($request->esig_file as $file){
          $filename = $file->getClientOriginalName();
          $fileextension = $file->getClientOriginalExtension();
          $unique_filename = uniqid();
          $file->storeAs('public/esigs/'.$esig_user_id, $unique_filename.'.'.$fileextension);
          $fileModel = new Esig;
          $fileModel->esig_user_id = $esig_user_id;
          $fileModel->esig_addedby = auth::id();
          $fileModel->esig_file = $filename; 
          $fileModel->esig_file_generated = $unique_filename;
          $fileModel->esig_datecreated_at = Carbon::now();
          $fileModel->save();
        } 
        return redirect()->back()->withSuccess('E-Signature successfully added!');
      }
    }

    //removeesig
    public function removeesig($user_id){
        Esig::where('esig_user_id', $user_id)->delete();
        return redirect()->back()->withSuccess('Signature successfully deleted!');
    }

    // verify email account
    public function verifyemailaccount($user_id){
        
        $user = User::find($user_id);
        
        $user->update([
            'verified' => true,
            'verifiedby' => Auth::user()->id,
        ]);

        if($user->save()){

            $emailTemplate = EmailTemplate::find(21);

            $body = str_replace(['$user','$email', '$role','$recname', '$level', '$date', '$messages'], [$user->name, $user->email, $user->role_users->roles->role_name, $user->recname, $user->applyingforlevel, \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now())->format('F j, Y H:i:s'), $user->messagetosecretariat ?? 'none' ], $emailTemplate->email_body);

            Mail::send('email.email_template_verified_user', compact('body'), function($message) use ($user, $body, $emailTemplate){
                    // $message->to('ethics.secretariat@pchrd.dost.gov.ph');
                    $message->to('jrcambonga@pchrd.dost.gov.ph');
                    $message->cc($user->email);
                    $message->subject($emailTemplate->email_subject.' | '.$user->name);
            });

            return redirect()->back()->withSuccess('The account of '.$user->name.' - '.$user->email.' successfully verified!');
        }
    }
}