<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Accreditorslist;
use App\Models\AccreditorsEvaluationCriteria;
use DataTables;

class AccreditorslistController extends Controller{
    
    public function show(){
    	return view('accreditors.accreditors');
    }

    public function show_ajax(){
    	$accreditorslist = Accreditorslist::all();
    	return DataTables::of($accreditorslist)->addColumn('action', function ($accreditorslist){
					return '<a href="'.route('view-accreditor', ['id' => encrypt($accreditorslist->id)]).'" title="View and Edit" class="btn btn-xs btn-success"><i class="fa fa-eye"> View</i></a> <a href="'.route('create-eval', ['id' => encrypt($accreditorslist->id)]).'" title="Evaluate" class="btn btn-xs btn-info"><i class="fa fa-check"></i> Evaluate</a> <a href="'.route('delete_acc', ['id' => $accreditorslist->id]).'" title="Delete" class="btn btn-xs btn-danger" onclick=""><i class="fa fa-trash-o"></i></a>';
				})->make(true);
    }

    public function add_acc(Request $request){
        $save = new Accreditorslist($request->all());
        $save->save();
    	return redirect()->back()->withSuccess('Accreditor Successfully Added!');
    }

    public function view_acc($id){
    	$acc = Accreditorslist::find(decrypt($id));
    	return view('accreditors.view-accreditor', compact('acc'));
    }

    public function update_acc(Request $request, $id){
    	Accreditorslist::find($id)->update([
          'accreditors_name' => $request->input('accreditors_name'),
          'accreditors_designation' => $request->input('accreditors_designation'),
          'accreditors_gender' => $request->input('accreditors_gender'),
          'accreditors_specialization' => $request->input('accreditors_specialization'),
          'accreditors_office' => $request->input('accreditors_office'),
          'accreditors_address' => $request->input('accreditors_address'),
          'accreditors_contactnumber' => $request->input('accreditors_contactnumber'),
          'accreditors_email' => $request->input('accreditors_email')
    	]);
    	return redirect()->back()->withSuccess('Data Successfully Updated!');
    }

    // methods for criteria module
    public function gotoacccriteria(){
        $criterialist = AccreditorsEvaluationCriteria::all();
        return view('accreditors.accreditors-criteria', compact('criterialist'));
    }

    public function addcriteria(Request $request){
        $savecriteria = new AccreditorsEvaluationCriteria($request->all());
        $savecriteria->save();
        return redirect()->back()->withSuccess('Criteria Successfully Added!');
    }

    public function update_criteria(Request $request, AccreditorsEvaluationCriteria $id){
        $id->update([
            'criteria_name' => $request->criteria_name,
            'criteria_description' => $request->criteria_description,
            'criteria_percentage' => $request->criteria_percentage
        ]);
        return redirect()->back()->withSuccess('Criteria has been updated!');
    }

    public function delete_acc($id){
        $delete = Accreditorslist::find($id)->delete();
        return redirect()->back()->withSuccess('Successfully deleted in the list!');
    }
}