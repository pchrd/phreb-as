<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Trainingeventstrainees;
use App\Models\Accreditation_role;
use App\Models\Trainingeventstrainees_affiliations;
use DataTables;
use Auth;

class TrainingeventstraineesController extends Controller{
    
    public function show(){
      $rec_role = $this->getRecRole();
    	return view('trainees.show-trainees-list', compact('rec_role'));
    }

    public function show_ajax(){
    	$trainees = Trainingeventstrainees::with('acc_recroles');
    	return DataTables::of($trainees)
      ->addColumn('action', function ($trainees){
					return '<a href="'.route('view-trainees-list', ['id' => $trainees->id]).'" title="View and Edit" class="btn btn-xs btn-success edit"><i class="fa fa-pencil"></i></a><a href="" title="Delete" class="btn btn-xs btn-danger" onclick=""><i class="fa fa-trash-o"></i></a>';
				})->make(true);
    }

    public function view($id){
    	$trainee = Trainingeventstrainees::find($id);
      $rec_role = $this->getRecRole();
      //$rec_committee = $this->getCommittee();
    	return view('trainees.view-trainees-list', compact('trainee', 'rec_role', 'rec_committee'));
    }

    public function update(Request $request, $id){
    	$trainee = Trainingeventstrainees::find($id);
    	$trainee->update($request->all());
    	return redirect()->back()->withSuccess('Succcessfuly updated!');
    }

    public function add(Request $request){

    	$add_trainee = Trainingeventstrainees::create([
   			'trainees_lname' => $request->trainees_lname,
   			'trainees_fname' => $request->trainees_fname,
        'trainees_mname' => $request->trainees_mname,
   			'trainees_gender' => $request->trainees_gender,
   			'trainees_institution' => $request->trainees_institution,
   			'trainees_certificatename' => $request->trainees_certificatename,
   		]);
   		return redirect()->back()->withSuccess($request->trainees_fname.' '.$request->trainees_lname.' Succcessfuly Added!');
    }

    public function getRecRole(){
      $acc_role = Accreditation_role::whereIn('id', [5,6,7,8,9,10,11])->pluck('roleinevent', 'id');
      return $acc_role;
    }

    public function add_affiliation(Request $request, $id){
        $create = Trainingeventstrainees_affiliations::create([
            'trainees_id' => $id,
            'addedby' => Auth::user()->id,
            'trainees_institution' => $request->trainees_institution,
            'trainees_institution_address' => $request->trainees_institution_address,
            'trainees_specialization' => $request->trainees_specialization,
            'trainees_email' => $request->trainees_email,
            'trainees_contactno' => $request->trainees_contactno,
            'trainees_profbackground' => $request->trainees_profbackground,
            'trainees_recrole' => $request->trainees_recrole,
        ]);
        return redirect()->back()->withSuccess('Affiliation Added!');
    }

    public function update_affiliation(Request $request, $id){
        $update = Trainingeventstrainees_affiliations::find($id)
          ->update($request->all());
        return redirect()->back()->withSuccess('Affiliation Updated!');
    }

}