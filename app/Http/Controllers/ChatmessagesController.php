<?php
namespace App\Http\Controllers;
use App\Models\User;
use App\Models\Role;
use App\Models\Role_user;
use App\Models\level;
use App\Models\Chatmessage;
use App\Models\ChatmessageFile;
use App\Models\EmailTemplate;
use DataTables;
use auth;
use Carbon\Carbon;
use DB;
use Mail;
use Illuminate\Http\Request;

class ChatmessagesController extends Controller
{
    //view page of chatmessage
	public function chatmessages(){

        if(Auth::user()->can('accreditors_and_csachair_secretariat')){
        
            $select_recipient = $this->pluckRecipient()->pluck('recipient', 'users.id');
        
        }else if(Auth::user()->can('rec-access')){

            $select_recipient = $this->pluckRecipient()->where('role_users.role_id', 2)->pluck('recipient', 'users.id');
        }

		return view('messages.chatmessages', compact('select_recipient'));
    }

    public function chatmessages_ajax(){

        $chatmessages = Chatmessage::join('chatmessage_tos', 'chatmessage_tos.message_id', 'chatmessages.id')
        ->leftjoin('chatmessage_replies', 'chatmessage_replies.cmr_replytomessage_id', 'chatmessages.id')
        ->where('chatmessage_tos.messageTo_id', Auth::user()->id)
        ->where('chatmessages.send_user_id', '!=', Auth::user()->id)
        // ->orWhere('chatmessages.message_status', 1)       
        ->select('chatmessages.id', 'chatmessages.messageFrom', 'chatmessages.messageSubject', 'chatmessages.messages',  'chatmessages.created_at', 'chatmessages.bcrypted_id', 'chatmessage_tos.messageStatus')
        ->groupby('chatmessages.id', 'chatmessages.messageFrom', 'chatmessages.messageSubject', 'chatmessages.messages', 'chatmessages.created_at', 'chatmessages.bcrypted_id', 'chatmessage_tos.messageStatus')
        //->orderBy('chatmessages.created_at', 'desc')
        ->orderBy('chatmessages.created_at', 'desc')
        ->orderByRaw(DB::raw("FIELD(chatmessage_replies.created_at, 'desc')"))
        ->get();
    
        return DataTables::of($chatmessages)->addColumn('action', function ($chatmessages){

            foreach($chatmessages->messageReply as $mr){
            
                if($mr->messages->message_status == 1){

                     return '<a href="'.route('view-mail', ['id' => $mr->messagesGetReply->bcrypted_id, 'reply' => encrypt($mr->messagesGetReply->id), '#' => encrypt($mr->messages->id)]).'#'.$mr->messages->id.'" class="fa fa-file-o"> View</a> <a href="" class="fa fa-trash"> Delete</a>';
                }
            }

            return '<a href="'.route('view-mail', ['id' => $chatmessages->bcrypted_id, 'reply' => encrypt($chatmessages->id), '#' => encrypt($chatmessages->id)]).'#'.$chatmessages->id.'" class="fa fa-file-o"> Reply</a> <a href="" class="fa fa-trash"> Delete</a>'; 
        })
        ->setRowClass(function ($chatmessages) {

            return $chatmessages->messageFrom != 0 ? 'label-success' : 'label-warning';
        })
        ->editColumn('messages', function ($chatmessages){

            return str_limit($chatmessages->messages, 50);
            
        })
        ->editColumn('no_of_replies', function ($chatmessages){

            return $chatmessages->messageGetReply->count();
            
        })
        ->editColumn('no_of_files', function ($chatmessages){

            return $chatmessages->messageFiles->count();
            
        })
        ->make(true);
    }

     public function chatmessages_ajax_sent(){

        $chatmessages = Chatmessage::where('chatmessages.send_user_id', Auth::user()->id)
        ->orderBy('chatmessages.id', 'desc')
        ->get();

        return DataTables::of($chatmessages)
        ->editColumn('messageTo', function ($chatmessages){

            $to = [];

            foreach ($chatmessages->messageTo as $key => $value) {
               $to[] = explode(', ',$value->users->email);
            }

            return $to;
        })
        ->editColumn('messages', function ($chatmessages){

            return str_limit($chatmessages->messages, 50);
            
        })
        ->editColumn('no_of_replies', function ($chatmessages){

            return $chatmessages->messageGetReply->count();
            
        })
        ->editColumn('no_of_files', function ($chatmessages){

            return $chatmessages->messageFiles->count();
            
        })
        ->addColumn('action', function ($chatmessages){

             foreach($chatmessages->messageReply as $mr){
            
                if($mr->messages->message_status == 1){

                     return '<a href="'.route('view-mail', ['id' => $mr->messagesGetReply->bcrypted_id, '#' => encrypt($mr->messages->id)]).'#'.$mr->messages->id.'" class="fa fa-file-o"> View</a> <a href="" class="fa fa-trash"> Delete</a>';
                }
            }
             return '<a href="'.route('view-mail', ['id' => $chatmessages->bcrypted_id, '#' => encrypt($chatmessages->id)]).'#'.$chatmessages->id.'" class="fa fa-file-o"> Reply</a> <a href="" class="fa fa-trash"> Delete</a>';  

        })->make(true);

    }

    public function viewMail(Request $request){

        $viewMail = $this->viewMailFunction($request->id);

        if($viewMail == true){
           return view('messages.view_mail', compact('viewMail')); 
        }else{
            return redirect()->back()->withSuccess('Access Denied!');
        }

    }

    public function viewmail_seen(Request $request){

        $viewMail = $this->viewMailFunction($request->id);

        if($viewMail == true){
           return view('messages.view_mail_from', compact('viewMail')); 
        }else{
            return redirect()->back()->withSuccess('Access Denied!');
        }
    }

    // ---------------------------HELPERS-----------

            public function pluckRecipient(){

                $select_recipient = User::join('role_users', 'role_users.user_id', 'users.id')
                ->join('roles', 'roles.id', 'role_users.role_id')
                ->select(DB::raw("CONCAT(name, ' <',email, '> ', ' - ', roles.role_name) AS recipient"), 'users.id');

                return $select_recipient;
            }

            public function viewMailFunction($id){

               $viewMail = Chatmessage::where('bcrypted_id', $id)
                ->with(['messageTo', 'messageGetReply'])
                ->whereHas('messageTo', function($query){
                    $query->whereIn('messageTo_id', [Auth::user()->id]);
                    $query->orWhere('send_user_id', Auth::user()->id);
                })
               ->first();

               if($viewMail->send_user_id != Auth::user()->id AND $viewMail->message_id->messageStatus == null){
                    $viewMail->message_id->update([
                        'messageStatus' => Carbon::now()
                    ]);
               }
               return $viewMail;
            }

            public function getEmail($param){
                $getmessage_to = Chatmessage::find($param);

                foreach ($getmessage_to->messageTo as $key => $mt) {
                    $values[] = $mt->users->email;
                }
                return $values;   
            }

    // ------------------------END HELPERS----------


    public function sendmessage(Request $request){
    	
        try{

            DB::beginTransaction();

            try{ 

                //1st insert
                $create = Chatmessage::create([
                    'send_user_id' => Auth::user()->id,
                    'messageFrom' => Auth::user()->email,
                    'messageSubject' => $request->messageSubject ?? '',
                    'messages' => $request->messageBody ?? ' ',
                    'bcrypted_id' => bcrypt(uniqid()),
                ]);

                //insert if has reply | reply_id
                if($request->has('reply')){

                    $create->update(['message_status' => true]);
                    $add = $create->messageReply()->create([
                        'cmr_replytomessage_id' => decrypt($request->reply),
                    ]);

                }else if($request->has('reply_id')){

                    $create->update(['message_status' => true]);
                    $add = $create->messageReply()->create([
                        'cmr_replytomessage_id' => decrypt($request->reply_id),
                    ]);

                }

                //link dependes on url
                if($request->has('reply') OR $request->has('reply_id')){
                    $link = route('view-mail', ['id' => $add->messagesGetReply->bcrypted_id, 'reply' => encrypt($add->messagesGetReply->id), '#' => encrypt($add->messages->id)]);
                }else{
                    $link = route('view-mail', ['id' => $create->bcrypted_id, 'reply' => encrypt($create->id), '#' => encrypt($create->id)]);
                }
               
                //second insert
                for($i = 0; $i < count($request->messageTo); $i++){

                    $to = [
                        'messageTo_id' => $request->messageTo[$i],
                    ];

                    $messageTo = $create->messageTo()->create($to);
                }

                //3rd insert
                if($request->hasFile('messageFile')){

                    foreach($request->messageFile as $file){

                      $unique_filename = uniqid();
                      $file->storeAs('public/messageAttachedfiles/'.Auth::user()->id, $unique_filename.'-'.$file->getClientOriginalName().'.'.$file->getClientOriginalExtension());

                      $fileModel = new ChatmessageFile;
                      $fileModel->cmf_message_id = $create->id;
                      $fileModel->cmf_filename = $file->getClientOriginalName();
                      $fileModel->cmf_fileextensionname = $file->getClientOriginalExtension();
                      $fileModel->cmf_filesize = $file->getSize();
                      $fileModel->cmf_filegenerated_id = $unique_filename;
                      $fileModel->save();
                    } 

                }else{

                    echo 'Upload Failed';
                }

                //sendmail
                $emailTemplate = EmailTemplate::find(32);

                foreach($create->messageTo as $mt){
                   $x[] = $mt->users->name.' ('.$mt->users->role_users->roles->role_name.')';
                   $y = implode(', ', $x);
                }

                $body = str_replace(['$name', '$sender', '$link'], [$y, $create->users->name.' ('.$create->users->role_users->roles->role_name.')', $link], $emailTemplate->email_body);

                Mail::send('email.email_template1', compact('body'), function($message) use ($create, $emailTemplate){
                    $message->subject($emailTemplate->email_subject);
                    $message->to($this->getEmail($create->id));
                    $message->cc([Auth::user()->email]);
                    $message->bcc('jrcambonga@pchrd.dost.gov.ph');
                    // $message->cc('ethics.secretariat@pchrd.gov.ph');
                });

            DB::commit();

            }catch(\Exception $e){

                return $e->getMessage();
            }

            }catch(\Exception $e){

                DB::rollback();
            }
    	
        return redirect()->back()->withSuccess('Message Successfully Sent!'); 
    }

    public function downloadFile($id){

        $file = ChatmessageFile::with(['messages'])
        ->whereHas('messages', function($query) use ($id){
            $query->where('cmf_filegenerated_id', decrypt($id));
        })->first();

        $path = storage_path('app/public/messageAttachedfiles/'.$file->messages->send_user_id.'/'.$file->cmf_filegenerated_id.'-'.$file->cmf_filename.'.'.$file->cmf_fileextensionname);

        return response()->download($path);
    }

    public function seen_status($chat_id){
    	$decrypt_chat_id = decrypt($chat_id);
    	Chatmessage::where('chatmessages.id', $decrypt_chat_id)->update(['seen_status' => Carbon::now()]);
    	return redirect('chatmessages');
    }

    public function showhistory(){
     return view('messages.chathistory');
    }

    public function showhistory_ajax(){
         $chathistory = Chatmessage::select('chatmessages.*', 'recdetails.rec_name', 'users.name')->join('recdetails', 'recdetails.user_id', 'chatmessages.reciever_user_id')->join('users', 'users.id', 'chatmessages.send_user_id');
        return DataTables::of($chathistory)->addColumn('time', function($emaillogs){
            return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$emaillogs->created_at)->diffForHumans();
        })->make(true);
    }
}	
