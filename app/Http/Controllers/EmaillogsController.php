<?php
namespace App\Http\Controllers;
use App\Models\Emaillogs;
use DataTables;
use Illuminate\Http\Request;

class EmaillogsController extends Controller
{
    public function showemaillogs(){
    	return view('historysubmission.emaillogs');
    }

    public function showemaillogs_ajax(){
    	// $emaillogs = Emaillogs::select('emaillogs.*', 'recdetails.rec_name', 'users.name', 'users.email')->join('users', 'users.id', 'emaillogs.e_sender_user_id')->join('recdetails', 'recdetails.id', 'emaillogs.e_recdetails_id');
      $emaillogs = Emaillogs::with('recdetails', 'users');
      return DataTables::of($emaillogs)->addColumn('e_time', function($emaillogs){
        return 'Added '.\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$emaillogs->e_created_at)->diffForHumans();
      })->addColumn('action', function ($emaillogs){
        return '<a href="email-logs-show/'.$emaillogs->id.'" class="btn btn-default fa fa-expand"></a>';
      })->make(true);
    }

    public function viewlogs($id){
      $emails = Emaillogs::find($id);
      return view('historysubmission.email-logs-show', compact('emails'));
    }
}
