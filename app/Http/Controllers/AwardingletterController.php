<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Awardingletter;
use App\Models\Recdetails;
use App\Models\EmailTemplate;
use App\Models\Emaillogs;
use DataTables;
use Carbon\Carbon;
use Auth;
use PDF;
use Mail;

class AwardingletterController extends Controller{

	public function show_a_letter(){
		return view('awardingletter.awarding_show');
	} 

	public function show_a_letter_ajax(){
		$awardingletter = Awardingletter::with(['recdetails','users','documenttypes'])->where('id', '!=', false)->orderBy('id', 'desc');
    	return DataTables::of($awardingletter)
    	->addColumn('actions', function ($awardingletter){
					return '<a href="awarding_view/'.$awardingletter->id.'/'.$awardingletter->recdetails->rec_name.'" title="View" target="_blank" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a> | <a href="delete/'.$awardingletter->id.'" onclick="return confirm(\'Do you want to Delete this record ID: '.$awardingletter->id.'?\')" title="Delete" class="btn btn-xs btn-default"><i class="fa fa-trash-o"></i></a>';
		})
		->addColumn('time', function ($awardingletter){
					return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$awardingletter->created_at)->diffForHumans();
		})->rawColumns(['actions', 'time'])->make(true);
	}

	public function store_a_letter(Request $request, Recdetails $recdetail, $doctypes){
		
		if($doctypes == 21){
			$years_no = 1;
		}elseif($doctypes == 22){
			$years_no = 2;
		}elseif($doctypes == 23 OR $doctypes == 24){
			$years_no = 3;
		}elseif($doctypes == 25){
			$years_no == 4;
		}

		$data = Awardingletter::create([
			'a_letter_recdetails_id' => $recdetail->id,
			'a_letter_saveduser_id' => auth::user()->id,
			'a_letter_doctypes' => $doctypes, 
			'a_letter_years' => $years_no, 
			'a_letter_body' => $request->input('editor1')
		]);
		return redirect()->route('awarding_view', ['id' => $data->id, 'name' => $data->recdetails->rec_name])->withSuccess('Data Successfully Saved!');
	}

	public function view_a_letter($id, $name){
		$awardingletter = Awardingletter::find($id);
		return view('awardingletter.awarding_view', compact('awardingletter', 'id'));
	}

	public function update_a_letter(Request $request, $id){
		Awardingletter::find($id)->update([
			'a_letter_body' => $request->input('editor1'), 
		]);
		return redirect()->back()->withSuccess('Successfully Updated!');
	}

	public function delete_a_letter($id){
		Awardingletter::find($id)->delete();
		return redirect()->back()->withSuccess('Successfully Deleted!');
	}

	public function pdf_a_letter($id){
		// $get_details1 = Awardingletter::join('applicationlevel', 'applicationlevel.recdetails_id', 'awardingletters.a_letter_recdetails_id')->find($id);

		$get_details1 = Awardingletter::find($id);

		$pdf = PDF::loadview('Awardingletter.awarding_pdf', compact('get_details1'));
		$pdf->setPaper('A4', 'Portrait');

		$emailTemplate = EmailTemplate::find(19);
		$body = str_replace(['$rec_chairname', '$secname'], [$get_details1->recdetails->recchairs->rec_chairname, Auth::user()->name], $emailTemplate->email_body);

		Mail::send('email.email_template', compact('body', 'get_details1'), function($message) use ($id, $pdf, $get_details1, $emailTemplate, $body){
	        	$message->to($get_details1->recdetails->recchairs->rec_chairemail);
	        	$message->cc([$get_details1->recdetails->rec_email, $get_details1->recdetails->rec_contactemail, $get_details1->recdetails->users->email]);
	        	$message->subject($emailTemplate->email_subject.' '.$get_details1->recdetails->rec_name);
	        	$message->attachData($pdf->output(), $get_details1->recdetails->rec_name.'-'.Carbon::now().'-'.$id.'.pdf');

	        	$sender = auth::id();
                $recdetails_id = $get_details1->applicationlevel->recdetails_id;
                $from = auth::user()->email;
                $to = $get_details1->recdetails->recchairs->rec_chairemail;
                $cc = $get_details1->recdetails->rec_email.', '.$get_details1->recdetails->rec_contactemail.', '.$get_details1->recdetails->users->email;
                $bcc = '';
                $subject = $emailTemplate->email_subject.' '.$get_details1->recdetails->rec_name;
                $body = $body;
                emaillogs($sender, $recdetails_id, $from, $to, $cc, $bcc, $subject, $body); 

	        	// $emaillogs = Emaillogs::create([
		        // 	'e_sender_user_id' => auth::id(),
		        // 	'e_recdetails_id' => $get_details1->recdetails_id,
		        // 	'e_from' => auth::user()->email,
		        // 	'e_to' => $get_details1->recdetails->recchairs->rec_chairemail,
		        // 	'e_cc' => $get_details1->recdetails->rec_email.', '.$get_details1->recdetails->rec_contactemail.', '.$get_details1->recdetails->users->email,
		        // 	'e_bcc' => '',
		        // 	'e_subject' => $emailTemplate->email_subject.' '.$get_details1->recdetails->rec_name,
		        // 	'e_body' => $body,
		        // 	'e_created_at' => Carbon::now()
	        	// ]);
		});
		$get_details1->update(['a_letter_datesend' => Carbon::now()]);
		$get_details1->applicationlevel->update(['date_accreditation_expiry' => Carbon::today()->addYear($get_details1->a_letter_years)]);
		return redirect()->back()->withSuccess('Awarding Letter Successfully Sent to '.$get_details1->recdetails->rec_name);
	}

	public function a_downloadpdf(Awardingletter $id){
		$get_details1 = Awardingletter::find($id->id);
		$pdf = PDF::loadview('Awardingletter.awarding_pdf', compact('id', 'get_details1'));
		$pdf->setPaper('A4', 'Portrait');
		return $pdf->download($get_details1->recdetails->rec_institution.'-'.$get_details1->recdetails->rec_name.'-'.Carbon::now().'-'.$id->id.'.pdf');
	}

}