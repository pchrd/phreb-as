<?php
namespace App\Http\Controllers;
use App\Models\Recdetails;
use App\Models\Applicationlevel;
use App\Models\Status;
use App\Models\Level;
use App\Models\Region;
use DB;
use Auth;
use DataTables;
use Illuminate\Http\Request;

class filterController extends Controller{

	public function getrecdetails(){
		$getrec = Recdetails::join('applicationlevel', 'applicationlevel.recdetails_id', 'recdetails.id')->join('levels', 'levels.id', 'applicationlevel.level_id')->join('regions', 'regions.id', 'applicationlevel.region_id')->join('statuses', 'statuses.id', 'applicationlevel.statuses_id')->orderBy('levels.id', 'asc');

		return $getrec;
	}

    public function searchfilter(){
       
    	$searchkey_level = Auth::user() ? \Request::get('filter') : decrypt(\Request::get('filter'));
    	$searchkey_region = Auth::user() ? \Request::get('region') : decrypt(\Request::get('region'));

    	$recdetails_show = $this->getrecdetails()->where('applicationlevel.statuses_id', 4)->where('levels.level_name', 'like', '%'.$searchkey_level.'%')->where('regions.region_name', 'like', '%'.$searchkey_region.'%');

        if(Auth()->user() == true){
             
             $recdetails  = Auth::user()->role_users->user_assigned_region_id == null ? $recdetails_show->get() : $recdetails_show->where('applicationlevel.region_id', Auth::user()->role_users->user_assigned_region_id)->get();

        }else{

             $recdetails = $recdetails_show->get();
        }
        
    	return view('filter.filter', compact('recdetails', 'searchkey_level', 'searchkey_region'));
    }

    public function searchpending(){

    	$searchkey_pending = \Request::get('search');
    	
        // $recdetails_show = $this->getrecdetails()->where('applicationlevel.statuses_id', 1)->where('applicationlevel.recdetails_submission_status', 1)->where('statuses.status_name', 'like', '%'.$searchkey_pending.'%');

        $recdetails_show = Recdetails::with('applicationlevel.status')->whereHas('applicationlevel', function($query) use($searchkey_pending){
            $query->where('applicationlevel.statuses_id', 1)->where('applicationlevel.recdetails_submission_status', 1);
        });

        $recdetails  = Auth::user()->role_users->user_assigned_region_id == null ? $recdetails_show->get() : $recdetails_show->whereHas('applicationlevel', function($query){
            $query->where('region_id', Auth::user()->role_users->user_assigned_region_id);
        })->get();

    	
        // $recdetails  = Auth::user()->role_users->user_assigned_region_id == null ? $recdetails_show->get() : $recdetails_show->where('applicationlevel.region_id', Auth::user()->role_users->user_assigned_region_id)->get();

        return view('filter.pending', compact('searchkey_pending', 'recdetails'));
    }

    public function filtersearch(Request $request){

        $selectlevels = Level::orderBy('level_description')->where('levels.id', '<=', 3)->pluck('level_description','id');
        $selectregions = Region::orderBy('id')->pluck('region_name','id');
        $status = Status::whereIn('id', [4,2,3,1,5,17])->orWhere('id', $request->status)->pluck('status_name','id');

        $statusName = Status::find($request->status);
        
        $regions = Region::select('region_name',DB::raw('count(*) as total'))->leftjoin('applicationlevel', 'applicationlevel.region_id', 'regions.id')->whereBetween('date_accreditation',[$request->start, $request->end])->where('statuses_id', $request->status)->groupBy('statuses_id', 'region_name')->where('level_id', 'LIKE', '%' .$request->level. '%')->where('region_id', 'LIKE', '%' .$request->region. '%')->get();

        $recdetails = Applicationlevel::where('statuses_id', $request->status)->whereBetween('date_accreditation', [$request->start, $request->end])->where('level_id', 'LIKE', '%' .$request->level. '%')->where('region_id', 'LIKE', '%' .$request->region. '%')->paginate(100);
        
        return view('recdetails.filtersearch', compact('request','recdetails', 'status', 'selectlevels', 'selectregions', 'status', 'regions', 'statusName'));
    }

    // iframe only
    public function filteriframe($id){

        $recdetails = $this->getrecdetails()->where('applicationlevel.statuses_id', 4)->where('levels.level_name', 'like', '%'.$id.'%')->get();

        return view('filter.filter-iframe', compact('recdetails'));
    }
}