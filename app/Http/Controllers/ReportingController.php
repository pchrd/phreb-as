<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Reporting;
use App\Models\Recdetails;
use App\Models\Level;
use App\Models\Applicationlevel;
use App\Models\Pdfgeneratereport;
use Response;
use DataTables;
use Storage;
use Auth;
use DB;
use PDF;

class ReportingController extends Controller{

	public function showreport_ajax(){

    	$reporting = Reporting::latest()->first();

    // 	$recdetail = Applicationlevel::where('statuses_id', 4)->whereBetween('date_accreditation',[$reporting->report_start_date, $reporting->report_end_date])->with(['recdetails', 'recdetails.status', 'status', 'regions', 'levels']);

    // 	return DataTables::of($recdetail)->editColumn('date_accreditationFORMAT', function ($recdetails_ajax) {
    //                 return $recdetails_ajax->date_accreditation != null ? \Carbon\Carbon::createFromFormat('Y-m-d',$recdetails_ajax->date_accreditation)->format('F j, Y') : '---';

    //             })->editColumn('date_accreditation_expiryFORMAT', function ($recdetails_ajax) {
    //                 return $recdetails_ajax->date_accreditation_expiry != null ? \Carbon\Carbon::createFromFormat('Y-m-d',$recdetails_ajax->date_accreditation_expiry)->format('F j, Y') : '---';

    //             })->addColumn('action', function ($recdetails_ajax){

				// 	return Auth::check() ? '<a href="view-recdetails/'.encrypt($recdetails_ajax->recdetails_id).'" title="View" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i></a>' : '<a href="view/'.base64_encode(encrypt($recdetails_ajax->recdetails_id)).'" title="View" class="fa fa-file-text-o"></a>';

				// })->make(true);


        $recdetail = Recdetails::join('applicationlevel', 'applicationlevel.recdetails_id', 'recdetails.id')->join('levels', 'levels.id', 'applicationlevel.level_id')->join('regions', 'regions.id', 'applicationlevel.region_id')->join('statuses', 'statuses.id', 'applicationlevel.statuses_id')->select('region_name', 'level_description', 'rec_institution', 'rec_name', 'status_name', 'date_accreditation', 'date_accreditation_expiry', 'recdetails_id')->where('statuses_id', 4)->whereBetween('date_accreditation',[$reporting->report_start_date, $reporting->report_end_date]);
        
        return DataTables::of($recdetail)->editColumn('date_accreditationFORMAT', function ($recdetails_ajax) {
                    return $recdetails_ajax->date_accreditation != null ? \Carbon\Carbon::createFromFormat('Y-m-d',$recdetails_ajax->date_accreditation)->format('F j, Y') : '---';

                })->editColumn('date_accreditation_expiryFORMAT', function ($recdetails_ajax) {
                    return $recdetails_ajax->date_accreditation_expiry != null ? \Carbon\Carbon::createFromFormat('Y-m-d',$recdetails_ajax->date_accreditation_expiry)->format('F j, Y') : '---';

                })->addColumn('action', function ($recdetails_ajax){

                    if(!Auth::check()){
                       
                        return '-';

                    }else if(Auth::user()->can('accreditors_and_csachair_secretariat')) {

                         return '<a href="view-recdetails/'.encrypt($recdetails_ajax->recdetails_id).'" title="View" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i></a>'; 
                    }
                    

                })->toJson(true);
    }

    public function showreport(){
        
    	$reporting = Reporting::latest()->first();
        $regions = Applicationlevel::select('region_name',DB::raw('count(region_name) as regionTotal'))->whereBetween('date_accreditation',[$reporting->report_start_date, $reporting->report_end_date])->where('statuses_id', 4)->join('regions', 'regions.id', 'region_id')->groupBy('region_name')->get();

        $levels = Applicationlevel::join('levels', 'levels.id', 'level_id')->join('regions', 'regions.id', 'region_id')->whereBetween('date_accreditation',[$reporting->report_start_date, $reporting->report_end_date])->where('statuses_id', 4)->select('level_id',DB::raw('COUNT(*) as levelTotal, levels.level_name'), DB::raw("COUNT('regions.region_name') as regionTotal, regions.region_name, GROUP_CONCAT(DISTINCT regions.region_name ORDER BY regions.id desc SEPARATOR '- ') as region"))->groupBy('level_id','level_name', 'regions.region_name') ->get();

        $count = Level::join('applicationlevel', 'applicationlevel.level_id', 'levels.id')->whereBetween('date_accreditation',[$reporting->report_start_date, $reporting->report_end_date])->where('statuses_id', 4)->select(DB::raw("COUNT(*) as counts, level_name"))->groupBy('level_name')->get();

        $pdfreport = Pdfgeneratereport::paginate(5);

    	return view('reporting.show-reporting', compact('reporting', 'regions', 'levels', 'count', 'pdfreport'));
    }

    public function showpdfreport($uniqid){
        
        $view = Pdfgeneratereport::where('pdf_uniqid', decrypt($uniqid))->firstOrFail();
       
        $path = Storage::disk('public')->path('generatedreports/'.$view->pdf_uniqid.".pdf");

        if($view == true){

            return response()->file($path, [
                'Content-Disposition' => 'inline; filename="'. $view->created_at .'"'
            ]);
        
        }else{

            return redirect()->back()->withSuccess('Access Denied!'); 
        }

    }
  
    public function insert_report(Request $request){
    	Reporting::create([
    		'report_user_id' => Auth::id(),
    		'report_start_date' => $request->input('start'),
    		'report_end_date' => $request->input('end')
    	]);
    	return redirect()->back()->withSuccess('Report Date Succesfully Added!'); 
    }

}
