<?php
namespace App\Http\Controllers;
use DB;
use PDF;
use Auth;
use DataTables;
use App\Models\Accreditorslist;
use App\Models\AccreditorsEvaluation;
use App\Models\AccreditorsEvalTable;
use App\Models\AccreditorsEvaluationCriteria;
use App\Models\Recdetails;
use Illuminate\Http\Request;

class AccreditorsEvaluationController extends Controller{
    
	public function evaluate_accreditor($id){
		$accreditor = Accreditorslist::find(decrypt($id));
		$criteria = AccreditorsEvaluationCriteria::all();
		$selectrec = Recdetails::join('applicationlevel', 'applicationlevel.recdetails_id', 'recdetails.id')->select(DB::raw("CONCAT(rec_institution, '-', rec_name) as allrecname"), 'recdetails.id')
		->whereIn('statuses_id', [4,19])->distinct('allrecname')
		->pluck('allrecname', 'id');

		return view('accreditors.accreditors-add-eval',compact('accreditor', 'criteria', 'selectrec'));
	}

	public function submitevaluation(Request $request, $accreditor_id){

		$submit = AccreditorsEvaluation::create([
			'accreditors_id' => $accreditor_id,
			'title_award' => $request->acc_title_eval,
			'for_rec' => $request->for_rec,
			'for_level' => $request->for_level,
			'evaluatedby_user_id' => auth::id()
		]);

		$pivot = [];

		foreach($request->acc_rate as $datum => $values){
			$value_index = 0; 
			foreach($values as $k => $value){
				$pivot[$value_index][$datum] = $value;
				$value_index++;
			}
		}
		$submit->accreditors_eval_tables()->attach($pivot);
		return redirect()->route('view-accreditors-evaluation', ['id' => $submit->id])->withSuccess('Evaluation Successfully Submitted!');
	}

	public function listofevaluation(){
		return view('accreditors.accreditors-view-eval');
	}

	public function show_ajax(){
    	$accreditorsevaluation = AccreditorsEvaluation::with(['users', 'accreditors'])->orderBy('id', 'desc');
    	return DataTables::of($accreditorsevaluation)->addColumn('action', function ($accreditorsevaluation){
			return '<a href="'.route('view-accreditors-evaluation', ['id' => $accreditorsevaluation->id]).'" class="fa fa-file-o"> View</a>';
		})->make(true);
    }

    public function viewacceval(AccreditorsEvaluation $id){
    	$acc_eval = AccreditorsEvalTable::whereIn('accreditors_evaluations_id', [$id->id])->get();
    	$selectrec = Recdetails::join('applicationlevel', 'applicationlevel.recdetails_id', 'recdetails.id')->select(DB::raw("CONCAT(rec_institution, '-', rec_name) as allrecname"), 'recdetails.id')
		->whereIn('statuses_id', [4,19])->distinct('allrecname')
		->pluck('allrecname', 'id');
    	return view('accreditors.accreditors-eval-view', compact('id', 'acc_eval', 'selectrec'));
    }

    public function update_eval(Request $request, AccreditorsEvaluation $id){
    	$id->update([
    		'title_award' => $request->acc_title_eval,
    		'for_rec' => $request->for_rec,
    		'for_level' => $request->for_level,
    	]);

    	$pivot = [];

    	foreach($request->acc_rate as $datum => $values){
			$value_index = 0; 
			foreach($values as $k => $value){
				$pivot[$value_index][$datum] = $value;
				$value_index++;
			}
		}

		$id->accreditors_eval_tables()->sync([]);
		$id->accreditors_eval_tables()->attach($pivot);

    	return redirect()->back()->withSuccess('Evaluation Successfully Updated!');
    }

    public function download_eval(AccreditorsEvaluation $id){
    	$acc_eval = AccreditorsEvalTable::whereIn('accreditors_evaluations_id', [$id->id])->get();
		$pdf = PDF::loadview('accreditors.accreditors-eval-template', compact('id', 'acc_eval'));
		$pdf->setPaper('A4', 'Portrait');
		return $pdf->download($id->accreditors->accreditors_name.'- '.$id->title_award.'-Evaluations.pdf');
    } 
	
}

// public function pivotData($data)
//     {
//         $pivot = array();

//         foreach ($data as $datum => $values) {
//             $value_index = 0;
//             foreach ($values as $k => $value) {
//                 $pivot[$value_index][$datum] = $value;
//                 $value_index++;
//             }
//         }
        
//         return $pivot;
//     }