<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\Level;
use Auth;
use PDF;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    //landingpage ito
    public function index(){
        return view('');
    }

    public function xxx(){
        // $pdf = PDF::loadview('phrebforms_template.phrebform1_1', compact('id', 'acc_eval'));
        // $pdf->setPaper('legal', 'Portrait');
        // return $pdf->download('phrebform1_1.pdf');
        return view('phrebforms_webpage.phrebform1_1-Application-for-accreditation');
    }

}