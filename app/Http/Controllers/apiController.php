<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Accreditorslist;

class apiController extends Controller
{	
	public $successStatus = 200;

    public function show_acc(){
    	$show_acc = Accreditorslist::all();
    	return response()->json($show_acc);
    }
}
