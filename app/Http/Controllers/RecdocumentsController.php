<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\Recdocuments;
use App\Models\Recdetails;
use App\Models\DocumentTypes;
use App\Models\User;
use App\Models\Role_user;
use App\Models\Recdocumentsremark;
use App\Models\Recassessment;
use App\Models\Recassessmentrec;
use App\Models\Recassessmentscsa;
use App\Models\Recactionplan;
use App\Models\Recactionplancsa;
use App\Models\Applicationlevel;
use App\Models\EmailTemplate;
use App\Models\Emaillogs;
use App\Models\Phrebforms;
use App\Models\Phrebform_assessment_rec;
use App\Models\Phrebforms_template;
use App\Models\Submissionstatuses;
use App\Models\Recdocuments_emailattachment;

use DB;
use Auth;
use Mail;
use File;
use Response;
use Storage;
use DataTables;
use Carbon\Carbon;

class RecdocumentsController extends Controller
{    
	// methods in viewing documents per recdetails id
  public function documents($recdetail){

   $decrypted_id = decrypt($recdetail);

   $recdetail = Recdetails::find($decrypted_id);

   $phrebforms = Phrebforms::with(['phrebforms_template' => function ($query) {
        $query->orderBy('pft_name');
   }])->whereIn('pf_recdetails_id', [$decrypted_id])->whereIn('pf_pfid', [1,4,5,6,7,8,13])->get();

   $phrebformAssessment = Phrebforms::whereIn('pf_pfid', [9,10,11,14,15])->orderBy('id', 'desc')->where('pf_recdetails_id', $decrypted_id);

   //if pf_status == 2: consolidated
   $consolidatedAssessment = Phrebforms::with('sendtoaccreditors', 'phrebformAssessmentStatus')->whereIn('pf_pfid', [9,10,11,14,15])->orderBy('phrebforms.id', 'desc')->where('pf_recdetails_id', $decrypted_id)->where('pf_status', 2);

   if(auth::user()->can('accreditors-access')){
   
      $phrebforms_ass = $phrebformAssessment->where('pf_user_id', auth::user()->id)->get();

      $phrebformAssessmentConsolidated = $consolidatedAssessment->whereHas('sendtoaccreditors', function($query){

        $query->whereIn('sendtoaccreditors.a_accreditors_user_id', [auth::user()->id]);
      
      })->whereHas('phrebformAssessmentStatus', function($query){

        $query->whereIn('pfas_status_status_id', [28,29,30,31]);
      
      })->get();
   
   }else if(auth::user()->can('secretariat-access')){

      $phrebforms_ass = $phrebformAssessment->where('pf_recdetails_id', $decrypted_id)->where('pf_user_id', '!=', auth::user()->id )->get();
      $phrebformAssessmentConsolidated = $consolidatedAssessment->get();

   }else if(auth::user()->can('csachair-access')){
    
      $phrebformAssessmentConsolidated = $consolidatedAssessment->get();
   }

   $finalreportAssessment_lvl3 = Phrebforms_template::whereIn('id', [14,15])->pluck('pft_name', 'id');

   $roles = Role_user::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->firstOrFail();
  
   if($roles->role_id == 3){

    $levels_to_query = $recdetail->applicationlevel->levels->id;

    $excluded_ids = Recdocuments::whereIn('recdocuments_recdetails_id', [$decrypted_id])->pluck('recdocuments_document_types_id')->toArray();
    
   $required_files = DocumentTypes::whereNotIn('id', $excluded_ids)
    ->whereIn('id', [11111, 11112, 11113, 11114, 11115, 11116, 11117, 11118, 11119, 11120, 11121])
    ->where('required', 1) // Filter only required rows
    ->where(function ($query) use ($levels_to_query) {
        $query->whereNull('level') // Include files without level restrictions
            ->orWhere(function ($subQuery) use ($levels_to_query) {
                $subQuery->orWhereJsonContains('level', (string)$levels_to_query);
            });
    })
    ->orderBy('document_types.ordering', 'asc')
    ->get(['id', 'document_types_name']);

    $select_doc_type = DocumentTypes::whereNotIn('id', $excluded_ids)
    ->where(function ($query) use ($levels_to_query) {
        $query->whereNull('level') // Include files without level restrictions
            ->orWhere(function ($subQuery) use ($levels_to_query) {
                $subQuery->orWhereJsonContains('level', (string)$levels_to_query);
            });
    })
    ->whereIn('id', [11111, 11112, 11113, 11114, 11115, 11116, 11117, 11118, 11119, 11120, 11121])
    // ->orWhere('user_id', auth()->user()->id) // Include user-specific files
    ->orderBy('document_types.ordering', 'asc')
    ->pluck('document_types_name', 'id');
    
      // $select_doc_type = DocumentTypes::with(['recdocuments'])->orderBy('document_types.ordering', 'asc')->whereIn('id', [1,2,3,4,5,6,7,8,9,10,11,12,13,26])->orwhere('user_id', auth::user()->id)->whereDoesntHave('recdocuments')->pluck('document_types.document_types_name','document_types.id');

   }else{

      $required_files = [];

      // $select_doc_type = DocumentTypes::whereIn('id', [11111,11112,11113,11114,11115,11116,11117,11118,11119,11120, 11121])->orwhere('created_at', '!=', null)->orderBy('document_types.id', 'asc')->where('user_id', auth::user()->id)->pluck('document_types.document_types_name','document_types.id');

       $select_doc_type = DocumentTypes::whereIn('recdetails_id', [$recdetail->id])->orderBy('document_types.id', 'asc')->pluck('document_types.document_types_name','document_types.id');
   }

    $select_doc_type_acc = DocumentTypes::where('id', '=', 14)->orderBy('document_types.id', 'asc')->pluck('document_types.document_types_name','document_types.id');
    
    $selectcsachair = User::where('role_users.role_id', '=', 5)->join('role_users', 'role_users.user_id', 'users.id')->pluck('users.name','users.id');

    $selectrec = User::where('role_users.role_id', '=', 3)->join('role_users', 'role_users.user_id', 'users.id')->pluck('users.name','users.id');

    $selectapce_doctypes = DocumentTypes::whereIn('id',[17,18,19,20])->pluck('document_types_name', 'id');

    $document_check = Recdocuments::select('recdocuments_document_types_id')->where('recdocuments.recdocuments_recdetails_id', $decrypted_id)->where('recdocuments_document_types_id_check', true)->where('recdocuments.recdocuments_document_types_id',1)->distinct()->get();
    
    //removed this join('applicationlevel', 'applicationlevel.recdetails_id', '=', 'recdetails.id')->
    $recdetail_get = Recdetails::where('recdetails.id', $decrypted_id)->get();

    $recdetails = Recdetails::join('applicationlevel', 'applicationlevel.recdetails_id', '=', 'recdetails.id')
    ->join('levels', 'applicationlevel.level_id', '=', 'levels.id')
    ->join('regions', 'applicationlevel.region_id', '=', 'regions.id')
    ->join('recchairs', 'applicationlevel.recdetails_id', '=', 'recchairs.recdetails_id')
    ->join('recdocuments', 'recdocuments.recdocuments_recdetails_id', '=', 'recdetails.id')
    ->join('document_types', 'document_types.id', '=', 'recdocuments.recdocuments_document_types_id')

    ->leftJoin('recdocumentsremarks', function($leftjoin) use ($decrypted_id){
        $leftjoin->on('recdocumentsremarks.remarks_recdocuments_types_id', 'recdocuments.recdocuments_document_types_id')->where('recdocumentsremarks.remarks_recdetails_id', '=', $decrypted_id);
    })
    
    ->select('applicationlevel.recdetails_id', 'recdetails.rec_name','recdetails.user_id','recdetails.id', 'document_types.document_types_name',  'recdocuments.recdocuments_document_types_id', 'recdocumentsremarks.remarks_recdetails_id', 'document_types.ordering', 'document_types.required',

      DB::raw(" GROUP_CONCAT(DISTINCT recdocuments.recdocuments_file ORDER BY recdocuments.id desc SEPARATOR '| ') as 'files', GROUP_CONCAT(DISTINCT recdocumentsremarks.remarks_message_recdocuments ORDER BY recdocumentsremarks.remarks_message_recdocuments DESC SEPARATOR '| ') as 'message', GROUP_CONCAT(DISTINCT recdocuments.recdocuments_document_types_id_check) as 'check', COUNT(recdocuments.recdocuments_file) as 'totalfiles' "))

    ->groupBy('applicationlevel.recdetails_id', 'recdetails.rec_name', 'recdetails.user_id', 'recdetails.id', 'document_types.document_types_name',  'recdocuments.recdocuments_document_types_id', 'recdocumentsremarks.remarks_recdetails_id')
    ->orderBy('document_types.ordering', 'asc')
    ->where('recdocuments_submission_statuses_id', null)
    ->where('applicationlevel.recdetails_id', $decrypted_id)
    ->get();

    $showassessment = Recassessment::join('users', 'users.id', 'recassessments.ra_user_id')->join('recdetails', 'recdetails.id', 'recassessments.ra_recdetails_id')->join('document_types', 'document_types.id', 'recassessments.ra_documents_types_id')->join('role_users', 'role_users.user_id', 'users.id')->join('roles', 'roles.id', 'role_users.role_id');

    $showassessment_phreb = $showassessment->select('recdetails.rec_name','role_users.*', 'roles.role_name','recassessments.*', 'users.name', 'document_types.document_types_name')->where('ra_recdetails_id', $decrypted_id)->orderBy('recassessments.id', 'desc')->get();

    $assessmentREC = Phrebform_assessment_rec::orderby('id', 'desc')->where('pfar_recdetails_id', $decrypted_id)->get();

    // submissionstatuses added 08/09/2020 by JM

    $submission = Submissionstatuses::whereIn('submissionstatuses_statuses_id', [3, 4, 9, 19, 21, 22])
         ->where('submissionstatuses_recdetails_id', $decrypted_id)
         ->orderBy('submissionstatuses.id', 'desc');
         

    if(Auth::user()->can('rec-access')){
         
         $submissionstatuses = $submission->get();

    }else if(Auth::user()->can('secretariat-access')){

         $submissionstatuses = $submission->where('submissionstatuses_rec_response', '!=', null)->get();
    }
  

    foreach($recdetail_get as $r_g){

    if($roles->role_id == 2 or $roles->role_id == 4 or $r_g->user_id == auth::id()){
    
    $actionplan = Recactionplan::where('ap_recdetails_id', $decrypted_id)->orderByRaw('ap_document_types')->whereIn('ap_document_types',[15,16])->get();  
      
    $showactionplan_eval = Recactionplan::join('recdetails', 'recdetails.id', 'recactionplans.ap_recdetails_id')->join('recchairs', 'recchairs.recdetails_id', 'recdetails.id')->join('users', 'users.id', 'recactionplans.ap_sender_id')->join('role_users', 'role_users.user_id', 'users.id')->join('roles', 'roles.id', 'role_users.role_id')->select('recactionplans.*','users.name', 'recdetails.rec_name', 'role_users.role_id', 'roles.role_name')->where('recactionplans.ap_recdetails_id', $decrypted_id)->where('recactionplans.ap_status', '!=', null)->orderByRaw('recactionplans.ap_document_types desc')->get();

    foreach($showactionplan_eval as $sap){
      // $                                                                                                                           tory = Recactionplancsa::find($sap->id);
    }

    }elseif($roles->role_id == 5){

      $actionplan = Recactionplan::where('ap_recdetails_id', $decrypted_id)->orderByRaw('ap_document_types')->whereIn('ap_document_types',[15,16])->get();  

      $showactionplan_eval = Recactionplancsa::where('ap_csa_recdetails_id', $decrypted_id)->where('ap_csa_csachair_id', auth::id())->distinct()->orderby('id', 'desc')->get();

      $showactionplan_eval_sent = Recactionplan::where('ap_recdetails_id', $decrypted_id)->where('ap_sender_id', auth::user()->id)->get();

    }else{
      return redirect()->back()->withSuccess('Access Restriction Failed!');
    }
      return view('recdocuments.documents', compact('recdetail_get', 'decrypted_id', 'recdetail', 'recdetails', 'select_doc_type', 'required_files', 'select_doc_type_acc', 'selectapce_doctypes', 'document_check', 'actionplan', 'showassessment_phreb', 'selectcsachair', 'selectrec', 'showactionplan_eval', 'roles', 'phrebforms', 'assessmentREC', 'finalreportAssessment_lvl3', 'submissionstatuses'));
    }

  }

	//methods in storing the file or docs of recdocuments tbl
	public function storerecdocuments(Request $request, Recdetails $recdetail){

     try{
          DB::beginTransaction();
              
              $validator = \Validator::make($request->all(),[
               'recdocuments_file.*' => 'required|mimes:jpg,jpeg,png,pdf,doc,docx,zip',
              ]);

              if ($validator->fails()) {
                    return redirect()->back()->withSuccess('Upload Failed. Please use the following document formats only (.pdf, .docx, .doc, .png, .jpg)')->withErrors($validator)->withInput();
              
              }elseif($request->recdocuments_document_types_id == ""){
                    return redirect()->back()->withSuccess('No folder name detected, upload failed. Please choose in the selectbox.')->withErrors($validator)->withInput();
              }

                if($request->hasFile('recdocuments_file')){


                  if(Auth::user()->can('secretariat-access')){

                      $recdocuments_user_id = $recdetail->users->id;

                   }else{

                      $recdocuments_user_id = Auth::user()->id;

                   }
 
                   $recdetail_Id = decrypt($request->input('recdetails_id'));

                       if(strlen($request->recdocuments_document_types_id) >= 50 AND $recdetail->applicationlevel->recdetails_submission_status == 0){

                          $checkDocumentType = DocumentTypes::find(decrypt($request->recdocuments_document_types_id));
                          $recdocuments_name = $checkDocumentType->id;

                       }else{


                          if($recdetail->recdocument_typesID()->exists()){

                            foreach($recdetail->recdocument_typesID as $recdocumentsName){
                              $folderName = $recdocumentsName->document_types_name;
                            }

                            if($folderName == $request->recdocuments_document_types_id){

                              return redirect()->back()->withSuccess('Upload failed. Folder name already exists.');

                            }

                          }
                            
                          $createFolder = DocumentTypes::create(['document_types_name' => $request->recdocuments_document_types_id, 'user_id' => auth::user()->id, 'recdetails_id' => $recdetail->id, 'created_at' => Carbon::now()]);

                          $recdocuments_name = $createFolder->id;

                          

                       }

                  $recdocuments_dateuploaded = Carbon::now();

                  foreach($request->recdocuments_file as $file){

                    $filename = $file->getClientOriginalName();
                    $unique_filename = uniqid();
                    $file->storeAs('public/documents/'.$recdocuments_user_id.'/'.$recdetail_Id, $unique_filename.'-'.$filename);

                    $fileModel = new Recdocuments;
                    $fileModel->recdocuments_user_id = $recdocuments_user_id;
                    $fileModel->recdocuments_recdetails_id = Auth::user()->can('secretariat-access')  ? $recdetail->id : $recdetail_Id;
                    $fileModel->recdocuments_document_types_id = $recdocuments_name;
                    $fileModel->recdocuments_file = $filename;
                    $fileModel->recdocuments_file_generated = $unique_filename;
                    
                    $fileModel->recdocuments_submittophreb = Auth::user()->can('secretariat-access') ? Carbon::now() : null;

                    $fileModel->recdocuments_fileextension = $file->getClientOriginalExtension();
                    $fileModel->recdocuments_filesize = $file->getSize();
                    $fileModel->rec_dateuploaded = $recdocuments_dateuploaded;

                    $request->submissionstatuses_id == true ? $fileModel->recdocuments_submission_statuses_id = $request->submissionstatuses_id : $fileModel->recdocuments_submission_statuses_id = null; 

                    $fileModel->save();
                  } 
              }

          DB::commit();
      }catch(\Exception $e){

          DB::rollback();

          // dd($e);

          return redirect()->back()->withSuccess('Upload Failed. The first word of folder name must be a letter (eg. One (1), Two (Two), Three (3))');
      }
      return redirect()->back()->withSuccess('Requirements Successfully Uploaded!');
  }

  // additional docs inside folder
  public function adddocuments(Request $request, $id, $recdocID){
    
     try{
          DB::beginTransaction();

          $validator = \Validator::make($request->all(),[
               'recdocuments_file.*' => 'required|mimes:jpg,jpeg,png,pdf,doc,docx,zip',
          ]);

          if ($validator->fails()) {
                return redirect()->back()->withSuccess('Upload Failed. Please use the following format for the documents (.pdf, .docx, .doc) only')->withErrors($validator)->withInput();
          }

          if($request->hasFile('recdocuments_file')){

                foreach($request->recdocuments_file as $file){
                  $filename = $file->getClientOriginalName();
                  $unique_filename = uniqid();
                  $file->storeAs('public/documents/'.Auth::user()->id.'/'.decrypt($id), $unique_filename.'-'.$filename);

                  $fileModel = new Recdocuments;
                  $fileModel->recdocuments_user_id = Auth::user()->id;
                  $fileModel->recdocuments_recdetails_id = decrypt($id);
                  $fileModel->recdocuments_document_types_id = decrypt($recdocID);
                  $fileModel->recdocuments_file = $filename;            
                  $fileModel->recdocuments_file_generated = $unique_filename;
                  $fileModel->recdocuments_fileextension = $file->getClientOriginalExtension();
                  $fileModel->recdocuments_filesize = $file->getSize();
                  $fileModel->rec_dateuploaded = Carbon::now();

                  $request->submissionstatuses_id == true ? $fileModel->recdocuments_submission_statuses_id = $request->submissionstatuses_id : $fileModel->recdocuments_submission_statuses_id = null; 

                  $fileModel->save();
                } 
              }

          DB::commit();

        }catch(\Exception $e){


          DB::rollback();
          return redirect()->back()->withSuccess('Upload Failed. Error Detected!');
      }

    return redirect()->back()->withSuccess('Successfully Uploaded!');
  }

  //methods in viewing recdocuments per row
  public function viewrecdocuments($id, $recdocuments_document_types_id){

    // $size = File::size(public_path('\storage\documents\2\2\5c46c7b33dfea-PHREB ACCREDITATION FORMS 1.1 DRMC.pdf'));

    $units = [' B', ' KB', ' MB'];
    
    // $power = $size > 0 ? floor(log($size, 1024)) : 0;
    // dd(round($size / pow(1024, $power)).$units[$power]);
// ../../../storage/documents/{{$recdocument->users->id}}/{{$recdocument->recdocuments_recdetails_id}}/{{$recdocument->recdocuments_file_generated}}-{{$recdocument->recdocuments_file}}

    $roles=Role::where('role_users.id', '=', Auth::user()->id)->join('role_users', 'role_users.role_id', '=', 'roles.id')->first();
    
    $decrypted_id = decrypt($id);
    
    //cardheader only
    $recdocuments = Recdocuments::with(['documenttypes'])->where('recdocuments_document_types_id', $recdocuments_document_types_id)->whereIn('recdocuments_recdetails_id', [$decrypted_id])->first();

    $checkifsubmitted = Recdocuments::leftJoin('submissionstatuses', 'submissionstatuses.id', 'recdocuments_submission_statuses_id')->where('recdocuments_document_types_id', $recdocuments_document_types_id)->whereIn('recdocuments_recdetails_id', [$decrypted_id])->where('submissionstatuses_rec_response', null)->first();

    //for data-raw-row
     $recdocuments_files = Recdocuments::select('*','recdocuments.id')
     ->join('users', 'users.id', '=', 'recdocuments.recdocuments_user_id')
     ->where('recdocuments.recdocuments_document_types_id',$recdocuments_document_types_id)
     ->where('recdocuments.recdocuments_recdetails_id', '=', $decrypted_id)
     ->orderBy('recdocuments.id', 'desc')
     ->get();

     $recdocuments_remarks = Recdocumentsremark::join('recdocuments', function($join)use($decrypted_id, $recdocuments_document_types_id){ 
      $join->on('recdocuments.recdocuments_document_types_id', 'recdocumentsremarks.remarks_recdocuments_types_id')->where('recdocumentsremarks.remarks_recdetails_id', $decrypted_id)->where('recdocumentsremarks.remarks_recdocuments_types_id', $recdocuments_document_types_id);
      })->select('recdocumentsremarks.remarks_message_recdocuments', 'recdocumentsremarks.created_at', 'users.name')->distinct()->orderBy('recdocumentsremarks.id', 'desc')->join('users', 'users.id', 'recdocumentsremarks.remarks_user_id')->get();

     if($recdocuments_files->isEmpty()){

          return redirect()->route('documents', ['id' => $id])->withSuccess('Data Successfully Removed!');
     }

    // foreach($recdocuments_files as $recdocuments_file){
      if($recdocuments->users->id == auth::user()->id OR Auth::user()->can('accreditors_and_csachair_secretariat')){

          return view('recdocuments.view-recdocuments', compact('recdocuments', 'recdocuments_files', 'recdocuments_document_types_id', 'id', 'recdocuments_remarks', 'units', 'checkifsubmitted'));

      }else{

        return redirect()->back()->withSuccess('File Access Failed!');
      }
    // }

  }

  //methods for changing completechecklist = 1
  public function check_complete(Recdetails $recdetail, Request $request){
    $checked_doc_type = $request->input('recdocuments_document_types_id');

    Recdocuments::where('recdocuments.recdocuments_recdetails_id', $recdetail->id)->update(['recdocuments_document_types_id_check' => null]);

    if($checked_doc_type == null){
      return redirect()->back()->withSuccess('Successfully Checked');
    }else{
    $check = Recdocuments::whereIn('recdocuments.recdocuments_document_types_id', $checked_doc_type)->where('recdocuments.recdocuments_recdetails_id', $recdetail->id)->update(['recdocuments_document_types_id_check' => true]);
    }
    return redirect()->back()->withSuccess('Successfully Check-saved');
  }

  //remove recdocument if not yet submitted
  public function removerecdocument($id){

    $files = Recdocuments::where('recdocuments.id', $id)->delete();

    return redirect()->back()->withSuccess('Successfully Deleted');
  
  }

  public function resubmit_req($recdetail){

   try{

    DB::beginTransaction();

    $recdetails = Recdetails::find($recdetail);

    $documentTypes = Recdocuments::where('recdocuments_recdetails_id', $recdetail)->whereNull('recdocuments_submittophreb')->pluck('recdocuments_document_types_id') ->unique()->values()->toArray();

    //update rec response per row in submissionstatuses
    $getLatestStatus = Submissionstatuses::whereIn('submissionstatuses.submissionstatuses_recdetails_id', [$recdetail])->orderby('id', 'desc')->first()->update(['submissionstatuses_rec_response' => Carbon::now()]);

    // if not yet accredited execute this
    if($recdetails->applicationlevel->statuses_id != 4){

      $update = $recdetails->applicationlevel->update(['recdetails_resubmitreq' => true, 'statuses_id' => 1]);
  
    }
    
    $get_details1 = $recdetails;

    $add_sub_statuses = Submissionstatuses::create([
      'submissionstatuses_recdetails_id' => $recdetails->id,
      'submissionstatuses_user_id' => auth::user()->id,
      'submissionstatuses_statuses_id' => $recdetails->applicationlevel->statuses_id == 4 ? 20 : 1,
      'submissionstatuses_datecreated' => Carbon::now(),
      'submissionstatuses_emailmessage' => $recdetails->applicationlevel->statuses_id == 4 ? 'Submitted compliances' : 'Submitted additional requirements', 
      'submissionstatuses_emailsend' => Carbon::now(),
      'submissionstatuses_doctypes_id' => $documentTypes,
    ]); 

    $emailTemplate = EmailTemplate::find($recdetails->applicationlevel->statuses_id == 4 ? 41 : 38);

    $body = str_replace(['$rec_institution','$rec_name', '$level_name'], [$recdetails->rec_institution, $recdetails->rec_name, $recdetails->applicationlevel->levels->level_name], $emailTemplate->email_body);

    Mail::send('email.email_template', compact('body', 'get_details1'), function($message) use ($recdetails, $body, $emailTemplate){

      $message->to('ethics.secretariat@pchrd.dost.gov.ph');
      $message->subject($recdetails->rec_institution.'-'.$recdetails->rec_name.': '.$emailTemplate->email_subject);
      $message->cc([$recdetails->rec_email, $recdetails->rec_contactemail, $recdetails->recchairs->rec_chairemail]);
      // $message->bcc('jrcambonga@pchrd.dost.gov.ph');
          // foreach($get_rec_status as $get_rec_statuses){
          //   $message->cc([$recdetail->rec_email, $recdetail->rec_contactemail,$get_rec_statuses->rec_chairemail]);

      // $sender = auth::id();
      // $recdetails_id = $recdetails->applicationlevel->recdetails_id;
      // $from = auth::user()->email;
      // $to = 'ethics.secretariat@pchrd.dost.gov.ph';
      // $cc = $recdetails->rec_email.', '.$recdetails->rec_contactemail.', '.$recdetails->recchairs->rec_chairemail;
      // $bcc = 'jrcambonga@pchrd.dost.gov.ph';
      // $subject = $recdetails->rec_institution.' - '.$recdetails->rec_name.' Re-submission of Requirements';
      // $body = $body;
      // emaillogs($sender, $recdetails_id, $from, $to, $cc, $bcc, $subject, $body);   

    });

    $updateDocuments = Recdocuments::whereIn('recdocuments.recdocuments_recdetails_id', [$recdetail])->update(['recdocuments_submittophreb' => Carbon::now()]);

    DB::commit();

  }catch(\Exception $e){

    DB::rollback();
    return redirect()->back()->withSuccess('Submit Failed. Error Detected!');
  }

  return redirect()->back()->withSuccess('Requirements successfully submitted to PHREB Secretariat. Please wait for the response. Thank you!');
}

  public function createfolder(Request $request, $id){

    $create = DocumentTypes::create(['document_types_name' => $request->createfolder, 'user_id' => auth::user()->id, 'recdetails_id' => $id, 'created_at' => Carbon::now()]);
     return redirect()->back()->withSuccess('Document Folder/Name Added!');
  
  }

  public function downloadZipRecDoc(Request $request){

     try{
          DB::beginTransaction();

           $recdocument = Recdocuments::whereIn('id', $request->download_check)->get();
    
           foreach($recdocument as $rec){
              $files[] = glob(public_path('storage/documents/'.$rec->recdocuments_user_id.'/'.$rec->recdocuments_recdetails_id.'/'.$rec->recdocuments_file_generated.'-'.$rec->recdocuments_file));
            }

            $path = 'zipfiles/'.$rec->recdetails->rec_institution.''.$rec->recdetails->rec_name.'-'.$rec->documenttypes->document_types_name.'.zip';

            \Zipper::make(public_path($path))->add($files)->close();

            DB::commit();

            return response()->download(public_path($path));
      }
          catch(\Exception $e){

            DB::rollback();
            dd($e);
          
          return redirect()->back()->withSuccess('Download Failed. Please check atleast one file.');
      }

  }


  // step 3 for rec
  public function step3($id){

    $recdetails = Recdetails::find(decrypt($id));

    $foldernames = Recdocuments::join('document_types', 'document_types.id', 'recdocuments.recdocuments_document_types_id')
    ->select('recdocuments_document_types_id', 'document_types_name', 'required')
    ->groupBy('document_types_name')
    ->where('recdocuments_recdetails_id', decrypt($id))->orderBy('required', 'asc')->get();

    // $requirements = DocumentTypes::with('recdocuments')->whereHas('recdocuments', function($query) use($recdetails){
    //   $query->where('recdocuments_recdetails_id', $recdetails->id);
    // })->whereIn('required', [1])->get();

    // dd($requirements);

    return view('recdocuments.step3', compact('recdetails', 'foldernames'));
  }


  //download documents using route per file
  public function downloadfile($id){

    $view = Recdocuments::find(decrypt($id));

    $path = Storage::disk('public')->path('documents/'.$view->users->id.'/'.$view->recdocuments_recdetails_id.'/'.$view->recdocuments_file_generated.'-'.$view->recdocuments_file);

    if($view->users->id == Auth::user()->id or Auth::user()->can('accreditors_and_csachair_secretariat')){

        return response()->file($path, [
          'Content-Disposition' => 'inline; filename="'. $view->recdocuments_file .'"'
        ]);
    
    }else{

        return redirect()->back()->withSuccess('Download Failed and Access Denied!');
    }
   
  }


  //download documents of emailattachmment using route per file - files from phreb secretariat 
  public function emailattachmment($id){

    $attachment = Recdocuments_emailattachment::find(decrypt($id));

    $path = Storage::disk('public')->path('documentsEmailattachments/'.$attachment->re_user_id.'/'.$attachment->re_recdetails_id.'/'.$attachment->re_file_generated.'-'.$attachment->re_file_name);
  

    if($attachment->recdetails->users->id == Auth::user()->id or Auth::user()->can('accreditors_and_csachair_secretariat')){

        return response()->file($path, [
          'Content-Disposition' => 'inline; filename="'. $attachment->re_file_name .'"'
        ]);
    
    }else{

        return redirect()->back()->withSuccess('Download failed and access denied!');
    }


  }


  // rename folder
  public function renamefolder(Request $request, $id){
    
    $folder = DocumentTypes::find(decrypt($id));
    $folder->update(['document_types_name' => $request->renamefolder]);

    return redirect()->back()->withSuccess('Successfully renamed the folder!');

  }  

}