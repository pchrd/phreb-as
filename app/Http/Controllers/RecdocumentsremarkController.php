<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Recdetails;
use App\Models\Recdocumentsremark;
use Auth;
use DB;

class RecdocumentsremarkController extends Controller
{
    //add remarks from secretary-in-charge
    public function add_remarks(Request $data){
    	$add_recdetails = Recdocumentsremark::create([
			'remarks_user_id' => Auth::user()->id,
			'remarks_recdetails_id' => $data['remarks_recdetails_id'],
			'remarks_recdocuments_types_id' =>$data['remarks_recdocuments_types_id'],
			'remarks_message_recdocuments' => $data['remarks_message_recdocuments'],
		]);
		return redirect()->back()->withSuccess('Remarks/Comments Successfully Sent!');
    }
}
