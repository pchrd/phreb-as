<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Level;  
use App\Models\Recdetails;
use App\Models\Recdocuments;
use App\Models\Role;                           
use App\Models\Region;
use App\Models\Phrebform;
use App\Models\Status;
use App\Models\User;
use App\Models\Submissionstatuses;
use App\Models\Sendtoaccreditor;
use App\Models\Recassessment;
use App\Models\Recassessmentscsa;
use App\Models\Recassessmentrec;
use App\Models\Emaillogs;
use App\Models\EmailTemplate;
use Carbon\Carbon;
use DataTables;
use Auth; 
use DB;
use Mail;
use Gate;

class SendtoaccreditorController extends Controller
{	
	//sendtoaccreditor
    public function sendtoaccreditor(Request $request){
    	$data = $request->only('a_accreditors_user_id', 'a_recdetails_id');

        // $recdetails = Recdetails::join('applicationlevel', 'applicationlevel.recdetails_id', 'recdetails.id')->join('regions', 'regions.id', 'applicationlevel.region_id')->join('levels', 'levels.id', 'applicationlevel.level_id')->join('statuses', 'statuses.id', 'recdetails.rec_apptype_id')->where('recdetails.id', $data['a_recdetails_id'])->get();

        $recdetails = Recdetails::find($data['a_recdetails_id']);

        if($recdetails->applicationlevel->statuses_id != 2){

             return redirect()->back()->withSuccess('Sending Failed. Please make sure that the status of this REC is Completed. Thank you.');

        }else{

        $accreditors_email = User::where('users.id', $data['a_accreditors_user_id'])->first();

        $emailTemplate = EmailTemplate::find(6);
        // foreach($recdetails as $get_details1){
        $body = str_replace(['$name','$rec_name', '$rec_institution', '$level_name','$status_name','$secname'], [$accreditors_email->name, $recdetails->rec_name, $recdetails->rec_institution, $recdetails->applicationlevel->levels->level_name, $recdetails->applicationlevel->status->status_name, auth::user()->name], $emailTemplate->email_body);
        // }

        $get_details1 = $recdetails;

        $sendtoacc = Sendtoaccreditor::create(['a_recdetails_id' => $data['a_recdetails_id'], 'a_sender_user_id' => auth::user()->id, 'a_accreditors_user_id' => $data['a_accreditors_user_id'], 'a_datesubmitted' => Carbon::now(), 'a_dateofexpiry' => Carbon::now()->addDays(30)
        ]);


        Mail::send('email.email_for_accreditors_confirm_decline', compact('recdetails', 'sendtoacc', 'accreditors_email', 'body', 'get_details1'), function($message) use ($accreditors_email, $recdetails, $body){    
                $message->to($accreditors_email->email);
                $message->subject($recdetails->rec_name.' Invitation to be Accreditor');

                $sender = auth::id();
                $recdetails_id = $recdetails->applicationlevel->recdetails_id;
                $from = auth::user()->email;
                $to = $accreditors_email->email;
                $cc = '';
                $bcc = '';
                $subject = $recdetails->rec_name.' Invitation to be Accreditor';
                $body = $body;
                emaillogs($sender, $recdetails_id, $from, $to, $cc, $bcc, $subject, $body); 
        
            // $emaillogs = Emaillogs::create([
            //     'e_sender_user_id' => auth::id(),
            //     'e_recdetails_id' => $get_details1->recdetails_id,
            //     'e_from' => auth::user()->email,
            //     'e_to' => $accreditors_email->email,
            //     'e_cc' => '',
            //     'e_bcc' => '',
            //     'e_subject' => $get_details1->rec_name.' Invitation to be Accreditor',
            //     'e_body' => $body,
            //     'e_created_at' => Carbon::now()
            // ]);

        });
    		return redirect()->back()->withSuccess('Succesfully Submitted to Accreditor '.$accreditors_email->name); 
        }
    }

    public function removeacc($sendacc_tbl_id){
        Sendtoaccreditor::where('sendtoaccreditors.sendacc_tbl_id', $sendacc_tbl_id)->delete();
        return redirect()->back()->withSuccess('Accreditor/Coordinator has been removed!'); 
    }

    //send assessment form from accreditors
    public function sendassessmentform(Request $request){
        $this->validate($request,['recdocuments_file.*' => 'required|file|max:5000|mimes:pdf,docx,doc',]);
        $recdetail_Id = $request->input('recdetails_id');
        
        if($request->hasFile('recdocuments_file')){
            foreach($request->recdocuments_file as $file){
                $filename = $file->getClientOriginalName();
                $unique_filename = uniqid();
                $extension = $file->getClientOriginalExtension();
                $file->storeAs('public/assessmentforms/'.Auth::user()->id.'/'.$recdetail_Id, $unique_filename.'.'.$extension);

                $fileModel = new Recassessment;
                $fileModel->ra_user_id = auth::user()->id;
                $fileModel->ra_recdetails_id = $recdetail_Id;
                $fileModel->ra_documents_types_id = $request->input('recdocuments_document_types_id');
                $fileModel->ra_recdocuments_file = $filename;
                $fileModel->ra_recdocuments_file_generated = $unique_filename;
                $fileModel->ra_recdocuments_file_extension = $extension;
                $fileModel->ra_dateuploaded = Carbon::today();
                $fileModel->save();
            }
            $data = array(
                'recdocuments_file' => $request->recdocuments_file,
            );
            $file = $request->file('recdocuments_file');

            $getuserrole = Role::join('role_users', 'role_users.role_id', 'roles.id')->where('role_users.user_id', auth::id())->first();

            // $recdetails = Recdetails::where('recdetails.id', $recdetail_Id)->join('applicationlevel','applicationlevel.recdetails_id', 'recdetails.id')->join('statuses', 'statuses.id', 'recdetails.rec_apptype_id')->get();

            $recdetails = Recdetails::find($recdetail_Id);

            $accreditorsemail = Sendtoaccreditor::leftjoin('users', 'users.id', 'sendtoaccreditors.a_accreditors_user_id')->where('sendtoaccreditors.a_recdetails_id', $recdetail_Id)->get();

            $acc_name = Auth::user()->name;

            $emailTemplate = EmailTemplate::find(7);

            // foreach($recdetails as $get_details1){
            $body = str_replace(['$role_name','$acc_name','$rec_name','$level_id','$status_name'], [$getuserrole->role_name, $acc_name, $recdetails->rec_name, $recdetails->applicationlevel->level_id, $recdetails->applicationlevel->status->status_name], $emailTemplate->email_body);
            // }

            $get_details1 = $recdetails;

            Mail::send('email.email_template', compact('data','recdetails','body', 'get_details1'), function($message) use ($data, $file, $accreditorsemail, $recdetails, $body){
                    foreach($accreditorsemail as $ae){
                    $message->cc($ae->email);
                    $message->subject("Accreditor's Assessment Form for ".$recdetails->rec_name);
                    $message->from(auth::user()->email);

                    $sender = auth::id();
                    $recdetails_id = $recdetails->applicationlevel->recdetails_id;
                    $from = auth::user()->email;
                    $to = '';
                    $cc = $ae->email;
                    $bcc = '';
                    $subject = 'Accreditors Assessment Form for '.$recdetails->rec_name;
                    $body = $body;
                    emaillogs($sender, $recdetails_id, $from, $to, $cc, $bcc, $subject, $body); 

                    // $emaillogs = Emaillogs::create([
                    //     'e_sender_user_id' => auth::id(),
                    //     'e_recdetails_id' => $get_details1->recdetails_id,
                    //     'e_from' => auth::user()->email,
                    //     'e_to' => '',
                    //     'e_cc' => $ae->email,
                    //     'e_bcc' => '',
                    //     'e_subject' => 'Accreditors Assessment Form for '.$get_details1->rec_name,
                    //     'e_body' => $body,
                    //     'e_created_at' => Carbon::now()
                    // ]);
                    }  

                    foreach($file as $files){
                        $message->attach($files->getRealPath(), array(
                            'as' => $files->getClientOriginalName(),
                            'mime' => $files->getMimeType())
                        );
                    }   
             });
             return redirect()->back()->withSuccess('Assessment Successfully Submitted!');
        }
    }

    // sendtocsachair
    public function sendtocsachair(Recdetails $recdetail, Request $request){
        $recassessment_id = $request->input('recassessment_id');
        $ra_csachair_id = $request->input('ra_csachair_id');

        // $get_recassessment = Recassessment::join('users', 'users.id', 'recassessments.ra_user_id')->join('recdetails', 'recdetails.id', 'recassessments.ra_recdetails_id')->join('document_types', 'document_types.id', 'recassessments.ra_documents_types_id')->join('applicationlevel', 'applicationlevel.recdetails_id', 'recdetails.id')->join('statuses', 'statuses.id', 'recdetails.rec_apptype_id')->where('recassessments.id', $recassessment_id)->get();

        $get_recassessment = Recassessment::find($recassessment_id);

        $get_csachairemail = User::where('users.id', $ra_csachair_id)->first();

        $get_accreditors = Recassessment::join('users', 'users.id', 'recassessments.ra_user_id')->where('recassessments.id', $recassessment_id)->get();

        $secname = auth::user()->name;
        // foreach($get_recassessment as $get_details1){

            $get_recassessent_file = "/app/public/assessmentforms/$get_recassessment->ra_user_id/$get_recassessment->ra_recdetails_id/$get_recassessment->ra_recdocuments_file_generated.$get_recassessment->ra_recdocuments_file_extension";
            
            $emailTemplate = EmailTemplate::find(8);
             
            foreach($get_accreditors as $ga){
                $body = str_replace(['$acc_name','$rec_name','$status_name','$level_id','$secname'], [$ga->name.', ', $get_recassessment->recdetails->rec_name, $get_recassessment->applicationlevel->status->status_name, $get_recassessment->applicationlevel->level_id, $secname], $emailTemplate->email_body);
            }

            $get_details1 = $get_recassessment; 
            Mail::send('email.email_template', compact('body', 'get_details1'), function($message) use ($get_recassessment, $get_recassessent_file, $get_csachairemail, $get_details1, $body){  
                    $message->to($get_csachairemail->email); 
                    $message->subject($get_details1->rec_name. 'Evaluation of Accreditors on Level '.$get_details1->applicationlevel->level_id.' Application');
                    $message->attach(storage_path($get_recassessent_file), ['as' => $get_details1->ra_recdocuments_file]);

                    $sender = auth::id();
                    $recdetails_id = $get_details1->applicationlevel->recdetails_id;
                    $from = 'Forwarded to CSA CHAIR by '.auth::user()->email;
                    $to = $get_csachairemail->email;
                    $cc = '';
                    $bcc = '';
                    $subject = $get_details1->recdetails->rec_name. 'Evaluation of Accreditors on Level '.$get_details1->applicationlevel->level_id.' Accreditation Application';
                    $body = $body;
                    emaillogs($sender, $recdetails_id, $from, $to, $cc, $bcc, $subject, $body);

                    // $emaillogs = Emaillogs::create([
                    //     'e_sender_user_id' => auth::id(),
                    //     'e_recdetails_id' => $get_details1->recdetails_id,
                    //     'e_from' => 'Forwarded to CSA CHAIR by '.auth::user()->email,
                    //     'e_to' => $get_csachairemail->email,
                    //     'e_cc' => '',
                    //     'e_bcc' => '',
                    //     'e_subject' =>$get_details1->rec_name. 'Evaluation of Accreditors on Level '.$get_details1->level_id.' Accreditation Application',
                    //     'e_body' => $body,
                    //     'e_created_at' => Carbon::now()
                    // ]);
             });

            Recassessmentscsa::create([
                'ra_csa_ra_id' => $recassessment_id,
                'ra_csa_sender_id' => auth::id(),
                'ra_csa_csachair_id' => $ra_csachair_id,
                'ra_csa_recdetails_id' => $get_recassessment->applicationlevel->recdetails_id,
                'ra_csa_datecreated_at' => Carbon::now(),
                'ra_csa_dateexpiry' => Carbon::today()->addDays(15)
            ]);
         // }
        return redirect()->back()->withSuccess('Assessment Successfully Send to CSA Chair '.$get_csachairemail->name);
    
    }

    //sendtorec
    public function sendtorec(Request $request){
        $data = $request->only('ra_rec_ra_id','ra_rec_recdetails_id','ra_rec_csachair_id');
        $date = Carbon::now()->format('F d, Y');

        $actionplandeadline = Carbon::today()->addDays(30);
        $compliancedeadline = Carbon::today()->addMonths(6);

        // $recdetails = Recdetails::join('recassessments','recassessments.ra_recdetails_id', 'recdetails.id')->join('applicationlevel', 'applicationlevel.recdetails_id', 'recdetails.id')->where('recassessments.id', $data['ra_rec_ra_id'])->get();

        // $recdetails = Recassessment::where('id', $data['ra_rec_ra_id'])->get();
        $recdetails = Recassessment::find($data['ra_rec_ra_id']);

        $apce = DB::table('applicationlevel')->where('recdetails_id', $data['ra_rec_recdetails_id'])->update(['rec_apce' => true]);

        $get_chair_id = User::join('esigs', 'esig_user_id', 'users.id')->where('users.id', $data['ra_rec_csachair_id'])->first();

        $get_acc_file = Recassessment::join('users','users.id', 'recassessments.ra_user_id')->where('recassessments.id', $data['ra_rec_ra_id'])->first();

        $get_recassessent_file = "/app/public/assessmentforms/$get_acc_file->ra_user_id/$get_acc_file->ra_recdetails_id/$get_acc_file->ra_recdocuments_file_generated.pdf";
        $get_actionplan = "/app/public/templates/PHREB Form No. 4.3 (Action Plan Template).docx";
        $getesig = "storage/esigs/$get_chair_id->esig_user_id/$get_chair_id->esig_file_generated.jpg";

        // foreach($recdetails as $recdetail){
            Mail::send('email.email_sendrecassessment', compact('recdetails','date','get_chair_id', 'actionplandeadline', 'getesig'), function($message) use ($get_recassessent_file, $recdetails, $get_actionplan, $get_acc_file, $get_chair_id){
                $message->to($recdetails->recdetails->recchairs->rec_chairemail);
                $message->cc([$recdetails->recdetails->rec_email, $recdetails->recdetails->rec_contactemail, $recdetails->recdetails->users->email]); 
                $message->subject('PHREB Assessment Letter for Level '.$recdetails->recdetails->applicationlevel->level_id); 
                $message->attach(storage_path($get_recassessent_file), ['as' => $get_acc_file->ra_recdocuments_file]);
                $message->attach(storage_path($get_actionplan));

                $sender = auth::id();
                $recdetails_id = $recdetails->recdetails->applicationlevel->recdetails_id;
                $from = 'Forwarded to REC by '.auth::user()->email;
                $to = $recdetails->recdetails->recchairs->rec_chairemail;
                $cc = $recdetails->recdetails->rec_email.', '.$recdetails->recdetails->rec_contactemail.', '.$recdetails->recdetails->users->email;
                $bcc = '';
                $subject = 'PHREB Assessment Letter for Level '.$recdetails->recdetails->applicationlevel->level_id;
                $body = '';
                emaillogs($sender, $recdetails_id, $from, $to, $cc, $bcc, $subject, $body);
                
                // $emaillogs = Emaillogs::create([
                //         'e_sender_user_id' => auth::id(),
                //         'e_recdetails_id' => $recdetail->recdetails_id,
                //         'e_from' => 'Forwarded to REC by '.auth::user()->email,
                //         'e_to' => $recdetail->recchairs->rec_chairemail,
                //         'e_cc' => $recdetail->rec_email.', '.$recdetail->rec_contactemail.', '.$recdetail->email,
                //         'e_bcc' => '',
                //         'e_subject' =>'PHREB Assessment Letter for Level '.$recdetail->level_id,
                //         'e_body' => '',
                //         'e_created_at' => Carbon::now()
                // ]);
            });
        // }
        Recassessment::where('id', $data['ra_rec_ra_id'])->update(['ra_check' => true]);
        Recassessmentrec::create(['ra_rec_ra_id' => $data['ra_rec_ra_id'], 'ra_rec_recdetails_id' => $data['ra_rec_recdetails_id'], 'ra_rec_sender_id' => auth::user()->id, 'ra_rec_csachair_id' => $data['ra_rec_csachair_id'], 'ra_rec_datecreated_at' => Carbon::now(), 'ra_rec_ap_dateexpiry' => $actionplandeadline, 'ra_rec_ce_dateexpiry' => $compliancedeadline]);

        return redirect()->back()->withSuccess('Assessment Successfully Send to REC!');
    }

    public function confirm(Sendtoaccreditor $id){

        if($id->deleted_at == null){
        $id->update(['sendaccreditors_confirm_decline' => Carbon::now()]);
            Mail::raw($id->users->name.'['.$id->users->role_users->roles->role_name.']'.' has accepted the Invitation to be Accreditor/Reviewer for '.$id->recdetails->rec_institution.'-'.$id->recdetails->rec_name.'|'.$id->recdetails->applicationlevel->levels->level_name, function($message) use ($id){
                $message->subject('Invitation to be Accreditor');
                $message->to($id->users->email)->bcc('jrcambonga@pchrd.dost.gov.ph');
            });
        return redirect('show-recdetails')->withSuccess('You have Successfully Confirm the Review Invitation for REC '.$id->recdetails->rec_institution.'-'.$id->recdetails->rec_name);
        }else{
        return redirect('show-recdetails')->withSuccess('You are no longer to Accept Review Invitation for REC: '.$id->recdetails->rec_institution.'-'.$id->recdetails->rec_name.', that had been sent to you because you have already [DECLINED] it on the date of '.$id->deleted_at);
        }
    }

    public function decline(Sendtoaccreditor $id){
        if($id->sendaccreditors_confirm_decline == null){
        $id->update(['deleted_at' => Carbon::now()]);
            Mail::raw($id->users->name.'['.$id->users->role_users->roles->role_name.']'.' has declined the Invitation to be Accreditor/Reviewer for '.$id->recdetails->rec_institution.'-'.$id->recdetails->rec_name.'|'.$id->recdetails->applicationlevel->levels->level_name, function($message) use ($id){
                $message->subject('Invitation to be Accreditor');
                $message->to($id->users->email)->bcc('jrcambonga@pchrd.dost.gov.ph');
            });
        return redirect('show-recdetails')->withSuccess('You have Successfully [DECLINED] the Review Invitation for REC: '.$id->recdetails->rec_institution.'-'.$id->recdetails->rec_name.' Thank you!');
        }else{
        return redirect('show-recdetails')->withSuccess('You are no longer to Decline Review Invitation for REC: '.$id->recdetails->rec_institution.'-'.$id->recdetails->rec_name.', that had been sent to you because you have already [ACCEPT] it on the date of '.$id->sendaccreditors_confirm_decline);
        }
    }

}