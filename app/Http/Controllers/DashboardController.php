<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Recdetails;
use App\Models\EmailTemplate;
use App\Models\Chatmessage;
use App\Models\SendOrderpayment;
use App\Models\Reporting;
use App\Models\Region;
use App\Models\Applicationlevel;
use App\Models\Trainingeventstrainees;
use App\Models\Trainingeventsorientation;
use App\Models\Trainingeventsmanagement;
use App\Models\Submissionstatuses;
use Carbon\Carbon;
use Mail;
use DB;
use Auth;

class DashboardController extends Controller
{
    //dashboard
    public function dashboard(){
    	return view('dashboard', compact('user'));
    }

    //selecting all users verified and recdeatils and count then display in dashboard
    public function count_all(){

        $users = User::where('id', auth::id())->first();

        $output = []; 

    	$count_users_verified = DB::table('users')->select(DB::raw('count(*) as total, verified'))->where('verified', '=', true)->groupBy('verified')->get();

    	$allrecdetails = Recdetails::join('applicationlevel', 'applicationlevel.recdetails_id', '=', 'recdetails.id');

        $count_recdetails_submitted_lib = $allrecdetails->get();

        $count_recdetails_submitted = Auth::user()->role_users->user_assigned_region_id == null ? $count_recdetails_submitted_lib->count() : $count_recdetails_submitted_lib->where('applicationlevel.region_id', Auth::user()->role_users->user_assigned_region_id)->count();

        $countrecdetails_perrec = $allrecdetails->whereIn('user_id', [auth::user()->id])->count();

        // para lumabas ang duplication notice when creating entry
        $duplicationnotice = $allrecdetails->whereIn('user_id', [auth::user()->id])->whereIn('statuses_id', [1,2,3,19])->count();

        //count accredited level 1 2 3
        $count_lvl1 = $this->countLevels(1);
        $count_lvl2 = $this->countLevels(2);
        $count_lvl3 = $this->countLevels(3);

        $count_pending      = $this->countStatuses(1);
        $countongoing       = $this->countStatuses(19);
        $count_expired      = $this->countStatuses(17);
        $count_complete     = $this->countStatuses(2);
        $count_incomplete   = $this->countStatuses(3);
        $count_resubmission = $this->countStatuses(9);

        // $countmessages = Chatmessage::where('chatmessages.reciever_user_id', auth::user()->id)->count();

        $or = SendOrderpayment::count();

        $r = Reporting::latest()->first();
        $reports = Applicationlevel::where('statuses_id', 4)->whereBetween('date_accreditation',[$r->report_start_date, $r->report_end_date])->count();

        $phrebdatabase = Recdetails::where('rec_existing_record', '=', 1)->count();

        $regions = Region::select(DB::raw("count(*) as total, region_name"))->join('applicationlevel', 'applicationlevel.region_id', 'regions.id')->where('applicationlevel.statuses_id', 4)->groupBy('region_name', 'regions.id', 'statuses_id')->orderBy('regions.id', 'asc')->get();

         $count_rec_for_acc = Recdetails::join('sendtoaccreditors', 'sendtoaccreditors.a_recdetails_id', 'recdetails.id')->where('a_accreditors_user_id', auth::id())->where('sendaccreditors_confirm_decline', '!=', null)->count();

         $counttrainees = Trainingeventstrainees::count();
         $counttrainingevents = Trainingeventsorientation::count();
         $traineesmanagement = Trainingeventsmanagement::count();

         $accreditedrecsCompliances = Recdetails::with('applicationlevel', 'submissionsstatuses')->whereHas('applicationlevel', function($query){
        
           $query->where('statuses_id', '4');
         
         })->whereHas('submissionsstatuses', function($query1){
           
            $query1->latest('submissionstatuses_datecreated')->where('submissionstatuses_statuses_id', '=', 20);

         })->get();

         
      
    	return view('dashboard', compact('users', 'count_users_verified', 'count_pending', 'count_recdetails_submitted', 'count_lvl1', 'count_lvl2', 'count_lvl3', 'count_pending', 'or', 'reports', 'phrebdatabase', 'regions', 'countongoing', 'countrecdetails_perrec', 'count_rec_for_acc', 'counttrainees', 'counttrainingevents', 'traineesmanagement', 'count_expired', 'count_complete', 'count_incomplete', 'count_resubmission', 'duplicationnotice', 'accreditedrecsCompliances'));
    }

    //email reminder every end of the year//
    public function emailreminder_cron(){
        emailTemplate1();
    }

    //email reminder for deadline of accreditation
    public function dl_acc_cron(){
        emailTemplate();
    }

    public function countStatuses($number){
        
        $countStatuses_lib = DB::table('applicationlevel')->where('applicationlevel.statuses_id',$number)->where('applicationlevel.recdetails_submission_status', true);

        $countStatuses = Auth::user()->role_users->user_assigned_region_id == null ? $countStatuses_lib->count() : $countStatuses_lib->where('applicationlevel.region_id', Auth::user()->role_users->user_assigned_region_id)->count();

        return $countStatuses;
    }

    public function countLevels($number){

        $countLevels_lib = DB::table('applicationlevel')->where('applicationlevel.level_id',$number)->where('applicationlevel.statuses_id', 4);

        $countLevels = Auth::user()->role_users->user_assigned_region_id == null ? $countLevels_lib->count() : $countLevels_lib->where('applicationlevel.region_id', Auth::user()->role_users->user_assigned_region_id)->count();

        return $countLevels;

    }
}