<?php
namespace App\Http\Controllers;
use App\Models\SendOrderpayment;
use App\Models\Recdetails;
use App\Models\EmailTemplate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use auth;
use Carbon\Carbon;
use DataTables;
use Mail;

class OrderpaymentController extends Controller{
    
    public function show(){
    	return view('OrderPayment.orderofpayment');
    }

    public function show_ajax(){
    	$or = SendOrderpayment::with('recdetails','users')->where('or_serialno', '!=', null);
    	return DataTables::of($or)->addColumn('actions', function ($or){
					return '<a href="storage/ors/'.$or->or_file_generated.'.pdf" title="Download PDF" target="_blank" class="btn btn-xs btn-default"><i class="fa fa-download"></i></a>';
				})->rawColumns(['actions'])->make(true);
    }

     public function show_ajax_or_rec(){
    	$or = SendOrderpayment::with('recdetails','users')->where('or_serialno', null);
    	return DataTables::of($or)->addColumn('actions', function ($or){
					return '<a href="storage/orreceipts/'.$or->or_file_generated.'-'.$or->or_file.'" title="Download PDF" target="_blank" class="btn btn-xs btn-default"><i class="fa fa-download"></i></a>';
				})->rawColumns(['actions'])->make(true);
    }

    public function sendorreceipt(Recdetails $recdetails_id, Request $request){
    	$data = $request->only('sendorreceipt');
    	$get_details1 = $recdetails_id->find($recdetails_id->id);
    	
    	$emailTemplate = EmailTemplate::find(18);
        $body = str_replace(['$date', '$rec_name', '$rec_institution', '$rec_address', '$rec_chairname', '$level_id', '$secname'], [Carbon::now()->format('F j, Y'), $get_details1->rec_name, $get_details1->rec_institution, $get_details1->rec_address, $get_details1->recchairs->rec_chairname, $get_details1->applicationlevel->level_id, auth::user()->name], $emailTemplate->email_body); 

    	if($request->hasFile('sendorreceipt')){
        foreach($data['sendorreceipt'] as $file){
          $filename = $file->getClientOriginalName();
          $unique_filename = uniqid();
          $file->storeAs('public/orreceipts/', $unique_filename.'-'.$filename);
          $fileModel = new SendOrderpayment;
          $fileModel->or_sender_id = auth::user()->id;
          $fileModel->or_recdetails_id = $recdetails_id->id;
          $fileModel->or_serialno = null;
          $fileModel->or_file = $filename;
          $fileModel->or_file_generated = $unique_filename;
          $fileModel->or_datecreated_at = Carbon::now();
          $fileModel->save();
        }
          
          Mail::send('email.email_template', compact('body', 'get_details1'), function($message) use ($data, $get_details1, $emailTemplate, $file, $filename){
	        	$message->to($get_details1->recchairs->rec_chairemail);
	        	$message->cc([$get_details1->rec_email, $get_details1->rec_contactemail, $get_details1->users->email]);
	        	$message->subject($get_details1->rec_name.'-'.$emailTemplate->email_name);
	        	foreach($data['sendorreceipt'] as $files){
	        	$message->attach($files, ['as' => $files->getClientOriginalName(), 'mime' => $file->getMimeType()]);
				}
			});
         
        return redirect()->back()->withSuccess('Successfully Sent Order Payment and Receipt!');
      }
    }
}