<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Reclist;
use App\Models\Recdetails;
use App\Models\Trainingeventsorientation;
use App\Models\Trainingeventsmanagement;
use Auth;
use PDF;
use DB;

class TrainingeventsorientationController extends Controller{
    
    public function reclist(){
    	$reclist = Reclist::all()->pluck('reclist_name', 'id');
    	return $reclist;
    }

    public function institution(){
    	$institution = Recdetails::distinct('rec_institution')->pluck('rec_institution');
    	return $institution;
    }

	public function show(){
		$reclist = $this->reclist();
		$institution = $this->institution();
		
		$trainingevents = DB::table('trainingeventsorientations')->orderBy('trainingeventsorientations.id', 'desc')->leftJoin('trainingeventsmanagements', 'trainingeventsmanagements.te_orientation_id', 'trainingeventsorientations.id')
		->leftJoin('trainingeventstrainees', 'trainingeventstrainees.id', 'trainingeventsmanagements.te_trainee_id')
		->leftjoin('users', 'users.id', 'trainingeventsorientations.user_id')
		->join('reclists', 'reclists.id', 'trainingeventsorientations.rec_name')
		->select(DB::raw("COUNT(trainingeventsmanagements.id) as counts, trainingeventsorientations.id, training_type_name, institution_name, training_start_date, training_end_date, name, trainingeventsorientations.rec_name, reclists.reclist_name"))
		->groupBy('trainingeventsorientations.id', 'training_type_name', 'institution_name', 'training_start_date', 'training_end_date', 'name', 'trainingeventsorientations.rec_name', 'reclists.reclist_name')
		->get();

		// $trainingevents = Trainingeventsorientation::orderBy('id', 'desc')->paginate(10);

		//$count = DB::table('applicationlevel')->whereBetween('date_accreditation',[$reporting->report_start_date, $reporting->report_end_date])->join('levels', 'levels.id', 'level_id')->where('statuses_id', 4)->select(DB::raw("COUNT(*) as counts, level_name"))->groupBy('level_name')->get();

		return view('trainees.show-training-events-or-orientation', compact('reclist', 'institution', 'trainingevents'));
	}

	public function add(Request $request){
		$trainingeventsorientation  = Trainingeventsorientation::create([
			'training_type_name' => $request->training_type_name, 
			'institution_name' => $request->institution_name,
			'rec_name' => $request->rec_name,
			'training_start_date' => $request->training_start_date,
			'training_end_date' => $request->training_end_date,
			'user_id' => auth::id(),
		]);
		return redirect()->back()->withSuccess('Training Events/Orientation Added!');
	}

	public function update(Request $request, Trainingeventsorientation $id){
		$id->update([
			'training_type_name' => $request->training_type_name,
			'institution_name' => $request->institution_name,
			'rec_name' => $request->rec_name,
			'training_start_date' => $request->training_start_date,
			'training_end_date' => $request->training_end_date
		]);
		return redirect()->back()->withSuccess('Training Events/Orientation ID no. '.$id->id.' Updated!');
	}

	public function view_attendees(Trainingeventsorientation $id){
    	$attendee = Trainingeventsmanagement::where('te_orientation_id', $id->id)->get();
    	return view('trainees.view-events-attendees', compact('id', 'attendee'));
    }

    public function printattendee(Trainingeventsorientation $id){
    	$attendee = Trainingeventsmanagement::where('te_orientation_id', $id->id)->get();
    	$pdf = PDF::loadview('trainees.trainee-attendance-sheet-pdf', compact('attendee', 'id'));
    	$pdf->setPaper('legal', 'landscape');
    	return $pdf->stream($id->training_type_name.'-'.$id->institution_name.'.pdf');
    }
}