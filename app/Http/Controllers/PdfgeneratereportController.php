<?php

namespace App\Http\Controllers;

use App\Models\Pdfgeneratereport;
use Illuminate\Http\Request;

class PdfgeneratereportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pdfgeneratereport  $pdfgeneratereport
     * @return \Illuminate\Http\Response
     */
    public function show(Pdfgeneratereport $pdfgeneratereport)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pdfgeneratereport  $pdfgeneratereport
     * @return \Illuminate\Http\Response
     */
    public function edit(Pdfgeneratereport $pdfgeneratereport)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pdfgeneratereport  $pdfgeneratereport
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pdfgeneratereport $pdfgeneratereport)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pdfgeneratereport  $pdfgeneratereport
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pdfgeneratereport $pdfgeneratereport)
    {
        //
    }
}
