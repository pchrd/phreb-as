<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Level;  
use App\Models\Recdetails;
use App\Models\Role;    
use App\Models\Role_user;                                                  
use App\Models\Region;
use App\Models\Phrebforms;
use App\Models\Status;
use App\Models\User;
use App\Models\Submissionstatuses;
use App\Models\Sendtoaccreditor;
use App\Models\Recassessment;
use App\Models\Recassessmentrec;
use App\Models\Recassessmentscsa;
use App\Models\Recactionplan;
use App\Models\Recactionplancsa;
use App\Models\Recdocuments;
use App\Models\Emaillogs;
use App\Models\EmailTemplate;
use App\Models\SendOrderpayment;
use App\Models\DocumentTypes;
use App\Models\Applicationlevel;
use App\Models\AnnualReport;
use App\Models\AnnualReportTitle;
use App\Models\Reclist;
use App\Models\Existing_recs;
use App\Models\Phrebforms_template;
use App\Models\Statement;
use App\Models\Recdocuments_emailattachment;
use Carbon\Carbon;
use DataTables;
use File;
use Auth; 
use DB;
use Mail;
use Gate;
use PDF;

class RecdetailsController extends Controller{
	//method for view the logged user in dashboard
	public function dashboard(){
		$loggeduser_id = Auth::user()->id;
		$roles=Role::where('role_users.id', '=', $loggeduser_id)
		->join('users', 'users.id', '=', 'roles.id')
		->join('role_users', 'role_users.role_id', '=', 'roles.id')
		->findOrFail($loggeduser_id);
		return view('recdetails.show-recdetails', compact('roles'))->middleware('auth');
	}

    //to get the levels and regions and put and select box applyaccred
	public function selectlevelandregions(){

		$roles = Role_user::where('user_id', auth::user()->id)->first();
		$selectlevels = Level::orderBy('level_name')->where('levels.id', '<=', 3)->pluck('level_name','id');
		$selectregions = Region::orderBy('region_name')->pluck('region_name','id');
		$statements = Statement::whereIn('id', [1,2,3,4])->get();
		
		if($roles->role_id == 2){

			$selectapptype=Status::orderBy('id', 'desc')->whereIn('statuses.id', [12,13,14])->pluck('status_name', 'id');
		}else{

			$selectapptype=Status::orderBy('id')->whereIn('statuses.id', [12,13,14])->pluck('status_name', 'id');
		}

		$selectrecname=Reclist::pluck('reclist_name');

		return view('recdetails.applyaccred', compact('selectlevels', 'selectregions', 'selectapptype','selectrecname', 'statements'));
	}

	public function applyaccrednotice(){

		$recdetail = Recdetails::with('applicationlevel')->whereHas('applicationlevel', function($query){
			$query->whereIn('statuses_id', [1,2,3,19]);			
		})
		->whereIn('user_id', [Auth::user()->id])
		->orderBy('id', 'desc')
		->get();

		return view('recdetails.applyaccrednotice', compact('recdetail'));

	}

    //in order to submit general details
	public function add_rec_details(Request $data, Recdetails $recdetail){

		 try{

   			DB::beginTransaction();
				
				foreach($data->statement as $id => $s){
					$consent = $s;
				}

				if(count($consent) != 4){
					return redirect()->back()->withSuccess('Save Failed. REC must check all terms and agreement before  uploading details on the system. Thank you.');
				}
				
				$add_recdetails = Recdetails::create([
					'user_id' => Auth::user()->id,
					'rec_name' => $data['rec_name'],
					'rec_apptype_id' => $data['selectapptype'],
					'rec_existing_record' => $data['yesorno'],
					'rec_institution' => ucwords(strtolower($data['rec_institution'])),
					'rec_address' => ucwords(strtolower($data['rec_address'])),
					'rec_email' => $data['rec_email'],
					'rec_telno' => $data['rec_telno'],
					'rec_faxno' => $data['rec_faxno'],
					'rec_contactperson' => ucwords(strtolower($data['rec_contactperson'])),
					'rec_contactposition' => ucwords(strtolower($data['rec_contactposition'])),
					'rec_contactmobileno' => $data['rec_contactmobileno'],
					'rec_contactemail' => $data['rec_contactemail'],
					'rec_dateestablished' => $data['rec_dateestablished'],
				]);

				$add_recdetails->levels()->attach(
					['level_id' => $data['selectlevel']],
					['region_id' => $data['selectregions']]
				);

				$add_recdetails->recchairs()->create($data->only(['rec_chairname', 'rec_chairemail', 'rec_chairmobile']));
				$add_recdetails->applicationlevel_reminders()->create();

				// add statement policy or consent table
				$pivot = [];
	
				foreach($data->statement['id'] as $datum => $values){

					$add_recdetails->recdetails_statements()->create(['statement_statement_id' => $values]);

				}
				
				//getting the phreforms template in db
				$pf1 = getPhrebformTempID(1); //1.1
				$pf7 = getPhrebformTempID(7); //1.7
				$pf8 = getPhrebformTempID(8); //1.1a
				$pf13 = getPhrebformTempID(13); //1.3

				if(Auth::user()->role_users->role_id == 2){

					if($data['yesorno'] == 1){

						$add_recdetails->applicationlevel->update([
							'statuses_id' => 4	,
							'date_completed' => $data['date_completed'],
							'date_accreditation' => $data['date_accreditation'],
							'date_accreditation_expiry' => $data['date_accreditation_expiry'],
							'accreditation_number' => $data['accreditation_number'],
							'recdetails_submission_status' => true,
							'recdetails_resubmitreq' => false,
							'recdetails_lock_fields' => true,
						]);

						$add_recdetails->existing_recs()->create();

					}else if($data['yesorno'] == ""){

						$add_recdetails->update(['rec_existing_record' => "1"]);

						$add_recdetails->applicationlevel->update([
							'recdetails_submission_status' => true,
							'recdetails_resubmitreq' => false,
							'recdetails_lock_fields' => true,
						]);

						$add_recdetails->existing_recs()->create();

					}

				}else{

				$find = ['$rec_name','$rec_institution','$rec_address','$rec_email','$rec_telno','$rec_faxno','$rec_contactperson','$rec_contactposition','$rec_contactmobileno','$rec_contactemail', '$rec_dateestablished', '$rec_chairemail', '$rec_chairname', '$rec_chairmobile'];

				$replace = [$add_recdetails->rec_name, $add_recdetails->rec_institution, $add_recdetails->rec_address, $add_recdetails->rec_email, $add_recdetails->rec_telno, $add_recdetails->rec_faxno, $add_recdetails->rec_contactperson, $add_recdetails->rec_contactposition, $add_recdetails->rec_contactmobileno, $add_recdetails->rec_contactemail, \Carbon\Carbon::createFromFormat('Y-m-d', $add_recdetails->rec_dateestablished)->format('F j, Y'), $add_recdetails->recchairs->rec_chairemail, $add_recdetails->recchairs->rec_chairname, $add_recdetails->recchairs->rec_chairmobile];
				$body1 = str_replace($find, $replace, $pf1->pft_body);

				if($data['selectlevel'] == 1){
					$pf = getPhrebformTempID(4); //1.4

					//for 1.1a - Application for Accreditation of Specialty Clinics
					$add_recdetails->phrebform()->create(['pf_recdetails_id' => $add_recdetails->id, 'pf_pfid' => $pf8->id, 'pf_user_id' => Auth::user()->id,  'pf_body1' => str_replace($find, $replace, $pf8->pft_body)]);
					
				}elseif($data['selectlevel'] == 2){
					$pf = getPhrebformTempID(5); //1.5

					//for 1.1a - Application for Accreditation of Specialty Clinics
					$add_recdetails->phrebform()->create(['pf_recdetails_id' => $add_recdetails->id, 'pf_pfid' => $pf8->id, 'pf_user_id' => Auth::user()->id,  'pf_body1' => str_replace($find, $replace, $pf8->pft_body)]);

				}elseif($data['selectlevel'] == 3){
					$pf = getPhrebformTempID(6); //1.6
				}

				$body = str_replace($find, $replace, $pf->pft_body);

				//for 1.1 - Application for Accreditation form
				$add_recdetails->phrebform()->create(['pf_recdetails_id' => $add_recdetails->id, 'pf_pfid' => $pf1->id, 'pf_user_id' => Auth::user()->id,  'pf_body1' => $body1]);

				//for 1.3 Protocol Summary 
				$add_recdetails->phrebform()->create(['pf_recdetails_id' => $add_recdetails->id, 'pf_pfid' => $pf13->id, 'pf_user_id' => Auth::user()->id,  'pf_body1' => str_replace($find, $replace, $pf13->pft_body)]);

				//for 1.4 | 1.5. | 1.6 - assessment form
				$add_recdetails->phrebform()->create(['pf_recdetails_id' => $add_recdetails->id, 'pf_pfid' => $pf->id, 'pf_user_id' => Auth::user()->id,  'pf_body1' => $body]);

				//for 1.7 - ICC-PSs Protocol Summ Form
				$add_recdetails->phrebform()->create(['pf_recdetails_id' => $add_recdetails->id, 'pf_pfid' => $pf7->id, 'pf_user_id' => Auth::user()->id,  'pf_body1' => str_replace($find, $replace, $pf7->pft_body)]);

				}

				DB::commit();
		
		}catch(\Exception $e){

    		DB::rollback();
    		return redirect()->back()->withSuccess('Save Failed');
		}

		return redirect()->route('view-recdetails', ['id' => encrypt($add_recdetails->id)])->withSuccess('Data Successfully Saved!');
		
	}

	//get data using ajax datatables
	public function show_rec_details_ajax(Recdetails $recdetail){
		
		$logged_user_id = Auth::user()->id; 
		$logged_user_name = Auth::user()->name;

		$role = Role_user::where('user_id', '=', auth::id())
		->first();

		//foreach($roles as $role){

			//admin or sec
			if($role->role_id == 2 OR $role->role_id == 1){

				// a admin or sec view
				$recdetails_ajax_lib = Recdetails::where('applicationlevel.recdetails_submission_status', true)->join('applicationlevel', 'applicationlevel.recdetails_id', '=', 'recdetails.id')->join('levels', 'applicationlevel.level_id', '=', 'levels.id')->join('regions', 'applicationlevel.region_id', '=', 'regions.id')->join('role_users', 'recdetails.user_id', '=', 'role_users.user_id')->join('statuses', 'applicationlevel.statuses_id', '=', 'statuses.id')->with(['status']);

				$recdetails_ajax = Auth::user()->role_users->user_assigned_region_id == null ? $recdetails_ajax_lib : $recdetails_ajax_lib->where('applicationlevel.region_id', Auth::user()->role_users->user_assigned_region_id);

				return DataTables::of($recdetails_ajax)->editColumn('date_accreditationFORMAT', function ($recdetails_ajax){

					return $recdetails_ajax->date_accreditation != null ? \Carbon\Carbon::createFromFormat('Y-m-d',$recdetails_ajax->date_accreditation)->format('F j, Y') : '---';
            	})->editColumn('date_accreditation_expiryFORMAT', function ($recdetails_ajax) {

            		return $recdetails_ajax->date_accreditation_expiry != null ? \Carbon\Carbon::createFromFormat('Y-m-d',$recdetails_ajax->date_accreditation_expiry)->format('F j, Y') : '---';
            	})

            	->addColumn('rec_change_of_acc', function ($recdetails_ajax){

					return '<a href="edit-record/'.encrypt($recdetails_ajax->recdetails_id).'" title="View Notes" class=""><i class="fa fa-sticky-note-o  text text-danger "> </i>'.str_limit($recdetails_ajax->existingrec_remarks, 50).'</a>';
				})->addColumn('action', function ($recdetail) use ($logged_user_name){

					return '<a href="view-recdetails/'.encrypt($recdetail->recdetails_id).' " title="View and Edit" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i></a>';
				})->rawColumns(['rec_change_of_acc', 'action'])
				// ->setRowAttr(['style' => function($recdetails_ajax){
				// 	return $recdetails_ajax->date_accreditation_expiry <= Carbon::today() ? 'background-color: #ffdab9;' : ($recdetails_ajax->date_accreditation_expiry <= Carbon::today()->addMonths(1) ? 'background-color: #eedd82;	' : '');
				// }])
				->make(true);

			//REC
			}elseif($role->role_id == 3){
				//a view for per REC user
				$recdetails_ajax = Recdetails::where('recdetails.user_id', '=', $logged_user_id)->join('applicationlevel', 'applicationlevel.recdetails_id', '=', 'recdetails.id')->join('levels', 'applicationlevel.level_id', '=', 'levels.id')->join('regions', 'applicationlevel.region_id', '=', 'regions.id')->join('role_users', 'recdetails.user_id', '=', 'role_users.user_id')->join('statuses', 'applicationlevel.statuses_id', '=', 'statuses.id')->with(['status']);

				return DataTables::of($recdetails_ajax)->editColumn('date_accreditationFORMAT', function ($recdetails_ajax){

					return $recdetails_ajax->date_accreditation != null ? \Carbon\Carbon::createFromFormat('Y-m-d',$recdetails_ajax->date_accreditation)->format('F j, Y') : '---';
            	})->editColumn('date_accreditation_expiryFORMAT', function ($recdetails_ajax) {

            		return $recdetails_ajax->date_accreditation_expiry != null ? \Carbon\Carbon::createFromFormat('Y-m-d',$recdetails_ajax->date_accreditation_expiry)->format('F j, Y') : '---';
            	
            	})->addColumn('action', function ($recdetail){
				
					return '<a href="'.route('view-recdetails', ['id' => encrypt($recdetail->recdetails_id)]).'" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i> View</a>';
				
				})->editColumn('status_name', function($recdetails_ajax){

            		return $recdetails_ajax->recdetails_submission_status == 0 ? 'Not yet submitted' : 
            		$recdetails_ajax->status_name;

            	})->make(true);


			//accreditors
			}elseif($role->role_id == 4){
					//a view for per Accreditors
				$recdetails_ajax = Recdetails::where('sendtoaccreditors.a_accreditors_user_id', $logged_user_id)->where('sendtoaccreditors.sendaccreditors_confirm_decline', '!=', null)->join('applicationlevel', 'applicationlevel.recdetails_id', '=', 'recdetails.id')->join('levels', 'applicationlevel.level_id', '=', 'levels.id')->join('regions', 'applicationlevel.region_id', '=', 'regions.id')->join('role_users', 'recdetails.user_id', '=', 'role_users.user_id')->join('statuses', 'applicationlevel.statuses_id', '=', 'statuses.id')->join('sendtoaccreditors', 'sendtoaccreditors.a_recdetails_id', 'recdetails.id')->with(['status']);

				return DataTables::of($recdetails_ajax)->editColumn('date_accreditationFORMAT', function ($recdetails_ajax){

					return $recdetails_ajax->date_accreditation != null ? \Carbon\Carbon::createFromFormat('Y-m-d',$recdetails_ajax->date_accreditation)->format('F j, Y') : '---';
            	})->editColumn('date_accreditation_expiryFORMAT', function ($recdetails_ajax) {

            		return $recdetails_ajax->date_accreditation_expiry != null ? \Carbon\Carbon::createFromFormat('Y-m-d',$recdetails_ajax->date_accreditation_expiry)->format('F j, Y') : '---';
            	})->addColumn('action', function ($recdetail){
					return '<a href="'.route('view-recdetails', ['id' => encrypt($recdetail->recdetails_id)]).'" class="btn btn-xs btn-success"><i class="fa fa-eye"></i> View</a>';

				})->make(true);

			//csachair
			}elseif($role->role_id == 5){

				$recdetails_ajax = Recdetails::select(['recdetails_id','recdetails.id', 'statuses.status_name', 'regions.region_name', 'levels.level_name', 'recdetails.rec_name', 'recdetails.rec_institution', 'recdetails.rec_email', 'applicationlevel.created_at'])->where('recassessmentscsas.ra_csa_csachair_id', auth::id())->join('applicationlevel', 'applicationlevel.recdetails_id', '=', 'recdetails.id')->join('levels', 'applicationlevel.level_id', '=', 'levels.id')->join('regions', 'applicationlevel.region_id', '=', 'regions.id')->join('role_users', 'recdetails.user_id', '=', 'role_users.user_id')->join('statuses', 'applicationlevel.statuses_id', '=', 'statuses.id')->join('recassessments', 'recassessments.ra_recdetails_id', 'recdetails.id')->join('recassessmentscsas', 'recassessmentscsas.ra_csa_ra_id', 'recassessments.id')->distinct()->with(['status']);
				
				return DataTables::of($recdetails_ajax)->addColumn('action', function ($recdetail) use ($logged_user_name){
					return '<a href=" view-recdetails/'.encrypt($recdetail->recdetails_id).' " title="View and Edit" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i></a> <a href="#" title="Print to PDF" class="btn btn-xs btn-warning"><i class="fa fa-print"></i></a>';

				})->make(true);
			}
			return view('recdetails.show-recdetails', compact('recdetails_ajax', 'roles'));
		// }
	}

    // show recdetails in table/users dashboard
	public function show_rec_details(Request $request){
		$output = [];
		
		//Status Pending Req == 1
		$recdetail_count_1 = DB::table('applicationlevel')->select(DB::raw('count(*) as total, statuses_id'))->where('statuses_id', '=', 1)->where('recdetails_submission_status', true)->groupBy('statuses_id')->get();	

		$selectlevels=Level::orderBy('level_description')->where('levels.id', '<=', 3)->pluck('level_description','id');
		$selectregions=Region::orderBy('id')->pluck('region_name','id');
		$status = Status::whereIn('id', [4,2,3,1,5,17,19])->pluck('status_name','id');

		return view('recdetails.show-recdetails', compact('request', 'selectlevels', 'selectregions', 'status'));
	}

	public function view_rec_details($recdetail){
		$decrypted_url_id = decrypt($recdetail);
		$currentdate = Carbon::now();
		$selectarps = DocumentTypes::whereIn('id', [3,4])->pluck('document_types_name', 'id');

		$recdetails = Recdetails::find($decrypted_url_id);

		$getapptype = Recdetails::join('statuses', 'statuses.id', 'recdetails.rec_apptype_id')->where('recdetails.id', '=', $decrypted_url_id)->first();

		$selectaccreditation = DocumentTypes::whereIn('id', [21,22,23,24,25])->pluck('document_types_name', 'id');

		$roles=Role::where('role_users.user_id', Auth::user()->id)->join('role_users', 'role_users.role_id', '=', 'roles.id')->first();
		$selectlevels=Level::orderBy('level_name')->where('levels.id', '<=', 3)->pluck('level_name','id');
		$selectregions=Region::orderBy('id')->pluck('region_name','id');

		$selectstatuses=Status::orderBy('id', 'asc')->whereIn('id', $recdetails->applicationlevel->statuses_id == 4 ? [17,21,22,23,26,25,28,29] : [2,3,4,9,5,11,19,24,28,29])->pluck('status_name','id');
		
		$selectapptype=Status::orderBy('id')->whereIn('statuses.id', [12,13,14])->pluck('status_name', 'id');
		$selectapptype1=Status::orderBy('id')->whereIn('statuses.id', [12,13,14])->pluck('status_name', 'id');
		$selectawarding=DocumentTypes::orderBy('id')->whereIn('id', [21,22,23,24,25])->pluck('document_types_name', 'id');

		$recdetails_user_ass = Recdetails::join('applicationlevel', 'applicationlevel.recdetails_id', '=', 'recdetails.id')->join('levels', 'applicationlevel.level_id', '=', 'levels.id')->where('applicationlevel.recdetails_id',$decrypted_url_id)->get();

		$get_sendaccreditors = Recdetails::join('applicationlevel', 'applicationlevel.recdetails_id', 'recdetails.id')->join('sendtoaccreditors', 'sendtoaccreditors.a_recdetails_id', 'recdetails.id')->join('users', 'users.id', 'sendtoaccreditors.a_accreditors_user_id')->where('recdetails.id', $decrypted_url_id)->where('sendtoaccreditors.deleted_at', '=', null)->distinct()->get();
		
		$get_recassessment = Recdetails::join('recassessments', 'recassessments.ra_recdetails_id', 'recdetails.id')->join('users', 'users.id', 'recassessments.ra_user_id')->where('recdetails.id', $decrypted_url_id)->orderBy('recassessments.id', 'desc')->get();

		$get_recassessment_recieved = Recassessmentrec::where('recassessmentrecs.ra_rec_recdetails_id', $decrypted_url_id)->get();

		$get_recactionplan = Recactionplan::where('recactionplans.ap_recdetails_id', $decrypted_url_id)->where('recactionplans.ap_status', null)->get();

		$get_recactionplan_acc = Recactionplan::join('users', 'users.id', 'recactionplans.ap_sender_id')->where('recactionplans.ap_recdetails_id', $decrypted_url_id)->where('recactionplans.ap_status', '!=', null)->orderBy('recactionplans.id', 'desc')->get();

		$get_recactionplan_csa = Recactionplancsa::join('users', 'users.id', 'recactionplancsas.ap_csa_sender_id')->where('recactionplancsas.ap_csa_recdetails_id', $decrypted_url_id)->where('ap_csa_rec_id', null)->get();

		$selectaccreditors=User::orderBy('users.id')->join('role_users', 'role_users.user_id', 'users.id')->join('roles', 'roles.id', 'role_users.role_id')->where('role_users.role_id','=', 4)->pluck('users.name','users.id', 'users.email');

			// foreach($recdetails as $recdetail){
				foreach($recdetails_user_ass as $key => $recdetails_user_ass1){
					if(  (Auth::user()->id == $recdetails->user_id) or ($roles->role_id == 2) or ($roles->user_assigned_level_id == 4) ){
					return view('recdetails.view-recdetails', compact('recdetail', 'recdetails','selectregions','selectlevels', 'selectstatuses', 'selectaccreditors', 'selectapptype', 'selectapptype1', 'selectawarding', 'getapptype', 'roles', 'recdetails_user_ass', 'get_sendaccreditors', 'get_recassessment', 'currentdate', 'get_recassessment_recieved','get_recactionplan','get_recactionplan_acc','get_recactionplan_csa', 'decrypted_url_id', 'selectarps', 'selectaccreditation'));
					}else{
						return redirect()->back()->withSuccess('Access Restriction!');
				    }
				}
			// }
	}
	
    //methods for updating recdetails per row in table
	public function update_rec_details(Request $request, $recdetail){

		$data = array(
			'rec_apptype_id' => $request->input('selectapptype'),
			'rec_name' => $request->input('rec_name'),
			'level_id' => $request->input('selectlevel'),
			'region_id' => $request->input('rec_region'),
			'rec_institution' => $request->input('rec_institution'),
			'rec_address' => $request->input('rec_address'),
			'rec_email' => $request->input('rec_email'),
			'rec_telno' => $request->input('rec_telno'),
			'rec_faxno' => $request->input('rec_faxno'),
			'rec_chairname' => $request->input('rec_chairname'),
			'rec_chairemail' => $request->input('rec_chairemail'),
			'rec_chairmobile' => $request->input('rec_chairmobile'),
			'rec_contactperson' => $request->input('rec_contactperson'),
			'rec_contactposition' => $request->input('rec_contactposition'),
			'rec_contactmobileno' => $request->input('rec_contactmobileno'),
			'rec_contactemail' => $request->input('rec_contactemail'),
			'rec_dateestablished' => $request->input('rec_dateestablished')
		);

		$update = Recdetails::where('recdetails.id', '=', decrypt($recdetail))->join('applicationlevel', 'applicationlevel.recdetails_id', '=', 'recdetails.id')->join('recchairs', 'recdetails.id', '=', 'recchairs.recdetails_id')->update($data);

		return redirect('documents/'.$recdetail)->withSuccess('Step 1 Successfully Saved. You can now upload your requirements here in Step 2');
	}

	//methods for submitting recdetails to phreb
	public function submittophreb($recdetail){

		 try{
   			 DB::beginTransaction();

			   	try{
					$decrypted_url_id = decrypt($recdetail);

					$submittophreb = Recdetails::with(['applicationlevel', 'recdocuments'])->find($decrypted_url_id);

					$recdocuments1 = Recdocuments::where('recdocuments.recdocuments_recdetails_id', $decrypted_url_id);
					$recdocuments1->update(['recdocuments_submittophreb' => Carbon::now()]);
					$recdocuments = $recdocuments1->get();

						
					$data = $submittophreb->update(['recdetails_lock_fields' => true, 'recdetails_resubmitreq' => true]);

					$submittophreb->applicationlevel->update(['statuses_id' => true, 'recdetails_submission_status' => true, 'recdetails_lock_fields' => true, 'recdetails_resubmitreq' => true]);

					$submittophreb->phrebform()->whereIn('pf_recdetails_id', [$decrypted_url_id])->update(['pf_status' => true]);
					
					$add_sub_statuses = Submissionstatuses::create([
						'submissionstatuses_recdetails_id' => $decrypted_url_id,
						'submissionstatuses_user_id' => auth::user()->id,
						'submissionstatuses_statuses_id' => true,
						'submissionstatuses_emailmessage' => $submittophreb->applicationlevel->recdetails_submission_status == 0 ? 'Submitted initial requirements' : null,
						'submissionstatuses_datecreated' => Carbon::now(),
						'submissionstatuses_emailsend' => Carbon::now(),
					]);

					// foreach($get_details as $get_details1){
						$emailTemplate = EmailTemplate::find(1);
						
						$find = ['$rec_name','$rec_institution','$rec_address','$level_name','$region_name','$status_name','$created_at'];
						$replace = [$submittophreb->rec_name,$submittophreb->rec_institution,$submittophreb->rec_address,$submittophreb->applicationlevel->levels->level_name,$submittophreb->applicationlevel->regions->region_name,$submittophreb->applicationlevel->status->status_name,\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $submittophreb->applicationlevel->created_at)->format('F j, Y')];
						$body = str_replace($find, $replace, $emailTemplate->email_body);
					// }

					$get_details1 = $submittophreb;

					Mail::send('email.email_template', compact('get_details1', 'body'), function($message) use ($get_details1, $body){

			            // foreach($get_details as $get_details1){

							// $message->to($get_details1->applicationlevel->level_id == 1 ? 'accreditation-level1@pchrd.dost.gov.ph' : ($get_details1->applicationlevel->level_id == 2 ?'accreditation-level2@pchrd.dost.gov.ph' : 'accreditation-level13@pchrd.dost.gov.ph'));

							$message->to('ethics.secretariat@pchrd.dost.gov.ph');
							$message->subject($get_details1->applicationlevel->status->status_name.' Application: '.$get_details1->rec_institution.'-'.$get_details1->rec_name);
							$message->cc([$get_details1->rec_email, $get_details1->rec_contactemail, $get_details1->recchairs->rec_chairemail]);
							$message->bcc(Auth::user()->email);
			           		$message->from(auth::user()->email);  
						// }
						 	  $sender = auth::id();
			                  $recdetails_id = $get_details1->applicationlevel->recdetails_id;
			                  $from = auth::user()->email;
			                  $to = '';
			                  $cc = $get_details1->rec_email.', '.$get_details1->rec_contactemail;
			                  $bcc = '';
			                  $subject = $get_details1->rec_institution.' - '.$get_details1->rec_name.' '.$get_details1->applicationlevel->status->status_name.' Application';
			                  $body = $body;
			                  emaillogs($sender, $recdetails_id, $from, $to, $cc, $bcc, $subject, $body);
			        });
						
					}catch(\Exception $e){

					    return $e->getMessage();
					}

    		DB::commit();

		}catch(\Exception $e){
    		
    		DB::rollback();
    		return redirect()->back()->withSuccess('Error Detected!');
		}

		return redirect()->back()->withSuccess('Application successfully submitted!');
		
	}

		//methods to update rec_status or datecompleted
	public function update_rec_status($recdetail, Request $request){
		
		$validator = \Validator::make($request->only('emailattachment'),[
            'emailattachment.*' => 'required|mimes:jpg,jpeg,png,pdf,doc,docx',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withSuccess('Update Failed. Please use the following format for the documents .pdf, .docx, .doc only')->withErrors($validator)->withInput();
        }

		$decrypted_id = decrypt($recdetail);
		$sender = auth::user()->name;
		$deadline = Carbon::today()->addDays(14);

		$data = $request->only('status', 'date_completed', 'date_accreditation', 'date_accreditation_expiry', 'accreditation_no', 'rec_accreditation', 'selectlevelnew');

		$update_rec_status = Recdetails::find($decrypted_id);

		$get_statuses = Status::where('statuses.id', $data['status'])->first();

		 try{
   			 DB::beginTransaction();

			//incomplete
			if($data['status'] == 3){
				$update_rec_status->applicationlevel->update(['statuses_id' => $data['status'], 'recdetails_lock_fields' => true, 'recdetails_resubmitreq' => false]); 
				$update_rec_status->phrebform()->update(['pf_status' => null]);
		    	$emailTemplate = EmailTemplate::find(2);

		    //Resubmission of requirements
			}elseif($data['status'] == 9){
				$update_rec_status->applicationlevel->update(['recdetails_submission_status' => true, 'recdetails_lock_fields' => true, 'recdetails_resubmitreq' => false]);
				$update_rec_status->phrebform()->update(['pf_status' => null]);
		    	$emailTemplate = EmailTemplate::find(3);	

		    //Completed requirements
			}elseif($data['status'] == 2){
			    $update_rec_status->applicationlevel->update(['date_completed' => Carbon::today(), 'recdetails_lock_fields' => true, 'recdetails_resubmitreq' => true]);
		    	$emailTemplate = EmailTemplate::find(4);
			
			//Accredited
			}elseif($data['status'] == 4 || $data['status'] == 28 || $data['status'] == 29){
				$update_rec_status->applicationlevel->update(['level_id' => $data['selectlevelnew'], 'date_accreditation' => $data['date_accreditation']]);
				$emailTemplate = EmailTemplate::find(20);

			//On-going //Deffered // Withdrawn //Accreditation Expired //Complete, Incomplete, and Resubmission of REC Compliances //Disapproved
			}elseif($data['status'] == 19 OR $data['status'] == 11 OR $data['status'] == 5 OR $data['status'] == 17 OR $data['status'] == 21 OR $data['status'] == 22 OR $data['status'] == 23 OR $data['status'] == 24 OR $data['status'] == 25){
				$update_rec_status->applicationlevel->update(['recdetails_lock_fields' => true, 'recdetails_resubmitreq' => true]);
				$emailTemplate = EmailTemplate::find(5);
			}

			$get_details1 = $update_rec_status;
			
	   	//}

	    $checkSubmissionStats = Submissionstatuses::whereIn('submissionstatuses_recdetails_id', [$decrypted_id])->orderBy('id', 'desc')->first();

			if($checkSubmissionStats->submissionstatuses_statuses_id != $data['status'] OR $checkSubmissionStats->submissionstatuses_statuses_id == 25 OR $checkSubmissionStats->submissionstatuses_statuses_id == 19 OR $checkSubmissionStats->submissionstatuses_statuses_id == 4) {

				if(($request->allow_upload_compliance == true AND $data['status'] == 4) OR ($request->allow_upload_compliance == true AND $data['status'] == 19) OR ($data['status'] == 3) OR ($data['status'] == 9) OR ($data['status'] == 21) OR ($data['status'] == 22)){

					$responseData = null;

				}else{

					$responseData = Carbon::now();
				}

				// $add_sub_statuses = Submissionstatuses::create([

				// 	'submissionstatuses_recdetails_id' => $decrypted_id,
				// 	'submissionstatuses_user_id' => auth::user()->id,
				// 	'submissionstatuses_statuses_id' => $data['status'],
				// 	'submissionstatuses_emailmessage' => $request->emailmessage,
				// 	'submissionstatuses_datecreated' => Carbon::now(),
				// 	'submissionstatuses_rec_response' =>  $responseData,

				// 	if($data['status' == 4] OR $data['status' == 25] OR $data['status' == 28] OR $data['status' == 29]){

				// 		'submissionstatuses_date_accreditation' => $request->date_accreditation,
				// 		'submissionstatuses_date_expiry' => $request->date_accreditation_expiry,
				// 		'submissionstatuses_level' => $request->selectlevelnew,
				// 	}
				// ]);

				$dataArray = [
				    'submissionstatuses_recdetails_id' => $decrypted_id,
				    'submissionstatuses_user_id' => auth()->user()->id,
				    'submissionstatuses_statuses_id' => $data['status'],
				    'submissionstatuses_emailmessage' => $request->emailmessage,
				    'submissionstatuses_datecreated' => Carbon::now(),
				    'submissionstatuses_rec_response' => $responseData,
				];

				// Conditionally add fields if status matches 4, 25, 28, or 29
				in_array($data['status'], [4, 25, 28, 29]) ? 
				    $dataArray = array_merge($dataArray, [
				        'submissionstatuses_date_accreditation' => $request->date_accreditation,
				        'submissionstatuses_date_expiry' => $request->date_accreditation_expiry,
				        'submissionstatuses_level' => $request->selectlevelnew,
				    ]) 
				    : null;

				// Create record
				$add_sub_statuses = Submissionstatuses::create($dataArray);


				if($data['status'] == 3 or $data['status'] == 9){

					$add_sub_statuses->submissionstatuses_duedate()->create([
						'submissionstatuses_duedate' => $request->submisssionstatus_duedate,
						'submissionstatuses_user_id' => Auth::user()->id
					]);
				}

				if($request->hasFile('emailattachment')){

					foreach($request->emailattachment as $file){

						$generatedfileID = uniqid();

						$file->storeAs('public/documentsEmailattachments/'.Auth::user()->id.'/'.$update_rec_status->id, $generatedfileID.'-'.$file->getClientOriginalName());

						$fileModel = new Recdocuments_emailattachment;
						$fileModel->re_submissionstatuses_id = $add_sub_statuses->id;
						$fileModel->re_recdetails_id = $update_rec_status->id;
						$fileModel->re_user_id = Auth::user()->id;
						$fileModel->re_file_name = $file->getClientOriginalName();
						$fileModel->re_file_generated = $generatedfileID;
						$fileModel->re_file_extension = $file->getClientOriginalExtension();
						$fileModel->re_file_size = $file->getSize();

						$fileModel->save();
					}
				}

			}

	   if($data['status'] == 21 OR $data['status'] == 22 OR $data['status'] == 23 OR $data['status'] == 25 OR $data['status'] == 26 OR $data['status'] == 28 OR $data['status'] == 29){
		
			$remain_status = $update_rec_status->applicationlevel->statuses_id;
		
		}else{

			$remain_status = $data['status'];
		}

		$update_rec_status->applicationlevel->update(['statuses_id' => $remain_status, 'date_accreditation_expiry' => $data['date_accreditation_expiry'], 'accreditation_number' => $data['accreditation_no'], 'rec_accreditation' => $data['rec_accreditation']]);

		DB::commit();

		}catch(\Exception $e){
    		dd($e);
    		DB::rollback();

    		return redirect()->back()->withSuccess('Upload failed, please try again or contact administrator.');
		}
		
		return redirect()->back()->withSuccess('Application successfully updated!');

	}

	public function sendOR(Recdetails $recdetail){
		$recdetails = Recdetails::find($recdetail->id);
		$uniqid = uniqid();
		$date = carbon::today();
		$serialno = Carbon::today()->toDateString().$recdetail->id;

		$pdf = PDF::loadview('email.email_orpayeescopy', compact('recdetails','date','serialno'))->save(public_path('storage/ors/'.$uniqid.'.pdf'));
		$get_details1 = $recdetails;
		// $get_details1 = $recdetails;
		// foreach($recdetails as $get_details1){
		$emailTemplate = EmailTemplate::find(13);
		$body = str_replace(['$rec_chairname', '$secname'], [$recdetails->recchairs->rec_chairname, Auth::user()->name], $emailTemplate->email_body);
		// }

			Mail::send('email.email_template', compact('recdetails', 'body', 'get_details1'), function($message) use ($recdetail, $pdf, $get_details1, $body, $date){
				
	        	$message->to($get_details1->recchairs->rec_chairemail);
	        	$message->cc([$get_details1->rec_email, $get_details1->rec_contactemail, $get_details1->users->email]);
	        	$message->subject($get_details1->rec_name.'-'.$get_details1->rec_institution.' Order of Payment');
	        	$message->attachData($pdf->output(), $get_details1->rec_name.'-'.$get_details1->rec_institution.' Order of Payment.pdf');

	        	$sender = auth::id();
            	$recdetails_id = $get_details1->applicationlevel->recdetails_id;
            	$from = auth::user()->email;
            	$to = $get_details1->recchairs->rec_chairemail;
            	$cc = $get_details1->rec_email.', '.$get_details1->rec_contactemail.', '.$get_details1->users->email;
            	$bcc = '';
            	$subject = $get_details1->rec_name.'-'.$get_details1->rec_institution.' Order of Payment';
            	$body = $body;
            	emaillogs($sender, $recdetails_id, $from, $to, $cc, $bcc, $subject, $body);   
			});
			
			SendOrderpayment::create([
				'or_sender_id' => auth::id(),
				'or_recdetails_id' => $recdetail->id,
				'or_serialno' => $serialno,
				'or_file' => $recdetail->rec_name.'-'.$recdetail->rec_institution.' Order of Payment.pdf', 
				'or_file_generated' => $uniqid,
				'or_datecreated_at' => carbon::now()
			]);
		
		return redirect()->back()->withSuccess('Order of Payment Successfully Sent!');
	}

	public function generateOR(Recdetails $recdetail){
		$recdetails = Recdetails::find($recdetail->id);
		$date = carbon::today(); 
		$serialno = Carbon::today()->toDateString().$recdetail->id;
		$customPaper = array(0,0,530,1220);
		$uniqid = uniqid();

		$pdf = PDF::loadview('OrderPayment.generateORTemplate', compact('recdetails', 'date', 'serialno'));
		$pdf->setPaper($customPaper, 'landscape');
		$pdf->save(storage_path('/app/public/ors/'.$uniqid.'.pdf'));

		$fileModel = new SendOrderpayment;
          $fileModel->or_sender_id = auth::id();
          $fileModel->or_recdetails_id = $recdetail->id;
          $fileModel->or_serialno = $serialno;
          $fileModel->or_file = $serialno.'-'.$recdetail->rec_name.'-'.$recdetail->rec_institution.' Order of Payment.pdf';
          $fileModel->or_file_generated = $uniqid;
          $fileModel->or_datecreated_at = carbon::now();
          $fileModel->save();

		return $pdf->download($serialno.'-'.$recdetail->rec_name.' Order of Payment.pdf');
	}

	public function awarding(Request $request, Recdetails $recdetail){
		$data = $request->only('awarding_letter');

		// $recdetails = Recdetails::join('applicationlevel', 'applicationlevel.recdetails_id', 'recdetails.id')->find($recdetail->id);
		$recdetails = Recdetails::find($recdetail->id);
		$awarding_status = DocumentTypes::find($data['awarding_letter']);
		// $body = str_replace(['$date', '$rec_chairname', '$rec_name', '$rec_institution', '$date_accreditation', '$awarding_status', ''], replace, subject);
		return view('awardingletter.awarding', compact('recdetails', 'awarding_status'));
	}

	public function acc_no(Request $request, $id){
		$add_acc_no = Applicationlevel::find($id)->update(['accreditation_number' => $request->input('acc_no')]);;
		return redirect()->back()->withSuccess('Accreditation Number Has Been Added!');
	}

	public function sendaccreditationexpiry_reminder($id){
		return emailTemplate2($id);
	}

	public function changeofacc(Request $request, $id){
		$rec = Recdetails::find($id);

		$recdetails = $rec->replicate();
		$recdetails->user_id = auth::user()->id;
		$recdetails->rec_apptype_id = $request->input('selectapptype');
		$recdetails->save();

		$recdetails->levels()->attach(
			['level_id' => $request->input('selectlevel')],
			['region_id' => $rec->applicationlevel->region_id]
		);

		$recdetails->recchairs()->create(['rec_chairname' => $rec->recchairs->rec_chairname, 'rec_chairemail' => $rec->recchairs->rec_chairemail, 'rec_chairmobile' => $rec->recchairs->rec_chairmobile,]);

		$recdetails->applicationlevel_reminders()->create();
		$recdetails->existing_recs()->create();

		$recdetails->applicationlevel()->update(['recdetails_submission_status' => true, 'recdetails_lock_fields' => true, 'statuses_id' => 19]);
		$rec->applicationlevel()->update(['rec_change_of_acc' => 'This accreditation has been changed from '.$rec->status->status_name.' to '.$recdetails->status->status_name]);

		Mail::raw(Auth::user()->name.' changed the application of accreditation type of '.$rec->rec_institution.'- '.$rec->rec_name.' from Level '.$rec->applicationlevel->level_id.' - '.$rec->status->status_name.' to '.' Level '.$recdetails->applicationlevel->level_id.' - '.$recdetails->status->status_name, function($message){
            $message->subject('Change of Application in Database');
            //$message->from('phrebaccreditationsystem@gmail.com','PHREB Accreditation Database System');
            $message->to('phrebaccreditationsystem@gmail.com')->cc([Auth::user()->email, 'ethics.secretariat@pchrd.dost.gov.ph'])->bcc('jrcambonga@pchrd.dost.gov.ph');
        });

		return redirect()->route('view-recdetails', ['id' => encrypt($recdetails->id)])->withSuccess('Application of Accreditation Type  has been created!');
	}

	//helper
	public function phrebforms($add_recdetails, $pf1){
		$find = ['$rec_name','$rec_institution','$rec_address','$rec_email','$rec_telno','$rec_faxno','$rec_contactperson','$rec_contactposition','$rec_contactmobileno','$rec_contactemail', '$rec_dateestablished', '$rec_chairemail', '$rec_chairname', '$rec_chairmobile'];

		$replace = [$add_recdetails->rec_name, $add_recdetails->rec_institution, $add_recdetails->rec_address, $add_recdetails->rec_email, $add_recdetails->rec_telno, $add_recdetails->rec_faxno, $add_recdetails->rec_contactperson, $add_recdetails->rec_contactposition, $add_recdetails->rec_contactmobileno, $add_recdetails->rec_contactemail, \Carbon\Carbon::createFromFormat('Y-m-d', $add_recdetails->rec_dateestablished)->format('F j, Y'), $add_recdetails->recchairs->rec_chairemail, $add_recdetails->recchairs->rec_chairname, $add_recdetails->recchairs->rec_chairmobile];

		$body1 = str_replace($find, $replace, $pf1->pft_body);

		return;
	}

	// withdraw application
	// public function withdraw($id){
		
	// 	$withdraw = Recdetails::find($id);
	// 	$withdraw->applicationlevel->update(['statuses_id' => 27, 'recdetails_submission_status' => true, 'recdetails_lock_fields' => true, 'recdetails_resubmitreq' => true]);

	// 	$recdocuments = Recdocuments::where('recdocuments.recdocuments_recdetails_id', $withdraw->id);
	// 	$recdocuments->update(['recdocuments_submittophreb' => Carbon::now()]);

	// 	$add_sub_statuses = Submissionstatuses::create([
	// 		'submissionstatuses_recdetails_id' => $withdraw->id,
	// 		'submissionstatuses_user_id' => auth::user()->id,
	// 		'submissionstatuses_statuses_id' => 27,
	// 		'submissionstatuses_emailmessage' => $withdraw->applicationlevel->recdetails_submission_status == 0 ? 'Application Withdrawn' : null,
	// 		'submissionstatuses_datecreated' => Carbon::now(),
	// 		'submissionstatuses_emailsend' => null,
	// 	]);

	// 	return redirect()->back()->withSuccess('Application withdrawn successfully.');
	// }

}