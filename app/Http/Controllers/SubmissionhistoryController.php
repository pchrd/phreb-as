<?php
namespace App\Http\Controllers;
use App\Models\Recdocuments;
use App\Models\Recdetails;
use App\Models\Submissionstatuses;
use App\Models\Recassessmentscsa;
use App\Models\Recassessmentrec;
use App\Models\Recactionplancsa;
use App\Models\User;
use Auth;
use DB;
use Carbon\Carbon;

use Illuminate\Http\Request;

class SubmissionhistoryController extends Controller
{
    public function sub_history(){
    	$sub_history = Submissionstatuses::select('*', 'submissionstatuses.id')->join('recdetails', 'recdetails.id', 'submissionstatuses.submissionstatuses_recdetails_id')->join('users', 'users.id', 'submissionstatuses.submissionstatuses_user_id')->join('statuses', 'statuses.id', 'submissionstatuses.submissionstatuses_statuses_id')->where('submissionstatuses.id', '!=', false)->orderBy('submissionstatuses.id', 'desc')->paginate(10);
    	return view('historysubmission.history-submission', compact('sub_history'));
    }

    public function assessmenthistory($assname, $filename, $id){
    	$asscsachair = Recassessmentscsa::where('recassessmentscsas.ra_csa_ra_id', $id)->orderBy('recassessmentscsas.id', 'desc')->get();
    	$assrec = Recassessmentrec::where('recassessmentrecs.ra_rec_ra_id', $id)->orderBy('recassessmentrecs.id', 'desc')->get();
    	return view('historysubmission.assessmenthistory', compact('assname', 'id', 'filename', 'asscsachair', 'assrec', 'csachairname'));
    }

    public function actionplanhistory($sender, $filename, $id){
    	$apce_csachair = Recactionplancsa::where('recactionplancsas.ap_csa_ap_id', $id)->where('ap_csa_rec_id', null)->orderBy('recactionplancsas.id', 'desc')->get();
    	$apce_rec = Recactionplancsa::where('recactionplancsas.ap_csa_ap_id', $id)->where('ap_csa_csachair_id', null)->orderBy('recactionplancsas.id', 'desc')->get();
    	return view('historysubmission.actionplanhistory', compact('sender', 'filename', 'apce_csachair', 'apce_rec'));
    }
}