<?php
namespace App\Http\Controllers;
use App\Models\Existing_arps;
use App\Models\Recdetails;
use Illuminate\Http\Request;

class ExistingArpsController extends Controller{

   public function add_arps(Request $request, $id){

	   	$this->validate($request,[
	     'file.*' => 'mimes:zip,pdf,docx,doc|max:25600',
	    ]);
        if($request->hasFile('file')){
        foreach($request->file as $files){
          $filename = $files->getClientOriginalName();
          $unique_filename = uniqid();
          $extension = $files->getClientOriginalExtension();	
          $files->storeAs('public/phrebdatabasefiles/'.$id, $unique_filename.'.'.$extension);

          $fileModel = new Existing_arps;
          $fileModel->recdetails_id = $id;
          $fileModel->selectarps = $request->input('selectarps');
          $fileModel->foryear = $request->input('foryear');
          $fileModel->arps_submission = $request->input('arps_submission');
          $fileModel->file = $filename;
          $fileModel->file_uniqid = $unique_filename;
          $fileModel->file_extension = $extension;
          $fileModel->save();
        }
        }else{
          Existing_arps::create([
            'recdetails_id' => $id,
            'selectarps' => $request->input('selectarps'),
            'foryear' => $request->input('foryear'),
            'arps_submission' => $request->input('arps_submission'),
          ]);
        }
        return redirect()->back()->withSuccess('Upload Successful!');
   }

   public function delete_arps($id){
   		$arps = Existing_arps::find($id)->delete();
   		return redirect()->back()->withSuccess('File Successfully Deleted!');
   }

}