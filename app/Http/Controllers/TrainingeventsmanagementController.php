<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Trainingeventsmanagement;
use App\Models\Trainingeventstrainees;
use App\Models\Trainingeventsorientation;
use App\Models\Trainingeventstrainees_affiliations;
use App\Models\Accreditation_role;
use App\Models\EmailTemplate;
use DataTables;
use DB;
use Auth;
use Mail;

class TrainingeventsmanagementController extends Controller{

	public function show(){
		$eventnames = Trainingeventsorientation::orderBy('id', 'desc')->pluck('training_type_name', 'id');
		$trainees = Trainingeventstrainees::select(
			DB::raw("CONCAT(trainees_lname, ', ',trainees_fname, ' ', trainees_mname) AS trainee_name"), 'trainingeventstrainees.id'
		)->pluck('trainee_name', 'trainingeventstrainees.id');
		$acc_recroles = $this->getRecRole();
		$getTrainingRole = $this->getTrainingRole();

		return view('trainees.show-training-management', compact('eventnames', 'trainees', 'acc_recroles', 'getTrainingRole'));
	}

	public function show_ajax(){
		$trainees_mngt = Trainingeventsmanagement::with('trainees', 'eventsorientations', 'addedby_users', 'roleinevents')->get();
		return DataTables::of($trainees_mngt)->addColumn('action', function ($trainees_mngt){
			return '<a href="'.route('delete-trainee-attendance', ['id' => $trainees_mngt->id]).'" title="Delete" class="btn btn-xs btn-danger" onclick="'.'return confirm(\'Do you want to Delete the attendance of '.$trainees_mngt->trainees->trainees_fname.' '.$trainees_mngt->trainees->trainees_mname.' '.$trainees_mngt->trainees->trainees_lname.' in '.$trainees_mngt->eventsorientations->training_type_name.' ['.$trainees_mngt->eventsorientations->training_start_date.' - '.$trainees_mngt->eventsorientations->training_end_date.']?\')'.'"><i class="fa fa-trash-o"></i></a>';
		})->make(true);
	}

	public function add(Request $request){

		$emailTemplate = EmailTemplate::find(28);

		if($request->te_trainee_id != null){

			$getAffiliation = Trainingeventstrainees::find($request->te_trainee_id);

			$add_attendance = Trainingeventsmanagement::create([
				'te_orientation_id' => $request->te_orientation_id,
				'te_trainee_id' => $request->te_trainee_id,
				'te_role_event_id' => $request->te_role_event_id,
				'te_affiliation_id' => $getAffiliation->affiliationsLatestOnly->id,
				'te_addedby_id' => auth::id(),
			]);

			if($add_attendance->trainees->affiliationsLatestOnly()->exists()){
      
	        	$body = str_replace(['$fname', '$mname', '$lname', '$trainingtype', '$start_date', '$end_date'], [$add_attendance->trainees->trainees_lname, $add_attendance->trainees->trainees_fname, $add_attendance->trainees->trainees_mname, $add_attendance->eventsorientations->training_type_name, \Carbon\Carbon::createFromFormat('Y-m-d', $add_attendance->eventsorientations->training_start_date)->format('F j, Y'), \Carbon\Carbon::createFromFormat('Y-m-d', $add_attendance->eventsorientations->training_end_date)->format('F j, Y')], $emailTemplate->email_body);
	                 
	            Mail::send('email.email_template1', compact('body'), function($message) use ($add_attendance){
	            	$message->subject($add_attendance->eventsorientations->training_type_name);
	                $message->to($add_attendance->trainees->affiliationsLatestOnly->trainees_email);
	                $message->bcc('jrcambonga@pchrd.dost.gov.ph');
	                $message->cc('ethics.secretariat@pchrd.gov.ph');
	            });

				return redirect()->back()->withSuccess('Trainee Attendance of '.$add_attendance->trainees->trainees_lname.', '.$add_attendance->trainees->trainees_fname.' '.$add_attendance->trainees->trainees_mname.' has added to the list.');
			}else{
				return "<script>alert('Process Denied!. No Affiliation in his/her profile')</script>".redirect()->back();
			}

		}else{

			$validator = \Validator::make($request->all(), [
				'trainees_lname' => 'required',
				'trainees_fname' => 'required',
				'trainees_gender' => 'required',
				'trainees_institution' => 'required',
				'trainees_institution_address' => 'required',
				'trainees_email' => 'required',
				'trainees_recrole' => 'required',
				'trainees_certificatename' => 'required',
			]);

			if($request->trainees_mnam = "") {
				$request->trainees_mname = ' ';
			}

			if($validator->fails()){
				$alert = "<script>alert('Attendance Submission Failed! Please Complete other fields!')</script>";
				return $alert.redirect()->back()->withErrors($validator->errors());
			}


			$add_trainee = Trainingeventstrainees::create([
				'trainees_lname' => $request->trainees_lname,
				'trainees_fname' => $request->trainees_fname,
				'trainees_mname' => $request->trainees_mname,
				'trainees_gender' => $request->trainees_gender,
				'trainees_certificatename' => $request->trainees_certificatename,
			]);

			$add_affiliations = Trainingeventstrainees_affiliations::create([
				'trainees_id' => $add_trainee->id,
				'trainees_institution' => $request->trainees_institution,
				'trainees_institution_address' => $request->trainees_institution_address,
				'trainees_specialization' => $request->trainees_specialization,
				'trainees_email' => $request->trainees_email,
				'trainees_contactno' => $request->trainees_contactno,
				'trainees_profbackground' => $request->trainees_profbackground,
				'trainees_committee' => $request->trainees_committee,
				'trainees_recrole' => $request->trainees_recrole,
				'addedby' => auth::user()->id,
			]);
			
			$add_attendance = Trainingeventsmanagement::create([
				'te_orientation_id' => $request->te_orientation_id,
				'te_trainee_id' => $add_trainee->id,
				'te_role_event_id' => $request->te_role_event_id,
				'te_affiliation_id' => $add_affiliations->id,
				'te_addedby_id' => auth::id(),
			]);

			$body = str_replace(['$fname', '$mname', '$lname', '$trainingtype', '$start_date', '$end_date'], [$request->trainees_lname, $request->trainees_fname, $request->trainees_mname, $add_attendance->eventsorientations->training_type_name, \Carbon\Carbon::createFromFormat('Y-m-d', $add_attendance->eventsorientations->training_start_date)->format('F j, Y'), \Carbon\Carbon::createFromFormat('Y-m-d', $add_attendance->eventsorientations->training_end_date)->format('F j, Y')], $emailTemplate->email_body);
	                
	        Mail::send('email.email_template1', compact('body'), function($message) use ($request, $add_attendance){
	            	$message->subject($add_attendance->eventsorientations->training_type_name);
	                $message->to($request->trainees_email);
	                $message->bcc('jrcambonga@pchrd.dost.gov.ph');
	                //$message->cc('ethics.secretariat@pchrd.gov.ph');
	            });

			return redirect()->back()->withSuccess('Trainee Attendance of '.$request->trainees_lname.', '.$request->trainees_fname.' '.$request->trainees_mname.' has added to the list of attendance.');
		}
	}

	public function delete($id){
		$deleteData = Trainingeventsmanagement::findOrFail($id);
		$deleteData->delete();
		return redirect()->back()->withSuccess('Trainee Successfully Deleted!');
	}

	public function getRecRole(){
       $acc_role = Accreditation_role::whereIn('id', [5,6,7,8,9,10])->pluck('roleinevent', 'id');
       return $acc_role;
    }

    public function getTrainingRole(){
        $training_role = Accreditation_role::whereIn('id', [1,2,3,4])->pluck('roleinevent', 'id');
    	return $training_role;
    }

}
