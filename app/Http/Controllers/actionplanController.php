<?php
namespace App\Http\Controllers;
use App\Models\Recdetails;
use App\Models\Recactionplan;
use App\Models\Recactionplancsa;
use App\Models\User;
use App\Models\Emaillogs;
use App\Models\Sendtoaccreditor;
use App\Models\DocumentTypes;
use App\Models\EmailTemplate;
use Carbon\Carbon;
use Auth;
use Mail;
use Illuminate\Http\Request;

class actionplanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id){
        $decrypted_id = decrypt($id);
        $roles = User::join('role_users', 'role_users.user_id', 'users.id')->where('users.id',auth::id())->first();
        $doctypes = DocumentTypes::whereIn('id',[15,16])->pluck('document_types_name', 'id'); 
        $actionplan = Recactionplan::where('ap_sender_id', auth::id())->where('ap_recdetails_id', $decrypted_id)->orderByRaw('ap_document_types',15)->get();
        $recdetails = Recdetails::find($decrypted_id);
        return view('recactionplan.actionplan', compact('recdetails','actionplan', 'doctypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // send action plan and comp evi for REC
    public function store(Request $request){
       $this->validate($request,[
        'ap_recactionplan.*' => 'mimes:pdf,docx,doc',
       ]);

        if($request->hasFile('ap_recactionplan')){
            $ap_recdetails_id = $request->input('ap_recdetails_id');
            $ap_document_types = $request->input('document_type');
            $ap_sender_id = auth::id();

            foreach($request->ap_recactionplan as $file){
                $filename = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $unique_filename = uniqid();
                $file->storeAs('public/actionplansandcomplianceevidences/'.Auth::user()->id.'/'.$ap_recdetails_id, $unique_filename.'-'.$filename);
                $fileModel = new Recactionplan;
                $fileModel->ap_recdetails_id = $ap_recdetails_id;
                $fileModel->ap_sender_id = $ap_sender_id;
                $fileModel->ap_document_types = $ap_document_types;
                $fileModel->ap_recactionplan_name = $filename;
                $fileModel->ap_recactionplan_uniqid = $unique_filename;
                $fileModel->save();

                // $recdetails = Recdetails::join('applicationlevel', 'applicationlevel.recdetails_id', 'recdetails.id')->find($request->input('ap_recdetails_id'));

                $recdetails = Recdetails::find($request->input('ap_recdetails_id'));

                $accreditorsemail = Sendtoaccreditor::leftjoin('users', 'users.id', 'sendtoaccreditors.a_accreditors_user_id')->where('sendtoaccreditors.a_recdetails_id', $request->input('ap_recdetails_id'))->get();

                $doctypes = DocumentTypes::find($ap_document_types);

                $get_details1 = $recdetails;
                $me = auth::user()->email;
                $date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $recdetails->applicationlevel->created_at)->format('F j, Y');

                $emailTemplate = EmailTemplate::find(14);

                $body = str_replace(['$date', '$rec_name', '$level_id', '$doctype_name'], [$date, $recdetails->rec_name, $recdetails->level_id, $doctypes->document_types_name], $emailTemplate->email_body);
                
                Mail::send('email.email_template', compact('get_details1','body'), function($message) use ($recdetails,$file,$filename,$accreditorsemail,$body,$doctypes,$request){ 
                    $message->to('jrcambonga@pchrd.gov.ph'); 
                    $message->cc([$recdetails->rec_email, $recdetails->rec_contactemail, $recdetails->recchairs->rec_chairemail, $recdetails->users->email]);
                    $message->subject($recdetails->rec_name." - ".$doctypes->document_types_name);

                    foreach($request->ap_recactionplan as $file){
                    $message->attach($file->getRealPath(), ['as' => $filename]);
                    }

                    foreach($accreditorsemail as $ae){
                    $message->bcc($ae->email);   

                    $sender = auth::id();
                    $recdetails_id = $recdetails->applicationlevel->recdetails_id;
                    $from = auth::user()->email;
                    $to = 'PHREB Accreditation System - Ethics';
                    $cc = $recdetails->rec_email.', '.$recdetails->rec_contactemail.', '.$recdetails->recchairs->rec_chairemail.', '.$recdetails->users->email;
                    $bcc = $ae->email;
                    $subject = $recdetails->rec_name." - ".$doctypes->document_types_name;
                    $body = $body;
                    emaillogs($sender, $recdetails_id, $from, $to, $cc, $bcc, $subject, $body);

                    // $emaillogs = Emaillogs::create([
                    //     'e_sender_user_id' => auth::id(),
                    //     'e_recdetails_id' => $recdetails->recdetails_id,
                    //     'e_from' => auth::user()->email,
                    //     'e_to' => 'PHREB Accreditation System - Ethics',
                    //     'e_cc' => $recdetails->rec_email.', '.$recdetails->rec_contactemail.', '.$recdetails->recchairs->rec_chairemail.', '.$recdetails->users->email,
                    //     'e_bcc' => $ae->email,
                    //     'e_subject' => $recdetails->rec_name." - ".$doctypes->document_types_name,
                    //     'e_body' => $body,
                    //     'e_created_at' => Carbon::now()
                    // ]);
                    }

                });
            }
            return redirect()->back()->withSuccess('Action Plan Successfully Submitted!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function apcheck(Request $request){   
        $check = $request->input('ap_status');
        dd($check);
        return 'yes';
    }

    public function ap_sendtocsachair(Request $request){
       $data = $request->only('ap_csa_ap_id', 'ap_csa_recdetails_id', 'ap_csa_csachair_id','sap_id');

       // $get_details1 = Recactionplan::join('applicationlevel', 'applicationlevel.recdetails_id', 'recactionplans.ap_recdetails_id')->join('users', 'users.id', 'recactionplans.ap_sender_id')->find($data['sap_id']);

       $get_details1 = Recactionplan::join('users', 'users.id', 'recactionplans.ap_sender_id')->find($data['sap_id']);

       $getcsaemail = User::where('users.id', $data['ap_csa_csachair_id'])->first();

       $emailTemplate = EmailTemplate::find(16);

       $body = str_replace(['$date', '$csachair', '$doctype_name', '$rec_name', '$level_id', '$status_name', '$accname', '$secname'], [Carbon::today()->format('F j, Y'), $getcsaemail->name, $get_details1->documenttypes->document_types_name, $get_details1->recdetails->rec_name, $get_details1->applicationlevel->level_id, $get_details1->recdetails->status->status_name, $get_details1->name, auth::user()->name], $emailTemplate->email_body);

       $get_actionplan_file = "app/public/actionplansandcomplianceevidences/$get_details1->ap_sender_id/$get_details1->ap_recdetails_id/$get_details1->ap_recactionplan_uniqid-$get_details1->ap_recactionplan_name";

       Mail::send('email.email_template', compact('body', 'get_details1'), function($message) use ($get_details1, $getcsaemail, $get_actionplan_file){ 
                    $message->to($getcsaemail->email);
                    $message->cc($get_details1->email);
                    $message->subject($get_details1->recdetails->rec_name." - ".$get_details1->documenttypes->document_types_name);
                    $message->attach(storage_path($get_actionplan_file), ['as' => $get_details1->ap_recactionplan_name]);
        });
                    $sender = auth::id();
                    $recdetails_id = $get_details1->applicationlevel->recdetails_id;
                    $from = 'PHREB - Accreditation System';
                    $to = $getcsaemail->email;
                    $cc = $get_details1->email;
                    $bcc = '';
                    $subject = $get_details1->recdetails->rec_name." - ".$get_details1->documenttypes->document_types_name;
                    $body = $body;
                    emaillogs($sender, $recdetails_id, $from, $to, $cc, $bcc, $subject, $body);

       Recactionplancsa::create([
            'ap_csa_ap_id' => $data['ap_csa_ap_id'],
            'ap_csa_recdetails_id' => $data['ap_csa_recdetails_id'],
            'ap_csa_sender_id' => auth::id(),
            'ap_csa_csachair_id' => $data['ap_csa_csachair_id'],
            'ap_csa_datecreated_at' => Carbon::now(),
       ]);

       return redirect()->back()->withSuccess($get_details1->documenttypes->document_types_name.' Successfully Submitted to '.$getcsaemail->name);
    }

    // send apce to REC from csachair
    public function ap_sendtorec(Request $request){
       $this->validate($request,[
            'apce_letter.*' => 'mimes:pdf,docx,doc',
       ]);

       $data = $request->only('ap_csa_recdetails_id', 'ap_sender_id','sap_id', 'apce_letter');

       $get_details1 = Recactionplan::join('applicationlevel', 'applicationlevel.recdetails_id', 'recactionplans.ap_recdetails_id')->where('recactionplans.id', $data['sap_id'])->first();

       $get_actionplan_file = "app/public/actionplansandcomplianceevidences/$get_details1->ap_sender_id/$get_details1->recdetails_id/$get_details1->ap_recactionplan_uniqid-$get_details1->ap_recactionplan_name";

       $emailTemplate = EmailTemplate::find(17);
       
       $body = str_replace(['$date', '$rec_name', '$rec_address', '$rec_chairname', '$doctype_name', '$level_id', '$status_name', '$secname'], [Carbon::today()->format('F j, Y'), $get_details1->recdetails->rec_name, $get_details1->recdetails->rec_address, $get_details1->recdetails->recchairs->rec_chairname, $get_details1->documenttypes->document_types_name, $get_details1->level_id, $get_details1->recdetails->status->status_name, auth::user()->name], $emailTemplate->email_body); 

       if($request->hasFile('apce_letter')){
           $filename = $data['apce_letter'];
           foreach($request->apce_letter as $file){
              $filename = $file->getClientOriginalName();
            
              Mail::send('email.email_template', compact('body', 'get_details1'), function($message) use ($filename, $file, $get_details1, $get_actionplan_file, $body, $data){
                    $message->subject($get_details1->recdetails->rec_name.'| Level '.$get_details1->level_id.' - '.$get_details1->documenttypes->document_types_name); 
                    $message->to($get_details1->recdetails->recchairs->rec_chairemail);
                    $message->cc([$get_details1->recdetails->rec_email, $get_details1->recdetails->rec_contactemail, $get_details1->recdetails->users->email]);
                    $message->attach($file->getRealPath(), ['as' => $filename]);
                    $message->attach(storage_path($get_actionplan_file), ['as' => $get_details1->ap_recactionplan_name]);
              });
           }
       }

       if($request->only('apce_letter') == null){
                 Mail::send('email.email_template', compact('body', 'get_details1'), function($message) use ($get_details1, $get_actionplan_file, $body, $data){
                    $message->subject($get_details1->recdetails->rec_name.'| Level '.$get_details1->level_id.' - '.$get_details1->documenttypes->document_types_name); 
                    $message->to($get_details1->recdetails->recchairs->rec_chairemail);
                    $message->cc([$get_details1->recdetails->rec_email, $get_details1->recdetails->rec_contactemail, $get_details1->recdetails->users->email]);
                    $message->attach(storage_path($get_actionplan_file), ['as' => $get_details1->ap_recactionplan_name]);
                });
              }
                $sender = auth::id();
                $recdetails_id = $get_details1->recdetails_id;
                $from = auth::user()->email;
                $to = $get_details1->recdetails->recchairs->rec_chairemail;
                $cc = $get_details1->recdetails->rec_email.', '.$get_details1->recdetails->rec_contactemail.', '.$get_details1->recdetails->users->email;
                $bcc = '';
                $subject = $get_details1->recdetails->rec_name.'| Level '.$get_details1->level_id.' - '.$get_details1->documenttypes->document_types_name;
                $body = $body;
                emaillogs($sender, $recdetails_id, $from, $to, $cc, $bcc, $subject, $body);

                // Emaillogs::create([
                //         'e_sender_user_id' => auth::id(),
                //         'e_recdetails_id' => $get_details1->recdetails_id,
                //         'e_from' => auth::user()->email,
                //         'e_to' => $get_details1->recdetails->recchairs->rec_chairemail,
                //         'e_cc' => $get_details1->recdetails->rec_email.', '.$get_details1->recdetails->rec_contactemail.', '.$get_details1->recdetails->users->email,
                //         'e_bcc' => '',
                //         'e_subject' => $get_details1->recdetails->rec_name.'| Level '.$get_details1->level_id.' - '.$get_details1->documenttypes->document_types_name,
                //         'e_body' => $body,
                //         'e_created_at' => Carbon::now()
                // ]);
        Recactionplancsa::create([
                        'ap_csa_ap_id' => $data['sap_id'],
                        'ap_csa_recdetails_id' => $get_details1->recdetails_id,
                        'ap_csa_sender_id' => auth::id(),
                        'ap_csa_rec_id' => $get_details1->recdetails->users->id,
                        'ap_csa_datecreated_at' => Carbon::now()
        ]);
        Recactionplan::where('id', $data['sap_id'])->update(['ap_submittedto_rec' => Carbon::now()]);
        
        return redirect()->back()->withSuccess($get_details1->documenttypes->document_types_name.' Successfully Submitted to REC Chair: '.$get_details1->recdetails->recchairs->rec_chairname.'.');
    }

    //send A . plan and comp evi for accreditors and csa chairs
    public function sendactionplan(Request $request){
        $x = $this->validate($request,[
            'ap_recactionplan_name.*' => 'mimes:pdf,docx,doc',
        ]);
      
        $data = $request->only('ap_recdetails_id','ap_document_types','ap_recactionplan_name');

        if($request->hasFile('ap_recactionplan_name')){
            $ap_recdetails_id = $request->input('ap_recdetails_id');
            $ap_sender_id = auth::id();
           
            foreach($request->ap_recactionplan_name as $file){
                $unique_filename = uniqid();
                $filename = $file->getClientOriginalName();
                $file->storeAs('public/actionplansandcomplianceevidences/'.Auth::user()->id.'/'.$ap_recdetails_id, $unique_filename.'-'.$filename);
                $fileModel = new Recactionplan;
                $fileModel->ap_recdetails_id = $ap_recdetails_id;
                $fileModel->ap_sender_id = $ap_sender_id;
                $fileModel->ap_document_types = $data['ap_document_types'];
                $fileModel->ap_recactionplan_name = $filename;
                $fileModel->ap_recactionplan_uniqid = $unique_filename;
                $fileModel->ap_status = Carbon::now();
                $fileModel->save();
                
               // $get_details1 = Recdetails::join('applicationlevel', 'applicationlevel.recdetails_id', 'recdetails.id')->find($data['ap_recdetails_id']);

               $get_details1 = Recdetails::find($data['ap_recdetails_id']);

               $get_doctype = DocumentTypes::find($data['ap_document_types']);

               $get_names = User::join('role_users', 'role_users.user_id', 'users.id')->join('roles', 'roles.id', 'role_users.role_id')->find(auth::id());

               $accreditorsemail = Sendtoaccreditor::leftjoin('users', 'users.id', 'sendtoaccreditors.a_accreditors_user_id')->where('sendtoaccreditors.a_recdetails_id', $data['ap_recdetails_id'])->get();

               $emailTemplate = EmailTemplate::find(15);
               $body = str_replace(['$date', '$role_name', '$name', '$doctype_name', '$rec_name'], [Carbon::today()->format('F j, Y'), $get_names->role_name, $get_names->name, $get_doctype->document_types_name, $get_details1->rec_name], $emailTemplate->email_body);

               Mail::send('email.email_template', compact('body','get_details1'), function($message) use ($get_details1,$file,$filename,$get_doctype,$accreditorsemail,$body){

                    $message->to('jrcambonga@pchrd.gov.ph'); 
                    $message->subject($get_details1->rec_name." - ".$get_doctype->document_types_name);
                    $message->attach($file->getRealPath(), ['as' => $filename]);

                    foreach($accreditorsemail as $ae){
                    $message->cc($ae->email);

                    $sender = auth::id();
                    $recdetails_id = $get_details1->applicationlevel->recdetails_id;
                    $from = auth::user()->email;
                    $to = 'PHREB Accreditation System - Ethics';
                    $cc = $ae->email;
                    $bcc = '';
                    $subject = $get_details1->rec_name." - ".$get_doctype->document_types_name;
                    $body = $body;
                    emaillogs($sender, $recdetails_id, $from, $to, $cc, $bcc, $subject, $body);

                    // $emaillogs = Emaillogs::create([
                    //     'e_sender_user_id' => auth::id(),
                    //     'e_recdetails_id' => $get_details1->recdetails_id,
                    //     'e_from' => auth::user()->email,
                    //     'e_to' => 'PHREB Accreditation System - Ethics',
                    //     'e_cc' => $ae->email,
                    //     'e_bcc' => '',
                    //     'e_subject' => $get_details1->rec_name." - ".$get_doctype->document_types_name,
                    //     'e_body' => $body,
                    //     'e_created_at' => Carbon::now()
                    // ]);
                    }

                });
            }
            return redirect()->back()->withSuccess('Action Plan Successfully Submitted!');
        }
    }
}
