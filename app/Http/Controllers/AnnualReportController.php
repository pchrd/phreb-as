<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\AnnualReportTitle;
use App\Models\AnnualReport;
use DataTables;

class AnnualReportController extends Controller{
    
    public function showannualreport(){
    	$show_annual_title = AnnualReportTitle::orderBy('id', 'desc')->get(); 
    	return view('reporting.show-annual-reporting', compact('show_annual_title'));
    } 

    public function showannualreport_ajax(){
    	$ar_report = AnnualReport::with('recdetails', 'recdetails.status', 'applicationlevel.regions', 'applicationlevel.levels', 'documents', 'applicationlevel.status', 'annualreporttitles');
    	return DataTables::of($ar_report)->addColumn('action', function ($ar_report){
					return '<a href="view-recdetails/'.encrypt($ar_report->recdetails->id).'" title="View" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i></a>  <a href=" ../public/storage/documents/'.$ar_report->documents->users->id.'/'.$ar_report->documents->recdocuments_recdetails_id.'/'.$ar_report->documents->recdocuments_file_generated.'-'.$ar_report->documents->recdocuments_file.'" title="Download" class="btn btn-xs btn-warning"><i class="fa fa-download"></i>
					</a>

					';
				})->make(true);
    }
// ../storage/actionplansandcomplianceevidences/{{$ses->ap_sender_id}}/{{$ses->recdetails->id}}/{{$ses->ap_recactionplan_uniqid}}-{{$ses->ap_recactionplan_name}}
    // <a href="../storage/actionplansandcomplianceevidences/{{$ses->ap_sender_id}}/{{$ses->recdetails->id}}/{{$ses->ap_recactionplan_uniqid}}-{{$ses->ap_recactionplan_name}}" title="Download" class="btn btn-xs btn-warning"><i class="fa fa-download"></i>
				// 	</a>

   
}
