<?php
namespace App\Http\Controllers;
use App\Models\Phrebforms;
use App\Models\PhrebformSubmission;
use App\Models\Phrebforms_template;
use App\Models\Recdetails;
use App\Models\Sendtoaccreditor;
use App\Models\User;
use App\Models\Role_user;
use App\Models\EmailTemplate;
use App\Models\Phrebform_attached_letter;
use App\Models\Phrebform_assessment_rec;
use App\Models\Phrebform_actionplan;
use App\Models\Phrebform_remarks;
use App\Models\Phrebform_assessment_statuses;
use App\Models\Phrebform_emailmessage;
use App\Models\Phrebform_assessment_finalsubmission;
use Carbon\Carbon;
use App\Models\Status;
use PDF;
use Auth;
use Mail;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PhrebformsController extends Controller
{

    public function update(Request $request, Phrebforms $id){
    	$users = User::find(\Auth::id());
    	
    	if($users->role_users->role_id == 3 AND $id->recdetails->user_id == \Auth::id() AND $id->pf_status == null){
    		$id->update(['pf_body1' => $request->editor1]);
    		return redirect()->back()->withSuccess('Data Successfully Updated!');

    	}elseif($id->pf_status != null){
    		
    		if($users->role_users->role_id == 3){
    			return redirect()->back()->withSuccess('Update Failed! You are no longer update this form once you submitted it to PHREB. Thank you!');
    		}else{
    			return redirect()->back()->withSuccess('Update Disabled. Only the REC can Update this!');
    		}
    	}
    }

    public function view($id, $rec_id){

    	$users = \App\Models\User::find(\Auth::id());
    	$pf = Phrebforms::find(decrypt($id));

        // if($pf->pf_status == true AND $users->role_users->role_id == 3){

        //    print "<script>alert('This is to remind you that you are not able to update this form once you have already submitted it to PHREB. Please contact administrator or the secretariat regarding for your changes. Thank You!')</script>";

        // }

        if($pf->recdetails->users->id == Auth::user()->id OR $users->role_users->role_id == 2 OR $users->role_users->role_id == 4){
        
            return view('phrebforms_webpage.view-phrebform', compact('pf','rec_id'));
        
        }
        else{

            return redirect()->back()->withSuccess('Access Denied!');
        }

    }

    public function updatepfcomments(Request $request, Phrebforms $id){
    	$id->update(['pf_comments' => $request->editor2, 'pf_comments_date' => \Carbon\Carbon::now()]);
    	return redirect()->back()->withSuccess('Remarks/Comments successfully updated for this Form!');
    }

    public function createassessment(Request $request, $id){

        $id = Recdetails::find(decrypt($id));

        $find = ['$rec_name','$rec_institution','$rec_address','$rec_email','$rec_telno','$rec_faxno','$rec_contactperson','$rec_contactposition','$rec_contactmobileno','$rec_contactemail', '$rec_dateestablished', '$rec_chairemail', '$rec_chairname', '$rec_chairmobile'];

		$replace = [$id->rec_name, $id->rec_institution, $id->rec_address, $id->rec_email, $id->rec_telno, $id->rec_faxno, $id->rec_contactperson, $id->rec_contactposition, $id->rec_contactmobileno, $id->rec_contactemail, \Carbon\Carbon::createFromFormat('Y-m-d', $id->rec_dateestablished)->format('F j, Y'), $id->recchairs->rec_chairemail, $id->recchairs->rec_chairname, $id->recchairs->rec_chairmobile];

    	if($id->applicationlevel->level_id == 1){

    		$pf = Phrebforms_template::find(9);
    		$body  = str_replace($find, $replace, $pf->pft_body);

    	}elseif($id->applicationlevel->level_id == 2){

    		$pf = Phrebforms_template::find(10);
    		$body  = str_replace($find, $replace, $pf->pft_body);

    	}elseif($id->applicationlevel->level_id == 3){

            $v = Validator::make($request->all(), [
                'pf_finalreportLvl3' => 'required',
            ]);

            if ($v->fails()){
                return redirect()->back()->withSuccess('Create Failed. The Selection of Assessment Forms for Level 3 is required');
            }else{

                $pf = Phrebforms_template::find($request->pf_finalreportLvl3);
                $body = str_replace($find , $replace, $pf->pft_body);
            }
    	}

    	$createPF = Phrebforms::create(['pf_recdetails_id' => $id->id, 'pf_pfid' => $pf->id, 'pf_user_id' => Auth::id(), 'pf_body1' => $body]);

        if(auth::user()->can('secretariat-access')){
            $createPF->update(['pf_status' => 2]);
        }

        return redirect()->route('view-assessment-form', ['id' => encrypt($createPF->id)])->withSuccess('REC Assessment has successfully created, you can now fill up the form below.');
    }

    public function viewassessment($encrypted_id){
        
        $id = Phrebforms::findOrFail(decrypt($encrypted_id));

        $getAttachedActionPlan = Phrebform_actionplan::where('ap_pft_id', 12)->where('ap_pf_id', decrypt($encrypted_id))->orderBy('id', 'desc')->get();

        $selectCSAChair = User::with('role_users')->whereHas('role_users', function($query){
            $query->where('role_id', 5);
        })->pluck('name', 'id');

    	$accreditor = Sendtoaccreditor::whereIn('a_accreditors_user_id', [auth::id()])->where('sendaccreditors_confirm_decline', '!=', null)->first();
    	
 		$user = User::find(auth::id());

        $validateAssessmentRec = $this->validateAssessmentRec($id->id, $id->pf_recdetails_id)->exists();

        $mcenoneditableAccreditors = $this->mcenoneditableAccreditors($id->id);

        $validateFinalSubmissionForAccreditors = $this->validateFinalSubmissionForAccreditors($id->id);

        $attachedLetter = Phrebform_attached_letter::where('pfal_recdetails_id', $id->pf_recdetails_id)->where('pfal_pf_id', $id->id)->orderBy('id', 'desc')->get();

        if(Auth::user()->role_users->roles->id == 2){
            $pluckActionStatus = Status::whereIn('id', [29, 30, 31])->pluck('status_name', 'id');
        }elseif(Auth::user()->role_users->roles->id == 4 OR Auth::user()->role_users->roles->id == 5){
            $pluckActionStatus = Status::whereIn('id', [28])->pluck('status_name', 'id');
        }

    	if($accreditor == true OR $user->role_users->role_id == 2 or $user->role_users->role_id == 5 or $user->role_users->role_id == 3 AND $id->recdetails->users->id == Auth::user()->id){

    		$history = $id->audits()->where('auditable_id', $id->id)->orderBy('audits.id', 'desc')->with('user')->paginate(5);
    
    		return view('phrebforms_webpage.phrebform-assessment', compact('id', 'history', 'accreditor', 'selectCSAChair', 'attachedLetter', 'validateAssessmentRec', 'user', 'getAttachedActionPlan', 'pluckActionStatus', 'mcenoneditableAccreditors', 'validateFinalSubmissionForAccreditors'));
    	}else{
    		echo 'Access Denied!';
    	}
    	
    }

    public function updateassessment(Request $request, Phrebforms $id){

    	// $accreditor = Sendtoaccreditor::whereIn('a_accreditors_user_id', [auth::id()])->where('sendaccreditors_confirm_decline', '!=', null)->first();

        if($this->validateFinalSubmissionForAccreditors($id->id) == true){

             return redirect()->back()->withSuccess('Access Denied. Save Changes Failed');
        
        }

        if($id->phrebformAssessmentStatus()->exists()){

            if($this->validateRecAssessment($id) == true){

                return redirect()->back()->withSuccess('Updates and Changes to this form is currently disabled');

            }else{
                
                $id->update(['pf_body1' => $request->editor1]);
                return redirect()->back()->withSuccess('Assessment matrix form successfully updated!');
            }


        }else{

            if(!$id->phrebformAssessmentStatus()){

                return redirect()->back()->withSuccess('Updates/Changes Failed. Please Wait for the Accreditors to Submit it to PHREB-Secretariat');

            }

            if($id->pf_user_id == Auth::id() OR $accreditor == true OR Role_user::where('role_id', 5)->get()){
                
                $id->update(['pf_body1' => $request->editor1]);
                return redirect()->back()->withSuccess('Assessment Matrix Form Successfully Updated!');

            }else{

                return redirect()->back()->withSuccess('Access Denied. Save Changes Failed');

            }
            
        }

        if(Auth::user()->role_users->roles->id == 2){
                
            $id->update(['pf_body1' => $request->editor1]);
            return redirect()->back()->withSuccess('Assessment matrix form successfully updated!');

        }
	}

    public function acc_ass_submittocsachair(Request $request, $id){

        try{
            $id = Phrebforms::find($id);

            $getCSACHAIR = User::find($request->ra_csachair_id);

            $assisgnedAccreditors = $this->getAccreditors($id->pf_recdetails_id);

            $emailTemplate = EmailTemplate::find(26);
            
            $body = str_replace(['$csachair_name', '$recname', '$rec_institution', '$level', '$accreditor_name', '$link', '$secretariat'], [$getCSACHAIR->name, $id->recdetails->rec_name, $id->recdetails->rec_institution, $id->recdetails->applicationlevel->levels->level_name, $assisgnedAccreditors, $request->getAccreditorsAssessmentURL, Auth::user()->name], $emailTemplate->email_body);
                     
                Mail::send('email.email_template1', compact('body'), function($message) use ($id, $emailTemplate, $getCSACHAIR){
                    $message->to($getCSACHAIR->email)->bcc('jrcambonga@pchrd.dost.gov.ph');
                    $message->subject($emailTemplate->email_subject);
            });
            
            if(PhrebformSubmission::where('ps_recdetails_id', $id->recdetails->id)->where('ps_csachair_id',  $request->ra_csachair_id)->where('ps_pfid', $id->id)->exists()){

                 return redirect()->back()->withSuccess('Email sent. But you already sent this to CSA-Chair');
            }else{

                $PhrebformSubmission = PhrebformSubmission::create([
                'ps_recdetails_id' => $id->recdetails->id,
                'ps_pfid' => $id->id,
                'ps_phrebforms_id' => $id->pf_pfid,
                'ps_sender_id' => Auth::user()->id,
                'ps_csachair_id' => $request->ra_csachair_id,
                'ps_link' => $request->getAccreditorsAssessmentURL,
            ]);

            }

            return redirect()->back()->withSuccess('Successfully Submitted to CSA-Chair '.$getCSACHAIR->name);
        
        }catch(Exeption $e){
            report($e);
            return back()->withError('Sending Failed. Please contact administrator.')->withInput();        }
    }


    //this is for letter------------
    public function create_a_letter(Request $request){

        $phrebformName = Phrebforms::find(decrypt($request->pfID));
        $pfTemplate = Phrebforms_template::find(11);
        $getCSACHAIR = Role_user::where('role_id', 5)->first();

        $body = str_replace(['$date', '$chairname', '$institution', '$recname', '$actionplandeadline', '$levelname', '$secretariatname', '$csachairname'], [Carbon::now()->format('F j, Y'), $phrebformName->recdetails->recchairs->rec_chairname, $phrebformName->recdetails->rec_institution, $phrebformName->recdetails->rec_name, Carbon::now()->addMonths(6)->format('F j, Y'), $phrebformName->recdetails->applicationlevel->levels->level_name, Auth::user()->name, $getCSACHAIR->users->name], $pfTemplate->pft_body);

        return view('phrebforms_webpage.phrebform_letters', compact('phrebformName', 'body', 'pfTemplate'));
    }

    public function save_a_letter(Request $request){

        $letter = Phrebform_attached_letter::where('pfal_pf_id', $request->pfID)->first();

        if($letter){
            $alert = "<script>alert('Create Failed! Duplicate Letter Detected.')</script>";
            return $alert.redirect()->back()->withErrors($validator->errors());
        }else{
           $create = Phrebform_attached_letter::create([
                'pfal_recdetails_id' => decrypt($request->recID),
                'pfal_pf_id' => decrypt($request->pfID),
                'pfal_pft_id' => $request->pfTemplate,
                'pfal_users_id' => Auth::user()->id,
                'pfal_body' => $request->editor1,
            ]); 

           return redirect()->route('view_a_letter', ['id' => encrypt($create->id)]);
        }
    }

    public function view_a_letter($id){
        $viewLetter = Phrebform_attached_letter::findOrFail(decrypt($id));
        return view('phrebforms_webpage.phrebform_letters_view', compact('viewLetter'));
    }

    public function update_a_letter(Request $request, $id){
        $updateLetter = Phrebform_attached_letter::findOrFail(decrypt($id));
        $updateLetter->update(['pfal_body' => $request->editor1]);
        return redirect()->back()->withSuccess('Data successfully updated!');
    }

    public function sendtorec_a_letter($request, $id){
        $getLetter = Phrebform_attached_letter::findOrFail(decrypt($id));

        $pdf = PDF::loadview('email.email_sendrecassessment', compact('getLetter'));
        
        $file = $pdf->download();
        $filename = $getLetter->phrebforms_templates->pft_name.'-'.$getLetter->phrebforms->recdetails->rec_institution.'-'.$getLetter->phrebforms->recdetails->rec_name.'-'.$getLetter->phrebforms->recdetails->applicationlevel->levels->level_name.'-'.time().'.pdf';

        $path = 'public/letters/'.$filename;

        \Storage::put($path, $file);

        Mail::raw(strip_tags($request), function($message) use($getLetter, $pdf, $path, $filename){
            $message->subject($getLetter->phrebforms_templates->pft_name);
            $message->to([$getLetter->phrebforms->recdetails->rec_email, $getLetter->phrebforms->recdetails->rec_contactemail, $getLetter->phrebforms->recdetails->users->email])
                //->cc('ethics.secretariat@pchrd.dost.gov.ph')
            ->bcc('jrcambonga@pchrd.dost.gov.ph');
            $message->attachData($pdf->output(), $filename);
        });

        $getLetter->update(['pfal_status' =>Carbon::now()]);
        //return redirect()->back()->withSuccess('You have successfully sent this letter to REC!');
        return;
    }

    public function sendassessmenttorec($pfID, $recID){
        
        try{

        $getData = Phrebforms::where('id', $pfID)->where('pf_recdetails_id', $recID)->first();

        $emailTemplate = EmailTemplate::find(27);

        $link = config("app.url")."/view-assessment-form/".encrypt($getData->id);
        
        $body = str_replace(['$recchair', '$recname', '$rec_institution', '$level', '$link', '$secretariat'], [$getData->recdetails->recchairs->rec_chairname, $getData->recdetails->rec_name, $getData->recdetails->rec_institution, $getData->recdetails->applicationlevel->levels->level_name, $link, Auth::user()->name], $emailTemplate->email_body);

         if($this->validateAssessmentRec($pfID, $recID)->exists()){
            $alert = "<script>alert('Submission Failed. You already submitted this to the REC!')</script>";
            return $alert.redirect()->back();
        
        }else{

            $PhrebformSubmission = Phrebform_assessment_rec::create([
                'pfar_recdetails_id' => $recID,
                'pfar_pf_id' => $pfID,
                'pfar_sender_id' => Auth::user()->id,
            ]);

            Mail::send('email.email_template1', compact('body'), function($message) use ($getData, $emailTemplate){
                $message->to([$getData->recdetails->rec_email, $getData->recdetails->rec_contactemail, $getData->recdetails->recchairs->rec_chairemail])->bcc('jrcambonga@pchrd.dost.gov.ph');
                $message->subject($emailTemplate->email_subject);
            });

        }

        return redirect()->back()->withSuccess('Successfully Submitted to REC');
        
        }catch(Exeption $e){
            report($e);
            return back()->withError('Sending Failed. Please contact administrator.')->withInput();   
        }
    }

    public function assessmentactions(Request $request, $pfID, $recID){

        $validator = Validator::make($request->all(), [
            'assessmentaction_status' => 'required|max:255',
        ]);

        if($validator->fails()) {
            return redirect()->back()->withSuccess('Action Selection is Required');
        }

        $id = Phrebforms::find($pfID);

        $link = config("app.url")."/view-assessment-form/".encrypt($id->id);

        $assisgnedAccreditors = $this->getAccreditors($id->pf_recdetails_id);
        
        $getAssignedAccreditors = Sendtoaccreditor::whereIn('a_recdetails_id', [$id->pf_recdetails_id])->get();

        $final = Phrebform_assessment_finalsubmission::whereIn('pfa_fs_pfid', [$pfID])->where('pfa_fs_mark_as_final', null)->get();

        if($id->phrebformAssessmentStatus()->exists()){

            if(Auth::user()->can('secretariat-access')){

                if($id->phrebformAssessmentStatus_getLatest()->statuses->id == 30 OR $id->phrebformAssessmentStatus_getLatest()->statuses->id == 29){

                    return redirect()->back()->withSuccess('Action Failed. Please wait for the Accreditors/CSA-Chairs to submit the Assessment Form');    
                }
            }

            if(Auth::user()->can('accreditors-access')){

                if($id->phrebformAssessmentStatus_getLatest()->statuses->id == 30 OR $id->phrebformAssessmentStatus_getLatest()->statuses->id == 29 OR $id->phrebformAssessmentStatus_getLatest()->statuses->id == 28){

                    return redirect()->back()->withSuccess('Action Failed. Please wait for the PHREB-Secretariat return the Assessment Form');    
                }
            }

            if(Auth::user()->can('csachair-access')){

                if($id->phrebformAssessmentStatus_getLatest()->statuses->id == 28 OR $id->phrebformAssessmentStatus_getLatest()->statuses->id == 30){

                    return redirect()->back()->withSuccess('Action Failed. Please wait for the PHREB-Secretariat/Accreditors submit the Assessment Form');    
                }
            }
        }

       
        if($request->assessmentaction_status == 28 AND Auth::user()->role_users->roles->id == 4 OR Auth::user()->role_users->roles->id == 5){

        $emailTemplate = EmailTemplate::find(33);
        $body = str_replace(['$accreditorName', '$phrebformTemplate', '$recname', '$link', '$assessmentMessage'], [Auth::user()->role_users->roles->role_name.' - '.Auth::user()->name, $id->phrebforms_template->pft_name, $id->recdetails->rec_name.'-'.$id->recdetails->applicationlevel->levels->level_name, $link, $request->assessmentMessage], $emailTemplate->email_body);

        Mail::send('email.email_template1', compact('body'), function($message) use ($emailTemplate){
                $message->to('jrcambonga@pchrd.dost.gov.ph');
                $message->subject($emailTemplate->email_subject);
        });

        $create = Phrebform_assessment_statuses::create([
                    'pfas_status_pfid' => $pfID,
                    'pfas_status_sender_id' => Auth::user()->id,
                    'pfas_status_status_id' => $request->assessmentaction_status,
        ]);

        }elseif($request->assessmentaction_status == 29){

            $validator = Validator::make($request->all(), [
                'ra_csachair_id' => 'required|max:255',
            ]);

           if($validator->fails()) {
                
                return redirect()->back()->withSuccess('CSA-Chair Selection is Required');
           
           }else{           

                if($final->count()){
                   
                    return redirect()->back()->withSuccess('Action Submission Failed. Please complete the Accreditors Mark as Done to forward this in CSA-Chair.');
                }

                $create = Phrebform_assessment_statuses::create([
                    'pfas_status_pfid' => $pfID,
                    'pfas_status_sender_id' => Auth::user()->id,
                    'pfas_status_status_id' => $request->assessmentaction_status,
                ]);

                $emailmessage = $create->phrebform_emailmessages()->create([
                    'pfem_sender_id' => Auth::user()->id,
                    'pfem_pfid_status_id' => $request->assessmentaction_status,
                    'pfem_emailmessage' => $request->assessmentMessage
                ]);

                $sendtofinal = $create->sendtofinalassessment()->create([
                    'pfa_fs_pf_status_id' => $request->assessmentaction_status,
                    'pfa_fs_to_id' => $request->ra_csachair_id, 
                    'pfa_fs_sender_id' => Auth::user()->id,
                ]);

                $this->acc_ass_submittocsachair($request, $pfID);

            }

        }elseif($request->assessmentaction_status == 30){

            if($final->count()){
               
                return redirect()->back()->withSuccess('Action Submission Failed. Please complete the PHREB CSA-Chair Mark as Done or Submit in this form to forward this in REC.');
            }

                    $create = Phrebform_assessment_statuses::create([
                        'pfas_status_pfid' => $pfID,
                        'pfas_status_sender_id' => Auth::user()->id,
                        'pfas_status_status_id' => $request->assessmentaction_status,
                    ]);

                    $emailmessage = $create->phrebform_emailmessages()->create([
                        'pfem_sender_id' => Auth::user()->id,
                        'pfem_pfid_status_id' => $request->assessmentaction_status,
                        'pfem_emailmessage' => $request->assessmentMessage
                    ]);   
            
            $this->sendassessmenttorec($pfID, $recID);
            $this->sendtorec_a_letter($request->assessmentMessage, encrypt($id->phrebform_attached_letter()->id));
        
        }elseif($request->assessmentaction_status == 31){

             foreach ($getAssignedAccreditors as $key => $getAccreditors) {

                $value[] = $getAccreditors->users->email;

                    $create = Phrebform_assessment_statuses::create([
                        'pfas_status_pfid' => $pfID,
                        'pfas_status_sender_id' => Auth::user()->id,
                        'pfas_status_status_id' => $request->assessmentaction_status,
                    ]);

                    $emailmessage = $create->phrebform_emailmessages()->create([
                        'pfem_sender_id' => Auth::user()->id,
                        'pfem_pfid_status_id' => $request->assessmentaction_status,
                        'pfem_emailmessage' => $request->assessmentMessage
                    ]);

                    $sendtofinal = $create->sendtofinalassessment()->create([
                        'pfa_fs_pf_status_id' => $request->assessmentaction_status,
                        'pfa_fs_to_id' => $getAccreditors->users->id, 
                        'pfa_fs_sender_id' => Auth::user()->id,
                    ]);

             }
           
            $emailTemplate = EmailTemplate::find(34);
            $body = str_replace(['$accreditors', '$name', '$phrebformTemplate', '$recname', '$link', '$assessmentMessage'], [$assisgnedAccreditors, Auth::user()->role_users->roles->role_name.' - '.Auth::user()->name, $id->phrebforms_template->pft_name, $id->recdetails->rec_institution.'-'.$id->recdetails->rec_name.'-'.$id->recdetails->applicationlevel->levels->level_name, $link, $request->assessmentMessage], $emailTemplate->email_body);

            Mail::send('email.email_template1', compact('body'), function($message) use ($value, $emailTemplate){
                    $message->to($value);
                    $message->subject($emailTemplate->email_subject);
            });

        }

        return redirect()->back()->withSuccess('Action Successfully Submitted!');
    
    }

    //PHREB FORM SUBMISSION PAGE
    public function show_phrebformsubmission(){   
        
        return view('phrebforms_webpage.phrebform_submissions');
    }

    public function show_phrebformsubmission_ajax(){

        $dataPFS = PhrebformSubmission::orderBy('id', 'desc')
        ->where('ps_csachair_id', Auth::user()->id)
        ->with(['recdetails', 'sender'])
        ->get();

        return DataTables::of($dataPFS)
        ->addColumn('action', function ($dataPFS){
               return '<a href="'.$dataPFS->ps_link.'" target="_blank" title="Go to this Phreb form" class="btn btn-xs btn-success"><i class="fa fa-arrow-circle-o-right"></i></a>';
        })
        ->editColumn('phrebforms', function ($dataPFS) {
               return $dataPFS->phrebformsTemplate->pft_name;
        })
        ->editColumn('accreditors', function ($dataPFS) {
               return $this->getAccreditors($dataPFS->ps_recdetails_id);
        })
        ->make(true);
    }

    public function phrebformRemarks(Request $request, Phrebforms $id){

        $create = $id->phrebform_remarks()->create([
            'pf_remarks_recdetails_id' => $id->recdetails->id,
            'pf_remarks_user_id' => Auth::user()->id,
            'pf_remarks_body' => $request->editor2,
        ]);
        return redirect()->back()->withSuccess('Remarks/Comments Successfully Added!');
    }

    public function updatephrebformRemarks(Request $request, Phrebform_remarks $id){

        $update = $id->update([
            'pf_remarks_body' => $request->editor1,
        ]);
        return redirect()->back()->withSuccess('Remarks/Comments Successfully Updated!');
    }

    public function viewPhrebformRemarks($id){

        $view = Phrebform_remarks::find(decrypt($id));
        return view('phrebforms_webpage.phrebform_viewPhrebformRemarks', compact('view'));
    }

    public function markasdone($id){

        $mark = Phrebform_assessment_finalsubmission::whereIn('pfa_fs_pfid', [$id])->whereIn('pfa_fs_to_id', [Auth::user()->id])->first();

        $mark->update(['pfa_fs_mark_as_final' => Carbon::now()]);

        if(Auth::user()->can('csachair-access')){

            $create = Phrebform_assessment_statuses::create([
                'pfas_status_pfid' => $id,
                'pfas_status_sender_id' => auth::user()->id,
                'pfas_status_status_id' => 28,
            ]);

        }

        return redirect()->back()->withSuccess('You have successfully marked this form as Done & Ready to Submit!');
    }


// --------------------------------------------------------------------------//helpers
    public function getAccreditors($param){
        $getAssignedAccreditors = Sendtoaccreditor::whereIn('a_recdetails_id', [$param])->get();

        foreach ($getAssignedAccreditors as $key => $getAccreditors) {
            $values[] = $getAccreditors->users->name;
        }
        return $assisgnedAccreditors = implode(', ', $values);   
    }

    public function validateAssessmentRec($pfID, $recID){
        $validate = Phrebform_assessment_rec::where('pfar_pf_id', $pfID)->where('pfar_recdetails_id',  $recID); 
        return $validate;
    }

    public function validateRecAssessment($id){

        $accreditor = Sendtoaccreditor::whereIn('a_accreditors_user_id', [auth::id()])->where('sendaccreditors_confirm_decline', '!=', null)->first();

        if(Auth::user()->can('accreditors-access')){
            
            if($accreditor == true AND $id->phrebformAssessmentStatus_getLatest()->statuses->id == 28 OR $id->phrebformAssessmentStatus_getLatest()->statuses->id == 29 OR $id->phrebformAssessmentStatus_getLatest()->statuses->id == 30){
            
                $alert = "<script>alert('Update Failed. You already sent it to PHREB-Secretariat!')</script>";
                return $alert.redirect()->back();
            }
        }

        if(Auth::user()->can('secretariat-access')){
            
            if($id->phrebformAssessmentStatus_getLatest()->statuses->id == 29 OR $id->phrebformAssessmentStatus_getLatest()->statuses->id == 30 OR $id->phrebformAssessmentStatus_getLatest()->statuses->id == 31){

                $alert = "<script>alert('Update Failed. You already sent it to Accreditor or PHREB CSA-Chair or REC.')</script>";
                return $alert.redirect()->back();

            }
        }

        if(Auth::user()->can('csachair-access')){
            
            if($id->phrebformAssessmentStatus_getLatest()->statuses->id == 28 OR $id->phrebformAssessmentStatus_getLatest()->statuses->id == 30 OR $id->phrebformAssessmentStatus_getLatest()->statuses->id == 31){

                $alert = "<script>alert('Update Failed. You already sent it to PHREB CSA-Chair or REC.')</script>";
                return $alert.redirect()->back();

            }
        }

    }

    public function mcenoneditableAccreditors($pfID){

        if(Auth::user()->can('accreditors-access')){
              $validate = Phrebform_assessment_statuses::where('pfas_status_pfid', $pfID)->whereIn('pfas_status_status_id', [28,29,30])->latest()->first();
        }

        if(Auth::user()->can('csachair-access')){
             $validate = Phrebform_assessment_statuses::where('pfas_status_pfid', [$pfID])->whereIn('pfas_status_status_id', [28,30,31])->latest()->first();
        }

        if(Auth::user()->can('secretariat-access')){
              $validate = Phrebform_assessment_statuses::where('pfas_status_pfid', $pfID)->whereIn('pfas_status_status_id', [30, 31, 29])->latest()->first();
        }

        if(Auth::user()->can('rec-access')){
              $validate = Phrebform_assessment_statuses::where('pfas_status_pfid', $pfID)->whereIn('pfas_status_status_id', [28, 29, 31])->latest()->first();
        }

        return $validate;
       
    }

    public function validateFinalSubmissionForAccreditors($pfID){
        return $validate = Phrebform_assessment_finalsubmission::where('pfa_fs_pfid', $pfID)->where('pfa_fs_to_id', Auth::user()->id)->where('pfa_fs_mark_as_final', '!=', null)->latest()->first();
    }

}