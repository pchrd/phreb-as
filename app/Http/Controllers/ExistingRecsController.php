<?php
namespace App\Http\Controllers;
use App\Models\Recdetails;
use App\Models\Existing_arps;
use App\Models\Accreditorslist;
use App\Models\DocumentTypes;
use App\Models\Status;
use App\Models\Region;
use App\Models\Level;
use App\Models\Reclist;
use Illuminate\Http\Request;

class ExistingRecsController extends Controller
{
    public function showrecords($id){
    	
    	$selectstatuses=Status::orderBy('id')->whereIn('statuses.id', [2,3,4,9,5,11,17,19])->pluck('status_name', 'id');
    	$selectapptype=Status::orderBy('id')->whereIn('statuses.id', [12,13,14])->pluck('status_name', 'id');
    	$selectlevels=Level::orderBy('level_name')->where('levels.id', '<=', 3)->pluck('level_name','id');
    	$selectacc=Accreditorslist::orderBy('id')->pluck('accreditors_name', 'id');
		$selectregions=Region::orderBy('region_name')->pluck('region_name','id');
		$selectaccreditation=DocumentTypes::orderby('id')->whereIn('id', [21,22,23,24,25])->pluck('document_types_name', 'id');
		$recnamelists=Reclist::orderby('id', 'asc')->pluck('reclist_name', 'id');
    	$recdetails = Recdetails::find(decrypt($id));
    	
    	return view('recdetails.existing_recs', compact('recdetails', 'selectapptype', 'selectregions', 'selectlevels', 'selectstatuses', 'selectacc', 'selectaccreditation', 'recnamelists'));
    }

    public function updaterecords(Request $request, $id){

    	$data = array(
    		'statuses_id' => $request->input('status'),
			'rec_apptype_id' => $request->input('selectapptype'),
			'rec_name' => implode('', (array)$request->input('rec_name')),
			'level_id' => $request->input('selectlevel'),
			'region_id' => $request->input('rec_region'),
			'rec_institution' => $request->input('rec_institution'),
			'rec_address' => $request->input('rec_address'),
			'rec_email' => $request->input('rec_email'),
			'rec_telno' => $request->input('rec_telno'),
			'rec_faxno' => $request->input('rec_faxno'),
			'rec_chairname' => $request->input('rec_chairname'),
			'rec_chairemail' => $request->input('rec_chairemail'),
			'rec_chairmobile' => $request->input('rec_chairmobile'),
			'rec_contactperson' => $request->input('rec_contactperson'),
			'rec_contactposition' => $request->input('rec_contactposition'),
			'rec_contactmobileno' => $request->input('rec_contactmobileno'),
			'rec_contactemail' => $request->input('rec_contactemail'),
			'rec_dateestablished' => $request->input('rec_dateestablished'),

			'date_completed' => $request->input('date_completed'),
			'date_accreditation' => $request->input('date_accreditation'),
			'date_accreditation_expiry' => $request->input('date_accreditation_expiry'),
			'accreditation_number' => $request->input('accreditation_number'),
			'rec_accreditation' => implode('', (array)$request->input('rec_accreditation')),
			
			'name_of_accreditors' => implode(', ', (array)$request->input('name_of_accreditors')),
			'date_req_deck_accreditors' => $request->input('date_req_deck_accreditors'),
			'date_rec_received_awarding_letter' => $request->input('date_rec_received_awarding_letter'),
			'date_deadline_accreditors_ass' => $request->input('date_deadline_accreditors_ass'),
			'date_accreditors_send_ass' => $request->input('date_accreditors_send_ass'),
			'date_accreditation_visit_finalreport' => $request->input('date_accreditation_visit_finalreport'),
			'date_rec_recieved_acc_ass' => $request->input('date_rec_recieved_acc_ass'),
			'date_rec_sent_apce' => $request->input('date_rec_sent_apce'),
			'date_accreditors_sent_apce' => $request->input('date_accreditors_sent_apce'),
			'date_forward_to_csachair' => $request->input('date_forward_to_csachair'),
			'existingrec_remarks' => $request->input('existingrec_remarks'),
		);

		Recdetails::where('recdetails.id', '=', decrypt($id))->join('applicationlevel', 'applicationlevel.recdetails_id', '=', 'recdetails.id')->join('recchairs', 'recdetails.id', '=', 'recchairs.recdetails_id')->join('existing_recs', 'existing_recs.recdetails_id', 'recdetails.id')->update($data);
		return redirect()->back()->withSuccess('Records Successfully Updated!');
    }
}
