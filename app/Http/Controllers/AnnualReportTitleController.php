<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\AnnualReportTitle;
use Carbon\Carbon;

class AnnualReportTitleController extends Controller{

    public function add_ar(Request $request){
    	AnnualReportTitle::create([
    		'ar_name' => $request->input('ar_name'),
    		'ar_desc' => $request->input('ar_desc'),
    		'ar_start_year' => $request->input('ar_start'),
    		'ar_end_year' => $request->input('ar_end'),
    	]);
    	return redirect()->back()->withSuccess('Annual Report Title Added Successfully');
    }

    public function art_delete($id){
    	AnnualReportTitle::find($id)->delete();
    	return redirect()->back()->withSuccess('Annual Report Title Deleted Successfully');
    }
}
