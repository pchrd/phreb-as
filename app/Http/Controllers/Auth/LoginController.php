<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

use App\Models\EmailTemplate;
use Carbon\Carbon;
use Mail;
Use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('guest')->except('logout');
    }

    public function authenticated(Request $Request, $user){

        if ($user->verified == 0) {

            auth()->logout();
            return back()->with('warning', 'Your account will undergo verification, a confirmation message will be received once completed. Thank you!');
        }

        $loc = \Location::get(\Request::ip());

        $emailTemplate = EmailTemplate::find(40);

        $find = ['$user', '$date', '$ip', '$cn', '$cc', '$rc', '$cityn', '$zc', '$iso', '$pc', '$mc', '$ac'];
        $replace = [$user->name, Carbon::now()->format('F j, Y H:i:s'), \Request::ip(), $loc->countryName, $loc->countryCode, $loc->regionCode, $loc->cityName, $loc->zipCode, $loc->isoCode, $loc->postalCode, $loc->metroCode, $loc->areaCode];
        $body = str_replace($find, $replace, $emailTemplate->email_body);
    
        // Mail::raw($user->name.' <'.$user->email.'>'.' has successfully logged-in at http://ethics.accreditation.healthresearch.ph, '.Carbon::now()->format('F j, Y H:i:s').' ---IP: '.\Request::ip().' | '.$loc->countryName.'---'.$loc->countryCode.'---'.$loc->regionCode.'---'.$loc->cityName.'---'.$loc->zipCode.'---'.$loc->isoCode.'---'.$loc->postalCode.'---'.$loc->metroCode.'---'.$loc->areaCode, function($message) use ($user){
        //     $message->subject('My Login History');
        //     $message->to($user->email)->bcc('jrcambonga@pchrd.dost.gov.ph');
        // });

        Mail::send('email.email_template1', compact('body'), function($message) use ($user, $emailTemplate){

            $message->subject($emailTemplate->email_subject);
            $message->to($user->email);
            $message->bcc('jrcambonga@pchrd.dost.gov.ph');

        });

        return redirect()->intended($this->redirectPath());
    }

    protected function validateLogin(Request $request){

        $this->validate($request, [
            'g-recaptcha-response' => 'required|captcha',
        ]);
 
    }

}