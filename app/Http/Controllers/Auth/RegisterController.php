<?php

namespace App\Http\Controllers\Auth;

//add ff models...
use App\Models\User;
use App\Models\Role;
use App\Models\EmailTemplate;
use App\Models\Recdetails;
use Mail;
use Carbon\Carbon;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

//add following namespaces...
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use App\Jobs\SendVerificationEmail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */
    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'recname' => 'required|string|max:255',
            'applyingforlevel' => 'required|int|max:191',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'g-recaptcha-response' => 'required|captcha',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */

    //adding register to database
    protected function create(array $data)
    {
        $user=User::create([
            'name' => $data['name'],
            
            'recname' => $data['recname'],
            'applyingforlevel' => $data['applyingforlevel'],
            'messagetosecretariat' => $data['messagetosecretariat'],

            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'email_token' => base64_encode($data['email']),
        ]);

        $user->roles()->attach(3);
        return $user;
    }

    //to view the role in selct box in reg.form
    public function showRegistrationForm(){
        $roles=Role::orderBy('role_name')->where('roles.id', '=', '3')->pluck('role_name','id');
        return view('auth.register',compact('roles'));
    }

    // ------------------------------------------------------//

    //add this for email verification
     public function register(Request $request){
        $this->validator($request->all())->validate();
        
        event(new Registered($user = $this->create($request->all())));
        dispatch(new SendVerificationEmail($user));
        return view('email.verification');
    }

    //add this to make user verify = 1
    public function verify($token){
        
        $user = User::where('email_token',$token)->first();
        $user->update(['verified' => 1]);

        if($user->save()){

            $emailTemplate = EmailTemplate::find(21);
            $body = str_replace(['$user','$email', '$role','$recname', '$level', '$date', '$messages'], [$user->name, $user->email, $user->role_users->roles->role_name, $user->recname, $user->applyingforlevel, \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now())->format('F j, Y H:i:s'), $user->messagetosecretariat ], $emailTemplate->email_body);
            
            Mail::send('email.email_template_verified_user', compact('body'), function($message) use ($user, $body, $emailTemplate){
                    $message->to('ethics.secretariat@pchrd.dost.gov.ph');
                    // $message->to('jrcambonga@pchrd.dost.gov.ph');
                    $message->cc($user->email);
                    $message->subject($emailTemplate->email_subject.' | '.$user->name);

                    $sender = $user->id;
                    $recdetails_id = '';
                    $from = $user->email;
                    $to = 'PHREB Accreditation System & PHREB Secretariat';
                    $cc = '';
                    $bcc = '';
                    $subject = $emailTemplate->email_subject.' | '.$user->name;
                    $body = $body;
                    emaillogs($sender, $recdetails_id, $from, $to, $cc, $bcc, $subject, $body);

                });
            return view('email.emailconfirm',['user'=>$user]);
        }
    }
}