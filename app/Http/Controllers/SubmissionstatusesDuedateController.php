<?php

namespace App\Http\Controllers;

use App\Models\Submissionstatuses_duedate;
use Illuminate\Http\Request;

class SubmissionstatusesDuedateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Submissionstatuses_duedate  $submissionstatuses_duedate
     * @return \Illuminate\Http\Response
     */
    public function show(Submissionstatuses_duedate $submissionstatuses_duedate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Submissionstatuses_duedate  $submissionstatuses_duedate
     * @return \Illuminate\Http\Response
     */
    public function edit(Submissionstatuses_duedate $submissionstatuses_duedate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Submissionstatuses_duedate  $submissionstatuses_duedate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Submissionstatuses_duedate $submissionstatuses_duedate)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Submissionstatuses_duedate  $submissionstatuses_duedate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Submissionstatuses_duedate $submissionstatuses_duedate)
    {
        //
    }
}
