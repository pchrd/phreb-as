<?php

namespace App\Http\Controllers;
use App\Models\Recdetails;
use Illuminate\Http\Request;
use DataTables;
use Auth;
use DB;

class OngoingController extends Controller
{
    public function show_ongoing_ajax(Recdetails $recdetail){

    	$recdetails_ajax_lib = Recdetails::where('status_name', 'like', '%'.\Request::get('key').'%')->join('applicationlevel', 'applicationlevel.recdetails_id', '=', 'recdetails.id')->join('levels', 'applicationlevel.level_id', '=', 'levels.id')->join('regions', 'applicationlevel.region_id', '=', 'regions.id')->join('statuses', 'applicationlevel.statuses_id', '=', 'statuses.id')->with(['status']);

        $recdetails_ajax = Auth::user()->role_users->user_assigned_region_id == null ? $recdetails_ajax_lib : $recdetails_ajax_lib->where('applicationlevel.region_id', Auth::user()->role_users->user_assigned_region_id);
				    
				return DataTables::of($recdetails_ajax)->editColumn('date_accreditationFORMAT', function ($recdetails_ajax){

					return $recdetails_ajax->date_accreditation != null ? \Carbon\Carbon::createFromFormat('Y-m-d',$recdetails_ajax->date_accreditation)->format('F j, Y') : '---';
            	})->editColumn('date_accreditation_expiryFORMAT', function ($recdetails_ajax) {

            		return $recdetails_ajax->date_accreditation_expiry != null ? \Carbon\Carbon::createFromFormat('Y-m-d',$recdetails_ajax->date_accreditation_expiry)->format('F j, Y') : '---';
            	})->addColumn('rec_change_of_acc', function ($recdetails_ajax){

					return '<a href="edit-record/'.encrypt($recdetails_ajax->recdetails_id).'" title="View Notes" class=""><i class="fa fa-sticky-note-o  text text-danger "> </i>'.str_limit($recdetails_ajax->existingrec_remarks, 50).'</a>';
				})->addColumn('action', function ($recdetail){

					return '<a href="view-recdetails/'.encrypt($recdetail->recdetails_id).' " title="View and Edit" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i></a>';
				})->rawColumns(['rec_change_of_acc', 'action'])
				->make(true);
    }

     public function show_ongoing(){

     	 $count_lib = DB::table('applicationlevel')->where('status_name', 'like', '%'.\Request::get('key').'%')->join('levels', 'levels.id', 'level_id')->join('statuses', 'statuses.id', 'applicationlevel.statuses_id')->select(DB::raw("COUNT(*) as counts, level_name"))->groupBy('level_name');

         $count = Auth::user()->role_users->user_assigned_region_id == null ? $count_lib->get() : $count_lib->where('applicationlevel.region_id', Auth::user()->role_users->user_assigned_region_id)->get();

    	return view('recdetails.on-going-view', compact('count'));
    }

}
