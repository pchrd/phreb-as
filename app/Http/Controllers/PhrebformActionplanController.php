<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Phrebform_actionplan;
use App\Models\Phrebform_assessment_rec;
use App\Models\Phrebforms_template;
use App\Models\Phrebforms;
use App\Models\Phrebform_file;
use App\Models\Phrebform_filefolder;
use App\Models\Phrebform_actionplanfromrec;
use App\Models\EmailTemplate;
use App\Models\Status;
use App\Models\Phrebform_actionplanfromrecsstatus;
use App\Models\User;
use App\Models\Role_user;
use Mail;
use Auth;
use DB;
use Carbon\Carbon;

class PhrebformActionplanController extends Controller{

    public function makeActionplan($id){

    	try{

    		DB::beginTransaction();

    		try{
    			
			    	$data = Phrebform_assessment_rec::find(decrypt($id));
			    	$pf = Phrebforms_template::find(12);

			    	$find = ['$rec-institution', '$rec_name', '$date_recassessment_recieved', '$recchair'];
					$replace = [$data->recdetails->rec_institution, $data->recdetails->rec_name, \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $data->created_at)->format('F j, Y'), $data->recdetails->recchairs->rec_chairname];
					$body = str_replace($find, $replace, $pf->pft_body);

					$createPhrebform = Phrebforms::create([
						'pf_recdetails_id' => $data->recdetails->id,
						'pf_pfid' => $pf->id,
						'pf_user_id' => Auth::user()->id, 
						'pf_body1' => $body,
					]);

					$createPhrebform->phrebformActionplan()->create([
						'ap_recassessment_id' => $data->id,
						'ap_pf_id' => $data->pfar_pf_id,
						'ap_pft_id' => $pf->id,
					]);

			}catch(\Exception $e){

		    	return $e->getMessage();
			}

			DB::commit();

			}catch(\Exception $e){

				DB::rollback();

			}

		return redirect()->back()->withSuccess($createPhrebform->phrebforms_template->pft_name.' Successfully Created. Please see the form below.');

    }

    public function viewActionplan($id){

    	try{

    		$phrebform = Phrebforms::with(['phrebform_filefolders'])->find(decrypt($id));

    		if(Auth::user()->can('secretariat-access')){
    			$getStatuses = Status::orderby('id', 'desc')->whereIn('id', [21,22,23,27])->pluck('status_name', 'id');

    		}elseif('accreditors-access'){
    			$getStatuses = Status::orderby('id', 'desc')->whereIn('id', [24,25,26])->pluck('status_name', 'id');

    		}
    		
    		$actionplanValidation = $this->validateSubmissionofREC($phrebform);

    		$accreditors = $phrebform->recdetails->sendaccreditors()->whereIn('a_accreditors_user_id', [Auth::user()->id])->first();
 		
    		if($phrebform->recdetails->users->id == Auth::user()->id OR Auth::user()->can('csachair_and_secretariat') OR $accreditors){

    			if($phrebform->phrebform_apsubmission_recs()->exists() OR Auth::user()->can('rec-access')){

	    			$pluckFolderName = Phrebform_filefolder::where('pfff_pf_id', decrypt($id))->where('pfff_recdetails_id', $phrebform->recdetails->id)->orderby('id', 'desc')->pluck('pfff_name', 'id');

	    			$history = $phrebform->audits()->where('auditable_id', $phrebform->id)->orderBy('audits.id', 'desc')->with('user')->paginate(5);

	    			return view('phrebforms_webpage.phrebform_view_actionplan', compact('phrebform', 'pluckFolderName', 'history', 'getStatuses', 'actionplanValidation'));
	    		}else{

	    			return redirect()->back()->withSuccess('Access Failed. '.$phrebform->phrebforms_template->pft_name.' is not yet submitted by '.$phrebform->recdetails->rec_institution.'-'.$phrebform->recdetails->rec_name.' '.$phrebform->recdetails->applicationlevel->levels->level_name);
	    		}
    		}else{
    			return redirect()->back()->withSuccess('Access Denied!');
    		}	

    	}catch(\Exception $e){

		    return $e->getMessage();
		}
    }

    public function updateActionplan(Request $request, $id){

    	try{

    		DB::beginTransaction();

    		$phrebform = Phrebforms::find(decrypt($id));

    		$accreditors = $phrebform->recdetails->sendaccreditors()->whereIn('a_accreditors_user_id', [Auth::user()->id])->first();

    		if($this->validateSubmissionofREC($phrebform)){
 
    			$message = "<script>alert('Actions Disabled')</script>";
		    	return $message.redirect()->back()->withSuccess('The ff actions are disabled in this form.');

    		}else{

    			if($phrebform->recdetails->users->id == Auth::user()->id OR $this->validateUserAccess(auth::user()->id) OR $accreditors){
	    			$update = $phrebform->update([
	    				'pf_body1' => $request->editor1,
	    			]);
	    		}else{
	    			return redirect()->back()->withSuccess('Access Denied!');
	    		}
    		}

    		DB::commit();

		}catch(\Exception $e){

			DB::rollback();
		}
		return redirect()->back()->withSuccess('Data Successfully Updated!');
    }


    public function Pf_addFiles(Request $request, $id){
    	
    	try{

    		DB::beginTransaction();

    		$phrebform = Phrebforms::find(decrypt($id));

    		if($this->validateSubmissionofREC($phrebform)){

    			$message = "<script>alert('Submission Failed. You already sent this to PHREB')</script>";
		    	return $message.redirect()->back()->withSuccess('You already sent this to PHREB');
    		}

    		$folderId = Phrebform_filefolder::where('pfff_name', $request->apFolder)->first();

    		$validator = \Validator::make($request->only(['apFolder', 'apFile']),[
               'apFile' => 'required',
            ]);

            if ($validator->fails()) {
                    return redirect()->back()->withSuccess('Upload Failed')->withErrors($validator)->withInput();
            }

            if($folderId){
            

            }else{

            	 $folder = $phrebform->phrebform_filefolders()->create([
                		'pfff_name' => $request->apFolder,
                		'pfff_recdetails_id' => $phrebform->recdetails->id,
                		'pfff_user_id' => Auth::user()->id,
                	]);
            	
            }
           
                if($request->hasFile('apFile')){

	                foreach($request->apFile as $file){

	             	  $unique_filename = uniqid();
	                  $file->storeAs('public/phrebformFiles/'.Auth::user()->id.'/'.$phrebform->recdetails->id.'/'.$phrebform->id, $unique_filename.'-'.$file->getClientOriginalName().'.'.$file->getClientOriginalExtension());

	                  $fileModel = new Phrebform_file;
	                  $fileModel->pf_files_pfid = $phrebform->id;

	                  if($folderId){
	                  	$fileModel->pf_files_pfff_id = $folderId->id;
	                  }else{
	                  	$fileModel->pf_files_pfff_id = $folder->id;
	                  }
	                  	
	                  $fileModel->pf_files_addedby = Auth::user()->id;
	                  $fileModel->pf_files_originalfilename = $file->getClientOriginalName();
	                  $fileModel->pf_files_originalfileextension = $file->getClientOriginalExtension();
	                  $fileModel->pf_files_originalfilesize = $file->getSize();
	                  $fileModel->pf_files_uniqid = $unique_filename;
	                  $fileModel->save();
	                } 
              	}else{
              		echo 'Upload Failed';
              	}

    		DB::commit();
    		return redirect()->back()->withSuccess('Files Uploaded Successfully');

		}catch(\Exception $e){

			DB::rollback();
			return redirect()->back()->withSuccess('Upload Failed. Error Detected!');

		}
    }

    public function updateFolder(Request $request, $id){

    	$filefolder = Phrebform_filefolder::find(decrypt($id));
    	$phrebform = Phrebforms::find($filefolder->pfff_pf_id);

    	if($this->validateSubmissionofREC($phrebform)){

    		$message = "<script>alert('Submission Failed. You already sent this to PHREB')</script>";

		    return $message.redirect()->back()->withSuccess('You already sent this to PHREB');

    	}else{

    		if($phrebform->recdetails->users->id == Auth::user()->id){
	    	   	
	    	   	$update = $filefolder->update([
	    			'pfff_name' => $request->apFolder,
	    	   	]);

    	   	}else{

    	   		return redirect()->back()->withSuccess('Access Denied!');
    	   	}

    		return redirect()->back()->withSuccess('Folder Name Successfully Saved!');	
    	}
    	
    }

    public function deleteFolder($id){

    	$filefolder = Phrebform_filefolder::find(decrypt($id));

    	if($this->validateSubmissionofREC(Phrebforms::find($filefolder->pfff_pf_id))){

    		$message = "<script>alert('Submission Failed. You already sent this to PHREB')</script>";

		    return $message.redirect()->back()->withSuccess('You already sent this to PHREB');

    	}else{

    		if($phrebform->recdetails->users->id == Auth::user()->id){
	    		
	    		$delete = $filefolder->delete();

	    		return redirect()->back()->withSuccess('Folder Name Successfully Deleted!');
	    	}else{

	    		return redirect()->back()->withSuccess('Access Denied!');
	    	}
    	}
    }

    public function viewDocuments($id){

    	$actionplan_document = Phrebform_filefolder::find(decrypt($id));
    	$units = [' B', ' KB', ' MB']; 

    	if($actionplan_document->users->id == Auth::user()->id OR $this->validateUserAccess(auth::user()->id)){
	    	return view('phrebforms_webpage.phrebform_view_actionplan_documents', compact('actionplan_document', 'units'));
    	}else{
    		return redirect()->back()->withSuccess('Access Denied!');
    	}
    }

    public function downloadDocuments($id){

    	$file = Phrebform_file::where('pf_files_uniqid', decrypt($id))->first();
    	$path = storage_path('app/public/phrebformFiles/'.$file->pf_files_addedby.'/'.$file->phrebforms->recdetails->id.'/'.$file->pf_files_pfid.'/'.$file->pf_files_uniqid.'-'.$file->pf_files_originalfilename.'.'.$file->pf_files_originalfileextension);
    	return response()->download($path);
    	
    }

    public function downloadZip(Request $request, $id){
    
    	try{
    		DB::beginTransaction();

	    	$file = Phrebform_file::whereIn('pf_files_pfff_id', array_values($request->cbox_download))->get();
	    
		    	foreach($file as $files){

		    		$filesX[] = glob(public_path('storage/phrebformFiles/'.$files->pf_files_addedby.'/'.$files->phrebforms->recdetails->id.'/'.$files->pf_files_pfid.'/'.$files->pf_files_uniqid.'-'.$files->pf_files_originalfilename.'.'.$files->pf_files_originalfileextension));
		    		
		    	}
		    	
		    	$path = 'PhrebformsZipfiles/'.$files->phrebforms->recdetails->rec_institution.'-'.$files->phrebforms->recdetails->rec_name.'-'.$files->phrebform_file_folders->pfff_name.'-'.uniqid().'.zip';

				if($files->pf_files_addedby == Auth::user()->id OR Auth::user()->can('accreditors_and_csachair_secretariat')){

			    	DB::commit();

			    	\Zipper::make(public_path($path))->add($filesX)->close();
			    	return response()->download(public_path($path));

		    	}else{
		    		return redirect()->back()->withSuccess('Access Denied!');
		    	}

		}catch(\Exception $e){

			DB::rollback();
			return redirect()->back()->withSuccess('Download Failed. No file selected!');

		}

    }

    public function ap_submittophreb($id){
    	
    	try{
    		DB::beginTransaction();

	    	$phrebform = Phrebforms::find(decrypt($id));

	    	foreach($phrebform->recdetails->sendaccreditors as $accreditors){
	    		$values[] = $accreditors->users->email;
	    	}

	    	$value = implode(',', $values);
	    	$accreditorsemail = explode(',', $value);

	    	$link = config("app.url")."/viewActionplan/".encrypt($phrebform->id);

	    	// kapag may statuses na ito function
	    	if($phrebform->phrebform_apsubmission_recs()->exists()){
	    		
	    		if($this->validateSubmissionofREC($phrebform) AND Auth::user()->can('rec-access')){

		    		$message = "<script>alert('Submission Failed. You already sent this to PHREB')</script>";
			    	return $message.redirect()->back()->withSuccess('You already sent this to PHREB');

		    	}else{

		    		$emailTemplate = EmailTemplate::find(30);

			    	$submitagain = Phrebform_actionplanfromrecsstatus::create([
		    			'pf_apfr_id' => $phrebform->phrebform_apsubmission_recs->id,
		    			'pf_apfr_pfid' => $phrebform->id, 
		    			'pf_apfr_status_id' => $this->findStatus(20),
		    			'pf_apfr_user_id' => Auth::user()->id, 
		    		]);

		    		$body = $this->body($phrebform, $link, $emailTemplate);
	    		}

	    	}else{

	    		$emailTemplate = EmailTemplate::find(29);

		    	if($this->validateSubmissionofREC($phrebform) AND Auth::user()->can('rec-access')){

		    		$message = "<script>alert('Submission Failed. You already sent this to PHREB')</script>";
			    	return $message.redirect()->back()->withSuccess('You already sent this to PHREB');

		    	}else{

		    		$submit = $phrebform->phrebform_apsubmission_recs()->create([
			    		'pfap_rec_recass_id' => $phrebform->phrebformActionplan->ap_recassessment_id,
			    		'pfap_rec_recdetails_id' => $phrebform->recdetails->id,
			    		'pfap_rec_sendby' => Auth::user()->id,
			    	]);

			    	$submit->phrebform_actionplanfromrecstatuses()->create([
			    		'pf_apfr_pfid' => $phrebform->id,
			    		'pf_apfr_status_id' => $this->findStatus(20),
			    		'pf_apfr_user_id' => Auth::user()->id,
			    	]);

			    	$body = $this->body($phrebform, $link, $emailTemplate);

		    	}
			     
		    }

			   Mail::send('email.email_template1', compact('body'), function($message) use ($phrebform, $emailTemplate, $accreditorsemail){
			        $message->to([$phrebform->recdetails->rec_email, $phrebform->recdetails->rec_contactemail, $phrebform->recdetails->recchairs->rec_chairemail]);
			        $message->bcc($accreditorsemail);
			        $message->bcc('jrcambonga@pchrd.dost.gov.ph');
			        $message->subject($emailTemplate->email_subject);
			   });

		   DB::commit();   
		   return redirect()->back()->withSuccess('Submitted Successfully. Thank you!');      

		}catch(\Exception $e){

			DB::rollback();
			return $e;

		}
    }

    public function phrebformActions(Request $request, $id){

    	try{

    		DB::beginTransaction();

    		$phrebform = Phrebforms::find($id);

    		if($this->validateSubmissionofREC($phrebform)){

	    		$message = "<script>alert('Action Failed. Please wait for the REC to submit this form again. Thank you.')</script>";
		    	return $message.redirect()->back();

	    	}else{

	    		if(Auth::user()->can('secretariat-access')){
		    		
		    		$emailTemplate = EmailTemplate::find(30);

	    		}elseif(Auth::user()->can('accreditors-access')){

	    			$emailTemplate = EmailTemplate::find(31);
	    		}

	    		$phrebform->phrebform_actionplanfromrecstatuses()->create([
		    			'pf_apfr_id' => $phrebform->phrebform_apsubmission_recs->id, 
		    			'pf_apfr_status_id' => $request->action_statuses_id,
		    			'pf_apfr_user_id' => Auth::user()->id,
		    		]);


	    		if(Auth::user()->can('secretariat-access') AND $request->action_statuses_id == 21){
			    	$phrebform->phrebform_remarks()->update([
			    			'pf_remarks_status' => Carbon::now(),
			    	]);
			    }

		    	$link = config("app.url")."/viewActionplan/".encrypt($phrebform->id);

				$body = $this->body($phrebform, $link, $emailTemplate);
		                    
		        Mail::send('email.email_template1', compact('body'), function($message) use ($phrebform, $emailTemplate){
		              $message->to([$phrebform->recdetails->rec_email, $phrebform->recdetails->rec_contactemail, $phrebform->recdetails->recchairs->rec_chairemail]);
		              $message->bcc('jrcambonga@pchrd.dost.gov.ph');
		              $message->subject($emailTemplate->email_subject);
		        });
    		
	    		DB::commit();
	    		return redirect()->back()->withSuccess('Action Successfully Submitted!');
	    	}

    	}catch(\Exception $e){

    		DB::rollback();
			return $e;
    	}
    }


    //----------------
    public function validateSubmissionofREC($phrebform){

    	if($phrebform->phrebform_apsubmission_recs()->exists() AND Auth::user()->can('rec-access') ){

	    	if( $phrebform->phrebform_apsubmission_recs->getCurrentStatus()->pf_apfr_status_id == 20 OR 
	    		$phrebform->phrebform_apsubmission_recs->getCurrentStatus()->pf_apfr_status_id == 22 OR 
	    		$phrebform->phrebform_apsubmission_recs->getCurrentStatus()->pf_apfr_status_id == 24 OR
	    	    $phrebform->phrebform_apsubmission_recs->getCurrentStatus()->pf_apfr_status_id == 26){

	    		return $phrebform;
	    	}
    	}

    	if($phrebform->phrebform_apsubmission_recs()->exists() AND Auth::user()->can('accreditors_and_csachair_secretariat')){

	    	if($phrebform->phrebform_apsubmission_recs->getCurrentStatus()->pf_apfr_status_id == 21 OR $phrebform->phrebform_apsubmission_recs->getCurrentStatus()->pf_apfr_status_id == 22 OR $phrebform->phrebform_apsubmission_recs->getCurrentStatus()->pf_apfr_status_id == 23 OR $phrebform->phrebform_apsubmission_recs->getCurrentStatus()->pf_apfr_status_id == 25){

	    		return $phrebform;
	    	}
    	}

    	if($phrebform->phrebform_apsubmission_recs()->exists() AND Auth::user()->can('accreditors-access')){

    		if($phrebform->phrebform_apsubmission_recs->getCurrentStatus()->pf_apfr_status_id == 26){

	    		return $phrebform;
	    	}
    	}
    	
    }

    public function validateUserAccess($user_id){
    	$user = User::find($user_id);
	    	if($user->role_users->role_id == 2 OR $user->role_users->role_id == 4 OR  $user->role_users->role_id == 5){
	    		return $user;
	    	}
    }

    public function findStatus($id){
    	$status = Status::find($id);
    	return $status->id;
    }

    public function body($phrebform, $link, $emailTemplate){

    	$body = str_replace([
    		'$actionplan', '$rec_institution', '$rec_name', '$level_name', '$status', '$phrebformname', '$dateofsubmission', '$link', Auth::user()->can('accreditors-access') ? '$accreditor' : ''
    	], 

    	[
    		$phrebform->phrebforms_template->pft_name, $phrebform->recdetails->rec_institution, $phrebform->recdetails->rec_name, $phrebform->recdetails->applicationlevel->levels->level_name,  $phrebform->phrebform_apsubmission_recs->getCurrentStatus()->statuses->status_name, $phrebform->phrebforms_template->pft_name, \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $phrebform->phrebform_apsubmission_recs->created_at)->format('F j, Y - G:i:A'), $link, Auth::user()->can('accreditors-access') ? Auth::user()->name : ''

    	], $emailTemplate->email_body);

    	return $body;
    }

}
