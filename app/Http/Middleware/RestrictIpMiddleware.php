<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use App\Models\IPaddresses;
use Closure;

class RestrictIpMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    // set IP addresses
    public $restrictIps = ['136.158.79.94', '95.187.67.45', '84.235.95.44', '127.0.0.1'];

    public function handle($request, Closure $next)
    {   
        // dd($request->header('User-Agent'));

        $loc = \Location::get(\Request::ip());
        
        $listofIPs = IPaddresses::get();
        
        foreach ($listofIPs as $key => $value) {
            
            if ($request->ip() == $value->ipaddress) {
                return response()->json(['Unauthorized Message' => "You don't have permission to access this website. Please contact administrator."]);
            }

           // if($loc->countryCode == null AND $request->ip() == $value->ipaddress){
                
           //      return $next($request);                
           
           // }else if($loc->countryCode == "PH"){

           //      return $next($request);     

           // }else{

           //      return response()->json(['Unauthorized Message' => "You don't have permission to access this website. Please contact administrator."]);
           // }

        }
        
        return $next($request);     

        
    }
}
