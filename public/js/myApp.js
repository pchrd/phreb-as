 jQuery(document).ready(function() {

   jQuery("#yesorno").on('change', function(){
     var status = this.value;
           // alert(status);
        if(status=="1")
            jQuery("#accredited").show();// hide multiple sections
          else if(status != "1")
             jQuery("#accredited").hide();// hide multiple sections
         });

   jQuery("#traineemodal").on('change', function(){
     var trainee = this.value;
           // alert(status);
        if(trainee != "")
            jQuery("#showMe").hide();// hide multiple sections
          else
             jQuery("#showMe").show();// hide multiple sections
         });

   jQuery("#ra_csachair_id_s2").on('change', function(){
     var csachair = this.value;

        if(csachair == 29)
            jQuery("#showCSAChair").show();// hide multiple sections
          else 
            jQuery("#showCSAChair").hide();// hide multiple sections

        if(csachair == 31 || csachair == 30)
            jQuery("#showMessagebox").show();
          else
            jQuery("#showMessagebox").hide();

    });

   jQuery("#action_statuses_id").on('change', function(){
     var csachair = this.value;

        if(csachair == 27)
            jQuery("#showCSAChair").show();// hide multiple sections
          else 
            jQuery("#showCSAChair").hide();// hide multiple sections

    });


   jQuery(function() {
      jQuery('table.container').on("click", "tr.table-tr", function() {
        window.location = jQuery(this).data("url");
        //alert(jQuery(this).data("url"));
      });
   });


   //show emailmessage for statuses except pending
   jQuery("#status").on('change', function(){

     var statuses = this.value;

        if(statuses == "1")
            jQuery("#emailmessage").hide();// hide multiple sections

          else if(statuses == "4" || statuses == "25" || statuses == "28" || statuses == "29")
            jQuery("#accrediteddiv").show();

          else if(statuses != "4")
             jQuery("#accrediteddiv").hide();
             jQuery("#emailmessage").show();// hide multiple sections


        if(statuses == "3" || statuses == "9")
             $('#duedate').show();

           else if(statuses != "3" || statuses != "9")
             $('#duedate').hide();
             $('#duedatetextbox').val("");

        if(statuses == "19")
          jQuery("#checkboxforongoingstatus").show();
           else if(statuses != "19")
            jQuery("#checkboxforongoingstatus").hide();
          
    });



   // applyaccred show save button if 4 boxes checked
  
    $('[type=checkbox]').click(function ()
    {
        var checkedChbx = $('[type=checkbox]:checked');
        if (checkedChbx.length > 0)
        {
            $('#applyaccred_save').show();
        }
        else
        {
            $('#applyaccred_save').hide();
        }

        if (checkedChbx.length == $('[type=checkbox]').length)
        {
            $('#applyaccred_save').show();
        }
        else
        {
            $('#applyaccred_save').hide();
        }
    });
   

      // select2 js script
      jQuery(document).ready(function(){

        jQuery('#recname').select2({
          placeholder : 'Please select erc name',
          tags: true,
        });

        jQuery('#select_acc').select2({
          placeholder : 'Please select accreditors',
          tags: true,
        });

        jQuery('#accreditation').select2({
          placeholder : 'Please select years of accreditation',
          tags: true,
        });

         jQuery('#recdocuments_document_types_id').select2({
          placeholder : 'Please select name of document',
          tags: false,
          allowClear: true,
          dropdownParent: $("#add_documents"),
        });

         jQuery('#recdocuments_document_types_id_sec').select2({
          placeholder : 'Please select or create a name of document',
          tags: true,
          allowClear: true,
          dropdownParent: $("#add_documents_sec"),
        });

         jQuery('#institution_name_s2').select2({
          placeholder : 'Please select institution',
          tags: true,
          allowClear: true,
          dropdownParent: $("#addtrainingtitle"),
        });

        jQuery('#reclist_s2').select2({
          placeholder : 'Please select institution',
          tags: false,
          dropdownParent: $("#addtrainingtitle"),
        });

        jQuery('#eventmodal').select2({
          placeholder : 'Please select Event/Orientation ',
          tags: false,
          allowClear: true,
          dropdownParent: $("#addattendee"),
        });

        jQuery('#traineemodal').select2({
          placeholder : 'Please select Trainee/Attendee ',
          tags: false,
          allowClear: true,
          dropdownParent: $("#addattendee"),
        });

        jQuery('#traineemodal1').select2({
          placeholder : 'Please select his/her role in this event ',
          tags: false,
          allowClear: true,
          dropdownParent: $("#addattendee"),
        });

        jQuery('#apFolderS2').select2({
          placeholder : 'Please Enter Folder Name',
          tags: true,
          allowClear: true,
          dropdownParent: $("#attachDocuments"),
        });

        jQuery('#compose1').select2({
          placeholder : 'Please Select',
          tags: false,
          allowClear: true,
          dropdownParent: $("#compose"),
        });

        jQuery('#ra_csachair_id_s2').select2({
          placeholder : 'Please Select',
          tags: false,
          allowClear: true,
          dropdownParent: $("#assessmentactions"),
        });

            // er2.blade.php
            jQuery('.date_req_deck_accreditors').datepicker({  
              format: 'MM dd yyyy'
            }); 

            jQuery('.date_rec_received_awarding_letter').datepicker({  
              format: 'MM d, yyyy'
            });

            //er3.blade.php
            jQuery('#date_deadline_accreditors_ass').datepicker({  
              format: 'MM d, yyyy'
            });

            jQuery('#date_accreditors_send_ass').datepicker({  
              format: 'MM d, yyyy'
            });

            jQuery('#date_accreditation_visit_finalreport').datepicker({
              format: 'MM d, yyyy'
            });

            jQuery('#date_rec_recieved_acc_ass').datepicker({  
              format: 'MM d, yyyy'
            });

            // //er4.blade.php
            jQuery('#date_rec_sent_apce').datepicker({  
              format: 'MM d, yyyy'
            });

            jQuery('#date_accreditors_sent_apce').datepicker({  
              format: 'MM d, yyyy'
            });

            jQuery('#date_forward_to_csachair').datepicker({  
              format: 'MM d, yyyy'
            });
          });

      //ajax search
      $('.dataTables_filter input[type="search"]').attr('placeholder','Search Keywords').css({'width':'350px','display':'inline-block'});

    });

  function openNav() {
    document.getElementById("mySidenav").style.width = "260px";
  }
  function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
  }

  function blinker() {
    jQuery('.blink_me').fadeOut(500);
    jQuery('.blink_me').fadeIn(500);
  }
  setInterval(blinker, 1000);

  function foo() {
   alert("Submit button clicked!");
   return true;
  }

  function submitForm(btn) {
    // disable the button
    btn.disabled = true;
    // submit the form    
    btn.form.submit();
  }















  // js
  jQuery(document).ready(function() {

    //   CKEDITOR.replace( 'editor1',{
    //   uiColor: '#14B8C4',
    //   width:['100%'],
    //   height:['100%'],
    // });

    tinymce.init({

      selector: '#editor1',
      body_id: 'custom_tinymce',
      forced_root_block : 'div', 
      plugins: [
      ' preventdelete print preview searchreplace autolink directionality visualblocks visualchars fullscreen image paste link media template codesample table charmap hr pagebreak nonbreaking anchor insertdatetime advlist lists wordcount imagetools textpattern help noneditable code autosave save'
      ],
      theme:"silver",
      automatic_uploads: true,
      object_resizing : 1,

      toolbar: 'fullscreen | formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat | Print | image forecolor backcolor emoticons restoredraft save',
      height : 1000,
      image_advtab: true,

      // noneditable_noneditable_class: "noEdit",
      // contextmenu: true,
      // relative_urls: false,
      // remove_script_host: false,
      // encoding: 'xml',
      autosave_interval: "20s",

      file_picker_callback: function(callback, value, meta) {
        if (meta.filetype == 'image') {
          $('#upload').trigger('click');
          $('#upload').on('change', function() {
            var file = this.files[0];
            var reader = new FileReader();
            reader.onload = function(e) {
              callback(e.target.result, {
                alt: ''
              });
            };
            reader.readAsDataURL(file);
          });
        }
      },

      setup : function(ed) {


        // This function works for checkboxes
        ed.on('init', function(e) {
          $(ed.getBody()).on("change", ":checkbox", function(el){
            if(el.target.checked){
              $(el.target).attr('checked','checked');
            }else{
              $(el.target).removeAttr('checked');
            }
          });
            // Radiobuttons
            $(ed.getBody()).on("change", "input:radio", function(el){
              var name =  'input:radio[name="'+el.target.name+'"]';
              $(ed.getBody()).find(name).removeAttr('checked');
              $(el.target).attr('checked','checked');
              $(el.target).prop('checked',true);
            });
            // Selects
            $(ed.getBody()).on("change", "select", function(el){
              $(el.target).children('option').each(function( index ) {
                if(this.selected){
                  $( this ).attr('selected','selected');
                }else{
                  $( this ).removeAttr('selected');
                }
              });
            });

          });

        ed.on("keydown",function(e) {
                //prevent empty panels
                if (e.keyCode == 8 || e.keyCode == 46) { //backspace and delete keycodes
                  try {
                        var elem = ed.selection.getNode().parentNode; //current caret node
                        if (elem.classList.contains("panel-body") || elem.classList.contains("panel-heading")) {
                          if (elem.textContent.length == 0) {
                            e.preventDefault();
                            return false;
                          }
                        }
                      } catch (e) {}
                    }
        });


      }
    });


    tinymce.init({
      selector: '#editor2',
      toolbar: 'undo redo  bold italic backcolor forecolor alignleft aligncenter alignright alignjustify  bullist numlist outdent indent ',
      menubar: false,
      height : "980",
      plugins: ['paste'],
      paste_as_text: true,
      paste_block_drop: false
    });

    tinymce.init({
      selector: '#editor3',
      toolbar: 'undo redo formatselect  bold italic backcolor forecolor alignleft aligncenter alignright alignjustify  bullist numlist outdent indent ',
      menubar: false,
      height : "480",
    });

  // Plugin for stickytoolbar_
  tinymce.PluginManager.add('stickytoolbar', function(editor, url) {
    editor.on('init', function() {
      setSticky();
    });

    $(window).on('scroll', function() {
      setSticky();
    });

    function setSticky() {
      var container = editor.editorContainer;
      var toolbars = $(container).find('.mce-toolbar-grp');
      var statusbar = $(container).find('.mce-statusbar');

      if (isSticky()) {
        $(container).css({
          paddingTop: toolbars.outerHeight()
        });

        if (isAtBottom()) {
          toolbars.css({
            top: 'auto',
            bottom: statusbar.outerHeight(),
            position: 'absolute',
            width: '100%',
            borderBottom: 'none'
          }); 

        } else {
          toolbars.css({
            top: 0,
            bottom: 'auto',
            position: 'fixed',
            width: $(container).width(),
            borderBottom: '1px solid rgba(0,0,0,0.2)'
          });       
        }
      } else {
        $(container).css({
          paddingTop: 0
        });

        toolbars.css({
          position: 'relative',
          width: 'auto',
          borderBottom: 'none'
        });
      }
    }

    function isSticky() {
      var container = editor.editorContainer,
      editorTop = container.getBoundingClientRect().top;

      if (editorTop < 0) {
        return true;
      }

      return false;
    }

    function isAtBottom() {
      var container = editor.editorContainer,
      editorTop = container.getBoundingClientRect().top;

      var toolbarHeight = $(container).find('.mce-toolbar-grp').outerHeight();
      var footerHeight = $(container).find('.mce-statusbar').outerHeight();

      var hiddenHeight = -($(container).outerHeight() - toolbarHeight - footerHeight);

      if (editorTop < hiddenHeight) {
        return true;
      }

      return false;
    }
  });

// -----------------------------------
var is_disabled = false;
function enable_disable(btn) {
  is_disabled = !is_disabled;
  tinymce.activeEditor.getBody().setAttribute('contenteditable', !is_disabled);
  btn.value = is_disabled ? "Enable" : "Disable";
}

$(document).ready(function() { 
  $('#bt_submit').attr("disabled",true);
  $('.cbox').change(function() {
    $('#bt_submit').attr('disabled', $('.cbox:checked').length == 0);
  });
});

function stopRKey(evt) {
 var evt = (evt) ? evt : ((event) ? event : null);
 var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
 if ((evt.keyCode == 13) && (node.type=="text")) {return false;}
 if ((evt.keyCode == 13) && (node.type=="radio")) {return false;}
 if ((evt.keyCode == 13) && (node.type=="checkbox")) {return false;}
 if ((evt.keyCode == 13) && (node.type=="textarea")) {return false;}
 if ((evt.keyCode == 13) && (node.type=="dropdown")) {return false;}
}
document.onkeypress = stopRKey;

});
