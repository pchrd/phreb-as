@extends('layouts.myApp')
@section('content')
<div class="container">
	<div class="col-md-12" id="savedclass">
		@if(session('success'))
			<div class="alert alert-secondary">
				{{session('success')}}
			<button class="close" onclick="document.getElementById('savedclass').style.display='none'">x</button>
			</div>
		@endif
	</div>

	<form action="{{route('store_awarding', ['recdetail' => $recdetails->id, 'doctypes' => \Request::get('awarding_letter')])}}" method="POST">
		{{ @csrf_field() }}
		<label class="form-control text-dark"><b>{{$recdetails->rec_institution}} - {{$recdetails->rec_name}} | Level {{$recdetails->applicationlevel->level_id}} | {{$recdetails->status->status_name}}</b></label>
		<textarea class="form-control" name="editor1" id="editor1">
			@include('awardingletter.awardingtemplate'
)		</textarea>
		<div class="modal-footer btn-group pull-right" role="group" aria-label="Basic example">
			<a href="" data-toggle="modal" data-target="#awarding_save" class="btn btn-success"><span class="fa fa-save"></span> Save</a>
			<a href="{{ url()->previous() }}" class="btn btn-danger"><span class="fa fa-arrow-left"></span> Cancel</a>
		</div>
		@include('awardingletter.awarding-modals')
	</form>
</div>
@endsection