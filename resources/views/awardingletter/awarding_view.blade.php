@extends('layouts.myApp')
@section('content')
<div class="container">
	<div class="col-md-12" id="savedclass">
		@if(session('success'))
		<div class="alert alert-success">
			{{session('success')}}
			<button class="close" onclick="document.getElementById('savedclass').style.display='none'">x</button>
		</div>
		@endif
	</div>

	<form action="{{route('awarding_save', ['id' => $awardingletter->id])}}" method="POST">
		{{ @csrf_field() }}
		<label class="form-control text-dark"><b>{{$awardingletter->recdetails->rec_institution}}-{{$awardingletter->recdetails->rec_name}} | Level {{$awardingletter->level_id}} | {{$awardingletter->recdetails->status->status_name}}</b> <p class="pull-right small">Date Created at: <b>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $awardingletter->created_at)->toDayDateTimeString()}}</b> | Last Updated at: <b>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $awardingletter->updated_at)->toDayDateTimeString()}}</b></p></label>
		<textarea class="form-control" name="editor1">
			{{$awardingletter->a_letter_body}}
		</textarea>
		<div class="modal-footer btn-group pull-right" role="group" aria-label="Basic example">
			<a href="" data-toggle="modal" data-target="#awarding_save" class="btn btn-success"><span class="fa fa-save"></span> Save</a>
			<a href="#" class="btn btn-info" data-toggle="modal" data-target="#awardingsend"><span class="fa fa-envelope"></span> Send</a>
			<a href="#" data-toggle="modal" data-target="#a_download" class="btn btn-warning"><span class="fa fa-download"></span> Download</a>
			<a href="{{ url('show-awarding-letters') }}" class="btn btn-danger"><span class="fa fa-arrow-left"></span> Cancel</a>
		</div>
		@include('awardingletter.awarding-modals')
		@include('awardingletter.awarding-modals-1')
	</form>
</div>
@endsection