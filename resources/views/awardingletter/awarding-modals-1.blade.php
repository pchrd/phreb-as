<div class="modal bs-example-modal-sm-awardingsend" id="awardingsend" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header badge badge-info">
						<h4>Send Confirmation</h4> 
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<div class="modal-body">
						<p class="justify">Do you want to send this letter to <b>{{$awardingletter->recdetails->rec_institution}} - {{$awardingletter->recdetails->rec_name}}</b>?</p>
					</div>
					<div class="modal-footer">
						<a href="{{route('a_sendpdf', ['id' => $id])}}" class="btn btn-info">Send</a>
						<button data-dismiss="modal" class="btn btn-danger">Cancel</button>
					</div>
				</div>
			</div>
	</div>

<div class="modal bs-example-modal-sm-a_download" id="a_download" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header badge badge-warning">
				<h4  style="color: white">Download Confirmation</h4> 
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<p class="justify">Do you want to download this as PDF?</p>
			</div>
			<div class="modal-footer">
				<a href="{{route('a_downloadpdf', ['id' => $awardingletter->id])}}" class="btn btn-warning">Download</a>
				<button data-dismiss="modal" class="btn btn-danger">Cancel</button>
			</div>
		</div>
	</div>
</div> 