<div class="modal bs-example-modal-sm-awarding_save" id="awarding_save" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header badge badge-success">
				<h4>Save Awarding Letter</h4> 
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<p class="justify">Do you want to save your current work?</p>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-success">Save</button>
				<button data-dismiss="modal" class="btn btn-danger">Cancel</button>
			</div>
		</div>
	</div>
</div>