@extends('layouts.myApp')
@section('content')
<div class="container">
	<div class="col-md-12" id="verifyuser">
		@if(session('success'))
		<div class="alert alert-success">
			{{session('success')}}
			<button class="close" onclick="document.getElementById('verifyuser').style.display='none'">x</button>
		</div>
		@endif
	</div>
	<div class="row">
		<div class="col-md-12 col-md-offset-1">
			<div class="panel panel-info">
				<div class="panel-heading"><h3><strong>Awarding Letter</strong></h3></div>
				<div class="panel-body">
					<nav>
						<div class="nav nav-tabs" id="navbar-example2" role="tablist">
							<a class="nav-item nav-link active text text-info" id="letter-tab" data-toggle="tab" href="#or_req" role="tab" aria-controls="nav-home" aria-selected="true">Saved Letter</a>
						</div>
					</nav>

					<div class="tab-content" id="nav-tabContent">
						<div class="tab-pane fade show active" id="letter" role="tabpanel" aria-labelledby="letter-tab">
							<table class="table table-hover cell-border" id="awardingletter">
								<thead class="badge-info">
									<tr>
										<th>ID</th>
										<th>TO REC</th>
										<th>SAVED BY</th>
										<th>AWARDING NAME</th>
										<th>NO. OF YEARS</th>
										<th>BODY</th>
										<th>DATE CREATED</th>
										<th>DATE SENT</th>
										<th>DAYTIME</th>
										<th>ACTIONS</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
@include('layouts.myApp_footer')
<script type="text/javascript">
	$(function(){
		$('#awardingletter').DataTable({ 
			dom: 'Blfrtip',
			buttons: 
			['colvis','copy', 'csv', 'excel', 'pdf', 'print'],
			processing: true,
			serverSide: true,
			ajax: '{{ route('ajax-show-a-letters') }}',
			columns: [
			{ data: 'id', name: 'id', visible:false },
			{ data: 'recdetails.rec_name', name: 'recdetails.rec_name', visible:true },
			{ data: 'users.name', name: 'users.name', visible:true },
			{ data: 'documenttypes.document_types_name', name: 'documenttypes.document_types_name', visible:true },
			{ data: 'a_letter_years', name: 'a_letter_years', visible:true },
			{ data: 'a_letter_body', name: 'a_letter_body', visible:false },
			{ data: 'created_at', name: 'created_at', visible:true },
			{ data: 'a_letter_datesend', name: 'a_letter_datesend', visible:true },
			{ data: 'time', name: 'time', visible:true },
			{ data: 'actions', name: 'actions', visible:true },
			],
			initComplete: function(){
				this.api().columns([0,1,2]).every(function(){
					var column = this;
					var input = document.createElement("input");
					$(input).appendTo($(column.footer()).empty())
					.on('keypress', function () {
						column.search($(this).val(), false, false, true).draw();
					});
				});
			}
		});
	});
</script>
@endsection