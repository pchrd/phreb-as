<div class=WordSection1>
<span style='font-family:"Arial","sans-serif"'>
<p class=MsoNormal style='text-align:justify'>{{\Carbon\Carbon::now()->format('F j, Y')}} <br> <br><br>

{{$recdetails->recchairs->rec_chairname}} <br>
CHAIR<br>
{{$recdetails->rec_name}}<br>
{{$recdetails->rec_institution}}<br><br><br>

Dear <b>{{$recdetails->recchairs->rec_chairname}}</b>,<br><br>
The Philippine Health Research Ethics Board (PHREB), upon the recommendation of
Committee on Standards and Accreditation will grant a <b>{{$awarding_status->document_types_name}} on Level {{$recdetails->level_id}} Accreditation</b> to the <b>{{$recdetails->rec_name}} - {{$recdetails->rec_institution}}</b> effective
<b>{{\Carbon\Carbon::createFromFormat('Y-m-d',$recdetails->applicationlevel->date_accreditation)->format('F j, Y')}}</b>. This will give the IRB enough time to address the
issues raised by the Committee on Standards and Accreditation.<br><br>

Once accredited, the REC must comply with the 2016 PHREB accreditation policies and
2017 National Ethical Guidelines for Health and Health Related Research. These
can be accessed through PHREB website <a href="http://ethics.healthresearch.ph/"><span
style='color:windowtext'>http://ethics.healthresearch.ph/</span></a>. As PHREB
accredited REC, your responsibilities include

<ol style='margin-top:0in' start=1 type=1>
 <li class=MsoNormal style='text-align:justify'>Submission
     of annual report.</li>
 <li class=MsoNormal style='text-align:justify'>Reporting
     of any controversial or important ethical issues in the course of its work</li>
 <li class=MsoNormal style='text-align:justify'>Preparedness
     to be monitored or audited by PHREB.</li>
 <li class=MsoNormal style='text-align:justify'>Posting
     of accreditation certificate <b>– {{$recdetails->rec_name}}</b> shall post or display its
     certificate of accreditation in a conspicuous area within its office.</li>
 <li class=MsoNormal style='text-align:justify'>Send
     at least two (2) <b>{{$recdetails->rec_name}}</b> officers/members as accreditor-trainees
     when invited to participate in a PHREB accreditation visit.</li>
</ol><br>

In addition, you are required to submit to PHREB as recommended by the PHREB accreditor the following:<br><br>


<table align="center" border="1" cellpadding="4" cellspacing="0" style="width:100%">
  <tbody>
    <tr>
      <td>
      <p style="text-align:justify"><strong>Observations on submitted<br />
      &nbsp; Action Plan</strong></p>
      </td>
      <td style="text-align:justify"><strong>Recommendations</strong></td>
      <td style="text-align:justify"><strong>Document/s to<br />
      &nbsp; submit</strong></td>
      <td>
      <p style="text-align:justify"><strong>Due date</strong></p>
      </td>
    </tr>
    <tr>
      <td>
      <p style="text-align:justify">&nbsp;</p>
      </td>
      <td style="text-align:justify">&nbsp;</td>
      <td style="text-align:justify">
      <p>&nbsp;</p>
      </td>
      <td style="text-align:justify">&nbsp;</td>
    </tr>
  </tbody>
</table> <br><br>

Please take note of the submission dates. Accreditation status at the end of the {{$awarding_status->document_types_name}} accreditation will depend on the submission of annual report and compliance to the above recommendations. <br><br>

As a PHREB accredited Level {{$recdetails->level_id}} REC, please continue to upgrade your work as a
quality review committee by consistently being updated about, and complying
with international, national standards and your SOPs, continuing ethics training,
improving review of protocols and monitoring the conduct of studies and
post-approval procedures till the end of the study. <br><br>

Ifyou have any queries, please contact Mr/Ms {{Auth::user()->name}} through telephone number (632) 837-7537 or email at ethics.secretariat@pchrd.dost.gov.ph. <br><br><br>

Congratulations and warm regards<br><br><br>

Very truly yours,<br><br><br><br><br>

<b>LEONARDO D. DE CASTRO, PhD</b><br>

<p class=MsoNormal style='text-align:justify'>                    
Chair</p>

</p></span>

</div>