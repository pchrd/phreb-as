<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=Generator content="Microsoft Word 14 (filtered)">
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:Wingdings;
	panose-1:5 0 0 0 0 0 0 0 0 0;}
@font-face
	{font-family:Wingdings;
	panose-1:5 0 0 0 0 0 0 0 0 0;}
@font-face
	{font-family:Cambria;
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:Tahoma;
	panose-1:2 11 6 4 3 5 4 4 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin-right:1in;
	margin-bottom:1.5in;
	font-size:12.0pt;
	font-family:"Cambria","serif";}
p.MsoHeader, li.MsoHeader, div.MsoHeader
	{mso-style-link:"Header Char";
	margin:0in;
	margin-bottom:1.5pt;
	font-size:12.0pt;
	font-family:"Cambria","serif";}
p.MsoFooter, li.MsoFooter, div.MsoFooter
	{mso-style-link:"Footer Char";
	margin:0in;
	margin-bottom:1.5in;
	font-size:12.0pt;
	font-family:"Cambria","serif";}
a:link, span.MsoHyperlink
	{color:blue;
	text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
	{color:purple;
	text-decoration:underline;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
	{mso-style-link:"Balloon Text Char";
	margin:0in;
	margin-bottom:.0001pt;
	font-size:8.0pt;
	font-family:"Tahoma","sans-serif";}
span.HeaderChar
	{mso-style-name:"Header Char";
	mso-style-link:Header;}
span.FooterChar
	{mso-style-name:"Footer Char";
	mso-style-link:Footer;}
span.BalloonTextChar
	{mso-style-name:"Balloon Text Char";
	mso-style-link:"Balloon Text";
	font-family:"Tahoma","sans-serif";}
p.Default, li.Default, div.Default
	{mso-style-name:Default;
	margin:0in;
	margin-bottom:.1.5in;
	text-autospace:none;
	font-size:12.0pt;
	font-family:"Cambria","serif";
	color:black;}
.MsoChpDefault
	{font-size:10.0pt;
	font-family:"Cambria","serif";}
 /* Page Definitions */
 @page WordSection1
	{size:595.35pt 841.95pt;
	margin:1.0in 1.0in 1.0in 1.0in;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 ol
	{margin-bottom:0in;}
ul
	{margin-bottom:0in;}
footer{
  position: fixed; 
  bottom: 0cm; 
  left: 0cm; 
  right: 0cm;
  height: 2cm;
  margin-bottom: 20px;

  /** Extra personal styles **/
  color: white;
  text-align: center;
  line-height: 1.5cm;
}
</style>
</head>

<body lang=EN-US link=blue vlink=purple>
<div class=WordSection1>
<img src="{{asset('image/image002.jpg')}}" style="height: 120px; width: 100%;"><br>
	{!! $get_details1->a_letter_body !!}
	<footer>
<img src="{{asset('image/footer.png')}}" style="height: auto; width: 100%;">
    </footer>
</div>
</body>
</html>