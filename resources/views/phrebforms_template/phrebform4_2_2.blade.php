<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=Generator content="Microsoft Word 15 (filtered)">
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:Wingdings;
	panose-1:5 0 0 0 0 0 0 0 0 0;}
@font-face
	{font-family:SimSun;
	panose-1:2 1 6 0 3 1 1 1 1 1;}
@font-face
	{font-family:Mangal;
	panose-1:0 0 4 0 0 0 0 0 0 0;}
@font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:"Palatino Linotype";
	panose-1:2 4 5 2 5 5 5 3 3 4;}
@font-face
	{font-family:"Microsoft YaHei";
	panose-1:2 11 5 3 2 2 4 2 2 4;}
@font-face
	{font-family:"\@Microsoft YaHei";}
@font-face
	{font-family:"\@SimSun";
	panose-1:2 1 6 0 3 1 1 1 1 1;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoHeader, li.MsoHeader, div.MsoHeader
	{mso-style-link:"Header Char";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoFooter, li.MsoFooter, div.MsoFooter
	{mso-style-link:"Footer Char";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoCaption, li.MsoCaption, div.MsoCaption
	{margin-top:6.0pt;
	margin-right:0cm;
	margin-bottom:6.0pt;
	margin-left:0cm;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;
	font-style:italic;}
p.MsoList, li.MsoList, div.MsoList
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:6.0pt;
	margin-left:0cm;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoBodyText, li.MsoBodyText, div.MsoBodyText
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:6.0pt;
	margin-left:0cm;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:10.0pt;
	margin-left:36.0pt;
	line-height:115%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
p.MsoListParagraphCxSpFirst, li.MsoListParagraphCxSpFirst, div.MsoListParagraphCxSpFirst
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	line-height:115%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
p.MsoListParagraphCxSpMiddle, li.MsoListParagraphCxSpMiddle, div.MsoListParagraphCxSpMiddle
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	line-height:115%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
p.MsoListParagraphCxSpLast, li.MsoListParagraphCxSpLast, div.MsoListParagraphCxSpLast
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:10.0pt;
	margin-left:36.0pt;
	line-height:115%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
p.Heading, li.Heading, div.Heading
	{mso-style-name:Heading;
	margin-top:12.0pt;
	margin-right:0cm;
	margin-bottom:6.0pt;
	margin-left:0cm;
	page-break-after:avoid;
	font-size:14.0pt;
	font-family:"Arial",sans-serif;}
p.Index, li.Index, div.Index
	{mso-style-name:Index;
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.TableContents, li.TableContents, div.TableContents
	{mso-style-name:"Table Contents";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.TableHeading, li.TableHeading, div.TableHeading
	{mso-style-name:"Table Heading";
	margin:0cm;
	margin-bottom:.0001pt;
	text-align:center;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;
	font-weight:bold;}
span.HeaderChar
	{mso-style-name:"Header Char";
	mso-style-link:Header;
	font-family:SimSun;}
span.FooterChar
	{mso-style-name:"Footer Char";
	mso-style-link:Footer;
	font-family:SimSun;}
span.ListLabel1
	{mso-style-name:"ListLabel 1";
	font-family:"Courier New";}
 /* Page Definitions */
 @page WordSection1
	{size:792.0pt 612.0pt;
	margin:2.0cm 2.0cm 49.5pt 2.0cm;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 ol
	{margin-bottom:0cm;}
ul
	{margin-bottom:0cm;}
-->
</style>

</head>

<body lang=EN-PH style='line-break:strict'>

<div class=WordSection1>
<table class="MsoNormalTable" style="width: 98.1944%; border: none; height: 141px;" border="1" width="106%" cellspacing="0" cellpadding="0">
<tbody>
<tr style="height: 27px;">
<td style="width: 17.76%; border: solid windowtext 1.0pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 27.5pt;" rowspan="5" width="17%">
<p style="margin: 0cm 0cm 0.0001pt; text-align: center; line-height: normal; font-size: 11pt; font-family: Calibri, sans-serif;" align="center"><img style="width: 169px; height: 161px; visibility: visible;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMEAAACuCAYAAAH451mFAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAIdUAACHVAQSctJ0AACvfSURBVHhe7Z0HfBzF9ccNScif/AlJbISxLRFICH9qaKH3gCGEYEJIIHQIGGISCNW0BIMDxkaScbdxb7jhXsAd96Ziq1lWt6ol2ZIly7Zky/L7z2925jQ3t7e3dzrpTqf9fvS0fXd237yZN7Nzbzu1OecOTqZgiTilJ9joi8fn5Yk5a8QpPbFzEbuIU3pi9yJNJ0+KOe+IU3pi5yLnDtkl5qwRp/TE10XeWlnIp+cO2cmnVohTeiIvEr+5jObtrqLE0sNUVnecr5uTcYBPe83aw6e+EKf0RF7k0a+z6Z1VhTQmoZyW5xzk68CknZVizp2ZaQdoR0kdlYsEAXFKT+RFnpyXTT2n7Ka/Lcyl/uuK+brzhnrXxctL8+mLrWW0JKtarLFxkXMHJ7kJOHaiiU/X5tdQ+eHmFIPrxqbSw7OzqK/QGRCn9MTqIgA5qw9L9fikCrHGwGxfcUpPvF1EHmy27ji7Q30dEKf0xOoidkUiTulJSC8isbNeIk7pSZtepKXgPOKUYYCWmrOU5R/LeWXdWUx+K6YuwuuOTPipmLqIjku8V8yGAXh+wRB2qlOMM2qInNdixEU8EdstsVOvt+wCX/iuzwO+AIqI/utLxZJ3AroAUl7f2EQDNpTSsG37xFpzfF5gQnIFrWFVafaBer4snzsO/CptP5+3wucFXlqSR59tLKFZzMvYWlzH1+EO+q8rpdsn7+bLkpE79tGK3IOuxACfF7hhXBo9PCuLXl++V6whWplfK+bceWxuNv17TRFN2VVJm4oO8XWWF8jcf5Tt4FkffLLRUG565VE+Vbl7Sga9sKjZiw/oAuAh5hHCt/omu9mJU/fbLS4e8AV8rQv4AnYlfC8gMVuvLodeB9gQDLxeoNXBldWrY/6Mu55dIRaBmwehoHogF4hpmIEU9ohP/EQscvQ77njg7sMNvzXi3IRG7BZrd8QubXITfZYWiDmi11cYzsPs9Co+zT5glLotoU1uovLwMTHXjKwyKrTukUAIyk18m3OQ+X6HKIMlrKjmGFUfbRRbDKIHN/viv/kylU8vHJ7Cp+fGJ/JpS2jxTby+vIA+Zp4Y3O2pzHdcvKeaNuytpV3lR6j++Am+DyrrjMoj9ODMLCo8KLxpUak3Np2k9IojfN4bcCWLa49RTb1xPp0W38QdkzLogRl76Ml5OfTPZfncEY7bXMqbACpd41nlxJ76uV94avLnXnpB0eM2kPnE45IqeAfn2oJa2rnvMOVXN4g9DFp8E5KrRqfQLRPS6b7pmXTSpJMZx9npslW5fVI6Pcge0DMLcuj1bwvov+uLWZOknDddVFp0E3a8WTPeX11II7bvo/ns6W5hTZfc6uY2khlDt5XRzePTxJInIbmJK5nW7mLNI7TD3mDttcIa9+whMTsfRJZskla/idaQiLiJzUXuLWxHE4HYhNk+UnTM9oE4NyHBQeEkIln28PsAhUCO7fHJujox2za05AbDndPFlBMdm5THJqeJG+ad7FEj0s/AVOFabD/tgms3YxoTu71JrHdwCCuQP8NNRNLsgQPCiZDcwE0TMlxjJ1qKo4FA4e/ujrdcCyG7gWkp+ym13Lonww4h1EAynWdz0JgVbXID14xN59No8bIafUOyr+PBWVliLjDaTAMbCo0XrQBPv8+y5tFZLaFNbiBF5PWD9e5dliB+a5mYC4w208DE5AqKYXl+ZEK5WGNo4on5uWIpMIJyA98V1FJy2WHKrarnA/rqjrn3f1422ujEQjMRjNhRTvuPGNqQ6wKlxTeAEQtfpe6nZdkHaXPRIUqrOEJ7axqoSvRw4yl/sLaI94BLUAeMTnTvcw2UFt0ARje+t7qQBm0qpbGJ5Xwk6srcGtpeUsc7AgBKG/2m0Tl83hBjnVnfq0qi0Gyll3cRLbqBP840Omtf/aaA+n1XREOYQU7eWUkLM6toHctWKvuPHKffTtlNK9gN2gV2g3Ohqz+VaRbdlfq7ixbdwI3j0+ieqbvpz7Oz6PlFufTWir30yfoSV7YCzy1Uh5IY+f1Y4wmWkOPU0OjdlXiFP5RiGrptH01ntTZGkWAAjtSspEU3AK4ek0K3TUyn+7/KpMfnZvOhvuit1pFjZuzyENPucwtz6U32UAZsKOHjmZFFV+e5a7DFNwAuHbmLDx9G7zQubIbZCxEr7pyUTvcy7T4yJ5v+viSPP5S4zWUeA7FbdAOyB82bqNwzLVPMES3aU81HfRccbKDaBvNXTuAK8aLlD0y7eHOEbKXTZjdQXNP8thODyjCOayXLDnY8UrxY1F91SdrsBgDK/54sW+Bp9l25l5dac9LdXzGpmJ0TotKmNyC5cVwa9Zqxh15cnMdLGm+YnROi0qo3APEst8330zHbR4pKq9+AXfHnfCphcwP+iIpzA6EQlVa9ATv7BCIqrX4DKv1Yu8BsPyk6ZvtAVNr0BoDZfhAzzPaDqDg3YCVmmO0HMcNsP4hKi24gHAjoBsJNRNI6ANEDNn0jZsH3xdQ2Vk8reuCWIWK2U9T7S68Us8FF3kDM4KTP2US9gVPxzySBGM7uGtIutvN1+r76DfzsyU8vU/bvdPbbs3uJebfzmVzTof3T+bn4nWKWDx9Q1HyJqnIx31fuEx2/cwtb7sE3NnO9mLYdyg38Gv9MEs2RCeeJH5w4JiYu4aTYpNL2N+DgEDhdBmXy30k7Yi7iMbUe6sUcPFGfj3hkwcdRgG86lBLQI44fnt86ufn9RDjQoZQg+dWwFHp1eXCGaASDDqmEcKNDKgG/H7t/RhYN215BJ5p8BzJrbTqkEjDABGk6diL0CgARoQRfYfnw083rxqbzQWMYHZRQepgONZzgoeLCgYhQAmL4WNFrVhZN21XJ0pHEh57AS4LilmQ1jyULJRGhhDglYoEceQhXFIOOthTVUV51Ax+/pgNl/G76HlqYWU05JtvbirBSQnV9Ix8BifhNGAW5r+44HTjayIsOf1FzPRRydpx7GibuFJaB9LF9UFSFipArAcUC4hJgGBFiE2AUJwYtYiQnRjBjzF/+wQYqOXSMKo8cp4MmMQYuHplCzy7Mp9V5tXw/cEAZtbk4q5pVxkSXj06jcUmVlCWCdF00wgjUAF5cki/m2p6QKuGdlYV8CBQGpSHM6eiEcj4SFaEDMIIUcVgxIhUDMJNZ5Yrfu6JYQcAHFVeuZ7kZD9rM7dRz/SNf54gtoSekSsCAOgReQ3ALjKRFgAuMpkWQC4yoxeg/jKpFgAoM2EbcO0SBQzReFFuSfXXNSnnlGyMwTMwXu+jTDaWu3wOA+kbDiuTY5hOKV3VOvH+xJwDShVG6GBGPjLKLZRRYLoZoljKLPHDEXlEaUiXcPWU3Hyv6J+a9YDQwRvBiRDBCqGAALALafc5cyuHb99HYpAr+g6KvMw7wwMC4UW8gIg+/rqgPHpyZTf2ZclXumtqy/iOMMob1YqQxhpYiXUtZujDiGKP7k8oO8xg2iL0By7Vyo0OqBIDRzBi4+9vJGXTftN18sDxCQ2PAPIIyYtA8wkRj4LwstnDjoeTp+UbaXvu2gP8yAZlFWi8G46P9geIUQSLXs/oNA5itxv6GXAng5aV5dMWoFD4A+abxaTwOz71MIRg6+5c5WfQUu2lZbFnxIFMgcv8vhu2iFxa3XkWL+JWwYPwoQRapMrN8xOo4NAgRCAmBsfHDAkSkQbHljZApwU4wC18SKKjcUWa35Ce3MsPcySz4dyzD4HcJiJwji1S0UVDHfSqKLSsiTgkXDEU9sJOu+tI9xhEeBHLmbOYKI1fCDcbPy/aw+gNlNirRQLhs5C66akwKDzeLQFH42Q4sGD/uQJFqh4hTgqSWNfwuHZ1KvZcU0F9ZHYMiAxG0PlxbRLGs/Ea9As8LPzpZm2+0S/zBLD1WooeTUYlYJegglyI8GuoYFBn45c77q43KHpHE4OH4g1l6rMRRguByVnRcPcbwxlBsIK7zU6xizfcRws0Ms/RYScQqwUyuHO3eB2S2jy/RwQdOzPbzRzqUEsJVHCWEgThKCANxlBAGErFKMMNsv2DIW8vdf4xuto+VdCgl6Jgd50vsYHaclThK8FPsYHaclThK8FPsYHaclYSlEto7Zg/aSsJCCY74FvHIHCKC6Pidrp8nxsTvWCBmOTGDkw6JWW4hYjaoqNdX5/0hJj4pVsy60++77wd6zjYlJi6xTjzgq8XPmr/H18cnNImfN4PvK0r4B/79z3UP9MVUVY63eaAuR7062fVtLKxXBeu69V/bwDcK1GPVeYn4yTT/GTa7n0ZMAX4+rdzfWWcPSe1qbPE4zynRg5PHiHnXtlPO6PItX2F8IfQCub5HfNJGTEH3j1fWYfr9qPPWYGqWPr9QfxyPk5kpwWwaE5/4XnR88sts8UOsA3K7pPNjH83u3G/bmWxfxB7/obHWfT+zc2PK+F5MXMLI7rE7/qqfF6i/W1e3Kz/C5xkL9IhNaDh7yNauUa9MrerUiXi89G79V9ZjGh2bUNl90I7/++lDfXOxrKXDpQTAng1Xtp4efdnBwe2X6BL+i/RAkbms+4ANJXxF4HScX7X7UoJquud8sJQVD+54M3UvJv8bdb2XfSROaAEHBwcHh/AGFZkj5sJa5P3FY2odzC7qiKeIxxVc1AvoXxp1aO6uhnQZtOnH4rEFD3nyKVpEc4dmVCWIxxY85IkdrOkwCrhx/G56c6X3WOyhosMoAL9JuHik9+8uh4oOo4BwpUMpAL/QfGBmlt/ffGlNOpYC+M9kd9KvRxtfkAsHOpQCwpEOpwD87Gl+ZrVYCj0dSgHyl/pvryqmKSnGx8xCTcdSANIkgoncMD5DrA0tHUoB4JIRO+k8Pz8c15q0ewUg5AZytPrtw2+yq/k6lY/WlVBSqXtsiGvHpdOhhsB+AB4s2rUC8rSfrF7PipV7pnt+LhFl/0OzsngwKjW0juT8oREYrastFIBf2PviOqYUo9zfSb+fnkUfrC6mK8aET5dEu1aA/snVvquKqGu85zWN4eQsPaIhphMTwjqhXStAMsvkk5kINqhy5LhntKwP1hS7vqEeKiJCARKEP/slK89vmpDO4wHtEYEB1eBYMnAs4tX9d30xD7lWddT8k9BtQbtWwMvL3OsABAnk12VFzR++yqLYzWW0fm/zR/clv0LIHLbPhcNT6MPvinn4tVARNgqQQfbg2ZTUHqMKlpvxsX+L0G4Uv8U92FKXWPnzIUMJuisqwTfI+X6icr6AKSJUhFwBCPi3Kq+GNhYeoh2lRpxSBGmS0Q7xsGosoh1uKKylh2bnkPp99NvGp/IHrNJj8E7qpkRjPMoqcOxz8/jQekQhVcDE5Eqakbqf5mcasdsQlElGOExn/jpCGuNr94jgW6VVqpJfDU+lEdvLaXelUd5nKj+ck2HPYAkvLS2g9QV1VKR8vFsSyoo4ZAoYuLGUf5X/y0QjzCRClSEyloxquE3E/tytFUsq/BqsqLl/5h4ax8pxKFJnGFOO7P+B14NOuG6aq3rMRxDy1iRkCvhgTRHzQkp4aDJEwpKRDOdmVLnifW5ixVICK5YQXhKhjlEslYmwyODi4cY1oAQEh71zimcr+NCxE2yf5vIen1Z5ZK57rLg17FqhIiQKGLKllIcie3vlXvrPWiN6ISrUUTvKeYxPXiyJkJIolrZoxZLKufGJrgcs2VFymE8vZMUTMD4gYVTOElkf/GetewBZfzjMlLtLqbPKRFF5RESRt0NIFIAAfc8uyKW/i0Cr74q4niiWhmwt81ksqfydle1A7eN5aHYuLcw6SMXCWtSovB+uNYam/I65qSDQ8h/9StJ5wMcpcH1kDtQx0oOzQ0gUIIOrPjEvh55flEv/WJZPby7fK4qlYl4sITyyt2JJgmJHZSF7KLK8f3d1MS1mSqg2aWR9tM59fJCvr4HojE00wjYjXcuyEdGxhlupDGyObyYggvx+G6E2Q6KAu6YYAVURURfxOxF2uPdiI8otiiWErUQs6MGiWJq0s5K1YPfTAlbJfpvTHOn2ohFGESOBx6SW9zdNzOQhinV6tyB678dMebLeQkBzFJdIFyLMw0plyH8US3tZsQQPzoqQKADcOiGdK+L30zN5RER/iiUVGSJfzcSyvEflDB5VQuQP2WrEuF7ELCoQXheWisjyst5CBkGofxSX+PaCUSzVuYolK0KmgGvGpNKNiGY70QhLqRdLPMQwu1nEDFWLpVCDtCFW9tsrDEtFPFOZQaalGFHbUSwhvD8vlpjzEJKgHr4UAK4YnULXfplKN09ojvOsFksvsmIJ3lJfUSzlVVncCCty/jInh75Kq6LaAD65YhcElJVp+5ewVBRLCOvP3WkvxZI3QqoAIGM7X8+s4VZmDWqxhIDbRrGUT/sU/98MWe7fx7ybN1e23icUkUkQzh9F5nMsbX2Ypb6hFUvyCyQoLuEtVRz2nvaQKwA0NTXRr0el0DXMGm4c3xxk+4EZRrFkh4vQKIMSRLnfWiCSr8wkD89iRSbLJC/IYklY6kBRLElvyYqQKMCoJAOXQEHfEDwT+dmUQPjjzEy6fqxhrT1F3SW/pYBiCZ6cdCDk10asiCgFwP9/edle3jBSwfdq4L6iDYEv/OEzXehb8tbB5wvEteZOhPiohay7pCeHYkl6S76ILAWgT4gp4XfTm4utRuamwlWcyjwUfMIEjTm0qOEi4rdrvvx0b7zyTT7/ugicCF4sMScCxRK+v4Ni6T9r7NVDEaUAsKWoVswZ4GsZ8FJQOaLHFA06tKrxWS68jsRLIDTgAgX9THdMTONOxG2sWPL3i7ERpwCAougPLCduKznEXcZ/sCIB35GBl4IucHT4wV+Hm4hGE4olfzBLk5VYEZkKYEURXFJwP6sk8QWo5xfmcr8d347Bx3xQLMFDgb+OYskfzNJkJVZEpAJU8DGfnlOMShKtbHzqCh+qg5eCvia4iuj29gezNFmJFRGvAICvKqF8hrfyMGtXPMM8FfTAwl3Euwh/MUuTlVjRIRQA0NBDRQm3kRdLc1ixxBpQgWCWJiuxol0qQMpjX3tvJZvt70t8gaHtZsf5EivatQJ0wbfHJGbbfYkOPEqz/fwVKyJKAS0VcOM4Y0xRMMUKRwFtIFY4CmgDscJRQBuIFY4C2kCscBTQBmKFo4A2ECscBbSBWNEuFQDWF9Sabmttwftes/VWYkW7VYDOr0fuMt03GKJjto+VWBExClDBOymz4+zKlaOtf7JkdoyVWBGRCgBmx/kSu5gdayVWOApQxC5mx1qJFY4CFLGL2bFWYoWjAEXsYnaslVjhKEARu5gdayVWOApQxC5mx1qJFSFRQHvH7CFbiTcwWqPVFRCpSmgpxi83m5+ReGzBQz25I9YiHlnwMbuYI+4SE5eofyfNwcHBf/AdXu1bvN3ElBP9eUIRTE4s2sLf/QNBXiMmPnkYpmf1HnUfpjom99cqBHzPrgM7/0om8mMxddHaD/TUM7r0EbMgSkwtkWmS09Mvu3sKpjpd+849gGmXl77k30RuLVqkAOVDyK4T/aTXW3djeuY9L85XT67Os+NOitlpYnoR/in78A94Ksv8I8s9Pt1Qx5cE2C6FLUZFxyWuMrY0H3vKj85czFewrMKku1yvT3/0+5flN4U5MbEJJ83uT6Iun9Nv5VExy1G3KfPP4t9Zr07iihWcrp/XNiYHulmAroAen206LmbNjgWX6+ux3O3DFe+x2bMUcaFbgDjedF+BhwKsLEBLzzlM5HmhTPU+1Gv+RD1OO4cH2vZoMfWNyYktFYD56M8TH4qJTZ7LFq+V68ymEl/bzYqgmNgdOV0H77z0jFsfT8OydqxfCsBU31+dRg9O3q+vw5TxvejYxCkxcUm9lHUcfV9tu30F+IueEAcHB4cwhfnoqHhdeCkvwWliaouu7y8qFrMB0WGKUS8KkC7bn5jAI3E9EOnWRr04pghTxltd357l8SF/uX+3uER4K/9i0rn7gM3pWCe3iempcjnq8+241ktM/leui3hsWkBPLJ/z3zU/F8suYj7f1ihmzThbPY+Xh+pSgLq9x4DNe8RsZOOPAjDTrf/qpZiqn4nV9pWcwuTCqH+Mn2EswnoSuPUwS0gwlpOPsEmzBfQZNx1T4OWcDg4ODg4ODg4ODg4O7RS0chxxpKXSrV/ij0SWaj+Y3YgqCAfs4KDTa0amaX6RIrJX+8DsBhwc/AER6szykchi4Y+ecAeHQMAHavS8JLJY+KMn3MEhUPS8JLJY+KMn3KEZhGdGnOwbxqXTn+fkUO8lgX8dqiOg5yWRxcIfPeEOzezZf9QVqxzG4GCNnpdEFgt/9IQ7eOfeaXvo5aUFNHBTGTX5+TmJjoCel0QWC3/0hDt4wht9vEbYRb8Ytot6Tt1DTy/Ioxlp1t/S6mjoeUlksfBHT7iDOfiELzcEIW+v9P05rY6GnpdEFgt/9IQ7OASKnpdEFgt/9IQ7mIPPFeG7gndOzaS3VhXR2OT9tCynhtYXOm/UJXpeElks/NET7uAJj14rvu8LgTHcMXk3PbMwj95YXtiiT8pGEnpeElks/NET7mDO3pqG5uekGMQr3xaIPRzUfAQRWSz80RPu4BAoel4SWSz80RPensFH0GvqPT+y3j3e+DCv5C9fZ4u5ZlIrjlCR8llqdIdePz6D/sXOOXnXftpSdIhyq92/pn/8xEn6cK3np0y7x3fMwkTPSyKLhT96wtsr5w21fqObV11Pdcc8DUSnL2v08mchXJ7zmDE8OT+HBm0so2mpxhesIX2WOm6Qjp6XRBYLf/SEt1eixWfYvYHfRZw4eZLYnyXzd1ex5yBjV7NnIv1/ZgznD0uhmydk0B9nZ9M7q63fE/i6TiSi5yWRxcIfPeHtmXxW2s/bXS2WDMrrjtNTC9w/AH7xyBQ6fKxJLBnxwM/XapIhW0vp3PhE9kzcDaLu2AmxB1Fj00l6bXmhWGomhhlNR0TPSyKLhT96wts7MaxGqD7aSMdONDGfvYn59el09HhzxgX3TM+kR+bm0sDNpbQk6yCllB+h0kPu3Zy5VQ306Nc5dIJldAjO18Mkc18/Lp1eWFxAIxMqaE1eLWVWHqX6RvfrdRT0vCSyWPijJ7y9cuEI6y/J/VVkaOneoK+/18xs+mR9Cc1Jr6LVBbW0ofAQNbLG7rML8sRR5vyC1RqjE8oNN4nJ+UNT6JkF+TR0WzktyqymdXsP0abiOrF3x0HPSyKLhT96wlsLZMD6xiZqYIJS1SipjVI2GP7zvax0t6Lq6HH6Lr+G3aOnr3/pqHS676s99MS8XFqRe5ByqurFUeb0nJZJV42W3+JsPhcM6wbWZniYtRmeX2RtSJGImo8gIouFP3rCgwF6YjawhujmokO0vaSOksoOc5cjveIIZe4/yjNZ/sEGKqxpoJJDx2gf89srjxxnGbWRDtafoEMNJ/jITX/pMdjcF39uYfOPYcrrjpn6+sjEiaWHxV5E145NE3PunD+sue1wy/hU5TxM+HmMczUyI+9o6HlJZLHwR094oCDTzk4/wBqmVbRoTzUtyz7IS9U1rPRFzwwMYhtzEZDRdu07TGnCILIP1HOjwRvZktpjVMYMouLwcTrADaKRG4Q/1LJjPlhbRJN2VXL35lCDebfoFaNTWQ3ABNORqWKtO0/Nz6V/ry2hWey+djBjLjcZHgHjhfHhPHDJHpiRJbZ0PPS8JLJY+KMnPBBGbN9HY5iPPD6pgibvrKTpKftpVtoBmptRRQszq2hpVjUtzzlIq/NqaB3zvTexzLmVGURCaR0lM4NIZbXEbtagzGIGkctqiQJWSxTDIFgtAYPYf6SRN3atmLf7AC+B4d7cPjmT3l1bTDNYGjYW1tEudv6U8qN8v+/Y9b2xn2VooLo3PadmUr91MIQqWs/OtYMZMX5xhrRacYmPNkokouclkcXCHz3h/lBc20AfryumTzeU0OebSil+SxkN27aPRu0op7GJFTQxuYKmshJ5Rup+mpNxgPfBL2a1xDeslliZW0Nr81ljVHObkGGt3CZv6K4NjOGmiRn09IIcemNlISvRi1nt4PtHMK98u9fjXDCG68Zl0KNzc6jPsgJ6c1URXf1lujjCQaLmI4jIYuGPnnB/eO3bAnprxV56dxXLZGuK6KPvinlvy8CNhkEM2VpGI3fsoy8Ty2kCM4gpzCC+YgZh5TZtMnGbdmtukzc8/XOj9wa/EU4Q/r5VL1LXOOP+0bXqeS5xPuWl3KNfu79/UEEtGCrqmAtZzJ5TEatNS0VteoDVpuiIaE30vCSyWPijJ9wuz8zPoecX5tJLi/PoH8vy6V/MIN5cvpfeYQbxATOIfswg/ru+mD7bWEKxm0u5QQxnbhO6FgNxm3YqbhMMwgpk4unM2FCDeAO9UjCMHHYuq6wxiNVwv2d+/iiWbm+gx2vAhlIaw2o/vJVuSwoP1rtqVIxvQtuFu5hK4ZFfbdSmMAi8PJTuZS0zlmC+09Dzkshi4Y+ecLv8aVYWPTInix6fm01PM4P4GzOI3swg+izNp1e/KaA3mEG8vXIvvb+6kD5kDdX+zG0aINymwcJtgkHAbZrEDGIaM4iZafvpa+Y2YWzOEmYQ3zKDWMUMAn48Grlbig0lw23SQdZDaWcG3gh/wjIpkK7SlWPS6ZVvCrmLtGHvIUoXbRKAH9J7A3kcRgn6MoP3RswXrRudQrqZ8nmpBYjsiFB75jJkmwu1KXMx0QnBe+VYLYFeuRo/OyDM0POSyGLhj55wu9wzdTf9fnomPTBjDz00aw/9hRnEX7/Opifn5dCzC3LphUV59PclefRPVktItwm1hO42xYlawl+3SQdvbX0Bg+P3qbQbbpmYSb2XFNCHrPE7aHMZu0YNb49YsSCzmq760rwLtbVZxDK82vYap9WqeF6y3SULkI2uGvUwr1Flz5xscxXVuLtNgaLnJZHFwh894XZJKDlEt09Mp99OzuAGcR8ziD98lUl/nLmH/jw7i/nL2fQEM4hnWMP0+UXMbWIG8bKoJdzdpiI3t+kL4TZZ9TahZNeBYq24fLSRaa18fRgFgEFasXOfca2sA96N5aIR5t2uLeEDVqu+J2pW+cz0QgSdEdNSKlmt2vy8lmVX8wJkrShA3NwmxcXME24TOiECQc9LIouFP3rC/eGtFQV0w7g0unlCGt3GDOJOZhA9p+zmAVphEA+yWsJwm7JdbtNzzG16kblN0iDwGwArtwkGIUs8uE1482sFzONPs7PpV8NT6MYJGV7fMxTX1HMjvmVCOm8XeGNm6gF+vuUsE3ljOysQ0BN115RM2m/yLiFYyOeG9pfaIYEeOvnMUIjILmtZq6puE9xM1W1CR4TuNu1hxn1YGShoFz0viSwW/ugJ9xc8rKvGpNBvvkyl68em0U3j0+hWlrnumIRMkUH3MoOA29RLcZseYwbxlDAIw23K527T64py/yOUi+5XNE7hAgTK3Sxzvr68kNUslaxErGUNwsCr/FAi3c3nmLupt79kzSpdTTwzte3lqlWFm6l2V0u3CR0Ras+cv+h5SWSx8EdPeKD8ZswuunwUa3COTqFrxqTSdWNTeS2Bkla6TT3hNolawpvb5Crt4DaJdsQKVnIFAjKCdHUuZu7JE/Nzqd+6UpYpWAnJXKz2hixM9E4J6W6il062v6TbhJpV7aFDLeHuNhm1hOo2eWt3+ULPSyKLhT96woPBJSOYQYzcRVcwg7ia1RLXslqCu02sltDdpvvhNjGDeFhzm/owpbYU9Hzw+5J+v2gMXzgc0SKsB9yFI65nJ9pgD7Bn9xB7dnrtqrpNKEhUt0m+2FTdJtQS0m3iLzWZQQSCnpdEFgt/9IS3Buh1uJ7VDC63iRmE6ja9zRSl/lgl2KAdAcM8b0gyV3p7BjXsjXh2rIbFs7ubuZyuNhgKE1a76r10cJteEW5T35XubpN8sSndJrS9jgSoCz0viSwW/ugJtwO619x6WUIggTCWKXgUc4Wa/HiftbmolvvJyazhiB4ojBvCyyeMbcKLJ3Qp1tSfsHzhFmyuGOVew3KXc1I63SV66qTbJGvXJ1gtofbSyfYXOiT03qaWoOclkcXCHz3hdmhvRnA+7/40XKG7p2bSs6x0fJuViN5ABkcjEu7BvAyjRwW+Mt5i4yUUXpahJwVvZNHXjqEc6GeH++VroF8wuRRup2yHMYNALSFrWOk2yVqCu02iltDdJjSug4Gel0QWC3/0hNuhPdYE+vsAvCQyA6Ui3AQ0JIduK+PuExqReHmHBiRe3OFNNvrc0ZuC7kW8gELXIvraMQIWb2MxLKMtGc8au5e52mGGQcBtQi2huk2yDfbSYu/jngJFz0sii4U/esLt0F7dIZXJOyvo/hlZfFQoXiyBqaz077PEaFD2Za4CfGf0rqCXCQ1JvK9AIxK9KuhmNHpUjD53jN2B24T+drhNVi/SWorZ8wim4OVZIOh5SWSx8EdPuB3auxHgDbSsEX45LIVunZTJ4w2hhERPC7ps4S5gcCAalOhyfH+10bsCvxkNSfSsoBGJXhUYEfrdl2ZX8yHicJswzqm1MHsewRTHCGwQCTWBGdN2VfD3GXjB98CMTN4fDx8aXba9WTsC/fBoUOLdBdymT5nbhO7GoduMrsZJycYQD+k2tRZmzyOY4hiBDSLVCAAakHjzLX1p9LbAj8bbbrzYe4rVEhhCjm5HvJhCDwv64P+ruU2tidnzCKY4RmCDSDYCCXpdrmKNzGtFjwu6IPGCD92PHm7TkjzewwK3aXVeYG+3/cHseQRTHCOwQTgYwcXDER2i9WP7IMocel3QDYl++RvHGW+9MS4KQ0DQHz+NtQvaErPnEUxxjMAG4WAEvmTUjjLTIdc6Zse2hvgDUo1BfmbnaQtxjMAG7cEIrAQ+vgyxbra9NcSMvKp6XqOZ7R9KcYzABu3dCByxFscIbOAYQWSLYwQ2cIwgssUxAhs4RhDZ4hiBDRwjiGxxjMAGjhFEtjhGYAPHCCJbHCOwgWMEkS2OEdggHIzAjBNNTfTW8gLT/dur4GUavuqjYrZfMMUxAhuEqxH4AqFEEAXD7HyhFvwazS5mxwdTHCOwQXs1AjPMzt1agg+JBAOzcwdTHCOwgWME/kswMTt/MMUxAhs4RuC/BBOz8wdTHCOwgWME/kswMTt/MMUxAhs4RuC/BBOz8wdTHCOwgWME/kswMTt/MMUxgg6GWSZoDQkmZucPpnR4I4A4OPgDfsZqlo9EFgt/zBIvRX9T6eCggpDvZvkGEh2fXCOyWPvA7CYccSRQiYlPThFZq/0RE59UZHZTjjhiR0Q2cnBwcHBwCDdQTf3wklsTIdGDtjaq1Vb0gE3fsMnHxpInZ9z22IhTzz53HebFcd/HfPSg7ScwDSKnRA/cFuxzukDaTz2jSx+x6ELcU5SxFBzU5wvcnvfALUNOv+zuKWLRJ1HvL72y2/tL61z6Y89IP397IyTp1y/aY2hKtFwnjQDLXZ4YUNT1zdnV6v4xg5M+P/OeF+djXqznRqDug3lITNz2pqjeQ0vlstjMkesgP3v0473e9hGzrv3Pfm1KdZcnBhaIbacYWw2wrtt7i+qiXp1cifkecYleeyOw3Y4RRMclHce6s56OK+3Wf1W92H6qsdUA6yBdnhpUHBO3o0kus03d5XZMewzc+m73f397BMuYslWnSyMwjv+0+OzXvzog9zcDRtC179wDYpHTPT5pXNf3FlSLxU4xsUmpOMdZfcZWdvtg0WHMn/n6uM5iM09Plz5jKmJit/O0itWd2PNqwHLnZ78oO6ff8qPqNgnW8eOf+KxEHi/2u0DdHvXP8UhjL6zrEZdUi3U4L7tuOeZ7xCe40otllq9SY2J3nDzjlseT+faBmzPE5tYBF2GTJ5n8kcmlWCeBEfz4gTd2ikUX4hjbRiBm3fBjnx8p8xyz/eW67l8k3RgTu60pJi65UBer65z6v51fY7M/VUXsz40A8+gY8DhvfFLjD7pf+KjcB1Mdsd7NCCTqMowg6p9ji8SiC2/n5UbwzvwqbI96ZRKOu9rYYoD1PQZtK9fT3CM+sSbqhZHrsU/05wketQeWY+ISmRG7Hyf2e0Lug6mOWO8yAkwl3o5RMdtHrOtiLLUCVgnz5g7JY9rICDi+9ve1vWtswh0xcQk7xKIb2N9XTYD5H/f8+32YVzG5rluNBMR6W0Zg5g7px0j0mkC//jnvzlvc9Z0Fm8SiC2/nk+u7D9hY9rMXhr3NVyqox4n57xlLzYj1Xo2g89/i/iYWXUT1Sz9DzHocA8S6aGOpdegrpmacywQ1hI485idMHjNmO73ORCrAdU7lpj754aW3J7PpVGOxGWWfZ8U+85hwg1JQ02mWZn3d6UzmnnbBdQlsGsvXeOcaJrcZs270ZuJSkODJ0y64BudcyeQSvsaTAeI+JhuLnR5kgmcF9MylpvssJrxW0TC7X4BM+C9j1sU5TPRr3HbKmWdvYNO1TO7ma5q58LQLbsD9eOiF8chpF9+UxKarmFzB13jSX9NrTybShfSW7js7nX4m2pJID56xitkxjzNB7dw+MbNsHTv7ODg4ODg4uBMTn/weahBdxGZZu/zaWPJd24jt9xhLwUGmCRITl+TWi9PWiPu73lhyiAhgBJ2fi/fouQKswc676zCFYJ3IBJ1YZuS9IXJZIvdTkfvxfQdt+rFYzZHruw7Y6jVj8eMU9GUgz2O2TSK3x8QlnsSymlbMd/8iqRe2n3FdL1cbRR4Dkcts4hhBJAEj6Nb/u4Zz3pxTo4rYLJWu1wRuJb1Yh4aknOeo82bExCae6Pz8EDQALcF5ogdu5i8gIT0+2zJSbJLXiDGWmlGvrc6riPX8/YS+j4/zOkYQSVjVBEAo3dIdEut4Zla3m+3rhVOt9tW39fhsa9KPbn6U9897O06s/7ky74FYb2UEHoj1jhFEEsFoE4h1HkYAonoPL8E6Kd0/XX+crUa3a6eoV6e4RtP+9A+v5WKdGerxELbqeWOLwQ8uvH64ur1z7+HlYpOLnz3zeY66T6dTvjedT70YAfjBL3/zmXrMDy+6ZYfYzzECBwcHB4eIpFOn/wcOUjVeMu//UgAAAABJRU5ErkJggg==" /></p>
</td>
</td>
<td style="width: 83.4106%; border-top-width: 1pt; border-right-width: 1pt; border-bottom-width: 1pt; border-top-color: windowtext; border-right-color: windowtext; border-bottom-color: windowtext; border-left: none; padding: 0cm 5.4pt; height: 27px;" colspan="3" width="82%">
<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; line-height: normal; layout-grid-mode: char;" align="center"><strong><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-size: 11.0pt; mso-fareast-font-family: 'Times New Roman';">PHREB ACCREDITATION FORMS</span></strong></p>
</td>
</tr>
<tr style="height: 10px;">
<td style="width: 52.04%; border-top: none; border-left: none; border-bottom-width: 1pt; border-bottom-color: windowtext; border-right-width: 1pt; border-right-color: windowtext; padding: 0cm 5.4pt; height: 27px;" rowspan="4" width="52%">
<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; line-height: normal;" align="center"><strong><span lang="EN-US" style="font-size: 12.0pt; mso-bidi-font-size: 11.0pt; text-transform: uppercase;">LEVEL 2 PHREB ACCREDITOR’S ASSESSMENT FORM</span></strong></p>
</td>
<td style="width: 15.56%; border-top: none; border-left: none; border-bottom-width: 1pt; border-bottom-color: windowtext; border-right-width: 1pt; border-right-color: windowtext; padding: 0cm 5.4pt; height: 10px;" width="15%">
<p class="MsoNormalCxSpMiddle" style="margin-bottom: .0001pt; mso-add-space: auto; line-height: normal; layout-grid-mode: char;"><span lang="EN-US">Form No.</span></p>
</td>
<td style="width: 15.8106%; border-top: none; border-left: none; border-bottom-width: 1pt; border-bottom-color: windowtext; border-right-width: 1pt; border-right-color: windowtext; padding: 0cm 5.4pt; height: 10px;" width="14%">
<p class="MsoNormalCxSpMiddle" style="margin-bottom: .0001pt; mso-add-space: auto; text-align: center; line-height: normal;" align="center"><span lang="EN-US" style="color: red;">4.2.2</span></p>
</td>
</tr>
<tr style="height: 3px;">
<td style="width: 15.56%; border-top: none; border-left: none; border-bottom-width: 1pt; border-bottom-color: windowtext; border-right-width: 1pt; border-right-color: windowtext; padding: 0cm 5.4pt; height: 3px;" width="15%">
<p class="MsoNormalCxSpMiddle" style="margin-bottom: .0001pt; mso-add-space: auto; line-height: normal; layout-grid-mode: char;"><span lang="EN-US">Version No.</span></p>
</td>
<td style="width: 15.8106%; border-top: none; border-left: none; border-bottom-width: 1pt; border-bottom-color: windowtext; border-right-width: 1pt; border-right-color: windowtext; padding: 0cm 5.4pt; height: 3px;" width="14%">
<p class="MsoNormalCxSpMiddle" style="margin-bottom: .0001pt; mso-add-space: auto; text-align: center; line-height: normal;" align="center"><span lang="EN-US" style="color: red;">02</span></p>
</td>
</tr>
<tr style="height: 7px;">
<td style="width: 15.56%; border-top: none; border-left: none; border-bottom-width: 1pt; border-bottom-color: windowtext; border-right-width: 1pt; border-right-color: windowtext; padding: 0cm 5.4pt; height: 7px;" width="15%">
<p class="MsoNormalCxSpMiddle" style="margin-bottom: .0001pt; mso-add-space: auto; line-height: normal; layout-grid-mode: char;"><span lang="EN-US">Version Date</span></p>
</td>
<td style="width: 15.8106%; border-top: none; border-left: none; border-bottom-width: 1pt; border-bottom-color: windowtext; border-right-width: 1pt; border-right-color: windowtext; padding: 0cm 5.4pt; height: 7px;" width="14%">
<p class="MsoNormalCxSpMiddle" style="margin-bottom: .0001pt; mso-add-space: auto; text-align: center; line-height: normal;" align="center"><span lang="EN-US" style="color: red;">26 April 2016</span></p>
</td>
</tr>
<tr style="height: 7px;">
<td style="width: 15.56%; border-top: none; border-left: none; border-bottom-width: 1pt; border-bottom-color: windowtext; border-right-width: 1pt; border-right-color: windowtext; padding: 0cm 5.4pt; height: 7px;" width="15%">
<p class="MsoNormalCxSpMiddle" style="margin-bottom: .0001pt; mso-add-space: auto; line-height: normal; layout-grid-mode: char;"><span lang="EN-US">Page</span></p>
</td>
<td style="width: 15.8106%; border-top: none; border-left: none; border-bottom-width: 1pt; border-bottom-color: windowtext; border-right-width: 1pt; border-right-color: windowtext; padding: 0cm 5.4pt; height: 7px;" width="14%">
<p class="MsoNormalCxSpMiddle" style="margin-bottom: .0001pt; mso-add-space: auto; text-align: center; line-height: normal;" align="center"><br /><!-- [if supportFields]><b
  style='mso-bidi-font-weight:normal'><span lang=EN-US style='color:red'><span
  style='mso-element:field-end'></span></span></b><![endif]--></p>
</td>
</tr>
</tbody>
</table><br>
<p class=MsoNormal><span lang=EN-US style='font-family:"Palatino Linotype",serif'>&nbsp;</span></p>

<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=906
 style='width:679.5pt;margin-left:5.4pt;border-collapse:collapse;border:none'>
 <tr>
  <td width=198 colspan=2 style='width:148.5pt;border:solid black 1.0pt;
  background:#E5E5E5;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><b><span lang=EN-US style='font-family:"Palatino Linotype",serif'>Name
  of Research Ethics Committee:</span></b></p>
  </td>
  <td width=708 colspan=9 style='width:531.0pt;border:solid black 1.0pt;
  border-left:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoListParagraphCxSpFirst style='margin:0cm;margin-bottom:.0001pt;
  line-height:normal'><span lang=EN-US style='font-family:"Palatino Linotype",serif'>$rec_institution - $rec_name</span></p>
  <p class=MsoListParagraphCxSpLast style='margin:0cm;margin-bottom:.0001pt;
  line-height:normal'><span lang=EN-US style='font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr>
  <td width=198 colspan=2 style='width:148.5pt;border:solid black 1.0pt;
  border-top:none;background:#E5E5E5;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><b><span lang=EN-US style='font-family:"Palatino Linotype",serif'>Name
  of Institution:</span></b></p>
  </td>
  <td width=708 colspan=9 style='width:531.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoListParagraphCxSpFirst style='margin:0cm;margin-bottom:.0001pt;
  line-height:normal'><span lang=EN-US style='font-family:"Palatino Linotype",serif'>$rec_institution</span></p>
  <p class=MsoListParagraphCxSpLast style='margin:0cm;margin-bottom:.0001pt;
  line-height:normal'><span lang=EN-US style='font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr>
  <td width=198 colspan=2 style='width:148.5pt;border:solid black 1.0pt;
  border-top:none;background:#E5E5E5;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><b><span lang=EN-US style='font-size:10.0pt;font-family:
  "Palatino Linotype",serif'>Address of the REC (Street, Town/City, Province,
  Region):</span></b></p>
  </td>
  <td width=354 colspan=5 style='width:265.5pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoListParagraphCxSpFirst style='margin:0cm;margin-bottom:.0001pt;
  line-height:normal'><span lang=EN-US style='font-family:"Palatino Linotype",serif'>$rec_address</span></p>
  </td>
  <td width=177 colspan=3 style='width:132.75pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoListParagraphCxSpMiddle style='margin:0cm;margin-bottom:.0001pt;
  line-height:normal'><b><span lang=EN-US style='font-family:"Palatino Linotype",serif'>Year
  Established</span></b></p>
  </td>
  <td width=177 style='width:132.75pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoListParagraphCxSpLast style='margin:0cm;margin-bottom:.0001pt;
  line-height:normal'><span lang=EN-US style='font-family:"Palatino Linotype",serif'>$rec_dateestablished</span></p>
  </td>
 </tr>
 <tr style='height:28.8pt'>
  <td width=198 colspan=2 style='width:148.5pt;border:solid black 1.0pt;
  border-top:none;background:#E5E5E5;padding:0cm 5.4pt 0cm 5.4pt;height:28.8pt'>
  <p class=MsoListParagraphCxSpFirst style='margin:0cm;margin-bottom:.0001pt;
  line-height:normal'><b><span lang=EN-US style='font-family:"Palatino Linotype",serif'>Name
  of Contact Person:</span></b></p>
  </td>
  <td width=180 colspan=2 style='width:135.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:28.8pt'>
  <p class=MsoListParagraphCxSpMiddle style='margin:0cm;margin-bottom:.0001pt;
  line-height:normal'><span lang=EN-US style='font-family:"Palatino Linotype",serif'>$rec_contactperson</span></p>
  </td>
  <td width=132 colspan=2 style='width:99.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;background:
  #E5E5E5;padding:0cm 5.4pt 0cm 5.4pt;height:28.8pt'>
  <p class=MsoListParagraphCxSpMiddle style='margin:0cm;margin-bottom:.0001pt;
  line-height:normal'><b><span lang=EN-US style='font-family:"Palatino Linotype",serif'>Designation:</span></b></p>
  </td>
  <td width=396 colspan=5 style='width:297.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:28.8pt'>
  <p class=MsoListParagraphCxSpLast style='margin:0cm;margin-bottom:.0001pt;
  line-height:normal'><span lang=EN-US style='font-family:"Palatino Linotype",serif'>$rec_contactposition</span></p>
  </td>
 </tr>
 <tr style='height:28.8pt'>
  <td width=96 style='width:72.0pt;border:solid black 1.0pt;border-top:none;
  background:#E5E5E5;padding:0cm 5.4pt 0cm 5.4pt;height:28.8pt'>
  <p class=MsoListParagraphCxSpFirst style='margin:0cm;margin-bottom:.0001pt;
  line-height:normal'><b><span lang=EN-US style='font-family:"Palatino Linotype",serif'>Telephone:</span></b></p>
  </td>
  <td width=206 colspan=2 style='width:154.5pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:28.8pt'>
  <p class=MsoListParagraphCxSpMiddle style='margin:0cm;margin-bottom:.0001pt;
  line-height:normal'><b><span lang=EN-US style='font-family:"Palatino Linotype",serif'>$rec_telno</span></b></p>
  </td>
  <td width=96 colspan=2 style='width:72.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;background:
  #E5E5E5;padding:0cm 5.4pt 0cm 5.4pt;height:28.8pt'>
  <p class=MsoListParagraphCxSpMiddle style='margin:0cm;margin-bottom:.0001pt;
  line-height:normal'><b><span lang=EN-US style='font-family:"Palatino Linotype",serif'>$rec_contactmobileno</span></b></p>
  </td>
  <td width=226 colspan=3 style='width:169.5pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:28.8pt'>
  <p class=MsoListParagraphCxSpMiddle style='margin:0cm;margin-bottom:.0001pt;
  line-height:normal'><span lang=EN-US style='font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=96 style='width:72.0pt;border-top:none;border-left:none;border-bottom:
  solid black 1.0pt;border-right:solid black 1.0pt;background:#D9D9D9;
  padding:0cm 5.4pt 0cm 5.4pt;height:28.8pt'>
  <p class=MsoListParagraphCxSpMiddle style='margin:0cm;margin-bottom:.0001pt;
  line-height:normal'><b><span lang=EN-US style='font-family:"Palatino Linotype",serif'>Email:</span></b></p>
  </td>
  <td width=186 colspan=2 style='width:139.5pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:28.8pt'>
  <p class=MsoListParagraphCxSpLast style='margin:0cm;margin-bottom:.0001pt;
  line-height:normal'><span lang=EN-US style='font-family:"Palatino Linotype",serif'>$rec_email/$rec_contactemail/$rec_chairemail</span></p>
  </td>
 </tr>
 <tr height=0>
  <td width=96 style='border:none'></td>
  <td width=102 style='border:none'></td>
  <td width=104 style='border:none'></td>
  <td width=76 style='border:none'></td>
  <td width=20 style='border:none'></td>
  <td width=112 style='border:none'></td>
  <td width=42 style='border:none'></td>
  <td width=72 style='border:none'></td>
  <td width=96 style='border:none'></td>
  <td width=9 style='border:none'></td>
  <td width=177 style='border:none'></td>
 </tr>
</table>

<p class=MsoNormal><span lang=EN-US style='font-family:"Palatino Linotype",serif'>&nbsp;</span></p>

<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=912
 style='width:684.0pt;margin-left:2.75pt;border-collapse:collapse;border:none'>
 <tr>
  <td width=180 style='width:135.0pt;border:solid black 1.0pt;background:#D9D9D9;
  padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents align=center style='text-align:center'><b><span
  lang=EN-US style='font-family:"Palatino Linotype",serif'>CRITERIA</span></b></p>
  </td>
  <td width=228 style='width:171.0pt;border:solid black 1.0pt;border-left:none;
  background:#D9D9D9;padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents align=center style='text-align:center'><b><span
  lang=EN-US style='font-family:"Palatino Linotype",serif'>INDICATORS</span></b></p>
  </td>
  <td width=192 style='width:144.0pt;border:solid black 1.0pt;border-left:none;
  background:#D9D9D9;padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents align=center style='text-align:center'><b><span
  lang=EN-US style='font-family:"Palatino Linotype",serif'>SOURCE/ PHYSICAL
  EVIDENCE</span></b></p>
  </td>
  <td width=156 style='width:117.0pt;border:solid black 1.0pt;border-left:none;
  background:#D9D9D9;padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents align=center style='text-align:center'><b><span
  lang=EN-US style='font-family:"Palatino Linotype",serif'>FINDINGS</span></b></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border:solid black 1.0pt;
  border-left:none;background:#D9D9D9;padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents><b><span lang=EN-US style='font-family:"Palatino Linotype",serif'>RECOMMENDATION</span></b></p>
  </td>
 </tr>
 <tr style='height:20.85pt'>
  <td width=180 rowspan=18 style='width:135.0pt;border:solid black 1.0pt;
  border-top:none;padding:2.75pt 2.75pt 2.75pt 2.75pt;height:20.85pt'>
  <p class=TableContents style='margin-left:15.25pt;text-indent:-13.5pt'><b><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>1.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></b><b><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>FUNCTIONALITY
  OF STRUCTURE AND MEMBERSHIP</span></b></p>
  </td>
  <td width=228 rowspan=5 style='width:171.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:20.85pt'>
  <p class=TableContents style='margin-left:10.75pt'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>Independence</span></p>
  </td>
  <td width=192 rowspan=5 style='width:144.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:20.85pt'>
  <p class=TableContents><b><span lang=EN-US style='font-size:11.0pt;
  font-family:"Palatino Linotype",serif'>Application Form /Membership File</span></b></p>
  <p class=TableContents style='margin-left:23.65pt;text-indent:-13.5pt'><span
  lang=EN-US style='font-size:11.0pt;font-family:Symbol'>·<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>Organizational
  Structure</span></p>
  <p class=TableContents style='margin-left:23.65pt;text-indent:-13.5pt'><span
  lang=EN-US style='font-size:11.0pt;font-family:Symbol'>·<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>Non-membership
  of officials</span></p>
  <p class=TableContents style='margin-left:23.65pt;text-indent:-13.5pt'><span
  lang=EN-US style='font-size:11.0pt;font-family:Symbol'>·<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>Non-affiliated
  member</span></p>
  <p class=TableContents style='margin-left:23.65pt;text-indent:-13.5pt'><span
  lang=EN-US style='font-size:11.0pt;font-family:Symbol'>·<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>Mandate
  of the REC</span></p>
  </td>
  <td width=156 style='width:117.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:2.75pt 2.75pt 2.75pt 2.75pt;
  height:20.85pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:20.85pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:20.85pt'>
  <td width=156 style='width:117.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:2.75pt 2.75pt 2.75pt 2.75pt;
  height:20.85pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:20.85pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:20.85pt'>
  <td width=156 style='width:117.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:2.75pt 2.75pt 2.75pt 2.75pt;
  height:20.85pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:20.85pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:20.85pt'>
  <td width=156 style='width:117.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:2.75pt 2.75pt 2.75pt 2.75pt;
  height:20.85pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:20.85pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:20.85pt'>
  <td width=156 style='width:117.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:2.75pt 2.75pt 2.75pt 2.75pt;
  height:20.85pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:20.85pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:20.85pt'>
  <td width=228 rowspan=5 style='width:171.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:20.85pt'>
  <p class=TableContents style='margin-left:10.75pt'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>Multi-disciplinary</span></p>
  </td>
  <td width=192 rowspan=5 style='width:144.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:20.85pt'>
  <p class=TableContents><b><span lang=EN-US style='font-size:11.0pt;
  font-family:"Palatino Linotype",serif'>Application Form /Membership File</span></b></p>
  <p class=TableContents style='margin-left:23.65pt;text-indent:-13.5pt'><span
  lang=EN-US style='font-size:11.0pt;font-family:Symbol'>·<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>Expertise
  of members  reflects usual topic of proposals for review</span></p>
  <p class=TableContents style='margin-left:23.65pt;text-indent:-13.5pt'><span
  lang=EN-US style='font-size:11.0pt;font-family:Symbol'>·<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>Presence
  of non-scientist member</span></p>
  </td>
  <td width=156 style='width:117.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:2.75pt 2.75pt 2.75pt 2.75pt;
  height:20.85pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:20.85pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:20.85pt'>
  <td width=156 style='width:117.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:2.75pt 2.75pt 2.75pt 2.75pt;
  height:20.85pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:20.85pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:20.85pt'>
  <td width=156 style='width:117.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:2.75pt 2.75pt 2.75pt 2.75pt;
  height:20.85pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:20.85pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:20.85pt'>
  <td width=156 style='width:117.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:2.75pt 2.75pt 2.75pt 2.75pt;
  height:20.85pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:20.85pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:20.85pt'>
  <td width=156 style='width:117.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:2.75pt 2.75pt 2.75pt 2.75pt;
  height:20.85pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:20.85pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr>
  <td width=228 style='width:171.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents style='margin-left:10.75pt'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>Gender
  representation</span></p>
  </td>
  <td width=192 style='width:144.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents style='margin-left:10.15pt;text-indent:-10.15pt'><b><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>Membership
  File</span></b></p>
  </td>
  <td width=156 style='width:117.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:79.8pt'>
  <td width=228 style='width:171.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:2.75pt 2.75pt 2.75pt 2.75pt;
  height:79.8pt'>
  <p class=TableContents style='margin-left:10.75pt'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>Age
  representation</span></p>
  </td>
  <td width=192 style='width:144.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:2.75pt 2.75pt 2.75pt 2.75pt;
  height:79.8pt'>
  <p class=TableContents><b><span lang=EN-US style='font-size:11.0pt;
  font-family:"Palatino Linotype",serif'>Application Form /Membership File</span></b></p>
  <p class=TableContents style='margin-left:23.65pt;text-indent:-13.5pt'><span
  lang=EN-US style='font-size:11.0pt;font-family:Symbol'>·<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>Membership
  roster includes age range of members</span></p>
  </td>
  <td width=156 style='width:117.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:2.75pt 2.75pt 2.75pt 2.75pt;
  height:79.8pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:79.8pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:6.0pt'>
  <td width=228 rowspan=5 style='width:171.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:6.0pt'>
  <p class=TableContents style='margin-left:10.75pt'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>Training</span></p>
  </td>
  <td width=192 rowspan=5 style='width:144.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:6.0pt'>
  <p class=TableContents style='margin-left:10.15pt;text-indent:-10.15pt'><b><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>Ethics-related
  Training of REC members </span></b></p>
  </td>
  <td width=156 style='width:117.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:2.75pt 2.75pt 2.75pt 2.75pt;
  height:6.0pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:6.0pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:6.0pt'>
  <td width=156 style='width:117.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:2.75pt 2.75pt 2.75pt 2.75pt;
  height:6.0pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:6.0pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:6.0pt'>
  <td width=156 style='width:117.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:2.75pt 2.75pt 2.75pt 2.75pt;
  height:6.0pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:6.0pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:6.0pt'>
  <td width=156 style='width:117.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:2.75pt 2.75pt 2.75pt 2.75pt;
  height:6.0pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:6.0pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:6.0pt'>
  <td width=156 style='width:117.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:2.75pt 2.75pt 2.75pt 2.75pt;
  height:6.0pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:6.0pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:62.7pt'>
  <td width=228 style='width:171.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:2.75pt 2.75pt 2.75pt 2.75pt;
  height:62.7pt'>
  <p class=TableContents style='margin-left:10.75pt'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>Management of
  conflict of interest</span></p>
  </td>
  <td width=192 style='width:144.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:2.75pt 2.75pt 2.75pt 2.75pt;
  height:62.7pt'>
  <p class=TableContents style='margin-left:10.15pt;text-indent:-10.15pt'><b><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>Self-Assessment
  Checklist</span></b></p>
  <p class=TableContents style='text-indent:4.5pt'><b><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>SOP Manual</span></b></p>
  <p class=TableContents style='margin-left:23.65pt;text-indent:-13.5pt'><span
  lang=EN-US style='font-size:11.0pt;font-family:Symbol'>·<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>SOP
  on management of COI</span></p>
  </td>
  <td width=156 style='width:117.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:2.75pt 2.75pt 2.75pt 2.75pt;
  height:62.7pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:62.7pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:37.95pt'>
  <td width=180 style='width:135.0pt;border:solid black 1.0pt;border-top:none;
  background:#D0CECE;padding:2.75pt 2.75pt 2.75pt 2.75pt;height:37.95pt'>
  <p class=TableContents align=center style='text-align:center'><b><span
  lang=EN-US style='font-family:"Palatino Linotype",serif'>CRITERIA</span></b></p>
  </td>
  <td width=228 style='width:171.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;background:
  #D0CECE;padding:2.75pt 2.75pt 2.75pt 2.75pt;height:37.95pt'>
  <p class=TableContents align=center style='text-align:center'><b><span
  lang=EN-US style='font-family:"Palatino Linotype",serif'>INDICATORS</span></b></p>
  </td>
  <td width=192 style='width:144.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;background:
  #D0CECE;padding:2.75pt 2.75pt 2.75pt 2.75pt;height:37.95pt'>
  <p class=TableContents align=center style='text-align:center'><b><span
  lang=EN-US style='font-family:"Palatino Linotype",serif'>SOURCE/ PHYSICAL
  EVIDENCE</span></b></p>
  </td>
  <td width=156 style='width:117.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;background:
  #D0CECE;padding:2.75pt 2.75pt 2.75pt 2.75pt;height:37.95pt'>
  <p class=TableContents align=center style='text-align:center'><b><span
  lang=EN-US style='font-family:"Palatino Linotype",serif'>FINDINGS</span></b></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  background:#D0CECE;padding:2.75pt 2.75pt 2.75pt 2.75pt;height:37.95pt'>
  <p class=TableContents><b><span lang=EN-US style='font-family:"Palatino Linotype",serif'>RECOMMENDATION</span></b></p>
  </td>
 </tr>
 <tr style='height:37.95pt'>
  <td width=180 rowspan=3 style='width:135.0pt;border:solid black 1.0pt;
  border-top:none;padding:2.75pt 2.75pt 2.75pt 2.75pt;height:37.95pt'>
  <p class=TableContents style='margin-left:15.25pt;text-indent:-13.5pt'><b><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>2.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></b><b><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>ADHERENCE
  TO INTERNATIONAL, NATIONAL, AND INSTITUTIONAL GUIDELINES AND POLICIES</span></b></p>
  </td>
  <td width=228 style='width:171.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:2.75pt 2.75pt 2.75pt 2.75pt;
  height:37.95pt'>
  <p class=TableContents style='margin-left:10.75pt'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>Membership
  structure</span></p>
  </td>
  <td width=192 style='width:144.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:2.75pt 2.75pt 2.75pt 2.75pt;
  height:37.95pt'>
  <p class=TableContents><b><span lang=EN-US style='font-size:11.0pt;
  font-family:"Palatino Linotype",serif'>Self-Assessment Checklist</span></b></p>
  </td>
  <td width=156 style='width:117.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:2.75pt 2.75pt 2.75pt 2.75pt;
  height:37.95pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:37.95pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:39.3pt'>
  <td width=228 style='width:171.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:2.75pt 2.75pt 2.75pt 2.75pt;
  height:39.3pt'>
  <p class=TableContents style='margin-left:10.75pt'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>Policy on
  review of researches involving human participants</span></p>
  </td>
  <td width=192 style='width:144.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:2.75pt 2.75pt 2.75pt 2.75pt;
  height:39.3pt'>
  <p class=TableContents><b><span lang=EN-US style='font-size:11.0pt;
  font-family:"Palatino Linotype",serif'>SOP Manual</span></b></p>
  </td>
  <td width=156 style='width:117.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:2.75pt 2.75pt 2.75pt 2.75pt;
  height:39.3pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:39.3pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:48.3pt'>
  <td width=228 style='width:171.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:2.75pt 2.75pt 2.75pt 2.75pt;
  height:48.3pt'>
  <p class=TableContents style='margin-left:10.75pt'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>Regularity of
  meetings Quorum</span></p>
  </td>
  <td width=192 style='width:144.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:2.75pt 2.75pt 2.75pt 2.75pt;
  height:48.3pt'>
  <p class=TableContents><b><span lang=EN-US style='font-size:11.0pt;
  font-family:"Palatino Linotype",serif'>Minutes of Meetings</span></b></p>
  <p class=TableContents><b><span lang=EN-US style='font-size:11.0pt;
  font-family:"Palatino Linotype",serif'>&nbsp;</span></b></p>
  </td>
  <td width=156 style='width:117.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:2.75pt 2.75pt 2.75pt 2.75pt;
  height:48.3pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:48.3pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:19.05pt'>
  <td width=180 style='width:135.0pt;border:solid black 1.0pt;border-top:none;
  background:#D0CECE;padding:2.75pt 2.75pt 2.75pt 2.75pt;height:19.05pt'>
  <p class=TableContents align=center style='text-align:center'><b><span
  lang=EN-US style='font-family:"Palatino Linotype",serif'>CRITERIA</span></b></p>
  </td>
  <td width=228 style='width:171.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;background:
  #D0CECE;padding:2.75pt 2.75pt 2.75pt 2.75pt;height:19.05pt'>
  <p class=TableContents align=center style='text-align:center'><b><span
  lang=EN-US style='font-family:"Palatino Linotype",serif'>INDICATORS</span></b></p>
  </td>
  <td width=192 style='width:144.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;background:
  #D0CECE;padding:2.75pt 2.75pt 2.75pt 2.75pt;height:19.05pt'>
  <p class=TableContents align=center style='text-align:center'><b><span
  lang=EN-US style='font-family:"Palatino Linotype",serif'>SOURCE/ PHYSICAL
  EVIDENCE</span></b></p>
  </td>
  <td width=156 style='width:117.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;background:
  #D0CECE;padding:2.75pt 2.75pt 2.75pt 2.75pt;height:19.05pt'>
  <p class=TableContents align=center style='text-align:center'><b><span
  lang=EN-US style='font-family:"Palatino Linotype",serif'>FINDINGS</span></b></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  background:#D0CECE;padding:2.75pt 2.75pt 2.75pt 2.75pt;height:19.05pt'>
  <p class=TableContents><b><span lang=EN-US style='font-family:"Palatino Linotype",serif'>RECOMMENDATION</span></b></p>
  </td>
 </tr>
 <tr style='height:101.85pt'>
  <td width=180 rowspan=10 style='width:135.0pt;border:solid black 1.0pt;
  border-top:none;padding:2.75pt 2.75pt 2.75pt 2.75pt;height:101.85pt'>
  <p class=TableContents style='margin-left:15.25pt;text-indent:-13.5pt'><b><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>3.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></b><b><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>ADEQUACY
  OF THE STANDARD OPERATING PROCEDURES (SOPS) AND CONSISTENCY IN ITS IMPLEMENTATION</span></b></p>
  </td>
  <td width=228 valign=top style='width:171.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:101.85pt'>
  <p class=MsoNormal style='margin-left:19.75pt;text-indent:-19.75pt'><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>SOP
  1 – ERC Structure and Composition</span></p>
  <p class=MsoNormal style='margin-left:19.75pt;text-indent:-19.75pt'><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>1.1
  Selection and Appointment of Members</span></p>
  <p class=MsoNormal style='margin-left:19.75pt;text-indent:-19.75pt'><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>1.2
  Designation of Officers</span></p>
  <p class=MsoNormal style='margin-left:19.75pt;text-indent:-19.75pt'><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>1.3
  Appointment of Independent Consultants</span></p>
  </td>
  <td width=192 rowspan=10 style='width:144.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:101.85pt'>
  <p class=TableContents><b><span lang=EN-US style='font-size:11.0pt;
  font-family:"Palatino Linotype",serif'>SOP Manual</span></b></p>
  <p class=TableContents><b><span lang=EN-US style='font-size:11.0pt;
  font-family:"Palatino Linotype",serif'>Minutes of Meetings (3)</span></b></p>
  <p class=TableContents><b><span lang=EN-US style='font-size:11.0pt;
  font-family:"Palatino Linotype",serif'>Protocol Files (3)</span></b></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:101.85pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:101.85pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:41.55pt'>
  <td width=228 valign=top style='width:171.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:41.55pt'>
  <p class=MsoNormal style='margin-left:19.75pt;text-indent:-19.75pt'><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>SOP
  2 – Management of Initial Submissions and Resubmissions</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:41.55pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:41.55pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:131.55pt'>
  <td width=228 valign=top style='width:171.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:131.55pt'>
  <p class=MsoNormal style='margin-left:19.75pt;text-indent:-19.75pt'><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>SOP
  3 – Management of Post Approval Submissions</span></p>
  <p class=MsoNormal style='margin-left:19.75pt;text-indent:-19.75pt'><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>3.1
  Review of Progress, Final, and Early Termination Reports, and Protocol
  Amendments</span></p>
  <p class=MsoNormal><span lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>3.2
  Review of SAE/SUSAR Reports 3.3 Review of Protocol Deviations and Violations</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:131.55pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:131.55pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:46.05pt'>
  <td width=228 valign=top style='width:171.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:46.05pt'>
  <p class=MsoNormal style='margin-left:19.75pt;text-indent:-19.75pt'><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>SOP
  4 –Review Procedures</span></p>
  <p class=MsoNormal style='margin-left:19.75pt;text-indent:-19.75pt'><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>4.1
  Expedited Review</span></p>
  <p class=MsoNormal style='margin-left:19.75pt;text-indent:-19.75pt'><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>4.2
  Full Review</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:46.05pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:46.05pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:93.3pt'>
  <td width=228 valign=top style='width:171.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:93.3pt'>
  <p class=MsoNormal style='margin-left:19.75pt;text-indent:-19.75pt'><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>SOP
  5 – Meeting Procedures</span></p>
  <p class=MsoNormal style='margin-left:19.75pt;text-indent:-19.75pt'><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>5.1
  Preparing for a Meeting</span></p>
  <p class=MsoNormal style='margin-left:19.75pt;text-indent:-19.75pt'><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>5.2
  Preparing the Meeting Agenda</span></p>
  <p class=MsoNormal style='margin-left:19.75pt;text-indent:-19.75pt'><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>5.3
  Conduct of Regular and Special Meetings</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:93.3pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:93.3pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:1.0pt'>
  <td width=228 valign=top style='width:171.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:1.0pt'>
  <p class=MsoNormal style='margin-left:19.75pt;text-indent:-19.75pt'><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>SOP
  6 – Documentation of REC Actions</span></p>
  <p class=MsoNormal style='margin-left:19.75pt;text-indent:-19.75pt'><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>6.1
  Managing the Meeting Minutes</span></p>
  <p class=MsoNormal style='margin-left:19.75pt;text-indent:-19.75pt'><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>6.2
  Communicating REC Decisions</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:1.0pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:1.0pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:47.55pt'>
  <td width=228 valign=top style='width:171.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:47.55pt'>
  <p class=MsoNormal style='margin-left:19.75pt;text-indent:-19.75pt'><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>SOP
  7 – Management and Archiving of Files</span></p>
  <p class=MsoNormal style='margin-left:19.75pt;text-indent:-19.75pt'><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>7.1
  Managing REC Incoming/Outgoing Communications</span></p>
  <p class=MsoNormal style='margin-left:19.75pt;text-indent:-19.75pt'><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>7.2
  Managing Active Files (Administrative and Study Files)</span></p>
  <p class=MsoNormal style='margin-left:19.75pt;text-indent:-19.75pt'><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>7.3
  Archiving of Terminated, Inactive, and Completed Files </span></p>
  <p class=MsoNormal style='margin-left:19.75pt;text-indent:-19.75pt'><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>7.4
  Managing Access to Confidential Files</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:47.55pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:47.55pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:47.55pt'>
  <td width=228 valign=top style='width:171.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:47.55pt'>
  <p class=MsoNormal style='margin-left:19.75pt;text-indent:-19.75pt'><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>SOP
  8 – Site Visits</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:47.55pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:47.55pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:47.55pt'>
  <td width=228 valign=top style='width:171.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:47.55pt'>
  <p class=MsoNormal style='margin-left:19.75pt;text-indent:-19.75pt'><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>SOP
  9 – Management of Queries/Complaints</span></p>
  <p class=TableContents><span lang=EN-US style='font-size:11.0pt;font-family:
  "Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:47.55pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:47.55pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:47.55pt'>
  <td width=228 valign=top style='width:171.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:47.55pt'>
  <p class=TableContents><span lang=EN-US style='font-size:11.0pt;font-family:
  "Palatino Linotype",serif'>SOP 10 – Writing and Revising SOPs<b><br
  clear=all style='page-break-before:always'>
  </b></span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:47.55pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:47.55pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr>
  <td width=180 rowspan=3 valign=top style='width:135.0pt;border:solid black 1.0pt;
  border-top:none;padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents style='margin-left:15.25pt;text-indent:-13.5pt'><b><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>4.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></b><b><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>COMPLETENESS
  OF THE REVIEW PROCESS</span></b></p>
  </td>
  <td width=228 style='width:171.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents><span lang=EN-US style='font-size:11.0pt;font-family:
  "Palatino Linotype",serif'>Adequate assessment forms </span></p>
  </td>
  <td width=192 valign=top style='width:144.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents><b><span lang=EN-US style='font-size:11.0pt;
  font-family:"Palatino Linotype",serif'>Protocol Files </span></b></p>
  <p class=TableContents><span lang=EN-US style='font-size:11.0pt;font-family:
  "Palatino Linotype",serif'>     Assessment Forms</span></p>
  <p class=TableContents><span lang=EN-US style='font-size:11.0pt;font-family:
  "Palatino Linotype",serif'>     Assignment of Reviewers</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr>
  <td width=228 style='width:171.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents><span lang=EN-US style='font-size:11.0pt;font-family:
  "Palatino Linotype",serif'>Consistent and meaningful use of Assessment forms</span></p>
  </td>
  <td width=192 valign=top style='width:144.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents><b><span lang=EN-US style='font-size:11.0pt;
  font-family:"Palatino Linotype",serif'>Minutes of Meetings</span></b></p>
  <p class=TableContents><span lang=EN-US style='font-size:11.0pt;font-family:
  "Palatino Linotype",serif'>Letters to the Investigator <b>(Protocol file)</b></span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr>
  <td width=228 style='width:171.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents><span lang=EN-US style='font-size:11.0pt;font-family:
  "Palatino Linotype",serif'>Technical and Ethical issues are addressed</span></p>
  </td>
  <td width=192 valign=top style='width:144.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents><b><span lang=EN-US style='font-size:11.0pt;
  font-family:"Palatino Linotype",serif'>Minutes of Meetings</span></b></p>
  <p class=TableContents><span lang=EN-US style='font-size:11.0pt;font-family:
  "Palatino Linotype",serif'>Letters to the Investigator <b>(Protocol file)</b></span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr>
  <td width=180 rowspan=3 valign=top style='width:135.0pt;border:solid black 1.0pt;
  border-top:none;padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents style='margin-left:15.25pt;text-indent:-13.5pt'><b><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>5.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></b><b><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>ADEQUACY
  OF THE AFTER-REVIEW PROCEDURES</span></b></p>
  </td>
  <td width=228 valign=top style='width:171.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents><span lang=EN-US style='font-size:11.0pt;font-family:
  "Palatino Linotype",serif'>REC Requirement for submission of reports</span></p>
  </td>
  <td width=192 valign=top style='width:144.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents><span lang=EN-US style='font-size:11.0pt;font-family:
  "Palatino Linotype",serif'>Approval letter <b>(Protocol File)</b></span></p>
  <p class=TableContents><b><span lang=EN-US style='font-size:11.0pt;
  font-family:"Palatino Linotype",serif'>&nbsp;</span></b></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr>
  <td width=228 valign=top style='width:171.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents><span lang=EN-US style='font-size:11.0pt;font-family:
  "Palatino Linotype",serif'>Inclusion of reports in the agenda</span></p>
  </td>
  <td width=192 style='width:144.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents><b><span lang=EN-US style='font-size:11.0pt;
  font-family:"Palatino Linotype",serif'>Minutes of Meetings</span></b></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr>
  <td width=228 valign=top style='width:171.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents><span lang=EN-US style='font-size:11.0pt;font-family:
  "Palatino Linotype",serif'>Assessments of the Reports</span></p>
  </td>
  <td width=192 valign=top style='width:144.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents><b><span lang=EN-US style='font-size:11.0pt;
  font-family:"Palatino Linotype",serif'>Minutes of Meetings and Protocol File</span></b></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr>
  <td width=180 rowspan=2 valign=top style='width:135.0pt;border:solid black 1.0pt;
  border-top:none;padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents style='margin-left:15.25pt;text-indent:-13.5pt'><b><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>6.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></b><b><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>ADEQUACY
  OF ADMINISTRATIVE SUPPORT FOR ERC ACTIVITIES</span></b></p>
  </td>
  <td width=228 valign=top style='width:171.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents><span lang=EN-US style='font-size:11.0pt;font-family:
  "Palatino Linotype",serif'>Availability of a regular support staff </span></p>
  </td>
  <td width=192 style='width:144.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents><b><span lang=EN-US style='font-size:11.0pt;
  font-family:"Palatino Linotype",serif'>Application for Accreditation</span></b></p>
  <p class=TableContents><b><span lang=EN-US style='font-size:11.0pt;
  font-family:"Palatino Linotype",serif'>Appointment Letter of Staff</span></b></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr>
  <td width=228 valign=top style='width:171.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents><span lang=EN-US style='font-size:11.0pt;font-family:
  "Palatino Linotype",serif'>Provision of an Office</span></p>
  </td>
  <td width=192 style='width:144.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents><b><span lang=EN-US style='font-size:11.0pt;
  font-family:"Palatino Linotype",serif'>Self-Assessment Checklist</span></b></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:30.3pt'>
  <td width=180 rowspan=2 valign=top style='width:135.0pt;border:solid black 1.0pt;
  border-top:none;padding:2.75pt 2.75pt 2.75pt 2.75pt;height:30.3pt'>
  <p class=TableContents style='margin-left:15.25pt;text-indent:-13.5pt'><b><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>7.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></b><b><span
  lang=EN-US style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>EFFICIENT
  AND SYSTEMATIC RECORDING AND ARCHIVING</span></b></p>
  </td>
  <td width=228 valign=top style='width:171.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:30.3pt'>
  <p class=TableContents><span lang=EN-US style='font-size:11.0pt;font-family:
  "Palatino Linotype",serif'>Availability of log books</span></p>
  <p class=TableContents><span lang=EN-US style='font-size:11.0pt;font-family:
  "Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=192 style='width:144.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:2.75pt 2.75pt 2.75pt 2.75pt;
  height:30.3pt'>
  <p class=TableContents><b><span lang=EN-US style='font-size:11.0pt;
  font-family:"Palatino Linotype",serif'>Self-Assessment Checklist</span></b></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:30.3pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:30.3pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:30.3pt'>
  <td width=228 valign=top style='width:171.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:30.3pt'>
  <p class=TableContents><span lang=EN-US style='font-size:11.0pt;font-family:
  "Palatino Linotype",serif'>Adequacy of Agenda and Minutes of the Meeting</span></p>
  </td>
  <td width=192 style='width:144.0pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:2.75pt 2.75pt 2.75pt 2.75pt;
  height:30.3pt'>
  <p class=TableContents><b><span lang=EN-US style='font-size:11.0pt;
  font-family:"Palatino Linotype",serif'>Agenda and Minutes of the Meeting</span></b></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:30.3pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:2.75pt 2.75pt 2.75pt 2.75pt;height:30.3pt'>
  <p class=TableContents style='layout-grid-mode:char'><span lang=EN-US
  style='font-size:11.0pt;font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
</table>

<p class=MsoNormal><span lang=EN-US style='font-family:"Palatino Linotype",serif'>&nbsp;</span></p>

<p class=MsoNormal><span lang=EN-US style='font-family:"Palatino Linotype",serif'>&nbsp;</span></p>

<p class=MsoListParagraph style='margin-top:0cm;margin-right:0cm;margin-bottom:
0cm;margin-left:45.0pt;margin-bottom:.0001pt;text-indent:-45.0pt;line-height:
normal'><b><span lang=EN-US style='font-family:"Palatino Linotype",serif;
text-transform:uppercase'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>I.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=EN-US style='font-family:"Palatino Linotype",serif'>OVERALL
ASSESSMENT: </span></b></p>

<p class=MsoNormal style='margin-left:45.0pt'><b><span lang=EN-US
style='font-family:"Palatino Linotype",serif;text-transform:uppercase'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-left:45.0pt'><b><span lang=EN-US
style='font-family:"Palatino Linotype",serif;text-transform:uppercase'>Summary
of  </span></b><b><span lang=EN-US style='font-family:"Palatino Linotype",serif'>RECOMMENDATIONS<span
style='text-transform:uppercase'> according to PHREB Standards:</span></span></b></p>

<p class=MsoNormal style='margin-left:45.0pt'><b><span lang=EN-US
style='font-family:"Palatino Linotype",serif;text-transform:uppercase'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpFirst style='margin-top:0cm;margin-right:0cm;
margin-bottom:0cm;margin-left:63.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;
line-height:normal'><b><span style='font-family:"Palatino Linotype",serif;
text-transform:uppercase'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span style='font-family:"Palatino Linotype",serif;
text-transform:uppercase'>Functionality of the structure and membership of the
ERC</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0cm;margin-right:0cm;
margin-bottom:0cm;margin-left:63.0pt;margin-bottom:.0001pt;line-height:normal'><b><span
style='font-family:"Palatino Linotype",serif;text-transform:uppercase'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0cm;margin-right:0cm;
margin-bottom:0cm;margin-left:63.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;
line-height:normal'><b><span style='font-family:"Palatino Linotype",serif;
text-transform:uppercase'>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span style='font-family:"Palatino Linotype",serif;
text-transform:uppercase'>Adequacy of the Standard Operating Procedures and
consistency in its implementation</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0cm;margin-right:0cm;
margin-bottom:0cm;margin-left:63.0pt;margin-bottom:.0001pt;line-height:normal'><b><span
style='font-family:"Palatino Linotype",serif;text-transform:uppercase'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0cm;margin-right:0cm;
margin-bottom:0cm;margin-left:63.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;
line-height:normal'><b><span style='font-family:"Palatino Linotype",serif;
text-transform:uppercase'>3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span style='font-family:"Palatino Linotype",serif;
text-transform:uppercase'>Adherence to international, national and
institutional guidelines and policies</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0cm;margin-right:0cm;
margin-bottom:0cm;margin-left:63.0pt;margin-bottom:.0001pt;line-height:normal'><b><span
style='font-family:"Palatino Linotype",serif;text-transform:uppercase'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0cm;margin-right:0cm;
margin-bottom:0cm;margin-left:63.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;
line-height:normal'><b><span style='font-family:"Palatino Linotype",serif;
text-transform:uppercase'>4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span style='font-family:"Palatino Linotype",serif;
text-transform:uppercase'>Completeness of the review process</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0cm;margin-right:0cm;
margin-bottom:0cm;margin-left:63.0pt;margin-bottom:.0001pt;line-height:normal'><b><span
style='font-family:"Palatino Linotype",serif;text-transform:uppercase'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0cm;margin-right:0cm;
margin-bottom:0cm;margin-left:63.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;
line-height:normal'><b><span style='font-family:"Palatino Linotype",serif;
text-transform:uppercase'>5.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span style='font-family:"Palatino Linotype",serif;
text-transform:uppercase'>Adequacy of the after-review procedures</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0cm;margin-right:0cm;
margin-bottom:0cm;margin-left:63.0pt;margin-bottom:.0001pt;line-height:normal'><b><span
style='font-family:"Palatino Linotype",serif;text-transform:uppercase'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0cm;margin-right:0cm;
margin-bottom:0cm;margin-left:63.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;
line-height:normal'><b><span style='font-family:"Palatino Linotype",serif;
text-transform:uppercase'>6.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span style='font-family:"Palatino Linotype",serif;
text-transform:uppercase'>Adequacy of administrative support for ERC activities</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0cm;margin-right:0cm;
margin-bottom:0cm;margin-left:63.0pt;margin-bottom:.0001pt;line-height:normal'><b><span
style='font-family:"Palatino Linotype",serif;text-transform:uppercase'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0cm;margin-right:0cm;
margin-bottom:0cm;margin-left:63.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;
line-height:normal'><b><span style='font-family:"Palatino Linotype",serif;
text-transform:uppercase'>7.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span style='font-family:"Palatino Linotype",serif;
text-transform:uppercase'>Efficient and systematic recording and archiving</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0cm;margin-right:0cm;
margin-bottom:0cm;margin-left:45.0pt;margin-bottom:.0001pt;line-height:normal'><b><span
lang=EN-US style='font-family:"Palatino Linotype",serif'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0cm;margin-right:0cm;
margin-bottom:0cm;margin-left:45.0pt;margin-bottom:.0001pt;text-indent:-45.0pt;
line-height:normal'><b><span lang=EN-US style='font-family:"Palatino Linotype",serif;
text-transform:uppercase'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>II.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=EN-US style='font-family:"Palatino Linotype",serif'>RECOMMENDATION</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0cm;margin-right:0cm;
margin-bottom:0cm;margin-left:27.0pt;margin-bottom:.0001pt;line-height:normal'><b><span
lang=EN-US style='font-family:"Palatino Linotype",serif;text-transform:uppercase'>&nbsp;</span></b></p>

<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=756
 style='width:20.0cm;margin-left:23.4pt;border-collapse:collapse;border:none'>
 <tr style='height:22.45pt'>
  <td width=258 rowspan=2 valign=top style='width:193.5pt;border:solid windowtext 1.0pt;
  background:#E5E5E5;padding:0cm 5.4pt 0cm 5.4pt;height:22.45pt'>
  <p class=MsoListParagraphCxSpMiddle style='margin-left:0cm;text-align:justify'><b><span
  lang=EN-US style='font-family:"Palatino Linotype",serif'>Accreditation for
  Level 2 REC:</span></b></p>
  </td>
  <td width=156 valign=top style='width:117.0pt;border:solid windowtext 1.0pt;
  border-left:none;padding:0cm 5.4pt 0cm 5.4pt;height:22.45pt'>
  <p class=MsoListParagraphCxSpLast style='margin-top:0cm;margin-right:0cm;
  margin-bottom:0cm;margin-left:18.0pt;margin-bottom:.0001pt;text-align:justify;
  text-indent:-18.0pt;line-height:normal'><span lang=EN-US style='font-family:
  Symbol'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span><span lang=EN-US style='font-family:"Palatino Linotype",serif'>Approved</span></p>
  <p class=MsoNormal style='text-align:justify'><span lang=EN-US
  style='font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=342 valign=top style='width:256.5pt;border:solid windowtext 1.0pt;
  border-left:none;padding:0cm 5.4pt 0cm 5.4pt;height:22.45pt'>
  <p class=MsoListParagraph style='margin-left:0cm;text-align:justify'><span
  lang=EN-US style='font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr>
  <td width=156 valign=top style='width:117.0pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoListParagraphCxSpMiddle style='margin-top:0cm;margin-right:0cm;
  margin-bottom:0cm;margin-left:18.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;
  line-height:normal'><span lang=EN-US style='font-family:Symbol'><span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span><span lang=EN-US style='font-family:"Palatino Linotype",serif'>Approved
  Action Plan required before approval</span></p>
  </td>
  <td width=342 valign=top style='width:256.5pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoListParagraphCxSpLast style='margin-left:0cm;text-align:justify'><span
  lang=EN-US style='font-family:"Palatino Linotype",serif'>Remarks:</span></p>
  </td>
 </tr>
</table>

<p class=MsoListParagraph style='margin-top:0cm;margin-right:0cm;margin-bottom:
0cm;margin-left:18.0pt;margin-bottom:.0001pt;line-height:normal'><b><span
lang=EN-US style='font-family:"Palatino Linotype",serif'>&nbsp;</span></b></p>

<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=756
 style='width:20.0cm;margin-left:23.4pt;border-collapse:collapse;border:none'>
 <tr style='height:40.9pt'>
  <td width=126 valign=top style='width:94.5pt;border:solid windowtext 1.0pt;
  border-bottom:solid black 1.0pt;background:#E5E5E5;padding:0cm 5.4pt 0cm 5.4pt;
  height:40.9pt'>
  <p class=MsoNormal style='text-align:justify'><b><span lang=EN-US
  style='font-family:"Palatino Linotype",serif'>Accomplished by:</span></b></p>
  </td>
  <td width=246 valign=top style='width:184.5pt;border:solid windowtext 1.0pt;
  border-left:none;padding:0cm 5.4pt 0cm 5.4pt;height:40.9pt'>
  <p class=MsoNormal style='text-align:justify'><span lang=EN-US
  style='font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  <p class=MsoNormal style='text-align:justify'><span lang=EN-US
  style='font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  <p class=MsoNormal align=center style='text-align:center'><b><u><span
  lang=EN-US style='font-family:"Palatino Linotype",serif'>____________________________</span></u></b></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-family:"Palatino Linotype",serif'>Signature over Printed Name</span></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-family:"Palatino Linotype",serif'>Accreditor</span></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=84 valign=top style='width:63.0pt;border:solid windowtext 1.0pt;
  border-left:none;background:#E5E5E5;padding:0cm 5.4pt 0cm 5.4pt;height:40.9pt'>
  <p class=MsoNormal><b><span lang=EN-US style='font-family:"Palatino Linotype",serif'>Position:</span></b></p>
  </td>
  <td width=114 valign=top style='width:85.5pt;border:solid windowtext 1.0pt;
  border-left:none;padding:0cm 5.4pt 0cm 5.4pt;height:40.9pt'>
  <p class=MsoNormal style='text-align:justify'><span lang=EN-US
  style='font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  <p class=MsoNormal style='text-align:justify'><span lang=EN-US
  style='font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=60 valign=top style='width:45.0pt;border-top:solid windowtext 1.0pt;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid windowtext 1.0pt;
  background:#E5E5E5;padding:0cm 5.4pt 0cm 5.4pt;height:40.9pt'>
  <p class=MsoNormal><b><span lang=EN-US style='font-family:"Palatino Linotype",serif'>Date:</span></b></p>
  </td>
  <td width=126 valign=top style='width:94.5pt;border:solid windowtext 1.0pt;
  border-left:none;padding:0cm 5.4pt 0cm 5.4pt;height:40.9pt'>
  <p class=MsoNormal style='text-align:justify'><span lang=EN-US
  style='font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  <p class=MsoNormal style='text-align:justify'><span lang=EN-US
  style='font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  <p class=MsoNormal align=center style='text-align:center'><u><span
  lang=EN-US style='font-family:"Palatino Linotype",serif'>___________</span></u></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-family:"Palatino Linotype",serif'>MM/DD/YYYY</span></p>
  </td>
 </tr>
 <tr>
  <td width=126 valign=top style='width:94.5pt;border:solid windowtext 1.0pt;
  border-top:none;background:#E5E5E5;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='text-align:justify'><b><span lang=EN-US
  style='font-family:"Palatino Linotype",serif'>Noted by:</span></b></p>
  </td>
  <td width=246 valign=top style='width:184.5pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='text-align:justify'><span lang=EN-US
  style='font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  <p class=MsoNormal style='text-align:justify'><span lang=EN-US
  style='font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  <p class=MsoNormal align=center style='text-align:center'><b><u><span
  lang=EN-US style='font-family:"Palatino Linotype",serif'>____________________________</span></u></b><span
  lang=EN-US style='font-family:"Palatino Linotype",serif'>Signature over
  Printed Name</span></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-family:"Palatino Linotype",serif'>CSA Chair</span></p>
  </td>
  <td width=84 valign=top style='width:63.0pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#E5E5E5;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='text-align:justify'><b><span lang=EN-US
  style='font-family:"Palatino Linotype",serif'>Position:</span></b></p>
  </td>
  <td width=114 valign=top style='width:85.5pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='text-align:justify'><span lang=EN-US
  style='font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  <p class=MsoNormal style='text-align:justify'><span lang=EN-US
  style='font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  </td>
  <td width=60 valign=top style='width:45.0pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#E5E5E5;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='text-align:justify'><b><span lang=EN-US
  style='font-family:"Palatino Linotype",serif'>Date:</span></b></p>
  </td>
  <td width=126 valign=top style='width:94.5pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='text-align:justify'><span lang=EN-US
  style='font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  <p class=MsoNormal style='text-align:justify'><span lang=EN-US
  style='font-family:"Palatino Linotype",serif'>&nbsp;</span></p>
  <p class=MsoNormal align=center style='text-align:center'><u><span
  lang=EN-US style='font-family:"Palatino Linotype",serif'>___________</span></u></p>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-family:"Palatino Linotype",serif'>MM/DD/YYYY</span></p>
  </td>
 </tr>
</table>

<p class=MsoNormal><b><span lang=EN-US style='font-family:"Palatino Linotype",serif;
text-transform:uppercase'>&nbsp;</span></b></p>

<p class=MsoNormal><span lang=EN-US style='font-family:"Palatino Linotype",serif'>&nbsp;</span></p>

<p class=MsoNormal><span lang=EN-US style='font-family:"Palatino Linotype",serif'>&nbsp;</span></p>

</div>

</body>

</html>
