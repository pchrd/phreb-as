@extends('layouts.myApp')
@section('content')

<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-10">

			<div class="card-header label-default">
				<span class="text text-dark"><strong>Assessment Logs - {{$assname}} <small>{{$filename}}</small></strong>
					<div class="pull-right">
						<a href="{{URL::previous()}}" class="btn btn-default fa fa-arrow-circle-o-left" title="Back"></a>
					</div>
				</span><br>
			</div>

			<nav>
				<div class="nav nav-tabs" id="nav-tab" role="tablist">
					<a class="nav-item nav-link active text text-info" id="asscsachair-tab" data-toggle="tab" href="#asscsachair" role="tab" aria-controls="nav-home" aria-selected="true">CSA Chair</a>
					<a class="nav-item nav-link text text-info" id="assrec-tab" data-toggle="tab" href="#assrec" role="tab" aria-controls="nav-profile" aria-selected="false">REC</a>

				</div>
			</nav>
			<div class="tab-content" id="nav-tabContent">
				
				<div class="tab-pane fade show active" id="asscsachair" role="tabpanel" aria-labelledby="asscsachair-tab">
					<div class="row">
						<div class="col-md-12 col-md-offset-1">
							<div class="panel panel-info">
								<div class="panel-body">
									<table class="table table-hover cell-border" id="recdetails_table">
										<thead>
											<tr class="label label-info">
												<th scope="col">Forwarded by</th>
												<th scope="col">Forwarded to </th>
												<th scope="col">Date Forwarded</th>
												<th scope="col">Deadline</th>
												<th scope="col">Emailed Reminder Date</th>
											</tr>
										</thead>
										@foreach($asscsachair as $ac)
										<tbody>
											<tr>
												<td>{{$ac->users_sender->name}}</td>
												<td>{{$ac->users_csachair->name}}</td>
												<td>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $ac->ra_csa_datecreated_at)->format('M j, Y')}} <small>({{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $ac->ra_csa_datecreated_at)->diffForHumans()}})</small></td>
												<td>{{$ac->ra_csa_dateexpiry}}</td>
												<td>{{$ac->ra_csa_reminder_status}}</td>
											</tr>
										</tbody>
										@endforeach
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="tab-pane fade" id="assrec" role="tabpanel" aria-labelledby="assrec-tab">
						
				<div class="tab-pane fade show active" id="asscsachair" role="tabpanel" aria-labelledby="asscsachair-tab">
					<div class="row">
						<div class="col-md-12 col-md-offset-1">
							<div class="panel panel-info">
								<div class="panel-body">
									<table class="table table-hover table-border" id="recdetails_table">
										
										<col>
										<colgroup span="2"></colgroup>
										<colgroup span="2"></colgroup>

										<tr class="label label-info text-center">
											<td rowspan="2">Forward Information</td>
											<th colspan="2" scope="colgroup">Action Plan</th>
											<th colspan="2" scope="colgroup">Compliance Evidences</th>
										</tr>
										<tr class="label label-danger">
											<th scope="col">Deadline</th>
											<th scope="col">Reminder Emailed Date</th>
											<th scope="col">Deadline</th>
											<th scope="col">Reminder Emailed Date</th>
										</tr>
										
										@foreach($assrec as $ar)
										<tbody>
										<tr>
											<th scope="row"><b><a href="" class="fa fa-expand" onclick="return confirm('Action Plan Deadline Emailed Date: {{$ar->ra_rec_ap_reminder_status_deadline}}  |  Compliance Evidences Deadline Emailed Date: {{$ar->ra_rec_ce_reminder_status_deadline}}')"></a> {{$ar->recname->rec_name}}</b><small>,<br>
												{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $ar->ra_rec_datecreated_at)->toDayDateTimeString()}} ({{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $ar->ra_rec_datecreated_at)->diffForHumans()}}) 	
												<br>
												Forwarded by: {{$ar->users_sender->name}}
												</small>
											</th>
											<td>{{\Carbon\Carbon::createFromFormat('Y-m-d', $ar->ra_rec_ap_dateexpiry)->format('M j, Y')}}</td>
											<td>{{$ar->ra_rec_ap_reminder_status}}</td>

											<td>{{\Carbon\Carbon::createFromFormat('Y-m-d', $ar->ra_rec_ce_dateexpiry)->format('M j, Y')}}</td>
											<td>{{$ar->ra_rec_ce_reminder_status}}</td>
										</tr>
										</tbody>
										@endforeach
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				</div>

			</div>

		</div>
	</div>
</div>
@endsection