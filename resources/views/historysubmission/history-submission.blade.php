@extends('layouts.myApp')
@section('content')

<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-12">

			<div class="card">
				<div class="card-header navbar-light" style="background-color: #E2E8AD;"><a href="{{url('/history-submission')}}">SUBMISSION HISTORY</a>
					<caption class="row">
						@foreach(Auth::user()->roles as $role)
						| <small class="text-muted lead"> {{$role->role_name}} </small>  
						@endforeach
					</caption>
				</div>
				<div class="card-body">

					<div class="container col-md-12">
						<table id="" class="col-md-8 table table-hover" data-toggle="dataTable" data-form="deleteForm">
							<thead class="thead-default"> 
								<tr>
									<div class="container-fluid"><caption class="form-group"></caption></div>
									<th>ID</th>
									<th>FROM USER</th>
									<th>REC APPLICATION</th>
									<th>STATUS</th>
									<th>DATE CREATED</th>
								</tr>
							</thead> 
							<div class="form-group row">
							@foreach($sub_history as $sub)
								<tbody>
									<tr>
										<td>{{$sub->id}}</td>
										<td>{{$sub->name}}</td>
										<td>{{$sub->rec_name}}</td>
										<td>{{$sub->status_name}}</td>
										<td>{{$sub->submissionstatuses_datecreated}}</td>
									</tr>  
								</tbody>
							</div>
							@endforeach
						</table>
					</div>
					<center>
						<div class="form-group">
							<div class="col-md-2"> 
								
								<caption></caption>
							</div>
						</div>
					</center>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection