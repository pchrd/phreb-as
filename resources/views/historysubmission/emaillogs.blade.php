@extends('layouts.myApp')
@section('content')

<div class="col-md-12" id="verifyuser">
	@if(session('success'))
	<div class="alert alert-success">
		{{session('success')}}
		<button class="close" onclick="document.getElementById('verifyuser').style.display='none'">x</button>
	</div>
	@endif
</div>

<div class="container">
		<div class="row">
			<div class="col-md-12 col-md-offset-1">
				<div class="panel panel-info">
					<div class="panel-heading"><h3><strong>EMAIL LOGS</strong></h3></div>
					<div class="panel-body">
						<table class="table table-responsive table-condense table-hover cell-border nowrap" id="emaillogs">
							<thead class="badge-info">
								<tr>
									<th scope="col"></th>
									<th scope="col">ID</th>
									<th scope="col">TIME</th>
									<th scope="col">DATE</th>
									<th scope="col">REC</th>
									<th scope="col">SENDER</th>
									<th scope="col">EMAIL FROM</th>
									<th scope="col">EMAIL TO</th>
									<th scope="col">CC</th>
									<th scope="col">BCC</th>
									<th scope="col">SUBJECT</th>
									<th scope="col">BODY</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

<script type="text/javascript">
	$(function(){
		$('#emaillogs').DataTable({

			dom: 'Blfrtip',
        	buttons: 
        		['colvis','copy', 'csv', 'excel', 'pdf', 'print'],
			processing: true,
			serverSide: true,
			ajax: '{{ route('email/emaillogs') }}',
			columns: [
			{ data: 'action',   name: 'action', visible:true },
			{ data: 'id',   name: 'emaillogs.id', visible:true },
			{ data: 'e_time', name: 'emaillogs.e_created_at'},
			{ data: 'e_created_at', name: 'emaillogs.e_created_at'},
			{data: 'recdetails.rec_institution', name: 'recdetails.rec_institution'},
			{ data: 'users.name', name: 'users.name'},
			{ data: 'e_from', name: 'emaillogs.e_from'},
			{ data: 'e_to', name: 'emaillogs.e_to'},
			{ data: 'e_cc', name: 'emaillogs.e_cc'},
			{ data: 'e_bcc', name: 'emaillogs.e_bcc'},
			{ data: 'e_subject', name: 'emaillogs.e_subject'},
			{ data: 'e_body', name: 'emaillogs.e_body'}
			],
        	
		});

	});
</script>
@endsection