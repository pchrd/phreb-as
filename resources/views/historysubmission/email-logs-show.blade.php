@extends('layouts.myApp')
@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-8">

			<div class="card">
				<div class="card-header navbar-light">
					<caption class="row pull-right">
						<a href="{{url()->previous()}}"><span class="fa fa-arrow-left"></span> Back </a>
					</caption>
				</div>
				<div class="card-body">
					<div class="row form-group">
						<table class="table">
							<tbody>
								<tr>
									<th>SUBJECT:</th>
									<td>{{$emails->e_subject}}</td>
								</tr>
								<tr>
									<th>FROM:</th>
									<td>{{$emails->e_from}}</td>		
								</tr>
								<tr>
									<th>TO:</th>
									<td>{{$emails->e_to}}</td>
								</tr>
								<tr>
									<th>CC:</th>
									<td>{{$emails->e_cc}}</td>	
								</tr>
								<tr>
									<th>BCC:</th>
									<td>{{$emails->e_bcc}}</td>
								</tr>
								<tr>
									<th>MESSAGE:</th>
									<td>{!!$emails->e_body!!}</td>
								</tr>
								<tr>
									<th>DATE:</th>
									<td>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $emails->e_created_at)->format('F j, Y')}}</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection