@extends('layouts.myApp')
@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-10">

			<div class="card-header label-default">
				<span class="text text-dark"><strong>Evaluation on Action Plan & Compliance Evidences Logs - {{$sender}} 
					<div class="pull-right">
						<a href="{{URL::previous()}}" class="btn btn-default fa fa-arrow-circle-o-left" title="Back"></a>
					</div>
					<br>
					<small>{{$filename}}</small></strong>
				</span><br>
			</div>

			<nav>
				<div class="nav nav-tabs" id="nav-tab" role="tablist">
					<a class="nav-item nav-link active text text-info" id="asscsachair-tab" data-toggle="tab" href="#asscsachair" role="tab" aria-controls="nav-home" aria-selected="true">CSA Chair</a>
					<a class="nav-item nav-link text text-info" id="assrec-tab" data-toggle="tab" href="#assrec" role="tab" aria-controls="nav-profile" aria-selected="false">REC</a>
				</div>
			</nav>

			<div class="tab-content" id="nav-tabContent">
				<div class="tab-pane fade show active" id="asscsachair" role="tabpanel" aria-labelledby="asscsachair-tab">
					<div class="row">
						<div class="col-md-12 col-md-offset-1">
							<div class="panel panel-info">
								<div class="panel-body">
									<table class="table table-hover cell-border" id="recdetails_table">
										<thead>
											<tr class="label label-info">
												<th scope="col">Forwarded by</th>
												<th scope="col">Forwarded to </th>
												<th scope="col">Date Forwarded</th>
											</tr>
										</thead>
										
										@foreach($apce_csachair as $apce_csahairs)
										<tbody>
											<tr>
												<td>{{$apce_csahairs->users->name}}</td>
												<td>{{$apce_csahairs->users_csachair->name}}</td>
												<td>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $apce_csahairs->ap_csa_datecreated_at)->toDayDateTimeString()}}</td>
											</tr>
										</tbody>
										@endforeach
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="tab-pane fade" id="assrec" role="tabpanel" aria-labelledby="assrec-tab">	
				<div class="tab-pane fade show active" id="asscsachair" role="tabpanel" aria-labelledby="asscsachair-tab">
					<div class="row">
						<div class="col-md-12 col-md-offset-1">
							<div class="panel panel-info">
								<div class="panel-body">
								<table class="table table-hover cell-border" id="recdetails_table">
										<thead>
											<tr class="label label-info">
												<th scope="col">Forwarded by</th>
												<th scope="col">Forwarded to </th>
												<th scope="col">Date Forwarded</th>
											</tr>
										</thead>
										@foreach($apce_rec as $apce_recs)
										<tbody>
											<tr>
												<td>{{$apce_recs->users->name}}</td>
												<td>{{$apce_recs->users_rec->name}}</td>
												<td>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $apce_recs->ap_csa_datecreated_at)->toDayDateTimeString()}}</td>
											</tr>
										</tbody>
										@endforeach
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection