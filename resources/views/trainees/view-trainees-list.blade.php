@extends('layouts.myApp')

@section('content')
<div class="container">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12">
				<div class="col-md-12" id="verifyuser">
					@if(session('success'))
					<div class="alert alert-success">
						{{session('success')}}
						<button class="close" onclick="document.getElementById('verifyuser').style.display='none'">x</button>
					</div>
					@endif
				</div>
				
				<div class="card">
					<div class="card-header navbar-light"><strong>Personal Information</strong>
						<caption class="row pull-right">
							<a href="{{url('/show-trainees-list')}}" class="pull-right"> Back </a>
						</caption>
					</div>
					<div class="card-body">
						
						<form method="POST" action="{{route('update-trainees-list', ['id' => $trainee->id])}}">
							@csrf
							<div class="form-group row">
								<label for="trainees_lname" class="col-md-4 col-form-label text-md-right">{{ __('Last Name') }}</label>

								<div class="col-md-6">
									<input id="trainees_lname" type="text" class="form-control{{ $errors->has('trainees_lname') ? ' is-invalid' : '' }}" name="trainees_lname" value="{{$trainee->trainees_lname}}" required autofocus>

									@if ($errors->has('trainees_lname'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('trainees_lname') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="form-group row">
								<label for="trainees_fname" class="col-md-4 col-form-label text-md-right">{{ __('First Name') }}</label>

								<div class="col-md-6">
									<input id="trainees_fname" type="text" class="form-control{{ $errors->has('trainees_fname') ? ' is-invalid' : '' }}" name="trainees_fname" value="{{$trainee->trainees_fname}}" required autofocus>

									@if ($errors->has('trainees_fname'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('trainees_fname') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="form-group row">
								<label for="trainees_mname" class="col-md-4 col-form-label text-md-right">{{ __('Middle Name') }}</label>

								<div class="col-md-6">
									<input id="trainees_mname" type="text" class="form-control{{ $errors->has('trainees_mname') ? ' is-invalid' : '' }}" name="trainees_mname" value="{{$trainee->trainees_mname}}" autofocus>

									@if ($errors->has('trainees_mname'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('trainees_mname') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="form-group row">
								<label for="trainees_gender" class="col-md-4 col-form-label text-md-right">{{ __('Gender') }}</label>

								<div class="col-md-6">
									<select id="trainees_gender" type="text" class="form-control{{ $errors->has('trainees_gender') ? ' is-invalid' : '' }}" name="trainees_gender" required autofocus>
										<option value="{{$trainee->trainees_gender}}" selected>{{$trainee->trainees_gender}}</option>
										<option value="Male">Male</option>
										<option value="Female">Female</option>
									</select>
									@if ($errors->has('trainees_gender'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('trainees_gender') }}</strong>
									</span>
									@endif
								</div>
							</div>

							
							<div class="form-group row">
								<label for="trainees_certificatename" class="col-md-4 col-form-label text-md-right">{{ __('Name for Certificate') }}</label>

								<div class="col-md-6">
									<input id="trainees_certificatename" type="text" class="form-control{{ $errors->has('trainees_certificatename') ? ' is-invalid' : '' }}" name="trainees_certificatename" value="{{$trainee->trainees_certificatename}}" required autofocus>

									@if ($errors->has('trainees_certificatename'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('trainees_certificatename') }}</strong>
									</span>
									@endif
								</div>
							</div>




							<div class="modal-footer">
								<button type="submit" class="btn btn-primary">{{ __('Save Changes') }}</button>
							</div>
						</form>
						
					</div>

					<div class="card">
						<div class="card-header navbar-light"><strong>Affiliation</strong>
							<caption class="row pull-right">
								<a href="#" data-toggle="modal" data-target="#add-affiliation" class="pull-right"> Add </a>
							</caption>

						</div>
						<div class="card-body">

							<table class="table table-condensed table-hover">
								<thead>
									<tr>
										<td><span class="fa fa-address-card-o"></span></td>
										<td>Instituion</td>
										<td>Specialization</td>
										<td>Email</td>
										<td>Contact No.</td>
										<td>Professional Background</td>
										<td>REC Role</td>
										<td>Commitee</td>
									</tr>
								</thead>
								<tbody>
									@foreach($trainee->affiliations as $aff)
									<tr>
										<td><a href="#" data-toggle="modal" data-target="#viewAffiliation{{$aff->id}}"><span class="fa fa-eye" title="View"></span></a></td>
										<td><b>{{$aff->trainees_institution}}</b><br>
											<small>{{$aff->trainees_institution_address}}</small>
										</td>
										<td>{{$aff->trainees_specialization}}</td>
										<td>{{$aff->trainees_email}}</td>
										<td>{{$aff->trainees_contactno}}</td>
										<td>{{$aff->trainees_profbackground}}</td>
										<td>{{$aff->acc_roles->roleinevent}}</td>
										<td>{{$aff->trainees_committee}}</td>
									</tr>
									@endforeach
								</tbody>
							</table>

				@foreach($trainee->affiliations as $affModal)
						<div class="modal fade" id="viewAffiliation{{$affModal->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header bg-green">
											<h4 class="modal-title" id="myModalLabel">Update Affiliation</h4>
										</div>
										<div class="modal-body">
											<form method="POST" action="{{route('update-affiliation', ['id' => $affModal->id])}}">
												{{ csrf_field() }}

												<div class="form-group row">
													<label for="trainees_institution" class="col-md-4 col-form-label text-md-left">{{ __('Institution/Department:') }}</label>

													<div class="col-md-8">
														<input id="trainees_institution" type="text" class="form-control{{ $errors->has('trainees_institution') ? ' is-invalid' : '' }}" name="trainees_institution" style="width: 100%" required value="{{$affModal->trainees_institution}}">

														@if ($errors->has('trainees_institution'))
														<span class="invalid-feedback">
															<strong>{{ $errors->first('trainees_institution') }}</strong>
														</span>
														@endif
													</div>
												</div>

												<div class="form-group row">
													<label for="trainees_institution_address" class="col-md-4 col-form-label text-md-left">{{ __('Institution Address:') }}</label>

													<div class="col-md-8">
														<input id="trainees_institution_address" type="text" class="form-control{{ $errors->has('trainees_institution_address') ? ' is-invalid' : '' }}" name="trainees_institution_address" style="width: 100%" required value="{{$affModal->trainees_institution_address}}">

														@if ($errors->has('trainees_institution_address'))
														<span class="invalid-feedback">
															<strong>{{ $errors->first('trainees_institution_address') }}</strong>
														</span>
														@endif
													</div>
												</div>

												<div class="form-group row">
													<label for="trainees_specialization" class="col-md-4 col-form-label text-md-left">{{ __('Specialization:') }}</label>

													<div class="col-md-8">
														<input id="trainees_specialization" type="text" class="form-control{{ $errors->has('trainees_specialization') ? ' is-invalid' : '' }}" name="trainees_specialization" style="width: 100%" required value="{{$affModal->trainees_specialization}}">

														@if ($errors->has('trainees_specialization'))
														<span class="invalid-feedback">
															<strong>{{ $errors->first('trainees_specialization') }}</strong>
														</span>
														@endif
													</div>
												</div>

												<div class="form-group row">
													<label for="trainees_email" class="col-md-4 col-form-label text-md-left">{{ __('Email:') }}</label>

													<div class="col-md-8">
														<input id="trainees_email" type="text" class="form-control{{ $errors->has('trainees_email') ? ' is-invalid' : '' }}" name="trainees_email" style="width: 100%" required value="{{$affModal->trainees_email}}">

														@if ($errors->has('trainees_email'))
														<span class="invalid-feedback">
															<strong>{{ $errors->first('trainees_email') }}</strong>
														</span>
														@endif
													</div>
												</div>

												<div class="form-group row">
													<label for="trainees_contactno" class="col-md-4 col-form-label text-md-left">{{ __('Contact no.:') }}</label>

													<div class="col-md-8">
														<input id="trainees_contactno" type="text" class="form-control{{ $errors->has('trainees_contactno') ? ' is-invalid' : '' }}" name="trainees_contactno" style="width: 100%" value="{{$affModal->trainees_contactno}}">

														@if ($errors->has('trainees_contactno'))
														<span class="invalid-feedback">
															<strong>{{ $errors->first('trainees_contactno') }}</strong>
														</span>						
														@endif
													</div>
												</div>

												<div class="form-group row">
													<label for="trainees_profbackground" class="col-md-4 col-form-label text-md-left">{{ __('Academic/Professional Background:') }}</label>

													<div class="col-md-8">
														<input id="trainees_profbackground" type="text" class="form-control{{ $errors->has('trainees_profbackground') ? ' is-invalid' : '' }}" name="trainees_profbackground" style="width: 100%" value="{{$affModal->trainees_profbackground}}">

														@if ($errors->has('trainees_profbackground'))
														<span class="invalid-feedback">
															<strong>{{ $errors->first('trainees_profbackground') }}</strong>
														</span>						
														@endif
													</div>
												</div>

												<div class="form-group row">
													<label for="trainees_committee" class="col-md-4 col-form-label text-md-left">{{ __('Committee:') }}</label>

													<div class="col-md-8">
														<input type="text" id="trainees_committee" class="form-control{{ $errors->has('trainees_committee') ? ' is-invalid' : '' }}" name="trainees_committee" style="width: 100%" value="{{$affModal->trainees_committee}}">

														@if ($errors->has('trainees_committee'))
														<span class="invalid-feedback">
															<strong>{{ $errors->first('trainees_committee') }}</strong>
														</span>						
														@endif
													</div>
												</div>

												<div class="form-group row">
													<label for="trainees_recrole" class="col-md-4 col-form-label text-md-left">{{ __('Role in REC*:') }}</label>

													<div class="col-md-8">
														<select id="trainees_recrole" class="form-control{{ $errors->has('trainees_recrole') ? ' is-invalid' : '' }}" name="trainees_recrole" style="width: 100%" required>
															<option value="{{$affModal->acc_roles->id}}" selected>{{$affModal->acc_roles->roleinevent}}</option>
															@foreach($rec_role as $id => $rr)
															<option value="{{$id}}">{{$rr}}</option>
															@endforeach
														</select>
														@if ($errors->has('trainees_recrole'))
														<span class="invalid-feedback">
															<strong>{{ $errors->first('trainees_recrole') }}</strong>
														</span>						
														@endif
													</div>
												</div>

												<div class="modal-footer">
													<button type="submit" class="btn bg-green">Save Change</button>
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="card">
						<div class="card-header navbar-light"><strong class="text text-info">Accreditation Visits/Orientation Attended</strong>

						</div>
						<div class="card-body">

							<table class="table table-condensed table-hover">
								<thead>
									<tr>
										<td>No.</td>
										<td>Training Title</td>
										<td>Institution</td>
										<td>Training Date</td>
									</tr>
								</thead>
								<tbody>
									@foreach($trainee->traineesAttendance as $id => $ta)
									<tr>
										<td>{{$id+1}}</td>
										<td>{{$ta->eventsorientations->training_type_name}}</td>
										<td>{{$ta->eventsorientations->institution_name}}</td>
										<td>{{\Carbon\Carbon::createFromFormat('Y-m-d', $ta->eventsorientations->training_start_date)->format('F j, Y')}} - {{\Carbon\Carbon::createFromFormat('Y-m-d', $ta->eventsorientations->training_end_date)->format('F j, Y')}}</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@endforeach

							<div class="modal fade" id="add-affiliation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header bg-aqua">
											<h4 class="modal-title" id="myModalLabel">Affiliation Form</h4>
										</div>
										<div class="modal-body">
											<form method="POST" action="{{route('add-affiliation', ['id' => $trainee->id])}}">
												{{ csrf_field() }}

												<div class="form-group row">
													<label for="trainees_institution" class="col-md-4 col-form-label text-md-left">{{ __('Institution/Department:') }}</label>

													<div class="col-md-8">
														<input id="trainees_institution" type="text" class="form-control{{ $errors->has('trainees_institution') ? ' is-invalid' : '' }}" name="trainees_institution" style="width: 100%" required>

														@if ($errors->has('trainees_institution'))
														<span class="invalid-feedback">
															<strong>{{ $errors->first('trainees_institution') }}</strong>
														</span>
														@endif
													</div>
												</div>

												<div class="form-group row">
													<label for="trainees_institution_address" class="col-md-4 col-form-label text-md-left">{{ __('Institution Address:') }}</label>

													<div class="col-md-8">
														<input id="trainees_institution_address" type="text" class="form-control{{ $errors->has('trainees_institution_address') ? ' is-invalid' : '' }}" name="trainees_institution_address" style="width: 100%" required>

														@if ($errors->has('trainees_institution_address'))
														<span class="invalid-feedback">
															<strong>{{ $errors->first('trainees_institution_address') }}</strong>
														</span>
														@endif
													</div>
												</div>

												<div class="form-group row">
													<label for="trainees_specialization" class="col-md-4 col-form-label text-md-left">{{ __('Specialization:') }}</label>

													<div class="col-md-8">
														<input id="trainees_specialization" type="text" class="form-control{{ $errors->has('trainees_specialization') ? ' is-invalid' : '' }}" name="trainees_specialization" style="width: 100%" required>

														@if ($errors->has('trainees_specialization'))
														<span class="invalid-feedback">
															<strong>{{ $errors->first('trainees_specialization') }}</strong>
														</span>
														@endif
													</div>
												</div>

												<div class="form-group row">
													<label for="trainees_email" class="col-md-4 col-form-label text-md-left">{{ __('Email:') }}</label>

													<div class="col-md-8">
														<input id="trainees_email" type="text" class="form-control{{ $errors->has('trainees_email') ? ' is-invalid' : '' }}" name="trainees_email" style="width: 100%" required>

														@if ($errors->has('trainees_email'))
														<span class="invalid-feedback">
															<strong>{{ $errors->first('trainees_email') }}</strong>
														</span>
														@endif
													</div>
												</div>

												<div class="form-group row">
													<label for="trainees_contactno" class="col-md-4 col-form-label text-md-left">{{ __('Contact no.:') }}</label>

													<div class="col-md-8">
														<input id="trainees_contactno" type="text" class="form-control{{ $errors->has('trainees_contactno') ? ' is-invalid' : '' }}" name="trainees_contactno" style="width: 100%">

														@if ($errors->has('trainees_contactno'))
														<span class="invalid-feedback">
															<strong>{{ $errors->first('trainees_contactno') }}</strong>
														</span>						
														@endif
													</div>
												</div>

												<div class="form-group row">
													<label for="trainees_profbackground" class="col-md-4 col-form-label text-md-left">{{ __('Academic/Professional Background:') }}</label>

													<div class="col-md-8">
														<input id="trainees_profbackground" type="text" class="form-control{{ $errors->has('trainees_profbackground') ? ' is-invalid' : '' }}" name="trainees_profbackground" style="width: 100%">

														@if ($errors->has('trainees_profbackground'))
														<span class="invalid-feedback">
															<strong>{{ $errors->first('trainees_profbackground') }}</strong>
														</span>						
														@endif
													</div>
												</div>

												<div class="form-group row">
													<label for="trainees_committee" class="col-md-4 col-form-label text-md-left">{{ __('Committee:') }}</label>

													<div class="col-md-8">
														<input type="text" id="trainees_committee" class="form-control{{ $errors->has('trainees_committee') ? ' is-invalid' : '' }}" name="trainees_committee" style="width: 100%">

														@if ($errors->has('trainees_committee'))
														<span class="invalid-feedback">
															<strong>{{ $errors->first('trainees_committee') }}</strong>
														</span>						
														@endif
													</div>
												</div>


												<div class="form-group row">
													<label for="trainees_recrole" class="col-md-4 col-form-label text-md-left">{{ __('Role in REC*:') }}</label>

													<div class="col-md-8">
														<select id="trainees_recrole" class="form-control{{ $errors->has('trainees_recrole') ? ' is-invalid' : '' }}" name="trainees_recrole" style="width: 100%" required>
															<option selected="">Select Role in REC</option>
															@foreach($rec_role as $id => $rr)
															<option value="{{$id}}">{{$rr}}</option>
															@endforeach
														</select>
														@if ($errors->has('trainees_recrole'))
														<span class="invalid-feedback">
															<strong>{{ $errors->first('trainees_recrole') }}</strong>
														</span>						
														@endif
													</div>
												</div>


												<div class="modal-footer">
													<button type="submit" class="btn bg-aqua">Add</button>
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@endsection