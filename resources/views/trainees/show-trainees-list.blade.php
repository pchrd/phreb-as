@extends('layouts.myApp')

@section('content')

<div class="container">
	<div class="col-md-12" id="verifyuser">
		@if(session('success'))
		<div class="alert alert-success">
			{{session('success')}}
			<button class="close" onclick="document.getElementById('verifyuser').style.display='none'">x</button>
		</div>
		@endif
	</div> 

	<div class="row">
		<div class="col-md-12 col-md-offset-1">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3><strong>List of Trainees</strong>
						<a href="#" data-toggle="modal" data-target="#add-trainee" class="btn btn-outline-info small">Add</a>
					</h3>
				</div>
				<div class="panel-body">
					<table class="table table-hover table-bordered" id="trainees_tbl">
						<thead class="badge-success">
							<tr>
								<td>Last Name</td>
								<td>First Name</td>
								<td>Middle Name</td>
								<td>Gender</td>
								<td>Name for Certificate</td>
								<td>Actions</td>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="trainee" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-yellow">
				<h4 class="modal-title" id="myModalLabel">Trainee</h4>
			</div>
			<div class="modal-body">
				<form method="POST" action="">
					{{ csrf_field() }}

					<input id="lname" type="text" class="form-control{{ $errors->has('lname') ? ' is-invalid' : '' }}" name="lname" required>
					@if ($errors->has('lname'))
					<span class="invalid-feedback" role="alert">
						<strong>{{ $errors->first('lname') }}</strong>
					</span>
					@endif
					<div class="modal-footer">
						<input type="hidden" name="trainee_id" id="trainee_id" value=""/>
						<input type="hidden" name="button_action" id="button_action" value="insert">
						<input type="submit" name="submit" id="action" value="Add" class="btn btn-info">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">

	$(function(){
		// alert(moment().add(1, 'M').format('YYYY-MM-DD'))
		var oTable = $('#trainees_tbl').DataTable({

			responsive: true,
			
			dom: '<"fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix ui-corner-tl ui-corner-tr"HlfrB>'+
			't'+'<"fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix ui-corner-bl ui-corner-br"ip>',

			buttons:[
        		// ['copy', 'csv', 'excel', 'pdf', 'print'],
        		{  
        			extend: 'colvis',

        		},

        		{ 
        			extend: 'excel',
        			footer: true,
        			exportOptions: {
        				columns: []
        			}
        		},

        		{  
        			extend: 'copy',

        		},

        		{  
        			extend: 'csv',

        		},

        		{  
        			extend: 'pdf',

        		},
        		{  
        			extend: 'print',
        			message: "REC Records",

        			customize: function (win) {
        				$(win.document.body)
        				.css('font-size', '10pt')
        				.prepend(
        					'<img src="{{asset('image/image002.jpg')}}" style="position:static; top:10px; left:10px;" />'
        					);

        				$(win.document.body).find('table')
        				.addClass('compact')
        				.css('font-size', 'inherit');
        			}
        		}

        		],

        		stateSave: true,
        		processing: true,
        		serverSide: true,
        		ajax: {
        			url: '{{ route('trainees/show_ajax') }}',
        		},

        		columns: [
        		{ data: 'trainees_lname',   name: 'trainees_lname', searchable:true, visible:true},
        		{ data: 'trainees_fname', name: 'trainees_fname'},
        		{ data: 'trainees_mname', name: 'trainees_mname', visible:true},
        		{ data: 'trainees_gender', name: 'trainees_gender', visible:true},
        		{ data: 'trainees_certificatename', name: 'trainees_certificatename', visible:true},

        		{ data: 'action', name:'action', orderable: false, searchable: false}
        		],

        	});

	});
</script>


<div class="modal fade" id="add-trainee" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-aqua">
				<h4 class="modal-title" id="myModalLabel">Trainee Form</h4>
			</div>
			<div class="modal-body">
				<form method="POST" action="{{route('add-trainee')}}">
					{{ csrf_field() }}

					<div class="form-group row">
						<label for="trainees_lname" class="col-md-4 col-form-label text-md-left">{{ __('Last Name:') }}</label>

						<div class="col-md-8">
							<input id="trainees_lname" type="text" class="form-control{{ $errors->has('trainees_lname') ? ' is-invalid' : '' }}" name="trainees_lname" required>

							@if ($errors->has('trainees_lname'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('trainees_lname') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group row">
						<label for="trainees_fname" class="col-md-4 col-form-label text-md-left">{{ __('First Name:') }}</label>

						<div class="col-md-8">
							<input id="trainees_fname" type="text" class="form-control{{ $errors->has('trainees_fname') ? ' is-invalid' : '' }}" name="trainees_fname" required>

							@if ($errors->has('trainees_fname'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('trainees_fname') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group row">
						<label for="trainees_mname" class="col-md-4 col-form-label text-md-left">{{ __('Middle Name:') }}</label>

						<div class="col-md-8">
							<input id="trainees_mname" type="text" class="form-control{{ $errors->has('trainees_mname') ? ' is-invalid' : '' }}" name="trainees_mname">

							@if ($errors->has('trainees_mname'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('trainees_mname') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group row">
						<label for="trainees_gender" class="col-md-4 col-form-label text-md-left">{{ __('Gender:') }}</label>

						<div class="col-md-8">
							<select id="trainees_gender" class="form-control{{ $errors->has('trainees_gender') ? ' is-invalid' : '' }}" name="trainees_gender" style="width: 100%" required>
								<option selected>Please Select</option>
								<option value="Male">Male</option>
								<option value="Female">Female</option>
							</select>

							@if ($errors->has('trainees_gender'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('trainees_gender') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group row">
						<label for="trainees_certificatename" class="col-md-4 col-form-label text-md-left">{{ __('For Certificate:') }}</label>

						<div class="col-md-8">
							<input id="trainees_certificatename" type="text" class="form-control{{ $errors->has('trainees_certificatename') ? ' is-invalid' : '' }}" name="trainees_certificatename" style="width: 100%" required>

							@if ($errors->has('trainees_certificatename'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('trainees_certificatename') }}</strong>
							</span>						
							@endif

							<p class="small text text-danger">Complete Name and Title to be reflected in the certificate.</p>
						</div>
					</div>
					
					<div class="modal-footer">
						<button type="submit" class="btn bg-aqua">Add</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

@endsection