@extends('layouts.myApp')

@section('content')

<div class="container">
	<div class="col-md-12" id="verifyuser">
		@if(session('success'))
		<div class="alert alert-success">
			{{session('success')}}
			<button class="close" onclick="document.getElementById('verifyuser').style.display='none'">x</button>
		</div>
		@endif
	</div> 

	<div class="row">
		<div class="col-md-12 col-md-offset-1">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3><strong>Training Events/Orientations for Accreditation</strong>
						<a href="#" data-toggle="modal" data-target="#addtrainingtitle" class="btn btn-outline-info small">Create</a>
					</h3>
				</div>
				<div class="panel-body">
					<table class="table table-hover table-bordered" id="recdetails_table">
						<thead class="badge-info">
							<tr>
								<td>ID</td>
								<td>Training Title</td>
								<td>Institution</td>
								<td>Start Date</td>
								<td>End Date</td>
								<td>Created by</td>
								<td>Actions</td>
							</tr>
						</thead>
						<tbody>
							@foreach($trainingevents as $te)
							<tr>
								<td>{{$te->id}}</td>
								<td>{{$te->training_type_name}}</td>
								<td>{{$te->institution_name}}</td>
								<td>{{\Carbon\Carbon::createFromFormat('Y-m-d', $te->training_start_date)->format('F j, Y')}}</td>
								<td>{{\Carbon\Carbon::createFromFormat('Y-m-d', $te->training_end_date)->format('F j, Y')}}</td>
								<td>{{$te->name}}</td>
								<td><div class="form-group"><a title="Edit" href="#" data-target="#edit{{$te->id}}" data-toggle="modal" class="btn btn-outline-success"><span class="fa fa-pencil"></span></a><a title="Attendees" href="{{route('view-events-attendees', ['id' => $te->id])}}" class="btn btn-outline-info"><span class="fa fa-users"></span><span class="badge badge-secondary"> {{$te->counts}} </span></a></div></td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="addtrainingtitle" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header label-info">
				<h4 class="modal-title" id="myModalLabel">Create Training</h4>
			</div>
			<div class="modal-body">
				<form method="POST" action="{{route('add_trainingtitle')}}">
					{{ csrf_field() }}

					<div class="form-group row">
						<label for="training_type_name" class="col-md-4 col-form-label text-md-left">{{ __('Training Title/Name:') }}</label>
						<div class="col-md-8">
							<input id="training_type_name" type="text" class="form-control{{ $errors->has('training_type_name') ? ' is-invalid' : '' }}" name="training_type_name" required>
							@if ($errors->has('training_type_name'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('training_type_name') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group row">
						<label for="institution_name" class="col-md-4 col-form-label text-md-left">{{ __('Institution:') }}</label>
						<div class="col-md-8">
							<option value="" selected></option>
							<select id="institution_name_s2" class="form-control{{ $errors->has('training_type_name') ? ' is-invalid' : '' }}" name="institution_name" required  style="width: 100%;" autofocus>
								<option selected></option>
								@foreach($institution as $id => $institutions)
								<option value="{{$institutions}}">{{$institutions}}</option>
								@endforeach

							</select>
							@if ($errors->has('institution_name'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('institution_name') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group row">
						<label for="rec_name" class="col-md-4 col-form-label text-md-left">{{ __('REC Name:') }}</label>
						<div class="col-md-8">

							<select id="reclist_s2" class="form-group{{ $errors->has('rec_name') ? ' is-invalid' : '' }}" name="rec_name" style="width: 100%;" autofocus>
								<option value="" selected></option>
								@foreach($reclist as $id => $reclists)
								<option value="{{$id}}">{{$reclists}}</option>
								@endforeach
							</select>
							
							@if ($errors->has('rec_name'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('rec_name') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group row">
						<label for="training_start_date" class="col-md-4 col-form-label text-md-left">{{ __('Start Date:') }}</label>
						<div class="col-md-8">
							<input id="training_start_date" type="date" class="form-control{{ $errors->has('training_start_date') ? ' is-invalid' : '' }}" name="training_start_date" required>
							@if ($errors->has('training_start_date'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('training_start_date') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group row">
						<label for="training_end_date" class="col-md-4 col-form-label text-md-left">{{ __('End Date:') }}</label>
						<div class="col-md-8">
							<input id="training_end_date" type="date" class="form-control{{ $errors->has('training_end_date') ? ' is-invalid' : '' }}" name="training_end_date" required>
							@if ($errors->has('training_end_date'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('training_end_date') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="modal-footer">
						<button type="submit" class="btn btn-info">Create</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

@foreach($trainingevents as $te)
<div class="modal fade" id="edit{{$te->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header label-info">
				<h4 class="modal-title" id="myModalLabel">Edit Ethics Training/Orientation</h4>
			</div>
			<div class="modal-body">
				<form method="POST" action="{{route('update_trainingtitle', ['id' => $te->id])}}">
					{{ csrf_field() }}

					<div class="form-group row">
						<label for="training_type_name" class="col-md-4 col-form-label text-md-left">{{ __('Training Title/Name:') }}</label>
						<div class="col-md-8">
							<input id="training_type_name" type="text" class="form-control{{ $errors->has('training_type_name') ? ' is-invalid' : '' }}" name="training_type_name" value="{{$te->training_type_name}}"  required>
							@if ($errors->has('training_type_name'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('training_type_name') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group row">
						<label for="institution_name" class="col-md-4 col-form-label text-md-left">{{ __('Institution:') }}</label>
						<div class="col-md-8">

							<input id="institution_name_s2" type="text" class="form-control{{ $errors->has('training_type_name') ? ' is-invalid' : '' }}" name="institution_name" value="{{$te->institution_name}}" required  style="width: 100%;">
		
							@if ($errors->has('institution_name'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('institution_name') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group row">
						<label for="rec_name" class="col-md-4 col-form-label text-md-left">{{ __('REC Name:') }}</label>
						<div class="col-md-8">
							
							<select id="reclist_s2" class="form-control{{ $errors->has('rec_name') ? ' is-invalid' : '' }}" name="rec_name" autofocus style="width: 100%">
								<option value="{{$te->rec_name}}" selected>{{$te->reclist_name}}</option>
								@foreach($reclist as $id => $reclists)
									<option value="{{$id}}">{{$reclists}}</option>
								@endforeach
							</select>
							
							@if ($errors->has('rec_name'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('rec_name') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group row">
						<label for="training_start_date" class="col-md-4 col-form-label text-md-left">{{ __('Start Date:') }}</label>
						<div class="col-md-8">
							<input id="training_start_date" type="date" class="form-control{{ $errors->has('training_start_date') ? ' is-invalid' : '' }}" name="training_start_date" required value="{{$te->training_start_date}}">
							@if ($errors->has('training_start_date'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('training_start_date') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group row">
						<label for="training_end_date" class="col-md-4 col-form-label text-md-left">{{ __('End Date:') }}</label>
						<div class="col-md-8">
							<input id="training_end_date" type="date" class="form-control{{ $errors->has('training_end_date') ? ' is-invalid' : '' }}" name="training_end_date" required value="{{$te->training_end_date}}">
							@if ($errors->has('training_end_date'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('training_end_date') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="modal-footer">
						<button type="submit" class="btn btn-success">Save Changes</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endforeach

@endsection