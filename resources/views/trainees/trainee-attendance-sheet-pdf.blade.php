<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<div class="row">
		<div class="col-md-12 col-md-offset-1">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3><text class="text text-info">{{$id->training_type_name}} - {{$id->institution_name}}</text> <span class="float-right">{{\Carbon\Carbon::createFromFormat('Y-m-d', $id->training_start_date)->format('F j, Y')}}</span></strong></h3>
				</div>
				<div class="panel-body">
					<table class="table table-bordered">
						<thead>
							<tr>
								<td>No.</td>
								<td>Last Name</td>
								<td>First Name</td>
								<td>Middle Name</td>
								<td>Gender</td>
								<td>Institution</td>
								<td>Email</td>
								<td>Contact Number</td>
								<td>Signature</td>
							</tr>
						</thead>

						<tbody>
							@foreach($attendee as $id => $attendees)
							<tr>
								<td>{{$id + 1}}</td>
								<td>{{$attendees->trainees->trainees_lname}}</td>
								<td>{{$attendees->trainees->trainees_fname}}</td>
								<td>{{$attendees->trainees->trainees_mname}}</td>
								<td>{{$attendees->trainees->trainees_gender}}</td>
								<td>{{$attendees->trainees->affiliationsLatestOnly->trainees_institution}}</td>
								<td>{{$attendees->trainees->affiliationsLatestOnly->trainees_email}}</td>
								<td>{{$attendees->trainees->affiliationsLatestOnly->trainees_contactno}}</td>
								<td></td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>