@extends('layouts.myApp')

@section('content')

<div class="container">
	<div class="col-md-12" id="verifyuser">
		@if(session('success'))
		<div class="alert alert-success">
			{{session('success')}}
			<button class="close" onclick="document.getElementById('verifyuser').style.display='none'">x</button>
		</div>
		@endif

	</div> 

	<div class="row">
		<div class="col-md-12 col-md-offset-1">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3><strong>Attendance Management for Accreditation Trainings & Orientations</strong></h3>
					<div class="container">
						<a href="#" data-toggle="modal" data-target="#addattendee" data-keyboard="false" data-backdrop="static" class="btn bg-aqua btn-block small">Click to add attendance</a>
					</div><br>
				</div>
				<div class="panel-body">
					<table class="table table-hover table-bordered" id="trainingmanagement">
						<thead class="badge-success">
							<tr>
								<td>Last Name</td>
								<td>First Name</td>
								<td>Middle Name</td>
								<td>Title of Events/Orientation</td>
								<td>Event Role</td>
								<td>Institution</td>
								<td>Start Date</td>
								<td>End Date</td>
								<td>Date Created</td>
								<td>Added By</td>
								<td>Actions</td>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

	$(function(){
		// alert(moment().add(1, 'M').format('YYYY-MM-DD'))
		var oTable = $('#trainingmanagement').DataTable({

			responsive: true,
			
			dom: '<"fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix ui-corner-tl ui-corner-tr"HlfrB>'+
				't'+'<"fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix ui-corner-bl ui-corner-br"ip>',

        	buttons:[
        		// ['copy', 'csv', 'excel', 'pdf', 'print'],
        		{  
       				extend: 'colvis',
		          
       			},

        		{ 
        		   extend: 'excel',
		           footer: true,
		           exportOptions: {
		                columns: []
		           }
       			},

       			{  
       				extend: 'copy',
		          
       			},

       			{  
       				extend: 'csv',
		          
       			},

       			{  
       				extend: 'pdf',
		          
       			},
       			{  
       				extend: 'print',
       				message: "REC Records",

       				customize: function (win) {
                    $(win.document.body)
                        .css('font-size', '10pt')
                        .prepend(
                            '<img src="{{asset('image/image002.jpg')}}" style="position:static; top:10px; left:10px;" />'
                        );

                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                	}
       			}
			       
      		 ],

      		stateSave: true,
			processing: true,
			serverSide: true,
			ajax: {
				url: '{{ route('trainees/show_ajax/trainingmanagement') }}',
			},
      		
			columns: [
			{ data: 'trainees.trainees_lname', name: 'trainees.trainees_lname'},
			{ data: 'trainees.trainees_fname', name: 'trainees.trainees_fname'},
			{ data: 'trainees.trainees_mname', name: 'trainees.trainees_mname'},
			{ data: 'eventsorientations.training_type_name', name: 'eventsorientations.training_type_name'},
			{ data: 'roleinevents.roleinevent', name: 'roleinevents.roleinevent'},
			{ data: 'eventsorientations.institution_name', name: 'eventsorientations.institution_name', visible:false},
			{ data: 'eventsorientations.training_start_date', name: 'eventsorientations.training_start_date', visible:false},
			{ data: 'eventsorientations.training_end_date', name: 'eventsorientations.training_end_date', visible:false},
			{ data: 'created_at', name: 'created_at', visible:true},
			{ data: 'addedby_users.name', name: 'addedby_users.name', visible:false}, 
			{ data: 'action', name: 'action', visible:true}
			],

		});

	});
</script>

<div class="modal fade" id="addattendee" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-aqua">
				<h4 class="modal-title" id="myModalLabel">Attendance Form</h4>
			</div>
			<div class="modal-body">
			<form method="POST" action="{{route('add-trainee-attendance')}}">
				{{ csrf_field() }}

				<div class="container">
					<select id="eventmodal" type="text" class="form-control{{ $errors->has('te_orientation_id') ? ' is-invalid' : '' }}" name="te_orientation_id" required autofocus style="width: 100%">
						<option selected></option>
						@foreach($eventnames as $id => $event)
						<option value="{{$id}}">{{$event}}</option>
						@endforeach
					</select>
					@if ($errors->has('te_orientation_id'))
					<span class="invalid-feedback" role="alert">
						<strong>{{ $errors->first('te_orientation_id') }}</strong>
					</span>
					@endif
				</div><br>

				<div class="container">
					<select id="traineemodal1" class="form-control{{ $errors->has('te_role_event_id') ? ' is-invalid' : '' }}" name="te_role_event_id" style="width: 100%" requred autofocus>
						<option selected></option>
						@foreach($getTrainingRole as $id => $gtr)
						<option value="{{$id}}">{{$gtr}}</option>
						@endforeach
					</select>
					@if ($errors->has('te_role_event_id'))
					<span class="invalid-feedback" role="alert">
						<strong>{{ $errors->first('te_role_event_id') }}</strong>
					</span>
					@endif
				</div><br>

				<div class="container">
					<select id="traineemodal" type="text" class="form-control{{ $errors->has('te_trainee_id') ? ' is-invalid' : '' }}" name="te_trainee_id" style="width: 100%">
						<option selected></option>
						@foreach($trainees as $id => $trainee)te_trainee_id
						<option value="{{$id}}">{{$trainee}}</option>
						@endforeach
					</select>
					@if ($errors->has('te_trainee_id'))
					<span class="invalid-feedback" role="alert">
						<strong>{{ $errors->first('te_trainee_id') }}</strong>
					</span>
					@endif
				</div><br>

				<div id="showMe" class="container">
					<label class="text text-danger">Note: If no Trainee Name exist, fill this form below.( * Required field)</label>
			

				<div class="form-group row">
					<label for="trainees_lname" class="col-md-4 col-form-label text-md-left">{{ __('Last Name*:') }}</label>

					<div class="col-md-8">
						<input id="trainees_lname" type="text" class="form-control{{ $errors->has('trainees_lname') ? ' is-invalid' : '' }}" name="trainees_lname" placeholder="">

						@if ($errors->has('trainees_lname'))
						<span class="invalid-feedback">
							<strong>{{ $errors->first('trainees_lname') }}</strong>
						</span>
						@endif
					</div>
				</div>

				<div class="form-group row">
					<label for="trainees_fname" class="col-md-4 col-form-label text-md-left">{{ __('First Name*:') }}</label>

					<div class="col-md-8">
						<input id="trainees_fname" type="text" class="form-control{{ $errors->has('trainees_fname') ? ' is-invalid' : '' }}" name="trainees_fname" placeholder="">

						@if ($errors->has('trainees_fname'))
						<span class="invalid-feedback">
							<strong>{{ $errors->first('trainees_fname') }}</strong>
						</span>
						@endif
					</div>
				</div>

				<div class="form-group row">
					<label for="trainees_mname" class="col-md-4 col-form-label text-md-left">{{ __('Middle Name/Initial*:') }}</label>

					<div class="col-md-8">
						<input id="trainees_mname" type="text" class="form-control{{ $errors->has('trainees_mname') ? ' is-invalid' : '' }}" name="trainees_mname" placeholder="">

						@if ($errors->has('trainees_mname'))
						<span class="invalid-feedback">
							<strong>{{ $errors->first('trainees_mname') }}</strong>
						</span>
						@endif
					</div>
				</div>

				<div class="form-group row">
					<label for="trainees_gender" class="col-md-4 col-form-label text-md-left">{{ __('Gender*:') }}</label>

					<div class="col-md-8">
						<select id="trainees_gender" class="form-control{{ $errors->has('te_orientation_id') ? ' is-invalid' : '' }}" name="trainees_gender" style="width: 100%">
							<option value="Male">Male</option>
							<option value="Female">Female</option>
						</select>

						@if ($errors->has('trainees_gender'))
						<span class="invalid-feedback">
							<strong>{{ $errors->first('trainees_gender') }}</strong>
						</span>
						@endif
					</div>
				</div>

				<div class="form-group row">
					<label for="trainees_institution" class="col-md-4 col-form-label text-md-left">{{ __('Institution*:') }}</label>

					<div class="col-md-8">
						<input id="trainees_institution" type="text" class="form-control{{ $errors->has('trainees_institution') ? ' is-invalid' : '' }}" name="trainees_institution" style="width: 100%">

						@if ($errors->has('trainees_institution'))
						<span class="invalid-feedback">
							<strong>{{ $errors->first('trainees_institution') }}</strong>
						</span>
						@endif
					</div>
				</div>

				<div class="form-group row">
					<label for="trainees_institution_address" class="col-md-4 col-form-label text-md-left">{{ __('Institution Address*:') }}</label>

					<div class="col-md-8">
						<input id="trainees_institution_address" type="text" class="form-control{{ $errors->has('trainees_institution_address') ? ' is-invalid' : '' }}" name="trainees_institution_address" style="width: 100%">

						@if ($errors->has('trainees_institution_address'))
						<span class="invalid-feedback">
							<strong>{{ $errors->first('trainees_institution_address') }}</strong>
						</span>
						@endif
					</div>
				</div>

				<div class="form-group row">
					<label for="trainees_specialization" class="col-md-4 col-form-label text-md-left">{{ __('Specialization*:') }}</label>

					<div class="col-md-8">
						<input id="trainees_specialization" type="text" class="form-control{{ $errors->has('trainees_specialization') ? ' is-invalid' : '' }}" name="trainees_specialization" style="width: 100%">

						@if ($errors->has('trainees_specialization'))
						<span class="invalid-feedback">
							<strong>{{ $errors->first('trainees_specialization') }}</strong>
						</span>
						@endif
					</div>
				</div>

				<div class="form-group row">
					<label for="trainees_email" class="col-md-4 col-form-label text-md-left">{{ __('Email*:') }}</label>

					<div class="col-md-8">
						<input id="trainees_email" type="email" class="form-control{{ $errors->has('trainees_email') ? ' is-invalid' : '' }}" name="trainees_email" style="width: 100%"><h6><small>Please use primary email only</small></h6>

						@if ($errors->has('trainees_email'))
						<span class="invalid-feedback">
							<strong>{{ $errors->first('trainees_email') }}</strong>
						</span>
						@endif
					</div>
				</div>

				<div class="form-group row">
					<label for="trainees_contactno" class="col-md-4 col-form-label text-md-left">{{ __('Contact no.*:') }}</label>

					<div class="col-md-8">
						<input id="trainees_contactno" type="text" class="form-control{{ $errors->has('trainees_contactno') ? ' is-invalid' : '' }}" name="trainees_contactno" style="width: 100%">

						@if ($errors->has('trainees_contactno'))
						<span class="invalid-feedback">
							<strong>{{ $errors->first('trainees_contactno') }}</strong>
						</span>						
						@endif
					</div>
				</div>

				<div class="form-group row">
					<label for="trainees_profbackground" class="col-md-4 col-form-label text-md-left">{{ __('Academic/Professional Background:') }}</label>

					<div class="col-md-8">
						<input id="trainees_profbackground" type="text" class="form-control{{ $errors->has('trainees_profbackground') ? ' is-invalid' : '' }}" name="trainees_profbackground" style="width: 100%">

						@if ($errors->has('trainees_profbackground'))
						<span class="invalid-feedback">
							<strong>{{ $errors->first('trainees_profbackground') }}</strong>
						</span>						
						@endif
					</div>
				</div>

				<div class="form-group row">
					<label for="trainees_committee" class="col-md-4 col-form-label text-md-left">{{ __('Committee*:') }}</label>

					<div class="col-md-8">
						<input id="trainees_committee" type="text" class="form-control{{ $errors->has('trainees_committee') ? ' is-invalid' : '' }}" name="trainees_committee" style="width: 100%">

						@if ($errors->has('trainees_committee'))
						<span class="invalid-feedback">
							<strong>{{ $errors->first('trainees_committee') }}</strong>
						</span>						
						@endif
					</div>
				</div>

				<div class="form-group row">
					<label for="trainees_recrole" class="col-md-4 col-form-label text-md-left">{{ __('Role in REC*:') }}</label>

					<div class="col-md-8">
						<select id="trainees_recrole" class="form-control{{ $errors->has('trainees_recrole') ? ' is-invalid' : '' }}" name="trainees_recrole" style="width: 100%">
							<option selected>Please select REC Role</option>
						@foreach($acc_recroles as $id => $arr)
							<option  value="{{$id}}">{{$arr}}</option>
						@endforeach
						</select>

						@if ($errors->has('trainees_recrole'))
						<span class="invalid-feedback">
							<strong>{{ $errors->first('trainees_recrole') }}</strong>
						</span>						
						@endif
					</div>
				</div>

				<div class="form-group row">
					<label for="trainees_certificatename" class="col-md-4 col-form-label text-md-left">{{ __('For Certificate:') }}</label>

					<div class="col-md-8">
						<input id="trainees_certificatename" type="text" class="form-control{{ $errors->has('trainees_certificatename') ? ' is-invalid' : '' }}" name="trainees_certificatename" style="width: 100%">

						@if ($errors->has('trainees_certificatename'))
						<span class="invalid-feedback">
							<strong>{{ $errors->first('trainees_certificatename') }}</strong>
						</span>						
						@endif

						<p class="small text text-danger">Complete Name and Title to be reflected in the certificate.</p>
					</div>
				</div>

			</div>
				
					
				<div class="modal-footer">
					<button type="submit" class="btn bg-aqua">Add</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
			</div>
		</div>
	</div>
</div>
@endsection