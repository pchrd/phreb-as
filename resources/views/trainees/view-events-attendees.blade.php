@extends('layouts.myApp')

@section('content')

<div class="container">
	<div class="col-md-12" id="verifyuser">
		@if(session('success'))
		<div class="alert alert-success">
			{{session('success')}}
			<button class="close" onclick="document.getElementById('verifyuser').style.display='none'">x</button>
		</div>
		@endif
	</div> 

	<div class="row">
		<div class="col-md-12 col-md-offset-1">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3><strong>List of Trainee Attended in <i class="text text-info">{{$id->training_type_name}} - {{$id->institution_name}}</i> <small>{{$id->training_start_date}} to {{$id->training_end_date}}</small></strong><a href="{{route('print-attendee', ['id' => $id->id])}}" class="btn btn-outline-danger pull-right fa fa-print"> PDF</a></h3>
				</div>
				<div class="panel-body">
					<table class="table table-hover table-bordered" id="trainees">
						<thead class="badge-info">
							<tr>
								<td>No.</td>
								<td>Role</td>
								<td>Last Name</td>
								<td>First Name</td>
								<td>Middle Name</td>
								<td>Gender</td>
								<td>Institution</td>
								<td>Email</td>
								<td>Contact Number</td>
							</tr>
						</thead>
						@foreach($attendee as $id => $attendees)
						<tbody>
							<tr>
								<td>{{$id + 1}}</td>
								<td>{{$attendees->roleinevents->roleinevent}}</td>
								<td>{{$attendees->trainees->trainees_lname}}</td>
								<td>{{$attendees->trainees->trainees_fname}}</td>
								<td>{{$attendees->trainees->trainees_mname}}</td>
								<td>{{$attendees->trainees->trainees_gender}}</td>
								<td>{{$attendees->affiliationsGetCurrentForViewAttendees->trainees_institution}}</td>
								<td>{{$attendees->affiliationsGetCurrentForViewAttendees->trainees_email}}</td>
								<td>{{$attendees->affiliationsGetCurrentForViewAttendees->trainees_contactno}}</td>
							</tr>
						</tbody>
						@endforeach
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
