@extends('layouts.myApp')
@section('content')
<div class="container">
	<div class="row justify-content-center">
		
		<div class="card col-md-11">
			<div class="card-header badge-info ">
				<label class="text-light">Philippine Health Research Ethics Board</b></label>
				<a href="{{url('/#reports')}}" class=" btn btn-default fa fa-arrow-left pull-right"> Back</a>
			</div>


			<div class="card-body">
				

				<table class="table table-hover table-bordered">
			
					<tbody>
						<tr>
							<th scope="row">Name of REC:</th>
							<td>{{$recdetails->rec_institution}} - {{$recdetails->rec_name}}</td>
							
						</tr>
						<tr>
							<th scope="row">Name of Institution:</th>
							<td>{{$recdetails->rec_institution}}</td>
							
						</tr>
						<tr>
							<th scope="row">Level:</th>
							<td>{{$recdetails->applicationlevel->levels->level_name}}</td>
					
						</tr>

						<tr>
							<th scope="row">Region:</th>
							<td>{{$recdetails->applicationlevel->regions->region_name}}</td>
					
						</tr>

						<tr>
							<th scope="row">Address:</th>
							<td>{{$recdetails->rec_address}}</td>
					
						</tr>

						<tr>
							<th scope="row">REC Email Address:</th>
							<td>{{$recdetails->rec_email}}</td>
					
						</tr>

						<tr>
							<th scope="row">REC Tel. No:</th>
							<td>{{$recdetails->rec_telno}}</td>
					
						</tr>

						<tr>
							<th scope="row">Name of REC Chair:</th>
							<td>
								{{$recdetails->recchairs->rec_chairname}}<br>
								{{$recdetails->recchairs->rec_chairemail}}<br>
								{{$recdetails->recchairs->rec_chairmobile}}<br>
							</td>
					
						</tr>

						<tr>
							<th scope="row">Name of Contact Person:</th>
							<td>
								{{$recdetails->rec_contactperson}}<br>
								{{$recdetails->rec_contactemail}}<br>
								{{$recdetails->rec_contactmobileno}}<br>
							</td>
					
						</tr>

						<tr>
							<th scope="row">REC Date Established:</th>
							<td>{{$recdetails->rec_dateestablished ? \Carbon\Carbon::createFromFormat('Y-m-d', $recdetails->rec_dateestablished)->format('F j, Y') : ''}}</td>
					
						</tr>

						<tr>
							<th scope="row">Accreditation:</th>
							<td>
								<li>Effectivity - <label class="text text-danger">{{$recdetails->applicationlevel->date_accreditation ? \Carbon\Carbon::createFromFormat('Y-m-d', $recdetails->applicationlevel->date_accreditation)->format('F j, Y') : ''}}</label><br></li>
							
								<li>Expiry - <label class="text text-danger">{{$recdetails->applicationlevel->date_accreditation_expiry ? \Carbon\Carbon::createFromFormat('Y-m-d', $recdetails->applicationlevel->date_accreditation_expiry)->format('F j, Y') : ''}}</label></li>
								
								<li>{{$recdetails->applicationlevel->rec_accreditation}}</li>
							</td>
						</tr>

					</tbody>
				</table>


			</div>

		</div>

	</div>
</div>
@endsection