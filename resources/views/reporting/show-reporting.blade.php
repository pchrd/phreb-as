@extends('layouts.myApp')
@section('content')
<div class="container-fluid">
	<div class="container ui form">

		<div class="col-md-12" id="verifyuser">
			@if(session('success'))
				<div class="alert alert-success">
					{{session('success')}}
					<button class="close" onclick="document.getElementById('verifyuser').style.display='none'">x</button>
				</div>
			@endif
		</div>

		@can('secretariat-access')
			<form action="{{route('insert_report')}}" method="POST">
				{{csrf_field()}}
				<div class="two fields">
					<div class="field">
						<label>Start Date</label>
						<div class="ui calendar" id="rangestart">
							<div class="ui input left icon">
								<i class="calendar icon"></i>
								<input type="date" placeholder="Start" id="start" name="start" required>
							</div>
						</div>
					</div>
					<div class="field">
						<label>End Date</label>
						<div class="ui calendar" id="rangeend">
							<div class="ui input left icon">
								<i class="calendar icon"></i>
								<input type="date" placeholder="End" id="end" name="end" required>
							</div>
						</div>
					</div>
					<div class="field">
						<label>Insert Date</label>
						<div class="ui calendar" id="rangeend">
							<input type="submit" class="btn btn-outline-secondary btn-block">
						</div>
					</div>
				</div>
			</form>
		@endcan


		<div class="card">

			<center>
				<h1 class="card-header label label-info">Accreditation Report as of <b class="text-primary">{{\Carbon\Carbon::createFromFormat('Y-m-d', $reporting->report_start_date)->format('F j, Y')}} to {{\Carbon\Carbon::createFromFormat('Y-m-d', $reporting->report_end_date)->format('F j, Y')}}</b>

					<a href="" class="float-right fa fa-list" title="Summary Per Region" data-toggle="modal" data-target="#regions" data-backdrop="static" data-keyboard="false"></a>

				</h1>
			</center>

			<div class="card-body">
				<table class="table table-responsive table-hover table-bordered" width="100%" id="recdetails_table">
					<thead class="small">
						<tr>
							
							<th>Region</th>
							<th>Level</th>
							<th>Institution Name</th>
							<th>REC Name</th>
							<th>Date Accredited</th>
							<th>Date of Expiry</th>
							<th>Status</th>

							
							
								@can('secretariat-access')
									
									<th>Actions</th>

								@endcan

							@guest

								<th>!</th>

							@endguest

							

						</tr>
					</thead>
				</table>
			</div>

		</div>
		
		<br>

		@guest

		<div class="card">

			<center>
				<h1 class="card-header label label-info">
					LIST OF GENERATED ACCREDITATION REPORT <br>
				</h1>
			</center>

			<div class="card-body">
				<table class="table table-condensed table-hover">

								<tbody>

									
										@foreach($pdfreport as $id => $pdfreports)
											<tr>
												<td>

													<a href="{{route('showpdfreport', ['uniqid' => encrypt($pdfreports->pdf_uniqid)])}}" target="_blank" style="font-size: 20px"><span class="fa fa-file"></span></span> <label class="" style="font-size: 20px">{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $pdfreports->created_at)->format('F j, Y @ G:i:A')}}</label></a> <label class="float-right">{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $pdfreports->created_at)->diffforhumans()}}</label>

												</td>
												
											</tr>
										@endforeach
									
										

								</tbody>
								


							</table>

							{{ $pdfreport->links() }}		
			</div>

		</div>

		<br>


		<div class="card">
			<center><h1 class="card-header label label-info">ACCREDITED RECs PER REGIONS <br>
				
				@foreach($count as $id => $level)	

					{{$level->level_name}}:
					<label class="{{ $id + 1 == '1' ? 'badge bg-primary' : ($id + 1 == '2' ? 'badge bg-info' : 'badge bg-secondary') }}" style="color:white;"> {{$level->counts}} </label>
				
				@endforeach
					
					= <label class="badge badge-danger">Total: {{$count->sum('counts')}} </label>

			</h1></center>

			<div class="card-body">

				<div class="row">
					
					<div class="container">
						<div class="row">


					<div class="container">


					<div class="tab-content" id="myTabContent">

						<div class="tab-pane fade show active" id="pills-level1" role="tabpanel" aria-labelledby="pills-level1-tab">



							<table class="table table-bordered">
								<thead>
									<tr>
										<th>Levels</th>
										<th>Regions</th>
										<th><center>No. of Accredited RECs</center></th>
									</tr>
								</thead>

								<tbody>
									@foreach($levels as $level)
									<tr class="{{ $level->level_id == '1' ? 'bg-primary' : ($level->level_id == '2' ? 'bg-info' : 'bg-secondary') }}" style="color: white; opacity: 0.8;">
										<td><li>{{$level->level_name}}</li></td>
										<td>

											<a href="{{route('filter', ['region' => encrypt($level->region), 'filter' => encrypt($level->level_name)])}}" class="text text-light">{{$level->region}}</a>
											

										</td>
										<td><center>{{$level->levelTotal}}</center></td>
									</tr>
									@endforeach
								</tbody>
							

							</table>


						</div>

						<div class="tab-pane fade" id="pills-level2" role="tabpanel" aria-labelledby="pills-level2-tab">Level 2</div>

						<div class="tab-pane fade" id="pills-level3" role="tabpanel" aria-labelledby="pills-level3-tab">Level 3</div>
					</div>

				</div>
							
							
						    
						</div>



					</div>

				</div>
			</div>
		</div>

		@endguest


	<!-- Modal -->
	<div class="modal fade" id="regions" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h6 class="modal-title" id="exampleModalLabel">Summary per Regions As of <b class="text-primary">{{\Carbon\Carbon::createFromFormat('Y-m-d', $reporting->report_start_date)->format('F j, Y')}} to {{\Carbon\Carbon::createFromFormat('Y-m-d', $reporting->report_end_date)->format('F j, Y')}}</b></h6>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="container-fluid">
						<table class="table table-hover">
							<thead class="small">
								<tr>
									<th>Regions</th>
									<th>No. of Accredited REC's</th>
								</tr>
							</thead>
							<tbody>
								@foreach($regions as $region)
								<tr>
									<td>{{$region->region_name}}</td>
									<td><span class="">{{$region->regionTotal}}</span></td>
								</tr>	
								@endforeach
								
							</tbody>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	
	<script type="text/javascript">

		$(function(){

			var oTable = $('#recdetails_table').DataTable({

				dom: '<"fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix ui-corner-tl ui-corner-tr"HlfrB>'+
				't'+'<"fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix ui-corner-bl ui-corner-br"ip>',

				buttons:[
        		// ['copy', 'csv', 'excel', 'pdf', 'print'],
        		{  
        			extend: 'colvis',

        		},

        		{ 
        			extend: 'excel',
        			footer: true,
        			exportOptions: {
        				columns: []
        			}
        		},

        		{  
        			extend: 'pdf',

        		},
        		{  
        			extend: 'print',
        			message: "List of Research Ethics Committee (REC)",

        			customize: function (win) {
        				$(win.document.body)
        				.css('font-size', '10pt')
        				.prepend(
        					'<img src="{{asset('image/image002.jpg')}}" style="position:static; top:10px; left:10px;" />'
        					);

        				$(win.document.body).find('table')
        				.addClass('compact')
        				.css('font-size', 'inherit');
        			}
        		}

        		],

        		stateSave: true,
        		processing: true,
        		serverSide: true,
        		ajax: {
        			url: '{{ route('show-report-ajax') }}',
        		},

        		columns: [
     
        		{ data: 'region_name',   name: 'regions.region_name', searchable:true },
        		{ data: 'level_description',   name: 'levels.level_description', searchable:true  },
        		{ data: 'rec_institution',   name: 'rec_institution', searchable:true },
        		{ data: 'rec_name',   name: 'rec_name', searchable:true },
        		{ data: 'date_accreditationFORMAT',   name: 'applicationlevel.date_accreditation', searchable:true },
        		{ data: 'date_accreditation_expiryFORMAT',   name: 'applicationlevel.date_accreditation_expiry', searchable:true },
        		{ data: 'status_name',   name: 'statuses.status_name', visible:true, searchable:true },
        		{ data: 'action', name:'action', orderable: false, searchable: false},

        		],

        		createdRow: function(row, data, dataIndex){

        			if (data['date_accreditation_expiry'] <= moment().format('YYYY-MM-DD')){
        				$(row).find('td:eq(5)').addClass('text text-danger');

        			}else if (data['date_accreditation_expiry'] <= moment().add(1, 'M').format('YYYY-MM-DD')){
        				$(row).find('td:eq(5)').addClass('text text-warning');

        			}
        		}

        	});
		});

	</script>
@endsection