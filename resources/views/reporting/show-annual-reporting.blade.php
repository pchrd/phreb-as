@extends('layouts.myApp')
@section('content')
<div class="container-fluid">
	<div class="container ui form">
		<div class="col-md-12" id="verifyuser">
			@if(session('success'))
			<div class="alert alert-success">
				{{session('success')}}
				<button class="close" onclick="document.getElementById('verifyuser').style.display='none'">x</button>
			</div>
			@endif
		</div>

		<div class="row">
			<div class="col-md-12 col-md-offset-1">
				<div class="panel panel-info">
					<div class="panel-heading"><h3><strong>REC Annual Report</strong><small>  <b class="text-primary"></b> <a href="" class="pull-right fa fa-plus" title="Add Annual Report Title" data-toggle="modal" data-target="#add_ar" data-backdrop="static" data-keyboard="false"></a><a href="" class="pull-right fa fa-list" title="View Annual Report Title" data-toggle="modal" data-target="#show_ar" data-backdrop="static" data-keyboard="false"></a></small></h3></div>
					<div class="panel-body">	
						<table class="table table-hover" id="annual_table">
							<thead class="small">
								<tr>
									<th scope="col">ID</th>
									<th>APPLICATION TYPE</th>
									<th scope="col">REGION</th>
									<th scope="col">REC</th>
									<th scope="col">LEVEL</th>
									<th scope="col">INTITUTION NAME</th>
									<th scope="col">REPORT TITLE</th>
									<th scope="col">DOCUMENTS</th>
									<th scope="col">STATUS</th>
									<th scope="col">ACTIONS</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="add_ar">
			<div class="modal-dialog sm">
				<div class="modal-content">
					<!-- Modal Header -->
					<div class="modal-header badge badge-primary">
						<h4 class="modal-title">PHREB Annual Report</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<!-- Modal body -->
					<div class="modal-body">
						<div class="container-fluid">
							<form class="form" action="{{route('add_ar')}}" method="POST"> {{csrf_field()}}
							<label for="ar_name" class="col-md-6 col-form-label text-md-left">{{ __('Name:') }}</label>
							<div class="col-md-12">
								<input id="ar_name" type="text" class="form-control{{ $errors->has('ar_name') ? ' is-invalid' : '' }}" name="ar_name" required="">
								@if ($errors->has('ar_name'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('ar_name') }}</strong>
								</span>
								@endif
							</div>
							<label for="ar_desc" class="col-md-6 col-form-label text-md-left">{{ __('Description:') }}</label>
							<div class="col-md-12">
								<input id="ar_desc" type="text" class="form-control{{ $errors->has('ar_desc') ? ' is-invalid' : '' }}" name="ar_desc">
								@if ($errors->has('ar_desc'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('ar_desc') }}</strong>
								</span>
								@endif
							</div>
							<label for="ar_start" class="col-md-6 col-form-label text-md-left">{{ __('Start:') }}</label>
							<div class="col-md-12">
								<input id="ar_start" type="date" class="form-control{{ $errors->has('ar_start') ? ' is-invalid' : '' }}" name="ar_start" value="{{\Carbon\Carbon::today()->startOfYear()->format('m-d-Y')}}">
								@if ($errors->has('ar_start'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('ar_start') }}</strong>
								</span>
								@endif
							</div>
							<label for="ar_end" class="col-md-6 col-form-label text-md-left">{{ __('End:') }}</label>
							<div class="col-md-12">
								<input id="ar_end" type="date" class="form-control{{ $errors->has('ar_end') ? ' is-invalid' : '' }}" name="ar_end" value="{{\Carbon\Carbon::today()->endOfYear()}}">
								@if ($errors->has('ar_end'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('ar_end') }}</strong>
								</span>
								@endif
							</div><br>
							<div class="modal-footer">
								<button type="submit" class="btn btn-info pull-right">Submit</button>
							</div>
						</form>	
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="show_ar">
			<div class="modal-dialog sm">
				<div class="modal-content">
					<!-- Modal Header -->
					<div class="modal-header badge badge-primary">
						<h4 class="modal-title">PHREB Annual Report Title</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<!-- Modal body -->
					<div class="modal-body">
						<div class="container-fluid">
							<table class="table table-hover nowrap">
							<thead class="small">
								<tr>
									<th>ID</th>
									<th>NAME</th>
									<th>FROM</th>
									<th>TO</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody class="small">
								@foreach($show_annual_title as $sat)
								<tr><td>{{$sat->id}}</td>
									<td><b>{{$sat->ar_name}}</b><br>
										<small><i>{{$sat->ar_desc}}</i></small>
									</td>
									<td>{{\Carbon\Carbon::createFromFormat('Y-m-d', $sat->ar_start_year)->format('F j, Y')}}
	</div></td>
									<td>{{\Carbon\Carbon::createFromFormat('Y-m-d', $sat->ar_end_year)->format('F j, Y')}}</td>
									<td><a href="{{route('art_delete', ['id' => $sat->id])}}" class="btn btn-danger" onclick="return confirm('Do you want to Delete: {{$sat->ar_name}} ?')" >Delete</a></td>
								</tr>
								@endforeach	
							</tbody>
						</table>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>

<script type="text/javascript">

		$(function(){

			var oTable = $('#annual_table').DataTable({

				dom: '<"fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix ui-corner-tl ui-corner-tr"HlfrB>'+
				't'+'<"fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix ui-corner-bl ui-corner-br"ip>',

				buttons:[
        		// ['copy', 'csv', 'excel', 'pdf', 'print'],
        		{  
        			extend: 'colvis',

        		},

        		{ 
        			extend: 'excel',
        			footer: true,
        			exportOptions: {
        				columns: []
        			}
        		},

        		{  
        			extend: 'copy',

        		},

        		{  
        			extend: 'csv',

        		},

        		{  
        			extend: 'pdf',

        		},
        		{  
        			extend: 'print',
        			message: "REC Records",

        			customize: function (win) {
        				$(win.document.body)
        				.css('font-size', '10pt')
        				.prepend(
        					'<img src="{{asset('image/image002.jpg')}}" style="position:static; top:10px; left:10px;" />'
        					);

        				$(win.document.body).find('table')
        				.addClass('compact')
        				.css('font-size', 'inherit');
        			}
        		}

        		],

        		stateSave: true,
        		processing: true,
        		serverSide: true,
        		ajax: {
        			url: '{{ route('showannualreport_ajax') }}',
        		},

        		columns: [
        		{ data: 'recdetails.id',   name: 'recdetails.id', visible:false, searchable:true },
        		{ data: 'recdetails.status.status_name',   name: 'recdetails.status.status_name', visible:false, searchable:true},
        		{ data: 'applicationlevel.regions.region_name',   name: 'applicationlevel.regions.region_name'},
        		{ data: 'recdetails.rec_name',   name: 'recdetails.rec_name'},
        		{ data: 'applicationlevel.levels.level_name',   name: 'applicationlevel.levels.level_name'},
        		{ data: 'recdetails.rec_institution',   name: 'recdetails.rec_institution'},
        		{ data: 'annualreporttitles.ar_name',   name: 'annualreporttitles.ar_name'},
        		{ data: 'documents.recdocuments_file',   name: 'documents.recdocuments_file'},
        		{ data: 'applicationlevel.status.status_name',   name: 'applicationlevel.status.status_name'},

        		{ data: 'action', name:'action', orderable: false, searchable: false}
        		],

        	});
		});
	</script>
@endsection