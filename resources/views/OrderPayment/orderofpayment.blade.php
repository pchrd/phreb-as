@extends('layouts.myApp')
@section('content')

<div class="col-md-12" id="verifyuser">
	@if(session('success'))
	<div class="alert alert-success">
		{{session('success')}}
		<button class="close" onclick="document.getElementById('verifyuser').style.display='none'">x</button>
	</div>
	@endif
</div>

<div class="container">
		<div class="row">
			<div class="col-md-12 col-md-offset-1">
				<div class="panel panel-info">
					<div class="panel-heading"><h3><strong>REC Order of Payment </strong></h3></div>
					<div class="panel-body">

						<nav>
							<div class="nav nav-tabs" id="navbar-example2" role="tablist">
								<a class="nav-item nav-link active text text-info" id="or_req-tab" data-toggle="tab" href="#or_req" role="tab" aria-controls="nav-home" aria-selected="true">OR Request</a>
								<a class="nav-item nav-link text text-info" id="or_rec-tab" data-toggle="tab" href="#or_rec" role="tab" aria-controls="nav-profile" aria-selected="false">OR Receipt</a>

							</div>
						</nav>


						<div class="tab-content" id="nav-tabContent">

							<div class="tab-pane fade show active" id="or_req" role="tabpanel" aria-labelledby="or_req-tab">
								<table class="table table-hover cell-border" id="or">
									<thead class="badge-info">
										<tr>
											<th scope="col">ID</th>
											<th>Added/Forwarded By</th>
											<th scope="col">SERIAL NUMBER</th>
											<th scope="col">DATE</th>
											<th scope="col">PAYOR</th>
											<th scope="col">ADDRESS</th>
											<th scope="col">FILE</th>
											<th>FILE GENERATED</th>
											<th scope="col">ACTIONS</th>
										</tr>
									</thead>
								</table>
							</div>

							<div class="tab-pane fade" id="or_rec" role="tabpanel" aria-labelledby="or_rec-tab">
								<table class="table table-hover cell-border" id="or_receipt">
									<thead class="badge-info">
										<tr>
											<th scope="col">ID</th>
											<th>Added/Forwarded By</th>
											<th scope="col">SERIAL NUMBER</th>
											<th scope="col">DATE</th>
											<th scope="col">PAYOR</th>
											<th scope="col">ADDRESS</th>
											<th scope="col">FILE</th>
											<th>FILE GENERATED</th>
											<th scope="col">ACTIONS</th>
										</tr>
									</thead>
								</table>
							</div>

						</div>

					</div>
				</div>
			</div>
		</div>
	</div>

<script type="text/javascript">
	$(function(){
		$('#or').DataTable({ 
			dom: 'Blfrtip',
        	buttons: 
        		['colvis','copy', 'csv', 'excel', 'pdf', 'print'],
			processing: true,
			serverSide: true,
			ajax: '{{ route('orderpayment/or') }}',
			columns: [
			{ data: 'id', name: 'id', visible:true },
			{ data: 'users.name', name: 'users.name', visible:false},
			{ data: 'or_serialno', name: 'or_serialno'},
			{data: 'or_datecreated_at', name: 'or_datecreated_at'},
			{ data: 'recdetails.rec_name', name: 'recdetails.rec_name'},
			{ data: 'recdetails.rec_address', name: 'recdetails.rec_address'},
			{ data: 'or_file', name: 'or_file'},
			{ data: 'or_file_generated', name: 'or_file_generated', visible:false},
			{ data: 'actions', name: 'actions', orderable: false, searchable: false}
			]
		});

		$('#or_receipt').DataTable({ 
			dom: 'Blfrtip',
        	buttons: 
        		['colvis','copy', 'csv', 'excel', 'pdf', 'print'],
			processing: true,
			serverSide: true,
			ajax: '{{ route('orderpayment/or_rec') }}',
			columns: [
			{ data: 'id', name: 'id', visible:true },
			{ data: 'users.name', name: 'users.name', visible:false},
			{ data: 'or_serialno', name: 'or_serialno'},
			{ data: 'or_datecreated_at', name: 'or_datecreated_at'},
			{ data: 'recdetails.rec_name', name: 'recdetails.rec_name'},
			{ data: 'recdetails.rec_address', name: 'recdetails.rec_address'},
			{ data: 'or_file', name: 'or_file'},
			{ data: 'or_file_generated', name: 'or_file_generated', visible:false},
			{ data: 'actions', name: 'actions', orderable: false, searchable: false}
			]
		});
	});
</script>
@endsection