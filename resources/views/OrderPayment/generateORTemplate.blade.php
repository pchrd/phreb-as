<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=Generator content="Microsoft Word 14 (filtered)">
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:10.0pt;
	margin-left:0in;
	line-height:115%;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";}
.MsoChpDefault
	{font-family:"Calibri","sans-serif";}
.MsoPapDefault
	{margin-bottom:10.0pt;
	line-height:115%;}
@page WordSection1
	{size:14.0in 8.5in;
	margin:49.5pt 67.5pt 1.0in .5in;}
div.WordSection1
	{page:WordSection1;}
-->
</style>

</head>

<body lang=EN-US>

<div class=WordSection1>
<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=1230
 style='width:922.5pt;margin-left:-8.1pt;border-collapse:collapse'>
 <tr style='height:11.2pt'>
  <td width=58 nowrap valign=bottom style='width:43.75pt;border-top:solid black 1.0pt;
  border-left:solid black 1.0pt;border-bottom:none;border-right:none;
  padding:0in 5.4pt 0in 5.4pt;height:11.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  </td>
  <td width=24 nowrap colspan=2 valign=bottom style='width:17.65pt;border:none;
  border-top:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:11.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.7pt;border:none;border-top:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:11.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  </td>
  <td width=82 nowrap valign=bottom style='width:61.75pt;border:none;
  border-top:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:11.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  </td>
  <td width=62 nowrap valign=bottom style='width:46.15pt;border:none;
  border-top:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:11.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;border:none;border-top:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:11.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  </td>
  <td width=50 nowrap valign=bottom style='width:37.2pt;border:none;border-top:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:11.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  </td>
  <td width=84 nowrap valign=bottom style='width:63.0pt;border-top:solid black 1.0pt;
  border-left:none;border-bottom:none;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:11.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.2pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.2pt'></td>
  <td width=59 nowrap valign=bottom style='width:43.9pt;border-top:solid black 1.0pt;
  border-left:solid black 1.0pt;border-bottom:none;border-right:none;
  padding:0in 5.4pt 0in 5.4pt;height:11.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  </td>
  <td width=24 nowrap colspan=2 valign=bottom style='width:17.85pt;border:none;
  border-top:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:11.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.85pt;border:none;
  border-top:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:11.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  </td>
  <td width=82 nowrap valign=bottom style='width:61.8pt;border:none;border-top:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:11.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  </td>
  <td width=62 nowrap valign=bottom style='width:46.2pt;border:none;border-top:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:11.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;border:none;border-top:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:11.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  </td>
  <td width=51 nowrap valign=bottom style='width:38.15pt;border:none;
  border-top:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:11.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  </td>
  <td width=82 nowrap valign=bottom style='width:61.45pt;border-top:solid black 1.0pt;
  border-left:none;border-bottom:none;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:11.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  </td>
  <td width=16 nowrap valign=bottom style='width:12.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.2pt'></td>
  <td width=59 nowrap valign=bottom style='width:43.9pt;border-top:solid black 1.0pt;
  border-left:solid black 1.0pt;border-bottom:none;border-right:none;
  padding:0in 5.4pt 0in 5.4pt;height:11.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  </td>
  <td width=24 nowrap colspan=2 valign=bottom style='width:17.85pt;border:none;
  border-top:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:11.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.85pt;border:none;
  border-top:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:11.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  </td>
  <td width=82 nowrap valign=bottom style='width:61.8pt;border:none;border-top:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:11.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  </td>
  <td width=62 nowrap valign=bottom style='width:46.2pt;border:none;border-top:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:11.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;border:none;border-top:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:11.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  </td>
  <td width=51 nowrap valign=bottom style='width:38.15pt;border:none;
  border-top:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:11.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  </td>
  <td width=72 nowrap valign=bottom style='width:53.95pt;border-top:solid black 1.0pt;
  border-left:none;border-bottom:none;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:11.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  </td>
 </tr>
 <tr style='height:18.25pt'>
  <td width=262 nowrap colspan=7 valign=bottom style='width:196.8pt;border:
  none;border-left:solid black 1.0pt;padding:0in 5.75pt 0in 5.75pt;height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:6.0pt;font-family:"Times New Roman","serif"'>Entity
  Name: <u>Phil Council for Health Research &amp; Dev't. </u></span></b></p>
  </td>
  <td width=50 nowrap valign=bottom style='width:37.2pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:6.0pt;font-family:"Times New Roman","serif"'>SerialNo:</span></b></p>
  </td>
  <td width=84 nowrap valign=bottom style='width:63.0pt;border:none;border-right:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><u><span style='font-size:6.0pt;font-family:"Times New Roman","serif"'>{{$serialno}}</span></u></b></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=263 nowrap colspan=7 valign=bottom style='width:197.4pt;border:
  none;border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:6.0pt;font-family:"Times New Roman","serif"'>Entity
  Name: Phil Council for Health Research &amp; Dev't. </span></b></p>
  </td>
  <td width=51 nowrap valign=bottom style='width:38.15pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:6.0pt;font-family:"Times New Roman","serif"'>SerialNo:</span></b></p>
  </td>
  <td width=82 nowrap valign=bottom style='width:61.45pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><u><span style='font-size:6.0pt;font-family:"Times New Roman","serif"'>{{$serialno}}</span></u></b></p>
  </td>
  <td width=16 nowrap valign=bottom style='width:12.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=263 nowrap colspan=7 valign=bottom style='width:197.4pt;border:
  none;border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:6.0pt;font-family:"Times New Roman","serif"'>Entity
  Name: <u>Phil Council for Health Research &amp; Dev't.</u></span></b></p>
  </td>
  <td width=51 nowrap valign=bottom style='width:38.15pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:6.0pt;font-family:"Times New Roman","serif"'>SerialNo:</span></b></p>
  </td>
  <td width=72 nowrap valign=bottom style='width:53.95pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><u><span style='font-size:6.0pt;font-family:"Times New Roman","serif"'>{{$serialno}}</span></u></b></p>
  </td>
 </tr>
 <tr style='height:18.25pt'>
  <td width=66 nowrap colspan=2 valign=bottom style='width:49.5pt;border-top:
  none;border-left:solid black 1.0pt;border-bottom:none;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:6.0pt;font-family:"Times New Roman","serif"'>Fund
  Cluster:</span></b></p>
  </td>
  <td width=16 nowrap valign=bottom style='width:11.9pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=162 nowrap colspan=3 valign=bottom style='width:121.6pt;padding:
  0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><u><span style='font-size:6.0pt;font-family:"Times New Roman","serif"'>General
  Fund</span></u></b></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=50 nowrap valign=bottom style='width:37.2pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:6.0pt;font-family:"Times New Roman","serif"'>Date
  :  </span></b></p>
  </td>
  <td width=84 nowrap valign=bottom style='width:63.0pt;border:none;border-right:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><u><span style='font-size:6.0pt;font-family:"Times New Roman","serif"'>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('F j, Y')}}</span></u></b></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=66 nowrap colspan=2 valign=bottom style='width:49.5pt;border-top:
  none;border-left:solid black 1.0pt;border-bottom:none;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:6.0pt;font-family:"Times New Roman","serif"'>Fund
  Cluster:</span></b></p>
  </td>
  <td width=16 nowrap valign=bottom style='width:12.25pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=162 nowrap colspan=3 valign=bottom style='width:121.85pt;
  padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><u><span style='font-size:6.0pt;font-family:"Times New Roman","serif"'>General
  Fund</span></u></b></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=51 nowrap valign=bottom style='width:38.15pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:6.0pt;font-family:"Times New Roman","serif"'>Date
  :  </span></b></p>
  </td>
  <td width=82 nowrap valign=bottom style='width:61.45pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><u><span style='font-size:6.0pt;font-family:"Times New Roman","serif"'>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('F j, Y')}}</span></u></b></p>
  </td>
  <td width=16 nowrap valign=bottom style='width:12.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=67 nowrap colspan=2 valign=bottom style='width:49.95pt;border-top:
  none;border-left:solid black 1.0pt;border-bottom:none;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal style='margin-top:0in;margin-right:-6.45pt;margin-bottom:
  0in;margin-left:0in;margin-bottom:.0001pt;line-height:normal'><b><span
  style='font-size:7.0pt;font-family:"Times New Roman","serif"'>Fund Cluster:</span></b></p>
  </td>
  <td width=16 nowrap valign=bottom style='width:11.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=162 nowrap colspan=3 valign=bottom style='width:121.85pt;
  padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><u><span style='font-size:6.0pt;font-family:"Times New Roman","serif"'>General
  Fund</span></u></b></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=51 nowrap valign=bottom style='width:38.15pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:6.0pt;font-family:"Times New Roman","serif"'>Date
  :  </span></b></p>
  </td>
  <td width=72 nowrap valign=bottom style='width:53.95pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><u><span style='font-size:6.0pt;font-family:"Times New Roman","serif"'>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('F j, Y')}}</span></u></b></p>
  </td>
 </tr>
 <tr style='height:7.55pt'>
  <td width=66 nowrap colspan=2 valign=bottom style='width:49.5pt;border:none;
  border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.55pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  </td>
  <td width=16 nowrap valign=bottom style='width:11.9pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.7pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.75pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=62 nowrap valign=bottom style='width:46.15pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=50 nowrap valign=bottom style='width:37.2pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=84 nowrap valign=bottom style='width:63.0pt;border:none;border-right:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.55pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=66 nowrap colspan=2 valign=bottom style='width:49.5pt;border:none;
  border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.55pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  </td>
  <td width=16 nowrap valign=bottom style='width:12.25pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.85pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=62 nowrap valign=bottom style='width:46.2pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=51 nowrap valign=bottom style='width:38.15pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.45pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.55pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  </td>
  <td width=16 nowrap valign=bottom style='width:12.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=67 nowrap colspan=2 valign=bottom style='width:49.95pt;border:none;
  border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.55pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  </td>
  <td width=16 nowrap valign=bottom style='width:11.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.85pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=62 nowrap valign=bottom style='width:46.2pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=51 nowrap valign=bottom style='width:38.15pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=72 nowrap valign=bottom style='width:53.95pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.55pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  </td>
 </tr>
 <tr style='height:7.55pt'>
  <td width=66 nowrap colspan=2 valign=bottom style='width:49.5pt;border:none;
  border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.55pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=16 nowrap valign=bottom style='width:11.9pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.7pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.75pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=62 nowrap valign=bottom style='width:46.15pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=50 nowrap valign=bottom style='width:37.2pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=84 nowrap valign=bottom style='width:63.0pt;border:none;border-right:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.55pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=66 nowrap colspan=2 valign=bottom style='width:49.5pt;border:none;
  border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.55pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=16 nowrap valign=bottom style='width:12.25pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.85pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=62 nowrap valign=bottom style='width:46.2pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=51 nowrap valign=bottom style='width:38.15pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.45pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.55pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=16 nowrap valign=bottom style='width:12.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=67 nowrap colspan=2 valign=bottom style='width:49.95pt;border:none;
  border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.55pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=16 nowrap valign=bottom style='width:11.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.85pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=62 nowrap valign=bottom style='width:46.2pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=51 nowrap valign=bottom style='width:38.15pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=72 nowrap valign=bottom style='width:53.95pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.55pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:18.25pt'>
  <td width=396 nowrap colspan=9 style='width:297.0pt;border-top:none;
  border-left:solid black 1.0pt;border-bottom:none;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><u><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>ORDER OF PAYMENT</span></u></b></p>
  </td>
  <td width=18 nowrap style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=18 nowrap style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=396 nowrap colspan=9 style='width:297.0pt;border-top:none;
  border-left:solid black 1.0pt;border-bottom:none;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><u><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>ORDER OF PAYMENT</span></u></b></p>
  </td>
  <td width=16 nowrap style='width:12.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=386 nowrap colspan=9 style='width:289.5pt;border-top:none;
  border-left:solid black 1.0pt;border-bottom:none;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><u><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>ORDER OF PAYMENT</span></u></b></p>
  </td>
 </tr>
 <tr style='height:18.25pt'>
  <td width=182 nowrap colspan=5 valign=bottom style='width:136.85pt;
  border:none;border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>The
  Collecting Officer</span></b></p>
  </td>
  <td width=62 nowrap valign=bottom style='width:46.15pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=50 nowrap valign=bottom style='width:37.2pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=84 nowrap valign=bottom style='width:63.0pt;border:none;border-right:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=183 nowrap colspan=5 valign=bottom style='width:137.4pt;border:
  none;border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>The
  Collecting Officer</span></b></p>
  </td>
  <td width=62 nowrap valign=bottom style='width:46.2pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=51 nowrap valign=bottom style='width:38.15pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.45pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;line-height:normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=16 nowrap valign=bottom style='width:12.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=183 nowrap colspan=5 valign=bottom style='width:137.4pt;border:
  none;border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>The
  Collecting Officer</span></b></p>
  </td>
  <td width=62 nowrap valign=bottom style='width:46.2pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=51 nowrap valign=bottom style='width:38.15pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=72 nowrap valign=bottom style='width:53.95pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:18.25pt'>
  <td width=182 nowrap colspan=5 valign=bottom style='width:136.85pt;
  border:none;border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>Cash/Treasury
  Unit</span></p>
  </td>
  <td width=62 nowrap valign=bottom style='width:46.15pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=50 nowrap valign=bottom style='width:37.2pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=84 nowrap valign=bottom style='width:63.0pt;border:none;border-right:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=183 nowrap colspan=5 valign=bottom style='width:137.4pt;border:
  none;border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>Cash/Treasury
  Unit</span></p>
  </td>
  <td width=62 nowrap valign=bottom style='width:46.2pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=51 nowrap valign=bottom style='width:38.15pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.45pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=16 nowrap valign=bottom style='width:12.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=183 nowrap colspan=5 valign=bottom style='width:137.4pt;border:
  none;border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>Cash/Treasury
  Unit</span></p>
  </td>
  <td width=62 nowrap valign=bottom style='width:46.2pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=51 nowrap valign=bottom style='width:38.15pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=72 nowrap valign=bottom style='width:53.95pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:7.55pt'>
  <td width=58 nowrap valign=bottom style='width:43.75pt;border:none;
  border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.55pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=24 nowrap colspan=2 valign=bottom style='width:17.65pt;padding:
  0in 5.4pt 0in 5.4pt;height:7.55pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.7pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.75pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=62 nowrap valign=bottom style='width:46.15pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=50 nowrap valign=bottom style='width:37.2pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=84 nowrap valign=bottom style='width:63.0pt;border:none;border-right:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.55pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=59 nowrap valign=bottom style='width:43.9pt;border:none;border-left:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.55pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=24 nowrap colspan=2 valign=bottom style='width:17.85pt;padding:
  0in 5.4pt 0in 5.4pt;height:7.55pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.85pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=62 nowrap valign=bottom style='width:46.2pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=51 nowrap valign=bottom style='width:38.15pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.45pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.55pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=16 nowrap valign=bottom style='width:12.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=59 nowrap valign=bottom style='width:43.9pt;border:none;border-left:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.55pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=24 nowrap colspan=2 valign=bottom style='width:17.85pt;padding:
  0in 5.4pt 0in 5.4pt;height:7.55pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.85pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=62 nowrap valign=bottom style='width:46.2pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=51 nowrap valign=bottom style='width:38.15pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=72 nowrap valign=bottom style='width:53.95pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.55pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:7.55pt'>
  <td width=58 nowrap valign=bottom style='width:43.75pt;border:none;
  border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.55pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=24 nowrap colspan=2 valign=bottom style='width:17.65pt;padding:
  0in 5.4pt 0in 5.4pt;height:7.55pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.7pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.75pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=62 nowrap valign=bottom style='width:46.15pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=50 nowrap valign=bottom style='width:37.2pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=84 nowrap valign=bottom style='width:63.0pt;border:none;border-right:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.55pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=59 nowrap valign=bottom style='width:43.9pt;border:none;border-left:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.55pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=24 nowrap colspan=2 valign=bottom style='width:17.85pt;padding:
  0in 5.4pt 0in 5.4pt;height:7.55pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.85pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=62 nowrap valign=bottom style='width:46.2pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=51 nowrap valign=bottom style='width:38.15pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.45pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.55pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=16 nowrap valign=bottom style='width:12.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=59 nowrap valign=bottom style='width:43.9pt;border:none;border-left:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.55pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=24 nowrap colspan=2 valign=bottom style='width:17.85pt;padding:
  0in 5.4pt 0in 5.4pt;height:7.55pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.85pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=62 nowrap valign=bottom style='width:46.2pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=51 nowrap valign=bottom style='width:38.15pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=72 nowrap valign=bottom style='width:53.95pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.55pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:18.25pt'>
  <td width=182 nowrap colspan=5 valign=bottom style='width:136.85pt;
  border:none;border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>      Please issue Official Receipt in
  favor of  </span></p>
  </td>
  <td width=214 colspan=4 valign=bottom style='width:160.15pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>{{$recdetails->rec_name}}.</span></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=183 nowrap colspan=5 valign=bottom style='width:137.4pt;border:
  none;border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>      Please issue Official Receipt in
  favor of  </span></p>
  </td>
  <td width=213 colspan=4 valign=bottom style='width:159.6pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>{{$recdetails->rec_name}}.</span></p>
  </td>
  <td width=16 nowrap valign=bottom style='width:12.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=183 nowrap colspan=5 valign=bottom style='width:137.4pt;border:
  none;border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>      Please issue Official Receipt in
  favor of  </span></p>
  </td>
  <td width=203 colspan=4 valign=bottom style='width:152.1pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>{{$recdetails->rec_name}}</span></p>
  </td>
 </tr>
 <tr style='height:8.6pt'>
  <td width=58 nowrap valign=bottom style='width:43.75pt;border:none;
  border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:8.6pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=24 nowrap colspan=2 valign=bottom style='width:17.65pt;padding:
  0in 5.4pt 0in 5.4pt;height:8.6pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.7pt;padding:0in 5.4pt 0in 5.4pt;
  height:8.6pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.75pt;padding:0in 5.4pt 0in 5.4pt;
  height:8.6pt'></td>
  <td width=214 nowrap colspan=4 valign=top style='width:160.15pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:8.6pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>(Name of Payor)</span></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:8.6pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:8.6pt'></td>
  <td width=59 nowrap valign=bottom style='width:43.9pt;border:none;border-left:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:8.6pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=24 nowrap colspan=2 valign=bottom style='width:17.85pt;padding:
  0in 5.4pt 0in 5.4pt;height:8.6pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.85pt;padding:0in 5.4pt 0in 5.4pt;
  height:8.6pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:8.6pt'></td>
  <td width=213 nowrap colspan=4 valign=top style='width:159.6pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:8.6pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>(Name of Payor)</span></p>
  </td>
  <td width=16 nowrap valign=bottom style='width:12.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:8.6pt'></td>
  <td width=59 nowrap valign=bottom style='width:43.9pt;border:none;border-left:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:8.6pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=24 nowrap colspan=2 valign=bottom style='width:17.85pt;padding:
  0in 5.4pt 0in 5.4pt;height:8.6pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.85pt;padding:0in 5.4pt 0in 5.4pt;
  height:8.6pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:8.6pt'></td>
  <td width=203 nowrap colspan=4 valign=top style='width:152.1pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:8.6pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>(Name of Payor)</span></p>
  </td>
 </tr>
 <tr style='height:17.15pt'>
  <td width=396 nowrap colspan=9 valign=bottom style='width:297.0pt;border:
  solid black 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt;height:17.15pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>{{$recdetails->rec_address}}</span></p>
  </td>
  <td width=18 nowrap style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:17.15pt'></td>
  <td width=18 nowrap style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:17.15pt'></td>
  <td width=396 nowrap colspan=9 valign=bottom style='width:297.0pt;border:
  solid black 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt;height:17.15pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>{{$recdetails->rec_address}}</span></p>
  </td>
  <td width=16 nowrap style='width:12.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:17.15pt'></td>
  <td width=386 nowrap colspan=9 valign=bottom style='width:289.5pt;border:
  solid black 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt;height:17.15pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>{{$recdetails->rec_address}}</span></p>
  </td>
 </tr>
 <tr style='height:10.15pt'>
  <td width=396 nowrap colspan=9 valign=bottom style='width:297.0pt;border-top:
  none;border-left:solid black 1.0pt;border-bottom:none;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:10.15pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>(Address/Office of Payor)</span></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:10.15pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:10.15pt'></td>
  <td width=396 nowrap colspan=9 valign=bottom style='width:297.0pt;border-top:
  none;border-left:solid black 1.0pt;border-bottom:none;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:10.15pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>(Address/Office of Payor)</span></p>
  </td>
  <td width=16 nowrap valign=bottom style='width:12.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:10.15pt'></td>
  <td width=386 nowrap colspan=9 valign=bottom style='width:289.5pt;border-top:
  none;border-left:solid black 1.0pt;border-bottom:none;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:10.15pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>(Address/Office of Payor)</span></p>
  </td>
 </tr>
 <tr style='height:18.25pt'>
  <td width=82 nowrap colspan=3 valign=bottom style='width:61.4pt;border:none;
  border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>in
  the amount of </span></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.7pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=212 nowrap colspan=4 valign=bottom style='width:158.9pt;border:
  none;border-bottom:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>Three Thousand Pesos Only</span></p>
  </td>
  <td width=84 nowrap valign=bottom style='width:63.0pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>(P3000)</span></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=82 nowrap colspan=3 valign=bottom style='width:61.75pt;border:none;
  border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>in
  the amount of </span></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.85pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=213 nowrap colspan=4 valign=bottom style='width:159.95pt;
  border:none;border-bottom:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>Three Thousand Pesos Only</span></p>
  </td>
  <td width=82 nowrap valign=bottom style='width:61.45pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>(P3000)</span></p>
  </td>
  <td width=16 nowrap valign=bottom style='width:12.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=82 nowrap colspan=3 valign=bottom style='width:61.75pt;border:none;
  border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>in
  the amount of </span></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.85pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=213 nowrap colspan=4 valign=bottom style='width:159.95pt;
  border:none;border-bottom:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>Three Thousand Pesos Only</span></p>
  </td>
  <td width=72 nowrap valign=bottom style='width:53.95pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>(P3000)</span></p>
  </td>
 </tr>
 <tr style='height:18.25pt'>
  <td width=100 nowrap colspan=4 valign=bottom style='width:75.1pt;border:none;
  border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>for
  payment of  </span></p>
  </td>
  <td width=296 nowrap colspan=5 valign=bottom style='width:221.9pt;border-top:
  none;border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>PHREB Accreditation Processing Fee</span></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=101 nowrap colspan=4 valign=bottom style='width:1.05in;border:none;
  border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>for
  payment of  </span></p>
  </td>
  <td width=295 nowrap colspan=5 valign=bottom style='width:221.4pt;border-top:
  none;border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>PHREB Accreditation Processing Fee</span></p>
  </td>
  <td width=16 nowrap valign=bottom style='width:12.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=101 nowrap colspan=4 valign=bottom style='width:1.05in;border:none;
  border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>for
  payment of  </span></p>
  </td>
  <td width=285 nowrap colspan=5 valign=bottom style='width:213.9pt;border-top:
  none;border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>PHREB Accreditation Processing Fee</span></p>
  </td>
 </tr>
 <tr style='height:11.2pt'>
  <td width=396 nowrap colspan=9 valign=bottom style='width:297.0pt;border:
  solid black 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt;height:11.2pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.2pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.2pt'></td>
  <td width=396 nowrap colspan=9 valign=bottom style='width:297.0pt;border:
  solid black 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt;height:11.2pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=16 nowrap valign=bottom style='width:12.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.2pt'></td>
  <td width=386 nowrap colspan=9 valign=bottom style='width:289.5pt;border:
  solid black 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt;height:11.2pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:11.2pt'>
  <td width=396 nowrap colspan=9 valign=bottom style='width:297.0pt;border-top:
  none;border-left:solid black 1.0pt;border-bottom:none;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:11.2pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>(Purpose)</span></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.2pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.2pt'></td>
  <td width=396 nowrap colspan=9 valign=bottom style='width:297.0pt;border-top:
  none;border-left:solid black 1.0pt;border-bottom:none;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:11.2pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>(Purpose)</span></p>
  </td>
  <td width=16 nowrap valign=bottom style='width:12.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.2pt'></td>
  <td width=386 nowrap colspan=9 valign=bottom style='width:289.5pt;border-top:
  none;border-left:solid black 1.0pt;border-bottom:none;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:11.2pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>(Purpose)</span></p>
  </td>
 </tr>
 <tr style='height:18.25pt'>
  <td width=396 nowrap colspan=9 valign=bottom style='width:297.0pt;border-top:
  none;border-left:solid black 1.0pt;border-bottom:none;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>per
  Bill No. ________________ dated __________________.</span></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=396 nowrap colspan=9 valign=bottom style='width:297.0pt;border-top:
  none;border-left:solid black 1.0pt;border-bottom:none;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>per
  Bill No. ________________ dated __________________.</span></p>
  </td>
  <td width=16 nowrap valign=bottom style='width:12.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=386 nowrap colspan=9 valign=bottom style='width:289.5pt;border-top:
  none;border-left:solid black 1.0pt;border-bottom:none;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>per
  Bill No. ________________ dated __________________.</span></p>
  </td>
 </tr>
 <tr style='height:7.55pt'>
  <td width=58 nowrap valign=bottom style='width:43.75pt;border:none;
  border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.55pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=24 nowrap colspan=2 valign=bottom style='width:17.65pt;padding:
  0in 5.4pt 0in 5.4pt;height:7.55pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.7pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.75pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=62 nowrap valign=bottom style='width:46.15pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=50 nowrap valign=bottom style='width:37.2pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=84 nowrap valign=bottom style='width:63.0pt;border:none;border-right:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.55pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=59 nowrap valign=bottom style='width:43.9pt;border:none;border-left:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.55pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=24 nowrap colspan=2 valign=bottom style='width:17.85pt;padding:
  0in 5.4pt 0in 5.4pt;height:7.55pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.85pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=62 nowrap valign=bottom style='width:46.2pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=51 nowrap valign=bottom style='width:38.15pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.45pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.55pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=16 nowrap valign=bottom style='width:12.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=59 nowrap valign=bottom style='width:43.9pt;border:none;border-left:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.55pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=24 nowrap colspan=2 valign=bottom style='width:17.85pt;padding:
  0in 5.4pt 0in 5.4pt;height:7.55pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.85pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=62 nowrap valign=bottom style='width:46.2pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=51 nowrap valign=bottom style='width:38.15pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=72 nowrap valign=bottom style='width:53.95pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.55pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:7.55pt'>
  <td width=58 nowrap valign=bottom style='width:43.75pt;border:none;
  border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.55pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=24 nowrap colspan=2 valign=bottom style='width:17.65pt;padding:
  0in 5.4pt 0in 5.4pt;height:7.55pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.7pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.75pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=62 nowrap valign=bottom style='width:46.15pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=50 nowrap valign=bottom style='width:37.2pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=84 nowrap valign=bottom style='width:63.0pt;border:none;border-right:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.55pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=59 nowrap valign=bottom style='width:43.9pt;border:none;border-left:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.55pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=24 nowrap colspan=2 valign=bottom style='width:17.85pt;padding:
  0in 5.4pt 0in 5.4pt;height:7.55pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.85pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=62 nowrap valign=bottom style='width:46.2pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=51 nowrap valign=bottom style='width:38.15pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.45pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.55pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=16 nowrap valign=bottom style='width:12.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=59 nowrap valign=bottom style='width:43.9pt;border:none;border-left:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.55pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=24 nowrap colspan=2 valign=bottom style='width:17.85pt;padding:
  0in 5.4pt 0in 5.4pt;height:7.55pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.85pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=62 nowrap valign=bottom style='width:46.2pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=51 nowrap valign=bottom style='width:38.15pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.55pt'></td>
  <td width=72 nowrap valign=bottom style='width:53.95pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.55pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:18.25pt'>
  <td width=396 nowrap colspan=9 valign=bottom style='width:297.0pt;border-top:
  none;border-left:solid black 1.0pt;border-bottom:none;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>Please
  deposit the collections under Bank Account/s:</span></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=396 nowrap colspan=9 valign=bottom style='width:297.0pt;border-top:
  none;border-left:solid black 1.0pt;border-bottom:none;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>Please
  deposit the collections under Bank Account/s:</span></p>
  </td>
  <td width=16 nowrap valign=bottom style='width:12.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=386 nowrap colspan=9 valign=bottom style='width:289.5pt;border-top:
  none;border-left:solid black 1.0pt;border-bottom:none;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>Please
  deposit the collections under Bank Account/s:</span></p>
  </td>
 </tr>
 <tr style='height:11.2pt'>
  <td width=58 nowrap valign=bottom style='width:43.75pt;border:none;
  border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:11.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=24 nowrap colspan=2 valign=bottom style='width:17.65pt;padding:
  0in 5.4pt 0in 5.4pt;height:11.2pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.7pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.2pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.75pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.2pt'></td>
  <td width=62 nowrap valign=bottom style='width:46.15pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.2pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.2pt'></td>
  <td width=50 nowrap valign=bottom style='width:37.2pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.2pt'></td>
  <td width=84 nowrap valign=bottom style='width:63.0pt;border:none;border-right:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:11.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.2pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.2pt'></td>
  <td width=59 nowrap valign=bottom style='width:43.9pt;border:none;border-left:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:11.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=24 nowrap colspan=2 valign=bottom style='width:17.85pt;padding:
  0in 5.4pt 0in 5.4pt;height:11.2pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.85pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.2pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.2pt'></td>
  <td width=62 nowrap valign=bottom style='width:46.2pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.2pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.2pt'></td>
  <td width=51 nowrap valign=bottom style='width:38.15pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.2pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.45pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:11.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=16 nowrap valign=bottom style='width:12.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.2pt'></td>
  <td width=59 nowrap valign=bottom style='width:43.9pt;border:none;border-left:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:11.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=24 nowrap colspan=2 valign=bottom style='width:17.85pt;padding:
  0in 5.4pt 0in 5.4pt;height:11.2pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.85pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.2pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.2pt'></td>
  <td width=62 nowrap valign=bottom style='width:46.2pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.2pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.2pt'></td>
  <td width=51 nowrap valign=bottom style='width:38.15pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.2pt'></td>
  <td width=72 nowrap valign=bottom style='width:53.95pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:11.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:18.75pt'>
  <td width=82 nowrap colspan=3 valign=bottom style='width:61.4pt;border:none;
  border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:18.75pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><u><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>No.</span></u></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.7pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.75pt'></td>
  <td width=144 nowrap colspan=2 valign=bottom style='width:107.9pt;padding:
  0in 5.4pt 0in 5.4pt;height:18.75pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><u><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>Name of Bank</span></u></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.75pt'></td>
  <td width=134 nowrap colspan=2 valign=bottom style='width:100.2pt;border:
  none;border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:18.75pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><u><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>Amount</span></u></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.75pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.75pt'></td>
  <td width=82 nowrap colspan=3 valign=bottom style='width:61.75pt;border:none;
  border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:18.75pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><u><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>No.</span></u></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.85pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.75pt'></td>
  <td width=144 nowrap colspan=2 valign=bottom style='width:1.5in;padding:0in 5.4pt 0in 5.4pt;
  height:18.75pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><u><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>Name of Bank</span></u></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.75pt'></td>
  <td width=133 nowrap colspan=2 valign=bottom style='width:99.6pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:18.75pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><u><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>Amount</span></u></p>
  </td>
  <td width=16 nowrap valign=bottom style='width:12.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.75pt'></td>
  <td width=82 nowrap colspan=3 valign=bottom style='width:61.75pt;border:none;
  border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:18.75pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><u><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>No.</span></u></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.85pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.75pt'></td>
  <td width=144 nowrap colspan=2 valign=bottom style='width:1.5in;padding:0in 5.4pt 0in 5.4pt;
  height:18.75pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><u><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>Name of Bank</span></u></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.75pt'></td>
  <td width=123 nowrap colspan=2 valign=bottom style='width:92.1pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:18.75pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><u><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>Amount</span></u></p>
  </td>
 </tr>
 <tr style='height:18.75pt'>
  <td width=82 nowrap colspan=3 valign=bottom style='width:61.4pt;border-top:
  none;border-left:solid black 1.0pt;border-bottom:solid black 1.0pt;
  border-right:none;padding:0in 5.4pt 0in 5.4pt;height:18.75pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.7pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.75pt'></td>
  <td width=144 nowrap colspan=2 valign=bottom style='width:107.9pt;border:
  none;border-bottom:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:18.75pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.75pt'></td>
  <td width=134 nowrap colspan=2 valign=bottom style='width:100.2pt;border-top:
  none;border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:18.75pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><s><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>P</span></s></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.75pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.75pt'></td>
  <td width=82 nowrap colspan=3 valign=bottom style='width:61.75pt;border-top:
  none;border-left:solid black 1.0pt;border-bottom:solid black 1.0pt;
  border-right:none;padding:0in 5.4pt 0in 5.4pt;height:18.75pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.85pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.75pt'></td>
  <td width=144 nowrap colspan=2 valign=bottom style='width:1.5in;border:none;
  border-bottom:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:18.75pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.75pt'></td>
  <td width=133 nowrap colspan=2 valign=bottom style='width:99.6pt;border-top:
  none;border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:18.75pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><s><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>P</span></s></p>
  </td>
  <td width=16 nowrap valign=bottom style='width:12.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.75pt'></td>
  <td width=82 nowrap colspan=3 valign=bottom style='width:61.75pt;border-top:
  none;border-left:solid black 1.0pt;border-bottom:solid black 1.0pt;
  border-right:none;padding:0in 5.4pt 0in 5.4pt;height:18.75pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.85pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.75pt'></td>
  <td width=144 nowrap colspan=2 valign=bottom style='width:1.5in;border:none;
  border-bottom:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:18.75pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.75pt'></td>
  <td width=123 nowrap colspan=2 valign=bottom style='width:92.1pt;border-top:
  none;border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:18.75pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><s><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>P</span></s></p>
  </td>
 </tr>
 <tr style='height:18.75pt'>
  <td width=58 nowrap valign=bottom style='width:43.75pt;border:none;
  border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:18.75pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>Total</span></b></p>
  </td>
  <td width=24 nowrap colspan=2 valign=bottom style='width:17.65pt;padding:
  0in 5.4pt 0in 5.4pt;height:18.75pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.7pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.75pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.75pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.75pt'></td>
  <td width=62 nowrap valign=bottom style='width:46.15pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.75pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.75pt'></td>
  <td width=134 nowrap colspan=2 valign=bottom style='width:100.2pt;border-top:
  none;border-left:none;border-bottom:double black 2.25pt;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:18.75pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><s><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>P</span></s></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.75pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.75pt'></td>
  <td width=59 nowrap valign=bottom style='width:43.9pt;border:none;border-left:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:18.75pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>Total</span></b></p>
  </td>
  <td width=24 nowrap colspan=2 valign=bottom style='width:17.85pt;padding:
  0in 5.4pt 0in 5.4pt;height:18.75pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.85pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.75pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.75pt'></td>
  <td width=62 nowrap valign=bottom style='width:46.2pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.75pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.75pt'></td>
  <td width=133 nowrap colspan=2 valign=bottom style='width:99.6pt;border-top:
  none;border-left:none;border-bottom:double black 2.25pt;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:18.75pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><s><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>P</span></s></p>
  </td>
  <td width=16 nowrap valign=bottom style='width:12.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.75pt'></td>
  <td width=59 nowrap valign=bottom style='width:43.9pt;border:none;border-left:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:18.75pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>Total</span></b></p>
  </td>
  <td width=24 nowrap colspan=2 valign=bottom style='width:17.85pt;padding:
  0in 5.4pt 0in 5.4pt;height:18.75pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.85pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.75pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.75pt'></td>
  <td width=62 nowrap valign=bottom style='width:46.2pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.75pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.75pt'></td>
  <td width=123 nowrap colspan=2 valign=bottom style='width:92.1pt;border-top:
  none;border-left:none;border-bottom:double black 2.25pt;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:18.75pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><s><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>P</span></s></p>
  </td>
 </tr>
 <tr style='height:11.75pt'>
  <td width=58 nowrap valign=bottom style='width:43.75pt;border:none;
  border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:11.75pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=24 nowrap colspan=2 valign=bottom style='width:17.65pt;padding:
  0in 5.4pt 0in 5.4pt;height:11.75pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.7pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.75pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.75pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.75pt'></td>
  <td width=62 nowrap valign=bottom style='width:46.15pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.75pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.75pt'></td>
  <td width=50 nowrap valign=bottom style='width:37.2pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.75pt'></td>
  <td width=84 nowrap valign=bottom style='width:63.0pt;border:none;border-right:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:11.75pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.75pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.75pt'></td>
  <td width=59 nowrap valign=bottom style='width:43.9pt;border:none;border-left:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:11.75pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=24 nowrap colspan=2 valign=bottom style='width:17.85pt;padding:
  0in 5.4pt 0in 5.4pt;height:11.75pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.85pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.75pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.75pt'></td>
  <td width=62 nowrap valign=bottom style='width:46.2pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.75pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.75pt'></td>
  <td width=51 nowrap valign=bottom style='width:38.15pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.75pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.45pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:11.75pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=16 nowrap valign=bottom style='width:12.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.75pt'></td>
  <td width=59 nowrap valign=bottom style='width:43.9pt;border:none;border-left:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:11.75pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=24 nowrap colspan=2 valign=bottom style='width:17.85pt;padding:
  0in 5.4pt 0in 5.4pt;height:11.75pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.85pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.75pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.75pt'></td>
  <td width=62 nowrap valign=bottom style='width:46.2pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.75pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.75pt'></td>
  <td width=51 nowrap valign=bottom style='width:38.15pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.75pt'></td>
  <td width=72 nowrap valign=bottom style='width:53.95pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:11.75pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:19.3pt'>
  <td width=58 nowrap valign=bottom style='width:43.75pt;border:none;
  border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:19.3pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=24 nowrap colspan=2 valign=bottom style='width:17.65pt;padding:
  0in 5.4pt 0in 5.4pt;height:19.3pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.7pt;padding:0in 5.4pt 0in 5.4pt;
  height:19.3pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.75pt;padding:0in 5.4pt 0in 5.4pt;
  height:19.3pt'></td>
  <td width=214 nowrap colspan=4 valign=bottom style='width:160.15pt;
  border-top:none;border-left:none;border-bottom:solid black 1.0pt;border-right:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:19.3pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>JESSAMYN M. BUCLATIN </span></b></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:19.3pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:19.3pt'></td>
  <td width=59 nowrap valign=bottom style='width:43.9pt;border:none;border-left:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:19.3pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=24 nowrap colspan=2 valign=bottom style='width:17.85pt;padding:
  0in 5.4pt 0in 5.4pt;height:19.3pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.85pt;padding:0in 5.4pt 0in 5.4pt;
  height:19.3pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:19.3pt'></td>
  <td width=213 nowrap colspan=4 valign=bottom style='width:159.6pt;border-top:
  none;border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:19.3pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>JESSAMYN M. BUCLATIN </span></b></p>
  </td>
  <td width=16 nowrap valign=bottom style='width:12.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:19.3pt'></td>
  <td width=59 nowrap valign=bottom style='width:43.9pt;border:none;border-left:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:19.3pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=24 nowrap colspan=2 valign=bottom style='width:17.85pt;padding:
  0in 5.4pt 0in 5.4pt;height:19.3pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.85pt;padding:0in 5.4pt 0in 5.4pt;
  height:19.3pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:19.3pt'></td>
  <td width=203 nowrap colspan=4 valign=bottom style='width:152.1pt;border-top:
  none;border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:19.3pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>JESSAMYN M. BUCLATIN </span></b></p>
  </td>
 </tr>
 <tr style='height:19.8pt'>
  <td width=58 nowrap valign=bottom style='width:43.75pt;border:none;
  border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:19.8pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=24 nowrap colspan=2 valign=bottom style='width:17.65pt;padding:
  0in 5.4pt 0in 5.4pt;height:19.8pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.7pt;padding:0in 5.4pt 0in 5.4pt;
  height:19.8pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.75pt;padding:0in 5.4pt 0in 5.4pt;
  height:19.8pt'></td>
  <td width=214 colspan=4 valign=bottom style='width:160.15pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:19.8pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>Signature over Printed Name Head of 
  Accounting Division/Unit/Authorized Official</span></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:19.8pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:19.8pt'></td>
  <td width=59 nowrap valign=bottom style='width:43.9pt;border:none;border-left:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:19.8pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=24 nowrap colspan=2 valign=bottom style='width:17.85pt;padding:
  0in 5.4pt 0in 5.4pt;height:19.8pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.85pt;padding:0in 5.4pt 0in 5.4pt;
  height:19.8pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:19.8pt'></td>
  <td width=213 colspan=4 valign=bottom style='width:159.6pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:19.8pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>Signature over Printed Name Head of 
  Accounting Division/Unit/Authorized Official</span></p>
  </td>
  <td width=16 nowrap valign=bottom style='width:12.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:19.8pt'></td>
  <td width=59 nowrap valign=bottom style='width:43.9pt;border:none;border-left:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:19.8pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=24 nowrap colspan=2 valign=bottom style='width:17.85pt;padding:
  0in 5.4pt 0in 5.4pt;height:19.8pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.85pt;padding:0in 5.4pt 0in 5.4pt;
  height:19.8pt'></td>
  <td width=82 nowrap valign=bottom style='width:61.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:19.8pt'></td>
  <td width=203 colspan=4 valign=bottom style='width:152.1pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:19.8pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>Signature over Printed Name Head of 
  Accounting Division/Unit/Authorized Official</span></p>
  </td>
 </tr>
 <tr style='height:6.4pt'>
  <td width=58 nowrap valign=bottom style='width:43.75pt;border-top:none;
  border-left:solid black 1.0pt;border-bottom:solid black 1.0pt;border-right:
  none;padding:0in 5.4pt 0in 5.4pt;height:6.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=24 nowrap colspan=2 valign=bottom style='width:17.65pt;border:none;
  border-bottom:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:6.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.7pt;border:none;border-bottom:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:6.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=144 nowrap colspan=2 valign=bottom style='width:107.9pt;border:
  none;border-bottom:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:6.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><sup><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>&nbsp;</span></sup></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;border:none;border-bottom:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:6.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><sup><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>&nbsp;</span></sup></p>
  </td>
  <td width=50 nowrap valign=bottom style='width:37.2pt;border:none;border-bottom:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:6.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=84 nowrap valign=bottom style='width:63.0pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:6.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:6.4pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:6.4pt'></td>
  <td width=59 nowrap valign=bottom style='width:43.9pt;border-top:none;
  border-left:solid black 1.0pt;border-bottom:solid black 1.0pt;border-right:
  none;padding:0in 5.4pt 0in 5.4pt;height:6.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=24 nowrap colspan=2 valign=bottom style='width:17.85pt;border:none;
  border-bottom:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:6.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.85pt;border:none;
  border-bottom:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:6.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=144 nowrap colspan=2 valign=bottom style='width:1.5in;border:none;
  border-bottom:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:6.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><sup><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>&nbsp;</span></sup></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;border:none;border-bottom:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:6.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><sup><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>&nbsp;</span></sup></p>
  </td>
  <td width=51 nowrap valign=bottom style='width:38.15pt;border:none;
  border-bottom:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:6.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=82 nowrap valign=bottom style='width:61.45pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:6.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=16 nowrap valign=bottom style='width:12.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:6.4pt'></td>
  <td width=59 nowrap valign=bottom style='width:43.9pt;border-top:none;
  border-left:solid black 1.0pt;border-bottom:solid black 1.0pt;border-right:
  none;padding:0in 5.4pt 0in 5.4pt;height:6.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=24 nowrap colspan=2 valign=bottom style='width:17.85pt;border:none;
  border-bottom:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:6.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.85pt;border:none;
  border-bottom:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:6.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=144 nowrap colspan=2 valign=bottom style='width:1.5in;border:none;
  border-bottom:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:6.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><sup><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>&nbsp;</span></sup></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.8pt;border:none;border-bottom:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:6.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><sup><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>&nbsp;</span></sup></p>
  </td>
  <td width=51 nowrap valign=bottom style='width:38.15pt;border:none;
  border-bottom:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:6.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
  <td width=72 nowrap valign=bottom style='width:53.95pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:6.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:11.75pt'>
  <td width=396 nowrap colspan=9 valign=bottom style='width:297.0pt;border:
  solid black 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt;height:11.75pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>ACCOUNTING COPY</span></b></p>
  </td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.75pt'></td>
  <td width=18 nowrap valign=bottom style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.75pt'></td>
  <td width=396 nowrap colspan=9 valign=bottom style='width:297.0pt;border:
  solid black 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt;height:11.75pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>CASHIER'S COPY</span></b></p>
  </td>
  <td width=16 nowrap valign=bottom style='width:12.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.75pt'></td>
  <td width=386 nowrap colspan=9 valign=bottom style='width:289.5pt;border:
  solid black 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt;height:11.75pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:7.0pt;
  font-family:"Times New Roman","serif"'>PAYEE'S COPY</span></b></p>
  </td>
 </tr>

 <tr height=0>
  <td width=58 style='border:none'></td>
  <td width=8 style='border:none'></td>
  <td width=16 style='border:none'></td>
  <td width=18 style='border:none'></td>
  <td width=82 style='border:none'></td>
  <td width=62 style='border:none'></td>
  <td width=18 style='border:none'></td>
  <td width=50 style='border:none'></td>
  <td width=84 style='border:none'></td>
  <td width=18 style='border:none'></td>
  <td width=18 style='border:none'></td>
  <td width=59 style='border:none'></td>
  <td width=7 style='border:none'></td>
  <td width=16 style='border:none'></td>
  <td width=18 style='border:none'></td>
  <td width=82 style='border:none'></td>
  <td width=62 style='border:none'></td>
  <td width=18 style='border:none'></td>
  <td width=51 style='border:none'></td>
  <td width=82 style='border:none'></td>
  <td width=16 style='border:none'></td>
  <td width=59 style='border:none'></td>
  <td width=8 style='border:none'></td>
  <td width=16 style='border:none'></td>
  <td width=18 style='border:none'></td>
  <td width=82 style='border:none'></td>
  <td width=62 style='border:none'></td>
  <td width=18 style='border:none'></td>
  <td width=51 style='border:none'></td>
  <td width=72 style='border:none'></td>
 </tr>
</table>

<p class=MsoNormal style='margin-top:0in;margin-right:.25in;margin-bottom:10.0pt;
margin-left:4.5pt'>&nbsp;</p>

</div>

</body>

</html>
