<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=Generator content="Microsoft Word 15 (filtered)">
<style>
<!--
 /* Font Definitions */
 @font-face
  {font-family:Wingdings;
  panose-1:5 0 0 0 0 0 0 0 0 0;}
@font-face
  {font-family:"Cambria Math";
  panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
  {font-family:Calibri;
  panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
  {font-family:"Segoe UI";
  panose-1:2 11 5 2 4 2 4 2 2 3;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
  {margin-top:0cm;
  margin-right:0cm;
  margin-bottom:10.0pt;
  margin-left:0cm;
  line-height:115%;
  font-size:11.0pt;
  font-family:"Calibri",sans-serif;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
  {mso-style-link:"Balloon Text Char";
  margin:0cm;
  margin-bottom:.0001pt;
  font-size:9.0pt;
  font-family:"Segoe UI",sans-serif;}
p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
  {margin-top:0cm;
  margin-right:0cm;
  margin-bottom:10.0pt;
  margin-left:36.0pt;
  line-height:115%;
  font-size:11.0pt;
  font-family:"Calibri",sans-serif;}
p.MsoListParagraphCxSpFirst, li.MsoListParagraphCxSpFirst, div.MsoListParagraphCxSpFirst
  {margin-top:0cm;
  margin-right:0cm;
  margin-bottom:0cm;
  margin-left:36.0pt;
  margin-bottom:.0001pt;
  line-height:115%;
  font-size:11.0pt;
  font-family:"Calibri",sans-serif;}
p.MsoListParagraphCxSpMiddle, li.MsoListParagraphCxSpMiddle, div.MsoListParagraphCxSpMiddle
  {margin-top:0cm;
  margin-right:0cm;
  margin-bottom:0cm;
  margin-left:36.0pt;
  margin-bottom:.0001pt;
  line-height:115%;
  font-size:11.0pt;
  font-family:"Calibri",sans-serif;}
p.MsoListParagraphCxSpLast, li.MsoListParagraphCxSpLast, div.MsoListParagraphCxSpLast
  {margin-top:0cm;
  margin-right:0cm;
  margin-bottom:10.0pt;
  margin-left:36.0pt;
  line-height:115%;
  font-size:11.0pt;
  font-family:"Calibri",sans-serif;}
span.BalloonTextChar
  {mso-style-name:"Balloon Text Char";
  mso-style-link:"Balloon Text";
  font-family:"Segoe UI",sans-serif;}
.MsoChpDefault
  {font-family:"Calibri",sans-serif;}
.MsoPapDefault
  {margin-bottom:10.0pt;
  line-height:115%;}
@page WordSection1
  {size:612.0pt 792.0pt;
  margin:72.0pt 72.0pt 72.0pt 72.0pt;}
div.WordSection1
  {page:WordSection1;}
 /* List Definitions */
 ol
  {margin-bottom:0cm;}
ul
  {margin-bottom:0cm;}
-->
</style>

</head>

<body lang=EN-PH>

<div class=WordSection1>
<p class=MsoNormal align=center style='text-align:center'><b><span lang=EN-US>SELECTION
OF EVALUATORS FOR PHREB ACCREDITATION</span></b></p>

<p class=MsoNormal><span lang=EN-US>INSTRUCTIONS: Proposed evaluators must be
evaluated by the Ethics Secretariat using the criteria below. A consultant must
have a total score of 85% to qualify as an evaluator.</span></p><br>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span lang=EN-US>Name of Accreditor: {{$id->accreditors->accreditors_name}}</span></p>



<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span lang=EN-US>Award Title: {{$id->title_award}}</span></p>



<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span lang=EN-US>REC Name: {{$id->for_rec}}</span></p>



<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span lang=EN-US>Level: {{$id->for_level}}</span></p>



<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span lang=EN-US>Date of Evaluation: {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $id->created_at)->format('F j, Y')}}</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span lang=EN-US>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span lang=EN-US>&nbsp;</span></p>

<table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0
 style='margin-left:32.4pt;border-collapse:collapse;border:none'>
 <tr>
  <td width=361 valign=top style='width:270.4pt;border:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span lang=EN-US>CRITERIA</span></b></p>
  </td>
  <td width=113 valign=top style='width:84.45pt;border:solid windowtext 1.0pt;
  border-left:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span lang=EN-US>MAXIMUM (%)</span></b></p>
  </td>
  <td width=107 valign=top style='width:80.25pt;border:solid windowtext 1.0pt;
  border-left:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span lang=EN-US>RATING</span></b></p>
  </td>
 </tr>

 <tbody>
  @foreach($acc_eval as $ae)
   <tr>
    <td width=361 valign=top style='width:270.4pt;border:solid windowtext 1.0pt;
    border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
    normal'><b><span lang=EN-US>{{$ae->accreditors_criteria->criteria_name}}</span></b></p>
    <p class=MsoListParagraph style='margin-bottom:0cm;margin-bottom:.0001pt;
    text-indent:-18.0pt;line-height:normal'><span lang=EN-US><span
    style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </span></span><span lang=EN-US>{{$ae->accreditors_criteria->criteria_description}}</span></p>
    </td>
    <td width=113 valign=top style='width:84.45pt;border-top:none;border-left:
    none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
    padding:0cm 5.4pt 0cm 5.4pt'>
    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
    text-align:center;line-height:normal'><span lang=EN-US>&nbsp;</span></p>
    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
    text-align:center;line-height:normal'><span lang=EN-US>{{$ae->accreditors_criteria->criteria_percentage}}</span></p>
    </td>
    <td width=107 valign=top style='width:80.25pt;border-top:none;border-left:
    none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
    padding:0cm 5.4pt 0cm 5.4pt'>
    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
    normal'><span lang=EN-US>&nbsp;</span></p>
     <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
    text-align:center;line-height:normal'><span lang=EN-US>{{$ae->acc_rate}}</span></p>
    </td>
   </tr>
 @endforeach

 <tr style='height:5.25pt'>
  <td width=361 valign=top style='width:270.4pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:5.25pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><b><span lang=EN-US>                                                                  
  TOTAL               </span></b></p>
  </td>
  <td width=113 valign=top style='width:84.45pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:5.25pt'>
  <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span lang=EN-US>&nbsp;</span></p>
  </td>
  <td width=107 valign=top style='width:80.25pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:5.25pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span lang=EN-US>&nbsp;</span></p>
   <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
    text-align:center;line-height:normal'><span lang=EN-US><b>{{$acc_eval->sum('acc_rate')}} % </b></span></p>
  </td>
 </tr>
</tbody>
</table>

<p class=MsoNormal><span lang=EN-US>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:324.0pt;margin-bottom:.0001pt;text-indent:36.0pt;line-height:normal'><span
lang=EN-US>Evaluated by:</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span lang=EN-US>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span lang=EN-US>&nbsp;</span></p>

<b style="style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:350.0pt;margin-bottom:.0001pt;text-indent:36.0pt;line-height:normal'">{{auth::user()->name}}</b>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:324.0pt;margin-bottom:.0001pt;text-indent:36.0pt;line-height:normal'><span
lang=EN-US>PHREB Secretariat</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span lang=EN-US>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span lang=EN-US>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span lang=EN-US>Noted by:</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span lang=EN-US>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span lang=EN-US>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><b><span lang=EN-US>CARINA L. REBULANAN</span></b></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span lang=EN-US>Chief, IDD</span></p>

<p class=MsoNormal><span lang=EN-US>&nbsp;</span></p>

<p class=MsoNormal><span lang=EN-US>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt'><span
lang=EN-US style='font-size:10.0pt;line-height:115%'>Note: </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt'><i><span
lang=EN-US style='font-size:10.0pt;line-height:115%'>Adjectival Rating</span></i></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt'><i><span
lang=EN-US style='font-size:10.0pt;line-height:115%'>Excellent                                 -
95%-100%</span></i></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt'><i><span
lang=EN-US style='font-size:10.0pt;line-height:115%'>Very Satisfactory                  -
85%-94%</span></i></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt'><i><span
lang=EN-US style='font-size:10.0pt;line-height:115%'>Satisfactory                           -
81%-84%</span></i></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt'><i><span
lang=EN-US style='font-size:10.0pt;line-height:115%'>Fair                                         -
75%-80%</span></i></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt'><i><span
lang=EN-US style='font-size:10.0pt;line-height:115%'>Unsatisfactory                       -
74% below</span></i></p>

<p class=MsoNormal><i><span lang=EN-US>&nbsp;</span></i></p>

</div>

</body>

</html>
