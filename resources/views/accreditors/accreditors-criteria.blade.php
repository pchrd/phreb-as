@extends('layouts.myApp')
@section('content')
<div class="container">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-11">
				<div class="col-md-12" id="verifyuser">
					@if(session('success'))
					<div class="alert alert-success">
						{{session('success')}}
						<button class="close" onclick="document.getElementById('verifyuser').style.display='none'">x</button>
					</div>
					@endif
				</div>
				
				<div class="card">
					<div class="card-header navbar-light"><strong>List of Criteria for Accreditors Evaluation for Awards</strong><a href="#" data-toggle="modal" data-target="#addcriteria"> | Add Criteria</a>
						<caption class="row pull-right">
							<a href="{{url('/accreditors')}}" class="pull-right"> Back </a>
						</caption>

					</div>
					<div class="card-body">
						
						<div class="container-fluid">
							<table class="table table-condense">
								<thead>
									<tr>
										<th>Name of Criteria</th>
										<th>Description of Criteria</th>
										<th>Maximum Percentage (in %)</th>
										<th>Actions</th>
									</tr>
								</thead>
								<tbody>
									@foreach($criterialist as $cl)
									<tr>
										<td>{{$cl->criteria_name}}</td>
										<td>{{$cl->criteria_description}}</td>
										<td>{{$cl->criteria_percentage}}</td>
										<td><a href="#" data-toggle="modal" data-target="#criteria{{$cl->id}}" title="Edit" class="btn btn-outline-success fa fa-pencil"></a><a href="#" class="btn btn-outline-danger fa fa-trash"></a></td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="addcriteria" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Add New Criteria</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

				<form action="{{route('addcriteria')}}" method="POST" enctype="multipart/form-data">
				{{csrf_field()}}
				
				<div class="form-group row">
					<label for="criteria_name" class="col-md-4 col-form-label text-md-left"><h6>{{ __('Name of Criteria* ') }}</h6></label>
					<div class="col-md-8">
						<input id="criteria_name" type="text" class="form-control{{ $errors->has('criteria_name') ? ' is-invalid' : '' }}" name="criteria_name" value="{{ old('criteria_name') }}" required>

						@if ($errors->has('criteria_name'))
						<span class="invalid-feedback" role="alert"><strong>{{ $errors->first('criteria_name') }}</strong></span>
						@endif
					</div>
				</div>

				<div class="form-group row">
					<label for="criteria_description" class="col-md-4 col-form-label text-md-left"><h6>{{ __('Description of Criteria* ') }}</h6></label>
					<div class="col-md-8">

						<textarea class="form-control{{ $errors->has('criteria_description') ? ' is-invalid' : '' }}" name="criteria_description" value="{{ old('criteria_description') }}" required>
							
						</textarea>

						@if ($errors->has('criteria_description'))
						<span class="invalid-feedback" role="alert"><strong>{{ $errors->first('criteria_description') }}</strong></span>
						@endif
					</div>
				</div>

				<div class="form-group row">
					<label for="criteria_percentage" class="col-md-4 col-form-label text-md-left"><h6>{{ __('Maximum Percentage (in %)* ') }}</h6></label>
					<div class="col-md-8">
						<input id="criteria_percentage" type="number" class="form-control{{ $errors->has('criteria_percentage') ? ' is-invalid' : '' }}" name="criteria_percentage" value="{{ old('criteria_percentage') }}" required>

						@if ($errors->has('criteria_percentage'))
						<span class="invalid-feedback" role="alert"><strong>{{ $errors->first('criteria_percentage') }}</strong></span>
						@endif
					</div>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-primary"><span class="fa fa-save"></span> Save</button>
				</div>
			</form>
			</div>
			</div>
		</div>
	</div>

@foreach($criterialist as $cl)
	<div class="modal fade" id="criteria{{$cl->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Update Criteria</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

				<form action="{{route('update_criteria', ['id' => $cl->id])}}" method="POST" enctype="multipart/form-data">
				{{csrf_field()}}
				
				<div class="form-group row">
					<label for="criteria_name" class="col-md-4 col-form-label text-md-left"><h6>{{ __('Name of Criteria* ') }}</h6></label>
					<div class="col-md-8">
						<input id="criteria_name" type="text" class="form-control{{ $errors->has('criteria_name') ? ' is-invalid' : '' }}" name="criteria_name" value="{{$cl->criteria_name}}" required>

						@if ($errors->has('criteria_name'))
						<span class="invalid-feedback" role="alert"><strong>{{ $errors->first('criteria_name') }}</strong></span>
						@endif
					</div>
				</div>

				<div class="form-group row">
					<label for="criteria_description" class="col-md-4 col-form-label text-md-left"><h6>{{ __('Description of Criteria* ') }}</h6></label>
					<div class="col-md-8">

						<textarea class="form-control{{ $errors->has('criteria_description') ? ' is-invalid' : '' }}" name="criteria_description" required>
							{{$cl->criteria_description}}
						</textarea>

						@if ($errors->has('criteria_description'))
						<span class="invalid-feedback" role="alert"><strong>{{ $errors->first('criteria_description') }}</strong></span>
						@endif
					</div>
				</div>

				<div class="form-group row">
					<label for="criteria_percentage" class="col-md-4 col-form-label text-md-left"><h6>{{ __('Maximum Percentage (in %)* ') }}</h6></label>
					<div class="col-md-8">
						<input id="criteria_percentage" type="number" class="form-control{{ $errors->has('criteria_percentage') ? ' is-invalid' : '' }}" name="criteria_percentage" value="{{$cl->criteria_percentage}}" required>

						@if ($errors->has('criteria_percentage'))
						<span class="invalid-feedback" role="alert"><strong>{{ $errors->first('criteria_percentage') }}</strong></span>
						@endif
					</div>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-success"><span class="fa fa-save"></span> Save Changes</button>
				</div>
			</form>
			</div>
			</div>
		</div>
	</div>
@endforeach

@endsection