@extends('layouts.myApp')
@section('content')
<div class="container">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-11">
				<div class="col-md-12" id="verifyuser">
					@if(session('success'))
					<div class="alert alert-success">
						{{session('success')}}
						<button class="close" onclick="document.getElementById('verifyuser').style.display='none'">x</button>
					</div>
					@endif
				</div>
				
				<div class="card">
					<div class="card-header navbar-light"><strong>Selection Of Evaluators For Awards</strong>
						<caption class="row pull-right">
							<a href="{{url('/accreditors')}}" class="pull-right"> Back </a>
						</caption>

					</div>
					<div class="card-body">
						
						<form method="POST" action="{{route('submiteval', ['accreditor_id' => $accreditor->id ])}}">
							@csrf
							<div class="form-group row">
								<label for="acc_name_eval" class="col-md-4 col-form-label text-md-right">{{ __('Name of Consultant: ') }}</label>

								<div class="col-md-6">
									<input id="acc_name_eval" type="text" class="form-control{{ $errors->has('acc_name_eval') ? ' is-invalid' : '' }}" name="acc_name_eval" value="{{$accreditor->accreditors_name}}" readonly>

									@if ($errors->has('acc_name_eval'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('acc_name_eval') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="form-group row">
								<label for="acc_title_eval" class="col-md-4 col-form-label text-md-right">{{ __('Title of the Award: ') }}</label>

								<div class="col-md-6">
									<input id="acc_title_eval" type="text" class="form-control{{ $errors->has('acc_title_eval') ? ' is-invalid' : '' }}" name="acc_title_eval" value="">

									@if ($errors->has('acc_title_eval'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('acc_title_eval') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="form-group row">
								<label for="for_rec" class="col-md-4 col-form-label text-md-right">{{ __('For REC Name: ') }}</label>

								<div class="col-md-6">
									<select name="for_rec" id="evalforrec" class="form-control{{ $errors->has('created_at') ? ' is-invalid' : '' }}" required>
										<option selected disabled value=""></option>
                                     <!-- get the data in db and put in select -->
                                     @foreach($selectrec as $id => $sr)
                       					<option value="{{$sr}}">{{$sr}}</option>
                       				 @endforeach
                                	</select>

									@if ($errors->has('for_rec'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('for_rec') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="form-group row">
								<label for="for_level" class="col-md-4 col-form-label text-md-right">{{ __('For Level: ') }}</label>

								<div class="col-md-6">
									<select name="for_level" id="forlevel" class="form-control{{ $errors->has('created_at') ? ' is-invalid' : '' }}" required>
										<option selected disabled value=""></option>
                                     <!-- get the data in db and put in select -->
                                    <option value="Level 1">Level 1</option>
                                    <option value="Level 2">Level 2</option>
                                    <option value="Level 3">Level 3</option>
                                	</select>

									@if ($errors->has('for_level'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('for_level') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="form-group row">
								<label for="for_rec" class="col-md-4 col-form-label text-md-right">{{ __('Criteria of Evaluation: ') }}</label>

								<div class="col-md-6">
									
									<table class="table table-responsive">
										<thead class="small">
											<tr>
												<th>Criteria</th>
												<th>Maximum(%)</th>
												<th>Rating(%)</th>
											</tr>
										</thead>
										<tbody>
											@foreach($criteria as $c)
											<tr>
												<td><b>{{$c->criteria_name}}</b><br>
													<small>{{$c->criteria_description}}</small>
												</td>
												<td>{{$c->criteria_percentage}}</td>
												<td><input type="hidden" name="acc_rate[criteria_id][]" value="{{$c->id}}"><input class="form-control" type="number" name="acc_rate[acc_rate][]" required></td>
											</tr>
											@endforeach
										</tbody>
									</table>

									@if ($errors->has('acc_title_eval'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('acc_title_eval') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="modal-footer">
								<button type="submit" class="btn btn-primary">{{ __('Submit Evaluation') }}</button>
							</div>
						</form>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	 jQuery(document).ready(function(){

        jQuery('#evalforrec').select2({
          placeholder : 'Please select erc name',
          tags: true,
        });
    });

	 jQuery(document).ready(function(){

        jQuery('#forlevel').select2({
          placeholder : 'Please select level',
          tags: true,
        });
    });
</script>
@endsection