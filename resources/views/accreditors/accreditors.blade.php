@extends('layouts.myApp')

@section('content')
<div class="container">

	<div class="col-md-12" id="verifyuser">
		@if(session('success'))
		<div class="alert alert-success">
			{{session('success')}}
			<button class="close" onclick="document.getElementById('verifyuser').style.display='none'">x</button>
		</div>
		@endif
	</div> 

		<div class="row">
			<div class="col-md-12 col-md-offset-1">
				<div class="panel panel-info">
					<div class="panel-heading"><h3><strong>List of Accreditors <small><a href="{{url('/accreditors-criteria')}}" class="pull-right btn btn-outline-secondary"><span class="fa fa-file-o"></span> Evaluation Criteria</a><a href="{{url('/list-of-evaluation')}}" class="btn btn-outline-warning pull-right"><span class="fa fa-list"></span> List of Evaluations</a><a href="#" data-toggle="modal" data-target="#addaccreditors" class="btn btn-outline-primary pull-right"><span class="fa fa-plus"></span> Add Accreditor</a></small></strong></h3></div>
					<div class="panel-body">
					
					<table class="table table-condense table-hover cell-border " id="acc_table">
							<thead>
								<tr>
									<th>ID</th>
									<th>NAME</th>
									<th>ACTIONS</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
 

<script type="text/javascript">

	$(function(){

		var oTable = $('#acc_table').DataTable({

			dom: '<"fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix ui-corner-tl ui-corner-tr"HlfrB>'+
				't'+'<"fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix ui-corner-bl ui-corner-br"ip>',

        	buttons:[
        		// ['copy', 'csv', 'excel', 'pdf', 'print'],
        		{  
       				extend: 'colvis',
		          
       			},

        		{ 
        		   extend: 'excel',
		           footer: true,
		           exportOptions: {
		                columns: []
		           }
       			},

       			{  
       				extend: 'copy',
		          
       			},

       			{  
       				extend: 'csv',
		          
       			},

       			{  
       				extend: 'pdf',
		          
       			},
       			{  
       				extend: 'print',
       				message: "List of Accreditors",

       				customize: function (win) {
                    $(win.document.body)
                        .css('font-size', '10pt')
                        .prepend(
                            '<img src="{{asset('image/image002.jpg')}}" style="position:static; top:10px; left:10px;" />'
                        );

                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
       			}
			       
      		 ],

      		stateSave: true,
			processing: true,
			serverSide: true,
			ajax: {
				url: '{{route('accreditors/show_ajax')}}',
			},
      		
			columns: [
			{ data: 'id',   name: 'id', visible:true},
			{ data: 'accreditors_name', name: 'accreditors_name'},
			{ data: 'action', name: 'action'}
			],
			
		});

	});

</script>

<!--accreditors modal -->
<div class="modal fade" id="addaccreditors" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Add Accreditor</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

				<form action="{{route('addaccreditors')}}" method="POST" enctype="multipart/form-data">
				{{csrf_field()}}
				
				<div class="form-group row">
					<label for="accreditors_name" class="col-md-4 col-form-label text-md-left"><h6>{{ __('Name of Accreditor:* ') }}</h6></label>
					<div class="col-md-8">
						<input id="accreditors_name" type="text" class="form-control{{ $errors->has('accreditors_name') ? ' is-invalid' : '' }}" name="accreditors_name" value="{{ old('accreditors_name') }}" required>

						@if ($errors->has('accreditors_name'))
						<span class="invalid-feedback" role="alert"><strong>{{ $errors->first('accreditors_name') }}</strong></span>
						@endif
					</div>
				</div>

				<div class="form-group row">
					<label for="accreditors_designation" class="col-md-4 col-form-label text-md-left"><h6>{{ __('Designation: ') }}</h6></label>
					<div class="col-md-8">
						<input id="accreditors_designation" type="text" class="form-control{{ $errors->has('accreditors_designation') ? ' is-invalid' : '' }}" name="accreditors_designation" value="{{ old('accreditors_designation') }}" required>

						@if ($errors->has('accreditors_designation'))
						<span class="invalid-feedback" role="alert"><strong>{{ $errors->first('accreditors_designation') }}</strong></span>
						@endif
					</div>
				</div>

				<div class="form-group row">
					<label for="accreditors_gender" class="col-md-4 col-form-label text-md-left"><h6>{{ __('Gender: ') }}</h6></label>
					<div class="col-md-8">
						<select class="form-control{{ $errors->has('accreditors_gender') ? ' is-invalid' : '' }}" name="accreditors_gender">
							<option selected="" disabled=""></option>
							<option value="Male">Male</option>
							<option value="Female">Female</option>
						</select> 

						@if ($errors->has('accreditors_gender'))
						<span class="invalid-feedback" role="alert"><strong>{{ $errors->first('accreditors_gender') }}</strong></span>
						@endif
					</div>
				</div>

				<div class="form-group row">
					<label for="accreditors_specialization" class="col-md-4 col-form-label text-md-left"><h6>{{ __('Specialization: ') }}</h6></label>
					<div class="col-md-8">
						<input id="accreditors_specialization" type="text" class="form-control{{ $errors->has('accreditors_specialization') ? ' is-invalid' : '' }}" name="accreditors_specialization" value="{{ old('accreditors_specialization') }}">

						@if ($errors->has('accreditors_specialization'))
						<span class="invalid-feedback" role="alert"><strong>{{ $errors->first('accreditors_specialization') }}</strong></span>
						@endif
					</div>
				</div>

				<div class="form-group row">
					<label for="accreditors_office" class="col-md-4 col-form-label text-md-left"><h6>{{ __('Office: ') }}</h6></label>
					<div class="col-md-8">
						<input id="accreditors_office" type="text" class="form-control{{ $errors->has('accreditors_office') ? ' is-invalid' : '' }}" name="accreditors_office" value="{{ old('accreditors_office') }}">

						@if ($errors->has('accreditors_office'))
						<span class="invalid-feedback" role="alert"><strong>{{ $errors->first('accreditors_office') }}</strong></span>
						@endif
					</div>
				</div>

				<div class="form-group row">
					<label for="accreditors_address" class="col-md-4 col-form-label text-md-left"><h6>{{ __('Address: ') }}</h6></label>
					<div class="col-md-8">
						<input id="accreditors_address" type="text" class="form-control{{ $errors->has('accreditors_address') ? ' is-invalid' : '' }}" name="accreditors_address" value="{{ old('accreditors_address') }}">

						@if ($errors->has('accreditors_address'))
						<span class="invalid-feedback" role="alert"><strong>{{ $errors->first('accreditors_address') }}</strong></span>
						@endif
					</div>
				</div>

				<div class="form-group row">
					<label for="accreditors_contactnumber" class="col-md-4 col-form-label text-md-left"><h6>{{ __('Contact Number: ') }}</h6></label>
					<div class="col-md-8">
						<input id="accreditors_contactnumber" type="text" class="form-control{{ $errors->has('accreditors_contactnumber') ? ' is-invalid' : '' }}" name="accreditors_contactnumber" value="{{ old('accreditors_contactnumber') }}">

						@if ($errors->has('accreditors_contactnumber'))
						<span class="invalid-feedback" role="alert"><strong>{{ $errors->first('accreditors_contactnumber') }}</strong></span>
						@endif
					</div>
				</div>

				<div class="form-group row">
					<label for="accreditors_email" class="col-md-4 col-form-label text-md-left"><h6>{{ __('Email: ') }}</h6></label>
					<div class="col-md-8">
						<input id="accreditors_email" type="email" class="form-control{{ $errors->has('accreditors_email') ? ' is-invalid' : '' }}" name="accreditors_email" value="{{ old('accreditors_email') }}">

						@if ($errors->has('accreditors_email'))
						<span class="invalid-feedback" role="alert"><strong>{{ $errors->first('accreditors_email') }}</strong></span>
						@endif
					</div>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-primary"><span class="fa fa-save"></span> Save</button>
				</div>
			</form>
			</div>
			</div>
		</div>
	</div>
@endsection