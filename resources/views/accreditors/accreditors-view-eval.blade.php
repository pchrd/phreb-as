@extends('layouts.myApp')
@section('content')
<div class="container">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-11">
				<div class="col-md-12" id="verifyuser">
					@if(session('success'))
					<div class="alert alert-success">
						{{session('success')}}
						<button class="close" onclick="document.getElementById('verifyuser').style.display='none'">x</button>
					</div>
					@endif
				</div>
				
				<div class="card">
					<div class="card-header navbar-light"><strong>List of Accreditors Evaluation</strong>
						<caption class="row pull-right">
							<a href="{{url('/accreditors')}}" class="pull-right"> Back </a>
						</caption>

					</div>
					<div class="card-body">
						<table class="table table-condense table-hover cell-border " id="acc_eval_table">
							<thead>
								<tr>
									<th>ID</th>
									<th>Name of Accreditor</th>
									<th>Evaluation Title</th>
									<th>REC</th>
									<th>Date of Evaluation</th>
									<th>Evaluated By</th>
									<th>Actions</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

	$(function(){

		var oTable = $('#acc_eval_table').DataTable({

			dom: '<"fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix ui-corner-tl ui-corner-tr"HlfrB>'+
				't'+'<"fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix ui-corner-bl ui-corner-br"ip>',

        	buttons:[
        		// ['copy', 'csv', 'excel', 'pdf', 'print'],
        		{  
       				extend: 'colvis',
		          
       			},

        		{ 
        		   extend: 'excel',
		           footer: true,
		           exportOptions: {
		                columns: []
		           }
       			},

       			{  
       				extend: 'copy',
		          
       			},

       			{  
       				extend: 'csv',
		          
       			},

       			{  
       				extend: 'pdf',
		          
       			},
       			{  
       				extend: 'print',
       				message: "List of Evaluation",

       				customize: function (win) {
                    $(win.document.body)
                        .css('font-size', '10pt')
                        .prepend(
                            '<img src="{{asset('image/image002.jpg')}}" style="position:static; top:10px; left:10px;" />'
                        );

                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
       			}
			       
      		 ],

      		stateSave: true,
			processing: true,
			serverSide: true,
			ajax: {
				url: '{{route('accreditorsevaluation/show_ajax')}}',
			},
      		
			columns: [
			{ data: 'id', name: 'id', visible:true},
			{ data: 'accreditors.accreditors_name', name: 'accreditors.accreditors_name'},
			{ data: 'title_award', name: 'title_award'},
			{ data: 'for_rec', name: 'for_rec'},
			{ data: 'created_at', name: 'created_at'},
			{ data: 'users.name', name: 'users.name'},
			{ data: 'action', name: 'action'}
			],
			
		});

	});

</script>

@endsection