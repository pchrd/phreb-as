@extends('layouts.myApp')

@section('content')
<div class="container">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-8">
				<div class="col-md-12" id="verifyuser">
					@if(session('success'))
					<div class="alert alert-success">
						{{session('success')}}
						<button class="close" onclick="document.getElementById('verifyuser').style.display='none'">x</button>
					</div>
					@endif
				</div>
				
				<div class="card">
					<div class="card-header navbar-light"><strong>Update Records</strong>
						<caption class="row pull-right">
							<a href="{{url('/accreditors')}}" class="pull-right"> Back </a>
						</caption>

					</div>
					<div class="card-body">
						
						<form method="POST" action="{{route('update_acc', ['id' => $acc->id])}}">
							@csrf
							<div class="form-group row">
								<label for="accreditors_name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

								<div class="col-md-6">
									<input id="accreditors_name" type="text" class="form-control{{ $errors->has('accreditors_name') ? ' is-invalid' : '' }}" name="accreditors_name" value="{{$acc->accreditors_name}}" required autofocus>

									@if ($errors->has('accreditors_name'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('accreditors_name') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="form-group row">
								<label for="accreditors_designation" class="col-md-4 col-form-label text-md-right">{{ __('Designation') }}</label>

								<div class="col-md-6">
									<input id="accreditors_designation" type="text" class="form-control{{ $errors->has('accreditors_designation') ? ' is-invalid' : '' }}" name="accreditors_designation" value="{{$acc->accreditors_designation}}"  >

									@if ($errors->has('accreditors_designation'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('accreditors_designation') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="form-group row">
								<label for="accreditors_gender" class="col-md-4 col-form-label text-md-right">{{ __('Gender') }}</label>

								<div class="col-md-6">
									<select class="form-control{{ $errors->has('accreditors_designation') ? ' is-invalid' : '' }}" name="accreditors_gender">
										<option value="{{$acc->accreditors_gender}}">{{$acc->accreditors_gender}}</option>
										<option value="Male">Male</option>
										<option value="Female">Female</option>
									</select>

									@if ($errors->has('accreditors_gender'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('accreditors_gender') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="form-group row">
								<label for="accreditors_specialization" class="col-md-4 col-form-label text-md-right">{{ __('Specialization') }}</label>

								<div class="col-md-6">
									<input id="accreditors_specialization" type="text" class="form-control{{ $errors->has('accreditors_specialization') ? ' is-invalid' : '' }}" name="accreditors_specialization" value="{{$acc->accreditors_specialization}}"  >

									@if ($errors->has('accreditors_specialization'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('accreditors_specialization') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="form-group row">
								<label for="accreditors_office" class="col-md-4 col-form-label text-md-right">{{ __('Office ') }}</label>

								<div class="col-md-6">
									<input id="accreditors_office" type="text" class="form-control{{ $errors->has('accreditors_office') ? ' is-invalid' : '' }}" name="accreditors_office" value="{{$acc->accreditors_office}}"  >

									@if ($errors->has('accreditors_office'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('accreditors_office') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="form-group row">
								<label for="accreditors_address" class="col-md-4 col-form-label text-md-right">{{ __('Address') }}</label>

								<div class="col-md-6">
									<input id="accreditors_address" type="text" class="form-control{{ $errors->has('accreditors_address') ? ' is-invalid' : '' }}" name="accreditors_address" value="{{$acc->accreditors_address}}"  >

									@if ($errors->has('accreditors_address'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('accreditors_address') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="form-group row">
								<label for="accreditors_contactnumber" class="col-md-4 col-form-label text-md-right">{{ __('Contact Number') }}</label>

								<div class="col-md-6">
									<input id="accreditors_contactnumber" type="text" class="form-control{{ $errors->has('accreditors_contactnumber') ? ' is-invalid' : '' }}" name="accreditors_contactnumber" value="{{$acc->accreditors_contactnumber}}"  >

									@if ($errors->has('accreditors_contactnumber'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('accreditors_contactnumber') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="form-group row">
								<label for="accreditors_email" class="col-md-4 col-form-label text-md-right">{{ __('Email') }}</label>

								<div class="col-md-6">
									<input id="accreditors_email" type="text" class="form-control{{ $errors->has('accreditors_email') ? ' is-invalid' : '' }}" name="accreditors_email" value="{{$acc->accreditors_email}}" autofocus>

									@if ($errors->has('accreditors_email'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('accreditors_email') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="modal-footer">
								<button type="submit" class="btn btn-primary">{{ __('Save Changes') }}</button>
							</div>
						</form>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection