@extends('layouts.myApp')
@section('content')
<div class="container">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-11">
				<div class="col-md-12" id="verifyuser">
					@if(session('success'))
					<div class="alert alert-success">
						{{session('success')}}
						<button class="close" onclick="document.getElementById('verifyuser').style.display='none'">x</button>
					</div>
					@endif
				</div>
				
				<div class="card">
					<div class="card-header navbar-light"><strong>Selection of Evaluators for PHREB Accreditation</strong>
						<caption class="row pull-right">
							<a href="{{url('/list-of-evaluation')}}" class="pull-right"> Back </a>
						</caption>

					</div>
					<div class="card-body">
						<div class="container-fluid row">
						<p><li>INSTRUCTIONS: Proposed evaluators must be evaluated by the Ethics Secretariat using the criteria below. A consultant must have a total score of 85% to qualify as an evaluator.</li></p><br>
						</div>

						<form method="POST" action="{{route('update-evaluations', ['id' => $id->id])}}">
							@csrf
							<div class="form-group row">
								<label for="acc_name_eval" class="col-md-4 col-form-label text-md-right">{{ __('Name of Consultant: ') }}</label>

								<div class="col-md-6">
									<input id="acc_name_eval" type="text" class="form-control{{ $errors->has('acc_name_eval') ? ' is-invalid' : '' }}" name="acc_name_eval" value="{{$id->accreditors->accreditors_name}}" readonly>

									@if ($errors->has('acc_name_eval'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('acc_name_eval') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="form-group row">
								<label for="acc_title_eval" class="col-md-4 col-form-label text-md-right">{{ __('Title of the Award: ') }}</label>

								<div class="col-md-6">
									<input id="acc_title_eval" type="text" class="form-control{{ $errors->has('acc_title_eval') ? ' is-invalid' : '' }}" name="acc_title_eval" value="{{$id->title_award}}">

									@if ($errors->has('acc_title_eval'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('acc_title_eval') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="form-group row">
								<label for="for_rec" class="col-md-4 col-form-label text-md-right">{{ __('For REC Name: ') }}</label>

								<div class="col-md-6">
									<select name="for_rec" id="evalforrec" class="form-control{{ $errors->has('created_at') ? ' is-invalid' : '' }}" required>
										<option selected value="{{$id->for_rec}}">{{$id->for_rec}}</option>
										@foreach($selectrec as $sr)
											<option value="{{$sr}}">{{$sr}}</option>
										@endforeach
                                	</select>

									@if ($errors->has('for_rec'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('for_rec') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="form-group row">
								<label for="for_level" class="col-md-4 col-form-label text-md-right">{{ __('For Level: ') }}</label>

								<div class="col-md-6">	
									<select name="for_level" id="forlevel" class="form-control{{ $errors->has('created_at') ? ' is-invalid' : '' }}" required>
										<option selected value="{{$id->for_level}}">{{$id->for_level}}</option>
                                     <!-- get the data in db and put in select -->
                                    <option value="Level 1">Level 1</option>
                                    <option value="Level 2">Level 2</option>
                                    <option value="Level 3">Level 3</option>
                                	</select>

									@if ($errors->has('for_level'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('for_level') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="form-group row">
								<label for="acc_title_eval" class="col-md-4 col-form-label text-md-right">{{ __('Criteria of Evaluation: ') }}</label>

								<div class="col-md-6">
									
									<table class="table table-condensed">
										<thead class="small">
											<tr>
												<th>Criteria</th>
												<th>Maximum(%)</th>
												<th>Rating(%)</th>
											</tr>
										</thead>
										<tbody>
											
											@foreach($acc_eval as $ae)
											<tr>
												<td><b>{{$ae->accreditors_criteria->criteria_name}}</b><br>
													<small>{{$ae->accreditors_criteria->criteria_description}}</small>
												</td>
												<td>{{$ae->accreditors_criteria->criteria_percentage}}</td>
												<td><input type="hidden" name="acc_rate[criteria_id][]" value="{{$ae->criteria_id}}"><input class="form-control" type="number" name="acc_rate[acc_rate][]" value="{{$ae->acc_rate}}"></td>
											</tr>
											@endforeach
											<tr>
												<td><b class="text text-danger">TOTAL:</b></td>
												<td><b class="text text-dark">{{$ae->accreditors_criteria->sum('criteria_percentage')}} %:</b></td>
												<td><b class="text text-dark">{{$x = $acc_eval->sum('acc_rate')}} %</b>
													
														@if($x >= 95 )
															<label class="badge badge-info">Excellent</label>
														@elseif($x >= 85)
															<label class="badge badge-info">Very Satisfactory</label>
														@elseif($x >= 81)
															<label class="badge badge-info">Satisfactory</label>
														@elseif($x >= 75)
															<label class="badge badge-info">Fair</label>
														@elseif($x <= 74)
															<label class="badge badge-info">Unsatisfactory</label>
														@endif
													
												</td>
											</tr>
										</tbody>
									</table>

									@if ($errors->has('acc_title_eval'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('acc_title_eval') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="form-group row">
								<label for="evaluatedby_user_id" class="col-md-4 col-form-label text-md-right">{{ __('Evaluated By: ') }}</label>

								<div class="col-md-6">
									<input id="evaluatedby_user_id" type="text" class="form-control{{ $errors->has('evaluatedby_user_id') ? ' is-invalid' : '' }}" name="evaluatedby_user_id" value="{{$id->users->name}}" readonly>

									@if ($errors->has('evaluatedby_user_id'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('evaluatedby_user_id') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="form-group row">
								<label for="created_at" class="col-md-4 col-form-label text-md-right">{{ __('Date Created: ') }}</label>

								<div class="col-md-6">
									<input id="created_at" type="text" class="form-control{{ $errors->has('created_at') ? ' is-invalid' : '' }}" name="created_at" value="{{$id->created_at}}" readonly="">

									@if ($errors->has('created_at'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('created_at') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="container-fluid">
								<label>Note</label>
								<table class="table table-condensed">
										<thead>
											<tr>
												<td>Adjectival</td>
												<td>Rating</td>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>
													<li>Excellent</li>
													<li>Very Satisfactory</li>
													<li>Satisfactory</li>
													<li>Fair</li>
													<li>Unsatisfactory</li>
												</td>
												<td>
													- 95%-100%<br>
													- 85%-94%<br>
													- 81%-84%<br>
													- 75%-80%<br>
													- 74% below
												</td>
											</tr>
										</tbody>
								</table>
							</div>

							<div class="modal-footer">
								<a href="{{route('download-evaluations', ['id' => $id->id])}}" class="btn btn-warning" style="color:white;"><span class="fa fa-download">{{ __(' Download as PDF') }}</span></a>

								@if($id->users->id == Auth::user()->id)
								<button type="submit" class="btn btn-success"><span class="fa fa-save"></span> Save Changes</button>
								@endif

							</div>
						</form>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	 jQuery(document).ready(function(){

        jQuery('#evalforrec').select2({
          placeholder : 'Please select erc name',
          tags: true,
        });
    });
</script>
@endsection