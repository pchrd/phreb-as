@extends('layouts.myApp')
@section('content')
 <div class="container">
		<div class="row">
			<div class="col-md-12 col-md-offset-1">
				<div class="panel panel-info">


					<div class="card">
						<h5 class="card-header">Accredited RECs for {{$searchkey_level}} @auth  @else in 
							@endauth {{$searchkey_region}}

							@auth 
								<a href="{{url('/dashboard')}}" class="btn btn-outline-primary float-right" title="Go to dashboard"><span class="fa fa-home"></span></a> 
							@endauth

							@guest 
								
								<a href="{{url()->previous()}}" class="btn btn-outline-primary float-right" title="Go back">←</a>

							@endguest
						</h5>
						<div class="card-body">
							

									<table class="table table-bordered table-responsive table-hover" width="100%" id="filter_table">

							<thead class="bg-info" style="color: white;">
								<tr>
									<th>ID</th>
									<th>REGION</th>
									<th>RESEARCH ETHICS COMMITTEE</th>
									<th>DATE ACCREDITED</th>
									<th>DATE ACCREDITATION EXPIRY</th>
									<th>STATUS</th>
									
									@auth
										@can('secretariat-access')
											<th>ACTIONS</th>
										@endcan
									@endauth

								</tr>
							</thead>
							<tbody>
								@foreach($recdetails as $key => $recdetail)
								<tr>
									<td>{{$key+1}}</td>
									<td>{{$recdetail->region_name}}</td>
									<td><strong>{{$recdetail->rec_institution}}</strong><br>
										<small>{{$recdetail->rec_name}}</small><br>
										
										<small>
											@if(Auth::check() AND Auth::user()->can('accreditors_and_csachair_secretariat'))
												{{$recdetail->rec_email}}
											@endif 
										</small>
									</td>
									
									<td>
										{{$recdetail->date_accreditation == null ? '' : \Carbon\Carbon::createFromFormat('Y-m-d',$recdetail->date_accreditation)->format('F j, Y')}}
									</td>

									<td>
										{{$recdetail->date_accreditation_expiry == null ? '' : \Carbon\Carbon::createFromFormat('Y-m-d',$recdetail->date_accreditation_expiry)->format('F j, Y')}}
									</td>
									
									@if($recdetail->date_accreditation_expiry <= \Carbon\Carbon::today())
										
										<td>
											<span class="badge badge-danger">Accreditation has been Expired</span>
										</td>
										
									@else		

										<td>{{$recdetail->status_name}}<br>
							
												<small>- {{$recdetail->date_accreditation_expiry == null ? '' : \Carbon\Carbon::createFromFormat('Y-m-d',$recdetail->date_accreditation_expiry)->diffForHumans($recdetail->date_accreditation)}} </small><br>
												<small>- {{$recdetail->date_accreditation_expiry == null ? '' : \Carbon\Carbon::createFromFormat('Y-m-d',$recdetail->date_accreditation_expiry)->diffInDays(\Carbon\Carbon::now())}} days remaining </small>

										</td>

									@endif
									
									@auth

										@can('secretariat-access')
											<td><a href="{{ route('view-recdetails', ['id' => encrypt($recdetail->recdetails_id)]) }}" title="View Details" class="btn btn-xs btn-success"><i class="fa fa-eye"></i></a></td>
										@endcan

									@endauth

								</tr>

								@endforeach	
							</tbody>

						</table>

						</div>
					</div>



					
				</div>
			</div>
		</div>
	</div>
<!-- 
<script type="text/javascript">
	$(document).ready(function() {
		$('#filter_table').DataTable();
	} );
</script> -->
 @endsection