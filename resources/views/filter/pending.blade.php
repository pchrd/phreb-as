@extends('layouts.myApp')
@section('content')
 <div class="container">
		<div class="row">
			<div class="col-md-12 col-md-offset-1">
				<div class="panel panel-info">
					
					<div class="card panel-heading" style="background: red;">
						<center><h1 style="color: white;"><strong>{{$searchkey_pending}} Status</strong></h1></center>

					</div>

					<div class="panel-body">
						<table class="table table-bordered table-hover " id="recdetails_table">
							<thead>
								<tr>
									<th>ID</th>
									<th>Region</th>
									<th>Research Ethics Committee</th>
									<th>Level</th>
									
									<th>Status</th>
									@can('secretariat-access')
										<th>Actions</th>
									@endcan
								</tr>
							</thead>
							<tbody>
								@foreach($recdetails as $recdetail)
								<tr>
									<td>{{$recdetail->id}}</td>
									<td>{{$recdetail->applicationlevel->regions->region_name}}</td>
									<td><strong>{{$recdetail->rec_institution}} <br> {{$recdetail->rec_name}}</strong>
										<br>
										<small>{{$recdetail->rec_email}}</small>
										<br><br>
										<small>Created at {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$recdetail->applicationlevel->created_at)->format('F j, Y')}} -
										<i class="text text-danger">{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$recdetail->applicationlevel->created_at)->diffForHumans($recdetail->applicationlevel->date_accreditation)}}</i></small>
									</td>
									<td><h1>{{$recdetail->applicationlevel->level_id}}</h1></td>
									
									<td>

										<h3>{{$recdetail->applicationlevel->status->status_name}} </h3>

										@if($recdetail->submissionsstatuses->count() <= 1)
										
											<span class="badge badge-secondary">New Submission</span>

										@else

											<span class="badge badge-warning">Submitted Add’l Requirements</span>


										@endif

									<!-- 	<br>
 -->
										<!-- <small>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $recdetail->submissionsstatuses->last()->submissionstatuses_datecreated)->format('F j, Y')}} -
										<i class="text text-danger">{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $recdetail->submissionsstatuses->last()->submissionstatuses_datecreated)->diffForHumans()}}</i></small> -->

									</td>

									@can('secretariat-access')
									<td><a href="{{ route('view-recdetails', ['id' => encrypt($recdetail->applicationlevel->recdetails_id)]) }}" title="View Details" class="btn btn-xs btn-success"><i class="fa fa-eye"></i></a></td>
									@endcan
									
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
 @endsection