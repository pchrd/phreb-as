@extends('layouts.myApp')
@section('content')
<div class="container-fluid">
	<div class="container">
		<div class="col-md-12" id="savedclass">
			
			@if(session('success'))
			<div class="alert alert-success ">
				<i class="fa fa-exclamation"></i>{{session('success')}}
				<button class="close" onclick="document.getElementById('savedclass').style.display='none'">x</button>
			</div>
			@endif

			@if (session('error'))
				<div class="alert alert-danger">{{ session('error') }}</div>
			@endif
		</div>

		<div class="form-group col-md-12 col-md-offset-1">

			<label>
				<b>	
					<h3>
						{{$phrebform->phrebforms_template->pft_name}}
					</h3>

					{{$phrebform->recdetails->rec_institution}} - {{$phrebform->recdetails->rec_name}} {{$phrebform->recdetails->applicationlevel->levels->level_name}}<br>

					 @can('rec-access')
					 	Date Created: 
					 		<u class="label label-danger">
					 			{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $phrebform->created_at)->format('F j, Y g:i:A')}} 
					 			(<u class="text text-danger">{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $phrebform->created_at)->diffForHumans()}}</u>)
					 		</u>
					 	<br>

					 	Last Date Updated: 
					 		<u class="label label-danger">
					 			{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $phrebform->updated_at)->format('F j, Y g:i:A')}}
					 			(<u class="text text-danger">{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $phrebform->updated_at)->diffForHumans()}}</u>)
					 		</u>
					 @endcan

					 @if($phrebform->phrebform_apsubmission_recs()->exists())
						 <p>
						 	Status: 
							 	<label class="badge badge-danger" title="At {{$phrebform->phrebform_apsubmission_recs->getCurrentStatus()->created_at}} by {{Auth::user()->can('rec-access') ? '' : $phrebform->phrebform_apsubmission_recs->getCurrentStatus()->users->name }} ({{$phrebform->phrebform_apsubmission_recs->getCurrentStatus()->users->role_users->roles->role_name}}) - {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $phrebform->phrebform_apsubmission_recs->getCurrentStatus()->created_at)->diffforhumans()}}">
							 		{{$phrebform->phrebform_apsubmission_recs->getCurrentStatus()->statuses->status_name}}
							 	</label>
						 </p>
					 @endif
				</b>
			</label>

			<a href="{{route('view-assessment-form', encrypt($phrebform->phrebformActionplan->ap_pf_id)) }}" class="btn btn-default pull-right form-group"><span class="fa fa-arrow-left"></span> Back</a>

		</div>


		   @if($phrebform->phrebform_apsubmission_recs()->exists())
			  <div class="alert alert-info">
					{{session('success')}}

						<label class="small">

							@if(Auth::user()->can('rec-access'))

								Note: This form was already submitted to PHREB at <b>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $phrebform->phrebform_apsubmission_recs->created_at)->format('F j, Y - G:i:A')}}</b>. You are not able update and attach file in this form once the status is <b>Submitted</b> or <b>Approved</b>.

							@else

								Note: This form was already submitted to PHREB at <b>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $phrebform->phrebform_apsubmission_recs->created_at)->format('F j, Y - G:i:A')}}</b>. PHREB are not able take actions  and update/edit this form once the status is <b>Returned</b> or <b>Approved</b>.

							@endcan

						</label>
		 	  </div>
		  @endif

		<form action="{{route('updateActionplan', ['id' => encrypt($phrebform->id)])}}" method="POST">
			{{ csrf_field() }}
			<textarea class="form-control" id="editor1" name="editor1">
				<div class="{{$actionplanValidation ? 'mceNonEditable' : ''}}"> 
					{{$phrebform->pf_body1}}
				</div>			 
			</textarea>
			<input name="image" type="file" id="upload" hidden onchange="">
			
			@can('rec-access')
				<div class="container-fluid btn-group form-group" role="group" aria-label="Basic example">
					<button type="submit" title="Save" class="btn btn-success" onclick="return confirm('Do you want to save changes?')"><span class="fa fa-save"></span> Save Changes</button>				
					<button type="button"  data-toggle="modal" data-target="#attachDocuments" class="btn bg bg-black btn-block"><span class="fa fa-upload"></span> Attach Files</button>
				</div>	
			@endcan

			@can('secretariat-access')
				<div class="container-fluid btn-group form-group" role="group" aria-label="Basic example">
					<button type="button"  data-toggle="modal" data-target="#actions" class="btn bg bg-aqua btn-block"><span class="fa fa-tasks"></span> Actions</button>
				</div>	
			@endcan

			@can('accreditors-access')
				<div class="container-fluid btn-group form-group" role="group" aria-label="Basic example">
					<button type="button"  data-toggle="modal" data-target="#actions" class="btn bg bg-aqua btn-block"><span class="fa fa-tasks"></span> Notify PHREB Secretariat-in-Charge for Action </button>
				</div>	
			@endcan

		</form>

		

	<form action="{{route('downloadZip', ['id' => $phrebform->id])}}" method="GET"> 	
		<div class="form-control" id="history">
			<div class="col-md-12 col-md-offset-1">
				<div class="panel panel-secondary">
					<div class="panel-heading"><h3><strong>Attached Document / Compliance Evidences</strong>
						
						<button type="submit" class="btn bg-yellow fa fa-download btn-sm pull-right" onclick="return confirm('Do you want to download it anyway?')"> Download</button></h3>

					</div>
					<div class="panel-body">
						<table class="table table-condensed table-hover" id="assessment_tbl">
							<thead class="badge-default">
								<tr>
									<td>No.</td>
									<td>Folder Name</td>
									<td>Date Created</td>
									
									@if(Auth::user()->can('rec-access'))
										<td>Actions</td>
									@endif

									<td>Select all <input type="checkbox" title="Select All Folder?" class="cbox" onclick="toggle(this);"></td>
								</tr>
							</thead>
							<tbody>

								@foreach($phrebform->phrebform_filefolders as $id => $pfff)
									<tr>
										<td>{{$id+1}}</td>
										<td><a href="#" onclick="MyWindow=window.open('{{route('viewDocuments', ['id' => encrypt($pfff->id)])}}', 'MyWindow', width=1200, height=300); return false;" title="View Folder"><span class="fa fa-file"></span> {{$pfff->pfff_name}}</a></td>
										<td>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $pfff->created_at)->format('F j, Y G:i:A')}} | <small class="text text-danger">{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $pfff->created_at)->diffForHumans()}}</small></td>

										@if(Auth::user()->can('rec-access'))
											<td>
												<a href="#" data-toggle="modal" data-target="#editFolder{{$pfff->id}}" title="Edit Folder Name"><span class="fa fa-edit"></span></a>
												
												<a href="{{route('deleteFolder', ['id' => encrypt($pfff->id)])}}" title="Delete Folder" onclick="return confirm('Do you want to Delete {{$pfff->pfff_name}}?')"><span class="fa fa-trash"></span></a>
											</td>
										@endif

										<td><input type="checkbox" name="cbox_download[]" value="{{$pfff->id}}"></td>
									</tr>
								@endforeach

							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	 </form>

	 @can('rec-access')
		 <div class="footer form-group">
		 	<a href="#" data-toggle="modal" data-target="#submitToPhreb" class="btn btn-info btn-block fa fa-send"> SUBMIT </a>
		 </div>
	 @endcan
	

	 <div class="form-control" id="history">
	 	<div class="col-md-12">
	 		<div class="panel">
	 			<div class="panel-heading">
	 				<h5>
	 					<strong>PHREB Remarks/Comments
	 					@can('accreditors_and_csachair_secretariat')
	 						<a href="#" class="btn btn-primary fa fa-pencil btn-sm pull-right" data-toggle="modal" data-target="#createremarks"> Create Remarks </a>
	 					@endcan
	 					</strong>
	 				</h5></div>
	 			<div class="panel-body">
	 				<table class="table table-condensed table-hover small" id="assessment_tbl">
	 					<thead class="">
	 						<tr>
	 							<td>No.</td>
	 							<td>From</td>
	 							<td>Date Created</td>
	 							<td>Remarks/Comments</td>
	 							<td>Status</td>
	 						</tr>
	 					</thead>
	 					<tbody>
	 						@foreach($phrebform->phrebform_remarks as $id => $pf_remarks)
		 						<tr>
		 							<td>{{$id + 1}}</td>
		 							<td>{{Auth::user()->can('rec-access') ? $pf_remarks->users->role_users->roles->role_name : $pf_remarks->users->name}}</td>
		 							<td>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $pf_remarks->created_at)->format('F j, Y - G:i:A')}} | <label class="text text-danger">{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $pf_remarks->created_at)->diffforhumans()}}</label></td>
		 							<td><a href="{{route('viewPhrebformRemarks', ['id' => encrypt($pf_remarks->id)])}}" title="Click to view" target="_blank"><span class="fa fa-comments-o"></span>...</a></td>
		 							<td>
		 								@if($pf_remarks->pf_remarks_status != null)
		 									<label class="badge badge-success" title="at {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $pf_remarks->pf_remarks_status)->format('F j, Y - G:i:A')}} ({{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $pf_remarks->pf_remarks_status)->diffforhumans()}})">Submitted</label>
		 								@else
		 									<label class="badge badge-danger" title="Please choose RETURNED in the action button in order to submit it to {{$pf_remarks->phrebforms->recdetails->rec_institution}} - {{$pf_remarks->phrebforms->recdetails->rec_institution}} {{$pf_remarks->phrebforms->recdetails->applicationlevel->levels->level_name}}">Not Submitted</label>
		 								@endif
		 							</td>
		 						</tr>
	 						@endforeach
	 					</tbody>
	 				</table>
	 				<div class="footer">

	 				</div>
	 			</div>
	 		</div>
	 	</div>
	 </div>

	 	<div class="form-control" id="history">
			<div class="col-md-12">
				<div class="panel">
					<div class="panel-heading"><h5><strong>Save Changes Logs (History)</strong></h5></div>
					<div class="panel-body">
						<table class="table table-condensed table-hover small" id="assessment_tbl">
							<thead class="">
								<tr>
									<td>No.</td>
									<td>Revision By</td>
									<td>Last Date Updated</td>
									<td>Before</td>
									<td>After</td>
								</tr>
							</thead>
							<tbody>
								@foreach($history as $field => $value)
								<tr>
									<td>{{$field + 1}}</td>
									<td>
										<span class="fa fa-user"></span> {{Auth::user()->can('rec-access') ? $value->user->role_users->roles->role_name : $value->user->name}}<br>
										<small>{{$value->user->role_users->roles->role_name}}</small>
									</td>
									<td>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->created_at)->format('F j, Y g:i A')}}<br>
										<small class="text text-danger">{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->created_at)->diffForHumans()}}</small>
									</td>
									<td>
										<a href="#" data-toggle="modal" data-target="#before{{$value->id}}">
											<span class="fa fa-file"></span>
										</a>
									</td>
									<td>
										<a href="#" data-toggle="modal" data-target="#after{{$value->id}}">
											<span class="fa fa-file"></span>
										</a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
						<div class="footer">
							{{$history->links()}}
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>


</div>


<!-- start of modals -->
@can('accreditors_and_csachair_secretariat')
<!-- create remarks modal -->
<div class="modal fade" id="createremarks" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header badge-info">
				<h4 class="modal-title">PHREB Comments/Remarks Form</h4>
			</div>
			<div class="modal-body">

				<form action="{{route('phrebformRemarks', ['id' => $phrebform->id])}}" method="POST">
	     			{{ csrf_field() }}
		     		<textarea class="form-control" name="editor2" id="editor2">
		     			
		     		</textarea>
		     		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary">Create</button>
		      			<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
		      		</div>

     			</form>
			</div>
		</div>
	</div>
</div>
@endcan


@can('accreditors_and_csachair_secretariat')
<div class="modal fade" id="actions" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header badge-info">
				<h4 class="modal-title" id="myModalLabel">Form Action</h4>
			</div>
			<div class="modal-body">

				<form action="{{route('phrebformActions', ['id' => $phrebform->id])}}" method="POST">
					{{ csrf_field() }}

					<div class="form-group row">
							<label for="action_statuses_id" class="col-md-4 col-form-label text-md-left"><b></b>{{ __('Choose Action*') }}</label>

							<div class="col-md-8">
								<select id="action_statuses_id" type="text" class="form-control{{ $errors->has('action_statuses_id') ? ' is-invalid' : '' }}" name="action_statuses_id" value="{{ old('action_statuses_id') }}" required>
									
									<option selected disabled>---Please select---</option>

									@foreach($getStatuses as $id => $gs)
										<option value="{{$id}}">{{$gs}}</option>
									@endforeach

								</select>
									
									@if ($errors->has('action_statuses_id'))
									<span class="invalid-feedback">
										<strong>{{ $errors->first('action_statuses_id') }}</strong>
									</span>
									@endif
							</div>
					</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Proceed</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>

				</form>
			</div>
		</div>
	</div>
</div>
@endcan


@can('rec-access')
<div class="modal fade" id="submitToPhreb" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header badge-info">
				<h4 class="modal-title" id="myModalLabel">Submission Confirmation</h4>
			</div>
			<div class="modal-body">

				<label>
					Do you want to submit your <b>{{$phrebform->phrebforms_template->pft_name}} created at {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $phrebform->created_at)->format('F j, Y - G:i:A')}} ({{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $phrebform->created_at)->diffforhumans()}}) </b> ? <br><br>

					<p class="form-group text text-danger">
						Note: Once the status of this form is <b>Submitted or Approved</b>, REC are not able to update and attach files. Thank you.
					</p>
				</label>

				<div class="modal-footer">
					<a href="{{route('ap_submittophreb', ['id' => encrypt($phrebform->id)])}}" class="btn btn-info">Submit</a>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
</div>
@endcan

@can('rec-access')
<div class="modal fade" id="attachDocuments" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-black">
				<h4 class="modal-title" id="myModalLabel">Attach Documents</h4>
			</div>
			<div class="modal-body">
			<form method="POST" action="{{route('phrebformAddFiles', ['id' => encrypt($phrebform->id)])}}" enctype="multipart/form-data">
				{{ csrf_field() }}

				<div class="form-group row">
					<div class="col-md-12">
						<select id="apFolderS2" class="form-control{{ $errors->has('apFolder') ? ' is-invalid' : '' }}" name="apFolder" required style="width: 100%">
							<option selected></option>
							@foreach($pluckFolderName as $id => $pfn)
								<option value="{{$pfn}}">{{$pfn}}</option>
							@endforeach
							@if ($errors->has('apFolder'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('apFolder') }}</strong>
								</span>
							@endif
						</select>
					</div>
				</div>

				<div class="form-group row">
					<div class="col-md-12">
						<input id="apFile" type="file" class="form-control{{ $errors->has('apFile') ? ' is-invalid' : '' }}" name="apFile[]" placeholder="Name of folder" required multiple="true" accept="application/pdf,docx,doc">
							@if ($errors->has('apFile'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('apFile') }}</strong>
								</span>
							@endif
					</div>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn bg-black" onclick="return confirm('Do you want to upload it?')">Upload</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
			</div>
		</div>
	</div>
</div>



@foreach($phrebform->phrebform_filefolders as $id => $pfff)
	<div class="modal fade" id="editFolder{{$pfff->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-black">
				<h4 class="modal-title" id="myModalLabel">Update Folder Name</h4>
			</div>
			<div class="modal-body">
			<form method="POST" action="{{route('updateFolder', ['id' => encrypt($pfff->id)])}}" enctype="multipart/form-data">
				{{ csrf_field() }}

				<div class="form-group row">
					<div class="col-md-12">
						<input id="apFolder" type="text" class="form-control{{ $errors->has('apFolder') ? ' is-invalid' : '' }}" name="apFolder" placeholder="Name of folder for your files" required value="{{$pfff->pfff_name}}">
							@if ($errors->has('apFolder'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('apFolder') }}</strong>
								</span>
							@endif
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn bg-black" onclick="return confirm('Do you want to save change in folder name?')">Save Changes</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
			</div>
		</div>
	</div>
</div>
@endforeach

@endcan

@foreach($history as $field => $value)
<!-- Modal -->
<div class="modal fade" id="before{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-full" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Revised by <b>{{ Auth::user()->can('rec-access') ? $value->user->role_users->roles->role_name : $value->user->name }}</b> at <u>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->created_at)->format('F j, Y g:i A')}}</u></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row" >
					<div class="col-md-12">

						<div class="form-group">

							<div class="container">
								<label><h5>Before > <small>Old Version</small></h5></label>
								
									<div class="form-control mceNonEditable"> 

										{!!implode($value->old_values)!!}

									</div>			 
							</div>
						
						</div>

					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="after{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-full" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Revised by <b>{{ $value->user->name }}</b> at <u>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->created_at)->format('F j, Y g:i A')}}</u></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row" >
					<div class="col-md-12">

						<div class="form-group">

							<div class="container">
								<label><h5>After > <small>New Version</small></h5></label>
								
									<div class="form-control mceNonEditable"> 

										{!!implode($value->new_values)!!}

									</div>			 
							</div>
						
						</div>

					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
@endforeach

<!-- End of modals -->

<script type="text/javascript">
	//for view-recdocuments.blade.php
	function toggle(source) {
	   var checkboxes = document.querySelectorAll('input[type="checkbox"]');
	    for (var i = 0; i < checkboxes.length; i++) {
	    	if (checkboxes[i] != source)
	    		checkboxes[i].checked = source.checked;
	    }
	}

</script>
@endsection
<style type="text/css">
	.modal-full {
		min-width: 100%;
		margin: 0;
	}
	.modal-full .modal-content {
		min-height: 100vh;
	}
</style>