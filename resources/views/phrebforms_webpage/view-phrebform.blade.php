@extends('layouts.myApp')
@section('content')

<div class="container-fluid">
	<div class="col-md-12" id="savedclass">
		@if(session('success'))
		<div class="alert alert-success ">
			<i class="fa fa-exclamation"></i>{{session('success')}}
			<button class="close" onclick="document.getElementById('savedclass').style.display='none'">x</button>
		</div>
		@endif

		@can('rec-access')
		@if($pf->pf_status == true)
		<div class="alert alert-danger ">
			<i class="fa fa-exclamation"></i>
			<label>Note: This is to remind you that you are not able to update this form once you have already submitted it to PHREB. Please contact administrator or the secretariat regarding for your changes. Thank You!</label>
		</div>
		@endif
		@endcan
	</div>

	<label class=""><b>{{$pf->phrebforms_template->pft_name}}</b> - {{$pf->recdetail->rec_name}} - {{$pf->recdetail->rec_institution}}</label>

	<form action="{{route('update-phrebforms', ['id' => $pf->id])}}" method="POST">
		 {{ csrf_field() }}

		<textarea class="form-control" id="editor1" name="editor1">
			<div class="{{ $pf->recdetails->users->id == auth::id() ? '' : 'mceNonEditable' }}"> 
				{{$pf->pf_body1}}
			</div>			 
		</textarea>

		<input name="image" type="file" id="upload" hidden onchange="">
		
		<div class="btn-group btn-block" role="group" aria-label="Basic example">
			@can('rec-access')
				<button type="submit" class="btn btn-success pull-right {{$pf->pf_status == true ? 'disabled' : ''}}"><span class="fa fa-save"></span> Save Changes</button>
			@endcan
				<a href="{{url('documents', ['id' => encrypt($pf->pf_recdetails_id)])}}" class="btn btn-secondary"><span class="fa fa-arrow-left"></span> Back</a>
			@can('secretariat-access')
				<a href="" class="btn btn-info" data-toggle="modal" data-target="#pfremarkscomments" data-backdrop="static" data-keyboard="false"><span class="fa fa-comments"></span> Remarks/Comments for this form</a>
			@endcan
		</div>
	</form>
</div><br>

<div class="modal fade" id="pfremarkscomments">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">     

    <h4 class="modal-title">PHREB Comments/Remarks Form</h4>
     </div>
     <div class="modal-body">
     	<div class="container">
     		<form action="{{route('updatepfcomments', $pf->id)}}" method="POST">
     			{{ csrf_field() }}
     		<textarea class="form-control" name="editor2" id="editor2">
     			{{$pf->pf_comments}}
     		</textarea>
     		<div class="modal-footer">
      			<button type="submit" class="btn btn-info">Update</button>
      			<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      		</div>
     		</form>
     	</div>
  	</div>

  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div>

@endsection