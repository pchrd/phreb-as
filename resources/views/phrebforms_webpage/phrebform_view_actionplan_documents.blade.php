@extends('layouts.myApp')
@section('content')

<div class="container-fluid">
	<div class="container">
		<div class="col-md-12" id="savedclass">
			
			@if(session('success'))
			<div class="alert alert-success ">
				<i class="fa fa-exclamation"></i>{{session('success')}}
				<button class="close" onclick="document.getElementById('savedclass').style.display='none'">x</button>
			</div>
			@endif
			
		</div>

		<label>
			
		</label>

		<div class="form-control" id="history">
			<div class="col-md-12 col-md-offset-1">
				<div class="panel panel-secondary">
					<div class="panel-heading">
						<h5>
							<strong>
								<span class="fa fa-folder"></span> 
								{{$actionplan_document->pfff_name}}<br>
								<span class="fa fa-user"></span> 
								{{$actionplan_document->users->name}}<br>
								<span class="fa fa-clock-o"></span> 
								<i class="">{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $actionplan_document->created_at)->format('F j, Y - G:i:A')}}</i>
							</strong>
						</h5>
					<div class="panel-body">
						<table class="table table-condensed table-hover" id="assessment_tbl">
							<thead class="badge-secondary">
								<tr>
									<td>No.</td>
									<td>File Name</td>
									<td>File Size</td>
									<td>Date Created</td>
									<td>Ago</td>
								</tr>
							</thead>
							<tbody>
								@foreach($actionplan_document->phrebform_files as $id => $pff)
									<tr>
										<td>{{$id + 1}}</td>
										<td>
											<a href="{{ route('downloadDocuments', ['id' => encrypt($pff->pf_files_uniqid)]) }}">
												<span class="fa fa-file"></span> {{$pff->pf_files_originalfilename}} </a>
										</td>
										<td>{{round( $pff->pf_files_originalfilesize / pow(1024, $pff->pf_files_originalfilesize > 0 ? floor(log($pff->pf_files_originalfilesize, 1024)) : 0) ).$units[$pff->pf_files_originalfilesize > 0 ? floor(log($pff->pf_files_originalfilesize, 1024)) : 0]}}</td>
										<td>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $pff->created_at)->format('F j, Y - G:i:A')}}</td>
										<td>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $pff->created_at)->diffforhumans()}}</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

		

	</div>
</div>

@endsection