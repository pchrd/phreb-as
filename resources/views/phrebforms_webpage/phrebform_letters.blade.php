@extends('layouts.myApp')
@section('content')

<div class="container-fluid">
	<div class="container">
		<div class="col-md-12" id="savedclass">
			
			@if(session('success'))
			<div class="alert alert-success ">
				<i class="fa fa-exclamation"></i>{{session('success')}}
				<button class="close" onclick="document.getElementById('savedclass').style.display='none'">x</button>
			</div>
			@endif
			
		</div>

		<label>
			{{$phrebformName->recdetails->rec_institution}} - {{$phrebformName->recdetails->rec_name}} {{$phrebformName->recdetails->applicationlevel->levels->level_name}} | Attached PHREB Letter of {{$phrebformName->phrebforms_template->pft_name}} 
		</label>

		<form action="{{route('save_a_letter',['pfID' => Request::get('pfID'), 'recID' => Request::get('recID')])}}" method="POST">
			{{ csrf_field() }}
			<input type="text" name="pfTemplate" hidden value="{{$pfTemplate->id}}">
			<textarea class="form-control" id="editor1" name="editor1">
				{{$body}}	 
			</textarea>
			<input name="image" type="file" id="upload" hidden onchange="">

			<div class="btn-group " role="group" aria-label="Basic example">
				<button type="submit" title="Save only" class="btn btn-success fa fa-save" onclick="return confirm('Do you want to save-only this letter?')"> Save Letter Only</button>
				<a href="{{route('view-assessment-form', encrypt($phrebformName->id)) }}" class="btn btn-secondary" title="Back to Assessment Matrix">Back</a>
			</div>
		</form><br>

	</div>
</div>


@endsection
