@extends('layouts.myApp')
@section('content')

<div class="container-fluid">
	<div class="container">
		<div class="col-md-12" id="savedclass">
			
			@if(session('success'))
			<div class="alert alert-success ">
				<i class="fa fa-exclamation"></i>{{session('success')}}
				<button class="close" onclick="document.getElementById('savedclass').style.display='none'">x</button>
			</div>
			@endif

			<label class="row"> 
					<b>
						{{$viewLetter->phrebforms->recdetails->rec_institution}} - {{$viewLetter->phrebforms->recdetails->rec_name}} {{$viewLetter->phrebforms->recdetails->applicationlevel->levels->level_name}}
						| {{$viewLetter->phrebforms_templates->pft_name}} 

						@if($viewLetter->pfal_status == null)
							<label class="badge badge-danger blink_me">Waiting to Submit</label> 
						@else
							<label class="badge badge-success blink_me" title="{{$viewLetter->pfal_status}}">Submitted</label>
						@endif

						<br>
						Created by <u>{{$viewLetter->users->name}}</u> at {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $viewLetter->created_at)->format('F j, Y g:i A')}} | Updated at <i>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $viewLetter->updated_at)->format('F j, Y g:i A')}}</i>
					</b>
			</label>
			
		</div>

		<form action="{{route('update_a_letter', ['id' => encrypt($viewLetter->id)])}}" method="POST">
			{{ csrf_field() }}
			<textarea class="form-control" id="editor1" name="editor1">
		
				{{$viewLetter->pfal_body}}

			</textarea>
			<input name="image" type="file" id="upload" hidden onchange="">

			<div class="btn-group " role="group" aria-label="Basic example">
				<button type="submit" title="Save only" class="btn btn-success fa fa-save" onclick="return confirm('Do you want to Save this Letter?')"> Save Changes </button>
				<a href="#" data-toggle="modal" data-target="#ass_sendtorec" class="btn btn-info" title="Send to REC now via email"><span class="fa fa-send"></span> Send to REC</a>
				<a href="{{route('view-assessment-form', ['id' => encrypt($viewLetter->phrebforms->id)])}}" class="btn btn-secondary" title="Back to Assessment Matrix">Back</a>
			</div>
		</form><br>

	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="ass_sendtorec" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-full" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Send to {{$viewLetter->phrebforms->recdetails->rec_institution}} <br> {{$viewLetter->phrebforms->recdetails->rec_name}} {{$viewLetter->phrebforms->recdetails->applicationlevel->levels->level_name}}
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row" >
					<div class="col-md-12">

						<text class="small"></text>

						<form action="{{route('sendtorec_a_letter', ['id' => encrypt($viewLetter->id)])}}" method="POST">
							{{ csrf_field() }}
							
							<textarea class="form-control"  name="lettermessage" placeholder="Write some messages here...">
								
							</textarea>

							<i><label>To: <u>{{$viewLetter->phrebforms->recdetails->users->email}}, {{$viewLetter->phrebforms->recdetails->rec_email}}, {{$viewLetter->phrebforms->recdetails->rec_contactemail}}</u></label></i>


							<div class="modal-footer">
								<button class="btn btn-info" onclick="return confirm('Do you want to send this via email?')">Send</button>
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							</div>

						</form>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>


@endsection
