@extends('layouts.myApp')
@section('content')

<div class="container-fluid">
	<div class="container">
		<div class="col-md-12" id="savedclass">

			@if(session('success'))
				<div class="alert alert-success ">
					<i class="fa fa-exclamation"></i>{{session('success')}}
					<button class="close" onclick="document.getElementById('savedclass').style.display='none'">x</button>
				</div>
			@endif

			@if (session('error'))
				<div class="alert alert-danger">{{ session('error') }}</div>
			@endif
		</div>

		<div class="form-group col-md-12 col-md-offset-1">

			<label>
				
					<h3>
						PHREB Remarks/Comments
					</h3>

					<p>
						Date Created: <b>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $view->created_at)->format('F j, Y - G:i:A')}}</b><br>
						Date Updated: <b>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $view->updated_at)->format('F j, Y - G:i:A')}}</b><br>
						Created by: <b>{{$view->users->name}}</b>
					</p>
		
				
			</label>

			<form action="{{ route('updatephrebformRemarks', ['id' => $view->id]) }}" method="POST">
				{{ csrf_field() }}
				<textarea class="form-control" id="editor1" name="editor1">
					
					<div class="{{Auth::user()->can('rec-access') ? 'mceNonEditable' : ''}}"> 
						{{$view->pf_remarks_body}}
					</div>
								 
				</textarea>
				<input name="image" type="file" id="upload" hidden onchange="">

				@can('accreditors_and_csachair_secretariat')
					<div class="modal-footer">
						<button type="submit" class="btn btn-success">Save</button>
						<a href="{{route('viewActionplan', ['id' => encrypt($view->pf_remarks_pfid)])}}" class="btn btn-default "> Back</a>
					</div>
				@endcan

			</form>

		</div>

	</div>
</div>
@endsection