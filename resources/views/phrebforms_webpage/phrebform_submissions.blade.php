@extends('layouts.myApp')

@section('content')

<div class="container">

	<div class="col-md-12" id="verifyuser">
		@if(session('success'))
		<div class="alert alert-success">
			{{session('success')}}
			<button class="close" onclick="document.getElementById('verifyuser').style.display='none'">x</button>
		</div>
		@endif
	</div> 

		<div class="row">
			<div class="col-md-12 col-md-offset-1">
				<div class="panel panel-info">
					<div class="panel-heading"><h3><strong>PHREB Form Submissions<small></small></strong></h3></div>
					<div class="panel-body">
					<table class="table table-hover small" id="recdetails_table">
							<thead>
								<tr>
									<td>ID</td>
									<td>PHREB FORM</td>
									<td>INSTITUTION</td>
									<td>REC NAME</td>
									<td>ACCREDITORS</td>
									<td>SENDER</td>
									<td>ACTIONS</td>
								</tr>
							</thead>
					</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	
<script type="text/javascript">

	$(function(){
		// alert(moment().add(1, 'M').format('YYYY-MM-DD'))
		var oTable = $('#recdetails_table').DataTable({

			responsive: true,
			
			dom: '<"fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix ui-corner-tl ui-corner-tr"HlfrB>'+
				't'+'<"fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix ui-corner-bl ui-corner-br"ip>',

        	buttons:[
        		// ['copy', 'csv', 'excel', 'pdf', 'print'],
        		{  
       				extend: 'colvis',
		          
       			},

        		{ 
        		   extend: 'excel',
		           footer: true,
		           exportOptions: {
		                columns: []
		           }
       			},

       			{  
       				extend: 'copy',
		          
       			},

       			{  
       				extend: 'csv',
		          
       			},

       			{  
       				extend: 'pdf',
		          
       			},
       			{  
       				extend: 'print',
       				message: "REC Records",

       				customize: function (win) {
                    $(win.document.body)
                        .css('font-size', '10pt')
                        .prepend(
                            '<img src="{{asset('image/image002.jpg')}}" style="position:static; top:10px; left:10px;" />'
                        );

                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                	}
       			}
			       
      		 ],

      		stateSave: true,
			processing: true,
			serverSide: true,
			ajax: {
				url: '{{ route('phrebforms-submission-ajax') }}',
			},
      		
			columns: [
			{ data: 'id',   name: 'id', searchable:true},
			{ data: 'phrebforms',   name: 'phrebforms', searchable:true},
			{ data: 'recdetails.rec_institution',   name: 'recdetails.rec_institution', searchable:true},
			{ data: 'recdetails.rec_name',   name: 'recdetails.rec_name', searchable:true},
			{ data: 'accreditors',   name: 'accreditors', searchable:true},
			{ data: 'sender.name',   name: 'sender.name', searchable:true},
			{ data: 'action', name:'action', orderable: false, searchable: false}
			],
		});
	});
</script>
@endsection