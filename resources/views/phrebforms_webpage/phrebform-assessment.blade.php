@extends('layouts.myApp')
@section('content')

<div class="container-fluid">
	<div class="container">
		<div class="col-md-12" id="savedclass">
			
			@if(session('success'))
			<div class="alert alert-success ">
				<i class="fa fa-exclamation"></i>{{session('success')}}
				<button class="close" onclick="document.getElementById('savedclass').style.display='none'">x</button>
			</div>
			@endif

			@if (session('error'))
				<div class="alert alert-danger">{{ session('error') }}</div>
			@endif
		</div>

		<div class="form-group">

			<label>
				<b>	
					<h3>{{$id->phrebforms_template->pft_name}}</h3>
					{{$id->recdetails->rec_institution}} - {{$id->recdetails->rec_name}} {{$id->recdetails->applicationlevel->levels->level_name}}
					

					 @can('accreditors_and_csachair_secretariat')
					 	<br>
					 	Date Created: 
					 		<u class="label label-danger">{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $id->created_at)->format('F j, Y g:i:A')}}</u>
					 	<br>
					 	Author:
					 		<u>{{$id->users->name}}</u>
					 @endcan

					 @can('rec-access')
					 	<br>
					 	Date Submitted: 
					 		<u class="label label-danger">{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $id->phrebform_assessment_recs->created_at)->format('F j, Y g:i:A')}}</u>
					 	<br>
					 	Submitted by: <u class="label label-danger">{{$id->phrebform_assessment_recs->sentby->name}} ({{$id->phrebform_assessment_recs->sentby->role_users->roles->role_name}})</u>
					 @endcan

				</b>
			</label>

			@can('accreditors_and_csachair_secretariat')
				<a href="#history" class="float-right">History</a>
			@endcan

			@can('rec-access')
				<a href="{{route('documents', ['id' => encrypt($id->recdetails->id)])}}" class="pull-right btn btn-outline-default fa fa-arrow-left"> Back</a>
			@endcan

		</div>
		

		@can('accreditors_and_csachair_secretariat')
			@if($validateAssessmentRec == true)
				<div class="alert alert-info">
					<label>Note: This form was already submitted to REC at <b>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $id->phrebform_assessment_recs->created_at)->format('F j, Y - G:i:A')}}</b>. Changes are no longer available in this page.</label>
		 	  </div>
			@else
				<div class="alert alert-danger"><center><h3>NOT YET SUBMITTED TO REC</h3></center></div>
			@endif
		@endcan

		<form action="{{route('update-assessment-form', ['id' => $id->id])}}" method="POST">
			{{ csrf_field() }}
			<textarea class="form-control" id="editor1" name="editor1">
				<div class="{{Auth::user()->can('rec-access') ? 'mceNonEditable' : ($validateAssessmentRec == true ? 'mceNonEditable' : '')}}"> 
					{{$id->pf_body1}}
				</div>			 
			</textarea>
			<input name="image" type="file" id="upload" hidden onchange="">

			@can('accreditors_and_csachair_secretariat')
			<div class="btn-group" role="group" aria-label="Basic example">
				<button type="submit" title="Delete" class="btn btn-success ">Save Changes</button>

				@can('accreditors_and_secretariat')
				<a href="#" data-toggle="modal" data-target="#assessmentsendtocsa" class="btn btn-info pull-right">Submit to CSA Chair</a>
				@endcan

				<a href="{{url('documents', ['id' => encrypt($id->pf_recdetails_id)])}}" class="btn btn-secondary">Back</a>
			</div>
			@endcan

		</form><br>

		@can('rec-access')
		<div class="modal fade" id="createAPCE" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog sm" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Action Plan & Compliance Evidences</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">

						<div class="container">
							<label class="text text-default">
								Do you want to create Action Plan Template Form 4.3 for {{$id->recdetails->rec_institution}} - {{$id->recdetails->rec_name}} {{$id->recdetails->applicationlevel->levels->level_name}}
					
								{{$id->phrebforms_template->pft_name}}
							</label>

							<div class="modal-footer">
								<a href="{{route('makeActionplan', ['id' => encrypt($id->phrebform_assessment_recs->id)])}}" class="btn btn-info">Yes</a>
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		@endcan

		@can('accreditors_and_csachair_secretariat')
		<div class="form-control" id="history">
			<div class="col-md-12 col-md-offset-1">
				<div class="panel panel-info">
					<div class="panel-heading">
						<h3>
							<strong>Attached Letter</strong>

							@can('csachair_and_secretariat')
								<a href="#" data-toggle="modal" data-target="#assessmentletterrec" class="btn btn-primary pull-right fa fa-file"> Create Letter</a>
							@endcan
						</h3>
					</div>
					<div class="panel-body">
						<table class="table table-condensed table-hover" id="assessment_tbl">
							<thead class="badge-default">
								<tr>
									<td scope="col">Name & Descriptions</td>
									<td scope="col">Date Created</td>
									<td>Last Date Updated</td>
								</tr>
							</thead>
							<tbody>
								@foreach($attachedLetter as $al)
								<tr>
									<td>
										<span class="fa fa-file"></span>
										<a href="{{route('view_a_letter', ['id' => encrypt($al->id)])}}"> {{$al->phrebforms_templates->pft_name}}</a>

											@if($al->pfal_status == null)
												<label class="badge badge-danger">Waiting to submit</label>
											@else
												<label class="badge badge-success" title="{{$al->pfal_status}}">Submitted</label>
											@endif

										<br>
										For {{$al->phrebforms->recdetails->rec_institution}} - {{$al->phrebforms->recdetails->rec_name}} {{$al->phrebforms->recdetails->applicationlevel->levels->level_name}}<br>
										<small>Created by {{$al->users->name}}</small>
									</td>
									<td>
										{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $al->created_at)->format('F j, Y g:i A')}}<br>
										<small class="text text-danger">{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $al->created_at)->diffForHumans()}}</small>
									</td>
									<td>
										{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $al->updated_at)->format('F j, Y g:i A')}}<br>
										<small class="text text-danger">{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $al->updated_at)->diffForHumans()}}</small>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div> 
				</div>
				<a href="#" data-toggle="modal" data-target="#assessmenttorec" class="btn btn-info btn-block fa fa-send"> Send to REC</a>
			</div>
		</div>

		<div class="form-control" id="history">
			<div class="col-md-12 col-md-offset-1">
				<div class="panel">
					<div class="panel-heading"><h5><strong>Save Changes Logs (History)</strong></h5></div>
					<div class="panel-body">
						<table class="table table-condensed table-hover small" id="assessment_tbl">
							<thead class="badge-default">
								<tr>
									<td scope="col">Revision By</td>
									<td scope="col">Last Date Updated</td>
									<td scope="col">Before</td>
									<td scope="col">After</td>
								</tr>
							</thead>
							<tbody>
								@foreach($history as $field => $value)
								<tr>
									<td>
										<span class="fa fa-user"></span> {{$value->user->name}}<br>
										<small>{{$value->user->role_users->roles->role_name}}</small>
									</td>
									<td>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->created_at)->format('F j, Y g:i A')}}<br>
										<small class="text text-danger">{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->created_at)->diffForHumans()}}</small>
									</td>
									<td>
										<a href="#" data-toggle="modal" data-target="#before{{$value->id}}">
											<span class="fa fa-file"></span>
										</a>
									</td>
									<td>
										<a href="#" data-toggle="modal" data-target="#after{{$value->id}}">
											<span class="fa fa-file"></span>
										</a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
						<div class="footer">
							{{$history->links()}}
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>
@endcan


				@can('rec-access')
					<div class="form-group">
						<a href="#" data-toggle="modal" data-target="#createAPCE" class="btn btn-info fa fa-file btn-block"> Create Action Plan Template Form 4.3</a>
					</div>
				@endcan

<div class="modal fade" id="assessmenttorec" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Send Confirmation</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

				<div class="container">
					<label class="text text-default">Do you want to send {{$id->phrebforms_template->pft_name}} to <b> {{$id->recdetails->rec_institution}}-{{$id->recdetails->rec_name}} {{$id->recdetails->applicationlevel->levels->level_name}}? </b></label>

					<div class="modal-footer">
							<a href="{{route('sendassessmenttorec', ['pfID' => $id->id, 'recID' => $id->recdetails->id])}}" class="btn btn-info">Yes</a>
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- Modal -->
<div class="modal fade" id="assessmentletterrec" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Create Assessment Letter for REC</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

				<div class="container">

					<label class="text text-default">Do you want to create a letter for {{$id->phrebforms_template->pft_name}} and sent it to <b> {{$id->recdetails->rec_institution}}-{{$id->recdetails->rec_name}}? </b></label>

					<div class="modal-footer">
							<a href="{{route('create_a_letter', ['pfID' => encrypt($id->id), 'recID' => encrypt($id->recdetails->id)])}}" class="btn btn-primary">Yes, Create a Letter</a>
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="assessmentsendtocsa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Send to CSA Chair</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

				<!-- choose csa chair -->
				<div class="container">

					<form action="{{route('acc_assessment_submittocsachair', ['id' => $id->id])}}" method="POST">
						@csrf
						<div class="col-md-12">
							<input type="text" name="getAccreditorsAssessmentURL" value="{{Request::fullUrl()}}" hidden>
							<select id="ra_csachair_id" type="text" class="form-control{{ $errors->has('ra_csachair_id') ? ' is-invalid' : '' }}" name="ra_csachair_id" required>
								<option selected disabled="">--- Please Choose CSA Chair ---</option>
								<!-- get the data in db and put in select -->
								@foreach($selectCSAChair as $id => $selectCSAChairs)
								<option value="{{$id}}">{{$selectCSAChairs}}</option>
								@endforeach
							</select>

							@if ($errors->has('recdocuments_name'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('recdocuments_name') }}</strong>
							</span>
							@endif
						</div><br>
						<div class="modal-footer">
							<button type="submit" class="btn btn-info" onlick="return foo();">Submit</button>
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>
			
			
		</div>
	</div>
</div>


@foreach($history as $field => $value)
<!-- Modal -->
<div class="modal fade" id="before{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-full" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Revised by <b>{{ $value->user->name }}</b> at <u>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->created_at)->format('F j, Y g:i A')}}</u></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row" >
					<div class="col-md-12">

						<div class="form-group">

							<div class="container">

								<label><h5>Before > <small>Old Version</small></h5></label>
								
									<div class="form-control mceNonEditable"> 

										{!!implode($value->old_values)!!}

									</div>			 
								
							</div>
						
						</div>

					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="after{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-full" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Revised by <b>{{ $value->user->name }}</b> at <u>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->created_at)->format('F j, Y g:i A')}}</u></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row" >
					<div class="col-md-12">

						<div class="form-group">

							<div class="container">

								<label><h5>After > <small>New Version</small></h5></label>
								
									<div class="form-control mceNonEditable"> 

										{!!implode($value->new_values)!!}

									</div>			 
								
							</div>
						
						</div>

					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
@endforeach


@can('rec_secretariat_accreditors_csachair')
<div class="container">
<div class="form-control">
	<div class="col-md-12 col-md-offset-1">
		<div class="panel panel-info">
			<div class="panel-heading"><h3><strong>Attached Action Plan Template Form</strong></h3></div>
			<div class="panel-body">
				<table class="table table-condensed table-hover" id="assessment_tbl">
					<thead class="badge-default">
						<tr>
							<td>No.</td>
							<td>PHREB Form Name</td>
							<td>Date Created</td>
							<td>Last Date Updated</td>
							<td>Statuses</td>
						</tr>
					</thead>
					<tbody>

						@foreach($getAttachedActionPlan as $id => $gaap)
							<tr>
								<td>{{$id + 1}}</td>
								<td><a href="{{route('viewActionplan', ['id' => encrypt($gaap->ap_pfap_id)])}}" target="_blank"><span class="fa fa-file"></span> {{$gaap->phrebforms_templates->pft_name}}</a></td>
								<td>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $gaap->created_at)->format('F j, Y - G:i:A')}} <br>
									<small class="text text-danger">{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $gaap->phrebforms->created_at)->diffForHumans()}}</small>
								</td>
								<td>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $gaap->updated_at)->format('F j, Y - G:i:A')}} <br>
									<small class="text text-danger">{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $gaap->phrebforms->updated_at)->diffForHumans()}}</small>
								</td>
								<td>
									
									@if($gaap->phrebforms->phrebform_actionplanfromrecstatuses()->exists())

										@foreach($gaap->phrebforms->phrebform_actionplanfromrecstatuses as $statuses)
										<p>
											@if($loop->last)
											<label class="fa fa-arrow-right small">
												<mark>
													<b title="By {{Auth::user()->can('rec-access') ? $statuses->users->role_users->roles->role_name : $statuses->users->name}}  at {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $statuses->created_at)->format('F j, Y - G:i:A')}}">{{$statuses->statuses->status_name}} 
														<small>
															({{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $statuses->created_at)->diffforhumans()}})
														</small>
													</b>
												</mark>
											</label>
											@else
											<label class="small">
												<strike>
													<b title="By {{Auth::user()->can('rec-access') ? $statuses->users->role_users->roles->role_name : $statuses->users->name}} ({{$statuses->users->role_users->roles->role_name}}) at {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $statuses->created_at)->format('F j, Y - G:i:A')}}">{{$statuses->statuses->status_name}} 
														<small>
															({{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $statuses->created_at)->diffforhumans()}})
														</small>
													</b>
												</strike>
											</label>
											@endif

										</p>
										@endforeach

									@else
										<b class="badge badge-danger"> Not submitted </b>

									@endif

								</td>
							</tr>
						@endforeach

					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
</div>
@endcan

@endsection
<style type="text/css">
	.modal-full {
		min-width: 100%;
		margin: 0;
	}
	.modal-full .modal-content {
		min-height: 100vh;
	}
</style>