@extends('layouts.myApp')

@section('content')
<div class="container col-md-8" style="margin-top: 20px;">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header navbar-dark bg-primary"><h6><!-- <a href="" class="text text-light fa fa-google">oogle Sign-in</a> --><a class="text text-light fa fa-arrow-left" href="{{url('/')}}"> Back</a> <a href="{{url('/login')}}" class="float-right text text-light">Login </a></h6></div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                        @csrf
                        <center><a href="{{url('/')}}"><img src="{{ asset('brandlogo.png') }}" style="padding-bottom: 10px; margin-bottom: 10px; height: 150px; width: 150px"></a></center>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name of Contact Person:') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <!-- added name of rec and applying for level -->

                        <div class="form-group row">
                            <label for="recname" class="col-md-4 col-form-label text-md-right">{{ __('REC Name:') }}</label>

                            <div class="col-md-6">
                                <input id="recname" type="text" class="form-control" name="recname" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="applyingforlevel" class="col-md-4 col-form-label text-md-right">{{ __('Applying for level:') }}</label>

                            <div class="col-md-6">
                                <select class="form-control" name="applyingforlevel" required="">
                                    <option value="1">Level 1</option>
                                    <option value="2">Level 2</option>
                                    <option value="3">Level 3</option>
                                </select>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-mail Address:') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password:') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password:') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>


                         <div class="form-group row">
                            <label for="messagetosecretariat" class="col-md-4 col-form-label text-md-right">{{ __('Messages (Optional):') }}</label>

                            <div class="col-md-6">
                               
                                <textarea class="form-control" id="messagetosecretariat" name="messagetosecretariat" rows="4" cols="50">
                                    
                                </textarea>
                            </div>
                        </div>




                         <div class="form-group row{{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
                            <label class="col-md-4 col-form-label text-md-right">Captcha:</label>

                            <div class="col-md-6 pull-center">
                                {!! app('captcha')->display() !!}

                                @if ($errors->has('g-recaptcha-response'))
                                    <span class="help-block">
                                        <strong class="text text-danger">{{ $errors->first('g-recaptcha-response') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-outline-primary">
                                    {{ __('Submit') }}
                                </button>

                                 <button type="reset" class="btn btn-outline-secondary">
                                    {{ __('Reset') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection