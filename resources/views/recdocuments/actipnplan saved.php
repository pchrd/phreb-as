<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>CKEditor</title>
        <script src="https://cdn.ckeditor.com/4.7.1/standard/ckeditor.js"></script>
    </head>
    <body>
        <textarea name="editor1">
            sss

        </textarea>
        <script>
            CKEDITOR.replace( 'editor1' );
        </script>
    </body>
</html>

<!-- ------------------------------------------ -->


@foreach($showactionplan as $sap)
<!-- modal forward to CSA -->
<div class="modal fade" id="forwardtocsa{{$sap->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header badge-info">
				<h4 class="modal-title" id="myModalLabel">Forward to CSA Chair</h4>
			</div>
			<div class="modal-body">
			<form method="POST" action="{{route('ap_sendtocsachair')}}">
				{{ csrf_field() }}
				<h6>Do you want to forward/submit this REC <b>{{$sap->rec_name}}</b> 's Action Plan and Compliance Evidences to CSA Chair ?</h6>
				<input type="hidden" name="ap_csa_recdetails_id" value="{{$sap->ap_recdetails_id}}">
				<input type="hidden" value="{{$sap->id}}" name="sap_id">
				<select id="ap_csachair_id" type="text" class="form-control{{ $errors->has('ap_csachair_id') ? ' is-invalid' : '' }}" name="ap_csa_csachair_id" required><option disabled>---Please Choose CSA Chair---</option>
							<!-- get the data in db and put in select -->
							@foreach($selectcsachair as $id => $selectcsachairs)
							<option value="{{$id}}">{{ $selectcsachairs }}</option>
							@endforeach		
				</select>

				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Forward</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
			</div>
		</div>
	</div>
</div>
@endforeach


@foreach($showactionplan_eval as $sap)
<!-- modal forward to CSA -->
<div class="modal fade" id="forwardtorec{{$sap->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header badge-info">
				<h4 class="modal-title" id="myModalLabel">Forward to Research Ethics Committee </h4>
			</div>
			<div class="modal-body">
			<form method="POST" action="{{route('ap_sendtorec')}}">
				{{ csrf_field() }}
				<h6>Do you want to forward/submit this REC to <b>{{$sap->rec_name}} via email?</b>?</h6>
				<input type="hidden" name="ap_csa_recdetails_id" value="{{$sap->ap_recdetails_id}}">
				<input type="hidden" value="{{$sap->id}}" name="sap_id">
				<input type="hidden" value="{{$sap->ap_sender_id}}" name="ap_sender_id">

				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Forward</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
			</div>
		</div>
	</div>
</div>
@endforeach


@foreach($showactionplan_eval as $sap)
<!-- modal forward to CSA -->
<div class="modal fade" id="forwardtocsa{{$sap->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header badge-info">
				<h4 class="modal-title" id="myModalLabel">Forward to CSA Chair</h4>
			</div>
			<div class="modal-body">
			<form method="POST" action="{{route('ap_sendtocsachair')}}">
				{{ csrf_field() }}
				<h6>Do you want to forward/submit this REC <b>{{$sap->rec_name}}</b> 's Action Plan and Compliance Evidences to CSA Chair ?</h6>
				<input type="hidden" name="ap_csa_recdetails_id" value="{{$sap->ap_recdetails_id}}">
				<input type="hidden" value="{{$sap->id}}" name="sap_id">
				<select id="ap_csachair_id" type="text" class="form-control{{ $errors->has('ap_csachair_id') ? ' is-invalid' : '' }}" name="ap_csa_csachair_id" required><option disabled>---Please Choose CSA Chair---</option>
							<!-- get the data in db and put in select -->
							@foreach($selectcsachair as $id => $selectcsachairs)
							<option value="{{$id}}">{{ $selectcsachairs }}</option>
							@endforeach		
				</select>

				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Forward</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
			</div>
		</div>
	</div>
</div>
@endforeach







<!-- /////////////////////////////////sa documents ito
 -->

@foreach($showactionplan as $sap)
							@if($sap->role_id == 5)<tr style="background-color: lightblue;">@endif
								<td>{{$sap->id}}</td>
								
								<td><a href="../storage/actionplansandcomplianceevidences/{{$sap->name}}/{{$sap->ap_recdetails_id}}/{{$sap->ap_recactionplan_uniqid}}.pdf" target="_blank" title="Download"><b>{{$sap->ap_recactionplan_name}}</b></a><br>
									@can('rec-access')
									<small>{{$sap->rec_name}}</small>
									@endcan
									@can('accreditors_and_csachair_secretariat')
									<small>{{$sap->name}}</small>
									@endcan
									<small>| <i>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $sap->created_at)->format('M j, Y')}} | {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $sap->created_at)->diffForHumans()}}</i></small>
								</td>
								@can('secretariat-access')
								<td><a href="#" data-toggle="modal" data-target="#forwardtocsa{{$sap->id}}" class="btn btn-outline-warning"><span class="fa fa-send"></span> Forward to CSA Chair</a></td>
								
								@if($sap->role_id == 5)
								<td><a href="#" data-toggle="modal" data-target="#forwardtorec{{$sap->id}}" class="btn btn-outline-warning"><span class="fa fa-send"></span> Forward to REC</a></td>
								@endif
								
								@endcan
							</tr>
							@endforeach  

