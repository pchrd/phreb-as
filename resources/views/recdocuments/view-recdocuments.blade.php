@extends('layouts.myApp')
@section('content')

<div class="btn-group-vertical pull-right" style="position: fixed;bottom: 88px;
	right: 3px; width:200px; margin-right:30px; padding: 10px 20px;border-radius: 4px;border-color: #46b8da">

	<a href="{{ url('documents', ['id' => encrypt($recdocuments->recdocuments_recdetails_id)]) }}" class="btn btn-secondary btn-sm" title="Back"><span class="fa fa-arrow-circle-o-left"></span> Go Back</a>

	</div>

<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-10">
			
			<div class="card-header col-md-12">
				<h2 class="text-muted"><span class="fa fa-folder"></span>
					<strong> {{$recdocuments->documenttypes->document_types_name}}</strong> 

					@can('rec-access')

						@if($recdocuments->recdetails->applicationlevel->recdetails_submission_status == 0 OR $recdocuments->submissionstatuses == true AND $checkifsubmitted)
							
							@if($recdocuments->documenttypes->recdetails_id != null)
								<a href="#" class="small" title="Rename Folder" data-toggle="modal" data-target="#renamefolder"><span class="fa fa-edit"></span></a>
							@endif

						@endif

					@endcan
					
				</h2>
				<div class="float-right">

					<!-- @can('accreditors_and_secretariat')
						<a href="" data-toggle="modal" data-target="#add_remarks" class="btn btn-primary btn-sm"><span class="fa fa-comments-o"></span> Add Remarks/Comments</a>
					@endcan
					@can('csachair-access')
						<a href="" data-toggle="modal" data-target="#add_remarks" class="btn btn-primary btn-sm"><span class="fa fa-comments-o"></span> Add Remarks/Comments</a>
					@endcan -->
				</div>
			</div><br>

			<div class="col-md-12" id="savedclass">
				@if(session('success'))
				<div class="alert alert-info">
					{{session('success')}}
					<button class="close" onclick="document.getElementById('savedclass').style.display='none'">x</button>
				</div>
				@endif
			</div>
			
			@if($recdocuments->recdetails->applicationlevel->recdetails_submission_status == 0 OR $recdocuments->submissionstatuses == true AND $checkifsubmitted)

				<div class="container-fluid btn-group" role="group" aria-label="Basic example">
						<div class="btn btn-block bg bg-default block">
							<span class="fa fa-upload"></span> Add files here...</b>
					
							<form action="{{route('addtionalfiles', ['recdetail_id' => encrypt($recdocuments->recdocuments_recdetails_id), 'document_types_id' => encrypt($recdocuments->recdocuments_document_types_id)])}}" method="POST" enctype="multipart/form-data">
								
								{{ csrf_field() }}

								<input id="recdocuments_file" type="file" class="" name="recdocuments_file[]" multiple="true" accept="application/jpg,jpeg,png,pdf,docx,doc,zip," required="">

								@if($recdocuments->recdetails->applicationlevel->recdetails_submission_status != 0)
									<input type="hidden" name="submissionstatuses_id" value="{{$checkifsubmitted->recdocuments_submission_statuses_id}}">
								@endif 

								<input type="submit" class="btn btn-info" value="Upload">

								<p class="text text-danger"><small>You can upload multiple-files here with pdf, docx/doc format only </small></p>
							</form>
						
						</div>
					</div> <br>
			<br>

			@endif

			<div class="container">
				<form action="{{route('downloadZipRecDoc')}}" method="get">
					<table class="table table-hover table-condensed table-bordered" id="recdetails_table">
						<thead class="small">
							<tr>
								<th><center><input type="checkbox" class="cbox" onclick="toggle(this);" title="Select all"/></center></th>
								<th>File Name</th>
								<th>File Size</th>
								<th>Date Uploaded</th>
								<!-- <th>Upload by</th> -->

								@can('rec-access')
									
									<th>Action</th>
									
								@endcan

							</tr>
						</thead>
						<tbody>
							@foreach($recdocuments_files as $recdocument)
							<tr>
								<td><center><input type="checkbox" class="cbox" name="download_check[]" value="{{$recdocument->id}}"></center></td>
								<td>
									
									<!-- <a href="../../../storage/documents/{{$recdocument->users->id}}/{{$recdocument->recdocuments_recdetails_id}}/{{$recdocument->recdocuments_file_generated}}-{{$recdocument->recdocuments_file}}" target="_blank" class="text text-secondary fa fa-file"> {{$recdocument->recdocuments_file}}
									</a> -->

									<a href="{{ route('downloadfile', ['id' => encrypt($recdocument->id)]) }}" target="_blank" class="text text-secondary fa fa-file" title="Uploaded by {{$recdocument->users->name}}"> {{$recdocument->recdocuments_file}}
									</a>

									@can('rec-access')
										
										@if($recdocuments_remarks->count() == 0)

											@if(

												$recdocument->recdetails->applicationlevel->recdetails_resubmitreq == 0 AND 
												$recdocument->recdetails->applicationlevel->statuses_id == 1 OR 
												$recdocument->recdetails->applicationlevel->statuses_id == 9 OR
												$recdocument->recdetails->applicationlevel->statuses_id == 3 

											)
										
											@endif

										@endif
									@endcan

								</td>
								<td>{{round( $recdocument->recdocuments_filesize / pow(1024, $recdocument->recdocuments_filesize > 0 ? floor(log($recdocument->recdocuments_filesize, 1024)) : 0) ).$units[$recdocument->recdocuments_filesize > 0 ? floor(log($recdocument->recdocuments_filesize, 1024)) : 0]}}</td>
								<td>
									{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $recdocument->rec_dateuploaded)->format('M j, Y') }}
									<br><small>({{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $recdocument->rec_dateuploaded)->diffForHumans() }})</small>
								</td>
								


								@can('rec-access')

										<td>
											@if($recdocuments->recdocuments_submittophreb == null)
												<a href="{{route('removerecdocument', ['id' => $recdocument->id])}}" class="btn btn-outline-danger btn-sm fa fa-remove" onclick="return confirm('Do you want to remove this file {{$recdocument->recdocuments_file}} | {{$recdocument->rec_dateuploaded}}?')" title="Remove File">
													 			
												</a>
											
											@else

												<p>---</p>
											@endif
										</td>
								
								@endcan


							</tr>
							@endforeach
						</tbody>
					</table>
					<input type="submit" id="bt_submit" class="form-group btn btn-sm bg-yellow fa fa-zip-o" value="Download Zip">
				</form>
			<!-- 	<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="blog-comment"><br>
								<b><span class="text text-success">Comments</span></b>
								<hr/>
								<ul class="comments">
									@foreach($recdocuments_remarks as $rr)			
									<li class="clearfix">
										<img src="http://www.healthresearch.ph/images/image-icon/PHREB-large.png" class="avatar" alt="">
										<div class="post-comments">
											<p class="meta"><span class="fa fa-clock-o"></span> {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $rr->created_at )->format('M j, Y') }} <a href="#">| {{ $rr->name }}</a> says : <i class="pull-right"><a href="#">{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $rr->created_at )->diffForHumans()}}</a></i></p>
											<p>{{ $rr->remarks_message_recdocuments }}</p>
										</div>
									</li>
									@endforeach
								</ul>
							</div>
						</div>
					</div>
				</div> -->


			</div>
		</div>
	</div>
</div>

<!-- modal for add documents per recdetails id -->
<div class="modal fade bd-example-modal-lg" id="add_remarks" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header badge-primary">
				<h5 class="modal-title" id="exampleModalLabel"><span class="fa fa-comments-o"></span> Remarks and Comments</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form method="POST" action="{{ route('myfunctions.remarks_documents') }}" enctype="multipart/form-data">
					{{ csrf_field() }}
					<input type="hidden" name="remarks_recdetails_id" value="{{decrypt($id)}}">
					<input type="hidden" name="remarks_recdocuments_types_id" value="{{$recdocuments_document_types_id}}">
					<!--Messages-->
					<div class="form-group row">
						<label for="remarks_message_recdocuments" class="col-md-4 col-form-label text-md-left">{{ __('Write Your Messages Here:') }}</label>

						<div class="col-md-8">
							<textarea class="form-control" name="remarks_message_recdocuments" id="remarks" required></textarea>
							@if ($errors->has('remarks_message_recdocuments'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('remarks_message_recdocuments') }}</strong>
							</span>
							@endif
						</div>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-primary" value="Submit">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- modal  for folder rename -->
<div class="modal fade bd-example-modal-lg" id="renamefolder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header badge-primary">
				<h5 class="modal-title" id="exampleModalLabel"><span class="fa fa-edit"></span> Rename Folder </h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form method="GET" action="{{ route('renamefolder', ['id' => encrypt($recdocuments_document_types_id)]) }}" enctype="multipart/form-data">
					{{ csrf_field() }}
					<input type="hidden" name="recdetails_id" value="{{decrypt($id)}}">
					<input type="hidden" name="folder_id" value="{{encrypt($recdocuments_document_types_id)}}">
					<!--Messages-->
					<div class="form-group row">

						<div class="col-md-12">

							<input type="text" name="renamefolder" required="" class="form-control" value="{{$recdocuments->documenttypes->document_types_name}}">
							
							@if ($errors->has('renamefolder'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('renamefolder') }}</strong>
							</span>
							@endif
						</div>

					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-primary" value="Rename">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	//for view-recdocuments.blade.php
	function toggle(source) {
	   var checkboxes = document.querySelectorAll('input[type="checkbox"]');
	    for (var i = 0; i < checkboxes.length; i++) {
	    	if (checkboxes[i] != source)
	    		checkboxes[i].checked = source.checked;
	    }
	  }
</script>
@endsection