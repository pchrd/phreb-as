@extends('layouts.myApp')
@section('content')
@include('recdocuments.documents_modals')



<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-10">
			<div class="card-header label-default col-md-12">
				
				@foreach($recdetail_get as $recdetail_get1)

				<center>
					<label class="text-dark"><h1>{{$recdetail_get1->applicationlevel->levels->level_name}} <br> {{$recdetail_get1->rec_institution}} - {{$recdetail_get1->rec_name}}</h1></label>
				</center>
				
				<div class="float-right">

					@if($recdetail_get1->applicationlevel->recdetails_submission_status == 1)
						<div class="btn-group-vertical pull-right" style="position: fixed;bottom: 88px;
						right: 3px; width:200px; margin-right:30px; padding: 10px 20px;border-radius: 4px;border-color: #46b8da">

							@can('secretariat-access')
								<button type="button"  data-toggle="modal" data-target="#add_documents_sec" class="btn btn-block btn-outline-primary btn-sm block"><span class="fa fa-upload"></span> Upload Requirements</button>
							@endcan

							<a href="{{ url('view-recdetails', ['id' => encrypt($recdetail_get1->applicationlevel->recdetails_id)]) }}" class="btn btn-secondary" title="Back"><span class="fa fa-arrow-circle-o-left"></span> Go Back</a>

						</div>
					@endif

				</div>
			</div><br>


		</div>


		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-md-10">

					<div class="col-md-12" id="savedclass">
						@if(session('success'))
						<div class="alert alert-secondary">
							{{session('success')}}
							<button class="close" onclick="document.getElementById('savedclass').style.display='none'">x</button>
						</div>
						@endif
					</div>

				@can('rec-access')

					@if($recdetail_get1->applicationlevel->recdetails_submission_status == 0)
						<div id="stepper-example" class="bs-stepper">
							<div class="bs-stepper-header">
								<div class="step" data-target="#test-l-1">
									<a href="{{route('view-recdetails', ['id' => encrypt($recdetail_get1->id)])}}">
										<span class="bs-stepper-circle">1</span>
										<span class="bs-stepper-label"> First step <x class="fa fa-check"></x></span>
									</a>
								</div>
								<div class="line"></div>
								<div class="step" data-target="#test-l-2">
									<a href="#">
										<span class="bs-stepper-circle">2</span>
										<span class="bs-stepper-label badge badge-info"><u>Second step</u></span>
									</a>
								</div>
								<div class="line"></div>
								<div class="step" data-target="#test-l-3">
									@if($required_files->count() == 0)
										<a href="{{ route('step3finalverification', ['id' => encrypt($recdetail_get1->id)]) }}" class="disabled">
											<span class="bs-stepper-circle">3</span>
											<span class="bs-stepper-label" title="">Third step <x class="fa fa-remove"></x></span>
										</a>
									@else
										<a href="#" class="disabled">
											<span class="bs-stepper-circle">3</span>
											<span class="bs-stepper-label" title="">Third step <x class="fa fa-remove"></x></span>
										</a>
									@endif
								</div>
							</div>
						</div>
					@endif

				@endcan



				<ul class="nav nav-tabs" id="myTab" role="tablist">

				<!-- <li class="nav-item">
					<a class="nav-link text-info" id="recass-tab" data-toggle="tab" href="#pf" role="tab" aria-controls="pf" aria-selected="false" title="Assessments For REC">PHREB Forms</a>
				</li> -->

				@can('accreditors_and_csachair_secretariat')
				<!-- <li class="nav-item">
					<a class="nav-link text-info" id="rec-tab" data-toggle="tab" href="#rec" role="tab" aria-controls="rec" aria-selected="false" title="{{$recdetail_get1->rec_institution	}} - {{$recdetail_get1->rec_name}}">{{$recdetail_get1->rec_institution	}} - {{str_limit($recdetail_get1->rec_name, 7)}}</a>
				</li> -->

				<!-- <li class="nav-item">
					<a class="nav-link text-info" id="recass-tab" data-toggle="tab" href="#recass" role="tab" aria-controls="recass" aria-selected="false" title="Assessments For REC">Assessments w/ Attached Action Plans</a>
				</li> -->
				@endcan




				@endforeach
			</ul>

			<div class="tab-content" id="myTabContent">
				<div class="tab-pane show active" id="rec" role="tabpanel" aria-labelledby="recreq-tab">
					@can('rec_and_secretariat')
					
					@if($recdetail_get1->recdetails_resubmitreq == 0)
					

						@if($recdetail_get1->applicationlevel->recdetails_submission_status == 0 OR Auth::user()->can('only-admin-can-access-page'))
						<br>

							<div class="container-fluid btn-group form-group" role="group" aria-label="Basic example">
								<button type="button"  data-toggle="modal" data-target="#add_documents" class="btn btn-block btn-outline-primary btn-sm block"><span class="fa fa-upload"></span> Upload Requirements</button>
								<!-- <button type="button" data-toggle="modal" data-target="#createfolder" class="btn bg-yellow btn-block"><span class="fa fa-folder"></span> Create Folder</button> -->
							</div> 

						@endif

					@endif
					
					@endcan


					@foreach($recdetails as $recdetail)
					<form action="{{ route('check-complete', ['recdetail' => $recdetail->id]) }}" method="POST"> 
						@endforeach
						@csrf

						<a class="btn btn-outline-default col-md-12" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" title="View initial files" >
							<span class="fa fa-arrow-down"></span> 
						</a>



						<div class="collapse {{$recdetail_get1->applicationlevel->recdetails_submission_status != 0 ? '' : 'show'}}" id="collapseExample">
							
						<table class="table table-hover table-bordered">
							<thead class="thead-default label-info"> 
								<tr>
									<th>Application Documents</th>
									<th>No. of files</th>
								</tr>
							</thead> 
							<div class="form-group row">

								<tbody>
									@forelse($recdetails as $recdetail_tbl)
									
									<tr>
										<td>
											<b>
												@can('accreditors-access')
													@if($recdetail_tbl->check == true)
														<small><span class="badge badge-success fa fa-check">already checked by secretariat</span>@else<span class="badge badge-secondary fa fa-remove"></span>
														</small>
													@endif
												@endcan

												@can('rec-access')
													@if($recdetail_tbl->check == true)
														<span class="label label-success fa fa-check" title="PHREB Secretariat successfully checked this files & documents"></span>
													@else

														@if($recdetail_tbl->required == 1)
															<span class="label label-danger fa fa-check-square-o" title=""></span>
														@else
															<span class="fa fa-folder"></span>
														@endif

													@endif
												@endcan

												@can('secretariat-access')
													@if($recdetail_tbl->check == true)
														<input type="checkbox" class="checkboxes" name="recdocuments_document_types_id[]" value="{{$recdetail_tbl->recdocuments_document_types_id}}" checked>
													@else
														<!-- <input type="checkbox" class="checkboxes" name="recdocuments_document_types_id[]" value="{{$recdetail_tbl->recdocuments_document_types_id}}"> -->

														<span class="fa fa-folder-o"></span>
													@endif
												@endcan	

												<a href="{{ route('documents/view-recdocuments', 
												['id' => encrypt($recdetail_tbl->recdetails_id), 'recdocuments_document_types_id' => $recdetail_tbl->recdocuments_document_types_id]
												) }}"> 
													{{ $recdetail_tbl->document_types_name }}

												</a>


												</b><br> {{ str_limit($recdetail_tbl->files,70) }}
												

												<!-- <span title="This is required!" class="{{ $recdetail_tbl->required != null ? 'fa fa-check' : ''}}"></span> -->

												</b>

												<!-- <br> {{ str_limit($recdetail_tbl->files,70) }} -->
											

											</td>

											<td>
												<!-- <span class="fa fa-comments"></span> {{ str_limit($recdetail_tbl->message, 50) ?? 'No available comment(s)' }} -->

												{{$recdetail_tbl->totalfiles}}
											</td>

											@empty
    											<tr><td colspan="2" class="text text-danger"><center><i>"No files/folders uploaded, please click upload requirements"</center></i></td></tr>

										</tr>  

										@endforelse

										@foreach($required_files as $file)
								            <tr>
								                <td><span class="fa fa-times text text-danger blink_me" title="This is required"></span><b style="color: red;"> {{ $file->document_types_name }}</b></td>
								                <td>---</td>
								            </tr>
								        @endforeach

									</tbody>
									
								</div>
							</table>

							<!-- @can('secretariat-access')
								<button type="submit" class="btn btn-success btn-block pull-left" title=""><span class="fa fa-save"></span> Save Changes</button>
							@endcan -->

							@can('rec-access')
								@if($recdetail_get1->applicationlevel->recdetails_submission_status == 0 AND $required_files->count() == 0)
									<a href="{{ route('step3finalverification', ['id' => encrypt($recdetail_get1->id)]) }}" type="submit" class="btn btn-success btn-block pull-left" title="Mark as Complete"><span class="fa fa-save"></span> Save and Proceed to Step 3</a>

								@else
									<center><i class="text text-warning">--- Please complete the required document below before your proceed ---</i></center>
								@endif
							@endcan
							
						</div>

						</form> <br> 














						<!-- submission based on status tagged by sec -->


						@if($recdetail_get1->applicationlevel->recdetails_submission_status != 0)

						<div class="card">
							<div class="card-body">


								<table class="table table-condensed track_tbl">

									<thead>
										<tr>
											<th></th>


											<th><div class="card-header">
												<h2 style=""><center>After initial submission of requirements, REC with the status of <br><b><u>Incomplete or Re-submission</u> </b> can upload here below.</center></h2>
											</div></th>
										</tr>
									</thead>

									<tbody>

										@forelse($submissionstatuses as $id => $submissionStatus)

										<tr class="active">
											<td class="track_dot">
												<span class="track_line {{$loop->first ? 'bg-primary' : ''}}"></span>
											</td>


											<td>


												<div class="accordion" id="accordionExample">
													<div class="card">



														<div class="card-header bg-default" id="headingOne">
															<h2 class="mb-0">

																<button class="btn btn-link btn-block" type="button" data-toggle="collapse" data-target="#collapseOne{{$submissionStatus->id}}" aria-expanded="true" aria-controls="collapseOne">

																	<div class="container">
																		
																			@if($loop->first)
																				@can('rec-access')

																					@if($submissionStatus->submissionstatuses_rec_response == null)

																						<span class="badge badge-info blink_me fa fa-bell ">
																							<label class="small"> New Status. Please upload Here!</label>
																						</span> <br>

																					@else

																						<span class="fa fa-check">
																							<label class="label label-success small"> Submitted to PHREB Secretariat. Please wait for their response.</label>
																						</span> <br>

																					@endif
																				@endcan
																			@endif

																				<label style="color: black;"><span class="fa fa-file"></span> PHREB Secretariat - {{$submissionStatus->users->name}} tagged your REC Application as <br> <b class="text text-danger">{{$submissionStatus->status->status_name}}</b>
																				on {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $submissionStatus->submissionstatuses_datecreated)->format('F j, Y H:i:s')}} | {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $submissionStatus->submissionstatuses_datecreated)->diffForHumans()}}
																				</label>

																		</div>

																	</button>
																</h2>


																<div class="container">
																	<div class="col-md-12">


																		<center><a href="#" data-toggle="modal" data-target="#viewmessage{{$submissionStatus->id}}" style="color: maroon"><span class="fa fa-envelope"></span> View comments/messages</a></center>


																	</div>
																</div>



															</div>

															<div id="collapseOne{{$submissionStatus->id}}" class="{{$loop->first ? 'collapse show' : 'collapse'}}" aria-labelledby="headingOne" data-parent="#accordionExample">

																<div class="card-body">

																	@can('rec-access')

																	@if($submissionStatus->submissionstatuses_rec_response == null)
																	<div class="btn btn-block bg bg-default block">
																		
																		<form class="form-inline col-md-12" action="{{ route('myfunctions.documents', ['recdetail' => $decrypted_id]) }}" method="POST" enctype="multipart/form-data">
																			{{ csrf_field() }}

																			<input type="hidden" name="recdetails_id" value="{{encrypt($decrypted_id)}}">

																			<!-- added this for knowing na iadd sa submissionstatuses -->
																			<input type="hidden" name="submissionstatuses_id" value="{{ $submissionStatus->id }}">

																			<div class="container">
																				<div class="row">
																					<div class="col-sm-4">
																						<input id="recdocuments_file" type="file" class="" name="recdocuments_file[]" multiple="true" accept="application/jpg,jpeg,png,pdf,docx,doc,zip," required="">
																					</div>
																					<div class="col-sm-4">
																						<input type="text" class="form-control" name="recdocuments_document_types_id" placeholder="Folder Name" required>
																					</div>
																					<div class="col-sm-4">
																						<input type="submit" class="btn btn-primary mb-2" value="Upload">


																						@if($submissionStatus->recdocuments()->exists())
																							<a href="#" data-toggle="modal" data-target="#resubmit-requirements" class="btn btn-success mb-2"> Submit to PHREB</a>
																						@endif


																					</div>
																				</div>
																			</div>

																		</form>

																		<p class="text text-warning"><small>Note: Please use .pdf, .png, .jpg, and .docx format only</small></p>

																	</div> <br>

																	@endif

																	@endcan

																	<table class="table table-condensed table-hover">
																		<thead class="bg-white">
																			<tr>
																				<th></span> Document Name</th>
																				<th>Number of Files</th>
																			</tr>
																		</thead>

																		<tbody>

																			@forelse($submissionStatus->recdocuments as $id => $recdocument)
																			<tr>
																				<td>
																					
																						<span class="fa fa-folder">
																						<a href="{{ route('documents/view-recdocuments', 
																						['id' => encrypt($submissionStatus->submissionstatuses_recdetails_id), 'recdocuments_document_types_id' => $recdocument->docID]
																						) }}">
																						{{$recdocument->document_types_name}}
																					</a>
																				

																			</td>

																			<td> 
																				{{$recdocument->totalFiles}}
																			</td>

																		</tr>
																		
																		@empty

																			<tr><td colspan="2" class="text text-danger small"><center><i>"No file/folder uploaded..."</i></center></td></tr>
																		
																		@endforelse

																	</tbody>
																</table>

															</div>
														</div>

														@endforeach


														@foreach($submissionstatuses as $id => $submissionStatus)
														<div class="modal fade" id="viewmessage{{$submissionStatus->id}}" tabindex="-1" role="dialog">
															<div class="modal-dialog" role="document">
																<div class="modal-content">
																	<div class="modal-header bg-maroon">
																		<h5 class="modal-title">Messages from {{$submissionStatus->users->role_users->roles->role_name}} - {{$submissionStatus->users->name}}</h5>
																		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																			<span aria-hidden="true">&times;</span>
																		</button>
																	</div>
																	<div class="modal-body">
																		<p>
																			{!! $submissionStatus->submissionstatuses_emailmessage !!}
																			
																			<br>

																			@foreach($submissionStatus->emailattachments as $ea)
																				
																				<i>
																					<a href="{{ route('emailattachmment', ['id' => encrypt($ea->id)]) }}" target="_blank"><span class="fa fa-file"> </span> {{$ea->re_file_name}}</a> <br>

																				</i>
																			
																			@endforeach
																		</p>
																	</div>
																	<div class="modal-footer">
																		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
																	</div>
																</div>
															</div>
														</div>

													</div>
												</div>

											</td>

										</tr>

										@empty

											<tr>
												<td colspan="2"><center><small><p class="text text-danger">"No incomplete or re-submission status available"</p></small></center></td>
											</tr>


										@endforelse
									</tbody>

								</table>





							</div>
						</div>



						<div class="modal fade" id="resubmit-requirements">
							<div class="modal-dialog">
								<div class="modal-content">
									<!-- Modal Header -->
									<div class="modal-header badge badge-success">
										<h6 class="modal-title">Submission Confirmation Alert</h6>
										<button type="button" class="close" data-dismiss="modal">&times;</button>
									</div>
									<!-- Modal body -->
									<div class="modal-body">
										<div class="container-fluid">
											<div class="form-group">
												Do you want to submit requirements to PHREB Secretariat? After this, the system will not allowed you to add or remove the uploaded files inside of the folder.
											</div>
										</div>
									</div>
									<!-- Modal footer -->
									<div class="modal-footer">
										<a href="{{route('resubmit-req', ['recdetail' => $decrypted_id])}}" class="btn btn-success">Submit</a>
										<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
									</div>
								</div>
							</div>
						</div>

						@endif


					</div>



		





















					<div class="tab-pane fade" id="pf" role="tabpanel" aria-labelledby="pf-tab">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>PHREB Forms Name 

										<!-- <a href="#" data-toggle="Modal" data-target="#addForm" title="Create Phreb Form" class="{{Auth::user()->can('rec-access') ? '' : 'hide'}} btn btn-outline-info"><span class="fa fa-plus"></span> Create</a> -->

										<th>Remarks/Comments</th>
									</tr>
								</thead>
								<tbody>
									@foreach($phrebforms as $pf)
									<tr>
										<td>
											<i class="fa fa-check bg-lime {{$pf->pf_status != true ? 'hide' : ''}}" title="Submit Completed"></i><a href="{{route('view-phreb-forms', ['id' => encrypt($pf->id), 'rec_id' => encrypt($pf->pf_recdetails_id)])}}" class="fa fa-file" target="_blank"> {{$pf->phrebforms_template->pft_name}} </a> <br>

											<b>Created at: </b>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $pf->created_at)->format('F j, Y H:i:s')}}<small> ({{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $pf->created_at)->diffForHumans()}})</small> <br>

											<b>Updated at: </b>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $pf->updated_at)->format('F j, Y H:i:s')}}<small> ({{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $pf->updated_at)->diffForHumans()}})</small>

										</td>


										<td><a href="#" data-toggle="modal" data-target="#pfremarkscomment">{!! str_limit($pf->pf_comments, 50) !!}</a><br>

											@if($pf->pf_comments_date >= \Carbon\Carbon::today()->addDays(-2))
											<label class="badge badge-success blink_me small">You've new Remarks/Comments here!</label>
											@elseif($pf->pf_comments_date == null)
											---
											@else
											<label class="text taxt-success small">No new remarks/comments</label>
											@endif

										</td>
									</tr>

									<div class="modal fade" id="pfremarkscomment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
										<div class="modal-dialog modal-lg" role="document">
											<div class="modal-content">
												<div class="modal-header bg-aqua">
													<h4 class="modal-title" id="myModalLabel">Remarks/Comments </h4>
												</div>
												<div class="modal-body">
													<div class="container">
														<label><h3>{{$pf->phrebforms_template->pft_name}}<span class="fa fa-clock pull-right"> {{$pf->pf_comments_date}}</span></h3></label>
													</div>	
													<div class="form-control">
														<label>
															{!! $pf->pf_comments !!}
														</label>
													</div>
												</div>
												<div class="modal-footer">

												</div>
											</div>
										</div>
									</div>

									@endforeach
								</tbody>
							</table>	
						</div>



						<div class="tab-pane fade" id="recass" role="tabpanel" aria-labelledby="recass-tab">
							@can('accreditors_and_csachair_secretariat')

							<div class="modal fade" id="createassessmentform" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header bg-navy">
											<h4 class="modal-title" id="myModalLabel">Accreditors Assessment Form Matrix</h4>
										</div>
										<div class="modal-body">
											<div class="container">
												<label>Do you want to create a Assessment Matrix Form for <b>{{$recdetail_get1->rec_institution	}} - {{$recdetail_get1->rec_name}} | Level {{$recdetail_get1->applicationlevel->level_id}}</b> ?</label>

												<form action="{{route('create-assessment-form', ['id' => encrypt($recdetail_get1->id)])}}" method="GET">

													{{ csrf_field() }}

													@if($recdetail_get1->applicationlevel->levels->id == 3)
													<select id="pf_finalreportLvl3" type="text" class="form-control{{ $errors->has('pf_finalreportLvl3') ? ' is-invalid' : '' }}" name="pf_finalreportLvl3" value="{{ old('pf_finalreportLvl3') }}" required>

														<option selected disabled>---Please Select---</option>

														@foreach($finalreportAssessment_lvl3 as $id => $fra_lvl3)
														<option value="{{$id}}">{{$fra_lvl3}}</option>
														@endforeach

													</select>
													@endif

													<div class="modal-footer">
														<button type="submit" class="btn bg-navy btn-block" onclick="submitForm(this);">Create</button>
													</div>
												</form>	
											</div>

										</div>

									</div>
								</div>
							</div>

								@can('accreditors_and_secretariat')
									<table id="" class="table table-hover">
										<thead class="thead-default label-info"> 
											<tr>
												<th>
													@can('accreditors-access')
													Your Initial 
													@endcan

													@can('secretariat-access')
													Accreditors
													@endcan
													Assessment Matrix Forms @can('accreditors-access')| <a href="#" data-toggle="modal" data-target="#createassessmentform" class="btn btn-outline-info"><span class="fa fa-plus"></span> Create Assessment Form</a>@endcan</th>
													<th>Form Status</th>
													<th>Attached Action Plan w/ Compliance Evidences From REC</th>
												</tr>
											</thead> 
									
										</table>

								@endcan
								
						
								@endcan

								@can('rec-access')
								<table id="" class="table table-condensed table-hover">
									<thead class="thead-default label-info"> 
										<tr>
											<th>Initial Assessment Matrix Forms</th>
											<th>Your Attached Action Plan w/ Comliance Evidences</th>
										</tr>

									</thead> 
									<div class="form-group row">
										<tbody>
											@forelse($assessmentREC as $id => $assRec)
											<tr>
												<td>
													{{$id+1}}. 
													<a href="{{route('view-assessment-form', encrypt($assRec->pfar_pf_id)) }}"> <span class="fa fa-file"></span> {{$assRec->phrebforms->phrebforms_template->pft_name}}</a><br>
													<small>For {{$assRec->recdetails->rec_institution}} - {{$assRec->recdetails->rec_name}} {{$assRec->recdetails->applicationlevel->levels->level_name}}</small><br>

													{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $assRec->created_at)->format('M j, Y g:i:A')}} - {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $assRec->created_at)->diffForHumans()}}<br>
													<small>Sent by {{$assRec->sentby->name}}</small>
												</td>

												<td>

													@foreach($assRec->getAttachedActionPlan as $id => $gaap)
													<p>
														<a href="{{route('viewActionplan', ['id' => encrypt($gaap->ap_pfap_id)])}}" target="_blank" title="{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $gaap->created_at)->format('F j, Y - G:i:A')}}"> 
															<legend>
																<span class="fa fa-external-link"></span> 
																{{$gaap->phrebforms_templates->pft_name}}
															</legend>
														</a>
														Date Created: <b class="badge badge-secondary">
															{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $gaap->phrebforms->created_at)->format('F j, Y - G:i:A')}}
														</b>

														<span class="fa fa-clock-o"></span><b class="badge badge-info">{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $gaap->phrebforms->created_at)->diffforhumans()}}</b>

														<br>
														Date Updated: <b class="badge badge-secondary">
															{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $gaap->phrebforms->updated_at)->format('F j, Y - G:i:A')}}
														</b>

														<span class="fa fa-clock-o"></span><b class="badge badge-info">{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $gaap->phrebforms->updated_at)->diffforhumans()}}</b>

														<br>

														Current Status: 
														@foreach($gaap->phrebforms->phrebform_actionplanfromrecstatuses as $status)

														@if($loop->last)
														<label class="">
															<mark>
																<b title="By {{$status->users->role_users->roles->role_name}} at {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $status->created_at)->format('F j, Y - G:i:A')}}">{{$status->statuses->status_name}} 
																	<small>
																		({{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $status->created_at)->diffforhumans()}})
																	</small>
																</b>
															</mark>
														</label>
														@else
														<label class="">
															<strike>
																<b title="By {{$status->users->name}} at {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $status->created_at)->format('F j, Y - G:i:A')}}">{{$status->statuses->status_name}} 
																	<small>
																		({{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $status->created_at)->diffforhumans()}})
																	</small>
																</b>
															</strike>
														</label>
														@endif

														@endforeach

													</p>
													@endforeach

												</td>
												@empty

											</tr>
											@endforelse
										</tbody>
									</div>
								</table>

								@endcan
							</div>








							<div class="tab-pane fade" id="actionplan" role="tabpanel" aria-labelledby="actionplan-tab">
								@can('accreditors_and_csachair_secretariat')
								@foreach($recdetails as $recdetail)
								<form action="{{route('ap_check', ['ap_recid' => $recdetail->id])}}" method="POST">
									@endforeach
									<table id="" class="table table-hover">
										<thead class="thead-default label-info"> 
											<tr>
												<th>ID</th>
												<th>DOCUMENT</th>
												<th>DOCUMENT TYPE</th>
												@can('accreditors_and_csachair_secretariat')
												<th>ACTIONS</th>
												@endcan
											</tr>
										</thead> 
										<div class="form-group row">
											<tbody> 
												@foreach($actionplan as $ap)
												<tr>
													<td>{{$ap->id}}</td>
													<td>
														<b>{{$ap->ap_recactionplan_name}}</b><br>	
														<small>
															{{$ap->recdetails->rec_name}}<br>
															{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $ap->created_at)->format('M j, Y')}} - <i class="text text-danger"> {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $ap->created_at)->diffForHumans()}} </i>
														</small>
													</td>
													<td>{{$ap->documenttypes->document_types_name}}</td>
													<td><a href="../storage/actionplansandcomplianceevidences/{{$ap->recdetails->users->id}}/{{$ap->ap_recdetails_id}}/{{$ap->ap_recactionplan_uniqid}}-{{$ap->ap_recactionplan_name}}" target="_blank" class="btn btn-outline-warning fa fa-download pull-left"> Download</a></td>
												</tr>
												@endforeach
											</tbody>
										</div>
									</table>

								</form>
								@endcan
							</div>








							<div class="tab-pane fade" id="eval_actionplan" role="tabpanel" aria-labelledby="eval_actionplan-tab">

								<table id="" class="table table-hover">
									<thead class="thead-default label-info"> 
										<tr>
											<th>ID</th>
											<th>DOCUMENT</th>
											<th>DOCUMENT TYPE</th>
											@can('secretariat-access')
											<th>ACTIONS</th>
											@endcan

										</tr>
									</thead> 
									<div class="form-group row">
										@can('accreditors_and_secretariat')
										<tbody> 
											@foreach($showactionplan_eval as $sap)

											@if($sap->role_id == 5)<tr style="background-color: pink;">@endif
												<td>{{$sap->id}}</td>
												<td><a href="../storage/actionplansandcomplianceevidences/{{$sap->users->id}}/{{$sap->ap_recdetails_id}}/{{$sap->ap_recactionplan_uniqid}}-{{$sap->ap_recactionplan_name}}" target="_blank" title="Download"><b>{{$sap->ap_recactionplan_name}}</b></a><br> 
													@can('accreditors_and_csachair_secretariat')
													<small>{{$sap->users->name}}</small>
													@endcan
													<small> <br>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $sap->created_at)->format('M j, Y')}} - <i class="text text-danger">{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $sap->created_at)->diffForHumans()}}</i><br>
														<p class="badge badge-secondary">{{$sap->role_name}}</p>
													</small>
												</td>

												<td>{{$sap->documenttypes->document_types_name}}</td>
												@can('secretariat-access')
												<td>						
													<div class="btn-group" role="group">
														<button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
															<span class="fa fa-list"></span>
														</button>
														<div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
															@if($sap->role_id == 5)

															@if($sap->ap_submittedto_rec != null)
															<center><span class="badge badge-dark dropdown-item fa fa-check">Successfully <br> Forwarded to REC</span></center>
															@else
															<a href="#" data-toggle="modal" data-target="#forwardtorec{{$sap->id}}" class="btn btn-outline-default dropdown-item"><span class="fa fa-send"></span> Send to REC</a>
															@endif

															@endif
															<a href="#" data-toggle="modal" data-target="#forwardtocsa{{$sap->id}}" class="btn btn-outline-default dropdown-item"><span class="fa fa-send"></span> Send to CSA Chair</a>
															<a href="{{route('apce-history', ['sender' => $sap->users->name, 'filename' => $sap->ap_recactionplan_name, 'id' => $sap->id])}}" title="View Submission History" class="btn btn-outline-success fa fa-history dropdown-item"> History</a>
														</div>
													</div>
												</td>

												@endcan
											</tr>
											@endforeach  
										</tbody>
										@endcan

										@can('csachair-access')
										<tbody>
											@foreach($showactionplan_eval as $se)
											<tr> 
												<td>{{$se->id}}</td>
												<td>
													<b>{{$se->recdetails->rec_name}}</b><br>
													<a href="../storage/actionplansandcomplianceevidences/{{$se->recactionplan->ap_sender_id}}/{{$se->recdetails->id}}/{{$se->recactionplan->ap_recactionplan_uniqid}}-{{$se->recactionplan->ap_recactionplan_name}}" target="_blank">{{$se->recactionplan->ap_recactionplan_name}}</a> - <small>tblno{{$se->recactionplan->id}}</small><br>
													<small>Forwarded by <b>{{$se->users->name}}</b> - {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $se->ap_csa_datecreated_at)->format('M j, Y')}}<br>
														From: <b>{{$se->recactionplan->users->name}}</b><br>
														<i class="text text-danger">{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $se->ap_csa_datecreated_at)->diffForHumans()}}</i>
													</small>
												</td>
												<td>{{$se->recactionplan->documenttypes->document_types_name}}</td>
											</tr>
											@endforeach
										</tbody>
										@endcan
									</div>
								</table>
							</div>

							<!-- sent evaluations -->
							<div class="tab-pane fade" id="eval_actionplan_sent" role="tabpanel" aria-labelledby="eval_actionplan_sent-tab">

								<table id="" class="table table-hover">
									<thead class="thead-default label-info"> 
										<tr>
											<th>ID</th>
											<th>DOCUMENT</th>
											<th>DOCUMENT TYPE</th>
											@can('secretariat-access')
											<th>ACTIONS</th>
											@endcan

										</tr>
									</thead> 
									<div class="form-group row">
										@can('csachair-access')
										<tbody>
											@foreach($showactionplan_eval_sent as $ses)
											<tr> 
												<td>{{$ses->id}}</td>
												<td>
													<b>{{$ses->recdetails->rec_name}}</b><br>
													<a href="../storage/actionplansandcomplianceevidences/{{$ses->ap_sender_id}}/{{$ses->recdetails->id}}/{{$ses->ap_recactionplan_uniqid}}-{{$ses->ap_recactionplan_name}}" target="_blank">{{$ses->ap_recactionplan_name}}</a><br><small>From: {{$ses->users->name}}<br>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $ses->created_at)}}</small>
												</td>
												<td>{{$ses->documenttypes->document_types_name}}</td>
											</tr>
											@endforeach
										</tbody>
										@endcan
									</div>
								</table>

							</div>
						</div>
						@can('rec-access')
						@if($recdetail_get1->recdetails_resubmitreq == 0 AND $recdetail_get1->recdetails_lock_fields == 1)
						<a href="" class="btn btn-success btn-lg btn-block blink_me" data-toggle="modal" data-target="#resubmit-req"><span class="fa fa-send"></span> Update Final Submission of Requirements</a>

						<!-- Modal for resubmit req -->
						<div class="modal fade" id="resubmit-req">
							<div class="modal-dialog modal-sm">
								<div class="modal-content">
									<!-- Modal Header -->
									<div class="modal-header badge badge-success">
										<h6 class="modal-title">Confirmation Alert</h6>
										<button type="button" class="close" data-dismiss="modal">&times;</button>
									</div>
									<!-- Modal body -->
									<div class="modal-body">
										<div class="container-fluid">
											<div class="form-group">
												Do you want to resubmit/update your requirements? After this you will not allowed to add or remove your uploaded files in the system.
											</div>
										</div>
									</div>
									<!-- Modal footer -->
									<div class="modal-footer">
										<a href="{{route('resubmit-req', ['recdetail' => $decrypted_id])}}" class="btn btn-success">Update</a>
										<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
									</div>
								</div>
							</div>
						</div>
						@endif
						@endcan
					</div>
				</div>
			</div>




















		</div>
	</div>



	@endsection