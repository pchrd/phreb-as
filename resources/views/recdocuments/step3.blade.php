@extends('layouts.myApp')
@section('content')

<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-10">
			
			<div class="col-md-12" id="savedclass">
				@if(session('success'))
				<div class="alert alert-info">
					{{session('success')}}
					<button class="close" onclick="document.getElementById('savedclass').style.display='none'">x</button>
				</div>
				@endif
			</div>

			<div class="card-header col-md-12">

				<center>
					<label class="text-dark"><h1> {{$recdetails->applicationlevel->levels->level_name}} <br>{{$recdetails->rec_institution}} - {{$recdetails->rec_name}}</h1></label>
				</center>

				<div class="float-right">

					@can('accreditors_and_secretariat')
						<a href="" data-toggle="modal" data-target="#add_remarks" class="btn btn-primary btn-sm"><span class="fa fa-comments-o"></span> Add Remarks/Comments</a>
					@endcan
					
					@can('csachair-access')
						<a href="" data-toggle="modal" data-target="#add_remarks" class="btn btn-primary btn-sm"><span class="fa fa-comments-o"></span> Add Remarks/Comments</a>
					@endcan
				</div>
			</div><br>


			@can('rec-access')

			<div id="stepper-example" class="bs-stepper">
				<div class="bs-stepper-header">
					<div class="step" data-target="#test-l-1">
						<a href="{{route('view-recdetails', ['id' => encrypt($recdetails->id)])}}">
							<span class="bs-stepper-circle">1</span>
							<span class="bs-stepper-label"> First step <x class="fa fa-check"></x></span>
						</a>
					</div>
					<div class="line"></div>
					<div class="step" data-target="#test-l-2">
						<a href="{{ url('documents', ['id' => encrypt($recdetails->id)]) }}">
							<span class="bs-stepper-circle">2</span>
							<span class="bs-stepper-label"><u>Second step <x class="fa fa-check"></x></u></span>
						</a>
					</div>
					<div class="line"></div>
					<div class="step" data-target="#test-l-3">
						<a href="#">
							<span class="bs-stepper-circle">3</span>
							<span class="bs-stepper-label badge badge-info">Third step</span>
						</a>
					</div>
				</div>
			</div>

			@endcan	


			<br>
			<div class="card">
				<h3 class="card-header bg-default">Final Step: Summary of the Initial Submission</h3>
				<div class="card-body">
					
					<div class="accordion" id="accordionExample">
						<div class="card">
							<div class="card-header" id="headingOne">
								<h2 class="mb-0">
									<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
										Step 1: REC General Information & Details
									</button>
								</h2>
							</div>

							<div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
								<div class="card-body">
									
									<table class="table table-condensed table-bordered">
										<tbody>
											<tr>

												<td scope="col">Application Type</td>
												<td scope="col">{{$recdetails->status->status_name}}</td>
												
											</tr>

											<tr>

												<td scope="col">Level of Accreditation</td>
												<td scope="col">{{$recdetails->applicationlevel->levels->level_name}}</td>
												
											</tr>

											<tr>

												<td scope="col">Region</td>
												<td scope="col">{{$recdetails->applicationlevel->regions->region_name}}</td>
												
											</tr>

											<tr>

												<td scope="col">Name of REC</td>
												<td scope="col">{{$recdetails->rec_name}}</td>
												
											</tr>

											<tr>

												<td scope="col">Name of Institution</td>
												<td scope="col">{{$recdetails->rec_institution}}</td>
												
											</tr>

											<tr>

												<td scope="col">REC Address</td>
												<td scope="col">{{$recdetails->rec_address}}</td>
												
											</tr>

											<tr>

												<td scope="col">REC Email</td>
												<td scope="col">{{$recdetails->rec_email}}</td>
												
											</tr>

											<tr>

												<td scope="col">REC Tel No</td>
												<td scope="col">{{$recdetails->rec_telno}}</td>
												
											</tr>

											<tr>

												<td scope="col">REC Fax No</td>
												<td scope="col">{{$recdetails->rec_faxno}}</td>
												
											</tr>
										</tbody>
										
									</table>

								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header" id="headingTwo">
								<h2 class="mb-0">
									<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
										Step 2: Uploaded Folder Name w/ files
									</button>
								</h2>
							</div>
							<div id="collapseTwo" class="" aria-labelledby="headingTwo" data-parent="#accordionExample">
								<div class="card-body">
									<table class="table table-condensed">
										<thead>
											<tr>
												
												<th>Checklist:</th>

											</tr>
										</thead>
										<tbody>

											@foreach($foldernames as $id => $foldername)
											
												<tr>

													<td><span class="fa fa-check-square-o"></span> {{ $foldername->document_types_name }} <span class="" title="This is required!"></span></td>
												</tr>

											@endforeach
										</tbody>
									</table>

									<div class="container">
										<p class="text text-danger"><i>* Reminder: Please review the uploaded files before submitting to PHREB Secretariat</i></p>
									</div>

								</div>
							</div>
						</div>

					</div>

					<br>

					<a href="#" class="btn btn-primary btn-block {{$recdetails->applicationlevel->recdetails_lock_fields === 0 ? '' : 'hide'}}" data-toggle="modal" data-target="#submit_to_phreb"><span class="fa fa-send"></span> Submit to PHREB</a>

					<a href="{{ url('documents', ['id' => encrypt($recdetails->id)]) }}" class="btn btn-danger btn-block"><span class="fa fa-share"></span> Cancel & Go Back</a>

				</div>

				

			</div>	





		</div>
	</div>
</div>


<!-- Modal for Submit to PHREB -->
<div class="modal fade" id="submit_to_phreb" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header badge badge-primary">
				<h5 class="modal-title" id="exampleModalLabel">Submit Confirmation</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Do you want to submit your Application to PHREB?</p>
				<p class=" text text-danger">Note: Once you submit this application, the system will not allow you to update or edit your requirements uploaded in the step 2. Please make sure that you've completely check the previous step to avoid re-submission/incomplete status. Thank you!</p>
			</div>
			<div class="modal-footer">
				<a href="{{ route('submit_recdetails', ['id' => encrypt($recdetails->applicationlevel->recdetails_id)]) }}" class="btn btn-primary">Submit</a>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>

@endsection