<div class="modal fade" id="createfolder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-yellow">
				<h4 class="modal-title" id="myModalLabel">Folder Name</h4>
			</div>
			<div class="modal-body">
			<form method="POST" action="{{route('create-folder', ['id' => $decrypted_id])}}">
				{{ csrf_field() }}
				<input id="createfolder" type="text" class="form-control{{ $errors->has('createfolder') ? ' is-invalid' : '' }}" name="createfolder" placeholder="Name of folder" required>
						@if ($errors->has('createfolder'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('createfolder') }}</strong>
						</span>
						@endif
				<div class="modal-footer">
					<button type="submit" class="btn bg-yellow">Create</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
			</div>
		</div>
	</div>
</div>


@foreach($showactionplan_eval as $sap)
<!-- modal forward chachairs final evaluation -->
<div class="modal fade" id="forwardtorec{{$sap->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header badge-info">
				<h4 class="modal-title" id="myModalLabel">Forward to Research Ethics Committee </h4>
			</div>
			<div class="modal-body">
			<form method="POST" action="{{route('ap_sendtorec')}}" enctype="multipart/form-data">
				{{ csrf_field() }}
				<h6>Do you want to forward/submit this REC to <b>{{$sap->rec_name}} via email?</b>?</h6>
				<input type="hidden" name="ap_csa_recdetails_id" value="{{$sap->ap_recdetails_id}}">
				<input type="hidden" value="{{$sap->id}}" name="sap_id">
				<input type="hidden" value="{{$sap->ap_sender_id}}" name="ap_sender_id">

				<!-- FileName -->
				<div class="form-group row">
					<label for="apce_letter" class="col-md-4 col-form-label text-md-left">{{ __('Attach Letter:') }}</label>
					<div class="col-md-8">
						<input id="apce_letter" type="file" class="form-control{{ $errors->has('apce_letter') ? ' is-invalid' : '' }}" name="apce_letter[]"  multiple="true" accept="application/pdf,docx,doc">
						@if ($errors->has('apce_letter'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('apce_letter') }}</strong>
						</span>
						@endif
					</div>	
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Send</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
			</div>
		</div>
	</div>
</div>
@endforeach

<!-- /////////////////////////////////////////// -->

@foreach($showactionplan_eval as $sap)
<!-- modal forward eval on ap&ce to CSA -->
<div class="modal fade" id="forwardtocsa{{$sap->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header badge-info">
				<h4 class="modal-title" id="myModalLabel">Forward to CSA Chair</h4>
			</div>
			<div class="modal-body">
			<form method="POST" action="{{route('ap_sendtocsachair')}}">
				{{ csrf_field() }}
				<h6>Do you want to forward/submit this file <b>{{$sap->ap_recactionplan_name}}</b> to CSA Chair ?</h6>
				<input type="hidden" name="ap_csa_ap_id" value="{{$sap->id}}">
				<input type="hidden" name="ap_csa_recdetails_id" value="{{$sap->ap_recdetails_id}}">
				<input type="hidden" value="{{$sap->id}}" name="sap_id">
				<select id="ap_csachair_id" type="text" class="form-control{{ $errors->has('ap_csachair_id') ? ' is-invalid' : '' }}" name="ap_csa_csachair_id" required>
							<!-- get the data in db and put in select -->
							<option value="" disabled selected>---Please Select CSA Chair---</option>
							@foreach($selectcsachair as $id => $selectcsachairs)
							<option value="{{$id}}">{{ $selectcsachairs }}</option>
							@endforeach		
				</select>

				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Forward</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
			</div>
		</div>
	</div>
</div>
@endforeach

<!-- modal send assessment to rec & csachair -->
@foreach($showassessment_phreb as $s_p)
<div class="modal fade" id="sendtorec{{$s_p->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header badge-info">
				<h4 class="modal-title" id="myModalLabel">Send to Research Ethics Committee</h4>
			</div>
			<div class="modal-body">
				<form method="POST" action="{{route('sendtorec')}}">
				{{ csrf_field() }}
				<input type="hidden" name="ra_rec_ra_id" value="{{$s_p->id}}">
				<input type="hidden" name="ra_rec_recdetails_id" value="{{$s_p->ra_recdetails_id}}">
				<input type="hidden" name="ra_rec_csachair_id" value="{{$s_p->ra_user_id}}">
				Do you want to forward/submit this assessment to Research Ethics Committee, <b>{{$s_p->rec_name}}</b>?
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary">Send</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
			</form>
			</div>
		</div>
	</div>
</div>

<!-- send assessment form to CSA Chair -->
<div class="modal fade" id="sendtocsachair{{$s_p->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header badge-info">
				<h4 class="modal-title" id="myModalLabel">Send to CSA Chair</h4>
			</div>
			<div class="modal-body">
				<form method="POST" action="{{ route('sendtocsachair') }}">
				{{ csrf_field() }}
				<input type="hidden" name="recassessment_id" value="{{$s_p->id}}">
				<!-- FileName -->
				<div class="form-group row">
					<label for="from_accreditor" class="col-md-4 col-form-label text-md-left">{{ __('From Accreditor:') }}</label>
					<div class="col-md-8">
						<input id="from_accreditor" type="text" class="form-control{{ $errors->has('from_accreditor') ? ' is-invalid' : '' }}" name="from_accreditor" value="{{$s_p->name}}" readonly="">
						@if ($errors->has('from_accreditor'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('from_accreditor') }}</strong>
						</span>
						@endif
					</div>	
				</div>

				<!-- FileName -->
				<div class="form-group row">
					<label for="ra_recdocuments_file" class="col-md-4 col-form-label text-md-left">{{ __('File Name:') }}</label>
					<div class="col-md-8">
						<input id="ra_recdocuments_file" type="text" class="form-control{{ $errors->has('ra_recdocuments_file') ? ' is-invalid' : '' }}" name="ra_recdocuments_file" value="{{$s_p->ra_recdocuments_file}}" readonly="">
						@if ($errors->has('recdocuments_file'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('ra_recdocuments_file') }}</strong>
						</span>
						@endif
					</div>	
				</div>

				<!-- File ID -->
				<div class="form-group row">
					<label for="recdocuments_file_generated" class="col-md-4 col-form-label text-md-left">{{ __('File ID:') }}</label>
					<div class="col-md-8">
						<input id="recdocuments_file_generated" type="text" class="form-control{{ $errors->has('recdocuments_file') ? ' is-invalid' : '' }}" name="recdocuments_file_generated" value="{{$s_p->ra_recdocuments_file_generated}}" readonly="">

						@if ($errors->has('recdocuments_file_generated'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('recdocuments_file_generated') }}</strong>
						</span>
						@endif
					</div>	
				</div>

				<!-- choose csa chair -->
				<div class="form-group row">
					<label for="ra_csachair_id" class="col-md-4 col-form-label text-md-left">{{ __('Choose CSA Chair:') }}</label>
					<div class="col-md-8">
						<select id="ra_csachair_id" type="text" class="form-control{{ $errors->has('ra_csachair_id') ? ' is-invalid' : '' }}" name="ra_csachair_id" required>
							<!-- get the data in db and put in select -->
							@foreach($selectcsachair as $id => $selectcsachairs)
							<option value="{{$id}}">{{ $selectcsachairs }}</option>
							@endforeach		
						</select>

						@if ($errors->has('recdocuments_name'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('recdocuments_name') }}</strong>
						</span>
						@endif
					</div>
				</div>
				<div class="modal-footer">
				<button type="submit" class="btn btn-primary">Send</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</form>
			</div>	
		</div>
	</div>
</div>
@endforeach

<!-- modal for add documents per recdetails id -->
<div class="modal fade bd-example-modal-lg" id="add_documents" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header bg-aqua">
				<h5 class="modal-title" id="exampleModalLabel"><span class="fa fa-file"></span> Upload Requirements</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

				<form method="POST" action="{{ route('myfunctions.documents', ['recdetail' => $decrypted_id]) }}" enctype="multipart/form-data">
					{{ csrf_field() }}
					<input type="hidden" name="recdetails_id" value="{{encrypt($decrypted_id)}}">
					<!-- Folder Document name -->
					<div class="form-group row">
						<label for="recdocuments_name" class="col-md-4 col-form-label text-md-left">{{ __('List of requirements:*') }}</label>
						<div class="col-md-8">

 							<select id="recdocuments_document_types_id" type="text" class="form-control{{ $errors->has('recdocuments_folder_name_id') ? ' is-invalid' : '' }}" name="recdocuments_document_types_id" value="{{ old('recdocuments_document_types_id') }}" style="width: 100%">

 							<option selected disabled>--- Please select ---</option>
	                            <!-- get the data in db and put in select -->
								@foreach($select_doc_type as $id => $document_type)
										<option value="{{encrypt($id)}}">{{ $document_type }}</option>
								@endforeach		
							</select>


							@if ($errors->has('recdocuments_name'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('recdocuments_name') }}</strong>
							</span>
							@endif
						</div>
					</div>

				<!-- Documents -->
				<div class="form-group row">
					<label for="recdocuments_file" class="col-md-4 col-form-label text-md-left">{{ __('Browse document/s*:') }}</label>
					<div class="col-md-8">
						<input id="recdocuments_file" type="file" class="form-control{{ $errors->has('recdocuments_file') ? ' is-invalid' : '' }}" name="recdocuments_file[]" multiple="true" accept="application/jpg,jpeg,png,pdf,docx,doc,zip," required="">

						<p class="font-italic" style="color: brown; size: 1px;">(You can upload or drag multiple-files here with pdf,docx,doc format only)</p>

						@if ($errors->has('recdocuments_file'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('recdocuments_file') }}</strong>
						</span>
						@endif
					</div>
				</div>

					<div class="modal-footer">
						<input type="submit" class="btn btn-info" value="Upload">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					</div>
				</form>

			</div>
			
		</div>
	</div>
</div>

@can('secretariat-access')
<!-- modal for add documents per recdetails id -->
<div class="modal fade bd-example-modal-lg" id="add_documents_sec" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header bg-aqua">
				<h5 class="modal-title" id="exampleModalLabel"><span class="fa fa-file"></span> Upload Requirements</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

				<form method="POST" action="{{ route('myfunctions.documents', ['recdetail' => $decrypted_id]) }}" enctype="multipart/form-data">
					{{ csrf_field() }}
					<input type="hidden" name="recdetails_id" value="{{encrypt($decrypted_id)}}">
					<!-- Folder Document name -->
					<div class="form-group row">
						<label for="recdocuments_name" class="col-md-4 col-form-label text-md-left">{{ __('List of requirements:*') }}</label>
						<div class="col-md-8">

 							<select id="recdocuments_document_types_id_sec" type="text" class="form-control{{ $errors->has('recdocuments_folder_name_id') ? ' is-invalid' : '' }}" name="recdocuments_document_types_id" value="{{ old('recdocuments_document_types_id') }}" style="width: 100%">

 							<option selected disabled>--- Please select ---</option>
	                            <!-- get the data in db and put in select -->
								@foreach($select_doc_type as $id => $document_type)
										<option value="{{encrypt($id)}}">{{ $document_type }}</option>
								@endforeach		
							</select>


							@if ($errors->has('recdocuments_name'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('recdocuments_name') }}</strong>
							</span>
							@endif
						</div>
					</div>

				<!-- Documents -->
				<div class="form-group row">
					<label for="recdocuments_file" class="col-md-4 col-form-label text-md-left">{{ __('Browse document/s*:') }}</label>
					<div class="col-md-8">
						<input id="recdocuments_file" type="file" class="form-control{{ $errors->has('recdocuments_file') ? ' is-invalid' : '' }}" name="recdocuments_file[]" multiple="true" accept="application/jpg,jpeg,png,pdf,docx,doc,zip," required="">

						<p class="font-italic" style="color: brown; size: 1px;">(You can upload or drag multiple-files here with pdf,docx,doc format only)</p>

						@if ($errors->has('recdocuments_file'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('recdocuments_file') }}</strong>
						</span>
						@endif
					</div>
				</div>

					<div class="modal-footer">
						<input type="submit" class="btn btn-info" value="Upload">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					</div>
				</form>

			</div>
			
		</div>
	</div>
</div>
@endcan

<!-- modal for submit assessmentform per recdetails id -->
<div class="modal fade bd-example-modal-lg" id="assessmentform" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header badge-info">
				<h5 class="modal-title" id="exampleModalLabel"><span class="fa fa-file"></span> Submit Assessment Form</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

				<form method="POST" action="{{ route('sendassessmentform') }}" enctype="multipart/form-data">
					{{ csrf_field() }}
					<input type="hidden" name="recdetails_id" value="{{$decrypted_id}}">
					<!-- Folder Document name -->
					<div class="form-group row">
						<label for="recdocuments_name" class="col-md-4 col-form-label text-md-left">{{ __('Document Type:') }}</label>
						<div class="col-md-8">
 							<select id="recdocuments_document_types_id" type="text" class="form-control{{ $errors->has('recdocuments_folder_name_id') ? ' is-invalid' : '' }}" name="recdocuments_document_types_id" value="{{ old('recdocuments_document_types_id') }}" required="">
 							<option value="" disabled selected>---Please Select Document type---</option>

							@foreach($select_doc_type_acc as $id => $select_doc_type_accs)
									<option value="{{$id}}">{{ $select_doc_type_accs }}</option>
							@endforeach		
							</select>

							@if ($errors->has('recdocuments_name'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('recdocuments_name') }}</strong>
							</span>
							@endif
						</div>
					</div>

				<!-- Documents -->
				<div class="form-group row">
					<label for="recdocuments_file" class="col-md-4 col-form-label text-md-left">{{ __('Upload Documents*:') }}</label>
					<div class="col-md-8">
						<input id="recdocuments_file" type="file" class="form-control{{ $errors->has('recdocuments_file') ? ' is-invalid' : '' }}" name="recdocuments_file[]" multiple="true" accept="application/pdf" required="">

						<p class="font-italic" style="color: brown; size: 1px;">(You can upload or drag multiple-files here with pdf,docx,doc format only)</p>

						@if ($errors->has('recdocuments_file'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('recdocuments_file') }}</strong>
						</span>
						@endif
					</div>
				</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-info" value="Upload">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					</div>
				</form>

			</div>
			
		</div>
	</div>
</div>


<!-- modal for submit action plan & compliance evidences per recdetails id -->
<div class="modal fade bd-example-modal-lg" id="actionplan1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header badge-info">
				<h5 class="modal-title" id="exampleModalLabel"><span class="fa fa-file"></span> Submit Action Plan and Compliance Evidences</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

				<form method="POST" action="{{route('sendactionplan')}}" enctype="multipart/form-data">
					{{ csrf_field() }}
				<input type="hidden" name="ap_recdetails_id" value="{{$decrypted_id}}">

				<!-- select doc type -->
				<div class="form-group row">
					<label for="ap_document_types" class="col-md-4 col-form-label text-md-left">{{ __('Document Type*:') }}</label>
					<div class="col-md-8">
						<select id="ap_document_types" type="text" class="form-control{{ $errors->has('ap_document_types') ? ' is-invalid' : '' }}" name="ap_document_types" required>

							<option value="" disabled selected>---Please Select Document type---</option>
							
							@foreach($selectapce_doctypes as $id => $selectapce_doctype)
							 <option value="{{$id}}">{{$selectapce_doctype}}</option>
							@endforeach
						</select>
					</div>
				</div>

				<!-- Documents -->
				<div class="form-group row">
					<label for="ap_recactionplan_name" class="col-md-4 col-form-label text-md-left">{{ __('Upload Documents*:') }}</label>
					<div class="col-md-8">
						<input id="ap_recactionplan_name" type="file" class="form-control{{ $errors->has('ap_recactionplan_name') ? ' is-invalid' : '' }}" name="ap_recactionplan_name[]" multiple="true" accept="application/pdf,docx,doc" required="">

						<p class="font-italic" style="color: brown; size: 1px;">(You can upload or drag multiple-files here with pdf,docx,doc format only)</p>

						@if ($errors->has('ap_recactionplan_name'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('ap_recactionplan_name') }}</strong>
						</span>
						@endif
					</div>
				</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-info" value="Upload">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					</div>
				</form>

			</div>
			
		</div>
	</div>
</div>