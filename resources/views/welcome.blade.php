<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>PHREB ACCREDITATION </title>
        <link rel="icon" type="image/png" href="{{ asset('tablogo.png') }}">
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="theme/css/styles.css" rel="stylesheet" />
    </head>
    <body id="page-top">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
            <div class="container">
                <a class="navbar-brand js-scroll-trigger" href="#page-top">PHREB - ACCREDITATION</a>
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                    <i class="fas fa-bars"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">

                        <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#about">About</a></li>
                        <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#reports">Reports</a></li>
                        <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#forms">Downloadable Forms</a></li>
                        <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#manual">Manual</a></li>
                        <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#contactus">Contact us</a></li>
                        
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Masthead-->
        <header class="masthead">
            <div class="container d-flex h-100 align-items-center">
                <div class="mx-auto text-center">
                    <h1 class="mx-auto my-0 text-uppercase">PHREB</h1>
                    <h2 class="text-white-50 mx-auto mt-2 mb-5">Philippine Health Research Ethics Board</h2>
                     @if (Route::has('login'))

                        @auth
                            <a class="btn btn-primary js-scroll-trigger" href="{{url('/dashboard')}}">Go to Dashboard</a>
                        @else
                            <a class="btn btn-primary js-scroll-trigger" href="{{url('login')}}">Login</a> <a class="btn btn-primary js-scroll-trigger" href="{{url('register')}}">Request Account</a>

                        @endauth
                     
                     @endif
                    
                </div>
            </div>
        </header>
        <!-- About-->
        <section class="about-section text-center" id="about">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 mx-auto">
                        <h2 class="text-white mb-4">PHREB Accreditation Portal</h2>
                        <p class="text-white-50">
                           It is a web-based system designed to serve as an online repository of all required documents in Philippine Health Resaeach Ethics Board - Accreditation. The REC can easily monitor the status of their application when applying through the portal.
                        </p>
                    </div>
                </div>
                <img class="img-fluid" src="assets/img/ipad.png" alt="" />
            </div>
        </section>
        <!-- Projects-->
        <section class="reports-section bg-light" id="reports">
          <div class="container">
           <div class="row">
            
               <!-- i removed the generated report here -->

         </div>
       </div>
     </section>
 <!-- Projects-->
        <section class="projects-section bg-light" id="forms">
            <div class="container">
                <!-- Featured Project Row-->
                <div class="row align-items-center no-gutters mb-4 mb-lg-5">
                    <div class="col-xl-8 col-lg-7">

                       

                        <legend>
                            PHREB Accreditation Forms - Requirements (Level 1-3)
                        </legend>

                        <li>PHREB Form No. 1.1 Application for Accreditation</li>
                        <li>PHREB Form No. 1.4 Self-assessment Form for Level 1 or 2</li>
                        <li>PHREB Form No. 1.6 Self-assessment Form for Level 3</li>
                        <li>PHREB Form No. 1.1a Application for Accreditationof Specialty Clinics</li>

                        <br>

                        <legend>
                            Annual Report and Protocol Summary Forms
                        </legend>
                        <li>PHREB Form No. 1.2 Annual Report</li>
                        <li>PHREB Form No. 1.3 Protocol Summary</li>

                    </div>
                    <div class="col-xl-4 col-lg-5">
                        <div class="featured-text text-center text-lg-left">
                            <h4>Here are the Following PHREB Accreditation Forms</h4>
                            <p class="text-black-50 mb-0">These are the following forms to be uploaded in the portal.</p><br>
                            <p><a href="https://ethics.healthresearch.ph/index.php/forms" target="_blank" class="btn btn-primary">Download Here</a></p>
                        </div>
                    </div>
                </div>
               
            </div>
        </section>


        <!-- Signup-->
        <section class="signup-section" id="manual">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-lg-8 mx-auto text-center">
                        <i class="far fa-file fa-2x mb-2 text-white"></i>
                        <h2 class="text-white mb-5">PHREB Accreditation Portal - Manual</h2>
                        <a href="https://drive.google.com/file/d/10ocJtdt-MVhRGyvaG_xypz17bVlWXqih/view?usp=sharing" target="_blank" class="btn btn-primary mx-auto">Download here</a>
                    </div>
                </div>
            </div>
        </section>

        <!-- Contact-->
        <section class="contact-section bg-black " id="contactus">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 mb-3 mb-md-0">
                        <div class="card py-4 h-100">
                            <div class="card-body text-center">
                                <i class="fas fa-envelope text-primary mb-2"></i>
                                <h4 class="text-uppercase m-0">Accreditation Level 1</h4>
                                <hr class="my-4" />
                               <div class="small text-black-50"><a href="mailto:accreditation-level1@pchrd.dost.gov.ph">accreditation-level1@pchrd.dost.gov.ph</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 mb-3 mb-md-0">
                        <div class="card py-4 h-100">
                            <div class="card-body text-center">
                                <i class="fas fa-envelope text-primary mb-2"></i>
                                <h4 class="text-uppercase m-0">Accreditation Level 2</h4>
                                <hr class="my-4" />
                                <div class="small text-black-50"><a href="mailto:accreditation-level2@pchrd.dost.gov.ph">accreditation-level2@pchrd.dost.gov.ph</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 mb-3 mb-md-0">
                        <div class="card py-4 h-100">
                            <div class="card-body text-center">
                                <i class="fas fa-envelope text-primary mb-2"></i>
                                <h4 class="text-uppercase m-0">Accreditation Level 3</h4>
                                <hr class="my-4" />
                                <div class="small text-black-50"><a href="mailto:accreditation-level3@pchrd.dost.gov.ph">accreditation-level3@pchrd.dost.gov.ph</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Footer-->
        <footer class="footer bg-black small text-center text-white-50"><div class="container">©Philippine Council for Health Research and Development |  All Rights Reserved {{ date('Y') }} <br><small>visit ethics website: <a href="http://www.ethics.healthresearch.ph/">http://www.ethics.healthresearch.ph/</a></small></div>
        </footer>
        <!-- Bootstrap core JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script>
        <!-- Third party plugin JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
        <!-- Core theme JS-->
        <script src="theme/js/scripts.js"></script>
    </body>
</html>
