@extends('layouts.myApp')
@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-10">
			<div class="card-header col-md-12 label label-info">
				<label class="text" style="color: white;"> Action Plan and Compliance Evidences | {{$recdetails->rec_name}}</label>
				
				<div class="float-right">
					<a href="{{url()->previous()}}" class="btn btn-default btn-sm"><span class="fa fa-share"></span> Go Back</a>
					<a href="" data-toggle="modal" data-target="#actionplan" class="btn btn-info btn-sm"><span class="fa fa-file"></span> Add New Files</a>
					@can('accreditors_and_secretariat')
					<a href="" data-toggle="modal" data-target="#add_remarks" class="btn btn-primary btn-sm"><span class="fa fa-comments-o"></span> Add Remarks/Comments</a>
					@endcan
					@can('csachair-access')
					<a href="" data-toggle="modal" data-target="#add_remarks" class="btn btn-primary btn-sm"><span class="fa fa-comments-o"></span> Add Remarks/Comments</a>
					@endcan
				</div>

			</div>
			<div class="col-md-12" id="savedclass">
				@if(session('success'))
				<div class="alert alert-secondary">
					{{session('success')}}
					<button class="close" onclick="document.getElementById('savedclass').style.display='none'">x</button>
				</div>
				@endif
			</div>
			<form action="" method="POST">
			@csrf
				<table id="" class="table table-hover">
					<thead class="thead-default"> 
						<tr>
							<th>ID#</th>
							<th>DOCUMENT</th>
							<th>DOCUMENT TYPE</th>
							<th>STATUS</th>
						</tr>
					</thead> 
					<div class="form-group row">
						<tbody>
							@foreach($actionplan as $ap)
								<tr>
								<td>{{$ap->id}}</td>
								<td>
									<b><a href="../storage/actionplansandcomplianceevidences/{{$ap->recdetails->users->id}}/{{$ap->ap_recdetails_id}}/{{$ap->ap_recactionplan_uniqid}}-{{$ap->ap_recactionplan_name}}" target="_blank" title="Download">{{$ap->ap_recactionplan_name}}</a></b><br>
									<small>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$ap->created_at)->format('F j, Y')}} | {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$ap->created_at)->diffForHumans()}}</small>
								</td>
								<td>{{$ap->documenttypes->document_types_name}}</td>
								<td></td>
								</tr>
							@endforeach
						</tbody>
					</div>	
				</table>
		   </form>
		</div>
	</div>
</div>
@endsection

<!-- modal for submit actionplan per recdetails id -->
<div class="modal fade bd-example-modal-lg" id="actionplan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header badge-info">
				<h5 class="modal-title" id="exampleModalLabel"><span class="fa fa-file"></span> Submit Action Plan and Compliance Evidences</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form method="POST" action="{{route('actionplan_add')}}" enctype="multipart/form-data">
					{{ csrf_field() }}
					<input type="hidden" name="ap_recdetails_id" value="{{$recdetails->id}}">

					<div class="form-group row">
						<label for="document_type" class="col-md-4 col-form-label text-md-left">{{ __('Document Type*:') }}</label>
						<div class="col-md-8">
							<select id="document_type" type="text" class="form-control{{ $errors->has('document_type') ? ' is-invalid' : '' }}" name="document_type" required><option disabled>---Please Choose Document Types---</option>

								@foreach($doctypes as $id => $doctype)
								<option value="{{$id}}">{{$doctype}}</option>
								@endforeach
								
						</select>
						</div>
					</div>

					<!-- Documents -->
					<div class="form-group row">
						<label for="recactionplan" class="col-md-4 col-form-label text-md-left">{{ __('Upload Documents*:') }}</label>
						<div class="col-md-8">
							<input id="recactionplan" type="file" class="form-control{{ $errors->has('recactionplan') ? ' is-invalid' : '' }}" name="ap_recactionplan[]" multiple="true" accept="application/pdf,doc,docx" required="">
							<p class="font-italic" style="color: brown; size: 1px;">(You can upload or drag multiple-files here and pdf format only)</p>
							@if ($errors->has('recactionplan'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('recactionplan') }}</strong>
							</span>
							@endif
						</div>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-info" value="Upload">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					</div>
				</form>

			</div>
			
		</div>
	</div>
</div>