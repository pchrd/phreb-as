@include('layouts.myApp_links')
<center>
	<canvas id="404" height="100%" width="300"></canvas>
	<center><img src="{{ asset('brandlogo.png') }}" style="padding-bottom: 10px; margin-bottom: 10px; height: 150px; width: 150px"></center>
	<footer class="page-footer font-small mdb-color darken-32 pt-1">
		<h3>Server Request Error or Expired</h3>
		<h6>The error has been automatically sent to the administrator. Please wait for awhile. Thank you.</h6>

		@auth
			<a href="{{url('/dashboard')}}" class="fa fa-arrow-left btn btn-primary btn-sm"> Go back to Dashboard</a>
		@endauth
		
	</footer>
</center>
@include('layouts.myApp_footer')