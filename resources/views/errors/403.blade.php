@include('layouts.myApp_links')
<center>
	<canvas id="404" height="100%" width="300"></canvas>
	<center><img src="{{ asset('brandlogo.png') }}" style="padding-bottom: 10px; margin-bottom: 10px; height: 150px; width: 150px"></center>
	<footer class="page-footer font-small mdb-color darken-32 pt-1">
		<h3>Action is Unauthorized</h3>
		<h6>Please contact the administrator</h6>
		<a href="{{url()->previous()}}" class="fa fa-arrow-left btn btn-primary btn-sm"> Back</a>
	</footer>
</center>
@include('layouts.myApp_footer')