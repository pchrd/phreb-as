@extends('layouts.myApp')
<center>
	<canvas id="404" height="100%" width="300"></canvas>
	<!-- Footer -->
	<center><img src="{{ asset('brandlogo.png') }}" style="padding-bottom: 10px; margin-bottom: 10px; height: 150px; width: 150px"></center>
	<footer class="page-footer font-small mdb-color darken-32 pt-1">
		<h3>Page Not found</h3>
		<a href="{{url('/dashboard')}}" class="fa fa-arrow-left btn btn-primary btn-sm">Go back to Dashboard</a>
	</footer>
</center>

@include('layouts.myApp_footer')