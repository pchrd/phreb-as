@extends('layouts.myApp')

@section('content')

<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-8">

			<div class="card">
				<div class="card-header navbar-light"><a href="{{url('/user-management')}}"> Back to User Records </a>
					<caption class="row pull-right">
						
					</caption>

				</div>
				<div class="card-body">

					<div class="col-md-12" id="verifyuser">
						@if(session('success'))
						<div class="alert alert-success">
							{{session('success')}}
							<button class="close" onclick="document.getElementById('verifyuser').style.display='none'">x</button>
						</div>
						@endif
					</div>

					@foreach($users as $user)
					<form method="POST" action="{{ route('user-management-view-update', ['user_id' => $user->users->id]) }}" aria-label="{{ __('Register') }}">
						@csrf
						<div class="form-group row">
							<label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

							<div class="col-md-6">
								<input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $user->users->name }}" required autofocus>

								@if ($errors->has('name'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('name') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="form-group row">
							<label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

							<div class="col-md-6">
								<input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $user->users->email }}" required>

								@if ($errors->has('email'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('email') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<!-- custom select box -->
						<div class="form-group row">
							<label for="role" class="col-md-4 col-form-label text-md-right">{{ __('User Role') }}</label>
							<div class="col-md-6">

								<select id="role" type="text" class="form-control{{ $errors->has('role') ? ' is-invalid' : '' }}" name="role" required autofocus>
									<option value="{{$user->roles->id }}">{{ $user->roles->role_name }}</option>

									@foreach($roles as $id => $role)
									<option value="{{$id}}">{{$role}}, ({{ $id }})</option>
									@endforeach

								</select>

								@if ($errors->has('role'))
								<span class="invalid-feedback">
									<strong>{{ $errors->first('role') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<!-- custom select box -->
						<div class="form-group row">
							<label for="user_assigned_level_id" class="col-md-4 col-form-label text-md-right">{{ __('Assigned Level') }}</label>

							<div class="col-md-6">

								<select id="user_assigned_level_id" type="text" class="form-control{{ $errors->has('user_assigned_level_id') ? ' is-invalid' : '' }}" name="user_assigned_level_id" required autofocus>
									
									<option value="{{$user->levels->id}}">{{$user->levels->level_name}}</option>

									@foreach($levels as $id => $level)
									<option value="{{$id}}">{{$level}}, ({{ $id }})</option>
									@endforeach

								</select>

								@if ($errors->has('user_assigned_level_id'))
								<span class="invalid-feedback">
									<strong>{{ $errors->first('user_assigned_level_id') }}</strong>
								</span>
								@endif
							</div>
						</div>



						<!-- users created_at  -->
						<div class="form-group row">
							<label for="created_at" class="col-md-4 col-form-label text-md-right">{{ __('Date Created') }}</label>

							<div class="col-md-6">
								<input id="created_at" type="created_at" class="form-control{{ $errors->has('created_at') ? ' is-invalid' : '' }}" name="" value="{{ $user->users->created_at }}" readonly>

								@if ($errors->has('created_at'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('created_at') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<!-- check if verified user -->
						<div class="form-group row">
							<label for="verified" class="col-md-4 col-form-label text-md-right">{{ __('Account Status') }}</label>

							<div class="col-md-6">

								<select id="user_assigned_level_id" type="text" class="form-control{{ $errors->has('user_assigned_level_id') ? ' is-invalid' : '' }}" name="verified" required autofocus>
									
									<option value="{{$user->users->verified}}" selected="">{{$user->users->verified == 1 ? 'Access Granted' : 'Access Not Granted'}}</option>

									<option value="1">Access Granted</option>
									<option value="0">Access Not Granted</option>

								</select>
							</div>
						</div>

						<!-- esig user -->
				<!-- 		<div class="form-group row">
							<label for="esig" class="col-md-4 col-form-label text-md-right">{{ __('Signature') }} 
		
								<a href="#" data-toggle="modal" data-target="#esig" data-backdrop="static" class="btn-sm fa fa-pencil" title="Upload Signature">{{ __('') }}</a></label>

							<div class="col-md-6">
								@if($user->esig)
									<img src="{{asset($img)}}" width="200" height="50">
										<a href="{{route('removeesig', ['user_id' => $user->users->id])}}" class="fa fa-remove label-danger" onclick="return confirm('Do you want to remove Signature of {{$user->users->name}} ({{$user->users->id}})?')" title="Delete"></a>
									</img>
								@else
									<span class="badge badge-default">No Signature yet</span>
								@endif
							</div>
						</div>	 -->

						<div class="modal-footer">
							<button type="submit" class="btn btn-primary">{{ __('Save Changes') }}</button>
							<a href="{{url('/user-management')}}" class="btn btn-secondary">{{ __('Cancel') }}</a>
						</div>
					</form>
					@endforeach
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="esig" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header badge-dark">
				<h4 class="modal-title" id="myModalLabel">Upload Signature</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
				<form action="{{route('uploadesig')}}" method="POST" enctype="multipart/form-data">
					{{ csrf_field() }}
						<input type="hidden" name="esig_user_id" value="{{$user_id}}">
						<input id="esig_file" type="file" class="form-control{{ $errors->has('esig_file') ? ' is-invalid' : '' }}" name="esig_file[]" accept=".jpg" required="">
						<small><i class="text-danger pull-right">Please upload image format (.jpg) only</i></small>
						<div class="modal-footer">
							<button type="submit" class="form-control btn btn-secondary">Upload</button>
							<button type="button" data-dismiss="modal" class="form-control btn btn-default">Cancel</button>
						</div>
				</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection