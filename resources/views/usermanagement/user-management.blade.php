@extends('layouts.myApp')
@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header"><a href="{{url('/user-management')}}"> User Management </a>
					<caption class="row">
						@foreach(Auth::user()->roles as $role)
						| <small class="text-muted lead"> {{$role->role_name}} </small>  
						@endforeach
					</caption>

					<a class="btn btn-outline-info float-right" data-toggle="modal" data-target="#addaccount"><span class="fa fa-users"></span> Add Account</a>

					<!-- for search -->
					<form class="float-right col-md-4" action="" method="">
						<label for="search" class="sr-only">search</label>
						<div class="input-group stylish-input-group">
							<input type="text" class="form-control" name="search" id="search" placeholder="search...">
							<span class="input-group-addon">
								<button class="btn btn-default" type="submit">
									<span class="fa fa-search input-group"></span>
								</button> 
							</span>
						</div>
					</form>
				</div>
				<div class="card-body">

					<div class="col-md-12" id="verifyuser">
						@if(session('success'))
						<div class="alert alert-success">
							{{session('success')}}
							<button class="close" onclick="document.getElementById('verifyuser').style.display='none'">x</button>
						</div>
						@endif
					</div>

					<div class="container col-md-12">
						<table id="recdetails_table" class="col-md-8 table table-bordered table-condensed table-responsive table-hover small" data-toggle="dataTable" data-form="deleteForm">
							<thead class="thead-default"> 
								<tr>
									<div class="container-fluid"><caption class="form-group"></caption></div>

									<div class="form-group pull-left">
										<small>REC: </small>
										
										<strong>{{$countrec}}</strong>
										 |
										<small>Secretariat: </small>
										
										<strong>{{$countsec}}</strong>
										 |
										<small>Accreditors: </small>
										
										<strong>{{$countacc}}</strong>
										 |
										<small>CSA Chairs: </small>
										
										<strong>{{$countcsa}}</strong>
										 |
										<small>Admin: </small>
										
										<strong>{{$countadmin}}</strong>
																
									</div>
									
									<div class="form-group pull-right">
										<small>Verified: </small>
											@foreach($count_verified as $count_verified_1)
												<strong> {{$output[$count_verified_1->verified] = $count_verified_1->total}} </strong>
											@endforeach |
										<small>Not Verified: </small>
											@foreach($count_not_verified as $count_verified_1)
												<strong> {{$output[$count_verified_1->verified] = $count_verified_1->total}} </strong>
											@endforeach 						
									</div>

									<th data-column-id="">ID</th>
									
									<th data-column-id="">ACCOUNT NAME</th>

									<th data-column-id="">REC</th>
									<th data-column-id="">LEVEL</th>
									<th data-column-id="">MESSAGES/COMMENTS</th>
									
									<th data-column-id="">DATE CREATED</th>
									<th data-column-id="">STATUS</th>

									<th data-column-id="commands" data-formatter="commands" data-sortable="false">Actions</th>
								</tr>
							</thead> 

							<div class="form-group row">
								@foreach($users as $user)
									<tbody>
										<tr>
											<td>{{$user->id}}</td>
											
											<td><b>{{$user->name}}</b><br>
												{{$user->email}} <br> <br>
												Role: <i>{{$user->role_users->roles->role_name}}</i>
											</td>
										
											<td>{{$user->recname}}</td>
											<td>{{$user->applyingforlevel}}</td>
											<td><label title="{{$user->messagetosecretariat}}"> {{ str_limit($user->messagetosecretariat, $limit = 150, $end = '...') }} </label></td>
											<td>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $user->created_at)->toDayDateTimeString()}}</td>

											@if($user->verified == 1)
												<td><h6 class="badge badge-dark" title="By {{$users->find($user->verifiedby)['name'] ?? 'none'}}">Access Granted</h6></td>
											@else
												<td><a href="{{route('verifyemailaccount', ['id' => $user->id])}}" onclick="return confirm('Do you want to verify the account of {{$user->name}} - {{$user->email}}?')"><h6 class="badge badge-warning" >Access Not Granted </h6></a></td>
											@endif
											<td>
												<a href="{{ route('user-management-view', ['user_id' => $user->id])  }}" class="btn btn-outline-success col-md-12 fa fa-pencil" title="View"></a>
												<a href="{{ route('user-management-delete', ['user_id' => $user->id]) }}" class="btn btn-outline-danger col-md-12 fa fa-trash-o delete" onclick="return confirm('Do you want to Delete: {{$user->name}} | {{$user->email}} ?')" title="Delete"></a>
											</td>
										</tr>  
									</tbody>
								@endforeach
							</div>
						</table>
					</div>
					<center>
						<div class="form-group">
							<div class="col-md-2"> 
								
								<caption></caption>
							</div>
						</div>
					</center>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal for addaccount -->
<div class="modal fade" id="addaccount" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Account Registration</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

				<form method="POST" action="{{ route('user-management') }}" aria-label="{{ __('Register') }}">
					@csrf

					<div class="form-group row">
						<label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

						<div class="col-md-6">
							<input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

							@if ($errors->has('name'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('name') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group row">
						<label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

						<div class="col-md-6">
							<input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

							@if ($errors->has('email'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('email') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group row">
						<label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

						<div class="col-md-6">
							<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

							@if ($errors->has('password'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('password') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group row">
						<label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

						<div class="col-md-6">
							<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
						</div>
					</div>

					<!-- custom select box -->
					<div class="form-group row">
						<label for="role" class="col-md-4 col-form-label text-md-right">{{ __('User Role') }}</label>

						<div class="col-md-6">

							<select id="role" type="text" class="form-control{{ $errors->has('role') ? ' is-invalid' : '' }}" name="role" value="{{ old('role') }}" required autofocus>
								<option value="" disabled selected>---Please select role---</option>
								@foreach($roles as $id => $role)
								<option value="{{$id}}">{{$role}}, ({{ $id }})</option>
								@endforeach

							</select>

							@if ($errors->has('role'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('role') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<!-- custom select box for assigned level id -->
					<div class="form-group row">
						<label for="user_assigned_level_id" class="col-md-4 col-form-label text-md-right">{{ __('Choose Assigned Level') }}</label>

						<div class="col-md-6">

							<select id="user_assigned_level_id" type="text" class="form-control{{ $errors->has('user_assigned_level_id') ? ' is-invalid' : '' }}" name="user_assigned_level_id" value="{{ old('user_assigned_level_id') }}" required autofocus>
								<option value="" disabled selected>---Assign level---</option>
								
								@foreach($levels as $id => $assigned_levels)
								<option value="{{$id}}">{{$assigned_levels}}</option>			
								@endforeach

							</select>

							@if ($errors->has('user_assigned_level_id'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('user_assigned_level_id') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<!-- check if verified user -->
					<div class="form-group row">
						<label for="verified" class="col-md-4 col-form-label text-md-right">{{ __('Check Verification') }}</label>

						<div class="col-md-6">
							<div class="input-group">
								<input id="verified" type="checkbox" value="1" class="form-group" name="verified" required style="top: 1.10rem; width: 1.25rem; height: 2.25rem;">
							</div>
						</div>
					</div>	

					<div class="modal-footer">
						<button type="submit" class="btn btn-primary">{{ __('Create') }}</button>
						<button type="reset" class="btn btn-secondary">{{ __('Reset') }}</button>
					</div>

				</form>
			</div>
		</div>
	</div>
</div>
@endsection

