<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Word.Document>
<meta name=Generator content="Microsoft Word 14">
<meta name=Originator content="Microsoft Word 14">
<link rel=File-List href="orTemplate_files/filelist.xml">
<!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:Author>User1</o:Author>
  <o:Template>Normal</o:Template>
  <o:LastAuthor>User1</o:LastAuthor>
  <o:Revision>2</o:Revision>
  <o:TotalTime>2</o:TotalTime>
  <o:Created>2018-10-10T03:10:00Z</o:Created>
  <o:LastSaved>2018-10-10T03:10:00Z</o:LastSaved>
  <o:Pages>1</o:Pages>
  <o:Words>131</o:Words>
  <o:Characters>752</o:Characters>
  <o:Lines>6</o:Lines>
  <o:Paragraphs>1</o:Paragraphs>
  <o:CharactersWithSpaces>882</o:CharactersWithSpaces>
  <o:Version>14.00</o:Version>
 </o:DocumentProperties>
 <o:OfficeDocumentSettings>
  <o:AllowPNG/>
 </o:OfficeDocumentSettings>
</xml><![endif]-->
<link rel=themeData href="orTemplate_files/themedata.thmx">
<link rel=colorSchemeMapping href="orTemplate_files/colorschememapping.xml">
<!--[if gte mso 9]><xml>
 <w:WordDocument>
  <w:View>Print</w:View>
  <w:Zoom>104</w:Zoom>
  <w:SpellingState>Clean</w:SpellingState>
  <w:GrammarState>Clean</w:GrammarState>
  <w:TrackMoves>false</w:TrackMoves>
  <w:TrackFormatting/>
  <w:PunctuationKerning/>
  <w:ValidateAgainstSchemas/>
  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
  <w:DoNotPromoteQF/>
  <w:LidThemeOther>EN-US</w:LidThemeOther>
  <w:LidThemeAsian>X-NONE</w:LidThemeAsian>
  <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>
  <w:Compatibility>
   <w:BreakWrappedTables/>
   <w:SnapToGridInCell/>
   <w:WrapTextWithPunct/>
   <w:UseAsianBreakRules/>
   <w:DontGrowAutofit/>
   <w:SplitPgBreakAndParaMark/>
   <w:EnableOpenTypeKerning/>
   <w:DontFlipMirrorIndents/>
   <w:OverrideTableStyleHps/>
  </w:Compatibility>
  <m:mathPr>
   <m:mathFont m:val="Cambria Math"/>
   <m:brkBin m:val="before"/>
   <m:brkBinSub m:val="&#45;-"/>
   <m:smallFrac m:val="off"/>
   <m:dispDef/>
   <m:lMargin m:val="0"/>
   <m:rMargin m:val="0"/>
   <m:defJc m:val="centerGroup"/>
   <m:wrapIndent m:val="1440"/>
   <m:intLim m:val="subSup"/>
   <m:naryLim m:val="undOvr"/>
  </m:mathPr></w:WordDocument>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:LatentStyles DefLockedState="false" DefUnhideWhenUsed="true"
  DefSemiHidden="true" DefQFormat="false" DefPriority="99"
  LatentStyleCount="267">
  <w:LsdException Locked="false" Priority="0" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Normal"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="heading 1"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 2"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 3"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 4"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 5"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 6"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 7"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 8"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 9"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 1"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 2"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 3"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 4"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 5"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 6"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 7"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 8"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 9"/>
  <w:LsdException Locked="false" Priority="35" QFormat="true" Name="caption"/>
  <w:LsdException Locked="false" Priority="10" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Title"/>
  <w:LsdException Locked="false" Priority="1" Name="Default Paragraph Font"/>
  <w:LsdException Locked="false" Priority="11" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Subtitle"/>
  <w:LsdException Locked="false" Priority="22" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Strong"/>
  <w:LsdException Locked="false" Priority="20" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Emphasis"/>
  <w:LsdException Locked="false" Priority="59" SemiHidden="false"
   UnhideWhenUsed="false" Name="Table Grid"/>
  <w:LsdException Locked="false" UnhideWhenUsed="false" Name="Placeholder Text"/>
  <w:LsdException Locked="false" Priority="1" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="No Spacing"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 1"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 1"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 1"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 1"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 1"/>
  <w:LsdException Locked="false" UnhideWhenUsed="false" Name="Revision"/>
  <w:LsdException Locked="false" Priority="34" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="List Paragraph"/>
  <w:LsdException Locked="false" Priority="29" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Quote"/>
  <w:LsdException Locked="false" Priority="30" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Intense Quote"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 1"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 1"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 1"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 1"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 1"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 1"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 2"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 2"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 2"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 2"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 2"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 2"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 2"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 2"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 3"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 3"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 3"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 3"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 3"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 3"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 3"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 3"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 4"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 4"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 4"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 4"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 4"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 4"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 4"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 4"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 5"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 5"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 5"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 5"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 5"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 5"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 5"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 5"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 6"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 6"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 6"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 6"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 6"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 6"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 6"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 6"/>
  <w:LsdException Locked="false" Priority="19" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Subtle Emphasis"/>
  <w:LsdException Locked="false" Priority="21" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Intense Emphasis"/>
  <w:LsdException Locked="false" Priority="31" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Subtle Reference"/>
  <w:LsdException Locked="false" Priority="32" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Intense Reference"/>
  <w:LsdException Locked="false" Priority="33" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Book Title"/>
  <w:LsdException Locked="false" Priority="37" Name="Bibliography"/>
  <w:LsdException Locked="false" Priority="39" QFormat="true" Name="TOC Heading"/>
 </w:LatentStyles>
</xml><![endif]-->
<style>
<!--
 /* Font Definitions */
 @font-face
  {font-family:Calibri;
  panose-1:2 15 5 2 2 2 4 3 2 4;
  mso-font-charset:0;
  mso-generic-font-family:swiss;
  mso-font-pitch:variable;
  mso-font-signature:-536859905 -1073732485 9 0 511 0;}
@font-face
  {font-family:Tahoma;
  panose-1:2 11 6 4 3 5 4 4 2 4;
  mso-font-charset:0;
  mso-generic-font-family:swiss;
  mso-font-pitch:variable;
  mso-font-signature:-520081665 -1073717157 41 0 66047 0;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
  {mso-style-unhide:no;
  mso-style-qformat:yes;
  mso-style-parent:"";
  margin-top:0in;
  margin-right:0in;
  margin-bottom:10.0pt;
  margin-left:0in;
  line-height:115%;
  mso-pagination:widow-orphan;
  font-size:11.0pt;
  font-family:"Calibri","sans-serif";
  mso-ascii-font-family:Calibri;
  mso-ascii-theme-font:minor-latin;
  mso-fareast-font-family:Calibri;
  mso-fareast-theme-font:minor-latin;
  mso-hansi-font-family:Calibri;
  mso-hansi-theme-font:minor-latin;
  mso-bidi-font-family:"Times New Roman";
  mso-bidi-theme-font:minor-bidi;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
  {mso-style-noshow:yes;
  mso-style-priority:99;
  mso-style-link:"Balloon Text Char";
  margin:0in;
  margin-bottom:.0001pt;
  mso-pagination:widow-orphan;
  font-size:8.0pt;
  font-family:"Tahoma","sans-serif";
  mso-fareast-font-family:Calibri;
  mso-fareast-theme-font:minor-latin;}
span.BalloonTextChar
  {mso-style-name:"Balloon Text Char";
  mso-style-noshow:yes;
  mso-style-priority:99;
  mso-style-unhide:no;
  mso-style-locked:yes;
  mso-style-link:"Balloon Text";
  mso-ansi-font-size:8.0pt;
  mso-bidi-font-size:8.0pt;
  font-family:"Tahoma","sans-serif";
  mso-ascii-font-family:Tahoma;
  mso-hansi-font-family:Tahoma;
  mso-bidi-font-family:Tahoma;}
span.SpellE
  {mso-style-name:"";
  mso-spl-e:yes;}
span.GramE
  {mso-style-name:"";
  mso-gram-e:yes;}
.MsoChpDefault
  {mso-style-type:export-only;
  mso-default-props:yes;
  mso-ascii-font-family:Calibri;
  mso-ascii-theme-font:minor-latin;
  mso-fareast-font-family:Calibri;
  mso-fareast-theme-font:minor-latin;
  mso-hansi-font-family:Calibri;
  mso-hansi-theme-font:minor-latin;
  mso-bidi-font-family:"Times New Roman";
  mso-bidi-theme-font:minor-bidi;}
.MsoPapDefault
  {mso-style-type:export-only;
  margin-bottom:10.0pt;
  line-height:115%;}
@page WordSection1
  {size:595.35pt 841.95pt;
  margin:1.0in 1.0in 1.0in 1.0in;
  mso-header-margin:.5in;
  mso-footer-margin:.5in;
  mso-paper-source:0;}
div.WordSection1
  {page:WordSection1;}
-->
</style>
<!--[if gte mso 10]>
<style>
 /* Style Definitions */
 table.MsoNormalTable
  {mso-style-name:"Table Normal";
  mso-tstyle-rowband-size:0;
  mso-tstyle-colband-size:0;
  mso-style-noshow:yes;
  mso-style-priority:99;
  mso-style-parent:"";
  mso-padding-alt:0in 5.4pt 0in 5.4pt;
  mso-para-margin-top:0in;
  mso-para-margin-right:0in;
  mso-para-margin-bottom:10.0pt;
  mso-para-margin-left:0in;
  line-height:115%;
  mso-pagination:widow-orphan;
  font-size:11.0pt;
  font-family:"Calibri","sans-serif";
  mso-ascii-font-family:Calibri;
  mso-ascii-theme-font:minor-latin;
  mso-hansi-font-family:Calibri;
  mso-hansi-theme-font:minor-latin;
  mso-bidi-font-family:"Times New Roman";
  mso-bidi-theme-font:minor-bidi;}
</style>
<![endif]--><!--[if gte mso 9]><xml>
 <o:shapedefaults v:ext="edit" spidmax="1026"/>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <o:shapelayout v:ext="edit">
  <o:idmap v:ext="edit" data="1"/>
 </o:shapelayout></xml><![endif]-->
</head>

<body lang=EN-US style='tab-interval:.5in'>

<div class=WordSection1>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=638
 style='width:478.6pt;margin-left:4.65pt;border-collapse:collapse;mso-yfti-tbllook:
 1184;mso-padding-alt:0in 5.4pt 0in 5.4pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:10.6pt'>
  <td width=101 nowrap valign=bottom style='width:1.05in;border-top:solid black 1.0pt;
  border-left:solid black 1.0pt;border-bottom:none;border-right:none;
  padding:0in 5.4pt 0in 5.4pt;height:10.6pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=41 nowrap valign=bottom style='width:30.7pt;border:none;border-top:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:10.6pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&nbsp;<o:p></o:p></span></b></p>
  </td>
  <td width=22 nowrap valign=bottom style='width:16.35pt;border:none;
  border-top:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:10.6pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&nbsp;<o:p></o:p></span></b></p>
  </td>
  <td width=142 nowrap valign=bottom style='width:106.55pt;border:none;
  border-top:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:10.6pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&nbsp;<o:p></o:p></span></b></p>
  </td>
  <td width=106 nowrap valign=bottom style='width:79.65pt;border:none;
  border-top:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:10.6pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&nbsp;<o:p></o:p></span></b></p>
  </td>
  <td width=22 nowrap valign=bottom style='width:16.4pt;border:none;border-top:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:10.6pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&nbsp;<o:p></o:p></span></b></p>
  </td>
  <td width=81 nowrap valign=bottom style='width:60.65pt;border:none;
  border-top:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:10.6pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&nbsp;<o:p></o:p></span></b></p>
  </td>
  <td width=124 nowrap valign=bottom style='width:92.65pt;border-top:solid black 1.0pt;
  border-left:none;border-bottom:none;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:10.6pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&nbsp;<o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1;height:17.15pt'>
  <td width=434 nowrap colspan=6 valign=bottom style='width:325.25pt;
  border:none;border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:17.15pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:9.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><o:p>&nbsp;</o:p></span></b></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:9.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>Entity Name: <u>Phil Council for
  Health Research &amp; <span class=SpellE>Dev't</span>. </u><o:p></o:p></span></b></p>
  </td>
  <td width=81 nowrap valign=bottom style='width:60.65pt;padding:0in 5.4pt 0in 5.4pt;
  height:17.15pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>Serial No. :<o:p></o:p></span></b></p>
  </td>
  <td width=124 nowrap valign=bottom style='width:92.65pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:17.15pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><u><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman"'>{{$serialno}}<o:p></o:p></span></u></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:2;height:17.15pt'>
  <td width=101 nowrap valign=bottom style='width:1.05in;border-top:none;
  border-left:solid black 1.0pt;border-bottom:none;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:17.15pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>Fund Cluster:<o:p></o:p></span></b></p>
  </td>
  <td width=41 nowrap valign=bottom style='width:30.7pt;padding:0in 5.4pt 0in 5.4pt;
  height:17.15pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=270 nowrap colspan=3 valign=bottom style='width:202.55pt;
  padding:0in 5.4pt 0in 5.4pt;height:17.15pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><u><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>General Fund<o:p></o:p></span></u></b></p>
  </td>
  <td width=22 nowrap valign=bottom style='width:16.4pt;padding:0in 5.4pt 0in 5.4pt;
  height:17.15pt'></td>
  <td width=81 nowrap valign=bottom style='width:60.65pt;padding:0in 5.4pt 0in 5.4pt;
  height:17.15pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>Date :<span
  style='mso-spacerun:yes'>  </span><o:p></o:p></span></b></p>
  </td>
  <td width=124 nowrap valign=bottom style='width:92.65pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:17.15pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><u><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('F j, Y')}}<o:p></o:p></span></u></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:3;height:7.1pt'>
  <td width=101 nowrap valign=bottom style='width:1.05in;border:none;
  border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.1pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&nbsp;<o:p></o:p></span></b></p>
  </td>
  <td width=41 nowrap valign=bottom style='width:30.7pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.1pt'></td>
  <td width=22 nowrap valign=bottom style='width:16.35pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.1pt'></td>
  <td width=142 nowrap valign=bottom style='width:106.55pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.1pt'></td>
  <td width=106 nowrap valign=bottom style='width:79.65pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.1pt'></td>
  <td width=22 nowrap valign=bottom style='width:16.4pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.1pt'></td>
  <td width=81 nowrap valign=bottom style='width:60.65pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.1pt'></td>
  <td width=124 nowrap valign=bottom style='width:92.65pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.1pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&nbsp;<o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:4;height:7.1pt'>
  <td width=101 nowrap valign=bottom style='width:1.05in;border:none;
  border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.1pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&nbsp;<o:p></o:p></span></p>
  </td>
  <td width=41 nowrap valign=bottom style='width:30.7pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.1pt'></td>
  <td width=22 nowrap valign=bottom style='width:16.35pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.1pt'></td>
  <td width=142 nowrap valign=bottom style='width:106.55pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.1pt'></td>
  <td width=106 nowrap valign=bottom style='width:79.65pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.1pt'></td>
  <td width=22 nowrap valign=bottom style='width:16.4pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.1pt'></td>
  <td width=81 nowrap valign=bottom style='width:60.65pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.1pt'></td>
  <td width=124 nowrap valign=bottom style='width:92.65pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.1pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&nbsp;<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:5;height:9.35pt'>
  <td width=638 nowrap colspan=8 style='width:478.6pt;border-top:none;
  border-left:solid black 1.0pt;border-bottom:none;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:9.35pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><u><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman"'>ORDER
  OF PAYMENT<o:p></o:p></span></u></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:6;height:7.1pt'>
  <td width=101 nowrap valign=bottom style='width:1.05in;border:none;
  border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.1pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&nbsp;<o:p></o:p></span></p>
  </td>
  <td width=41 nowrap valign=bottom style='width:30.7pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.1pt'></td>
  <td width=22 nowrap valign=bottom style='width:16.35pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.1pt'></td>
  <td width=142 nowrap valign=bottom style='width:106.55pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.1pt'></td>
  <td width=106 nowrap valign=bottom style='width:79.65pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.1pt'></td>
  <td width=22 nowrap valign=bottom style='width:16.4pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.1pt'></td>
  <td width=81 nowrap valign=bottom style='width:60.65pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.1pt'></td>
  <td width=124 nowrap valign=bottom style='width:92.65pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.1pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&nbsp;<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:7;height:14.7pt'>
  <td width=306 nowrap colspan=4 valign=bottom style='width:229.2pt;border:
  none;border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:14.7pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>The Collecting Officer<o:p></o:p></span></b></p>
  </td>
  <td width=106 nowrap valign=bottom style='width:79.65pt;padding:0in 5.4pt 0in 5.4pt;
  height:14.7pt'></td>
  <td width=22 nowrap valign=bottom style='width:16.4pt;padding:0in 5.4pt 0in 5.4pt;
  height:14.7pt'></td>
  <td width=81 nowrap valign=bottom style='width:60.65pt;padding:0in 5.4pt 0in 5.4pt;
  height:14.7pt'></td>
  <td width=124 nowrap valign=bottom style='width:92.65pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:14.7pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&nbsp;<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:8;height:17.15pt'>
  <td width=306 nowrap colspan=4 valign=bottom style='width:229.2pt;border:
  none;border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:17.15pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>Cash/Treasury Unit<o:p></o:p></span></p>
  </td>
  <td width=106 nowrap valign=bottom style='width:79.65pt;padding:0in 5.4pt 0in 5.4pt;
  height:17.15pt'></td>
  <td width=22 nowrap valign=bottom style='width:16.4pt;padding:0in 5.4pt 0in 5.4pt;
  height:17.15pt'></td>
  <td width=81 nowrap valign=bottom style='width:60.65pt;padding:0in 5.4pt 0in 5.4pt;
  height:17.15pt'></td>
  <td width=124 nowrap valign=bottom style='width:92.65pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:17.15pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&nbsp;<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:9;height:7.1pt'>
  <td width=101 nowrap valign=bottom style='width:1.05in;border:none;
  border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.1pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&nbsp;<o:p></o:p></span></p>
  </td>
  <td width=41 nowrap valign=bottom style='width:30.7pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.1pt'></td>
  <td width=22 nowrap valign=bottom style='width:16.35pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.1pt'></td>
  <td width=142 nowrap valign=bottom style='width:106.55pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.1pt'></td>
  <td width=106 nowrap valign=bottom style='width:79.65pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.1pt'></td>
  <td width=22 nowrap valign=bottom style='width:16.4pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.1pt'></td>
  <td width=81 nowrap valign=bottom style='width:60.65pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.1pt'></td>
  <td width=124 nowrap valign=bottom style='width:92.65pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.1pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&nbsp;<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:10;height:7.1pt'>
  <td width=101 nowrap valign=bottom style='width:1.05in;border:none;
  border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.1pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&nbsp;<o:p></o:p></span></p>
  </td>
  <td width=41 nowrap valign=bottom style='width:30.7pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.1pt'></td>
  <td width=22 nowrap valign=bottom style='width:16.35pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.1pt'></td>
  <td width=142 nowrap valign=bottom style='width:106.55pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.1pt'></td>
  <td width=106 nowrap valign=bottom style='width:79.65pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.1pt'></td>
  <td width=22 nowrap valign=bottom style='width:16.4pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.1pt'></td>
  <td width=81 nowrap valign=bottom style='width:60.65pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.1pt'></td>
  <td width=124 nowrap valign=bottom style='width:92.65pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.1pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&nbsp;<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:11;height:17.15pt'>
  <td width=306 nowrap colspan=4 valign=bottom style='width:229.2pt;border:
  none;border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:17.15pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman"'><span
  style='mso-spacerun:yes'>      </span>Please issue Official Receipt in favor
  of<span style='mso-spacerun:yes'>  </span><o:p></o:p></span></p>
  </td>
  <td width=333 colspan=4 valign=bottom style='width:249.4pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-bottom-alt:solid black .5pt;mso-border-right-alt:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:17.15pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman"'>{{$recdetails->rec_name}} - {{$recdetails->rec_institution}}<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:12;height:8.1pt'>
  <td width=101 nowrap valign=bottom style='width:1.05in;border:none;
  border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:8.1pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&nbsp;<o:p></o:p></span></p>
  </td>
  <td width=41 nowrap valign=bottom style='width:30.7pt;padding:0in 5.4pt 0in 5.4pt;
  height:8.1pt'></td>
  <td width=22 nowrap valign=bottom style='width:16.35pt;padding:0in 5.4pt 0in 5.4pt;
  height:8.1pt'></td>
  <td width=142 nowrap valign=bottom style='width:106.55pt;padding:0in 5.4pt 0in 5.4pt;
  height:8.1pt'></td>
  <td width=333 nowrap colspan=4 valign=top style='width:249.4pt;border:none;
  border-right:solid black 1.0pt;mso-border-top-alt:solid black .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:8.1pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman"'>(Name
  of <span class=SpellE>Payor</span>)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:13;height:16.2pt'>
  <td width=638 nowrap colspan=8 valign=bottom style='width:478.6pt;border:
  solid black 1.0pt;border-top:none;mso-border-left-alt:solid black 1.0pt;
  mso-border-bottom-alt:solid black .5pt;mso-border-right-alt:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:16.2pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman"'>{{$recdetails->rec_address}}<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:14;height:9.6pt'>
  <td width=638 nowrap colspan=8 valign=bottom style='width:478.6pt;border-top:
  none;border-left:solid black 1.0pt;border-bottom:none;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;padding:0in 5.4pt 0in 5.4pt;height:9.6pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman"'>(Address/Office
  of <span class=SpellE>Payor</span>)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:15;height:17.15pt'>
  <td width=142 nowrap colspan=2 valign=bottom style='width:106.3pt;border:
  none;border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:17.15pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>in the amount of <o:p></o:p></span></p>
  </td>
  <td width=22 nowrap valign=bottom style='width:16.35pt;padding:0in 5.4pt 0in 5.4pt;
  height:17.15pt'></td>
  <td width=351 nowrap colspan=4 valign=bottom style='width:263.3pt;border:
  none;border-bottom:solid black 1.0pt;mso-border-bottom-alt:solid black .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:17.15pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman"'>Three
  Thousand Pesos Only<o:p></o:p></span></p>
  </td>
  <td width=124 nowrap valign=bottom style='width:92.65pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-bottom-alt:solid black .5pt;mso-border-right-alt:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:17.15pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman"'>(P3000)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:16;height:17.15pt'>
  <td width=164 nowrap colspan=3 valign=bottom style='width:122.65pt;
  border:none;border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:17.15pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>for payment of<span
  style='mso-spacerun:yes'>  </span><o:p></o:p></span></p>
  </td>
  <td width=475 nowrap colspan=5 valign=bottom style='width:355.95pt;
  border-top:none;border-left:none;border-bottom:solid black 1.0pt;border-right:
  solid black 1.0pt;mso-border-bottom-alt:solid black .5pt;mso-border-right-alt:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:17.15pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>PHREB Accreditation Processing Fee</span><span
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  "Times New Roman"'><o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:17;height:10.6pt'>
  <td width=638 nowrap colspan=8 valign=bottom style='width:478.6pt;border:
  solid black 1.0pt;border-top:none;mso-border-left-alt:solid black 1.0pt;
  mso-border-bottom-alt:solid black .5pt;mso-border-right-alt:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:10.6pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman"'>&nbsp;<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:18;height:10.6pt'>
  <td width=638 nowrap colspan=8 valign=bottom style='width:478.6pt;border-top:
  none;border-left:solid black 1.0pt;border-bottom:none;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;padding:0in 5.4pt 0in 5.4pt;height:10.6pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman"'>(Purpose)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:19;height:17.15pt'>
  <td width=638 nowrap colspan=8 valign=bottom style='width:478.6pt;border-top:
  none;border-left:solid black 1.0pt;border-bottom:none;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:17.15pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span class=GramE><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>per</span></span><span
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  "Times New Roman"'> Bill No. ________________ dated __________________.<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:20;height:7.1pt'>
  <td width=101 nowrap valign=bottom style='width:1.05in;border:none;
  border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.1pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&nbsp;<o:p></o:p></span></p>
  </td>
  <td width=41 nowrap valign=bottom style='width:30.7pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.1pt'></td>
  <td width=22 nowrap valign=bottom style='width:16.35pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.1pt'></td>
  <td width=142 nowrap valign=bottom style='width:106.55pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.1pt'></td>
  <td width=106 nowrap valign=bottom style='width:79.65pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.1pt'></td>
  <td width=22 nowrap valign=bottom style='width:16.4pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.1pt'></td>
  <td width=81 nowrap valign=bottom style='width:60.65pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.1pt'></td>
  <td width=124 nowrap valign=bottom style='width:92.65pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.1pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&nbsp;<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:21;height:7.1pt'>
  <td width=101 nowrap valign=bottom style='width:1.05in;border:none;
  border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.1pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&nbsp;<o:p></o:p></span></p>
  </td>
  <td width=41 nowrap valign=bottom style='width:30.7pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.1pt'></td>
  <td width=22 nowrap valign=bottom style='width:16.35pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.1pt'></td>
  <td width=142 nowrap valign=bottom style='width:106.55pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.1pt'></td>
  <td width=106 nowrap valign=bottom style='width:79.65pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.1pt'></td>
  <td width=22 nowrap valign=bottom style='width:16.4pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.1pt'></td>
  <td width=81 nowrap valign=bottom style='width:60.65pt;padding:0in 5.4pt 0in 5.4pt;
  height:7.1pt'></td>
  <td width=124 nowrap valign=bottom style='width:92.65pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:7.1pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&nbsp;<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:22;height:17.15pt'>
  <td width=638 nowrap colspan=8 valign=bottom style='width:478.6pt;border-top:
  none;border-left:solid black 1.0pt;border-bottom:none;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:17.15pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>Please deposit the collections
  under Bank Account/s:<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:23;height:10.6pt'>
  <td width=101 nowrap valign=bottom style='width:1.05in;border:none;
  border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:10.6pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&nbsp;<o:p></o:p></span></p>
  </td>
  <td width=41 nowrap valign=bottom style='width:30.7pt;padding:0in 5.4pt 0in 5.4pt;
  height:10.6pt'></td>
  <td width=22 nowrap valign=bottom style='width:16.35pt;padding:0in 5.4pt 0in 5.4pt;
  height:10.6pt'></td>
  <td width=142 nowrap valign=bottom style='width:106.55pt;padding:0in 5.4pt 0in 5.4pt;
  height:10.6pt'></td>
  <td width=106 nowrap valign=bottom style='width:79.65pt;padding:0in 5.4pt 0in 5.4pt;
  height:10.6pt'></td>
  <td width=22 nowrap valign=bottom style='width:16.4pt;padding:0in 5.4pt 0in 5.4pt;
  height:10.6pt'></td>
  <td width=81 nowrap valign=bottom style='width:60.65pt;padding:0in 5.4pt 0in 5.4pt;
  height:10.6pt'></td>
  <td width=124 nowrap valign=bottom style='width:92.65pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:10.6pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&nbsp;<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:24;height:17.75pt'>
  <td width=142 nowrap colspan=2 valign=bottom style='width:106.3pt;border:
  none;border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:17.75pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><u><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman"'>No.<o:p></o:p></span></u></p>
  </td>
  <td width=22 nowrap valign=bottom style='width:16.35pt;padding:0in 5.4pt 0in 5.4pt;
  height:17.75pt'></td>
  <td width=248 nowrap colspan=2 valign=bottom style='width:186.2pt;padding:
  0in 5.4pt 0in 5.4pt;height:17.75pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><u><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman"'>Name
  of Bank<o:p></o:p></span></u></p>
  </td>
  <td width=22 nowrap valign=bottom style='width:16.4pt;padding:0in 5.4pt 0in 5.4pt;
  height:17.75pt'></td>
  <td width=204 nowrap colspan=2 valign=bottom style='width:153.35pt;
  border:none;border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:17.75pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><u><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman"'>Amount<o:p></o:p></span></u></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:25;height:17.75pt'>
  <td width=142 nowrap colspan=2 valign=bottom style='width:106.3pt;border-top:
  none;border-left:solid black 1.0pt;border-bottom:solid black 1.0pt;
  border-right:none;mso-border-left-alt:solid black 1.0pt;mso-border-bottom-alt:
  solid black .5pt;padding:0in 5.4pt 0in 5.4pt;height:17.75pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman"'>&nbsp;<o:p></o:p></span></p>
  </td>
  <td width=22 nowrap valign=bottom style='width:16.35pt;padding:0in 5.4pt 0in 5.4pt;
  height:17.75pt'></td>
  <td width=248 nowrap colspan=2 valign=bottom style='width:186.2pt;border:
  none;border-bottom:solid black 1.0pt;mso-border-bottom-alt:solid black .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:17.75pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman"'>&nbsp;<o:p></o:p></span></p>
  </td>
  <td width=22 nowrap valign=bottom style='width:16.4pt;padding:0in 5.4pt 0in 5.4pt;
  height:17.75pt'></td>
  <td width=204 nowrap colspan=2 valign=bottom style='width:153.35pt;
  border-top:none;border-left:none;border-bottom:solid black 1.0pt;border-right:
  solid black 1.0pt;mso-border-bottom-alt:solid black .5pt;mso-border-right-alt:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:17.75pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><s><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>P</span></s><span
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  "Times New Roman"'><o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:26;height:17.75pt'>
  <td width=101 nowrap valign=bottom style='width:1.05in;border:none;
  border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:17.75pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>Total<o:p></o:p></span></b></p>
  </td>
  <td width=41 nowrap valign=bottom style='width:30.7pt;padding:0in 5.4pt 0in 5.4pt;
  height:17.75pt'></td>
  <td width=22 nowrap valign=bottom style='width:16.35pt;padding:0in 5.4pt 0in 5.4pt;
  height:17.75pt'></td>
  <td width=142 nowrap valign=bottom style='width:106.55pt;padding:0in 5.4pt 0in 5.4pt;
  height:17.75pt'></td>
  <td width=106 nowrap valign=bottom style='width:79.65pt;padding:0in 5.4pt 0in 5.4pt;
  height:17.75pt'></td>
  <td width=22 nowrap valign=bottom style='width:16.4pt;padding:0in 5.4pt 0in 5.4pt;
  height:17.75pt'></td>
  <td width=204 nowrap colspan=2 valign=bottom style='width:153.35pt;
  border-top:none;border-left:none;border-bottom:double black 2.25pt;
  border-right:solid black 1.0pt;mso-border-top-alt:solid black .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:17.75pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><s><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>P</span></s><span
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  "Times New Roman"'><o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:27;height:11.1pt'>
  <td width=101 nowrap valign=bottom style='width:1.05in;border:none;
  border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:11.1pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&nbsp;<o:p></o:p></span></p>
  </td>
  <td width=41 nowrap valign=bottom style='width:30.7pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.1pt'></td>
  <td width=22 nowrap valign=bottom style='width:16.35pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.1pt'></td>
  <td width=142 nowrap valign=bottom style='width:106.55pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.1pt'></td>
  <td width=106 nowrap valign=bottom style='width:79.65pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.1pt'></td>
  <td width=22 nowrap valign=bottom style='width:16.4pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.1pt'></td>
  <td width=81 nowrap valign=bottom style='width:60.65pt;padding:0in 5.4pt 0in 5.4pt;
  height:11.1pt'></td>
  <td width=124 nowrap valign=bottom style='width:92.65pt;border:none;
  border-right:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:11.1pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&nbsp;<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:28;height:18.25pt'>
  <td width=101 nowrap valign=bottom style='width:1.05in;border:none;
  border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&nbsp;<o:p></o:p></span></p>
  </td>
  <td width=41 nowrap valign=bottom style='width:30.7pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=22 nowrap valign=bottom style='width:16.35pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=142 nowrap valign=bottom style='width:106.55pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.25pt'></td>
  <td width=333 nowrap colspan=4 valign=bottom style='width:249.4pt;border-top:
  none;border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-bottom-alt:solid black .5pt;mso-border-right-alt:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:18.25pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman"'><o:p>&nbsp;</o:p></span></b></p>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman"'><o:p>&nbsp;</o:p></span></b></p>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman"'>JESSAMYN
  M. BUCLATIN <o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:29;height:18.75pt'>
  <td width=101 nowrap valign=bottom style='width:1.05in;border:none;
  border-left:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:18.75pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&nbsp;<o:p></o:p></span></p>
  </td>
  <td width=41 nowrap valign=bottom style='width:30.7pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.75pt'></td>
  <td width=22 nowrap valign=bottom style='width:16.35pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.75pt'></td>
  <td width=142 nowrap valign=bottom style='width:106.55pt;padding:0in 5.4pt 0in 5.4pt;
  height:18.75pt'></td>
  <td width=333 colspan=4 valign=bottom style='width:249.4pt;border:none;
  border-right:solid black 1.0pt;mso-border-top-alt:solid black .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:18.75pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:9.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman"'>Signature
  over Printed Name Head of<span style='mso-spacerun:yes'>  </span>Accounting
  Division/Unit/Authorized Official<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:30;height:6.05pt'>
  <td width=101 nowrap valign=bottom style='width:1.05in;border-top:none;
  border-left:solid black 1.0pt;border-bottom:solid black 1.0pt;border-right:
  none;padding:0in 5.4pt 0in 5.4pt;height:6.05pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&nbsp;<o:p></o:p></span></p>
  </td>
  <td width=41 nowrap valign=bottom style='width:30.7pt;border:none;border-bottom:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:6.05pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&nbsp;<o:p></o:p></span></p>
  </td>
  <td width=22 nowrap valign=bottom style='width:16.35pt;border:none;
  border-bottom:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:6.05pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&nbsp;<o:p></o:p></span></p>
  </td>
  <td width=248 nowrap colspan=2 valign=bottom style='width:186.2pt;border:
  none;border-bottom:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:6.05pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><sup><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman"'>&nbsp;</span></sup><span
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  "Times New Roman"'><o:p></o:p></span></p>
  </td>
  <td width=22 nowrap valign=bottom style='width:16.4pt;border:none;border-bottom:
  solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:6.05pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><sup><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman"'>&nbsp;</span></sup><span
  style='font-size:10.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
  "Times New Roman"'><o:p></o:p></span></p>
  </td>
  <td width=81 nowrap valign=bottom style='width:60.65pt;border:none;
  border-bottom:solid black 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:6.05pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&nbsp;<o:p></o:p></span></p>
  </td>
  <td width=124 nowrap valign=bottom style='width:92.65pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:6.05pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&nbsp;<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:31;mso-yfti-lastrow:yes;height:11.1pt'>
  <td width=638 nowrap colspan=8 valign=bottom style='width:478.6pt;border:
  solid black 1.0pt;border-top:none;mso-border-top-alt:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:11.1pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman"'>PAYEE’S
  COPY<o:p></o:p></span></b></p>
  </td>
 </tr>
</table>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

</div>

</body>

</html>
