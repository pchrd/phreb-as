@extends('layouts.myApp')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="card">
						<div class="card-header navbar-dark bg-primary">
							<h6>
								<strong class="text text-light">Account Request Sent!</strong>
							</h6>
						</div>

						<div class="card-body">
							<div class="">
								<caption>You have successfully sent the request of your account. Please wait the PHREB Secretariat to verify it. Thank you.</caption>
								<h6 class="text text-danger">Note: If you haven't received email verification, please check the <b><i>SPAM</i></b> Inbox. Thank you!</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

