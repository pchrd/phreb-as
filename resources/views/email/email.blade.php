@component('mail::layout')

{{-- Header --}}
@slot('header')
@component('mail::header', ['url' => config('app.url')])
	<!-- header here -->
	Philippine Health Research Ethics Board
@endcomponent
@endslot

Dear PHREB Secretariat,
						


The account of Mr/Ms. {{$name}} has been created as Research Ethics Committee in the PHREB Accreditation Portal. 

Please see details below to verify the user details. 



Thank you!


@component('mail::table')
    | INFORMATION DETAILS   |        							|
    | :------------- 		| -------------:					| 
    | Name:      			| {{$name}}     					|
    | Email:      			| {{$email}} 						|
    | REC Name:      		| {{$recname}} 						|
    | Applying for Level:   | {{$level}} 						|
@endcomponent

Other Messages: {{$messagetosecretariat ?? 'none'}}

{{-- Action Button --}}
@component('mail::button', ['url' => url('/verifyemail/'.$email_token)])
	Verify Account
@endcomponent

{{-- Footer --}}
@slot('footer')
@component('mail::footer')
	<!-- footer here -->
	PHREB © 2023
@endcomponent

@endslot

@endcomponent