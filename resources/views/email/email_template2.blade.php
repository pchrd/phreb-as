<!DOCTYPE html>
<html>
<head>
  <style>
    /* Email-optimized styling */
    body {
      font-family: Arial, sans-serif;
      margin: 0;
      padding: 0;
      background-color: #f9f9f9;
  }
  .email-container {
      max-width: 600px;
      margin: 0 auto;
      background: #ffffff;
      border: 1px solid #dddddd;
      border-radius: 5px;
      overflow: hidden;
  }
  .header {
      background-color: #004085;
      color: white;
      padding: 20px;
      text-align: center;
  }
  .content {
      padding: 20px;
  }
  .table-container {
      width: 100%;
      margin: 0 auto;
      border-collapse: collapse;
      font-size: 14px;
  }
  .table-container th, .table-container td {
      border: 1px solid #dddddd;
      padding: 8px;
      text-align: left;
  }
  .table-container th {
      background-color: #f2f2f2;
  }
  .month-header {
      background-color: #eaf0f6;
      font-weight: bold;
      padding: 10px;
      text-align: left;
      border-bottom: 2px solid #cccccc;
  }
  .total-row {
      font-weight: bold;
      text-align: center;
      background-color: #d9edf7; /* Light blue background for emphasis */
      border-top: 2px solid #004085;
      color: #004085; /* Blue font color */
  }
  .footer {
      background-color: #f2f2f2;
      text-align: center;
      padding: 10px;
      font-size: 12px;
      color: #666666;
  }
</style>
</head>
<body>
  <div class="email-container">
    <div class="header">
      <h1>Philippine Health Research Ethics Board</h1>
  </div>
  <div class="content">

      @foreach($mapped_recs as $month_year => $group)
      <!-- Display the month -->
      <div class="month-header">{{ $month_year }}</div>

      <table class="table-container">
          <thead>
            <tr>
              <th>REC Information</th>
              <th>Level</th>
              <th>No. of Accredited RECs</th>
          </tr>
      </thead>
      <tbody>
        @foreach ($group['recs'] as $rec)
        <tr>
            <td>{{ $rec['rec_name'] }} <br>
                <small><i style="color: brown;">{{ $rec['date_accreditation'] }} - {{ $rec['date_accreditation_expiry'] }}</i></small>
            </td>
            <td>{{ $rec['rec_level'] }}</td>
            <!-- The last column will only be rendered once due to the rowspan -->
            @if ($loop->first) <!-- Check if it's the first row -->
            <td rowspan="{{ count($group['recs']) }}" style="text-align: center; vertical-align: middle; font-weight: bold; font-size: 16px; color: black;">
                {{ $group['count'] }}
            </td>
            @endif
        </tr>
        @endforeach
   
    </tbody>
</table>

@endforeach
<div style="margin-top: 20px; text-align: center; font-weight: bold; color: #004085;">
    <h3>Grand Total Accredited RECs: {{ $grand_total }}</h3>
</div>

<div class="footer">
  &copy; 2025 Philippine Health Research Ethics Board. All Rights Reserved.
</div>
</div>
</body>
</html>
