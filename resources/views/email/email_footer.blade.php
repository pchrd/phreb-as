 <style>
 @media only screen and (max-width: 600px){
 	.inner-body{
 		width: 100% !important;
 	}

 	.footer{
 		width: 100% !important;
 	}
 }
 @media only screen and (max-width: 500px){
 	.button{
 		width: 100% !important;
 	}
 }
</style>
<tr>
	<td>
		<table class="footer" align="center" width="570" cellpadding="0" cellspacing="0">
			<tr>
				<td class="content-cell" align="left">
					<h2 style="color:blue">PHILIPPINE HEALTH RESEARCH ETHICS BOARD</h2>
					Philippine National Health Research System<br>
					c/o Philippine Council for Health Research and Development<br>
					Department of Science and Technology<br>
					Bicutan, Taguig City 1631, Philippines<br>
					+632 837 7537 (local 403) | +632 837 2924 (fax) | http://ethics.healthresearch.ph/<br>	
				</td>
			</tr>
		</table>
	</td>
</tr>