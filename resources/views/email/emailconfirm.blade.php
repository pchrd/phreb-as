@extends('layouts.myApp')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12 col-md-offset-2">
			<div class="panel panel-default">
			
				<div class="panel-body">
					<div class="card">
						<div class="card-header navbar-dark bg-primary">
						  <h6>
						  	<strong class="text text-light">Registration Confirmed</strong>
						  </h6>
						</div>

						<div class="card-body">
							<div class="">
								<p>The email <b style="color: blue;">{{$user->email}}</b> is successfully verified.<a class="text text-primary" href="{{url('/login')}}"> Click here to login</a></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection