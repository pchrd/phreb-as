@extends('layouts.myApp')
@section('content')

<div class="btn-group-vertical pull-right" style="position: fixed;bottom: 78px;
right: 3px; width:170px; margin-right:20px; padding: 10px 20px;border-radius: 4px;border-color: #46b8da">

<a href="#" class="btn btn-success fa fa-save" data-toggle="modal" data-target="#udpate_rec"> Save Changes</a>
<a href="{{route('view-recdetails', ['id' => encrypt($recdetails->applicationlevel->recdetails_id)])}}" class="btn btn-secondary fa fa-arrow-left"> Back</a>
</div>

<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-10">
			<div class="card-header label-default col-md-12">
				<label class="text-dark"><b>UPDATE RECORDS</b> </label><span class="pull-right small"><b>{{$recdetails->rec_name}} - {{$recdetails->rec_institution}}</b><br> {{$recdetails->applicationlevel->levels->level_name}} - {{$recdetails->applicationlevel->regions->region_name}}</span>
			</div>
			<div class="card-body container-fluid">
				<div class="form-group col-md-12">
					<div class="col-md-12" id="savedclass">
						@if(session('success'))
						<div class="alert alert-success ">
							<i class="fa fa-exclamation"></i>{{session('success')}}
							<button class="close" onclick="document.getElementById('savedclass').style.display='none'">x</button>
						</div>
						@endif
					</div>

					<form method="POST" action="{{ route('update-record', ['id' => encrypt($recdetails->applicationlevel->recdetails_id)]) }}" aria-label="">
						{{csrf_field()}}
						
							<ul class="nav nav-tabs" id="myTab" role="tablist">
								<li class="nav-item">
									<a class="nav-link active" id="er1-tab" data-toggle="tab" href="#er1" role="tab" aria-controls="er1" aria-selected="true">REC Information</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Others</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="assessments-tab" data-toggle="tab" href="#assessments" role="tab" aria-controls="assessments" aria-selected="false">Assessments</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="apce-tab" data-toggle="tab" href="#apce" role="tab" aria-controls="apce" aria-selected="false">Action Plan & Compliance Evidences</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="remarks-tab" data-toggle="tab" href="#remarks" role="tab" aria-controls="remarks" aria-selected="false">Notes</a>
								</li>

								<li class="nav-item">
									<a class="nav-link" id="arps-tab" data-toggle="tab" href="#arps" role="tab" aria-controls="arps" aria-selected="false">Annual Report & Protocol Summary</a>
								</li>


							</ul>
							<div class="container-fluid tab-content" id="myTabContent">
								<div class="tab-pane fade show active" id="er1" role="tabpanel" aria-labelledby="er1-tab"><br>
									<div class="container-fluid">
											@include('recdetails.er1')
									</div>
								</div>

								<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab"><br>
									<div class="container-fluid">
											@include('recdetails.er2')
									</div>
								</div>

								<div class="tab-pane fade" id="assessments" role="tabpanel" aria-labelledby="assessments-tab"><br>
									<div class="container-fluid">
											@include('recdetails.er3')
									</div>
								</div>

								<div class="tab-pane fade" id="apce" role="tabpanel" aria-labelledby="apce-tab"><br>
									<div class="container-fluid">
											@include('recdetails.er4')
									</div>
								</div>

								<div class="tab-pane fade" id="remarks" role="tabpanel" aria-labelledby="remarks-tab"><br>
									<div class="container-fluid">
										<div class="form-group row">
										 <textarea type="text" class="form-control{{ $errors->has('date_accreditation_expiry') ? ' is-invalid' : '' }}" id="" name="existingrec_remarks" style="height: 300px">{{$recdetails->existing_recs->existingrec_remarks}}</textarea>
										</div>
									</div>
								</div>

								<div class="tab-pane fade" id="arps" role="tabpanel" aria-labelledby="arps-tab"><br>
									<div class="container-fluid">
											@include('recdetails.er5')
									</div>
								</div>

							</div>

							
							<!-- Modal for update exsiting REC -->
							<div class="modal fade" id="udpate_rec">
								<div class="modal-dialog sm">
									<div class="modal-content">
										<!-- Modal Header -->
										<div class="modal-header badge badge-success">
											<h4 class="modal-title">Update Confirmation</h4>
											<button type="button" class="close" data-dismiss="modal">&times;</button>
										</div>
										<!-- Modal body -->
										<div class="modal-body">
											<div class="container-fluid">
												<p>Do you want to update the records of <b>{{$recdetails->rec_name}} - {{$recdetails->rec_institution}}</b> ?</p>
												<div class="modal-footer">
													<button type="submit" class="btn btn-success">Save</button>
													<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection