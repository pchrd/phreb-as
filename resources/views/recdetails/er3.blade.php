<div class="form-group row">
<label for="date_deadline_accreditors_ass" class="col-md-4 col-form-label text-md-left">{{ __(' Deadline of Accreditors Assessment Form:') }}</label>

<div class="col-md-8">
    <input id="date_deadline_accreditors_ass" type="text" class="form-control{{ $errors->has('created_at') ? ' is-invalid' : '' }}" name="date_deadline_accreditors_ass" value="{{$recdetails->existing_recs->date_deadline_accreditors_ass}}">

    @if ($errors->has('date_deadline_accreditors_ass'))
    <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('date_deadline_accreditors_ass') }}</strong>
    </span>
    @endif
</div>
</div>

<div class="form-group row">
<label for="date_accreditors_send_ass" class="col-md-4 col-form-label text-md-left">{{ __(' Date Accreditors Sent Assessment Form to the PHREB Secretariat (Level 1 & Level 2):') }}</label>

<div class="col-md-8">
    <input id="date_accreditors_send_ass" type="text" class="form-control{{ $errors->has('created_at') ? ' is-invalid' : '' }}" name="date_accreditors_send_ass" value="{{$recdetails->existing_recs->date_accreditors_send_ass}}">

    @if ($errors->has('date_accreditors_send_ass'))
    <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('date_accreditors_send_ass') }}</strong>
    </span>
    @endif
</div>
</div>

<div class="form-group row">
<label for="date_accreditation_visit_finalreport" class="col-md-4 col-form-label text-md-left">{{ __(' Date of Accreditors Visit Final Report (Level 3):') }}</label>

<div class="col-md-8">
    <input id="date_accreditation_visit_finalreport" type="text" class="form-control{{ $errors->has('created_at') ? ' is-invalid' : '' }}" name="date_accreditation_visit_finalreport" value="{{$recdetails->existing_recs->date_accreditation_visit_finalreport}}">

    @if ($errors->has('date_accreditation_visit_finalreport'))
    <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('date_accreditation_visit_finalreport') }}</strong>
    </span>
    @endif
</div>
</div>

<!-- date rec received acc eval form -->
<div class="form-group row">
<label for="date_rec_recieved_acc_ass" class="col-md-4 col-form-label text-md-left">{{ __(' Date REC Received Accreditors Evaluation Form:') }}</label>

<div class="col-md-8">
    <input id="date_rec_recieved_acc_ass" type="text" class="form-control{{ $errors->has('created_at') ? ' is-invalid' : '' }}" name="date_rec_recieved_acc_ass" value="{{$recdetails->existing_recs->date_rec_recieved_acc_ass}}">

    @if ($errors->has('date_rec_recieved_acc_ass'))
    <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('date_rec_recieved_acc_ass') }}</strong>
    </span>
    @endif
</div>
</div>
