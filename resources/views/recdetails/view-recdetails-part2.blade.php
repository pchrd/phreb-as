<div class="container-fluid">
		<div class="card">
			<div class="card-header">
				<span class="fa fa-clock-o"></span> Dates Monitoring of Accreditors Assessment | 
				<strong>{{$recdetails->rec_name}}</strong>
			</div>
			<div class="card-body">

				<table class="table table-striped table-border" id="recdetails_table">
					<col>
					<colgroup span="2"></colgroup>

					<tr class="label label-dark">
						<th colspan="2" scope="colgroup">• Date Requirements Decked to Accreditors:</th>
					</tr>
					<tr class="label label-default text-sm">
						<th scope="colgroupcolgroup">NAME</th>
						<th scope="colgroup">DATE</th>
						
					</tr>
					@foreach($get_sendaccreditors as $get_sendaccreditor)
					<tbody>
						<tr class="text-sm">
						<td>{{$get_sendaccreditor->name}}</td>
						<td>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$get_sendaccreditor->a_datesubmitted)->toDayDateTimeString()}}</td>
					</tr>
					</tbody>
					@endforeach

					<tr class="label label-dark">
						<th colspan="2" scope="colgroup">• Deadline of PHREB Accreditors Assessment:</th>
					</tr>
					<tr class="label label-default text-sm">
						<th scope="colgroupcolgroup">NAME</th>
						<th scope="colgroup">DEADLINE DATE</th>
					</tr>

					@foreach($get_sendaccreditors as $get_sendaccreditor)
					<tbody>
						<tr class="text-sm">
						<td>{{$get_sendaccreditor->name}}</td>
						<td>
							<span class="text-warning"><b>
									{{\Carbon\Carbon::createFromFormat('Y-m-d',$get_sendaccreditor->a_dateofexpiry)->diffInDays()}}</b> Days Before the deadline</span><br>

							{{\Carbon\Carbon::createFromFormat('Y-m-d',$get_sendaccreditor->a_dateofexpiry)->toDayDateTimeString()}}

							@if($get_sendaccreditor->a_reminder_status != null)
								 <i class="blink_me fa fa-envelope danger" title="Assessment Reminder Successfully Sent on {{$get_sendaccreditor->a_reminder_status}}"> </i>						
							@endif
							@if($get_sendaccreditor->a_reminder_status_deadline != null)
								<span class="label label-danger fa fa-exclamation-circle" title="Assessment Deadline Successfully Sent on {{$get_sendaccreditor->a_reminder_status_deadline}}"></span>
							@endif
						</td>
					</tr>
					</tbody>
					@endforeach

					<tr class="label label-dark">
						<th colspan="2" scope="colgroup">• Date Accreditors Sent Assessment:</th>
					</tr>
					<tr class="label label-default text-sm">
						<th scope="colgroupcolgroup">NAME</th>
						<th scope="colgroup">SUBMISSION DATE</th>
					</tr>

					@foreach($get_recassessment as $get_recassessments)
					<tbody>
						<tr class="text-sm">
						<td>{{$get_recassessments->name}}</td>
						<td><a href="" class="fa fa-file" title="{{$get_recassessments->ra_recdocuments_file}}"></a> {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$get_recassessments->ra_dateuploaded)->toDayDateTimeString()}}</td>
					</tr>
					</tbody>
					@endforeach

					<tr class="label label-dark">
						<th colspan="2" scope="colgroup">• Date REC Received Accreditor's Assessment:</th>
					</tr>
					<tr class="label label-default text-sm">
						<th scope="colgroupcolgroup">FROM CSA CHAIR</th>
						<th scope="colgroup">DATE REC RECIEVED</th>
					</tr>

					@foreach($get_recassessment_recieved as $get_recassessment_recieved1)
					<tbody>
						<tr class="text-sm">
						<td>{{$get_recassessment_recieved1->users_csachair->name}}</td>
						<td><a href="" class="fa fa-file" title="{{$get_recassessment_recieved1->recassessment->ra_recdocuments_file}}"></a> {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$get_recassessment_recieved1->ra_rec_datecreated_at)->toDayDateTimeString()}}</td>
					</tr>
					</tbody>
					@endforeach
			
			</table>
				
			</div>
		</div>
	</div>