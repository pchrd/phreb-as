@extends('layouts.myApp')

@section('content')

@include('recdetails.filtersearch-modal')

<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-14">

			<div class="col-md-12" id="verifyuser">
				@if(session('success'))
				<div class="alert alert-success">
					{{session('success')}}
					<button class="close" onclick="document.getElementById('verifyuser').style.display='none'">x</button>
				</div>
				@endif
			</div> 


			<div class="card">
				<div class="card-header bg-maroon">
					<h1>REC Application List

						@can('secretariat-access')
							 <a href="#" data-toggle="modal" data-target="#searchfilter" class="pull-right small text text-light"> | <span class="fa fa-filter"></span> Search Filter </a>
						@endcan
					
							<a href="{{ url('/dashboard') }}" class="pull-right small text text-light"><span class="fa fa-arrow-left"></span> Go to Dashboard </a>
						
					</h1>
				</div>

				<div class="card-body">

					<table class="table table-condensed table-bordered table-hover" id="recdetails_table">
						<thead>
							<tr>
								<td scope="col">ID NUMBER</td>
								<td scope="col">CATEGORY TYPE</td>
								<td scope="col">REGION</td>
								<td scope="col">LEVEL</td>

								<td scope="col">INTITUTION NAME</td>
								<td scope="col">REC NAME</td>

								<td scope="col">REC EMAIL</td>
								<td scope="col">DATE CREATED</td>
								<td scope="col">DATE ACCREDITED</td>
								<td scope="col">DATE ACCREDITATION EXPIRY</td>
								<td scope="col">CURRENT STATUS</td>
								<td scope="col">ACCREDITATION</td>

								@can('can:secretariat-access')
								<td scope="col">REMARKS/NOTES</td>
								@endcan

								<td scope="col">ACTIONS</td>
							</tr>
						</thead>
					</table>

					<div class="container-fluid">
						<b><p>Legend:

							<i class="badge badge-danger">Indicates expired </i > 
							<i class="badge badge-warning">Indicates 1 month before expiration </i>
							
						</p> 
					</b>
				</div> 

			</div>
		</div>
	</div>
</div>
</div>



<script type="text/javascript">

	$(function(){
		
		// alert(moment().add(1, 'M').format('YYYY-MM-DD'))
		var oTable = $('#recdetails_table').DataTable({

			responsive: true,
			
			dom: '<"fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix ui-corner-tl ui-corner-tr"HlfrB>'+
			't'+'<"fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix ui-corner-bl ui-corner-br"ip>',

			buttons:[
        		// ['copy', 'csv', 'excel', 'pdf', 'print'],
        		{  
        			extend: 'colvis',

        		},

        		{ 
        			extend: 'excel',
        			footer: true,
        			exportOptions: {
        				columns: []
        			}
        		},

        		{  
        			extend: 'copy',

        		},

        		{  
        			extend: 'csv',

        		},

        		{  
        			extend: 'pdf',

        		},
        		{  
        			extend: 'print',
        			message: "REC Records",

        			customize: function (win) {
        				$(win.document.body)
        				.css('font-size', '10pt')
        				.prepend(
        					'<img src="{{asset('image/image002.jpg')}}" style="position:static; top:10px; left:10px;" />'
        					);

        				$(win.document.body).find('table')
        				.addClass('compact')
        				.css('font-size', 'inherit');
        			}
        		}

        		],

        		stateSave: false,
        		processing: true,
        		serverSide: true,
        		ajax: {
        			url: '{{ route('recdetails/show-recdetails') }}',
        		},

        		columns: [
        		{ data: 'recdetails_id',   name: 'recdetails.id', searchable:true, visible:true},
        		{ data: 'status.status_name', name: 'status.status_name'},
        		{ data: 'region_name', name: 'regions.region_name', visible:true},
        		{ data: 'level_description', name: 'levels.level_description'},

        		{ data: 'rec_institution', name: 'recdetails.rec_institution'},
        		{ data: 'rec_name', name: 'recdetails.rec_name', visible:true},

        		{ data: 'rec_email', name: 'recdetails.rec_email', visible:false},
        		{ data: 'created_at', name: 'applicationlevel.created_at', visible:true},
        		{ data: 'date_accreditationFORMAT', name: 'applicationlevel.date_accreditation'},
        		{ data: 'date_accreditation_expiryFORMAT', name: 'applicationlevel.date_accreditation_expiry'},
        		{ data: 'status_name', name: 'statuses.status_name'},
        		{ data: 'rec_accreditation', name: 'applicationlevel.rec_accreditation', visible:false},

			// { data: 'rec_change_of_acc', name: 'rec_change_of_acc', orderable: false, searchable: false},
			
			{ data: 'action', name:'action', orderable: false, searchable: false}
			],

			createdRow: function(row, data, dataIndex){
				
				if (data['date_accreditation_expiry'] <= moment().format('YYYY-MM-DD')){
					$(row).find('td:eq(7)').addClass('text text-danger');
					$(row).find('td:eq(8)').addClass('text text-danger');

				}else if (data['date_accreditation_expiry'] <= moment().add(1, 'M').format('YYYY-MM-DD')){
					$(row).find('td:eq(7)').addClass('text text-warning');
					$(row).find('td:eq(8)').addClass('text text-warning');
				}
			}

		});
	});
</script>
@endsection