<div class="form-group row">
<label for="date_rec_sent_apce" class="col-md-4 col-form-label text-md-left">{{ __(' Date REC Sent Action Plan & Compliance Evidences:') }}</label>

<div class="col-md-8">
    <input id="date_rec_sent_apce" type="text" class="form-control{{ $errors->has('created_at') ? ' is-invalid' : '' }}" name="date_rec_sent_apce" value="{{$recdetails->existing_recs->date_rec_sent_apce}}">

    @if ($errors->has('date_rec_sent_apce'))
    <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('date_rec_sent_apce') }}</strong>
    </span>
    @endif
</div>
</div>

<div class="form-group row">
<label for="date_accreditors_sent_apce" class="col-md-4 col-form-label text-md-left">{{ __(' Date Accreditors Sent the Evaluation of Action Plan & Compliance Evidences:') }}</label>

<div class="col-md-8">
    <input id="date_accreditors_sent_apce" type="text" class="form-control{{ $errors->has('created_at') ? ' is-invalid' : '' }}" name="date_accreditors_sent_apce" value="{{$recdetails->existing_recs->date_accreditors_sent_apce}}">

    @if ($errors->has('date_accreditors_sent_apce'))
    <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('date_accreditors_sent_apce') }}</strong>
    </span>
    @endif
</div>
</div>

<div class="form-group row">
<label for="date_forward_to_csachair" class="col-md-4 col-form-label text-md-left">{{ __(' Date PHREB Sec forward to CSA Chair the Accreditors evaluation of Action Plan and Compliance Evidences
:') }}</label>

<div class="col-md-8">
    <input id="date_forward_to_csachair" type="text" class="form-control{{ $errors->has('created_at') ? ' is-invalid' : '' }}" name="date_forward_to_csachair" value="{{$recdetails->existing_recs->date_forward_to_csachair}}">

    @if ($errors->has('date_forward_to_csachair'))
    <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('date_forward_to_csachair') }}</strong>
    </span>
    @endif
</div>
</div>