	<br><div class="container-fluid">
		<div class="card">
			<div class="card-header">
				<span class="fa fa-clock-o"></span> Dates Monitoring of Action Plan & Compliance Evidences | 
				<strong>{{$recdetails->rec_name}}</strong>
			</div>
			<div class="card-body">
				
				<table class="table table-striped table-border" id="recdetails_table">
					<col>
					<colgroup span="2"></colgroup>

					<tr class="label label-dark">
						<th colspan="2" scope="colgroup">• Date REC sent Action Plan & Compliance Evidences:</th>
					</tr>
					<tr class="label label-default text-sm">
						<th scope="colgroupcolgroup">FILENAME</th>
						<th scope="colgroup">DATE</th>
					</tr>
					@foreach($get_recactionplan as $get_recactionplans)
					<tbody>
						<tr class="text-sm">
						<td><span class="fa fa-file" title="{{$get_recactionplans->ap_recactionplan_uniqid}}"></span> {{$get_recactionplans->ap_recactionplan_name}}</td>
						<td>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$get_recactionplans->created_at)->toDayDateTimeString()}}</td>
					</tr>
					</tbody>
					@endforeach

					<tr class="label label-dark">
						<th colspan="2" scope="colgroup">• Date of Acreditors Sent Action Plan & Compliance Evidences:</th>
					</tr>
					<tr class="label label-default text-sm">
						<th scope="colgroupcolgroup">NAME OF ACCREDITORS</th>
						<th scope="colgroup">DATE</th>
					</tr>
					@foreach($get_recactionplan_acc as $get_recactionplan_accs)
					<tbody>
						<tr class="text-sm">
						<td><span class="fa fa-file" title="{{$get_recactionplan_accs->ap_recactionplan_uniqid}} | {{$get_recactionplan_accs->ap_recactionplan_name}}"></span> {{$get_recactionplan_accs->name}} 
							@if($get_recactionplan_accs->ap_submittedto_rec != null)
							<span class="fa fa-check badge-success" title="Successfully sent to REC!"></span>
							@endif
						</td>
						<td>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$get_recactionplan_accs->ap_status)->toDayDateTimeString()}}</td>
					</tr>
					</tbody>
					@endforeach

					<tr class="label label-dark">
						<th colspan="2" scope="colgroup">• Date forwarded to CSA Chair:</th>
					</tr>
					<tr class="label label-default text-sm">
						<th scope="colgroupcolgroup">NAME OF CSA-CHAIR</th>
						<th scope="colgroup">DATE</th>
					</tr>
					@foreach($get_recactionplan_csa as $get_recactionplan_csas)
					<tbody>
						<tr class="text-sm">
						<td><b>{{$get_recactionplan_csas->users_csachair->name}}</b><br>
							Forwarded by: {{$get_recactionplan_csas->name}}
						</td>
						<td>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$get_recactionplan_csas->ap_csa_datecreated_at)->toDayDateTimeString()}}</td>
					</tr>
					</tbody>
					@endforeach
			</table>
			</div>
		</div>
	</div>