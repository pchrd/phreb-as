<!--filtersearh modal -->
<div class="modal fade" id="searchfilter" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Filter Search Records</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<form action="{{route('filtersearch')}}" method="GET">
						{{csrf_field()}}

						<div class="container">
							<div class="form-group row">
								<label for="start" class="col-md-4 col-form-label text-md-left"><h6>{{ __(' Start Date') }}</h6></label>
								<div class="col-md-8">
									<input type="date" class="form-control{{ $errors->has('start') ? ' is-invalid' : '' }}" name="start" value="{{$request->has('start') ? $request->start : ''}}" required>
									
									@if ($errors->has('start'))
									<span class="invalid-feedback" role="alert"><strong>{{ $errors->first('start') }}</strong></span>
									@endif
								</div>
							</div>

							<div class="form-group row">
								<label for="end" class="col-md-4 col-form-label text-md-left"><h6>{{ __(' End Date') }}</h6></label>
								<div class="col-md-8">
									<input type="date" class="form-control{{ $errors->has('end') ? ' is-invalid' : '' }}" name="end" value="{{$request->has('end') ? $request->end : ''}}" required>
									
									@if ($errors->has('end'))
									<span class="invalid-feedback" role="alert"><strong>{{ $errors->first('end') }}</strong></span>
									@endif
								</div>
							</div>

							<div class="form-group row">
								<label for="status" class="col-md-4 col-form-label text-md-left"><h6>{{ __(' Status of Application') }}</h6></label>
								<div class="col-md-8">
									<select id="status" type="text" class="form-control{{ $errors->has('status') ? ' is-invalid' : '' }}" name="status" value="{{ old('status') }}" placeholder="" autofocus id="status" required>
										<option value="" disabled selected>---Please select---</option>
						
										@foreach($status as $id => $statuses)
											<option value="{{$id}}">{{ $statuses }}</option>
										@endforeach
						
									</select>
									@if ($errors->has('status'))
									<span class="invalid-feedback" role="alert"><strong>{{ $errors->first('status') }}</strong></span>
									@endif
								</div>
							</div>


							<div class="form-group row">
								<label for="level" class="col-md-4 col-form-label text-md-left"><h6>{{ __(' Level') }}</h6></label>
								<div class="col-md-8">
									<select id="level" type="text" class="form-control{{ $errors->has('level') ? ' is-invalid' : '' }}" name="level" value="{{ old('level') }}" placeholder="" autofocus id="level">
										<option value="" disabled selected>---Please select---</option>
						
										@foreach($selectlevels as $id => $selectlevel)
											<option value="{{$id}}">{{ $selectlevel }}</option>
										@endforeach
						
									</select>
									@if ($errors->has('level'))
									<span class="invalid-feedback" role="alert"><strong>{{ $errors->first('level') }}</strong></span>
									@endif
								</div>
							</div>

							<div class="form-group row">
								<label for="region" class="col-md-4 col-form-label text-md-left"><h6>{{ __(' Region') }}</h6></label>
								<div class="col-md-8">
									<select id="region" type="text" class="form-control{{ $errors->has('region') ? ' is-invalid' : '' }}" name="region" value="{{ old('region') }}" placeholder="" autofocus id="Region">
										<option value="" disabled selected>---Please select---</option>

										@foreach($selectregions as $id => $selectregion)
											r<option value="{{$id}}">{{ $selectregion }}</option>
										@endforeach
										
									</select>
									@if ($errors->has('region'))
									<span class="invalid-feedback" role="alert"><strong>{{ $errors->first('region') }}</strong></span>
									@endif
								</div>
							</div>

							<div class="modal-footer">
								<button type="submit" class="btn btn-info"><span class="fa fa-search"></span> Search</button>
							</div>

						</div>
					
					</form>
				</div> 
			</div>
			</div>
		</div>
	</div>