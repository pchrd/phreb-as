@extends('layouts.myApp')
@section('content')

@if($recdetails->rec_existing_record == 1)

	<div class="btn-group-vertical pull-right" style="position: fixed;bottom: 88px;
	right: 3px; width:200px; margin-right:30px; padding: 10px 20px;border-radius: 4px;border-color: #46b8da">
	<a href="{{ route('edit-record', ['id' => encrypt($recdetails->applicationlevel->recdetails_id)]) }}" class="btn btn-success fa fa-pencil"> Click Here to Edit</a>
	<a href="#" data-toggle="modal" data-target="#newacc" class="btn btn-default  fa fa-share" > Change Accreditation</a>
	<a href=""  data-toggle="modal" data-target="#arps" class="btn btn-info  fa fa-upload"  data-backdrop="static" data-keyboard="false"> Upload Annual Report...</a>

	<a href="{{url('show-recdetails')}}" class="btn btn-secondary btn-sm" title="Back"><span class="fa fa-arrow-circle-o-left"></span> Go Back</a>

	</div>

@endif


@if($recdetails->applicationlevel->recdetails_submission_status == 1 AND $recdetails->rec_existing_record != 1)
	<div class="btn-group-vertical pull-right" style="position: fixed;bottom: 88px;
		right: 3px; width:200px; margin-right:30px; padding: 10px 20px;border-radius: 4px;border-color: #46b8da">

		<a href="{{url('show-recdetails')}}" class="btn btn-secondary" title="Back"><span class="fa fa-arrow-circle-o-left"></span> Go Back</a>

	</div>
@endif


<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-10">
			<div class="card-header col-md-12">

				<center>
					<label class="text-dark center"><h1>{{$recdetails->applicationlevel->levels->level_name}} <br> {{$recdetails->rec_institution}} - {{$recdetails->rec_name}}</h1></label>

				</center>


			</div>

				

			<div class="col-md-12" id="savedclass">
				@if(session('success'))
				<div class="alert alert-success ">
					<i class="fa fa-exclamation"></i>{{session('success')}}
					<button class="close" onclick="document.getElementById('savedclass').style.display='none'">x</button>
				</div>
				@endif
			</div>






			@can('rec-access')

			@if($recdetails->applicationlevel->recdetails_submission_status == 0)

			<div id="stepper-example" class="bs-stepper">
				<div class="bs-stepper-header">
					<div class="step" data-target="#test-l-1">
						<a href="#">
							<span class="bs-stepper-circle">1</span>
							<span class="bs-stepper-label badge badge-info"><u>First step</u></span>
						</a>
					</div>
					<div class="line"></div>
					<div class="step" data-target="#test-l-2">
						<a href="#" data-toggle="modal" data-target="#savechanges">
							<span class="bs-stepper-circle">2</span>
							<span class="bs-stepper-label">Second step</span>
						</a>
					</div>
					<div class="line"></div>
					<div class="step" data-target="#test-l-3">
						<a href="#">
							<span class="bs-stepper-circle">3</span>
							<span class="bs-stepper-label">Third step</span>
						</a>
					</div>
				</div>
			</div>

			@endif

			@endcan





			<ul class="nav nav-tabs" id="myTab" role="tablist">
				<li class="nav-item">
					<a class="nav-link active" id="p1-tab" data-toggle="tab" href="#p1" role="tab" aria-controls="p1" aria-selected="true"><span class="fa fa-file"></span> REC General Information</a>
				</li>


				<li>

						@can('rec-access')
					<!-- upload acpce -->
						@if($recdetails->rec_apce == true)
							<a href="{{ route('actionplan', ['id' => encrypt($recdetails->applicationlevel->recdetails_id)]) }}" class="nav-link" title=""><span class="fa fa-folder"></span> Action Plan and Compliance Evidences</a>
						@endif

						@if($recdetails->applicationlevel->recdetails_submission_status != 0)
						<!-- for rec users -->
							<a href="{{ route('documents', ['id' => encrypt($recdetails->applicationlevel->recdetails_id)]) }}" class="nav-link" title="View Documents"><span class="fa fa-folder"></span> My Requirements</a>
						@endif

					@endcan

					@can('accreditors_and_secretariat')
						<a href="{{ route('documents', ['id' => encrypt($recdetails->applicationlevel->recdetails_id)]) }}" class="nav-link" title="View Documents"><span class="fa fa-folder"></span> Requirements/Documents of {{str_limit($recdetails->rec_institution, 20)}} </a>
					@endcan

					@can('csachair-access')
						<a href="{{ route('documents', ['id' => encrypt($recdetails->applicationlevel->recdetails_id)]) }}" class="nav-link" title="View Documents"><span class="fa fa-folder"></span> {{$recdetails->rec_institution}} Requirements/Documents</a>
					@endcan
					
				</li>
			</ul>


			<div class="tab-content" id="myTabContent">
				<div class="tab-pane fade show active" id="p1" role="tabpanel" aria-labelledby="p1-tab">
					@if($recdetails->applicationlevel->recdetails_lock_fields == 0)<fieldset>@else<fieldset disabled>@endif 

						<!-- MAIN BODY FOREACH  -->
						<div class="container-fluid">
							@unless (Auth::check()) You are not signed in. @endunless
							<form method="POST" action="{{ route('update-recdetails', ['recdetail' => encrypt($recdetails->applicationlevel->recdetails_id)]) }}" aria-label="">
								{{csrf_field()}}

								<div class="form-group col-md-12">
									<strong>
										@can('secretariat-access')
										<h4> Research Ethics Committee Details
											@if($recdetails->applicationlevel->date_accreditation_expiry <= Carbon\Carbon::today()->format('Y-m-d') AND $recdetails->applicationlevel->level_id == 4) 
												<small class="text text-danger blink_me pull-right"><a href="#" data-toggle="modal" data-target="#sendexpirynotifs" title="Click here to send reminder notification" class="text text-danger"><span class="text text-danger fa fa-exclamation-circle"></span> Accreditation Expired, click here to remind</a>
												</small>
											@endif
											@endcan
										</h4>
									</strong>
									<!-- custom select box select apptype -->
									<div class="form-group row">
										<label for="selectapptype" class="col-md-4 col-form-label text-md-left">{{ __('Type of Application*') }}</label>

										<div class="col-md-8">
											<select id="selectapptype" type="text" class="form-control{{ $errors->has('selectapptype') ? ' is-invalid' : '' }}" name="selectapptype" value="{{ old('selectapptype') }}" placeholder="" required autofocus>
												<option value="{{$recdetails->rec_apptype_id}}">
													{{$getapptype->status_name}}
												</option>
												<!-- get the data in db and put in select -->
												@foreach($selectapptype as $id => $selectapptype)
												<option value="{{$id}}">{{$selectapptype}}</option>
												@endforeach

											</select>

											@if ($errors->has('selectapptype'))
											<span class="invalid-feedback">
												<strong>{{ $errors->first('selectapptype') }}</strong>
											</span>
											@endif
										</div>
									</div>

									<!-- custom select box lvl of accreditation -->
									<div class="form-group row">
										<label for="selectlevel" class="col-md-4 col-form-label text-md-left"><b></b>{{ __('Level of Accreditation*') }}</label>

										<div class="col-md-8">
											<select id="selectlevel" type="text" class="form-control{{ $errors->has('selectlevel') ? ' is-invalid' : '' }}" name="selectlevel" value="{{ old('selectlevel') }}" placeholder="">

												<option value="{{$recdetails->applicationlevel->level_id}}">{{ $recdetails->applicationlevel->levels->level_name }}</option>

												@foreach($selectlevels as $id => $selectlevel)
												<option value="{{$id}}">{{ $selectlevel }}</option>
												@endforeach
											</select>
											@if ($errors->has('selectlevel'))
											<span class="invalid-feedback">
												<strong>{{ $errors->first('selectlevel') }}</strong>
											</span>
											@endif
										</div>
									</div>
									<!-- rec_region -->
									<div class="form-group row">
										<label for="rec_region" class="col-md-4 col-form-label text-md-left"><b></b>{{ __('Region*') }}</label>

										<div class="col-md-8">
											<select id="rec_region" type="text" class="form-control{{ $errors->has('rec_region') ? ' is-invalid' : '' }}" name="rec_region" value="{{ old('rec_region') }}" placeholder="">

												<option value="{{$recdetails->applicationlevel->region_id}}">{{ $recdetails->applicationlevel->regions->region_name }}</option>

												@foreach($selectregions as $id => $selectregion)
												<option value="{{$id}}">{{$selectregion}}</option>
												@endforeach

											</select>
											@if ($errors->has('rec_region'))
											<span class="invalid-feedback">
												<strong>{{ $errors->first('rec_region') }}</strong>
											</span>
											@endif
										</div>
									</div>

									<!-- name of REC -->
									<div class="form-group row">
										<label for="rec_name" class="col-md-4 col-form-label text-md-left">{{ __('Name of REC*:') }}</label>

										<div class="col-md-8">
											<input id="rec_name" type="text" class="form-control{{ $errors->has('rec_name') ? ' is-invalid' : '' }}" name="rec_name" value="{{$recdetails->rec_name}}" required="">

											@if ($errors->has('rec_name'))
											<span class="invalid-feedback" role="alert">
												<strong>{{ $errors->first('rec_name') }}</strong>
											</span>
											@endif
										</div>
									</div>

									<!-- name of Institution -->
									<div class="form-group row">
										<label for="rec_institution" class="col-md-4 col-form-label text-md-left">{{ __('Name of Institution*:') }}</label>

										<div class="col-md-8">
											<input id="rec_institution" type="text" class="form-control{{ $errors->has('rec_institution') ? ' is-invalid' : '' }}" name="rec_institution" required="" value="{{$recdetails->rec_institution}}">

											@if ($errors->has('rec_institution'))
											<span class="invalid-feedback" role="alert">
												<strong>{{ $errors->first('rec_institution') }}</strong>
											</span>
											@endif
										</div>
									</div>

									<!-- REC Address -->
									<div class="form-group row">
										<label for="rec_address" class="col-md-4 col-form-label text-md-left">{{ __('REC Address*:') }}</label>

										<div class="col-md-8">
											<input id="rec_address" type="text" class="form-control{{ $errors->has('rec_address') ? ' is-invalid' : '' }}" name="rec_address" required="" value="{{$recdetails->rec_address}}">

											@if ($errors->has('rec_address'))
											<span class="invalid-feedback" role="alert">
												<strong>{{ $errors->first('rec_address') }}</strong>
											</span>
											@endif
										</div>
									</div>

									<!-- REC Email Address -->
									<div class="form-group row">
										<label for="rec_email" class="col-md-4 col-form-label text-md-left">{{ __('REC Email*:') }}</label>

										<div class="col-md-8">
											<input id="rec_email" type="email" class="form-control{{ $errors->has('rec_email') ? ' is-invalid' : '' }}" name="rec_email" required="" value="{{$recdetails->rec_email}}">

											@if ($errors->has('rec_email'))
											<span class="invalid-feedback" role="alert">
												<strong>{{ $errors->first('rec_email') }}</strong>
											</span>
											@endif
										</div>
									</div>

									<!-- REC Tel No -->
									<div class="form-group row">
										<label for="rec_telno" class="col-md-4 col-form-label text-md-left">{{ __('REC Tel No:') }}</label>

										<div class="col-md-8">
											<input id="rec_telno" type="text" class="form-control{{ $errors->has('rec_telno') ? ' is-invalid' : '' }}" name="rec_telno" value="{{$recdetails->rec_telno}}">

											@if ($errors->has('rec_telno'))
											<span class="invalid-feedback" role="alert">
												<strong>{{ $errors->first('rec_telno') }}</strong>
											</span>
											@endif
										</div>
									</div>

									<!-- REC Fax No -->
									<div class="form-group row">
										<label for="rec_faxno" class="col-md-4 col-form-label text-md-left">{{ __('REC Fax No:') }}</label>

										<div class="col-md-8">
											<input id="rec_faxno" type="text" class="form-control{{ $errors->has('rec_faxno') ? ' is-invalid' : '' }}" name="rec_faxno" value="{{$recdetails->rec_faxno}}">

											@if ($errors->has('rec_faxno'))
											<span class="invalid-feedback" role="alert">
												<strong>{{ $errors->first('rec_faxno') }}</strong>
											</span>
											@endif
										</div>
									</div>
								</div><br>

								<div class="form-group col-md-12">

									<strong><h4> Reasearch Ethics Committee Chair Details</h4></strong>

									<!-- name of REC chair -->
									<div class="form-group row">
										<label for="rec_chairname" class="col-md-4 col-form-label text-md-left">{{ __('Name of REC Chair*:') }}</label>

										<!-- for recchairs inner join -->


										<div class="col-md-8">
											<input id="rec_chairname" type="text" class="form-control{{ $errors->has('rec_chairname') ? ' is-invalid' : '' }}" name="rec_chairname" value="{{$recdetails->recchairs->rec_chairname}}" required="">

											@if ($errors->has('rec_chairname'))
											<span class="invalid-feedback" role="alert">
												<strong>{{ $errors->first('rec_chairname') }}</strong>
											</span>
											@endif
										</div>
									</div>

									<!-- email of REC chair -->
									<div class="form-group row">
										<label for="rec_chairemail" class="col-md-4 col-form-label text-md-left">{{ __('Email of REC Chair*:') }}</label>

										<div class="col-md-8">
											<input id="rec_chairemail" type="text" class="form-control{{ $errors->has('rec_chairemail') ? ' is-invalid' : '' }}" name="rec_chairemail" value="{{$recdetails->recchairs->rec_chairemail}}" required="">

											@if ($errors->has('rec_chairemail'))
											<span class="invalid-feedback" role="alert">
												<strong>{{ $errors->first('rec_chairemail') }}</strong>
											</span>
											@endif
										</div>
									</div>

									<!-- mobile of REC chair -->
									<div class="form-group row">
										<label for="rec_chairmobile" class="col-md-4 col-form-label text-md-left">{{ __('Mobile No. of REC Chair*:') }}</label>

										<div class="col-md-8">
											<input id="rec_chairmobile" type="text" class="form-control{{ $errors->has('rec_chairmobile') ? ' is-invalid' : '' }}" name="rec_chairmobile" value="{{$recdetails->recchairs->rec_chairmobile}}" required="">

											@if ($errors->has('rec_chairmobile'))
											<span class="invalid-feedback" role="alert">
												<strong>{{ $errors->first('rec_chairmobile') }}</strong>
											</span>
											@endif
										</div>
									</div>
								</div><br>



								<div class="form-group col-md-12">
									<strong><h4> Contact Details</h4></strong>

									<!-- REC contact person -->
									<div class="form-group row">
										<label for="rec_contactperson" class="col-md-4 col-form-label text-md-left">{{ __('Contact Person*:') }}</label>

										<div class="col-md-8">
											<input id="rec_contactperson" type="text" class="form-control{{ $errors->has('rec_contactperson') ? ' is-invalid' : '' }}" name="rec_contactperson" required="" value="{{$recdetails->rec_contactperson}}">

											@if ($errors->has('rec_contactperson'))
											<span class="invalid-feedback" role="alert">
												<strong>{{ $errors->first('rec_contactperson') }}</strong>
											</span>
											@endif
										</div>
									</div>

									<!-- contactperson position -->
									<div class="form-group row">
										<label for="rec_contactposition" class="col-md-4 col-form-label text-md-left">{{ __('Contact Person Position:') }}</label>

										<div class="col-md-8">
											<input id="rec_contactposition" type="text" class="form-control{{ $errors->has('rec_contactposition') ? ' is-invalid' : '' }}" name="rec_contactposition" value="{{$recdetails->rec_contactposition}}">

											@if ($errors->has('rec_contactposition'))
											<span class="invalid-feedback" role="alert">
												<strong>{{ $errors->first('rec_contactposition') }}</strong>
											</span>
											@endif
										</div>
									</div>

									<!-- contactperson mobile no -->
									<div class="form-group row">
										<label for="rec_contactmobileno" class="col-md-4 col-form-label text-md-left">{{ __('Contact Person Mobile No:') }}</label>

										<div class="col-md-8">
											<input id="rec_contactmobileno" type="text" class="form-control{{ $errors->has('rec_contactmobileno') ? ' is-invalid' : '' }}" name="rec_contactmobileno" value="{{$recdetails->rec_contactmobileno}}">

											@if ($errors->has('rec_contactmobileno'))
											<span class="invalid-feedback" role="alert">
												<strong>{{ $errors->first('rec_contactmobileno') }}</strong>
											</span>
											@endif
										</div>
									</div>

									<!-- contact person email -->
									<div class="form-group row">
										<label for="rec_contactemail" class="col-md-4 col-form-label text-md-left">{{ __('Contact Person Email:*') }}</label>

										<div class="col-md-8">
											<input id="rec_contactemail" type="email" class="form-control{{ $errors->has('rec_contactemail') ? ' is-invalid' : '' }}" name="rec_contactemail" required="" value="{{$recdetails->rec_contactemail}}">

											@if ($errors->has('rec_contactemail'))
											<span class="invalid-feedback" role="alert">
												<strong>{{ $errors->first('rec_contactemail') }}</strong>
											</span>
											@endif
										</div>
									</div>

									<!-- Date of REC Established -->
									<div class="form-group row">
										<label for="rec_dateestablished" class="col-md-4 col-form-label text-md-left">{{ __('Date REC Established:*') }}</label>

										<div class="col-md-8">
											<input id="rec_dateestablished" type="date" class="form-control{{ $errors->has('rec_dateestablished') ? ' is-invalid' : '' }}" name="rec_dateestablished" required="" value="{{$recdetails->rec_dateestablished}}">

											@if ($errors->has('rec_dateestablished'))
											<span class="invalid-feedback" role="alert">
												<strong>{{ $errors->first('rec_dateestablished') }}</strong>
											</span>
											@endif
										</div>
									</div><br>
								</div>

								<!-- footer -->
								<div class="">
									<div class="form-group">

										@can('rec-access')

											@if($recdetails->applicationlevel->recdetails_lock_fields === 0)
												
												<a href="#" class="btn btn-success fa fa-save btn-block" data-toggle="modal" data-target="#savechanges" data-backdrop="static"> Save and Proceed to step 2</a>

												<a href="{{ route('withdraw_application', ['id' => $recdetails->id]) }}" 
												   class="btn btn-secondary fa fa-times btn-block" 
												   onclick="return confirmWithdraw()">
												   Withdraw Application
												</a>

												<script>
												    function confirmWithdraw() {
												        return confirm("Are you sure you want to withdraw the application?");
												    }
												</script>

												<a href="{{url('show-recdetails')}}" class="btn btn-danger btn-block" title="Back"><span class="fa fa-arrow-left"></span> Cancel</a>

											@endif

										<!-- 	<a href="" class="btn btn-primary fa fa-send {{$recdetails->applicationlevel->recdetails_lock_fields === 0 ? '' : 'hide'}}" data-toggle="modal" data-target="#submit_to_phreb"> Submit to PHREB</a> -->


										@endcan
									</div>	
								</div>				

								<!-- Modal for save changes -->
								<div class="modal fade" id="savechanges" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header badge badge-success">
												<h5 class="modal-title" id="exampleModalLabel">Save Confirmation</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												Do you want to update this record and proceed to step 2 for the uploading of documents?

												<small class="small text text-danger">Note: Once you saved only, you can update or edit your this field again.</small>
											</div>
											<div class="modal-footer">
												<button type="submit" class="btn btn-success">Ok</button>
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
											</div>
										</div>
									</div>
								</div>

								<!-- Modal for Submit to PHREB -->
								<div class="modal fade" id="submit_to_phreb" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header badge badge-primary">
												<h5 class="modal-title" id="exampleModalLabel">Submit Confirmation</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												Do you want to submit your Application to PHREB?
												<p class="small text text-danger">Note: Once you submitted, this will not allow you to update or edit your application. Please make sure you've uploaded files/requirements completely.</p>
											</div>
											<div class="modal-footer">
												<a href="{{ route('submit_recdetails', ['id' => encrypt($recdetails->applicationlevel->recdetails_id)]) }}" class="btn btn-primary">Submit</a>
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
											</div>
										</div>
									</div>
								</div>

								<!--   //save status/datecompleted modal --> 
								<div class="modal bs-example-modal-sm-update_rec_status" tabindex="-1" role="dialog" aria-hidden="true">
									<div class="modal-dialog modal-sm">
										<div class="modal-content">
											<div class="modal-header badge badge-info">
												<h6>Status Confirmation Alert</h6> <span class="fa fa-exclamation"></span>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											</div>
											<div class="modal-body">Do you want to update status?</div>
											<div class="modal-footer">
												<button type="submit"class="btn btn-secondary btn-lg btn-block" title="Update REC Status">Yes</button>
											</div>
										</div>
									</div>
								</div> 
							</form>
						</fieldset>	

						@if($recdetails->rec_existing_record != 1)

						@can('can-see-pending-inc-complete')
						<div class="container-fluid">
							<div class="card">
								<div class="card-header"><strong style="color: blue;">ACTIONS IN REASEARCH ETHICS COMMITTEE APPLICATION</strong></div>
								<div class="card-body">
									<form action="{{route('update_rec_status', ['id' => encrypt($recdetails->applicationlevel->recdetails_id)])}}" method="Post" enctype="multipart/form-data">

										{{csrf_field()}}

										<div class="form-group row">
											<label for="" class="col-md-4 col-form-label text-md-left text text-danger"><h3>{{ __('Application Status*') }}</h3></label>

											<div class="col-md-8">
												<select id="status" type="text" class="form-control{{ $errors->has('status') ? ' is-invalid' : '' }}" name="status" value="{{ old('status') }}" placeholder="">

													<option value="{{$recdetails->applicationlevel->statuses_id}}">{{$recdetails->applicationlevel->status->status_name}}</option>

													@foreach($selectstatuses as $id => $status)
													<option value="{{$id}}">{{$status}}</option>
													@endforeach
												</select>
											</div>
										</div>




										<div id="emailmessage" style="display: none;">

											<div class="form-group row">
												<label for="" class="col-md-4 col-form-label text-md-left text text-danger"><h3>{{ __('Email Message') }}</h3></label>

												<div class="col-md-8">
													<textarea class="form-control" id="editor2" name="emailmessage"></textarea>
												</div>
											</div>

											<div class="form-group row">
												<label for="" class="col-md-4 col-form-label text-md-left text text-danger"><h3>{{ __('File Attachment(s)') }}</h3></label>
											
												<div class="col-md-8">
													<input type="file" class="form-control" name="emailattachment[]" multiple="true" accept="application/jpg,jpeg,png,pdf,docx,doc,zip">

													<p class="small"><i>Use .pdf, .docx, .doc, .jpg, and .png format only.</i></p>

												</div>
											</div>

											<div class="form-group row" id="duedate">
												<label for="" class="col-md-4 col-form-label text-md-left text text-warning"><h3>{{ __('Submisssion Due Date') }}</h3></label>
											
												<div class="col-md-8">
													<input type="date" id="duedatetextbox" class="form-control" name="submisssionstatus_duedate">

													<p class="small"><i>For Incomplete or Resubmission Status only.</i></p>

												</div>
											</div>

										</div>


										<div class="h-divider"></div> <br>


										<!-- date completed -->
										<!-- <div class="form-group row" id="">
											<label for="date_completed" class="col-md-4 col-form-label text-md-left"><h6>{{ __('Requirements Date Completed:') }}</h6></label>

											<div class="col-md-8">
												<input id="date_completed" type="date" class="form-control{{ $errors->has('date_completed') ? ' is-invalid' : '' }}" name="date_completed" value="{{$recdetails->applicationlevel->date_completed}}">

												@if ($errors->has('date_completed'))
													<span class="invalid-feedback" role="alert"><strong>{{ $errors->first('date_completed') }}</strong></span>
												@endif
											</div>
										</div> -->


								  <!--accred div  -->

									<div id="accrediteddiv" style="display: none;">

										<h3 class="text text-warning"><center><i>Reminder: If you selected status of <b>"Accredited"</b>, please do not forget to <br> put the Date of Accreditation and the Date of Accreditation Expiry. Thank you!</i></center></h3>

										<!-- date of accreditation -->
										<div class="form-group row">
											<label for="date_accreditation" class="col-md-4 col-form-label text-md-left"><h6>{{ __('Date of Accreditation:') }}</h6></label>

											<div class="col-md-8">
												<input id="date_accreditation" type="date" class="form-control{{ $errors->has('date_accreditation') ? ' is-invalid' : '' }}" name="date_accreditation" value="{{$recdetails->applicationlevel->date_accreditation}}">

												@if ($errors->has('date_accreditation'))
												<span class="invalid-feedback" role="alert"><strong>{{ $errors->first('date_accreditation') }}</strong></span>
												@endif
											</div>
										</div>

										<!-- date accreditation expiry -->
										<div class="form-group row">
											<label for="date_accreditation_expiry" class="col-md-4 col-form-label text-md-left"><h6>{{ __('Date of Accreditation Expiry:') }}</h6></label>

											<div class="col-md-8">
												<input id="date_accreditation_expiry" type="date" class="form-control{{ $errors->has('date_accreditation_expiry') ? ' is-invalid' : '' }}" name="date_accreditation_expiry" value="{{$recdetails->applicationlevel->date_accreditation_expiry}}"> <small></small>

												@if ($errors->has('date_accreditation_expiry'))
												<span class="invalid-feedback" role="alert"><strong>{{ $errors->first('date_accreditation_expiry') }}</strong></span>
												@endif
											</div>
										</div>

										<!-- level of accreditation -->
										<div class="form-group row">
											<label for="selectlevelnew" class="col-md-4 col-form-label text-md-left"><h6>{{ __('Final Level of Accreditation:') }}</h6></label>

											<div class="col-md-8">
												<select id="selectlevelnew" type="text" class="form-control{{ $errors->has('selectlevelnew') ? ' is-invalid' : '' }}" name="selectlevelnew" value="{{ old('selectlevelnew') }}" placeholder="">

													<option value="{{$recdetails->applicationlevel->level_id}}">{{ $recdetails->applicationlevel->levels->level_name }}</option>

													@foreach($selectlevels as $id => $selectlevel)
													<option value="{{$id}}">{{ $selectlevel }}</option>
													@endforeach
												</select>
												@if ($errors->has('selectlevelnew'))
												<span class="invalid-feedback">
													<strong>{{ $errors->first('selectlevelnew') }}</strong>
												</span>
												@endif
											</div>
										</div>

										<!-- Accreditation Number -->
										<div class="form-group row">
											<label for="accreditation_no" class="col-md-4 col-form-label text-md-left"><h6>{{ __('Accreditation Number:') }}</h6></label>

											<div class="col-md-8">
												<input id="accreditation_no" type="text" class="form-control{{ $errors->has('accreditation_no') ? ' is-invalid' : '' }}" name="accreditation_no" value="{{$recdetails->applicationlevel->accreditation_number}}" readonly> <small><a href="" class="fa fa-pencil sm text-primary" data-toggle="modal" data-target=".bs-example-modal-sm-acc_no"> Update Accreditation number</a></small> 

												@if ($errors->has('accreditation_no'))
												<span class="invalid-feedback" role="alert"><strong>{{ $errors->first('accreditation_no') }}</strong></span>
												@endif
											</div>
										</div>

										<div class="form-group row">
											<label for="rec_accreditation" class="col-md-4 col-form-label text-md-left">{{ __('Accreditation:*') }}</label>
											<div class="col-md-8">

												<select name="rec_accreditation" id="accreditation" class="input-group{{ $errors->has('rec_accreditation') ? ' is-invalid' : '' }}" style="width: 100%; background-color: #00f;">

													<option value="{{ $recdetails->applicationlevel->rec_accreditation }}" selected>{{ $recdetails->applicationlevel->rec_accreditation }}</option>

													@foreach($selectaccreditation as $key => $selectaccreditations)
													<option value="{{$selectaccreditations}}">{{$selectaccreditations}}</option>
													@endforeach
												</select>

												@if ($errors->has('rec_accreditation'))
												<span class="invalid-feedback" role="alert">
													<strong>{{ $errors->first('rec_accreditation') }}</strong>
												</span>
												@endif
											</div><br>
										</div>

										<!-- allow upload if accredited na -->
										<div class="form-group row">
											<label for="allow_upload_compliance" class="col-md-4 col-form-label text-md-left"><span class="blink_me fa fa-warning"></span> <b>{{ __('Check the box if you want to allow REC to upload evidences/compliances while they are on Accredited or Ongoing Status:') }}</b></label>
											<div class="col-md-8">

												<div class="form-check">
													<input class="input-group{{ $errors->has('allow_upload_compliance') ? ' is-invalid' : '' }}" type="checkbox" value="1" name="allow_upload_compliance" id="flexCheckDefault" style="width: 4.5rem; height: 4.5rem;">
													

												</div>
											
												@if ($errors->has('allow_upload_compliance'))
												<span class="invalid-feedback" role="alert">
													<strong>{{ $errors->first('allow_upload_compliance') }}</strong>
												</span>
												@endif
											</div><br>
										</div>

										<!--Accreditors -->
										<div class="form-group row">
											<label for="send_accreditors" class="col-md-4 col-form-label text-md-left"><caption>{{ __('Assigned Accreditors/Coordinators') }}</caption></label>
											<div class="col-md-8">
												@foreach($get_sendaccreditors as $get_sendaccreditor)
												<span class="badge badge-secondary">{{$get_sendaccreditor->name}} | <small>

													@if($get_sendaccreditor->sendaccreditors_confirm_decline == null)
													<label class="badge badge-warning blink_me" title="Waiting to confirm the invitation">
														Waiting to confirm
													</label>
													@else
													<label class="badge badge-success blink_me" title="Confirmed">
														Confirmed
													</label>
													@endif

													<a href="" data-toggle="modal" data-target="#removeacc{{$get_sendaccreditor->sendacc_tbl_id}}" data-backdrop="static" class="btn btn-danger btn-sm fa fa-remove" title="Remove Accreditors/Coordinators"></a></small></span>
													@endforeach
												</div>
											</div>

										</div>


										<div class="form-group row" id="checkboxforongoingstatus" style="display: none;">
											<label for="allow_upload_compliance" class="col-md-4 col-form-label text-md-left"><span class="blink_me fa fa-warning"></span> <b>{{ __('Check the box if you want to allow REC to upload evidences/compliances while they are on Accredited or Ongoing Status:') }}</b></label>
											<div class="col-md-8">

												<div class="form-check">
													<input class="input-group{{ $errors->has('allow_upload_compliance') ? ' is-invalid' : '' }}" type="checkbox" value="1" name="allow_upload_compliance" id="flexCheckDefault" style="width: 4.5rem; height: 4.5rem;">
													

												</div>
											
												@if ($errors->has('allow_upload_compliance'))
												<span class="invalid-feedback" role="alert">
													<strong>{{ $errors->first('allow_upload_compliance') }}</strong>
												</span>
												@endif
											</div><br>
										</div>

										<!-- accred div -->

											@can('secretariat-access')
											<div class="form-group">
												<div class="form-group pull-right">

													<div class="btn-group center" role="group" aria-label="Button group with nested dropdown">
														<a href="" data-toggle="modal" data-target=".bs-example-modal-sm-update_rec_status1" class="btn btn-primary fa fa-pencil"> Save Changes</a>

														<!-- <a href="" class="btn btn-primary fa fa-send" data-toggle="modal" data-target="#send_accreditors" data-backdrop="static"> Send to Accreditors</a> -->

														<!-- <a href="#" class="btn btn-primary fa fa-money" title="Send OR to REC" data-backdrop="static" data-toggle="modal" data-target="#sendOR"> Send Order of Payment</a> -->

														<!-- <div class="btn-group" role="group">
															<button id="btnGroupDrop1" type="button" class="btn btn-primary fa fa-money dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																Order of Payment
															</button>
															<div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
																<a class="dropdown-item" href="#" a href="#" class="btn btn-primary" title="Send OR to REC" data-backdrop="static" data-toggle="modal" data-target="#sendOR"><span class="fa fa-paper-plane"></span> Send OR to REC</a>
																<a class="dropdown-item" href="#"title="Generate OR for REC, Cashier & Accounting" data-backdrop="static" data-toggle="modal" data-target="#generateOR"><span class="fa fa-print"></span> Generate OR</a>
																<a class="dropdown-item" href="#"title="Send OR with Receipt to REC" data-backdrop="static" data-toggle="modal" data-target="#sendorreceipt"><span class="fa fa-share-square-o "></span> Send OR with Receipt</a>
															</div>
														</div> -->

														<!-- @if($recdetails->applicationlevel->date_accreditation != null)
															<div class="btn-group" role="group">
																<a href="" data-toggle="modal" data-target=".bs-example-modal-sm-awardingletter" class="btn btn-primary fa fa-trophy" data-keyboard="false" data-backdrop="static"> Awarding</a>
															</div>
														@else
															<a href="" data-toggle="modal" data-target=".bs-example-modal-sm-awardingletter" class="btn btn-primary fa fa-trophy disabled"> Awarding</a>
														@endif -->

													</div>

												</div>
											</div>

											@endcan

											<!--   //save status/datecompleted modal --> 
											<div class="modal bs-example-modal-sm-update_rec_status1" tabindex="-1" role="dialog" aria-hidden="true">
												<div class="modal-dialog modal-sm">
													<div class="modal-content">
														<div class="modal-header badge badge-info">
															<h6>Status Confirmation Alert <span class="fa fa-exclamation"></span></h6> 
															<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
														</div>
														<div class="modal-body">Do you want to update status?</div>
														<div class="modal-footer">
															<button type="submit"class="btn btn-secondary btn-lg btn-block" title="Update REC Status">Yes</button>
														</div>
													</div>
												</div>
											</div> 

										</form>
									</div>
								</div>
							</div><br>
							@endcan
							@endif

						</div>

					</div>


					<div class="container-fluid">
						<span class="fa fa-sticky-note"></span> <label class="text text-dark"><b> {{$recdetails->rec_institution}} -{{$recdetails->rec_name}} ({{$recdetails->applicationlevel->levels->level_name}})</b></label>
						<div class="card">
							<ul class="nav nav-tabs" id="myTab" role="tablist">
								<li class="nav-item">
									<a class="nav-link active" id="summary-tab" data-toggle="tab" href="#summary" role="tab" aria-controls="summary" aria-selected="true">Summary</a>
								</li>

								<li class="nav-item">
									<a class="nav-link" id="consent-tab" data-toggle="tab" href="#consent" role="tab" aria-controls="summary" aria-selected="true">Informed Consent</a>
								</li>

								@if($recdetails->rec_existing_record == 1)
								<li class="nav-item">
									<a class="nav-link" id="others-tab" data-toggle="tab" href="#others" role="tab" aria-controls="others" aria-selected="true">Others</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="remarks-tab" data-toggle="tab" href="#remarks" role="tab" aria-controls="remarks" aria-selected="true">Notes</a>
								</li>

								@else
								@can('secretariat-access')
								<!-- <li class="nav-item">
									<a class="nav-link" id="acc_assess-tab" data-toggle="tab" href="#acc_assess" role="tab" aria-controls="acc_assess" aria-selected="false">Accreditor's Assessment</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="apce-tab" data-toggle="tab" href="#apce" role="tab" aria-controls="apce" aria-selected="false">Action Plan & Compliance Evidences</a>
								</li> -->
								@endcan
								@endif

							</ul>

							<div class="card-body">
								<div class="tab-content" id="myTabContent">
									<div class="tab-pane fade show active" id="summary" role="tabpanel" aria-labelledby="summary-tab">

										<table class="table table-hover table-border">
											<tbody>
												<tr class="text-sm">
													<td>Application Status:</td>
													<td><b>{{$recdetails->applicationlevel->recdetails_submission_status == 0 ? 'Not yet submitted' : $recdetails->applicationlevel->status->status_name}}</b></td>
												</tr>
												<tr class="text-sm">
													<td>Application Date Created:</td>
													<td><b>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $recdetails->applicationlevel->created_at)->format('F j, Y')}}</b></td>
												</tr>
												<tr class="text-sm">
													<td>Requirements Date Completed:</td>
													@if($recdetails->applicationlevel->date_completed === null)
													<td><label>---</label></td>
													@else
													<td><b>{{\Carbon\Carbon::createFromFormat('Y-m-d', $recdetails->applicationlevel->date_completed)->format('F j, Y')}}</b></td>
													@endif
												</tr>
												<tr class="text-sm">
													<td>Date Effectivity of Accreditation:</td>
													@if($recdetails->applicationlevel->date_accreditation === null)
														<td>---</td>
													@else
														<td><b>{{\Carbon\Carbon::createFromFormat('Y-m-d',$recdetails->applicationlevel->date_accreditation)->format('F j, Y')}}</b></td>
													@endif
												</tr>
												<tr class="text-sm">
													<td>Date Accreditation Expiry:</td>
													
													
														<td>
															<b>
																{{ $recdetails->applicationlevel->date_accreditation_expiry != null ? \Carbon\Carbon::createFromFormat('Y-m-d',$recdetails->applicationlevel->date_accreditation_expiry)->format('F j, Y') : '---'}} 

																	

																	@if($recdetails->applicationlevel->date_accreditation_expiry <= Carbon\Carbon::today() AND $recdetails->applicationlevel->statuses_id == 4 AND $recdetails->applicationlevel->date_accreditation_expiry != null)

																		<label class="blink_me text text-danger"> | 
																			<span class="fa fa-exclamation-circle"></span> Accreditation Expired</label>

																	@elseif($recdetails->applicationlevel->date_accreditation_expiry != null AND $recdetails->applicationlevel->statuses_id == 4)

																		<br>
																	
																		<label class="text-danger small"><i>{{\Carbon\Carbon::createFromFormat('Y-m-d',$recdetails->applicationlevel->date_accreditation_expiry)->diffInDays(\Carbon\Carbon::now())}} days before the expiry!</i></label>

																	@endif


																	@if($recdetails->applicationlevel->statuses_id == 4 AND $recdetails->applicationlevel->date_accreditation_expiry != null)

																		@can('can-see-pending-inc-complete')
																			<a href="#" class="pull-right small" data-toggle="modal" data-target="#sendexpirynotifs" title="Click here to send reminder notification"><span class="fa fa-send"></span></a>
																		@endcan 

																	@endif
																	
																
															</b> 
													</td>


												</tr>
												<tr class="text-sm">
													<td>Accreditation History:</td>
													<td>
														@foreach($recdetails->submissionsstatuses->whereIn('submissionstatuses_statuses_id', [4,25,28,29]) as $id => $accredHistory)

															<ul>
																<li>{{$accredHistory->status->status_name}}<br>
																	<small>

																		{{ $accredHistory->submissionstatuses_date_accreditation 
																			? \Carbon\Carbon::parse($accredHistory->submissionstatuses_date_accreditation)->format('F j, Y') 
																			: 'N/A' }} 

																			- 

																			{{ $accredHistory->submissionstatuses_date_expiry 
																				? \Carbon\Carbon::parse($accredHistory->submissionstatuses_date_expiry)->format('F j, Y') 
																				: 'N/A' }}

																		<br>

																		{{$accredHistory->history_levels->level_name ?? '---'}}

																	</small>
																</li>
															</ul>

														@endforeach
													</td>
												</tr>

												<tr class="text-sm">
													<td>Accreditation Number:</td>
														@if($recdetails->applicationlevel->accreditation_number == null)
															<td>---</td>
														@else
															<td><b>{{$recdetails->applicationlevel->accreditation_number}}</b></td>
														@endif
												</tr>
											</tbody>
										</table>								
									</div>


									<div class="tab-pane fade" id="consent" role="tabpanel" aria-labelledby="consent-tab">

										<table class="table table-hover table-border">
											<tbody>
												@foreach($recdetails->recdetails_statements as $rs)
												<ul><span class="fa fa-check"></span> {{$rs->statement->statement_description}}</ul>
												@endforeach
											</tbody>
										</table>								
									</div>

									@can('secretariat-access')

									@if($recdetails->rec_existing_record == 1)
									<div class="tab-pane fade" id="others" role="tabpanel" aria-labelledby="others-tab">
										@include('recdetails.view-recdetails-accreditedview')
									</div>

									<div class="tab-pane fade" id="remarks" role="tabpanel" aria-labelledby="remarks-tab">
										<div class="container-fluid">
											<div class="form-group row">
												<label class="small">Documents still in need of compliance based on accreditation letter</label>
												<textarea class="form-control{{ $errors->has('file') ? ' is-invalid' : '' }}" readonly>{{$recdetails->existing_recs->existingrec_remarks}}</textarea>
											</div>
										</div>
									</div>
									@endif

									<div class="tab-pane fade" id="acc_assess" role="tabpanel" aria-labelledby="acc_assess-tab">
										@include('recdetails.view-recdetails-part2')
									</div>
									<div class="tab-pane fade" id="apce" role="tabpanel" aria-labelledby="apce-tab">
										@include('recdetails.view-recdetails-part3')
									</div>
									@endcan

								</div>
							</div>

						</div>
					</div>

					<br>

					<div class="container-fluid">
						
						<div class="card">
							<div class="card-header bg-orange">
								<strong>Submission Logs</strong>
							</div>
							<div class="card-body">







								<div class="container">
									<div class="row">
										<div class="col-md-12">
											<div class="main-timeline">

												@foreach($recdetails->submissionsstatuses as $ss)

												<div class="timeline">

													
													
													<div class="timeline-content">
														<div class="timeline-icon">
															<i class="fa fa-globe"></i>
														</div>

														<h6>  By <b>{{ $ss->users->name }}</b>	

															<br>

															{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $ss->submissionstatuses_datecreated)->format('F j, Y') }} 

															- <label class="text text-success"> {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $ss->submissionstatuses_datecreated)->diffForHumans() }} 


															</h6>

															<h3 class="title">{{ $ss->status->status_name }}</h3>

															<br> 
														</label>

														<p class="description">
															{!! $ss->submissionstatuses_emailmessage !!}
														</p>

														@foreach($ss->emailattachments as $ea)
															<p>
																<i>
																	<a href="{{ route('emailattachmment', ['id' => encrypt($ea->id)]) }}" target="_blank"><span class="fa fa-file"> </span> {{$ea->re_file_name}}</a>

																</i>
															</p>
														@endforeach

														@if($ss->submissionstatuses_doctypes_id != null)
															<br>
															<div class="">
																<ul>
																	@foreach($ss->submissionstatuses_doctypes_id as $doctypes)
																		<li>{{$doctypes}}</li>
																	@endforeach
																</ul>
															</div>
														@endif

													</div>
												</div>

												@endforeach
												
											</div>
										</div>
									</div>
								</div>






							<!-- 	<table class="table table-bordered table-hover table-responsive">
									<thead>
										<tr>
											<th>Submitted by</th>
											<th>Status</th>
											<th>Email Message</th>
											<th>Date Submitted</th>
										</tr>
									</thead>

									<tbody>
										
										@foreach($recdetails->submissionsstatuses as $ss)
											<tr>
												<td>{{ $ss->users->name }}</td>
												<td>{{ $ss->status->status_name }}</td>
												<td>{!! $ss->submissionstatuses_emailmessage !!}</td>
												<td>
													{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $ss->submissionstatuses_datecreated)->format('F j, Y') }} 

													- <label class="text text-success"> {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $ss->submissionstatuses_datecreated)->diffForHumans() }} </label>
												</td>
											</tr>
										@endforeach

									</tbody>
								</table> -->
								
							</div>
						</div>


					</div>
				</div>
			</div>
		</div>
		@include('recdetails.view-recdetails-modals')
	</div>
</div>
</div><br>

<!-- END OF MAIN BODY FOREACH  -->
@endsection