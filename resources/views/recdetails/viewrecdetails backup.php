@extends('layouts.myApp')
@section('content')

<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-10">

			<div class="card-header col-md-12">
				<label class="text-muted">EDIT RECORDS</label>
				<div class="float-right">

					@foreach($recdetails as $recdetails_sub_status)
					<a href="{{url('show-recdetails')}}" class="btn btn-default" title="Back"><span class="fa fa-arrow-circle-o-left"></span> Go Back</a>

					@can('rec-access')
					<!-- for rec users -->
					@if($recdetails_sub_status->recdetails_lock_fields == 0 or $recdetails_sub_status->recdetails_submission_status == 0)
					<a href="{{ route('documents', ['id' => encrypt($recdetail->id)]) }}" class="btn btn-warning btn-sm" title="View Documents"><span class="fa fa-folder"></span> {{$recdetail->rec_name}} Requirements</a>

					@if($recdetails_sub_status->recdetails_submission_status != 0)
					<a href="" class="btn btn-primary fa fa-send" data-toggle="modal" data-target="#resubmit-req"> Re-submit</a>
					
					<!-- Modal for resubmit req -->
					<div class="modal fade" id="resubmit-req">
						<div class="modal-dialog modal-sm">
							<div class="modal-content">
								<!-- Modal Header -->
								<div class="modal-header">
									<h6 class="modal-title">Confirmation Alert</h6>
									<button type="button" class="close" data-dismiss="modal">&times;</button>
								</div>
								<!-- Modal body -->
								<div class="modal-body">
									<div class="container-fluid">
										<form action="" method="">
											<div class="form-group col-md-12">
												Do you want to resubmit requirements?
											</div>
										</form>
									</div>
								</div>
								<!-- Modal footer -->
								<div class="modal-footer">
									<a href="{{route('resubmit-req', $recdetail->id)}}" class="btn btn-primary">Submit</a>
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
								</div>
							</div>
						</div>
					</div>
					@endif
					@endif
					@endcan

					@can('accreditors-access')
					@foreach($get_sendaccreditors as $get_sendaccreditor)
					@if($get_sendaccreditor->a_accreditors_user_id == auth::user()->id)
					<a href="{{ route('documents', ['id' => encrypt($recdetail->id)]) }}" class="btn btn-warning btn-sm" title="View Documents"><span class="fa fa-folder"></span> {{$recdetail->rec_name}} Requirements</a>
					@endif
					@endforeach
					@endcan

					@can('secretariat-access')
					<a href="{{ route('documents', [ 'id' => encrypt($recdetail->id) ]) }}" class="btn btn-warning btn-sm" title="View Documents"><span class="fa fa-folder"></span> {{$recdetail->rec_name}} Requirements</a>
					@endcan

				</div>
			</div><br>

			<div class="col-md-12" id="savedclass">
				@if(session('success'))
				<div class="alert alert-success ">
					<i class="fa fa-exclamation"></i>{{session('success')}}
					<button class="close" onclick="document.getElementById('savedclass').style.display='none'">x</button>
				</div>
				@endif
			</div>

			@foreach($recdetails as $recdetails_sub_status)	
			@if($recdetails_sub_status->recdetails_submission_status == 0)<fieldset>@else<fieldset disabled>@endif 
				@endforeach
				
				<div class="container-fluid">
					@unless (Auth::check()) You are not signed in. @endunless
					<form method="POST" action="{{ route('update-recdetails', ['recdetail' => $recdetail->id]) }}" aria-label="">
						{{csrf_field()}}
						@endforeach
						<div class="form-group col-md-12">

							<strong><h4> Research Ethics Committee Details</h4></strong>

							<!-- custom select box lvl of accreditation -->
							<div class="form-group row">
								<label for="selectlevel" class="col-md-4 col-form-label text-md-left"><b>✓ </b>{{ __('Level of Accreditation*') }}</label>

								<div class="col-md-8">

									<select id="selectlevel" type="text" class="form-control{{ $errors->has('selectlevel') ? ' is-invalid' : '' }}" name="selectlevel" value="{{ old('selectlevel') }}" placeholder="">

										@foreach($recdetails as $getlevel)
										<option value="{{$getlevel->level_id}}">{{ $getlevel->level_name }}</option>
										@endforeach

										@foreach($selectlevels as $id => $selectlevel)
										<option value="{{$id}}">{{ $selectlevel }}</option>
										@endforeach

									</select>
									@if ($errors->has('selectlevel'))
									<span class="invalid-feedback">
										<strong>{{ $errors->first('selectlevel') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<!-- rec_region -->
							<div class="form-group row">
								<label for="rec_region" class="col-md-4 col-form-label text-md-left"><b></b>{{ __('Region*') }}</label>

								<div class="col-md-8">
									<select id="rec_region" type="text" class="form-control{{ $errors->has('rec_region') ? ' is-invalid' : '' }}" name="rec_region" value="{{ old('rec_region') }}" placeholder="">

										@foreach($recdetails as $getregion)
										<option value="{{$getregion->id}}">{{ $getregion->region_name }}</option>
										@endforeach

										@foreach($selectregions as $id => $selectregion)
										<option value="{{$id}}">{{$selectregion}}</option>
										@endforeach

									</select>
									@if ($errors->has('rec_region'))
									<span class="invalid-feedback">
										<strong>{{ $errors->first('rec_region') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<!-- name of REC -->
							<div class="form-group row">
								<label for="rec_name" class="col-md-4 col-form-label text-md-left">{{ __('Name of REC*:') }}</label>

								<div class="col-md-8">
									<input id="rec_name" type="text" class="form-control{{ $errors->has('rec_name') ? ' is-invalid' : '' }}" name="rec_name" value="{{$recdetail->rec_name}}" required="">

									@if ($errors->has('rec_name'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('rec_name') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<!-- name of Institution -->
							<div class="form-group row">
								<label for="rec_institution" class="col-md-4 col-form-label text-md-left">{{ __('Name of Institution*:') }}</label>

								<div class="col-md-8">
									<input id="rec_institution" type="text" class="form-control{{ $errors->has('rec_institution') ? ' is-invalid' : '' }}" name="rec_institution" required="" value="{{$recdetail->rec_institution}}">

									@if ($errors->has('rec_institution'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('rec_institution') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<!-- REC Address -->
							<div class="form-group row">
								<label for="rec_address" class="col-md-4 col-form-label text-md-left">{{ __('REC Address*:') }}</label>

								<div class="col-md-8">
									<input id="rec_address" type="text" class="form-control{{ $errors->has('rec_address') ? ' is-invalid' : '' }}" name="rec_address" required="" value="{{$recdetail->rec_address}}">

									@if ($errors->has('rec_address'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('rec_address') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<!-- REC Email Address -->
							<div class="form-group row">
								<label for="rec_email" class="col-md-4 col-form-label text-md-left">{{ __('REC Email*:') }}</label>

								<div class="col-md-8">
									<input id="rec_email" type="email" class="form-control{{ $errors->has('rec_email') ? ' is-invalid' : '' }}" name="rec_email" required="" value="{{$recdetail->rec_email}}">

									@if ($errors->has('rec_email'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('rec_email') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<!-- REC Tel No -->
							<div class="form-group row">
								<label for="rec_telno" class="col-md-4 col-form-label text-md-left">{{ __('REC Tel No:') }}</label>

								<div class="col-md-8">
									<input id="rec_telno" type="number" class="form-control{{ $errors->has('rec_telno') ? ' is-invalid' : '' }}" name="rec_telno" value="{{$recdetail->rec_telno}}">

									@if ($errors->has('rec_telno'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('rec_telno') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<!-- REC Fax No -->
							<div class="form-group row">
								<label for="rec_faxno" class="col-md-4 col-form-label text-md-left">{{ __('REC Fax No:') }}</label>

								<div class="col-md-8">
									<input id="rec_faxno" type="number" class="form-control{{ $errors->has('rec_faxno') ? ' is-invalid' : '' }}" name="rec_faxno" value="{{$recdetail->rec_faxno}}">

									@if ($errors->has('rec_faxno'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('rec_faxno') }}</strong>
									</span>
									@endif
								</div>
							</div>
						</div><br>

						<div class="form-group col-md-12">

							<strong><h4> Reasearch Ethics Committee Chair Details</h4></strong>

							<!-- name of REC chair -->
							<div class="form-group row">
								<label for="rec_chairname" class="col-md-4 col-form-label text-md-left">{{ __('Name of REC Chair*:') }}</label>

								<!-- for recchairs inner join -->
								@foreach($recdetails as $recchair)				

								<div class="col-md-8">
									<input id="rec_chairname" type="text" class="form-control{{ $errors->has('rec_chairname') ? ' is-invalid' : '' }}" name="rec_chairname" value="{{$recchair->rec_chairname}}" required="">

									@if ($errors->has('rec_chairname'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('rec_chairname') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<!-- email of REC chair -->
							<div class="form-group row">
								<label for="rec_chairemail" class="col-md-4 col-form-label text-md-left">{{ __('Email of REC Chair*:') }}</label>

								<div class="col-md-8">
									<input id="rec_chairemail" type="text" class="form-control{{ $errors->has('rec_chairemail') ? ' is-invalid' : '' }}" name="rec_chairemail" value="{{$recchair->rec_chairemail}}" required="">

									@if ($errors->has('rec_chairemail'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('rec_chairemail') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<!-- mobile of REC chair -->
							<div class="form-group row">
								<label for="rec_chairmobile" class="col-md-4 col-form-label text-md-left">{{ __('Mobile No. of REC Chair*:') }}</label>

								<div class="col-md-8">
									<input id="rec_chairmobile" type="number" class="form-control{{ $errors->has('rec_chairmobile') ? ' is-invalid' : '' }}" name="rec_chairmobile" value="{{$recchair->rec_chairmobile}}" required="">

									@if ($errors->has('rec_chairmobile'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('rec_chairmobile') }}</strong>
									</span>
									@endif
								</div>
							</div>
						</div><br>

						@endforeach

						<div class="form-group col-md-12">
							<strong><h4> Contact Details</h4></strong>

							<!-- REC contact person -->
							<div class="form-group row">
								<label for="rec_contactperson" class="col-md-4 col-form-label text-md-left">{{ __('Contact Person*:') }}</label>

								<div class="col-md-8">
									<input id="rec_contactperson" type="text" class="form-control{{ $errors->has('rec_contactperson') ? ' is-invalid' : '' }}" name="rec_contactperson" required="" value="{{$recdetail->rec_contactperson}}">

									@if ($errors->has('rec_contactperson'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('rec_contactperson') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<!-- contactperson position -->
							<div class="form-group row">
								<label for="rec_contactposition" class="col-md-4 col-form-label text-md-left">{{ __('Contact Person Position:') }}</label>

								<div class="col-md-8">
									<input id="rec_contactposition" type="text" class="form-control{{ $errors->has('rec_contactposition') ? ' is-invalid' : '' }}" name="rec_contactposition" value="{{$recdetail->rec_contactposition}}">

									@if ($errors->has('rec_contactposition'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('rec_contactposition') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<!-- contactperson mobile no -->
							<div class="form-group row">
								<label for="rec_contactmobileno" class="col-md-4 col-form-label text-md-left">{{ __('Contact Person Mobile No:') }}</label>

								<div class="col-md-8">
									<input id="rec_contactmobileno" type="number" class="form-control{{ $errors->has('rec_contactmobileno') ? ' is-invalid' : '' }}" name="rec_contactmobileno" value="{{$recdetail->rec_contactmobileno}}">

									@if ($errors->has('rec_contactmobileno'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('rec_contactmobileno') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<!-- contact person email -->
							<div class="form-group row">
								<label for="rec_contactemail" class="col-md-4 col-form-label text-md-left">{{ __('Contact Person Email:*') }}</label>

								<div class="col-md-8">
									<input id="rec_contactemail" type="email" class="form-control{{ $errors->has('rec_contactemail') ? ' is-invalid' : '' }}" name="rec_contactemail" required="" value="{{$recdetail->rec_contactemail}}">

									@if ($errors->has('rec_contactemail'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('rec_contactemail') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<!-- Date of REC Established -->
							<div class="form-group row">
								<label for="rec_dateestablished" class="col-md-4 col-form-label text-md-left">{{ __('Date REC Established:*') }}</label>

								<div class="col-md-8">
									<input id="rec_dateestablished" type="date" class="form-control{{ $errors->has('rec_dateestablished') ? ' is-invalid' : '' }}" name="rec_dateestablished" required="" value="{{$recdetail->rec_dateestablished}}">

									@if ($errors->has('rec_dateestablished'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('rec_dateestablished') }}</strong>
									</span>
									@endif
								</div>
							</div><br>

							<!-- footer -->
							<div class="modal-footer">
								<div class="form-group">
									@can('rec-access')
									@foreach($recdetails as $recdetails_sub_status)
									<a href="" class="btn btn-success {{$recdetails_sub_status->recdetails_submission_status === 0 ? '' : 'hide'}} fa fa-pencil" data-toggle="modal" data-target="#savechanges" > Save Changes</a>

									<a href="" class="btn btn-primary fa fa-send {{$recdetails_sub_status->recdetails_submission_status === 0 ? '' : 'hide'}}" data-toggle="modal" data-target="#submit_to_phreb"> Submit to PHREB</a>

									@endforeach
									@endcan
								</div>	
							</div>				

							<!-- Modal for save changes -->
							<div class="modal fade" id="savechanges" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header badge badge-success">
											<h5 class="modal-title" id="exampleModalLabel">Save Confirmation</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											Do you want to update this records?
										</div>
										<div class="modal-footer">
											<button type="submit" class="btn btn-success">Ok</button>
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
										</div>
									</div>
								</div>
							</div>

							<!-- Modal for Submit to PHREB -->
							<div class="modal fade" id="submit_to_phreb" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header badge badge-primary">
											<h5 class="modal-title" id="exampleModalLabel">Submit Confirmation</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											Do you want to submit your Application to PHREB?
										</div>
										<div class="modal-footer">
											<a href="{{ route('submit_recdetails', ['id' => $recdetail->id]) }}" class="btn btn-primary">Submit</a>
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
										</div>
									</div>
								</div>
							</div>
						</form>
					</fieldset>
					@can('can-see-pending-inc-complete')
					<div class="container-fluid">
						<div class="card">
							<div class="card-header"><strong style="color: blue;">ACTIONS IN REASEARCH ETHICS COMMITTEE APPLICATION</strong></div>
							<div class="card-body">
								<form action="{{route('update_rec_status', ['id' => $recdetail->id])}}" method="Post">
									{{csrf_field()}}
									<div class="form-group row">
										<label for="" class="col-md-4 col-form-label text-md-left"><h6>{{ __('Application Status*') }}</h6></label>

										<div class="col-md-8">
											<select id="status" type="text" class="form-control{{ $errors->has('status') ? ' is-invalid' : '' }}" name="status" value="{{ old('status') }}" placeholder="">
												@foreach($recdetails as $recdetail)
												<option value="{{$recdetail->statuses_id}}">{{$recdetail->status_name}}</option>
												@endforeach

												@foreach($selectstatuses as $id => $status)
												<option value="{{$id}}">{{$status}}</option>
												@endforeach
											</select>
										</div>
									</div>


									<!-- date completed -->
									<div class="form-group row">
										<label for="date_completed" class="col-md-4 col-form-label text-md-left"><h6>{{ __('Requirements Date Completed:') }}</h6></label>

										<div class="col-md-8">
											<input id="date_completed" type="date" class="form-control{{ $errors->has('date_completed') ? ' is-invalid' : '' }}" name="date_completed" value="{{$recdetail->date_completed}}">

											@if ($errors->has('date_completed'))
											<span class="invalid-feedback" role="alert"><strong>{{ $errors->first('date_completed') }}</strong></span>
											@endif
										</div>
									</div>

									<!-- date of accreditation -->
									<div class="form-group row">
										<label for="date_accreditation" class="col-md-4 col-form-label text-md-left"><h6>{{ __('Date of Accreditation:') }}</h6></label>

										<div class="col-md-8">
											<input id="date_accreditation" type="date" class="form-control{{ $errors->has('date_accreditation') ? ' is-invalid' : '' }}" name="date_accreditation" value="{{$recdetail->date_accreditation}}">

											@if ($errors->has('date_accreditation'))
											<span class="invalid-feedback" role="alert"><strong>{{ $errors->first('date_accreditation') }}</strong></span>
											@endif
										</div>
									</div>

									<!-- date accreditation expiry -->
									<div class="form-group row">
										<label for="date_accreditation_expiry" class="col-md-4 col-form-label text-md-left"><h6>{{ __('Date of Accreditation Expiry') }}</h6></label>

										<div class="col-md-8">
											<input id="date_accreditation_expiry" type="date" class="form-control{{ $errors->has('date_accreditation_expiry') ? ' is-invalid' : '' }}" name="date_accreditation_expiry" value="{{$recdetail->date_accreditation_expiry}}">

											@if ($errors->has('date_accreditation_expiry'))
											<span class="invalid-feedback" role="alert"><strong>{{ $errors->first('date_accreditation_expiry') }}</strong></span>
											@endif
										</div>
									</div>


									<!--Accreditors -->
									<div class="form-group row">
										<label for="send_accreditors" class="col-md-4 col-form-label text-md-left"><caption>{{ __('Assigned Accreditors/Coordinators') }}</caption></label>
										<div class="col-md-8">
											@foreach($recdetails_user_ass as $recdetails_user_ass1)
											<span class="badge badge-secondary">{{$recdetails_user_ass1->name}}</span>
											@endforeach
										</div>
									</div>


									@can('secretariat-access')
									<div class="form-group">
										<div class="form-group pull-right">
											<a href="" data-toggle="modal" data-target=".bs-example-modal-sm-update_rec_status" class="btn btn-primary fa fa-pencil"> Save Changes</a>
											<a href="" class="btn btn-primary fa fa-send" data-toggle="modal" data-target="#send_accreditors"> Send to Accreditors</a>
											<a href="{{ url('/dashboard') }}" class="btn btn-secondary fa fa-arrow-left"> Back</a>
										</div>
									</div>
									@endcan

									<!--   //save status/datecompleted modal --> 
									<div class="modal bs-example-modal-sm-update_rec_status" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog modal-sm">
											<div class="modal-content">
												<div class="modal-header badge badge-info">
													<h6>Status Confirmation Alert</h6> <span class="fa fa-exclamation"></span>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												</div>
												<div class="modal-body">Do you want to update status?</div>
												<div class="modal-footer">
													<button type="submit"class="btn btn-secondary btn-lg btn-block" title="Update REC Status">Yes</button>
												</div>
											</div>
										</div>
									</div> 

								</form>
							</div>
						</div>
					</div><br>
					@endcan

					<div class="container-fluid">
						<div class="card">
							<div class="card-header">
								Summary Report of <strong>{{$recdetail->rec_name}}</strong>
							</div>
							<div class="card-body">
								@foreach($recdetails as $recdetail)
								<!-- REC App Status -->
								<div class="form-group row">
									<label for="rec_dateestablished" class="col-md-4 col-form-label text-md-left"><caption>{{ __('• Application Status:') }}</caption></label>

									<div class="col-md-8">
										<em><label>{{$recdetail->status_name}}</label></em>		
									</div>
								</div>

								<!-- Date of Submission -->
								<div class="form-group row">
									<label for="rec_dateestablished" class="col-md-4 col-form-label text-md-left"><caption>{{ __('• Application Date Submitted:') }}</caption></label>
									<div class="col-md-8">
										<em><label>{{$recdetail->created_at}}</label></em>							
									</div>
								</div>

								<!-- Date Completed -->
								<div class="form-group row">
									<label for="rec_dateestablished" class="col-md-4 col-form-label text-md-left"><caption>{{ __('• Application Date Completed:') }}</caption></label>
									<div class="col-md-8">
										@if($recdetail->date_completed === null)
										<em><label>TBA</label></em>
										@else
										<em><label>{{$recdetail->date_completed}}</label></em>
										@endif
									</div>
								</div>

								<!-- Date Effectivity of Accreditation -->
								<div class="form-group row">
									<label for="rec_dateestablished" class="col-md-4 col-form-label text-md-left"><caption>{{ __('• Date Effectivity of Accreditation:') }}</caption></label>
									<div class="col-md-8">
										@if($recdetail->date_completed === null)
										<em><label>TBA</label></em>
										@else
										<em><label>{{$recdetail->date_completed}}</label></em>
										@endif
									</div>
								</div>

								<!-- Date of Accreditation Expiry -->
								<div class="form-group row">
									<label for="rec_dateestablished" class="col-md-4 col-form-label text-md-left"><caption>{{ __('• Date Effectivity of Accreditation:') }}</caption></label>
									<div class="col-md-8">
										@if($recdetail->date_completed === null)
										<em><label>TBA</label></em>
										@else
										<em><label>{{$recdetail->date_completed}}</label></em>
										@endif
									</div>
								</div>

								<!--Accreditation Number -->
								<div class="form-group row">
									<label for="rec_dateestablished" class="col-md-4 col-form-label text-md-left"><caption>{{ __('• Accreditation Number:') }}</caption></label>
									<div class="col-md-8">
										@if($recdetail->date_completed === null)
										<em><label>TBA</label></em>
										@else
										<em><label>{{$recdetail->date_completed}}</label></em>
										@endif
									</div>
								</div>
								@endforeach
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>

		<!-- Modal for submit to accreditors -->
		<div class="modal fade" id="send_accreditors">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<!-- Modal Header -->
					<div class="modal-header badge badge-primary">
						<h4 class="modal-title">Send to Accreditors</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<!-- Modal body -->
					<div class="modal-body">
						<div class="container-fluid">
							<form action="{{ route('sendtoaccreditor') }}" method="POST">
								{{ csrf_field() }}
								<div class="form-group col-md-12">
									<!-- input rec email address -->
									<input type="hidden" name="a_recdetails_id" class="form-control{{ $errors->has('a_recdetails_id') ? ' is-invalid' : '' }}" value="{{$recdetail->recdetails_id}}">

									<div class="form-group row">
										<label for="a_accreditors_user_id" class="col-md-4 col-form-label text-md-left"><h6>{{ __('Name of Registered Accreditors') }}</h6></label>
										<div class="col-md-8">
											<select class="form-control{{ $errors->has('a_accreditors_user_id') ? ' is-invalid' : '' }}" name="a_accreditors_user_id"><option value="" disabled selected>---Choose Accreditors---</option>
												@foreach($selectaccreditors as $id => $selectaccreditor)
												<option value="{{$id}}">{{$selectaccreditor}}</option>
												@endforeach
											</select>
											@if ($errors->has('a_recdetails_id'))
											<span class="invalid-feedback" role="alert">
												<strong>{{ $errors->first('a_recdetails_id') }}</strong>
											</span>
											@endif
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="submit" class="btn btn-primary">Send</button>
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		@endsection


















			// public function documents(Recdetails $recdetail){
 //    // dd($recdetail->id);
 //    $roles=Role::where('role_users.id', '=', Auth::user()->id)
 //    ->join('role_users', 'role_users.role_id', '=', 'roles.id')
 //    ->first();

 //    $select_doc_type = DocumentTypes::orderBy('document_types.id', 'asc')->pluck('document_types.document_types_name','document_types.id');

 //    $document_check = Recdocuments::select('recdocuments_document_types_id')->where('recdocuments.recdocuments_recdetails_id', $recdetail->id)->where('recdocuments_document_types_id_check', true)->where('recdocuments.recdocuments_document_types_id',1)->distinct()->get();
    
 //    $recdetails = Recdetails::join('applicationlevel', 'applicationlevel.recdetails_id', '=', 'recdetails.id')
 //    ->join('levels', 'applicationlevel.level_id', '=', 'levels.id')
 //    ->join('regions', 'applicationlevel.region_id', '=', 'regions.id')
 //    ->join('recchairs', 'applicationlevel.recdetails_id', '=', 'recchairs.recdetails_id')
 //    ->join('recdocuments', 'recdocuments.recdocuments_recdetails_id', '=', 'recdetails.id')
 //    ->join('document_types', 'document_types.id', '=', 'recdocuments.recdocuments_document_types_id')

 //    ->leftJoin('recdocumentsremarks', function($leftjoin) use ($recdetail){
 //        $leftjoin->on('recdocumentsremarks.remarks_recdocuments_types_id', 'recdocuments.recdocuments_document_types_id')->where('recdocumentsremarks.remarks_recdetails_id', '=', $recdetail->id);
 //    })

 //    ->select('recdetails.id', 'document_types.document_types_name', 'rec_dateuploaded', 'recdocuments.recdocuments_document_types_id', 'recdocumentsremarks.remarks_recdetails_id',
 //      DB::raw(" GROUP_CONCAT(DISTINCT recdocuments.recdocuments_file ORDER BY recdocuments.id desc SEPARATOR ', ') as 'files', GROUP_CONCAT(DISTINCT recdocumentsremarks.remarks_message_recdocuments ORDER BY recdocumentsremarks.remarks_message_recdocuments DESC SEPARATOR '| ') as 'message', GROUP_CONCAT(DISTINCT recdocuments.recdocuments_document_types_id_check) as 'check' "))
 //    ->groupBy('recdetails.id', 'document_types.document_types_name', 'rec_dateuploaded', 'recdocuments.recdocuments_document_types_id', 'recdocumentsremarks.remarks_recdetails_id')
 //    ->orderBy('recdocuments.rec_dateuploaded', 'asc')->where('recdetails.id', $recdetail->id)
 //    ->findOrFail($recdetail);

 //      if($roles->role_id == 2 or $roles->role_id == 1){
 //        return view('recdocuments.documents', compact('recdetail', 'recdetails', 'recdocuments', 'select_doc_type', 'document_check'));
 //      }if(Auth::user()->id == $recdetail->user_id){
 //        return view('recdocuments.documents', compact('recdetail', 'recdetails', 'recdocuments', 'select_doc_type', 'document_check'));
 //      }else{
 //        return redirect()->back()->withSuccess('Access Restriction!');
 //      }
 //  }
