<div class="form-group row">
    <label for="created_at" class="col-md-4 col-form-label text-md-left">{{ __('Applicaton Date Submitted:') }}</label>

    <div class="col-md-8">
        <input id="created_at" type="date" class="form-control{{ $errors->has('created_at') ? ' is-invalid' : '' }}" name="created_at" value="{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $recdetails->applicationlevel->created_at)->format('d/m/Y')}}">

        @if ($errors->has('created_at'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('created_at') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label for="date_completed" class="col-md-4 col-form-label text-md-left">{{ __('Requirements Date Completed:') }}</label>

    <div class="col-md-8">
        <input id="date_completed" type="date" class="form-control{{ $errors->has('created_at') ? ' is-invalid' : '' }}" name="date_completed" value="{{$recdetails->applicationlevel->date_completed}}">

        @if ($errors->has('date_completed'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('date_completed') }}</strong>
        </span>
        @endif
    </div>
</div>


<!-- --------------------------------------------- -->
<div class="form-group row">
    <label for="name_of_accreditors" class="col-md-4 col-form-label text-md-left">{{ __(' Name of Accreditors:') }}</label>

    <div class="col-md-8">
        <select name="name_of_accreditors[]" id="select_acc" class="input-group{{ $errors->has('created_at') ? ' is-invalid' : '' }}" multiple="multiple" style="width: 100%;">
            <option value="{{ $recdetails->existing_recs->name_of_accreditors }}" selected>{{ $recdetails->existing_recs->name_of_accreditors }}</option>

            @foreach($selectacc as $key => $selectaccs)
            <option value="{{ $selectaccs }}">{{ $selectaccs }}</option>
            @endforeach
        </select>


        @if ($errors->has('name_of_accreditors'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('name_of_accreditors') }}</strong>
        </span>
        @endif
    </div><br><br>
</div>


<div class="form-group row">
    <label for="date_req_deck_accreditors" class="col-md-4 col-form-label text-md-left">{{ __(' Date Requirements Decked to Accreditors:') }}</label>

    <div class="col-md-8">
        <input id="date_req_deck_accreditors" type="text" class="date_req_deck_accreditors form-control{{ $errors->has('created_at') ? ' is-invalid' : '' }}" name="date_req_deck_accreditors" value="{{$recdetails->existing_recs->date_req_deck_accreditors}}">

        @if ($errors->has('date_req_deck_accreditors'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('date_req_deck_accreditors') }}</strong>
        </span>
        @endif
    </div><br><br>
</div>

<div class="form-group row">
    <label for="date_rec_received_awarding_letter" class="col-md-4 col-form-label text-md-left">{{ __(' Date REC received awarding letter of acreditation
    :') }}</label>

    <div class="col-md-8">
        <input id="date_rec_received_awarding_letter" type="text" class="date_rec_received_awarding_letter form-control{{ $errors->has('created_at') ? ' is-invalid' : '' }}" name="date_rec_received_awarding_letter" value="{{$recdetails->existing_recs->date_rec_received_awarding_letter}}">

        @if ($errors->has('date_rec_received_awarding_letter'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('date_rec_received_awarding_letter') }}</strong>
        </span>
        @endif
    </div><br><br>
</div>