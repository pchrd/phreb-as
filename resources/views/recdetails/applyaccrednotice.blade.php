@extends('layouts.myApp')
@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-10">
			<div class="card">
				<div class="card-header bg-yellow">
					<h1><center><span class="fa fa-exclamation-circle"></span> Application Notice</center></h1>
				</div>
				<div class="card-body">

					<h5 class="card-title">Hello Mr/Mrs {{Auth::user()->name}}, </h5>
					
					<p class="card-text">To avoid duplication of application entry, you can use or check your previous created application below. </p>
					

					<table class="table table-light table-hover">
						<thead>
							<tr>
								<th>#</th>
								<th scope="col">Information</th>
								
								<th>Status</th>

								<th>No. of Files Uploaded</th>

								<th scope="col">Date Created</th>
								
							</tr>
						</thead>
						<tbody>

							@foreach($recdetail as $id => $recdetails)

								<tr>
									<td>{{ $id + 1 }}</td>

									<td>
										
										<a href="{{ route('view-recdetails', ['id' => encrypt($recdetails->id)]) }}" title="View REC Application">
										
											{{$recdetails->rec_institution}} -
											{{$recdetails->rec_name}} <br>

											<p>{{$recdetails->applicationlevel->levels->level_name }}</p>
										</a>

									</td>

									<td>
										{{ $recdetails->applicationlevel->recdetails_submission_status == 0 ? 'Not yet submitted' : $recdetails->applicationlevel->status->status_name }}
									</td>

									<td>
										
										{{$recdetails->recdocuments->count()}} - files

									</td>

									<td>
										{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $recdetails->applicationlevel->created_at)->format('F j, Y | G:i:A') }} <br>

										<label class="text text-primary">{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $recdetails->applicationlevel->created_at)->DiffForHumans() }}</label>

									</td>
									
								</tr>
							
							@endforeach

						</tbody>
					</table>

					<a href="{{ url('/applyaccred') }}" class="btn btn-outline-success btn-block">
						<span class="fa fa-file"></span> Yes, I want to proceed and create New Application Entry 
					</a>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection