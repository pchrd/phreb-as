@extends('layouts.myApp')
@section('content')
@include('recdetails.filtersearch-modal')
<div class="container">

	<div class="col-md-12" id="verifyuser">
		@if(session('success'))
		<div class="alert alert-success">
			{{session('success')}}
			<button class="close" onclick="document.getElementById('verifyuser').style.display='none'">x</button>
		</div>
		@endif
	</div> 

	<div class="row">
		<div class="col-md-12 col-md-offset-1">
			<div class="panel panel-info">

				<div class="panel-heading"><h5><a href="{{url('show-recdetails')}}" class="text text-info"><span class="fa fa-arrow-left"></span></a><strong> SEARCHED REC RECORDS <small><a href="#" data-toggle="modal" data-target="#searchfilter" class="pull-right"><span class="fa fa-filter"></span> Search Filter</a></small></strong></h5></div>
				<div class="panel-body">

					<div class="">
						<div class="form-group">
							<b><label class="">Date Start: </label></b> <u>{{$request->start}}</u> |
							<b><label class=""> Date End: </label></b> <u>{{$request->end}}</u> |
							<b><label class=""> status: </label></b> <u>{{$statusName->status_name}}</u> |
							<b><label class=""> Region: </label></b> <u>{{$request->region ?? 'none'}}</u> |
							<b><label class=""> Level: </label></b> <u>{{$request->level ?? 'none'}}</u>
							<b><label class="text text-danger pull-right small">{{$recdetails->total()}} records found <a href="" class="text text-danger" data-toggle="modal" data-target="#viewregion"><span class="fa fa-list"></span></a></label></b>
						</div>
					</div>

					<!-- <table border="0" cellspacing="5" cellpadding="5"> -->
					        <!-- <tbody><tr>
					            <td>Minimum age:</td>
					            <td><input type="text" id="min" name="min"></td>
					        </tr>
					        <tr>
					            <td>Maximum age:</td>
					            <td><input type="text" id="max" name="max"></td>
					        </tr>
					    </tbody> -->
<!-- 					    <div class="ui form">
						<div class="two fields">
							<div class="field">
								<label>Start date</label>
								<div class="ui calendar" id="rangestart">
									<div class="ui input left icon">
										<i class="calendar icon"></i>
										<input type="text" placeholder="Start" id="min" name="min">
									</div>
								</div>
							</div>
							<div class="field">
								<label>End date</label>
								<div class="ui calendar" id="rangeend">
									<div class="ui input left icon">
										<i class="calendar icon"></i>
										<input type="text" placeholder="End" id="max" name="max">
									</div>
								</div>
							</div>
						</div>
					</div> -->
					<!-- 	</table> -->
					
					<table class="table table-hover table-border" id="recdetails_table">
						<div class="form-group">

						</div>
						<thead>
							<tr>
								<th></th>
								<th scope="col">TYPE</th>
								<th scope="col">REGION</th>
								<th scope="col">LEVEL</th>
								<th scope="col">REC NAME</th>
								<th scope="col">INTITUTION NAME</th>
								<th scope="col">DATE ACCREDITED</th>
								<th scope="col">DATE ACCREDITATION EXPIRY</th>
								<th scope="col">STATUS</th>
							</tr>
						</thead>
						<tbody>
							@foreach($recdetails as $key => $rec)
							<tr class="table-tr" data-url="view-recdetails/{{encrypt($rec->recdetails_id)}}'">
								<td>{{$key+1}}</td>
								<td>{{$rec->recdetails->status->status_name}}</td>
								<td>{{$rec->regions->region_name}}</td>
								<td>{{$rec->levels->level_name}}</td>
								<td>{{$rec->recdetails->rec_name}}</td>
								<td>{{$rec->recdetails->rec_institution}}</td>

								@if($rec->date_accreditation != null)
								<td>{{\Carbon\Carbon::createFromFormat('Y-m-d',$rec->date_accreditation)->format('F j, Y')}}</td>
								@else
								<td>---</td>
								@endif

								@if($rec->date_accreditation_expiry != null)
								<td>{{\Carbon\Carbon::createFromFormat('Y-m-d',$rec->date_accreditation_expiry)->format('F j, Y')}}</td>
								@else
								<td>---</td>
								@endif

								<td>{{$rec->status->status_name}}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					<div class="form-group"> {{ $recdetails->links() }} </div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="viewregion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h6 class="modal-title" id="exampleModalLabel">Searched Regions As of <b class="text-primary">
					@if($request->start != null)
					{{\Carbon\Carbon::createFromFormat('Y-m-d', $request->start)->format('F j, Y')}} 
					@else
					<text>---</text>
					@endif
					to
					@if($request->end != null)
					{{\Carbon\Carbon::createFromFormat('Y-m-d', $request->end)->format('F j, Y')}}</b>
					@else
					<text>---</text>
					@endif
				</h6>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="container-fluid">
					<table class="table table-hover">
						<thead class="small">
							<tr>
								<th>Regions</th>
								<th>No. of Accredited REC's</th>
							</tr>
						</thead>
						<tbody>
							@foreach($regions as $region)
							<tr>
								<td>{{$region->region_name}}</td>
								<td><span class="">{{$region->total}}</span></td>
							</tr>	
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

@endsection
<!-- <script type="text/javascript">

	$(function(){

		

		var oTable = $('#recdetails_table').DataTable({

			dom: '<"fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix ui-corner-tl ui-corner-tr"HlfrB>'+
				't'+'<"fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix ui-corner-bl ui-corner-br"ip>',

        	buttons:[
        		// ['copy', 'csv', 'excel', 'pdf', 'print'],
        		{  
       				extend: 'colvis',
		          
       			},

        		{ 
        		   extend: 'excel',
		           footer: true,
		           exportOptions: {
		                columns: []
		           }
       			},

       			{  
       				extend: 'copy',
		          
       			},

       			{  
       				extend: 'csv',
		          
       			},

       			{  
       				extend: 'pdf',
		          
       			},
       			{  
       				extend: 'print',
       				message: "REC Records",

       				customize: function (win) {
                    $(win.document.body)
                        .css('font-size', '10pt')
                        .prepend(
                            '<img src="{{asset('image/image002.jpg')}}" style="position:static; top:10px; left:10px;" />'
                        );

                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
       			}
			       
      		 ],

      		stateSave: true,
			processing: true,
			serverSide: true,
			ajax: {
				url: '',
			},
      		
			columns: [
			{ data: 'recdetails_id',   name: 'recdetails.id', searchable:true, visible:false},
			{ data: 'status.status_name', name: 'status.status_name'},
			{ data: 'applicationlevel.regions.region_name', name: 'applicationlevel.regions.region_name', visible:false},
			{ data: 'applicationlevel.levels.level_name', name: 'applicationlevel.levels.level_name'},
			{ data: 'rec_name', name: 'recdetails.rec_name'},
			{ data: 'rec_institution', name: 'recdetails.rec_institution'},
			{ data: 'rec_email', name: 'recdetails.rec_email', visible:false},
			{ data: 'created_at', name: 'applicationlevel.created_at', visible:false},
			{ data: 'date_accreditationFORMAT', name: 'applicationlevel.date_accreditation'},
			{ data: 'date_accreditation_expiryFORMAT', name: 'applicationlevel.date_accreditation_expiry'},
			{ data: 'applicationlevel.status.status_name', name: 'applicationlevel.status.status_name'},
			{ data: 'action', name:'action', orderable: false, searchable: false}
			
			],
			
		});

		// table.buttons().container()
		// .appendTo( $('.col-sm-6:eq(0)', table.table().container() ) );

		// $('#rangestart').calendar({
		// 	type: 'date',
		// 	endCalendar: $('#rangeend')
		// });
		// $('#rangeend').calendar({
		// 	type: 'date',
		// 	startCalendar: $('#rangestart')
		// });
	});

	// $(function(){
	    // var table = $('#recdetails_table').DataTable();
		    // Event listener to the two range filtering inputs to redraw on input
		    
		// });

		// $.fn.dataTable.ext.search.push(
		// 	function( settings, data, dataIndex ) {
		// 		var min = parse( $('#min').val(), 10 );
		// 		var max = parse( $('#max').val(), 10 );
  //       		var created_at = parse( data[7] ) || 0; // use data for the age column

	 //        if ( ( isNaN( min ) && isNaN( max ) ) ||
	 //        	( isNaN( min ) && created_at <= max ) ||
	 //        	( min <= created_at  && isNaN( max ) ) ||
	 //        	( min <= created_at  && created_at <= max ) ){
  //       		return true;
  //       	}
  //       		return false;
  //  		 	}
  //   	);

		// $(function(){
	 //    var table = $('#recdetails_table').DataTable();
		//     // Event listener to the two range filtering inputs to redraw on input
		    // $('#min, #max').keyup( function() {
		    //     table.draw();
		    // });
		// });

</script> -->