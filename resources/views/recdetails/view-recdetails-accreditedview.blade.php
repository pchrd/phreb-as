<table class="table table-hover table-border">
	<tbody>
		<tr class="text-sm">
			<td>• Name of Accreditors:</td>
			<td><b>{{$recdetails->existing_recs->name_of_accreditors}}</b></td>
		</tr>
		<tr class="text-sm">
			<td>• Date of Requirements Decked to Accreditors:</td>
			<td><b>{{$recdetails->existing_recs->date_req_deck_accreditors}}</b></td>
		</tr>
		<tr class="text-sm">
			<td>• Deadline of PHREB Accreditors Assessment Form:</td>
			<td><b>{{$recdetails->existing_recs->date_deadline_accreditors_ass}}</b></td>
		</tr>
		<tr class="text-sm">
			<td>• Date Accreditors Sent Assessment Form to PHREB Secretariat:</td>
			<td><b>{{$recdetails->existing_recs->date_accreditors_send_ass}}</b></td>
		</tr>

		<tr class="text-sm">
			<td>• Date of Accreditation Visit Final Report:</td>
			<td><b>{{$recdetails->existing_recs->date_accreditation_visit_finalreport}}</b></td>
		</tr>

		<tr class="text-sm">
			<td>• Date REC Received Accreditor's Evaluation Form:</td>
			<td><b>{{$recdetails->existing_recs->date_rec_recieved_acc_ass}}</b></td>
		</tr>
		<tr class="text-sm">
			<td>• Date REC Sent Action Plan & Compliance Evidences:</td>
			<td><b>{{$recdetails->existing_recs->date_rec_sent_apce}}</b></td>
		</tr>
		<tr class="text-sm">
			<td>• Date Accreditors Sent Evaluation of Action Plan & Compliance Evidences to PHREB Secretariat:</td>
			<td><b>{{$recdetails->existing_recs->date_accreditors_sent_apce}}</b></td>
		</tr>
		<tr class="text-sm">
			<td>• Date Evaluation of Action Plan & Compliance Evidences Forwarded to CSA Chair:</td>
			<td><b>{{$recdetails->existing_recs->date_forward_to_csachair}}</b></td>
		</tr>
	</tbody>
</table>