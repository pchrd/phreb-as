<div class="form-group row">
    <div class="form-control">
    <label for="" class="col-md-4 col-form-label text-md-left text-danger text-small"><h6>{{ __('Application Status*') }}</h6></label>

    <div class="col-md-12">
        <select id="status" type="text" class="form-control{{ $errors->has('status') ? ' is-invalid' : '' }}" name="status" value="{{ old('status') }}" placeholder="">

            <option value="{{$recdetails->applicationlevel->statuses_id}}">{{$recdetails->applicationlevel->status->status_name}}</option>

            @foreach($selectstatuses as $id => $status)
            <option value="{{$id}}">{{$status}}</option>
            @endforeach
            
        </select>
    </div><br>
</div>
</div>


<div class="form-group row">
    <label for="selectapptype" class="col-md-4 col-form-label text-md-left">{{ __('Type of Application*') }}</label>
    <div class="col-md-8">
        <select id="selectapptype" type="text" class="form-control{{ $errors->has('selectapptype') ? ' is-invalid' : '' }}" name="selectapptype" value="{{ old('selectapptype') }}" placeholder="" required autofocus>
            <option value="{{$recdetails->rec_apptype_id}}">
             {{$recdetails->status->status_name}}
         </option>
         <!-- get the data in db and put in select -->
         @foreach($selectapptype as $id => $selectapptype)
         <option value="{{$id}}">{{$selectapptype}}</option>
         @endforeach
     </select>
 </div>
</div>

<div class="form-group row">
    <label for="selectlevel" class="col-md-4 col-form-label text-md-left"><b></b>{{ __('Level of Accreditation*') }}</label>
    <div class="col-md-8">
        <select id="selectlevel" type="text" class="form-control{{ $errors->has('selectlevel') ? ' is-invalid' : '' }}" name="selectlevel" value="{{ old('selectlevel') }}" placeholder="">

            <option value="{{$recdetails->applicationlevel->level_id}}">{{ $recdetails->applicationlevel->levels->level_name }}</option>

            @foreach($selectlevels as $id => $selectlevel)
            <option value="{{$id}}">{{ $selectlevel }}</option>
            @endforeach
        </select>
    </div></div>

    <div class="form-group row">
        <label for="rec_region" class="col-md-4 col-form-label text-md-left"><b></b>{{ __('Region*') }}</label>
        <div class="col-md-8">
            <select id="rec_region" type="text" class="form-control{{ $errors->has('rec_region') ? ' is-invalid' : '' }}" name="rec_region" value="{{ old('rec_region') }}" placeholder="">

                <option value="{{$recdetails->applicationlevel->region_id}}">{{ $recdetails->applicationlevel->regions->region_name }}</option>

                @foreach($selectregions as $id => $selectregion)
                <option value="{{$id}}">{{$selectregion}}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group row">
        <label for="rec_name" class="col-md-4 col-form-label text-md-left">{{ __('Name of REC*:') }}</label>
        <div class="col-md-8">

            <select name="rec_name[]" id="recname" class="input-group{{ $errors->has('created_at') ? ' is-invalid' : '' }}" multiple="multiple" style="width: 100%; background-color: #00f;">
            
                <option value="{{ $recdetails->rec_name }}" selected>{{ $recdetails->rec_name }}</option>

                @foreach($recnamelists as $id => $recnamelist)
                <option value="{{$recnamelist}}">{{$recnamelist}}</option>
                @endforeach
            </select>

            @if ($errors->has('rec_name'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('rec_name') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="rec_institution" class="col-md-4 col-form-label text-md-left">{{ __('Name of Institution*:') }}</label>
        <div class="col-md-8">
            <input id="rec_institution" type="text" class="form-control{{ $errors->has('rec_institution') ? ' is-invalid' : '' }}" name="rec_institution" required="" value="{{$recdetails->rec_institution}}">

            @if ($errors->has('rec_institution'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('rec_institution') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="rec_address" class="col-md-4 col-form-label text-md-left">{{ __('REC Address*:') }}</label>
        <div class="col-md-8">
            <input id="rec_address" type="text" class="form-control{{ $errors->has('rec_address') ? ' is-invalid' : '' }}" name="rec_address" required="" value="{{$recdetails->rec_address}}">

            @if ($errors->has('rec_address'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('rec_address') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="rec_email" class="col-md-4 col-form-label text-md-left">{{ __('REC Email*:') }}</label>
        <div class="col-md-8">
            <input id="rec_email" type="email" class="form-control{{ $errors->has('rec_email') ? ' is-invalid' : '' }}" name="rec_email" required="" value="{{$recdetails->rec_email}}">

            @if ($errors->has('rec_email'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('rec_email') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="rec_telno" class="col-md-4 col-form-label text-md-left">{{ __('REC Tel No:') }}</label>
        <div class="col-md-8">
            <input id="rec_telno" type="text" class="form-control{{ $errors->has('rec_telno') ? ' is-invalid' : '' }}" name="rec_telno" value="{{$recdetails->rec_telno}}">

            @if ($errors->has('rec_telno'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('rec_telno') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="rec_faxno" class="col-md-4 col-form-label text-md-left">{{ __('REC Fax No:') }}</label>
        <div class="col-md-8">
            <input id="rec_faxno" type="text" class="form-control{{ $errors->has('rec_faxno') ? ' is-invalid' : '' }}" name="rec_faxno" value="{{$recdetails->rec_faxno}}">

            @if ($errors->has('rec_faxno'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('rec_faxno') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="rec_chairname" class="col-md-4 col-form-label text-md-left">{{ __('Name of REC Chair*:') }}</label>
        <div class="col-md-8">
            <input id="rec_chairname" type="text" class="form-control{{ $errors->has('rec_chairname') ? ' is-invalid' : '' }}" name="rec_chairname" value="{{$recdetails->recchairs->rec_chairname}}" required="">

            @if ($errors->has('rec_chairname'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('rec_chairname') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="rec_chairemail" class="col-md-4 col-form-label text-md-left">{{ __('Email of REC Chair*:') }}</label>
        <div class="col-md-8">
            <input id="rec_chairemail" type="text" class="form-control{{ $errors->has('rec_chairemail') ? ' is-invalid' : '' }}" name="rec_chairemail" value="{{$recdetails->recchairs->rec_chairemail}}" required="">

            @if ($errors->has('rec_chairemail'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('rec_chairemail') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="rec_chairmobile" class="col-md-4 col-form-label text-md-left">{{ __('Mobile No. of REC Chair*:') }}</label>
        <div class="col-md-8">
            <input id="rec_chairmobile" type="text" class="form-control{{ $errors->has('rec_chairmobile') ? ' is-invalid' : '' }}" name="rec_chairmobile" value="{{$recdetails->recchairs->rec_chairmobile}}" required="">

            @if ($errors->has('rec_chairmobile'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('rec_chairmobile') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="rec_contactperson" class="col-md-4 col-form-label text-md-left">{{ __('Contact Person*:') }}</label>
        <div class="col-md-8">
            <input id="rec_contactperson" type="text" class="form-control{{ $errors->has('rec_contactperson') ? ' is-invalid' : '' }}" name="rec_contactperson" required="" value="{{$recdetails->rec_contactperson}}">

            @if ($errors->has('rec_contactperson'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('rec_contactperson') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="rec_contactposition" class="col-md-4 col-form-label text-md-left">{{ __('Contact Person Position:') }}</label>
        <div class="col-md-8">
            <input id="rec_contactposition" type="text" class="form-control{{ $errors->has('rec_contactposition') ? ' is-invalid' : '' }}" name="rec_contactposition" value="{{$recdetails->rec_contactposition}}">

            @if ($errors->has('rec_contactposition'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('rec_contactposition') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="rec_contactmobileno" class="col-md-4 col-form-label text-md-left">{{ __('Contact Person Mobile No:') }}</label>
        <div class="col-md-8">
            <input id="rec_contactmobileno" type="text" class="form-control{{ $errors->has('rec_contactmobileno') ? ' is-invalid' : '' }}" name="rec_contactmobileno" value="{{$recdetails->rec_contactmobileno}}">

            @if ($errors->has('rec_contactmobileno'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('rec_contactmobileno') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="rec_contactemail" class="col-md-4 col-form-label text-md-left">{{ __('Contact Person Email:*') }}</label>
        <div class="col-md-8">
            <input id="rec_contactemail" type="email" class="form-control{{ $errors->has('rec_contactemail') ? ' is-invalid' : '' }}" name="rec_contactemail" required="" value="{{$recdetails->rec_contactemail}}">

            @if ($errors->has('rec_contactemail'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('rec_contactemail') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="rec_dateestablished" class="col-md-4 col-form-label text-md-left">{{ __('Date REC Established:*') }}</label>
        <div class="col-md-8">
            <input id="rec_dateestablished" type="date" class="form-control{{ $errors->has('rec_dateestablished') ? ' is-invalid' : '' }}" name="rec_dateestablished" required="" value="{{$recdetails->rec_dateestablished}}">

            @if ($errors->has('rec_dateestablished'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('rec_dateestablished') }}</strong>
            </span>
            @endif
        </div><br>
    </div>

    <div class="form-group row">
        <label for="date_accreditation" class="col-md-4 col-form-label text-md-left text-danger">{{ __('Date Effectivity of Accreditation:*') }}</label>
        <div class="col-md-8">
            <input id="date_accreditation" type="date" class="form-control{{ $errors->has('date_accreditation') ? ' is-invalid' : '' }}" name="date_accreditation" value="{{$recdetails->applicationlevel->date_accreditation}}">

            @if ($errors->has('date_accreditation'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('date_accreditation') }}</strong>
            </span>
            @endif
        </div><br>
    </div>

    <div class="form-group row">
        <label for="date_accreditation_expiry" class="col-md-4 col-form-label text-md-left text-danger">{{ __('Date Accreditation Expiry:*') }}</label>
        <div class="col-md-8">
            <input id="date_accreditation_expiry" type="date" class="form-control{{ $errors->has('date_accreditation_expiry') ? ' is-invalid' : '' }}" name="date_accreditation_expiry" value="{{$recdetails->applicationlevel->date_accreditation_expiry}}">

            @if ($errors->has('date_accreditation_expiry'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('date_accreditation_expiry') }}</strong>
            </span>
            @endif
        </div><br>
    </div>


    <div class="form-group row">
        <label for="accreditation_number" class="col-md-4 col-form-label text-md-left text-danger">{{ __('Accreditation Number*') }}</label>
        <div class="col-md-8">
            <input id="accreditation_number" type="text" class="form-control{{ $errors->has('accreditation_number') ? ' is-invalid' : '' }}" name="accreditation_number" value="{{$recdetails->applicationlevel->accreditation_number}}">

            @if ($errors->has('accreditation_number'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('accreditation_number') }}</strong>
            </span>
            @endif
        </div><br>
    </div>


    <div class="form-group row">
        <label for="rec_accreditation" class="col-md-4 col-form-label text-md-left text-danger">{{ __('Accreditation*') }}</label>
        <div class="col-md-8">

         <select name="rec_accreditation[]" id="accreditation" class="input-group{{ $errors->has('created_at') ? ' is-invalid' : '' }}" multiple="multiple" style="width: 100%; background-color: #00f;">
            
            <option value="{{ $recdetails->applicationlevel->rec_accreditation }}" selected>{{ $recdetails->applicationlevel->rec_accreditation }}</option>

            @foreach($selectaccreditation as $key => $selectaccreditations)
            <option value="{{$selectaccreditations}}">{{$selectaccreditations}}</option>
            @endforeach
        </select>

            @if ($errors->has('rec_accreditation'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('rec_accreditation') }}</strong>
            </span>
            @endif
        </div><br>
    </div>