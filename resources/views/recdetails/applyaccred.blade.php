@extends('layouts.myApp')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">

            <div class="col-md-12" id="savedclass">
                @if(session('success'))
                    <div class="alert alert-success ">
                        <i class="fa fa-exclamation"></i>{{session('success')}}
                        <button class="close" onclick="document.getElementById('savedclass').style.display='none'">x</button>
                    </div>
                @endif
            </div>


            <div class="card">
                <div class="card-body">
                    <img width="100%" border="0" height="160px" alt="" class="img-max justify-content-center" style="display: block; padding-left: 60px; padding-right: 60px; color: #3F3D33; margin-bottom: 25px;" src="{{asset('image/image002.jpg')}}"/>
                    <form method="POST" action="{{ route('add_rec_details') }}" aria-label="">
                        {{ csrf_field() }}

                        <div class="form-group col-md-12">

                        <strong><h5><b><span class="fa fa-chevron-circle-right"></span> Research Ethics Committee Details</b></h5></strong>

                        @can('secretariat-access')
                        <!-- custom select box yes or no -->
                         <div class="form-group row">
                            <label for="yesorno" class="col-md-4 col-form-label text-md-left">{{ __('Already Accredited?') }}</label>

                            <div class="col-md-8">
                                 <select type="text" class="form-control{{ $errors->has('yesorno') ? ' is-invalid' : '' }}" name="yesorno" value="{{ old('yesorno') }}" placeholder="" autofocus id="yesorno">
                                    <option value="" disabled selected>---Please Choose---</option>
                                    <option value="1">Yes</option>
                                    <option value="">No</option>
                                </select>

                                @if ($errors->has('yesorno'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('yesorno') }}</strong>
                                </span>
                                @endif
                               
                            </div>
                        </div>
                        @endcan

                        <!-- custom select box select apptype -->
                         <div class="form-group row">
                            <label for="selectapptype" class="col-md-4 col-form-label text-md-left">{{ __('Type of Application*') }}</label>

                            <div class="col-md-8">

                                <select id="selectapptype" type="text" class="form-control{{ $errors->has('selectapptype') ? ' is-invalid' : '' }}" name="selectapptype" value="{{ old('selectapptype') }}" placeholder="" required autofocus id="selectapptype">
                                    <option value="" disabled selected>---Please select type---</option>
                                    <!-- get the data in db and put in select -->
                                    @foreach($selectapptype as $id => $selectapptype)
                                    <option value="{{$id}}">{{$selectapptype}}</option>
                                    @endforeach

                                </select>

                                @if ($errors->has('selectapptype'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('selectapptype') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <!-- ACCREDITED REC 2014-PRESENT FROM EXCEL DATABASE -->
                        <input type="text" name="sample" id="sample" style="display: none">

                        <div id="accredited" style="display: none">

                          <!-- Date of Requirements Completion -->
                        <div class="form-group row sm">
                            <label for="date_completed" class="col-md-4 col-form-label text-md-left text-danger">{{ __('Date of Completion of Requirements*:') }}</label>

                            <div class="col-md-8">
                                <input id="date_completed" type="date" class="form-control{{ $errors->has('date_completed') ? ' is-invalid' : '' }}" name="date_completed" value="{{ old('date_completed') }}">

                                @if ($errors->has('date_completed'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('date_completed') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <!-- Date of Accreditation -->
                        <div class="form-group row sm">
                            <label for="date_accreditation" class="col-md-4 col-form-label text-md-left text-danger">{{ __('Date of Accreditation*:') }}</label>

                            <div class="col-md-8">
                                <input id="date_accreditation" type="date" class="form-control{{ $errors->has('date_accreditation') ? ' is-invalid' : '' }}" name="date_accreditation" value="{{ old('date_accreditation') }}">

                                @if ($errors->has('date_accreditation'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('date_accreditation') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <!-- Date of Accreditation Expiry -->
                        <div class="form-group row sm">
                            <label for="date_accreditation_expiry" class="col-md-4 col-form-label text-md-left text-danger">{{ __('Date of Accreditation Expiry*:') }}</label>

                            <div class="col-md-8">
                                <input id="date_accreditation_expiry" type="date" class="form-control{{ $errors->has('date_accreditation_expiry') ? ' is-invalid' : '' }}" name="date_accreditation_expiry" value="{{ old('date_accreditation_expiry') }}">

                                @if ($errors->has('date_accreditation_expiry'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('date_accreditation_expiry') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                         <!-- Accreditation number -->
                        <div class="form-group row sm">
                            <label for="accreditation_number" class="col-md-4 col-form-label text-md-left text-danger">{{ __('Accreditation No.*:') }}</label>

                            <div class="col-md-8">
                                <input id="accreditation_number" type="text" class="form-control{{ $errors->has('accreditation_number') ? ' is-invalid' : '' }}" name="accreditation_number" value="{{ old('accreditation_number') }}" placeholder="Enter accreditation number...">

                                @if ($errors->has('accreditation_number'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('accreditation_number') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        </div>

                        
                         <!-- custom select box lvl of accreditation -->
                         <div class="form-group row">
                            <label for="selectlevel" class="col-md-4 col-form-label text-md-left">{{ __('Level of Accreditation*') }}</label>

                            <div class="col-md-8">

                                <select id="selectlevel" type="text" class="form-control{{ $errors->has('selectlevel') ? ' is-invalid' : '' }}" name="selectlevel" value="{{ old('selectlevel') }}" placeholder="" required autofocus>
                                    <option value="" disabled selected>---Please select level---</option>
                                    <!-- get the data in db and put in select -->

                                    @foreach($selectlevels as $id => $selectlevel)
                                    <option value="{{$id}}">{{$selectlevel}}</option>
                                    @endforeach

                                </select>

                                @if ($errors->has('selectlevel'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('selectlevel') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <!-- rec_region -->
                        <div class="form-group row">
                            <label for="selectregions" class="col-md-4 col-form-label text-md-left"><b></b>{{ __('Region*') }}</label>
                            <div class="col-md-8">

                                <select id="selectregions" type="text" class="form-control{{ $errors->has('selectregions') ? ' is-invalid' : '' }}" name="selectregions" value="{{ old('selectregions') }}" placeholder="" required="">
                                    <option value="" disabled selected>---Please select region---</option>
                                    <!-- get the data in db and put in select -->
                                    @foreach($selectregions as $id => $selectregion)
                                    <option value="{{$id}}">{{$selectregion}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('selectregions'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('selectregions') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <!-- name of REC -->
                        <div class="form-group row">
                            <label for="rec_name" class="col-md-4 col-form-label text-md-left">{{ __('Name of REC*:') }}</label>

                            <div class="col-md-8">

                                  <select name="rec_name" id="recname" class="input-group{{ $errors->has('created_at') ? ' is-invalid' : '' }}" style="width: 100%; background-color: #00f;" required>
                                     <!-- get the data in db and put in select -->
                                    @foreach($selectrecname as $id => $selectrecname)
                                    <option value="{{$selectrecname}}">{{$selectrecname}}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('rec_name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('rec_name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <!-- name of Institution -->
                        <div class="form-group row">
                            <label for="rec_institution" class="col-md-4 col-form-label text-md-left">{{ __('Name of Institution*:') }}</label>

                            <div class="col-md-8">
                                <input id="rec_institution" type="text" class="form-control{{ $errors->has('rec_institution') ? ' is-invalid' : '' }}" name="rec_institution" value="{{ old('rec_institution') }}" required>

                                @if ($errors->has('rec_institution'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('rec_institution') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <!-- REC Address -->
                        <div class="form-group row">
                            <label for="rec_address" class="col-md-4 col-form-label text-md-left">{{ __('REC Address*:') }}</label>

                            <div class="col-md-8">
                                <input id="rec_address" type="text" class="form-control{{ $errors->has('rec_address') ? ' is-invalid' : '' }}" name="rec_address" value="{{ old('rec_address') }}" required>

                                @if ($errors->has('rec_address'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('rec_address') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <!-- REC Email Address -->
                        <div class="form-group row">
                            <label for="rec_email" class="col-md-4 col-form-label text-md-left">{{ __('REC Email*:') }}</label>

                            <div class="col-md-8">
                                <input id="rec_email" type="email" class="form-control{{ $errors->has('rec_email') ? ' is-invalid' : '' }}" name="rec_email" value="{{ old('rec_email') }}" required>

                                @if ($errors->has('rec_email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('rec_email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <!-- REC Tel No -->
                        <div class="form-group row">
                            <label for="rec_telno" class="col-md-4 col-form-label text-md-left">{{ __('REC Tel No*:') }}</label>

                            <div class="col-md-8">
                                <input id="rec_telno" type="text" class="form-control{{ $errors->has('rec_telno') ? ' is-invalid' : '' }}" name="rec_telno" value="{{ old('rec_telno') }}" required>

                                @if ($errors->has('rec_telno'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('rec_telno') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <!-- REC Fax No| 12/18/18 was changed from fax to mobile--> 
                        <div class="form-group row">
                            <label for="rec_faxno" class="col-md-4 col-form-label text-md-left">{{ __('REC Mobile No*:') }}</label>

                            <div class="col-md-8">
                                <input id="rec_faxno" type="text" class="form-control{{ $errors->has('rec_faxno') ? ' is-invalid' : '' }}" name="rec_faxno" value="{{ old('rec_faxno') }}" required>

                                @if ($errors->has('rec_faxno'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('rec_faxno') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div><br>
                    </div>



                    <div class="form-group col-md-12">
                        <h5><strong><span class="fa fa-chevron-circle-right"></span> REC Chair Details</strong></h5>

                        <!-- name of REC chair -->
                        <div class="form-group row">
                            <label for="rec_chairname" class="col-md-4 col-form-label text-md-left">{{ __('Name of REC Chair*:') }}</label>

                            <div class="col-md-8">
                                <input id="rec_chairname" type="text" class="form-control{{ $errors->has('rec_chairname') ? ' is-invalid' : '' }}" name="rec_chairname" value="{{ old('rec_chairname') }}" required="">

                                @if ($errors->has('rec_chairname'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('rec_chairname') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <!-- email of REC chair -->
                        <div class="form-group row">
                            <label for="rec_chairemail" class="col-md-4 col-form-label text-md-left">{{ __('Email of REC Chair*:') }}</label>

                            <div class="col-md-8">
                                <input id="rec_chairemail" type="email" class="form-control{{ $errors->has('rec_chairemail') ? ' is-invalid' : '' }}" name="rec_chairemail" value="{{ old('rec_chairemail') }}" required="">

                                @if ($errors->has('rec_chairemail'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('rec_chairemail') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <!-- mobile of REC chair -->
                        <div class="form-group row">
                            <label for="rec_chairmobile" class="col-md-4 col-form-label text-md-left">{{ __('Mobile No. of REC Chair*:') }}</label>

                            <div class="col-md-8">
                                <input id="rec_chairmobile" type="text" class="form-control{{ $errors->has('rec_chairmobile') ? ' is-invalid' : '' }}" name="rec_chairmobile" value="{{ old('rec_chairmobile') }}" required="">

                                @if ($errors->has('rec_chairmobile'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('rec_chairmobile') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div><br>

                    <div class="form-group col-md-12">
                       <h5><b><span class="fa fa-chevron-circle-right"></span> Contact Details</b></h5>
                        <!-- REC contact person -->
                        <div class="form-group row">
                            <label for="rec_contactperson" class="col-md-4 col-form-label text-md-left">{{ __('Contact Person*:') }}</label>

                            <div class="col-md-8">
                                <input id="rec_contactperson" type="text" class="form-control{{ $errors->has('rec_contactperson') ? ' is-invalid' : '' }}" name="rec_contactperson" value="{{ old('rec_contactperson') }}" required>

                                @if ($errors->has('rec_contactperson'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('rec_contactperson') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <!-- contactperson position -->
                        <div class="form-group row">
                            <label for="rec_contactposition" class="col-md-4 col-form-label text-md-left">{{ __('Contact Person Position*:') }}</label>

                            <div class="col-md-8">
                                <input id="rec_contactposition" type="text" class="form-control{{ $errors->has('rec_contactposition') ? ' is-invalid' : '' }}" name="rec_contactposition" value="{{ old('rec_contactposition') }}" required>

                                @if ($errors->has('rec_contactposition'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('rec_contactposition') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <!-- contactperson mobile no -->
                        <div class="form-group row">
                            <label for="rec_contactmobileno" class="col-md-4 col-form-label text-md-left">{{ __('Contact Person Mobile No*:') }}</label>

                            <div class="col-md-8">
                                <input id="rec_contactmobileno" type="text" class="form-control{{ $errors->has('rec_contactmobileno') ? ' is-invalid' : '' }}" name="rec_contactmobileno" value="{{ old('rec_contactmobileno') }}" required>

                                @if ($errors->has('rec_contactmobileno'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('rec_contactmobileno') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <!-- contact person email -->
                        <div class="form-group row">
                            <label for="rec_contactemail" class="col-md-4 col-form-label text-md-left">{{ __('Contact Person Email*:') }}</label>

                            <div class="col-md-8">
                                <input id="rec_contactemail" type="email" class="form-control{{ $errors->has('rec_contactemail') ? ' is-invalid' : '' }}" name="rec_contactemail" value="{{ old('rec_contactemail') }}" required>

                                @if ($errors->has('rec_contactemail'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('rec_contactemail') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <!-- Date of REC Established -->
                        <div class="form-group row">
                            <label for="rec_dateestablished" class="col-md-4 col-form-label text-md-left"><h6>{{ __('Date REC Established*:') }}</h6></label>

                            <div class="col-md-8">
                                <input id="rec_dateestablished" type="date" class="form-control{{ $errors->has('rec_dateestablished') ? ' is-invalid' : '' }}" name="rec_dateestablished" value="{{ old('rec_dateestablished') }}" required>

                                @if ($errors->has('rec_dateestablished'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('rec_dateestablished') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>


                    <div class="card ">
                      <div class="card-header">
                        <b>Informed Consent</b>
                    </div>
                    <div class="card-body">

                            @foreach($statements as $id => $statement)
                            
                                <ul>
                                    <input type="checkbox" value="{{$statement->id}}" name="statement[id][]" id="statementID{{$statement->id}}" required> 
                                        {{$statement->statement_description}}
                                </ul>
                           
                            @endforeach
                         
                    </div>
                </div>


                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <a href="" class="btn btn-success fa fa-floppy-o" data-toggle="modal" data-target=".bs-example-modal-sm-saverecdetails" id="applyaccred_save" style="display: none"> Save</a>
                        <a href="" class="btn btn-secondary fa fa-refresh" data-toggle="modal" data-target=".bs-example-modal-sm-resetfields"> Reset</a>
                        <a href="" class="btn btn-danger fa fa-close" data-toggle="modal" data-target=".bs-example-modal-sm-close"> Cancel</a> 
                    </div>

                    <!--   //submit recdetails modal --> 
                    <div class="modal bs-example-modal-sm-saverecdetails fade" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h6>Application Confirmation </h6>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">Are you sure you want to save only your application? This allow you to edit or update information. Thank you!</div>
                                <div class="modal-footer"><button type="submit" class="btn btn-success btn-lg btn-block" data-toggle="modal" data-target=".bs-example-modal-sm-submitrecdetails" onclick="submitForm(this);"><i class="fa fa-file"></i> Save Now </button></div>
                            </div>
                        </div>
                    </div>

                    <!--   //close and go back modal --> 
                    <div class="modal fade bs-example-modal-sm-close" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h6>Application Cancellation </h6>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body"><i class="glyphicon glyphicon-question-sign"></i> Are you sure you want to cancel your application?</div>
                                <div class="modal-footer">
                                    <a href="{{url('/dashboard')}}" class="btn btn-danger btn-lg btn-block" >Yes</a>
                                </div>
                            </div>
                        </div>
                    </div> 

                    <!--   //resetfields modal --> 
                    <div class="modal fade bs-example-modal-sm-resetfields" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h6>Application Reset </h6>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body"><i class="glyphicon glyphicon-question-sign"></i> Are you sure you want to clear text fields?</div>
                                <div class="modal-footer">
                                    <button type="reset"class="btn btn-secondary btn-lg btn-block" title="Reset all fields to empty">Reset</button>
                                </div>
                            </div>
                        </div>
                    </div> 



                </form>
            </div> 
        </div>
    </div>
</div>
</div>
@endsection     