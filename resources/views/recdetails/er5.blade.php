<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">Document Type</th>
      <th scope="col">Filename</th>
      <th scope="col">Year(s)</th>
      <th scope="col">Date of Submission</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>

  @foreach($recdetails->existing_arps() as $r_arps)

    @if(!$r_arps)
      No file uploaded...
    @else
    <tr>
      <td>{{$r_arps->documenttypes->document_types_name}}</td>

      @if($r_arps->file != null)
        <td><a href="../storage/phrebdatabasefiles/{{$recdetails->id}}/{{$r_arps->file_uniqid}}.{{$r_arps->file_extension}}" target="_blank" class="text text-warning"><span class="fa fa-file"></span> {{$r_arps->file}}</a></td>
      @else
        <td></td>
      @endif

      <td>{{$r_arps->foryear}}</td>
      <td>{{\Carbon\Carbon::createFromFormat('Y-m-d', $r_arps->arps_submission)->format('F j, Y')}}</td>
      <td><a href="{{route('delete_arps', ['id' => $r_arps->id])}}" class="text-danger" title="Delete"><span class="fa fa-trash" onclick="return confirm('Do you want to Delete the {{$r_arps->documenttypes->document_types_name}} - {{$r_arps->file}} for year {{$r_arps->foryear}} ?')"></span></a></td>
    </tr>
    @endif
  @endforeach
  </tbody>
</table>