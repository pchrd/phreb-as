@extends('layouts.myApp')
@section('content')
<div class="container">

	<div class="col-md-12" id="verifyuser">
		@if(session('success'))
		<div class="alert alert-success">
			{{session('success')}}
			<button class="close" onclick="document.getElementById('verifyuser').style.display='none'">x</button>
		</div>
		@endif
	</div> 

		<div class="row">
			<div class="col-md-12 col-md-offset-1">
				<div class="panel panel-info">
					<div class="panel-heading"><h3><strong>REC APPLICATION RECORDS - <small> {{\Request::get('key')}}</small></strong> | 
						@foreach($count as $counts)	
							<small>{{$counts->level_name}}</small>
							<label class="badge badge-secondary"> {{$counts->counts}} </label>
						@endforeach
					</h3></div>
					<div class="panel-body">
					<table class="table table-bordered table-condensed table-hover small" id="recdetails_table">
							<thead>
								<tr>
									<td scope="col">ID</td>
									<td scope="col">CATEGORY TYPE</td>
									<td scope="col">REGION</td>
									<td scope="col">LEVEL</td>
									<td scope="col">REC NAME</td>
									<td scope="col">INTITUTION NAME</td>
									<td scope="col">REC EMAIL</td>
									<td scope="col">DATE SUBMITTED</td>
									<td scope="col">DATE ACCREDITED</td>
									<td scope="col">DATE ACCREDITATION EXPIRY</td>
									<td scope="col">CURRENT STATUS</td>
									<td scope="col">ACCREDITATION</td>
									<td scope="col">REMARKS/NOTES</td>
									<td></td>
									<td scope="col">ACTIONS</td>
								</tr>
							</thead>
					</table>
					</div>
					<div class='container-fluid'> 
						<div class="container-fluid">
						<b><p>Legend:
						 
								<i class="badge badge-danger">Indicates Expired </i > 
								<i class="badge badge-warning">Indicates Near Expiration </i>
							
							</p> 
						</b>
						</div> 
					</div>
				</div>
			</div>
		</div>
	</div>
	
<script type="text/javascript">

	$(function(){
		// alert(moment().add(1, 'M').format('YYYY-MM-DD'))
		var oTable = $('#recdetails_table').DataTable({

			responsive: true,
			
			dom: '<"fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix ui-corner-tl ui-corner-tr"HlfrB>'+
				't'+'<"fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix ui-corner-bl ui-corner-br"ip>',

        	buttons:[
        		// ['copy', 'csv', 'excel', 'pdf', 'print'],
        		{  
       				extend: 'colvis',
		          
       			},

        		{ 
        		   extend: 'excel',
		           footer: true,
		           exportOptions: {
		                columns: []
		           }
       			},

       			{  
       				extend: 'copy',
		          
       			},

       			{  
       				extend: 'csv',
		          
       			},

       			{  
       				extend: 'pdf',
		          
       			},
       			{  
       				extend: 'print',
       				message: "REC Records",

       				customize: function (win) {
                    $(win.document.body)
                        .css('font-size', '10pt')
                        .prepend(
                            '<img src="{{asset('image/image002.jpg')}}" style="position:static; top:10px; left:10px;" />'
                        );

                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                	}
       			}
			       
      		 ],

      		stateSave: true,
			processing: true,
			serverSide: true,
			ajax: {
				url: '{{ route('recdetails/on-going-view', ['key' => Request::get('key')]) }}',
			},
      		
			columns: [
			{ data: 'recdetails_id',   name: 'recdetails.id', searchable:true, visible:false},
			{ data: 'status.status_name', name: 'status.status_name'},
			{ data: 'region_name', name: 'regions.region_name', visible:true},
			{ data: 'level_description', name: 'levels.level_description'},
			{ data: 'rec_name', name: 'recdetails.rec_name', visible:false},
			{ data: 'rec_institution', name: 'recdetails.rec_institution'},
			{ data: 'rec_email', name: 'recdetails.rec_email', visible:false},
			{ data: 'created_at', name: 'applicationlevel.created_at', visible:false},
			{ data: 'date_accreditationFORMAT', name: 'applicationlevel.date_accreditation'},
			{ data: 'date_accreditation_expiryFORMAT', name: 'applicationlevel.date_accreditation_expiry'},
			{ data: 'status_name', name: 'statuses.status_name'},
			{ data: 'rec_accreditation', name: 'applicationlevel.rec_accreditation', visible: false},
			{ data: 'rec_change_of_acc', name: 'rec_change_of_acc', orderable: false, searchable: false, visible: false},
			{ data: 'date_accreditation_expiry', name: 'applicationlevel.date_accreditation_expiry', visible: false},
			{ data: 'action', name:'action', orderable: false, searchable: false}
			],

			createdRow: function(row, data, dataIndex){
				
				if (data['date_accreditation_expiry'] <= moment().format('YYYY-MM-DD')){
				 	$(row).find('td:eq(6)').addClass('badge badge-danger');
	      		}else if (data['date_accreditation_expiry'] <= moment().add(1, 'M').format('YYYY-MM-DD')){
	      			$(row).find('td:eq(6)').addClass('badge badge-warning');
	      		}
	      	}

		});
	});
</script>
@endsection