<!-- Modal for submit to accreditors -->
<div class="modal fade" id="send_accreditors">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header badge badge-primary">
				<h4 class="modal-title">Send to Accreditors</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="container-fluid">
					<form action="{{ route('sendtoaccreditor') }}" method="POST">
						{{ csrf_field() }}
						<div class="form-group col-md-12">
							<!-- input rec email address -->
							<input type="hidden" name="a_recdetails_id" class="form-control{{ $errors->has('a_recdetails_id') ? ' is-invalid' : '' }}" value="{{$recdetails->applicationlevel->recdetails_id}}">

							<div class="form-group row">
								<label for="a_accreditors_user_id" class="col-md-4 col-form-label text-md-left"><h6>{{ __('Name of Accreditors') }}</h6></label>
								<div class="col-md-8">
									<select class="form-control{{ $errors->has('a_accreditors_user_id') ? ' is-invalid' : '' }}" name="a_accreditors_user_id"><option value="" disabled selected>---Choose Accreditors---</option>
										@foreach($selectaccreditors as $id => $selectaccreditor)
										<option value="{{$id}}">{{$selectaccreditor}}</option>
										@endforeach
									</select>
									@if ($errors->has('a_recdetails_id'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('a_recdetails_id') }}</strong>
									</span>
									@endif
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-primary">Send</button>
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@foreach($get_sendaccreditors as $get_sendaccreditor)
<!-- Modal for remove accreditors -->
<div class="modal fade" id="removeacc{{$get_sendaccreditor->sendacc_tbl_id}}">
	<div class="modal-dialog sm">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header badge badge-danger">
				<h4 class="modal-title">Remove Confirmation</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="container-fluid">
					<form action="{{ route('removeacc', ['sendacc_tbl_id' => $get_sendaccreditor->sendacc_tbl_id]) }}" method="POST">
						{{ csrf_field() }}
						<p class="">Do you want to remove <b>{{$get_sendaccreditor->name}}</b> as Accreditor/Coordinator for this REC: <b>{{$get_sendaccreditor->rec_name}}</b>?</p>
						<div class="modal-footer">
							<button type="submit" class="btn btn-danger">Remove</button>
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endforeach

<!-- Modal for send OR to REC -->
<div class="modal fade" id="sendOR">
	<div class="modal-dialog sm">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header badge badge-primary">
				<h4 class="modal-title">Send Order of Payment</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="container-fluid">
					<p>Do you want to send OR/Order of Payment to the REC: <b>{{$recdetails->rec_name}} - {{$recdetails->rec_institution}}</b>?</p>
					<div class="modal-footer">
						<a href="{{route('sendOR', ['recdetail' => $recdetails->applicationlevel->recdetails_id])}}" class="btn btn-primary">Send</a>
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal for generatre OR to REC,cahsier,accounting -->
<div class="modal fade" id="generateOR">
	<div class="modal-dialog sm">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header badge badge-primary">
				<h4 class="modal-title">Generate Order of Payment</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="container-fluid">
					<p>Do you want to Generate Order of Payment copies for REC, Cashier and Accounting of this REC <b>{{$recdetails->rec_name}} - {{$recdetails->rec_institution}}</b>?</p>
					<div class="modal-footer">
						<a href="{{route('generateOR', ['recdetail' => $recdetails->applicationlevel->recdetails_id])}}" class="btn btn-primary">Yes</a>
						<button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal for send OR req and Receipt -->
<div class="modal fade" id="sendorreceipt">
	<div class="modal-dialog sm">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header badge badge-primary">
				<h4 class="modal-title">Final OR and Receipt</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="container-fluid">
					<p>Do you want to send softcopy of <b>{{$recdetails->rec_name}} - {{$recdetails->rec_institution}}'s OR and Receipt </b>? <small><a href="#uploadorrec" data-toggle="collapse" aria-expanded="false" aria-controls="uploadorrec"><span class="fa fa-file"></span> Click here to Upload</a></small></p>
					<div class="collapse" id="uploadorrec">

						<form class="form-control" action="{{route('sendorreceipt', ['recdetails_id' => $recdetails->applicationlevel->recdetails_id])}}" method="POST" enctype="multipart/form-data"> {{csrf_field()}}
							<label for="sendorreceipt" class="col-md-6 col-form-label text-md-left">{{ __('Upload Files:') }}</label>
							<div class="col-md-12">
								<input id="sendorreceipt" type="file" class="form-control{{ $errors->has('sendorreceipt') ? ' is-invalid' : '' }}" name="sendorreceipt[]"  multiple accept="application/*" required="">
								@if ($errors->has('sendorreceipt'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('sendorreceipt') }}</strong>
								</span>
								@endif
							</div>
							<div class="modal-footer">
								<button type="submit" class="btn btn-info btn-sm pull-right">Upload</button>
							</div>
						</form>	

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--   //Generate Awarding Report --> 
<div class="modal bs-example-modal-sm-awardingletter" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog ">
		<div class="modal-content">
			<div class="modal-header badge badge-primary">
				<h4>Create Awarding</h4> 
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">

				<div class="form-group row">
					<label for="awarding_letter" class="col-md-4 col-form-label text-md-left"><h6>{{ __('Acceditation Year: ') }}</h6></label>
					<div class="col-md-8">
						<form action="{{route('awarding', ['id' => $recdetails->applicationlevel->recdetails_id])}}" method="GET">
							{{csrf_field()}}
							<select class="form-control{{ $errors->has('awarding_letter') ? ' is-invalid' : '' }}" name="awarding_letter" required><option value="" disabled selected>---Please Select---</option>
								<!-- get list from db -->
								@foreach($selectawarding as $id => $sa)
								<option value="{{$id}}"><span class="fa fa-file"></span>{{$sa}}</option>
								@endforeach
							</select>
							@if ($errors->has('awarding_letter'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('awarding_letter') }}</strong>
							</span>
							@endif
							<div class="modal-footer">
								<button type="submit" class="btn btn-default">Submit</button>
							</div>
						</form>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>

<div class="modal bs-example-modal-sm-acc_no" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header badge-secondary">
				<h6>Accreditation Number</h6> 
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<form action="{{route('acc_no', ['id' => $recdetails->applicationlevel->recdetails_id])}}" method="POST">
					{{ csrf_field() }}
					<div class="col-md-12">
						<input type="text" class="form-control{{ $errors->has('acc_no') ? ' is-invalid' : '' }}" name="acc_no" value="{{$recdetails->applicationlevel->accreditation_number}}" required>
						@if ($errors->has('acc_no'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('acc_no') }}</strong>
						</span>
						@endif
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-success btn-lg btn-block">Submit</button>
					</div>
				</form>

			</div>
			
		</div>
	</div>
</div>

<div class="modal bs-example-modal-sm-acc_expiry" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header badge-secondary">
				<h6>Accreditation Expiry Date</h6> 
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<form action="" method="POST">
					{{ csrf_field() }}
					<div class="col-md-12">
						<input type="date" class="form-control{{ $errors->has('acc_no') ? ' is-invalid' : '' }}" name="acc_no" value="{{$recdetails->applicationlevel->date_accreditation_expiry}}" required>
						@if ($errors->has('acc_no'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('acc_no') }}</strong>
						</span>
						@endif
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-success btn-lg btn-block">Submit</button>
					</div>
				</form>

			</div>
			
		</div>
	</div>
</div> 

<!--arps modal -->
<div class="modal fade" id="arps" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Upload Annual Report & Protocol Summary</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

				<form action="{{route('add_arps', ['id' => $recdetails->id])}}" method="POST" enctype="multipart/form-data">
				{{csrf_field()}}
				<div class="form-group row">
					<label for="selectarps" class="col-md-4 col-form-label text-md-left"><h6>{{ __('Upload as ') }}</h6></label>
					<div class="col-md-8">
						<select id="selectarps" type="text" class="form-control{{ $errors->has('selectarps') ? ' is-invalid' : '' }}" name="selectarps" value="{{ old('selectarps') }}" placeholder="" required autofocus id="selectarps">
							<option value="" disabled selected>---Please select---</option>
							<!-- get the data in db and put in select -->
							@foreach($selectarps as $id => $selectarps)
								<option value="{{$id}}">{{$selectarps}}</option>
							@endforeach
						</select>
						@if ($errors->has('selectarps'))
						<span class="invalid-feedback" role="alert"><strong>{{ $errors->first('selectarps') }}</strong></span>
						@endif
					</div>
				</div>

				<div class="form-group row">
					<label for="foryear" class="col-md-4 col-form-label text-md-left"><h6>{{ __('For Year ') }}</h6></label>
					<div class="col-md-8">
						<input id="foryear" type="number" class="form-control{{ $errors->has('foryear') ? ' is-invalid' : '' }}" name="foryear" value="{{ old('foryear') }}" required>

						@if ($errors->has('foryear'))
						<span class="invalid-feedback" role="alert"><strong>{{ $errors->first('foryear') }}</strong></span>
						@endif
					</div>
				</div>

				<div class="form-group row">
					<label for="arps_submission" class="col-md-4 col-form-label text-md-left"><h6>{{ __('Date of Submission') }}</h6></label>
					<div class="col-md-8">
						<input id="arps_submission" type="date" class="form-control{{ $errors->has('arps_submission') ? ' is-invalid' : '' }}" name="arps_submission" value="{{ old('arps_submission') }}" required>

						@if ($errors->has('arps_submission'))
						<span class="invalid-feedback" role="alert"><strong>{{ $errors->first('arps_submission') }}</strong></span>
						@endif
					</div>
				</div>

				<div class="form-group row">
					<label for="file" class="col-md-4 col-form-label text-md-left"><h6>{{ __('File') }}</h6></label>
					<div class="col-md-8">
						<input type="file" class="form-control{{ $errors->has('file') ? ' is-invalid' : '' }}" name="file[]" multiple="true" accept="application/" value="{{ old('file') }}">

						@if ($errors->has('file'))
						<span class="invalid-feedback" role="alert"><strong>{{ $errors->first('arps_submission') }}</strong></span>
						@endif
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary"><span class="fa fa-save"></span> Save</button>
				</div>
			</form>
			</div>
			</div>
		</div>
	</div>

	<!--arps modal -->
<div class="modal fade" id="sendexpirynotifs" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Accreditation Reminder</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="form-group">
					Do you want to send email reminder of <b>{{$recdetails->rec_name}}- {{$recdetails->rec_institution}}</b> for Accreditation Expiration Date?
				</div> 
				<div class="modal-footer">
					<a href="{{route('sendaccreditationexpiry_reminder', ['id'=>$recdetails->id])}}" class="btn btn-primary"><span class="fa fa-send"></span> Send</a>
				</div>
			</div>
			</div>
		</div>
	</div>

	<!--new accreditation per records modal -->
<div class="modal fade" id="newacc" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Changing of Application in Accreditation </h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<form action="{{route('changeofacc', ['id' => $recdetails->id])}}" method="POST">
						{{csrf_field()}}
							<div class="form-group row">
                            <label for="selectapptype" class="col-md-4 col-form-label text-md-left">{{ __('Select New Type of Application*') }}</label>

                            <div class="col-md-8">
                                <select id="selectapptype" type="text" class="form-control{{ $errors->has('selectapptype') ? ' is-invalid' : '' }}" name="selectapptype" value="{{ old('selectapptype') }}" placeholder="" required autofocus>
                                    <option value="{{$recdetails->rec_apptype_id}}">
                                    	{{$getapptype->status_name}}
                                    </option>
                                    <!-- get the data in db and put in select -->
                                    @foreach($selectapptype1 as $id => $selectapptype)
                                    <option value="{{$id}}">{{$selectapptype}}</option>
                                    @endforeach

                                </select>

                                @if ($errors->has('selectapptype'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('selectapptype') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
								<label for="selectlevel" class="col-md-4 col-form-label text-md-left"><b></b>{{ __('Select New Accreditation Level*') }}</label>

								<div class="col-md-8">
									<select id="selectlevel" type="text" class="form-control{{ $errors->has('selectlevel') ? ' is-invalid' : '' }}" name="selectlevel" value="{{ old('selectlevel') }}" placeholder="">
										
										<option value="{{$recdetails->applicationlevel->level_id}}">{{ $recdetails->applicationlevel->levels->level_name }}</option>

										@foreach($selectlevels as $id => $selectlevel)
										<option value="{{$id}}">{{ $selectlevel }}</option>
										@endforeach
									</select>
									@if ($errors->has('selectlevel'))
									<span class="invalid-feedback">
										<strong>{{ $errors->first('selectlevel') }}</strong>
									</span>
									@endif
								</div>
							</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-primary"><span class="fa fa-send"></span> Submit</button>
						</div>
				</form>
				</div> 
				
			</div>
			</div>
		</div>
	</div>