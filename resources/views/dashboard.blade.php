@extends('layouts.myApp')

@section('content')

<div class="wrapper row-offcanvas row-offcanvas-left ">

  <!-- Right side column. Contains the navbar and content of the page -->
  <aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="col-md-8" id="savedclass">
        @if(session('success'))
        <div class="alert alert-primary">
          {{session('success')}}
          <button class="close" onclick="document.getElementById('savedclass').style.display='none'">x</button>
        </div>
        @endif
      </div>
      <h1>
        Dashboard
        <small>Control panel | 
          <span class="pull-right text text-info"> Logged-in as {{$users->role_users->roles->role_name}}

            @can('secretariat-access')
                at {{$users->role_users->assignedRegions->region_name ?? 'PCHRD-DOST NCR - National Capital Region'}} 
            @endcan 

        </span></small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <div class="row">

         @can('rec-access')
        <div class="col-lg-9 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>
                Create New Application
              </h3>
              <p>
                for PHREB Accreditation
              </p>
            </div>
            <div class="icon">
              <i class="fa fa-file"></i>
            </div>
            <a href="{{url($duplicationnotice == false ? 'applyaccred' : 'applyaccrednotice')}}" class="small-box-footer blink_me">
              Click Here to Apply <i class=" fa fa-plus"></i>
            </a>
          </div>
        </div><!-- ./col -->
        @endcan
        
      </div>


      <div class="row">

        <div class=" {{ auth::user()->can('rec-access') ? 'col-lg-9' : 'col-lg-3' }} col-xs-6">
          <!-- small box -->
          <div class="small-box bg-maroon">
            <div class="inner">
              <h3>
               @can('secretariat_and_admin')

                  {{$count_recdetails_submitted}}

               <p>

                All REC Applications
                @endcan

                @can('rec-access')
                {{$countrecdetails_perrec}}
                <p>
                 My RECs Application List
               </p>
               @endcan

               @can('accreditors-access')
               {{$count_rec_for_acc}}
               <p>Your Assigned RECs
               </p>
               @endcan

               @can('csachair-access')
               {{$count_rec_for_acc}}
               <p>REC Management
               </p>
               @endcan
             </h3>
           </p>
         </div>
         <div class="icon">
          <i class="fa fa-book"></i>
        </div>
        <a href="{{route('rec-management')}}" class="small-box-footer">
          More info <i class="fa fa-arrow-circle-right"></i>
        </a>
      </div>
    </div><!-- ./col -->

    @can('csachair-access')
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3>
           0
         </h3>
         <p>
          PHREB Forms Submissions
        </p>
      </div>
      <div class="icon">
        <i class="fa fa-paper-plane"></i>
      </div>
      <a href="{{route('phrebforms-submissions')}}" class="small-box-footer">
        More info <i class="fa fa-arrow-circle-right"></i>
      </a>
    </div>
  </div><!-- ./col -->
  @endcan

  @can('secretariat_and_admin')


  

  @can('secretariat-access')
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-green">
      <div class="inner">
        <h3>
          {{$countongoing}}
        </h3>
        <p>
         Ongoing Applications
       </p>
     </div>
     <div class="icon">
      <i class="fa fa-file"></i>
    </div>
    <a href="{{route('status', 'key=On-going Application')}}" class="small-box-footer">
      More info <i class="fa fa-arrow-circle-right"></i>
    </a>
  </div>
</div><!-- ./col -->
@endcan

<div class="col-lg-3 col-xs-6">
  <!-- small box -->
  <div class="small-box bg-red">
    <div class="inner">
      <h3>
        {{$count_pending}}
      </h3>
      <p>
        Pending REC Applications
      </p>
    </div>
    <div class="icon">
      <i class="fa fa-globe"></i>
    </div>
    <a href="{{url('/pending?search=Pending')}}" class="small-box-footer">
      More info <i class="fa fa-arrow-circle-right"></i>
    </a>
  </div>
</div><!-- ./col -->
</div><!-- /.row -->
@endcan

</section><!-- /.content -->

@can('secretariat_and_admin')

<!-- Main content row no.2 -->
<section class="content">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-black">
        <div class="inner">
          <h3>
            {{$count_expired}}
          </h3>
          <p>
           Expired Applications
         </p>
       </div>
       <div class="icon">
        <i class="fa fa-exclamation"></i>
      </div>
      <a href="{{url('/status?key=Accreditation Expired')}}" class="small-box-footer">
        More info <i class="fa fa-arrow-circle-right"></i>
      </a>
    </div>
  </div><!-- ./col -->

  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-light-blue">
      <div class="inner">
        <h3>

          {{$count_complete}}

        </h3>
        <p class="small">
          Tagged Completed Requirements
        </p>
      </div>
      <div class="icon">
        <i class="fa fa-check"></i>
      </div>
      <a href="{{url('/status?key=Completed Requirements')}}" class="small-box-footer">
        More info <i class="fa fa-arrow-circle-right"></i>
      </a>
    </div>
  </div><!-- ./col -->
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-gray">
      <div class="inner">
        <h3>
         {{$count_incomplete}}
       </h3>
       <p class="small">
        Tagged Incomplete Requirements
      </p>
    </div>
    <div class="icon">
      <i class="fa fa-remove"></i>
    </div>
    <a href="{{url('/status?key=Incomplete Requirements')}}" class="small-box-footer">
      More info <i class="fa fa-arrow-circle-right"></i>
    </a>
  </div>
</div><!-- ./col -->
</div><!-- /.row -->

</section><!-- /.content -->

<!-- Main content row no.2 -->
<section class="content">
  <!-- Small boxes (Stat box) -->
  <div class="row">

    @can('only-admin-can-access-page')
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-fuchsia">
        <div class="inner">
          <h3>
            {{$phrebdatabase}}
          </h3>
          <p>
            PHREB REC Existing Records
          </p>
        </div>
        <div class="icon">
          <i class="fa fa-file-archive-o"></i>
        </div>
        <a href="#" class="small-box-footer">
          More info <i class="fa fa-arrow-circle-right"></i>
        </a>
      </div>
    </div><!-- ./col -->
    @endcan

    @can('secretariat-access')

    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-fuchsia">
        <div class="inner">
          <h3>
            {{$count_resubmission}}
          </h3>
          <p>
            Resubmission of Requirements
          </p>
        </div>
        <div class="icon">
          <i class="fa fa-arrow-left"></i>
        </div>
        <a href="{{url('/status?key=Re-submission of Requirements')}}" class="small-box-footer">
          More info <i class="fa fa-arrow-circle-right"></i>
        </a>
      </div>
    </div><!-- ./col -->

    @endcan


  @if(Auth::user()->role_users->user_assigned_region_id == null)
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-purple">
        <div class="inner">
          <h3>

            <strong> {{$or}} </strong>

          </h3>
          <p>
            Order of Payment
          </p>
        </div>
        <div class="icon">
          <i class="fa fa-money"></i>
        </div>
        <a href="{{url('/orderofpayment')}}" class="small-box-footer">
          More info <i class="fa fa-arrow-circle-right"></i>
        </a>
      </div>
    </div><!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-teal">
        <div class="inner">
          <h3>
           {{$reports}}
         </h3>
         <p>
          Reports
        </p>
      </div>
      <div class="icon">
        <i class="fa fa-sort-numeric-asc"></i>
      </div>
      <a href="{{url('/show-report')}}" class="small-box-footer">
        More info <i class="fa fa-arrow-circle-right"></i>
      </a>
    </div>
  </div><!-- ./col -->
@endif


</div><!-- /.row -->

</section><!-- /.content -->
@endcan

<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="col-md-8" id="savedclass">
  </div>
  <h1>
    Accredited RECs in {{$users->role_users->assignedRegions->region_name ?? 'PCHRD-DOST NCR - National Capital Region'}}
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-blue">
        <div class="inner">
          <h3>
           {{$count_lvl1}}
         </h3>
         <p>
           LEVEL 1
         </p>
       </div>
       <div class="icon">
        <i class="fa fa-level-up"></i>
      </div>
      <a href="{{url('/filter?filter=level+1')}}" class="small-box-footer">
        More info <i class="fa fa-arrow-circle-right"></i>
      </a>
    </div>
  </div><!-- ./col -->

  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-olive">
      <div class="inner">
        <h3>
         {{$count_lvl2}}
       </h3>
       <p>
        LEVEL 2
      </p>
    </div>
    <div class="icon">
      <i class="fa fa-level-up"></i>
    </div>
    <a href="{{url('/filter?filter=level+2')}}" class="small-box-footer">
      More info <i class="fa fa-arrow-circle-right"></i>
    </a>
  </div>
</div><!-- ./col -->
<div class="col-lg-3 col-xs-6">
  <!-- small box -->
  <div class="small-box bg-lime">
    <div class="inner">
      <h3>
       {{$count_lvl3}}
     </h3>
     <p>
      LEVEL 3
    </p>
  </div>
  <div class="icon">
    <i class="fa fa-level-up"></i>
  </div>
  <a href="{{url('/filter?filter=level+3')}}" class="small-box-footer">
    More info <i class="fa fa-arrow-circle-right"></i>
  </a>
</div>
</div><!-- ./col -->
</div><!-- /.row -->
</section>

@can('secretariat-access')
<section class="content">
  <div class="row">
      <div class="col-lg-9 col-xs-6">

        <table class="table table-bordered table-condensed table-hover">
          
          <tr>
            <th>Accredited RECs who submitted Compliances and Evidences</th>
            <td>Latest Status</td>
          </tr>

          @foreach($accreditedrecsCompliances as $id => $compliances)
            <tr>
              <td>
                <span class="fa fa-file"></span> <a href="{{route('view-recdetails', ['id' => encrypt($compliances->applicationlevel->recdetails_id)])}}"> {{$compliances->id}} - {{str_limit($compliances->rec_institution.' - '.$compliances->rec_name, $limit=50, $end="...")}}  </a>
                <p>Accreditation Date: <b>{{\Carbon\Carbon::createFromFormat('Y-m-d', $compliances->applicationlevel->date_accreditation)->format('F j, Y')}} - {{\Carbon\Carbon::createFromFormat('Y-m-d', $compliances->applicationlevel->date_accreditation_expiry)->format('F j, Y')}}</b></p>

                <p>{{$compliances->applicationlevel->levels->level_name}}</p>
              </td>

              <td>
                @foreach($compliances->submissionsstatuses as $statuses)
                  @if($loop->first)
                  <li><i class="{{$statuses->status->id == 20 ? 'badge badge-primary blink_me' : ''}}">{{$statuses->status->status_name}}</i></li>
                   
                        

                   @endif
                @endforeach
              </td>
            </tr>
          @endforeach

        </table>
      
      </div>
  </div>
</section>
@endcan


@if(Auth::user()->role_users->user_assigned_region_id == null)
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="col-md-8" id="savedclass">
  </div>
  <h1>
    Trainings Events/Orientations
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3>
           {{$traineesmanagement}}
         </h3>
         <p>
           Training/Oientation Attendances
         </p>
       </div>
       <div class="icon">
        <i class="fa fa-calendar"></i>
      </div>
      <a href="{{url('/show-training-mngt')}}" class="small-box-footer">
        More info <i class="fa fa-arrow-circle-right"></i>
      </a>
    </div>
  </div><!-- ./col -->

  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-green">
      <div class="inner">
        <h3>
         {{$counttrainees}}
       </h3>
       <p>
        Trainees/Attendees
      </p>
    </div>
    <div class="icon">
      <i class="fa fa-users"></i>
    </div>
    <a href="{{url('/show-trainees-list')}}" class="small-box-footer">
      More info <i class="fa fa-arrow-circle-right"></i>
    </a>
  </div>
</div><!-- ./col -->
<div class="col-lg-3 col-xs-6">
  <!-- small box -->
  <div class="small-box bg-navy">
    <div class="inner">
      <h3>
       {{$counttrainingevents}}
     </h3>
     <p>
      Events/Orientations Management
    </p>
  </div>
  <div class="icon">
    <i class="fa fa-building"></i>
  </div>
  <a href="{{url('/show-events-mngt')}}" class="small-box-footer">
    More info <i class="fa fa-arrow-circle-right"></i>
  </a>
</div>
</div><!-- ./col -->
</div><!-- /.row -->

<div class="row">
  @can('secretariat_and_admin')
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-orange">
      <div class="inner">
        <h3>
          @foreach($count_users_verified as $count_users_verifieds)
          <strong> {{$output[$count_users_verifieds->verified] = $count_users_verifieds->total}} </strong>
          @endforeach
        </h3>
        <p>
          User Management
        </p>
      </div>
      <div class="icon">
        <i class="fa fa-user"></i>
      </div>
      <a href="{{route('user-management')}}" class="small-box-footer">
        More info <i class="fa fa-arrow-circle-right"></i>
      </a>
    </div>
  </div><!-- ./col -->
  @endcan
</div>
</section>

@endif


@can('only-admin-access')
<section class="content-header">
  <div class="col-md-8" id="savedclass">
  </div>
  <h1>
    Summary of REC per Region
  </h1>
</section>

<section class="content">

  <div class="row">
    <table class="table table-hovered col-md-9">
      <thead>
        <tr>
          <th>Region</th>
          <th>No. of Accredited REC</th>
        </tr>
      </thead>

      <tbody>
        @foreach($regions as $region)
        <tr>
          <td><li><a href='{{url("/filter?region=$region->region_name")}}' class="text text-primary">{{$region->region_name}}</a></td>
            <td><span class="badge badge-secondary">{{$region->total}}</span></td></td>
          </tr> {{$region->regionSum}}
          @endforeach
        </tbody>
      </table>
    </div>

  </section>
  @endcan
</aside><!-- /.right-side -->
</div><!-- ./wrapper -->
@endsection