@extends('layouts.myApp')

@section('content')
<div class="container">

	<div class="col-md-12" id="verifyuser">
		@if(session('success'))
		<div class="alert alert-success">
			{{session('success')}}
			<button class="close" onclick="document.getElementById('verifyuser').style.display='none'">x</button>
		</div>
		@endif
	</div> 

	<div class="btn-group-vertical pull-right" style="position: fixed;bottom: 78px;
	right: 3px; width:170px; margin-right:20px; padding: 10px 20px;border-radius: 4px;border-color: #46b8da; z-index: 1">

		<a href="#" data-toggle="modal" data-target="#messageReply" class="btn btn-info"><span class="fa fa-reply"></span> Reply</a>
		
	</div>

	<div class="row">
		<div class="col-md-12 col-md-offset-1">
			<div class="panel panel-info">

				<div class="panel-heading"><h3><span class="fa fa-envelope-open"></span><strong> {{$viewMail->messageSubject}} 
					<small>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $viewMail->created_at)->format('F j, Y - G:i:A')}} ({{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $viewMail->created_at)->diffforhumans()}})<a href="javascript:window.location.reload(true)" class="pull-right small"><span class="fa fa-refresh"></span> Refresh</a></small></strong></h3>
				</div>

				<div id="">
					<div class="card">
						<div class="card-header" id="headingOne">
							
								<button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
									<span class="fa fa-envelope-open-o"></span> Subject: {{$viewMail->messageSubject}}
								</button>
							
							<div class="small pull-right">
								<span class="fa fa-clock-o"></span> {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $viewMail->created_at)->format('F j, Y G:i:A')}} ( {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $viewMail->created_at)->diffforhumans()}})
							</div>
							
							<div class="container small pull-right">
								<label>
									From: <b>{{$viewMail->users->name}} ({{$viewMail->messageFrom}}) </b>
								<br>
									To: 
										<b>
											@foreach($viewMail->messageTo as $mt)
												{{ $mt->users->name }} ({{ $mt->users->email }} )<br>
											@endforeach
										</b>
								</label>
							</div>

						</div>

						<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
							<div class="card-body">
								{!!$viewMail->messages!!}

								<div class="container form-group">
									<h3>Attached Files</h3>

									@foreach($viewMail->messageFiles as $mf)
										<i>
											<a href="{{route('downloadFile', ['id' => encrypt($mf->cmf_filegenerated_id)])}}" title="Click to Download" target="_blank"><span class="fa fa-file"></span> {{$mf->cmf_filename}} </a><br>
										</i>
									@endforeach
								</div>
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="messageReply" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Reply Message</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
      </div>
      <div class="modal-body">

        <form action="{{route('sendmessage', ['reply_id' => \Request::get('reply_id')])}}" method="POST" enctype="multipart/form-data">

        {{ csrf_field() }}
        
        <div class="form-group">
            <label for="recipient-name" class="col-form-label">To:</label>
                <select name="messageTo[]"  class="form-control input-group{{ $errors->has('created_at') ? ' is-invalid' : '' }}" required>
                  
                   @if($viewMail->send_user_id == Auth::user()->id)

	                   	@foreach($viewMail->messageTo as $mt)

	                    	<option value="{{$mt->users->id}}">{{$mt->users->name}} ({{$mt->users->email}})</option>

	                   	@endforeach

                   @else

                    <option value="{{$viewMail->send_user_id}}">{{$viewMail->users->name}} ({{$viewMail->messageFrom}})</option>

                   @endif

                </select>
        </div>

        <div class="form-group">
            <label for="message-text" class="col-form-label">Subject:</label>
            <input type="text" class="form-control" name="messageSubject" value="{{$viewMail->messageSubject}}"/>
        </div>

         <div class="form-group">
            <label for="message-text" class="col-form-label">Message:</label>
            <textarea class="form-control" id="editor2" name="messageBody"></textarea>
        </div>

        <div class="form-group">
            <label for="message-text" class="col-form-label">Attach Files:</label>
            <input type="file" name="messageFile[]" multiple>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary" onclick="return confirm('Do you want to send this message now?')">Send message</button>
        </div>
    </form>

</div>
</div>
</div>
</div>
@endsection