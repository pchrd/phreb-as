@extends('layouts.myApp')

@section('content')
<div class="container">

    <div class="col-md-12" id="verifyuser">
        @if(session('success'))
        <div class="alert alert-success">
            {{session('success')}}
            <button class="close" onclick="document.getElementById('verifyuser').style.display='none'">x</button>
        </div>
        @endif
    </div> 

    <div class="row">
        <div class="col-md-12 col-md-offset-1">
            <div class="panel panel-info">
                <div class="panel-heading"><h3><strong>Ethics Accreditation Mails <small><a href="" data-toggle="modal" data-target="#compose"><span class="fa fa-pencil"></span> Compose</a><a href="javascript:window.location.reload(true)" class="pull-right small"><span class="fa fa-refresh"></span> Refresh</a></small></strong></h3></div>

                <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">

                    <li class="nav-item">
                        <a href="#inbox" data-toggle="tab" class="nav-link text-info" role="tab" title="Show Inbox"><span class="fa fa-envelope"></span> Inbox</a>
                    </li>

                    <li class="nav-item">
                        <a href="#sent" data-toggle="tab" class="nav-link text-info" id="sent-tab" role="tab" title="Show Sent Messages"><span class="fa fa-send"></span> Sent</a>
                    </li>

                    <li class="nav-item">
                        <a href="#trash" data-toggle="tab" class="nav-link text-info" id="trash-tab" role="tab" aria-controls="trash" aria-selected="false" title="Assessments For REC"><span class="fa fa-trash"></span> Trash</a>
                    </li>

                </ul>

                <div id="row my-tab-content" class="tab-content">
                    <div class="tab-pane fade show active" id="inbox" role="tabpanel" aria-labelledby="inbox-tab">
                        <div class="panel-body">
                            <table class="table table-hover small" id="chatmessagetble">
                                <thead>
                                    <tr>
                                        <th scope="col">From</th>
                                        <th scope="col">Subject</th>
                                        <th scope="col">Initial Message</th>
                                        <th scope="col">No. of Replies</th>
                                        <th scope="col">No. of Files Attached</th>
                                        <th scope="col">Date Created</th>
                                        <th scope="col">Date Seen</th>
                                        <th scope="col">Actions</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="sent" role="tabpanel" aria-labelledby="recreq-tab">
                        <div class="panel-body">
                            <table class="table table-hover small" id="chatmessagetble_sent">
                                <thead>
                                    <tr>
    
                                        <th scope="col">From</th>
                                        <th scope="col">To</th> 
                                        <th scope="col">Subject</th>
                                        <th scope="col">Initial Message</th>
                                        <th scope="col">No. of replies</th>
                                        <th scope="col">No. of Files Attached</th>
                                        <th scope="col">Date Created</th>
                                        <th scope="col">Actions</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="compose" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Compose Message</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
      </div>
      <div class="modal-body">

        <form action="{{route('sendmessage')}}" method="POST" enctype="multipart/form-data">

        {{ csrf_field() }}
        <div class="form-group">
            <label for="recipient-name" class="col-form-label">Recipient:</label>
                <select name="messageTo[]" id="compose1" class="input-group{{ $errors->has('created_at') ? ' is-invalid' : '' }}" style="width: 100%; background-color: #00f;" required multiple>
                    @foreach($select_recipient as $id => $sr)
                        <option value="{{$id}}">{{$sr}}</option>
                    @endforeach
                </select>
        </div>

        <div class="form-group">
            <label for="message-text" class="col-form-label">Subject:</label>
            <input type="text" class="form-control" name="messageSubject"/>
        </div>

         <div class="form-group">
            <label for="message-text" class="col-form-label">Message:</label>
            <textarea class="form-control" id="editor2" name="messageBody"></textarea>
        </div>

        <div class="form-group">
            <label for="message-text" class="col-form-label">Attach Files:</label>
            <input type="file" name="messageFile[]" multiple>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Send message</button>
        </div>
    </form>
</div>
</div>
</div>
</div>

<script type="text/javascript">
    $(function(){
        
        // alert(moment().add(1, 'M').format('YYYY-MM-DD'))
        var oTable = $('#chatmessagetble').DataTable({

            responsive: true,
            
            dom: '<"fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix ui-corner-tl ui-corner-tr"HlfrB>'+
                't'+'<"fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix ui-corner-bl ui-corner-br"ip>',

            buttons:[
                // ['copy', 'csv', 'excel', 'pdf', 'print'],
                {  
                    extend: 'colvis',
                  
                },

                { 
                   extend: 'excel',
                   footer: true,
                   exportOptions: {
                        columns: []
                   }
                },

                {  
                    extend: 'copy',
                  
                },

                {  
                    extend: 'csv',
                  
                },

                {  
                    extend: 'pdf',
                  
                },
                {  
                    extend: 'print',
                    message: "Mail",

                    customize: function (win) {
                    $(win.document.body)
                        .css('font-size', '10pt')
                        .prepend(
                            '<img src="{{asset('image/image002.jpg')}}" style="position:static; top:10px; left:10px;" />'
                        );

                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                    }
                }
                   
             ],

            stateSave: true,
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route('accreditationMail/ajaxMail') }}',
            },
            
            columns: [

            { data: 'messageFrom',   name: 'messageFrom', searchable:true, visible:true},
            { data: 'messageSubject',   name: 'messageSubject', searchable:true, visible:true},
            { data: 'messages',   name: 'messages', searchable:true, visible:true},
            { data: 'no_of_replies',   name: 'no_of_replies', searchable:true, visible:true},
            { data: 'no_of_files',   name: 'no_of_files', searchable:true, visible:true},
            { data: 'created_at',   name: 'created_at', searchable:true, visible:true},
            { data: 'messageStatus',   name: 'messageStatus', searchable:true, visible:false},
            { data: 'action', name:'action', orderable: false, searchable: false}
            
            ],

            // "rowCallback": function( row, data ) {
            //     if ( data[ 5 ] == 0 ) {
            //         $('td', row).css('background-color', 'blue');
            //     }
            // }

            createdRow: function(row, data, dataIndex){
                
                if (data['messageStatus'] == null ){
                    $(row).css('background-color', '#e7ebeb');
                }else{
                    
                }
            } 

        });

        var oTable = $('#chatmessagetble_sent').DataTable({

            responsive: true,
            
            dom: '<"fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix ui-corner-tl ui-corner-tr"HlfrB>'+
                't'+'<"fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix ui-corner-bl ui-corner-br"ip>',

            buttons:[
                // ['copy', 'csv', 'excel', 'pdf', 'print'],
                {  
                    extend: 'colvis',
                  
                },

                { 
                   extend: 'excel',
                   footer: true,
                   exportOptions: {
                        columns: []
                   }
                },

                {  
                    extend: 'copy',
                  
                },

                {  
                    extend: 'csv',
                  
                },

                {  
                    extend: 'pdf',
                  
                },
                {  
                    extend: 'print',
                    message: "Mail",

                    customize: function (win) {
                    $(win.document.body)
                        .css('font-size', '10pt')
                        .prepend(
                            '<img src="{{asset('image/image002.jpg')}}" style="position:static; top:10px; left:10px;" />'
                        );

                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                    }
                }
                   
             ],

            stateSave: true,
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route('accreditationMail/ajaxMail/sent') }}',
            },
            
            columns: [

            { data: 'messageFrom',   name: 'messageFrom', searchable:true, visible:true},
            { data: 'messageTo',   name: 'messageTo', searchable:true, visible:true},
            { data: 'messageSubject',   name: 'messageSubject', searchable:true, visible:true},
            { data: 'messages',   name: 'messages', searchable:true, visible:true},
            { data: 'no_of_replies',   name: 'no_of_replies', searchable:true, visible:true},
            { data: 'no_of_files',   name: 'no_of_files', searchable:true, visible:true},
            { data: 'created_at',   name: 'created_at', searchable:true, visible:true},
            { data: 'action', name:'action', orderable: false, searchable: false}
            
            ],

        });
    });

</script>
@endsection