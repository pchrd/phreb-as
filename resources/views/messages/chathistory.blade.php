@include('layouts.myApp_ajax')

<div class="col-md-12" id="verifyuser">
	@if(session('success'))
	<div class="alert alert-success">
		{{session('success')}}
		<button class="close" onclick="document.getElementById('verifyuser').style.display='none'">x</button>
	</div>
	@endif
</div>

<div class="container">
		<div class="row">
			<div class="col-md-12 col-md-offset-1">
				<div class="panel panel-info">
					<div class="panel-heading"><h5><strong>HISTORY MESSAGES</strong></h5></div>
					<div class="panel-body">
						<table class="table table-hover cell-border" id="historymessage">
							<thead class="badge-info">
								<tr>
									<th scope="col">ID</th>
									<th scope="col">FROM</th>
									<th scope="col">TO</th>
									<th scope="col">MESSAGE</th>
									<th scope="col">DATE</th>
									<th scope="col">TIME</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	  @include('layouts.myApp_footer')

<script type="text/javascript">
	$(function() {
		$('#historymessage').DataTable({
			processing: true,
			serverSide: true,
			ajax: '{{ route('chat/chathistory') }}',
			columns: [
			{ data: 'id',   name: 'chatmessages.id' },
			{ data: 'name', name: 'users.name'},
			{ data: 'rec_name', name: 'recdetails.rec_name'},
			{ data: 'messages', name: 'chatmessages.messages'},
			{ data: 'created_at', name: 'chatmessages.created_at'},
			{ data: 'time', name: 'time', searchable: true}
			],
			initComplete: function(){
            this.api().columns([0,1,2]).every(function(){
                var column = this;
                var input = document.createElement("input");
                $(input).appendTo($(column.footer()).empty())
                .on('keypress', function () {
                    column.search($(this).val(), false, false, true).draw();
                });
            });
        	}
		});
	});
</script>