<div class="modal fade" id="messageReply" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Reply Message</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
      </div>
      <div class="modal-body">

        <form action="{{route('sendmessage', ['reply' => encrypt($viewMail->id)])}}" method="POST" enctype="multipart/form-data">

        {{ csrf_field() }}
        
        <div class="form-group">
            <label for="recipient-name" class="col-form-label">To:</label>
                <select name="messageTo[]" id="compose1"  class="form-control input-group{{ $errors->has('created_at') ? ' is-invalid' : '' }}" required style="width:100%">
                  
                   @if($viewMail->send_user_id == Auth::user()->id)

	                   	@foreach($viewMail->messageTo as $mt)

	                    	<option value="{{$mt->users->id}}">{{$mt->users->name}} ({{$mt->users->email}})</option>

	                   	@endforeach

                   @else

                    <option value="{{$viewMail->send_user_id}}">{{$viewMail->users->name}} ({{$viewMail->messageFrom}})</option>

                   @endif

                </select>
        </div>

        <div class="form-group">
            <label for="message-text" class="col-form-label">Subject:</label>
            <input type="text" class="form-control" name="messageSubject" value="{{$viewMail->messageSubject}}"/>
        </div>

         <div class="form-group">
            <label for="message-text" class="col-form-label">Message:</label>
            <textarea class="form-control" id="editor2" name="messageBody"></textarea>
        </div>

        <div class="form-group">
            <label for="message-text" class="col-form-label">Attach Files:</label>
            <input type="file" name="messageFile[]" multiple>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary" onclick="return confirm('Do you want to send this message now?')">Send message</button>
        </div>
    </form>

</div>
</div>
</div>
</div>