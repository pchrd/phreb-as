@extends('layouts.myApp')

@section('content')
<div class="container">

	<div class="col-md-12" id="verifyuser">
		@if(session('success'))
		<div class="alert alert-success">
			{{session('success')}}
			<button class="close" onclick="document.getElementById('verifyuser').style.display='none'">x</button>
		</div>
		@endif
	</div> 

	<div class="btn-group-vertical pull-right" style="position: fixed;bottom: 78px;
	right: 3px; width:170px; margin-right:20px; padding: 10px 20px;border-radius: 4px;border-color: #46b8da; z-index: 1">

		<a href="#" data-toggle="modal" data-target="#messageReply" class="btn btn-info"><span class="fa fa-reply"></span> Reply</a>
		<a href="{{url('/accreditationMail')}}" class="btn btn-secondary fa fa-arrow-left"> Back</a>
	</div>

	<div class="row">
		<div class="col-md-12 col-md-offset-1">
			<div class="panel panel-info">

				<div class="panel-heading"><h3><span class="fa fa-envelope-open"></span><strong> {{$viewMail->messageSubject}} 
					<small>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $viewMail->created_at)->format('F j, Y - G:i:A')}} ({{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $viewMail->created_at)->diffforhumans()}})<a href="javascript:window.location.reload(true)" class="pull-right small"><span class="fa fa-refresh"></span> Refresh</a></small></strong></h3>
				</div>

				<div id="">
					<div class="card">
						<div class="card-header" id="headingOne">
							
								<button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
									<span class="fa fa-envelope-open-o"></span> Subject: {{$viewMail->messageSubject}}
								</button>
							
							<div class="small pull-right">
								<span class="fa fa-clock-o"></span> {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $viewMail->created_at)->format('F j, Y G:i:A')}} ( {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $viewMail->created_at)->diffforhumans()}})
							</div>
							
							<div class="container small pull-right">
								<label>
									From: <b>{{$viewMail->users->name}} ({{$viewMail->messageFrom}}) </b>
								<br>
									To: 
										<b>
											@foreach($viewMail->messageTo as $mt)
												{{ $mt->users->name }} ({{ $mt->users->email }} )<br>
											@endforeach
										</b>
								</label>
							</div>

						</div>

						<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
							<div class="card-body">
								{!!$viewMail->messages!!}

								<div class="container form-group">
									<h3>Attached Files</h3>

									@foreach($viewMail->messageFiles as $mf)
										<i>
											<a href="{{route('downloadFile', ['id' => encrypt($mf->cmf_filegenerated_id)])}}" title="Click to Download" target="_blank"><span class="fa fa-file"></span> {{$mf->cmf_filename}} </a><br>
										</i>
									@endforeach

								</div>
							</div>
						</div>
					</div>
					<div class="container card form-contrl">
						<div class="" id="headingThree">
							<h5 class="mb-0">
								<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
									<span class="fa fa-reply-all"></span> Replies <span class="fa fa-arrow-right"></span> Subject: {{$viewMail->messageSubject}}
								</button>
							</h5>
						</div>
						<div id="collapseThree" class="collapse show" aria-labelledby="headingThree" data-parent="#accordion">
							<div class="card-body">
								<div class="panel-body">
									<table class="table table-hover small" id="chatmessagetble">
										<thead>
											<tr>
												<th>No#</th>
												<th scope="col">Subject</th>
												<th scope="col">Message</th>
												<th scope="col">Attached Files</th>
												<th scope="col">Date Created</th>
											</tr>
										</thead>
										<tbody>
							
											@foreach($viewMail->messageGetReply as $id => $mgr)
											<div id="{{$mgr->messages->id}}">
												<tr style="{{decrypt(\Request::get('#')) == $mgr->messages->id ? 'background-color:#F5D76E;' : ''}}">
													<td>{{$id + 1}}</td>
													<td>{{$mgr->messages->messageSubject}}<br>
														From:<label>{{$mgr->messages->users->name}} ({{$mgr->messages->users->email}})</label>
													</td>
													<td>
														<a href="{{route('viewnmail_seen', ['id' => $mgr->messages->bcrypted_id, 'reply_id' => encrypt($mgr->messagesGetReply->id)])}}" title="View"> 

															@if($mgr->messages->message_id->messageStatus == null AND $mgr->messages->message_id->messageTo_id == Auth::user()->id)
																<label class="badge badge-danger badge-block blink_me small">
																	<h6><span class="fa fa-envelope"></span> New Message, click to view!</h6>	
																</label>
															@else
																{{ str_limit($mgr->messages->messages, $limit = 50, $end = '...') }}
															@endif
														</a> 
													</td>
													<td>
														@foreach($mgr->messageFiles as $mgr_mf)
														<a href="{{route('downloadFile', ['id' => encrypt($mgr_mf->cmf_filegenerated_id)])}}" title="{{$mgr_mf->cmf_filename}}">
															<span class="fa fa-file"></span>
														</a>
														@endforeach
														<br>
														<label class="text text-warning">{{$mgr->messageFiles->count()}} Files</label>
													</td>
													<td>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $mgr->created_at)->format('F j, Y - G:i:A')}}

													<small class="text text-danger">({{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $mgr->created_at)->Diffforhumans()}})</small>
													</td>
												</tr>
											</div>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="" id="hello">

				</div>

			</div>
		</div>
	</div>
</div>
@include('messages.view_mail_modal')
@endsection