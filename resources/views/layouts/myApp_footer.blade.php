@auth
<!-- Footer -->
<footer class="page-footer font-small mdb-color darken-32 pt-1">
    <!-- Copyright -->
    <div class="footer-copyright text-center py-3"><small>©Philippine Council for Health Research and Development |  All Rights Reserved {{ date('Y') }}</small><br>
      <small>visit ethics website: <a href="http://www.ethics.healthresearch.ph/">http://www.ethics.healthresearch.ph/</a></small>
    </div>
    <!-- Copyright -->
</footer>
@endauth