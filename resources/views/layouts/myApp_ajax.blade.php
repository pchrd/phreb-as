@extends('layouts.myApp_ajax1')
<link rel="icon" type="image/png" href="{{ asset('tablogo.png') }}">
@stack('script')

  <div id="container">
    <!-- navbar -->
    <nav class="navbar navbar-expand-md navbar navbar-dark bg-dark navbar-laravel navbar-static-top">

     <div class="container">
      <a class="navbar-brand" href="{{url('dashboard')}}" title="Go to Dashboard">
        {{ config('', 'PHREB ACCREDITATION') }}
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <!-- Right Side Of Navbar -->
        <ul class="navbar-nav ml-auto">
            <a id="navbarDropdown" class="nav-link" href="{{url('\dashboard')}}" role="button"><span class="fa fa-user"></span> {{ Auth::user()->name }} </a>
      </ul>
      </div>
  	</div>

</nav>
</div>
<br>