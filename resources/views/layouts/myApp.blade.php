<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <link rel="icon" type="image/png" href="{{ asset('tablogo.png') }}">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  @auth
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

  @endauth 
  
  <title>PHREB Accreditation System</title>

  <script src="https://cdn.tiny.cloud/1/t1vwljr6d8ttt51l036g2ftg7wwgg5sokn3ihb13f647r0gi/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

 <!--  <script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=t1vwljr6d8ttt51l036g2ftg7wwgg5sokn3ihb13f647r0gi"></script> -->

  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.3/themes/silver/theme.min.js"></script>

  <script type="text/javascript" src="{{asset('js/preventdelete.js')}}"></script>

  <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
  <!-- <script src="//cdn.ckeditor.com/4.11.2/full/ckeditor.js"></script> -->

  @include('layouts.myApp_links')
</head>

<body>
  <div id="app">
    @auth
    <!-- navbar -->
    <nav class="navbar navbar-expand-md navbar navbar-dark bg-aqua  navbar-laravel navbar-static-top">

     <div class="">
       <span style="font-size:25px;cursor:pointer; color: white;" onclick="openNav()">&#9776;</span>
     </div>
     
     <div class="container">
      <a class="navbar-brand" href="{{url('dashboard')}}">
        {{ config('', 'PHREB ACCREDITATION') }}
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <!-- Left Side Of Navbar -->
        <ul class="navbar-nav mr-auto">

        </ul>

        <!-- Right Side Of Navbar -->
        <ul class="navbar-nav ml-auto">
          <!-- Authentication Links -->
          @guest
          <li class="nav-item">
            <!-- <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a> -->
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('register') }}">{{ __('Create Account') }}</a>
          </li>
          @else

        
          <li class="nav-item dropdown">
            <a class="nav-link" href="#" id="dLabel" role="button" data-toggle="dropdown" data-target="#"><span class="fa fa-bell" title="Notification"></span><i class="badge badge-info"><small>0</small></i></a>

            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">

              <div class="container"><h6 class="menu-title"> New Notifications</h6></div>
              <div class="">
                
              </div>
            </ul>
          </li>
   

          <li class="nav-item">
            <a class="nav-link" href="{{url('/accreditationMail')}}"><span class="fa fa-envelope-open" title="Mail"><i class="badge badge-info"><small>0</small></i></span></a>
          </li>
          
        
          <li class="nav-item dropdown">
            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
              {{ Auth::user()->name }} <span class="caret"></span>
            </a>

            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
              <a class="dropdown-item fa fa-sign-out" aria-hidden="true"
              data-toggle="modal" data-target=".bs-example-modal-sm-logout">
              {{ __('Logout') }}
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">

              @csrf
            
            </form>
          </div>
        </li>
        @endguest
      </ul>
    </div>
  </div>
</nav>
@endauth

<main class="py-4">
  @yield('content')


  <div class="page-footer">
    @include('layouts.myApp_footer')
  </div>

</main>

</div>


@auth
<!--   //logout modal --> 
<div class="modal bs-example-modal-sm-logout" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h6><span class="fa fa-exclamation-triangle"></span> Logout</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h6><i class=""></i></h6>
      </div>
      <div class="modal-body"><i class=""></i> Are you sure you want to log-out?</div>
      <div class="modal-footer"><a href="{{ route('logout') }}"  onclick="event.preventDefault();
      document.getElementById('logout-form').submit();" class="btn btn-primary btn-block">Ok</a></div>
    </div>
  </div>
</div>


<!-- left sidenav -->
<div id="mySidenav" class="sidenav navbar-dark">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <a class="fa fa-tachometer" href="{{url('/dashboard')}}"> Dashboard</a>

  @can('secretariat-access')
  <a class="fa fa-plus-square" href="{{url('/applyaccred')}}"> 
   
      Add Accredited RECs
 
  </a>
  @endcan

  <a class="fa fa-book" href="{{url('/show-recdetails')}}"> REC Management</a>
  
  @can('secretariat_and_admin')
    <a class="fa fa-user" href="{{url('/user-management')}}"> User Management</a>
    <a class="fa fa-money" href="{{url('/orderofpayment')}}"> OR Management</a>
    <a href="{{url('/email-management')}}" class="fa fa-envelope"> Email Management</a>

    
    <a class="fa fa-pencil" href="{{url('/show-awarding-letters')}}"> Awarding Letters</a>
    <!-- <a class="fa fa-exchange" href="{{url('/emaillogs')}}"> Email Logs</a>
    <a class="fa fa-share" href="{{url('/history-submission')}}"> Submission Logs</a>
    <a class="fa fa-envelope" href="{{url('/chathistory')}}-"> Message Logs</a> -->
    <a class="fa fa-calendar-check-o" href="{{url('/annual-report')}}"> Annual Reports</a>
    <a class="fa fa-list-ul" href="{{url('/show-report')}}"> Reporting</a>
    <a class="fa fa-users" href="{{url('/accreditors')}}"> Accreditors</a>

    <a class="fa fa-calendar" href="{{url('/show-training-mngt')}}"> Attendances</a>
    <a class="fa fa-users" href="{{url('/show-trainees-list')}}"> Trainees Lists</a>
    <a class="fa fa-building" href="{{url('/show-events-mngt')}}"> Events Title</a>
  @endcan
  <a class="fa fa-sign-out" href="" data-toggle="modal" data-target=".bs-example-modal-sm-logout"> Logout</a>

</div>

  
<style type="text/css">
  .modal-full {
    min-width: 100%;
    margin: 0;
  }
  .modal-full .modal-content {
    min-height: 100vh;
  }


.track_tbl td.track_dot {
    width: 50px;
    position: relative;
    padding: 0;
    text-align: center;
}
.track_tbl td.track_dot:after {
    content: "\f111";
    font-family: FontAwesome;
    position: absolute;
    margin-left: -5px;
    top: 11px;
}
.track_tbl td.track_dot span.track_line {
    background: #000;
    width: 3px;
    min-height: 50px;
    position: absolute;
    height: 101%;
}
.track_tbl tbody tr:first-child td.track_dot span.track_line {
    top: 30px;
    min-height: 25px;
}
.track_tbl tbody tr:last-child td.track_dot span.track_line {
    top: 0;
    min-height: 25px;
    height: 10%;
}

/*timeline*/
.main-timeline{
    font-family: 'Roboto Condensed', sans-serif;
    position: relative;
}
.main-timeline:after{
    content: '';
    display: block;
    clear: both;
}
.main-timeline:before{
    content: '';
    height: 100%;
    width: 7px;
    border-left: 7px dashed #999;
    transform: translateX(-50%);
    position: absolute;
    left: 50%;
    top: 0;
}
.main-timeline .timeline{
    width: 50%;
    padding: 0 0 0 40px;
    margin: 0 0 0 20px;
    float: right;
    position: relative;
}
.main-timeline .timeline:after{
    content: '';
    background-color: #6044F0;
    height: 35px;
    width: 40px;
    position: absolute;
    left: 0;
    top: 60px;
    clip-path: polygon(0 50%, 100% 0, 100% 100%);
}
.main-timeline .timeline-content{
    color: #555;
    text-align: left;
    padding: 35px 40px 35px;
    display: block;
    position: relative;
    z-index: 1;
}
.main-timeline .timeline-content:hover{ text-decoration: none; }
.main-timeline .timeline-content:before,
.main-timeline .timeline-content:after{
    content: '';
    background-color: #fff;
    border-radius: 50px 0;
    box-shadow: 0 0 10px -3px rgba(0,0,0,0.5);
    position: absolute;
    left: 10px;
    bottom: 10px;
    right: 0;
    top: 0;
    z-index: -1;
}
.main-timeline .timeline-content:after{
    background-color: transparent;
    box-shadow: none;
    border: 2px solid #6044F0;
    left: 0;
    bottom: 0;
    right: 10px;
    top: 10px;
}
.main-timeline .timeline-icon{
    color: #fff;
    background-color: #000;
    font-size: 25px;
    text-align: center;
    line-height: 50px;
    height: 50px;
    width: 50px;
    border-radius: 50%;
    position: absolute;
    left: 0;
    top: 0;
    z-index: 2;
}
.main-timeline .title{
    color: #6044F0;
    font-size: 20px;
    font-weight: 700;
    text-transform: uppercase;
    letter-spacing: 1px;
    margin: 0 0 5px;
}
.main-timeline .description{
    font-size: 15px;
    font-weight: 500;
    letter-spacing: 1px;
    margin: 0 0 0 10px;
}
.main-timeline .timeline:nth-child(even){
    float: left;
    padding: 0 40px 0 0;
    margin: 0 20px 0 0;
}
.main-timeline .timeline:nth-child(even):after{
    transform: rotateY(180deg);
    left: auto;
    right: 0;
}
.main-timeline .timeline:nth-child(even) .timeline-content:before,
.main-timeline .timeline:nth-child(even) .timeline-content:after{
    border-radius: 0 50px;
}
.main-timeline .timeline:nth-child(even) .timeline-icon{
    left: auto;
    right: 0;
}
.main-timeline .timeline:nth-child(4n+2):after{ background-color: #FF3754; }
.main-timeline .timeline:nth-child(4n+2) .timeline-content:after{ border-color: #FF3754; }
.main-timeline .timeline:nth-child(4n+2) .title{ color: #FF3754; }
.main-timeline .timeline:nth-child(4n+3):after{ background-color: #01C1E1; }
.main-timeline .timeline:nth-child(4n+3) .timeline-content:after{ border-color: #01C1E1; }
.main-timeline .timeline:nth-child(4n+3) .title{ color: #01C1E1; }
.main-timeline .timeline:nth-child(4n+4):after{ background-color: #10B175; }
.main-timeline .timeline:nth-child(4n+4) .timeline-content:after{ border-color: #10B175; }
.main-timeline .timeline:nth-child(4n+4) .title{ color: #10B175; }
@media screen and (max-width:767px){
    .main-timeline:before{ display: none; }
    .main-timeline .timeline,
    .main-timeline .timeline:nth-child(even){
        width: 100%;
        padding: 0 0 35px 0;
        margin: 0;
    }
    .main-timeline .timeline:last-child{ padding: 0; }
    .main-timeline .timeline:after,
    .main-timeline .timeline:nth-child(even):after{
        transform: translateX(-50%) rotate(-90deg);
        left: 50%;
        top: auto;
        bottom: 0;
    }
    .main-timeline .timeline:last-child:after{ display: none; }
}
@media screen and (max-width:576px){
    .main-timeline .title{ font-size: 18px; }
}


.h-divider{
 margin-top:5px;
 margin-bottom:5px;
 height:2px;
 width:100%;
 border-top:1px solid gray;


</style>

@endauth

</body>
</html>