   <!-- Scripts -->
   <script src="{{ asset('js/app.js') }}" defer></script>
   <script src="{{ asset('js/myApp.js') }}" defer></script>
   <script src="//www.google.com/recaptcha/api.js"></script>
   
   <!-- Fonts -->
   <link rel="dns-prefetch" href="https://fonts.gstatic.com">
   <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
   <!-- Styles -->
   <link href="{{ asset('css/app.css') }}" rel="stylesheet">


   @auth
   <!-- bootstrap links for css -->
   <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">

   <!-- font-awesome css -->
   <link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet" type="text/css" />
   <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
   <!-- mycustom navbar css -->
  
   <!-- adminLTE CSS link --> 
   <link href="{{ asset('css/AdminLTE.css') }}" rel="stylesheet" type="text/css" />
<!--    <link rel="stylesheet" href="{{ asset('css/AdminLTE.min.css') }}"> -->
   
   <!-- Comments Section link -->
   <link href="{{ asset('css/customcommentssection.css') }}" rel="stylesheet" type="text/css" />

   <link rel="stylesheet" href="https://unpkg.com/bs-stepper/dist/css/bs-stepper.min.css">
   <script src="https://unpkg.com/bs-stepper/dist/js/bs-stepper.min.js"></script>


   @endauth

    <link href="{{ asset('css/mycustom_sidenav.css') }}" rel="stylesheet" type="text/css" />
    
   <!-- select2 -->
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
   <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js" defer></script>
   
   <!-- datepicker -->
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css">
   <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js" defer></script>

   <link rel="stylesheet" href="{{ asset('css/dataTables.min.css') }}">
   <!-- CDN Online link for DATATABLES SEMANTIC UI -->
   <!-- <script src="https://code.jquery.com/jquery-3.3.1.js"></script> -->

   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.css">
   <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.semanticui.min.css">
   <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.semanticui.min.css">

   <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" defer></script>
   <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js" defer></script>
   <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js" defer></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" defer></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js" defer></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js" defer></script>
   <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js" defer></script>
   <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js" defer></script>

   <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js" defer></script>
   <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.semanticui.min.js" defer></script>
   <script src="https://cdn.datatables.net/1.10.19/js/dataTables.semanticui.min.js" defer></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.js" defer></script>
   <script src="//cdn.datatables.net/plug-ins/1.10.19/filtering/row-based/range_dates.js" defer></script>

   <!-- moement date for ajax-datatable -->
   <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js" defer></script>
   <script src="///cdn.datatables.net/plug-ins/1.10.19/sorting/datetime-moment.js" defer></script>

   <!-- End of CDN -->
   <!-- <script src="{{ asset('js/bootstrap.min.js') }}" defer></script> -->
   <!-- <script src="https://cdn.ckeditor.com/4.7.1/standard/ckeditor.js"></script> -->

   <!--   <link rel="stylesheet" type="text/css" href=" https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script> -->