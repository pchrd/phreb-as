<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/1CT_ethiCS@34/down', function(){
	\Artisan::call('down');
});

// try muna ito, wala lang ito //
Route::get('xxx', 'HomeController@xxx'); 

//landing page//************************************************
Route::get('/', function(){
   
   $reporting = App\Models\Reporting::latest()->first();
   $regions = App\Models\Applicationlevel::select('region_name',DB::raw('count(region_name) as regionTotal'))->whereBetween('date_accreditation',[$reporting->report_start_date, $reporting->report_end_date])->where('statuses_id', 4)->join('regions', 'regions.id', 'region_id')->groupBy('region_name')->get();

   $levels = App\Models\Applicationlevel::join('levels', 'levels.id', 'level_id')->join('regions', 'regions.id', 'region_id')->whereBetween('date_accreditation',[$reporting->report_start_date, $reporting->report_end_date])->where('statuses_id', 4)->select('level_id',DB::raw('COUNT(*) as levelTotal, levels.level_name'), DB::raw("COUNT('regions.region_name') as regionTotal, regions.region_name, GROUP_CONCAT(DISTINCT regions.region_name ORDER BY regions.id desc SEPARATOR '- ') as region"))->groupBy('level_id','level_name', 'regions.region_name')->get();

  $count = DB::table('applicationlevel')->whereBetween('date_accreditation',[$reporting->report_start_date, $reporting->report_end_date])->join('levels', 'levels.id', 'level_id')->where('statuses_id', 4)->select(DB::raw("COUNT(*) as counts, level_name"))->groupBy('level_name')->get();

  $pdfreport = App\Models\Pdfgeneratereport::orderBy('id', 'desc')->paginate(5);
  
   return view('welcome', compact('reporting', 'regions', 'levels', 'count', 'pdfreport'));
})->middleware('guest');

Route::get('view/{id}', function($id){
	$decrypted_id = base64_decode($id);
	$recdetails = App\Models\Recdetails::find(decrypt($decrypted_id));
	return view('reporting.landingpageview', compact('recdetails'));
})->middleware('can:secretariat-access');

Auth::routes();

// ******************************DashboardController*************************************//
Route::get('/dashboard', 'DashboardController@count_all')->middleware('auth');
//reminder emails such as arps, deadline of acc// CRON JOB 
Route::get('/emailreminder_cron', 'DashboardController@emailreminder_cron');
Route::get('/deadlinelofaccreditation/4023', 'DashboardController@dl_acc_cron');

// ******************************RecdetailsController*************************************//

	//to know the logged role-user
Route::get('/rec-management', 'RecdetailsController@dashboard')->middleware('auth');

	// to get or fill the data in level table & display in selectbox
Route::get('/applyaccred', 'RecdetailsController@selectlevelandregions')->middleware('auth');
Route::get('/applyaccrednotice', 'RecdetailsController@applyaccrednotice')->middleware('auth');

	//route to show recdetails in tables
Route::get('show-recdetails', 'RecdetailsController@show_rec_details')->name('rec-management')->middleware('auth');

	//route to get ajax show rec-details
Route::get('recdetails/show-recdetails', 'RecdetailsController@show_rec_details_ajax')->name('recdetails/show-recdetails')->middleware('auth');

	//route for adding recdetails
Route::post('/applyaccred', 'RecdetailsController@add_rec_details')->name('add_rec_details')->middleware('auth');

    //route to view reddetails in form from table
Route::get('/view-recdetails/{recdetail}', 'RecdetailsController@view_rec_details')->name('view-recdetails')->middleware('auth');

	//route to update recdetails in form from table
Route::post('/update-recdetails/{recdetail}', 'RecdetailsController@update_rec_details')->name('update-recdetails')->middleware('can:rec-access');
	
	//route for submitting final application to PHREB
Route::get('/submit_recdetails/{recdetail}', 'RecdetailsController@submittophreb')->name('submit_recdetails')->middleware('can:rec-access');

	//route to update REC status and date completed
Route::post('/update_rec_status/{recdetail}', 'RecdetailsController@update_rec_status')->name('update_rec_status')->middleware('can:secretariat-access');

//sendOR to rec's mail
Route::get('sendOR/{recdetail}', 'RecdetailsController@sendOR')->name('sendOR')->middleware('can:secretariat-access');

//generate of REC/Accounting/Cashier
Route::get('generateOR/{recdetail}', 'RecdetailsController@generateOR')->name('generateOR')->middleware('can:secretariat-access');

//create awarding
Route::get('awarding/{recdetail}', 'RecdetailsController@awarding')->name('awarding')->middleware('can:secretariat-access');

//add acc_no
Route::post('acc_no/{id}', 'RecdetailsController@acc_no')->name('acc_no')->middleware('can:secretariat-access');

//sendaccreditationexpiry reminders 
Route::get('sendaccreditationexpiry_reminder/{id}', 'RecdetailsController@sendaccreditationexpiry_reminder')->name('sendaccreditationexpiry_reminder')->middleware('can:secretariat-access');

Route::post('changeofacc/{id}', 'RecdetailsController@changeofacc')->name('changeofacc')->middleware('can:secretariat-access');

//withdraw application
Route::get('withdraw_application/{id}', 'RecdetailsController@withdraw')->name('withdraw_application')->middleware('can:rec-access');

// ******************************SendtoaccreditorController*************************************//

//send to accreditor
Route::post('/sendtoaccreditor', 'SendtoaccreditorController@sendtoaccreditor')->name('sendtoaccreditor')->middleware('can:secretariat-access');

//remove accreditor
Route::post('removeacc/{sendacc_tbl_id}', 'SendtoaccreditorController@removeacc')->name('removeacc')->middleware('can:secretariat-access');

//send assessment form
Route::post('/sendassessmentform', 'SendtoaccreditorController@sendassessmentform')->name('sendassessmentform')->middleware('can:accreditors_and_csachair');

//send assessment form to csachair
Route::post('/sendtocsachair', 'SendtoaccreditorController@sendtocsachair')->name('sendtocsachair')->middleware('can:accreditors_and_secretariat');

//send assessment to rec
Route::post('/sendtorec', 'SendtoaccreditorController@sendtorec')->name('sendtorec')->middleware('can:accreditors_and_secretariat');

//confirm decline
Route::get('/accreditors_confirmation/{id}', 'SendtoaccreditorController@confirm')->middleware('can:accreditors_and_secretariat');
Route::get('/accreditors_decline/{id}', 'SendtoaccreditorController@decline')->middleware('can:accreditors_and_secretariat');

// ******************************RegisterController*************************************//
	//route for user verification
Route::get('/verifyemail/{token}', 'Auth\RegisterController@verify')->middleware('guest');
	
// ******************************RecdocumentsController*************************************//

	//route to view documents per recetails based on id
Route::get('/documents/{recdetail}', ['as' => 'documents', 'uses' => 'RecdocumentsController@documents'])
->middleware('auth');

	//route to store the file or docs of phreb forms
Route::post('documents/{recdetail}', 'RecdocumentsController@storerecdocuments')
->name('myfunctions.documents')->middleware('can:rec_and_secretariat');
	
	// route in additional files
Route::post('addfiles/{recdetail}/{recdocID}', 'RecdocumentsController@adddocuments')->name('addtionalfiles')->middleware('can:rec_and_secretariat');

	//route in viewing recdocuments per row
Route::get('documents/view-recdocuments/{id}/{recdocuments_document_types_id}', ['as' => 'documents/view-recdocuments', 'uses' => 'RecdocumentsController@viewrecdocuments'])->middleware('auth');

	//route in checking completed doc types
Route::post('/check-complete/{recdetail}', 'RecdocumentsController@check_complete')->name('check-complete')->middleware('can:secretariat-access');

	//remove recdocs if not yet submitted
Route::get('removerecdocument/{id}', 'RecdocumentsController@removerecdocument')->name('removerecdocument')->middleware('can:rec_and_secretariat');

//route for resubmit req
Route::get('/resubmit-req/{recdetail}', 'RecdocumentsController@resubmit_req')->name('resubmit-req')->middleware('can:rec-access');

Route::post('create-folder/{id}', 'RecdocumentsController@createfolder')->name('create-folder')->middleware('auth');

//downloadZip
Route::get('downloadZipRecDoc', 'RecdocumentsController@downloadZipRecDoc')->name('downloadZipRecDoc')->middleware('auth');

//step 3 for rec
Route::get('step3finalverification/{id}', 'RecdocumentsController@step3')->name('step3finalverification')->middleware('can:rec-access');

//download documents using route per file
Route::get('viewFile/{id}', 'RecdocumentsController@downloadfile')->name('downloadfile')->middleware('auth');

//download documents of emailattachmment using route per file
Route::get('emailAttachment/{id}', 'RecdocumentsController@emailattachmment')->name('emailattachmment')->middleware('auth');

// renamefoldee
Route::get('renamefolder/{id}', 'RecdocumentsController@renamefolder')->name('renamefolder')->middleware('can:rec-access');

// ******************************RecdocumentsremarksController*************************************//
	//route for secretariat send remarks per docs type
Route::post('remarks_documents', 'RecdocumentsremarkController@add_remarks')->name('myfunctions.remarks_documents')->middleware('can:accreditors_and_csachair_secretariat');



// ******************************UsermanagementController*************************************//
	//show all users in table
Route::get('/user-management', 'UsermanagementController@show_users')->name('user-management')->middleware('can:secretariat_and_admin');

	//create or add user account
Route::post('/user-management', 'UsermanagementController@create_account')->name('user-management')->middleware('can:secretariat_and_admin');

	//view per user's record
Route::get('/user-management-view/{user_id}', 'UsermanagementController@view_users')->name('user-management-view')->middleware('can:secretariat_and_admin');

	//update user's record
Route::post('/user-management-view-update/{user_id}', 'UsermanagementController@update_users')->name('user-management-view-update')->middleware('can:secretariat_and_admin');

	//softdelete user's record
Route::get('/user-management-delete/{user_id}', 'UsermanagementController@delete_users')->name('user-management-delete')->middleware('can:only-admin-can-access-page');

	//upload esig
Route::post('uploadesig', 'UsermanagementController@uploadesig')->name('uploadesig')->middleware('can:only-admin-can-access-page');
	
	//remove esig
Route::get('removeesig/{user_id}', 'UsermanagementController@removeesig')->name('removeesig')->middleware('can:only-admin-can-access-page');
	
	//verify email address usermanagement view
Route::get('verifyemailaccount/{user_id}', 'UsermanagementController@verifyemailaccount')->name('verifyemailaccount')->middleware('can:secretariat_and_admin');

// ******************************ChatmessagesController*************************************//
Route::get('/accreditationMail', 'ChatmessagesController@chatmessages')->middleware('can:rec_secretariat_accreditors_csachair');
Route::get('/accreditationMail/ajax', 'ChatmessagesController@chatmessages_ajax')->name('accreditationMail/ajaxMail')->middleware('can:rec_secretariat_accreditors_csachair');
Route::get('/accreditationMail/ajax/sent', 'ChatmessagesController@chatmessages_ajax_sent')->name('accreditationMail/ajaxMail/sent')->middleware('can:rec_secretariat_accreditors_csachair');
Route::get('/view-mail-messages/', 'ChatmessagesController@viewMail')->name('view-mail')->middleware('can:rec_secretariat_accreditors_csachair');
Route::get('/download-mail-file/{id}', 'ChatmessagesController@downloadFile')->name('downloadFile')->middleware('can:rec_secretariat_accreditors_csachair');

Route::post('sendmessage', 'ChatmessagesController@sendmessage')->name('sendmessage')->middleware('can:rec_secretariat_accreditors_csachair');
Route::get('viewMail-seen', 'ChatmessagesController@viewmail_seen')->name('viewnmail_seen')->middleware('can:rec_secretariat_accreditors_csachair');

Route::get('seen/{chat_id}', 'ChatmessagesController@seen_status')->name('seen')->middleware('auth');

Route::get('chathistory', 'ChatmessagesController@showhistory')->middleware('can:only-admin-can-access-page');
Route::get('chat/chathistory', 'ChatmessagesController@showhistory_ajax')->name('chat/chathistory')->middleware('auth');

// ******************************historyController&EmaillogsController *****************************//
Route::get('/history-submission', 'SubmissionhistoryController@sub_history')->middleware('can:only-admin-can-access-page');

Route::get('assessment-history/{assname}/{filename}/{id}', 'SubmissionhistoryController@assessmenthistory')->name('assessment-history')->middleware('can:secretariat-access');
Route::get('apce-history/{sender}/{filename}/{id}', 'SubmissionhistoryController@actionplanhistory')->name('apce-history')->middleware('can:secretariat-access');

Route::get('/emaillogs', 'EmaillogsController@showemaillogs')->middleware('can:only-admin-can-access-page');
Route::get('chat/emaillogs', 'EmaillogsController@showemaillogs_ajax')->name('email/emaillogs')->middleware('can:only-admin-can-access-page');
Route::get('email-logs-show/{id}', 'EmaillogsController@viewlogs')->name('email-logs-show')->middleware('can:only-admin-can-access-page');

// ******************************filterController*************************************//
Route::get('/filter', 'filterController@searchfilter')->name('filter')->middleware('auth');
Route::get('/pending', 'filterController@searchpending')->middleware('can:secretariat-access');
Route::get('/filtersearch', 'filterController@filtersearch')->name('filtersearch')->middleware('auth');
Route::get('/filter-iframe/{id}', 'filterController@filteriframe')->middleware('guest');
// Route::get('/filtersearch_ajax', 'filterController@filtersearch_ajax')->name('recdetails/filtersearch_ajax')->middleware('auth');

// ******************************actionplanController*************************************//
Route::get('actionplan/{id}', 'actionplanController@index')->name('actionplan')->middleware('auth');
Route::post('actionplan_add', 'actionplanController@store')->name('actionplan_add')->middleware('can:rec-access');
Route::post('ap_check/{ap_recid}', 'actionplanController@apcheck')->name('ap_check')->middleware('can:secretariat-access');
Route::post('ap_sendtocsachair', 'actionplanController@ap_sendtocsachair')->name('ap_sendtocsachair')->middleware('can:secretariat-access');
Route::post('ap_sendtorec', 'actionplanController@ap_sendtorec')->name('ap_sendtorec')->middleware('can:secretariat-access');
Route::post('sendactionplan', 'actionplanController@sendactionplan')->name('sendactionplan')->middleware('can:accreditors_and_csachair_secretariat');

// ******************************OrderpaymentController*************************************//
Route::get('/orderofpayment', 'OrderpaymentController@show')->name('orderofpayment')->middleware('can:secretariat-access');
Route::get('orderpayment/or', 'OrderpaymentController@show_ajax')->name('orderpayment/or')->middleware('can:secretariat-access');
Route::get('orderpayment/or_rec', 'OrderpaymentController@show_ajax_or_rec')->name('orderpayment/or_rec')->middleware('can:secretariat-access');
Route::post('sendorreceipt/{recdetails_id}', 'OrderpaymentController@sendorreceipt')->name('sendorreceipt')->middleware('can:secretariat-access');

// ******************************AwardingletterController*************************************//
Route::get('show-awarding-letters', 'AwardingletterController@show_a_letter')->middleware('can:secretariat-access');
Route::get('ajax-show-a-letters', 'AwardingletterController@show_a_letter_ajax')->name('ajax-show-a-letters')->middleware('can:secretariat-access');
Route::post('store_awarding/{recdetail}/{doctypes}', 'AwardingletterController@store_a_letter')->name('store_awarding')->middleware('can:secretariat-access');
Route::get('awarding_view/{id}/{name}', 'AwardingletterController@view_a_letter')->name('awarding_view')->middleware('can:secretariat-access');
Route::post('awarding_save/{id}', 'AwardingletterController@update_a_letter')->name('awarding_save')->middleware('can:secretariat-access');
Route::get('delete/{id}', 'AwardingletterController@delete_a_letter')->middleware('can:secretariat-access');
Route::get('a_sendpdf/{id}', 'AwardingletterController@pdf_a_letter')->name('a_sendpdf')->middleware('can:secretariat-access');
Route::get('a_downloadpdf/{id}', 'AwardingletterController@a_downloadpdf')->name('a_downloadpdf')->middleware('can:secretariat-access');

// ******************************ReportingController*************************************//
Route::get('show-report', 'ReportingController@showreport')->name('show-report')->middleware('can:secretariat-access');
Route::get('show-report-ajax', 'ReportingController@showreport_ajax')->name('show-report-ajax');
Route::post('insert_report', 'ReportingController@insert_report')->name('insert_report')->middleware('can:secretariat-access');
Route::get('viewreportinglevel/{id}', 'ReportingController@viewlevel')->name('viewlevel')->middleware('guest');
Route::get('view-report/{id}', 'ReportingController@showpdfreport')->name('showpdfreport')->middleware('guest');

// ******************************AnnualReportController*************************************//
Route::get('annual-report', 'AnnualReportController@showannualreport')->name('annual-report')->middleware('can:secretariat-access');
Route::get('showannualreport_ajax', 'AnnualReportController@showannualreport_ajax')->name('showannualreport_ajax')->middleware('can:secretariat-access');

// ******************************AnnualReportTitleController*************************************//
Route::post('add_ar', 'AnnualReportTitleController@add_ar')->name('add_ar')->middleware('can:secretariat-access');
Route::get('art_delete/{id}', 'AnnualReportTitleController@art_delete')->name('art_delete')->middleware('can:secretariat-access');

// ******************************ExistingRecsController*************************************//
Route::get('edit-record/{id}', 'ExistingRecsController@showrecords')->name('edit-record')->middleware('can:secretariat-access');
Route::post('update-record/{id}', 'ExistingRecsController@updaterecords')->name('update-record')->middleware('can:secretariat-access');

// ******************************ExistingArpsController*************************************//
Route::post('add_arps/{id}', 'ExistingArpsController@add_arps')->name('add_arps')->middleware('can:secretariat-access');
Route::get('delete_arps/{id}', 'ExistingArpsController@delete_arps')->name('delete_arps')->middleware('can:secretariat-access');

// ******************************AccreditorslistController*************************************//
Route::get('accreditors', 'AccreditorslistController@show')->middleware('can:secretariat_and_admin');
Route::get('accreditors_ajax', 'AccreditorslistController@show_ajax')->name('accreditors/show_ajax')->middleware('can:secretariat_and_admin');
Route::post('addaccreditors', 'AccreditorslistController@add_acc')->name('addaccreditors')->middleware('can:secretariat_and_admin');
Route::get('view-accreditor/{id}', 'AccreditorslistController@view_acc')->name('view-accreditor')->middleware('can:secretariat_and_admin');
Route::post('update_acc/{id}', 'AccreditorslistController@update_acc')->name('update_acc')->middleware('can:secretariat_and_admin');

//criteria library module
Route::get('/accreditors-criteria', 'AccreditorslistController@gotoacccriteria')->middleware('can:secretariat_and_admin');
Route::post('/addcriteria', 'AccreditorslistController@addcriteria')->name('addcriteria')->middleware('can:secretariat_and_admin');
Route::post('update_criteria/{id}', 'AccreditorslistController@update_criteria')->name('update_criteria')->middleware('can:secretariat_and_admin');
Route::get('delete_accreditor/{id}', 'AccreditorslistController@delete_acc')->name('delete_acc')->middleware('can:secretariat-access');

//******************************AccreditorsEvaluationController*************************************//
Route::get('create-evaluation/{id}', 'AccreditorsEvaluationController@evaluate_accreditor')->name('create-eval')->middleware('can:secretariat_and_admin');
Route::post('submit-evaluation/{accreditor_id}', 'AccreditorsEvaluationController@submitevaluation')->name('submiteval')->middleware('can:secretariat_and_admin');
Route::get('/list-of-evaluation', 'AccreditorsEvaluationController@listofevaluation')->middleware('can:secretariat_and_admin');
Route::get('accreditorsevaluation_ajax', 'AccreditorsEvaluationController@show_ajax')->name('accreditorsevaluation/show_ajax')->middleware('can:secretariat_and_admin');
Route::get('view-accreditors-evaluation/{id}', 'AccreditorsEvaluationController@viewacceval')->name('view-accreditors-evaluation')->middleware('can:secretariat_and_admin');
Route::post('update-evaluations/{id}', 'AccreditorsEvaluationController@update_eval')->name('update-evaluations')->middleware('can:secretariat_and_admin');
Route::get('download-evaluations/{id}', 'AccreditorsEvaluationController@download_eval')->name('download-evaluations')->middleware('can:secretariat_and_admin');

//******************************PhrebformsController*************************************//
Route::post('update-phrebforms/{id}', 'PhrebformsController@update')->name('update-phrebforms')->middleware('auth');
Route::get('view-phreb-forms/{id}/{rec_id}', 'PhrebformsController@view')->name('view-phreb-forms')->middleware('auth');
Route::get('downloadpf/{id}', 'PhrebformsController@downloadpf')->name('downloadpf')->middleware('can:secretariat-access');
Route::post('updatepfcomments/{id}', 'PhrebformsController@updatepfcomments')->name('updatepfcomments')->middleware('can:secretariat-access');

Route::get('create-assessment-form/{id}', 'PhrebformsController@createassessment')->name('create-assessment-form')->middleware('can:accreditors_and_secretariat');
Route::get('view-assessment-form/{id}', 'PhrebformsController@viewassessment')->name('view-assessment-form')->middleware('can:rec_secretariat_accreditors_csachair');
Route::post('update-assessment-form/{id}', 'PhrebformsController@updateassessment')->name('update-assessment-form')->middleware('can:accreditors_and_csachair_secretariat');

Route::post('acc_assessment_submittocsachair/{id}', 'PhrebformsController@acc_ass_submittocsachair')->name('acc_assessment_submittocsachair')->middleware('can:accreditors_and_secretariat');
Route::get('phrebforms-submission', 'PhrebformsController@show_phrebformsubmission')->name('phrebforms-submissions')->middleware('can:accreditors_and_csachair_secretariat');
Route::get('phrebforms-submission_ajax', 'PhrebformsController@show_phrebformsubmission_ajax')->name('phrebforms-submission-ajax')->middleware('can:accreditors_and_csachair_secretariat');

Route::get('create_a_letter', 'PhrebformsController@create_a_letter')->name('create_a_letter')->middleware('can:secretariat-access');
Route::post('save_a_letter', 'PhrebformsController@save_a_letter')->name('save_a_letter')->middleware('can:secretariat-access');
Route::get('view_a_letter/{id}', 'PhrebformsController@view_a_letter')->name('view_a_letter')->middleware('can:csachair_and_secretariat');
Route::post('update_a_letter/{id}', 'PhrebformsController@update_a_letter')->name('update_a_letter')->middleware('can:csachair_and_secretariat');
Route::post('sendtorec_a_letter/{id}', 'PhrebformsController@sendtorec_a_letter')->name('sendtorec_a_letter')->middleware('can:secretariat-access');

Route::get('sendassessmenttorec/{pfID}/{recID}', 'PhrebformsController@sendassessmenttorec')->name('sendassessmenttorec')->middleware('can:secretariat-access');

Route::post('assessmentactions/{pfID}/{recID}', 'PhrebformsController@assessmentactions')->name('assessmentactions')->middleware('can:accreditors_and_csachair_secretariat');

Route::get('markasdone/{id}', 'PhrebformsController@markasdone')->name('markasdone')->middleware('can:accreditors_and_csachair_secretariat');

// phrebform remarks
Route::post('phrebformRemarks/{id}', 'PhrebformsController@phrebformRemarks')->name('phrebformRemarks')->middleware('can:accreditors_and_csachair_secretariat');
Route::post('updatephrebformRemarks/{id}', 'PhrebformsController@updatephrebformRemarks')->name('updatephrebformRemarks')->middleware('can:accreditors_and_csachair_secretariat');
Route::get('viewPhrebformRemarks/{id}', 'PhrebformsController@viewPhrebformRemarks')->name('viewPhrebformRemarks')->middleware('can:rec_secretariat_accreditors_csachair');

//******************************PhrebformActionplanController*************************************//
Route::get('makeActionplan/{id}', 'PhrebformActionplanController@makeActionplan')->name('makeActionplan')->middleware('can:rec-access');
Route::get('viewActionplan/{id}', 'PhrebformActionplanController@viewActionplan')->name('viewActionplan')->middleware('can:rec_secretariat_accreditors_csachair');
Route::post('updateActionplan/{id}', 'PhrebformActionplanController@updateActionplan')->name('updateActionplan')->middleware('can:rec_secretariat_accreditors_csachair');

// for files attached on action plan
Route::post('phrebformAddFiles/{id}', 'PhrebformActionplanController@Pf_addFiles')->name('phrebformAddFiles')->middleware('can:rec-access');
Route::post('updateFolder/{id}', 'PhrebformActionplanController@updateFolder')->name('updateFolder')->middleware('can:rec-access');
Route::get('deleteFolder/{id}', 'PhrebformActionplanController@deleteFolder')->name('deleteFolder')->middleware('can:rec-access');
Route::get('viewDocuments/{id}', 'PhrebformActionplanController@viewDocuments')->name('viewDocuments')->middleware('can:rec_secretariat_accreditors_csachair');

Route::get('ap_submittophreb/{id}', 'PhrebformActionplanController@ap_submittophreb')->name('ap_submittophreb')->middleware('can:rec-access');

//downloadAsZip
Route::get('downloadDocuments/{id}', 'PhrebformActionplanController@downloadDocuments')->name('downloadDocuments')->middleware('can:rec_secretariat_accreditors_csachair');
Route::get('downloadZip/{id}', 'PhrebformActionplanController@downloadZip')->name('downloadZip')->middleware('can:rec_secretariat_accreditors_csachair');

//actionbuttons
Route::post('phrebformActions/{id}', 'PhrebformActionplanController@phrebformActions')->name('phrebformActions')->middleware('can:rec_secretariat_accreditors_csachair');


//******************************OngoingController*************************************//
Route::get('status', 'OngoingController@show_ongoing')->name('status')->middleware('can:secretariat_and_admin');
Route::get('ongoing-view-ajax', 'OngoingController@show_ongoing_ajax')->name('recdetails/on-going-view')->middleware('can:secretariat_and_admin');

//******************************TrainingeventstraineesController*************************************//
Route::get('/show-trainees-list', 'TrainingeventstraineesController@show')->name('trainees/show_ajax')->middleware('can:secretariat-access');
Route::get('/show-trainees-list_ajax', 'TrainingeventstraineesController@show_ajax')->name('trainees/show_ajax')->middleware('can:secretariat-access');
Route::post('add-trainee', 'TrainingeventstraineesController@add')->name('add-trainee')->middleware('can:secretariat-access');
Route::get('view-trainees-list/{id}', 'TrainingeventstraineesController@view')->name('view-trainees-list')->middleware('can:secretariat-access');
Route::post('update-trainees-list/{id}', 'TrainingeventstraineesController@update')->name('update-trainees-list')->middleware('can:secretariat-access');
Route::post('add-affiliation/{id}', 'TrainingeventstraineesController@add_affiliation')->name('add-affiliation')->middleware('can:secretariat-access');
Route::post('update-affiliation/{id}', 'TrainingeventstraineesController@update_affiliation')->name('update-affiliation')->middleware('can:secretariat-access');

//******************************TrainingeventsorientationController*************************************//
Route::get('show-events-mngt', 'TrainingeventsorientationController@show')->name('show-events-mngt')->middleware('can:secretariat-access');
Route::post('add_trainingtitle', 'TrainingeventsorientationController@add')->name('add_trainingtitle')->middleware('can:secretariat-access');
Route::get('view-events-attendees/{id}', 'TrainingeventsorientationController@view_attendees')->name('view-events-attendees')->middleware('can:secretariat-access');
Route::post('update_trainingtitle/{id}', 'TrainingeventsorientationController@update')->name('update_trainingtitle')->middleware('can:secretariat-access');
Route::get('print-attendee/{id}', 'TrainingeventsorientationController@printattendee')->name('print-attendee')->middleware('can:secretariat-access');

//******************************TrainingeventsmanagementController*************************************//
Route::get('show-training-mngt', 'TrainingeventsmanagementController@show')->name('show-training-mngt')->middleware('can:secretariat-access');
Route::get('show-training-mngt_ajax', 'TrainingeventsmanagementController@show_ajax')->name('trainees/show_ajax/trainingmanagement')->middleware('can:secretariat-access');
Route::post('add-trainee-attendance', 'TrainingeventsmanagementController@add')->name('add-trainee-attendance')->middleware('can:secretariat-access');
Route::get('delete-trainee-attendance/{id}', 'TrainingeventsmanagementController@delete')->name('delete-trainee-attendance')->middleware('can:secretariat-access');