<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhrebformActionplanfromrecsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phrebform_actionplanfromrecs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('pfap_rec_pfid');
            $table->unsignedInteger('pfap_rec_recass_id');
            $table->unsignedInteger('pfap_rec_recdetails_id');
            $table->unsignedInteger('pfap_rec_sendby');
            $table->string('pfap_rec_status')->nullable();
            $table->timestamps();

            $table->foreign('pfap_rec_pfid')->references('id')->on('phrebforms')->onDelete('cascade');
            $table->foreign('pfap_rec_recass_id')->references('id')->on('phrebform_assessment_recs')->onDelete('cascade');
            $table->foreign('pfap_rec_recdetails_id')->references('id')->on('recdetails')->onDelete('cascade');
            $table->foreign('pfap_rec_sendby')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phrebform_actionplanfromrecs');
    }
}
