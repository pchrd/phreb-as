<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccreditorsEvaluationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accreditors_evaluations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('accreditors_id');
            $table->unsignedInteger('evaluatedby_user_id');
            $table->string('title_award');
            $table->string('for_rec')->nullable();
            $table->string('for_level')->nullable();
            $table->timestamps();

            $table->foreign('accreditors_id')->references('id')->on('accreditorslists')->onDelete('cascade');
            $table->foreign('evaluatedby_user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accreditors_evaluations');
    }
}
