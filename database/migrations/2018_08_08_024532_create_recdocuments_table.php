<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecdocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recdocuments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('recdocuments_user_id');
            $table->unsignedInteger('recdocuments_recdetails_id');
            $table->unsignedInteger('recdocuments_document_types_id');
            $table->unsignedInteger('recdocuments_document_types_id_check')->nullable();
            $table->dateTime('recdocuments_submittophreb')->nullable();
            $table->string('recdocuments_file');
            $table->string('recdocuments_file_generated');
            $table->string('recdocuments_fileextension')->nullable();
            $table->string('recdocuments_filesize')->nullable();
            $table->dateTime('rec_dateuploaded');
            
            $table->foreign('recdocuments_user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('recdocuments_recdetails_id')->references('id')->on('recdetails')->onDelete('cascade');
            $table->foreign('recdocuments_document_types_id')->references('id')->on('document_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recdocuments');
    }
}