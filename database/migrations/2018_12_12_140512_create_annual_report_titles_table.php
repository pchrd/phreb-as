<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnualReportTitlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('annual_report_titles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ar_name');
            $table->string('ar_desc')->nullable();
            $table->date('ar_start_year')->nullable();
            $table->date('ar_end_year')->nullable();
            $table->date('ar_sobrang_field')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('annual_report_titles');
    }
}
