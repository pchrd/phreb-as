<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAwardinglettersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('awardingletters', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('a_letter_recdetails_id');
            $table->unsignedInteger('a_letter_saveduser_id');
            $table->unsignedInteger('a_letter_doctypes');
            $table->unsignedInteger('a_letter_years')->nullable();
            $table->longText('a_letter_body')->nullable();
            $table->dateTime('a_letter_datesend')->nullable();
            $table->string('a_letter_status')->nullable();
            $table->timestamps();

            $table->foreign('a_letter_recdetails_id')->references('id')->on('recdetails')->onDelete('cascade');
            $table->foreign('a_letter_saveduser_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('a_letter_doctypes')->references('id')->on('document_types')->onDelete('cascade');

        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('awardingletters');
    }
}
