<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEsigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('esigs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('esig_user_id');
            $table->unsignedInteger('esig_addedby');
            $table->string('esig_file');
            $table->string('esig_file_generated');
            $table->dateTime('esig_datecreated_at');

            $table->foreign('esig_user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('esig_addedby')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('esigs');
    }
}
