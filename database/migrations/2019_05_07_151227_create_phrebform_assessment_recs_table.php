<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhrebformAssessmentRecsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phrebform_assessment_recs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('pfar_recdetails_id');
            $table->unsignedInteger('pfar_pf_id');
            $table->unsignedInteger('pfar_sender_id');
            $table->string('pfar_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phrebform_assessment_recs');
    }
}
