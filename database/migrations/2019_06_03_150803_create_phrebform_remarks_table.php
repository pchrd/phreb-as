<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhrebformRemarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phrebform_remarks', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('pf_remarks_pfid');
            $table->unsignedInteger('pf_remarks_recdetails_id');
            $table->unsignedInteger('pf_remarks_user_id');
            $table->longText('pf_remarks_body')->nullable();
            $table->string('pf_remarks_status')->nullable();
            $table->string('pf_remarks_seen')->nullable();
            $table->timestamps();

            $table->foreign('pf_remarks_pfid')->references('id')->on('phrebforms')->onDelete('cascade');
            $table->foreign('pf_remarks_recdetails_id')->references('id')->on('recdetails')->onDelete('cascade');
            $table->foreign('pf_remarks_user_id')->references('id')->on('users')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phrebform_remarks');
    }
}
