<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubmissionstatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('submissionstatuses', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('submissionstatuses_recdetails_id');
            $table->unsignedInteger('submissionstatuses_user_id');
            $table->unsignedInteger('submissionstatuses_statuses_id');
            $table->dateTime('submissionstatuses_datecreated');

            $table->foreign('submissionstatuses_recdetails_id')->references('id')->on('recdetails')->onDelete('cascade');
            $table->foreign('submissionstatuses_user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('submissionstatuses_statuses_id')->references('id')->on('statuses')->onDelete('cascade');
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('submissionstatuses');
    }
}
