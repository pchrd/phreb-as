<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhrebformFilefoldersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phrebform_filefolders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pfff_name');
            $table->unsignedInteger('pfff_recdetails_id');
            $table->unsignedInteger('pfff_pf_id');
            $table->unsignedInteger('pfff_user_id');
            $table->timestamps();

            $table->foreign('pfff_recdetails_id')->references('id')->on('recdetails')->onDelete('cascade');
            $table->foreign('pfff_pf_id')->references('id')->on('phrebforms')->onDelete('cascade');
            $table->foreign('pfff_user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phrebform_filefolders');
    }
}
