<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecactionplancsasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recactionplancsas', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('ap_csa_ap_id');
            $table->unsignedInteger('ap_csa_recdetails_id');
            $table->unsignedInteger('ap_csa_sender_id');
            $table->unsignedInteger('ap_csa_csachair_id')->nullable();
            $table->unsignedInteger('ap_csa_rec_id')->nullable();
            $table->dateTime('ap_csa_status')->nullable();
            $table->dateTime('ap_csa_datecreated_at');

            $table->foreign('ap_csa_ap_id')->references('id')->on('recactionplans')->onDelete('cascade');
            $table->foreign('ap_csa_recdetails_id')->references('id')->on('recdetails')->onDelete('cascade');
            $table->foreign('ap_csa_sender_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('ap_csa_csachair_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('ap_csa_rec_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recactionplancsas');
    }
}
