<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhrebformAttachedLettersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phrebform_attached_letters', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('pfal_recdetails_id');
            $table->unsignedInteger('pfal_pf_id');
            $table->unsignedInteger('pfal_pft_id');
            $table->unsignedInteger('pfal_users_id');
            $table->longText('pfal_body')->nullable();
            $table->string('pfal_status')->nullable();
            $table->timestamps();

            $table->foreign('pfal_recdetails_id')->references('id')->on('recdetails')->onDelete('cascade');
            $table->foreign('pfal_pf_id')->references('id')->on('phrebforms')->onDelete('cascade');
            $table->foreign('pfal_pft_id')->references('id')->on('phrebforms_templates')->onDelete('cascade');
            $table->foreign('pfal_users_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phrebform_attached_letters');
    }
}
