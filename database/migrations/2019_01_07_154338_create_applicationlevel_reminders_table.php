<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationlevelRemindersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applicationlevel_reminders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('recdetails_id')->nullable();
            $table->date('deadline_reminder_1st')->nullable();
            $table->date('deadline_reminder_2nd')->nullable();
            $table->date('deadline_reminder_3rd')->nullable();
            $table->string('status')->nullable();

            $table->foreign('recdetails_id')->references('id')->on('recdetails')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applicationlevel_reminders');
    }
}
