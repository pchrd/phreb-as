<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSendtoaccreditorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sendtoaccreditors', function (Blueprint $table) {
            $table->increments('sendacc_tbl_id');
            $table->unsignedInteger('a_recdetails_id');
            $table->unsignedInteger('a_sender_user_id');
            $table->unsignedInteger('a_accreditors_user_id');
            $table->bigInteger('sendaccreditors_status')->default(0);
            $table->bigInteger('sendaccreditors_confirm_decline')->nullable();
            $table->dateTime('a_datesubmitted');
            $table->date('a_dateofexpiry');
            $table->dateTime('a_reminder_status')->nullable();
            $table->dateTime('a_reminder_status_deadline')->nullable();
            $table->softDeletes();

            $table->foreign('a_recdetails_id')->references('id')->on('recdetails')->onDelete('cascade');
            $table->foreign('a_sender_user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('a_accreditors_user_id')->references('id')->on('users')->onDelete('cascade'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sendtoaccreditors');
    }
}
