<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSendOrderpaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('send_orderpayments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('or_sender_id');
            $table->unsignedInteger('or_recdetails_id');
            $table->string('or_serialno')->nullable();
            $table->string('or_file');
            $table->string('or_file_generated');
            $table->dateTime('or_datecreated_at');

            $table->foreign('or_sender_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('or_recdetails_id')->references('id')->on('recdetails')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('send_orderpayments');
    }
}
