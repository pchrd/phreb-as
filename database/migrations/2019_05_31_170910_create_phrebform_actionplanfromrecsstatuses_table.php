<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhrebformActionplanfromrecsstatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phrebform_actionplanfromrecsstatuses', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('pf_apfr_id');
            $table->unsignedInteger('pf_apfr_pfid');
            $table->unsignedInteger('pf_apfr_status_id');
            $table->unsignedInteger('pf_apfr_user_id');
            $table->timestamps();

            $table->foreign('pf_apfr_id')->references('id')->on('phrebform_actionplanfromrecs')->onDelete('cascade');
            $table->foreign('pf_apfr_pfid')->references('id')->on('phrebforms')->onDelete('cascade');
            $table->foreign('pf_apfr_status_id')->references('id')->on('statuses')->onDelete('cascade');
            $table->foreign('pf_apfr_user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phrebform_actionplanfromrecsstatuses');
    }
}
