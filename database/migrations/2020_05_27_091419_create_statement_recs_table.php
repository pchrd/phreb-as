<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatementRecsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statement_recs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('statement_recdetails_id');
            $table->unsignedInteger('statement_statement_id');
            $table->string('statement_extracolumn')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statement_recs');
    }
}
