<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatmessageRepliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chatmessage_replies', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('cmr_replytomessage_id');
            $table->unsignedInteger('cmr_message_id');
            // $table->unsignedInteger('cmr_messageTo_id');
            $table->string('cmr_messageStatus')->nullable();
            $table->timestamps();

            $table->foreign('cmr_message_id')->references('id')->on('chatmessages')->onDelete('cascade');
            //$table->foreign('cmr_replyto_id')->references('id')->on('chatmessages')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chatmessage_replies');
    }
}
