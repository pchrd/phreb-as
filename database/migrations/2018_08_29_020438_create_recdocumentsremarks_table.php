<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecdocumentsremarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recdocumentsremarks', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('remarks_user_id');
            $table->unsignedInteger('remarks_recdetails_id');
            $table->unsignedInteger('remarks_recdocuments_types_id');
            $table->longText('remarks_message_recdocuments');
            $table->timestamps();

            $table->foreign('remarks_user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('remarks_recdetails_id')->references('id')->on('recdetails')->onDelete('cascade');
            $table->foreign('remarks_recdocuments_types_id')->references('id')->on('document_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recdocumentsremarks');
    }
}
