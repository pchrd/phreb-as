<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecactionplansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recactionplans', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('ap_recdetails_id');
            $table->unsignedInteger('ap_sender_id');
            $table->unsignedInteger('ap_document_types');
            $table->string('ap_recactionplan_name');
            $table->string('ap_recactionplan_uniqid')->unique();
            $table->dateTime('ap_status')->nullable();
            $table->dateTime('ap_submittedto_rec')->nullable();
            $table->timestamps();
            
            $table->foreign('ap_recdetails_id')->references('id')->on('recdetails')->onDelete('cascade');
            $table->foreign('ap_sender_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('ap_document_types')->references('id')->on('document_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recactionplans');
    }
}