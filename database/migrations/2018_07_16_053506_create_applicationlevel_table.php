<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationlevelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applicationlevel', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('recdetails_id');
            $table->unsignedInteger('level_id');
            $table->unsignedInteger('region_id');
            $table->unsignedInteger('statuses_id')->default(true)->nullable();
            $table->bigInteger('recdetails_submission_status')->default(0);
            $table->bigInteger('recdetails_lock_fields')->default(0);
            $table->bigInteger('recdetails_resubmitreq')->default(0);
            $table->date('date_completed')->nullable();
            $table->date('date_accreditation')->nullable();
            $table->date('date_accreditation_expiry')->nullable();
            $table->longText('accreditation_number')->nullable();
            $table->integer('rec_apce')->default(0);
            $table->string('rec_accreditation')->nullable();
            $table->string('rec_change_of_acc')->nullable();
            $table->timestamps();

            $table->foreign('recdetails_id')->references('id')->on('recdetails')->onDelete('cascade');
            $table->foreign('level_id')->references('id')->on('levels')->onDelete('cascade');
            $table->foreign('statuses_id')->references('id')->on('statuses')->onDelete('cascade');
            $table->foreign('region_id')->references('id')->on('regions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applicationlevel');
    }
}