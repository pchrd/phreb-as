<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhrebformFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phrebform_files', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('pf_files_pfid');
            $table->unsignedInteger('pf_files_pfff_id');
            $table->unsignedInteger('pf_files_addedby');
            $table->string('pf_files_originalfilename');
            $table->string('pf_files_originalfileextension');
            $table->string('pf_files_originalfilesize');
            $table->string('pf_files_uniqid');
            $table->timestamps();

            $table->foreign('pf_files_pfid')->references('id')->on('phrebforms')->onDelete('cascade');
            $table->foreign('pf_files_pfff_id')->references('id')->on('phrebform_filefolders')->onDelete('cascade'); 
            $table->foreign('pf_files_addedby')->references('id')->on('users')->onDelete('cascade'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phrebform_files');
    }
}
