<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhrebformsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phrebforms', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('pf_recdetails_id');
            $table->unsignedInteger('pf_pfid');
            $table->unsignedInteger('pf_user_id')->nullable();
            $table->longText('pf_body1');
            $table->longText('pf_comments');
            $table->dateTime('pf_comments_date');
            $table->string('pf_status');
            $table->timestamps();

            $table->foreign('pf_recdetails_id')->references('id')->on('recdetails')->onDelete('cascade');
            $table->foreign('pf_pfid')->references('id')->on('document_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phrebforms');
    }
}
