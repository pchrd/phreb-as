<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingeventstraineesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainingeventstrainees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('trainees_lname');
            $table->string('trainees_fname');
            $table->string('trainees_mname')->nullable();
            
            $table->string('trainees_gender')->nullable();
            // $table->string('trainees_institution')->nullable();
            // $table->string('trainees_institution_address')->nullable();
            // $table->string('trainees_specialization')->nullable();
            // $table->string('trainees_email');
            // $table->string('trainees_contactno')->nullable();
            
            // $table->string('trainees_profbackground')->nullable();
            // $table->string('trainees_recrole')->nullable();
            // $table->string('trainees_committee')->nullable();
            $table->string('trainees_certificatename');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainingeventstrainees');
    }
}
