<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccreditorsEvalTablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accreditors_eval_tables', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('accreditors_evaluations_id');
            $table->unsignedInteger('criteria_id');
            $table->string('acc_rate');
            $table->string('field1')->nullable();
            $table->string('field2')->nullable();

            $table->foreign('accreditors_evaluations_id')->references('id')->on('accreditors_evaluation')->onDelete('cascade');
            $table->foreign('criteria_id')->references('id')->on('accreditors_evaluations_criterias')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accreditors_eval_tables');
    }
}
