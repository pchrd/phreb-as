<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccreditorslistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accreditorslists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('accreditors_name');
            $table->string('accreditors_status')->nullable();
            $table->string('accreditors_designation')->nullable();
            $table->string('accreditors_gender')->nullable();
            $table->string('accreditors_specialization')->nullable();
            $table->string('accreditors_office')->nullable();
            $table->string('accreditors_address')->nullable();
            $table->string('accreditors_contactnumber')->nullable();
            $table->string('accreditors_email')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accreditorslists');
    }
}
