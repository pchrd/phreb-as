<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecdocumentsEmailattachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recdocuments_emailattachments', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('re_submissionstatuses_id');
            $table->unsignedInteger('re_recdetails_id');
            $table->unsignedInteger('re_user_id');
            $table->string('re_file_name');
            $table->string('re_file_generated');
            $table->string('re_file_extension');
            $table->string('re_file_size');
            
            $table->string('re_extracol1')->nullable();
            $table->string('re_extracol2')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recdocuments_emailattachments');
    }
}
