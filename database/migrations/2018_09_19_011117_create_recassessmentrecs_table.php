<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecassessmentrecsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recassessmentrecs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('ra_rec_ra_id');
            $table->unsignedInteger('ra_rec_recdetails_id');
            $table->unsignedInteger('ra_rec_sender_id');
            $table->unsignedInteger('ra_rec_csachair_id');
            $table->dateTime('ra_rec_status')->nullable();
            $table->dateTime('ra_rec_datecreated_at');

            $table->date('ra_rec_ap_dateexpiry');
            $table->dateTime('ra_rec_ap_reminder_status')->nullable();
            $table->dateTime('ra_rec_ap_reminder_status_deadline')->nullable();

            $table->date('ra_rec_ce_dateexpiry');
            $table->dateTime('ra_rec_ce_reminder_status')->nullable();
            $table->dateTime('ra_rec_ce_reminder_status_deadline')->nullable();

            $table->foreign('ra_rec_ra_id')->references('id')->on('recassessments')->onDelete('cascade');
            $table->foreign('ra_rec_recdetails_id')->references('id')->on('recdetails')->onDelete('cascade');
            $table->foreign('ra_rec_sender_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('ra_rec_csachair_id')->references('id')->on('users')->onDelete('cascade');  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recassessmentrecs');
    }
}