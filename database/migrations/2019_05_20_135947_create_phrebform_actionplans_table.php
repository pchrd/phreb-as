<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhrebformActionplansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phrebform_actionplans', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('ap_recassessment_id');
            $table->unsignedInteger('ap_pf_id');
            $table->unsignedInteger('ap_pft_id');
            $table->unsignedInteger('ap_pfap_id');
            $table->longText('ap_body');
            $table->longtext('ap_remarks')->nullable();
            $table->unsignedInteger('ap_createdby');
            $table->string('ap_status')->nullable();
            $table->string('ap_extrafield1')->nullable();

            $table->foreign('ap_recassessment_id')->references('id')->on('phrebform_assessment_recs')->onDelete('cascade');
            $table->foreign('ap_createdby')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('ap_pf_id')->references('id')->on('phrebforms')->onDelete('cascade');
            $table->foreign('ap_pft_id')->references('id')->on('phrebforms_templates')->onDelete('cascade');
            $table->foreign('ap_pfap_id')->references('id')->on('phrebforms')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phrebform_actionplans');
    }
}
