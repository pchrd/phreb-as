<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExistingRecsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('existing_recs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('recdetails_id');

            $table->string('name_of_accreditors')->nullable();
            $table->string('date_req_deck_accreditors')->nullable();
            $table->string('date_rec_received_awarding_letter')->nullable();
            $table->string('date_deadline_accreditors_ass')->nullable();
            $table->string('date_accreditors_send_ass')->nullable();
            $table->string('date_accreditation_visit_finalreport')->nullable();
            $table->string('date_rec_recieved_acc_ass')->nullable();

            $table->string('date_rec_sent_apce')->nullable();
            $table->string('date_accreditors_sent_apce')->nullable();
            $table->string('date_forward_to_csachair')->nullable();

            $table->longText('existingrec_remarks')->nullable();

            $table->foreign('recdetails_id')->references('id')->on('recdetails')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('existing_recs');
    }
}
