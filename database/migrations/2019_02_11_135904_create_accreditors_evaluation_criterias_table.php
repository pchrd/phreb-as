<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccreditorsEvaluationCriteriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accreditors_evaluation_criterias', function (Blueprint $table) {
            $table->increments('id');
            $table->string('criteria_name');
            $table->string('criteria_description');
            $table->string('criteria_percentage');
            $table->string('criteria_field1')->nullable();
            $table->string('criteria_field2')->nullable();
            $table->string('criteria_field3')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accreditors_evaluation_criterias');
    }
}
