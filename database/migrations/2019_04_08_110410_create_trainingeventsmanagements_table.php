<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingeventsmanagementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainingeventsmanagements', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('te_orientation_id');
            $table->unsignedInteger('te_trainee_id');
            $table->unsignedInteger('te_addedby_id');
            $table->unsignedInteger('te_affiliation_id')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();

            $table->foreign('te_orientation_id')->references('id')->on('trainingeventsorientations')->onDelete('cascade');
            $table->foreign('te_trainee_id')->references('id')->on('trainingeventstrainees')->onDelete('cascade');
            $table->foreign('te_addedby_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainingeventsmanagements');
    }
}
