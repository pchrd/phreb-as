<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatmessageFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chatmessage_files', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('cmf_message_id');
            $table->string('cmf_filename');
            $table->string('cmf_fileextensionname');
            $table->string('cmf_filesize');
            $table->string('cmf_filegenerated_id');
            $table->timestamps();

            $table->foreign('cmf_message_id')->references('id')->on('chatmessages')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chatmessage_files');
    }
}
