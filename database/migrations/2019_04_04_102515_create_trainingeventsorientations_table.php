<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingeventsorientationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainingeventsorientations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('training_type_name');
            $table->string('institution_name');
            $table->string('rec_name');
            $table->date('training_start_date');
            $table->date('training_end_date');
            $table->unsignedInteger('user_id');
            $table->timestamps();
            
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainingeventsorientations');
    }
}
