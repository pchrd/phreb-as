<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExistingArpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('existing_arps', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('recdetails_id');
            $table->unsignedInteger('selectarps');
            $table->string('foryear');
            $table->date('arps_submission');
            $table->string('file')->nullable();
            $table->string('file_uniqid')->nullable();
            $table->string('file_extension')->nullable();
            $table->timestamps();

            $table->foreign('recdetails_id')->references('id')->on('recdetails')->onDelete('cascade');
            $table->foreign('selectarps')->references('id')->on('document_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('existing_arps');
    }
}