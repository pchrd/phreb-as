<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecassessmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recassessments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('ra_user_id');
            $table->unsignedInteger('ra_recdetails_id');
            $table->unsignedInteger('ra_documents_types_id');
            $table->integer('ra_check')->nullable();
            $table->string('ra_recdocuments_file');
            $table->string('ra_recdocuments_file_generated');
            $table->string('ra_recdocuments_file_extension')->nullable();
            $table->dateTime('ra_dateuploaded');

            $table->foreign('ra_user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('ra_recdetails_id')->references('id')->on('recdetails')->onDelete('cascade');
            $table->foreign('ra_documents_types_id')->references('id')->on('document_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recassessments');
    }
}