<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatmessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chatmessages', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('send_user_id');
            $table->string('messageFrom');
            $table->string('messageSubject');
            $table->longText('messages');
            $table->dateTime('seen_status')->nullable();
            $table->dateTime('message_status')->nullable();
            $table->timestamps();

            $table->foreign('send_user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chatmessages');
    }
}
