<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmaillogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emaillogs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('e_sender_user_id');
            $table->unsignedInteger('e_recdetails_id');
            $table->string('e_from')->nullable();
            $table->string('e_to')->nullable();
            $table->string('e_cc')->nullable();
            $table->string('e_bcc')->nullable();
            $table->string('e_subject');
            $table->longText('e_body')->nullable();
            $table->integer('e_status')->default(0);
            $table->dateTime('e_created_at');

            $table->foreign('e_sender_user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('e_recdetails_id')->references('id')->on('recdetails')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emaillogs');
    }
}
