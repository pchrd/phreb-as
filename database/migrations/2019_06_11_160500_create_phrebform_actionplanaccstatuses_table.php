<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhrebformActionplanaccstatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phrebform_actionplanaccstatuses', function (Blueprint $table){

            $table->increments('id');
            $table->unsignedInteger('pf_acas_pfid');
            $table->unsignedInteger('pf_acas_status_id');
            $table->unsignedInteger('pf_acas_addedby_id');
            $table->string('pf_acas_status')->nullable();
            $table->timestamps();

            $table->foreign('pf_acas_pfid')->references('id')->on('phrebforms')->onDelete('cascade');
            $table->foreign('pf_acas_status_id')->references('id')->on('statuses')->onDelete('cascade');
            $table->foreign('pf_acas_addedby_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phrebform_actionplanaccstatuses');
    }
}
