<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingeventstraineesAffiliationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainingeventstrainees_affiliations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('trainees_id'); 
            $table->string('trainees_institution');
            $table->string('trainees_institution')->nullable();
            $table->string('trainees_institution_address')->nullable();
            $table->string('trainees_specialization')->nullable();
            $table->string('trainees_email');
            $table->string('trainees_contactno')->nullable();
            
            $table->string('trainees_profbackground')->nullable();
            $table->string('trainees_recrole')->nullable();
            $table->string('trainees_committee')->nullable();

            $table->unsignedInteger('addedby');
            $table->timestamps();

            $table->foreign('trainees_id')->references('id')->on('traineeseventstrainees')->onDelete('cascade');

            $table->foreign('users')->references('id')->on('addedby')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainingeventstrainees_affiliations');
    }
}
