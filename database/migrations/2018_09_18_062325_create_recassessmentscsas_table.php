<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecassessmentscsasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recassessmentscsas', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('ra_csa_ra_id');
            $table->unsignedInteger('ra_csa_sender_id');
            $table->unsignedInteger('ra_csa_csachair_id');
            $table->unsignedInteger('ra_csa_recdetails_id');
            $table->unsignedInteger('ra_csa_status')->nullable();
            $table->dateTime('ra_csa_datecreated_at');
            $table->date('ra_csa_dateexpiry');
            $table->dateTime('ra_csa_reminder_status')->nullable();
            $table->dateTime('ra_csa_reminder_status_deadline')->nullable();

            $table->foreign('ra_csa_ra_id')->references('id')->on('recassessments')->onDelete('cascade');
            $table->foreign('ra_csa_sender_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('ra_csa_csachair_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('ra_csa_recdetails_id')->references('id')->on('recdetails')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recassessmentscsas');
    }
}
