<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecchairsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recchairs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('recdetails_id');
            $table->string('rec_chairname');
            $table->string('rec_chairemail');
            $table->string('rec_chairmobile')->nullable();
            $table->foreign('recdetails_id')->references('id')->on('recdetails')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recchairs');
    }
}
