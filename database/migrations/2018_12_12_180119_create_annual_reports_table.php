<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnualReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('annual_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('ar_recdetails_id');
            $table->unsignedInteger('ar_recdocuments_id');
            $table->unsignedInteger('ar_title_id');
            $table->string('ar_status')->nullable();
            $table->timestamps();

            $table->foreign('ar_recdetails_id')->references('id')->on('recdetails')->onDelete('cascade');
            $table->foreign('ar_recdocuments_id')->references('id')->on('recdocuments')->onDelete('cascade');
            $table->foreign('ar_title_id')->references('id')->on('annual_report_titles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('annual_reports');
    }
}
