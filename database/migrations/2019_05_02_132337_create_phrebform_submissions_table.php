<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhrebformSubmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phrebform_submissions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('ps_recdetails_id');
            $table->unsignedInteger('ps_pfid');
            $table->unsignedInteger('ps_phrebforms_id');
            $table->unsignedInteger('ps_sender_id');
            $table->unsignedInteger('ps_csachair_id');
            $table->string('ps_link')->nullable();
            $table->string('ps_status')->nullable();
            $table->timestamps();

            $table->foreign('ps_recdetails_id')->references('id')->on('recdetails')->onDelete('cascade');
            $table->foreign('ps_pfid')->references('id')->on('phrebforms')->onDelete('cascade');
            $table->foreign('ps_phrebforms_id')->references('id')->on('phrebforms_templates')->onDelete('cascade');
            $table->foreign('ps_sender_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('ps_csachair_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phrebform_submissions');
    }
}
