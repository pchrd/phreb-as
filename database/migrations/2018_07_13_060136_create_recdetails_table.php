<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecdetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recdetails', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('rec_apptype_id');
            $table->unsignedInteger('rec_existing_record')->nullable();
            $table->string('rec_name');
            $table->string('rec_institution');
            $table->string('rec_address');
            $table->string('rec_email');
            $table->string('rec_telno')->nullable();
            $table->string('rec_faxno')->nullable();

            $table->string('rec_contactperson');
            $table->string('rec_contactposition')->nullable();
            $table->string('rec_contactmobileno')->nullable();
            $table->string('rec_contactemail');
            $table->date('rec_dateestablished');

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('rec_apptype_id')->references('id')->on('statuses')->onDelete('cascade');
            // $table->foreign('rec_name')->references('id')->on('reclists')->onDelete('cascade');
            
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recdetails');
    }
}