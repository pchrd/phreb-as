<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class); 
        //replaced by 
        $this->call(\RolesSeeder::class);
        $this->call(\LevelsSeeder::class);
        $this->call(\RegionsSeeder::class);
        $this->call(\StatusSeeder::class);
        $this->call(\DocumentTypesSeeder::class);
        $this->call(\EmailTemplateSeeder::class);
        $this->call(\UserSeeder::class);
        $this->call(\ReportingSeeder::class);
        $this->call(\ReclistSeeder::class);
    }
}