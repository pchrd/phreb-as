<?php
use App\Models\Region;
use Illuminate\Database\Seeder;

class RegionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ncr = Region::create([
        	'region_name' => 'NCR - National Capital Region',
        ]);

        $car = Region::create([
        	'region_name' => 'CAR - Cordillera Administrative Region',
        ]);

        $region1 = Region::create([
        	'region_name' => 'REGION I (Ilocos Region)',
        ]);

        $region2 = Region::create([
        	'region_name' => 'REGION II (Cagayan Valley)',
        ]);

        $region3 = Region::create([
        	'region_name' => 'REGION III (Central Luzon)',
        ]);

        $region4a = Region::create([
        	'region_name' => 'REGION IV-A (CALABARZON)',
        ]);

        $region4b = Region::create([
        	'region_name' => 'REGION IV-B (MIMAROPA Region)',
        ]);

        $region5 = Region::create([
        	'region_name' => 'REGION V (Bicol Region)',
        ]);

        $region6 = Region::create([
        	'region_name' => 'REGION VI (Western Visayas)',
        ]);

        $region7 = Region::create([
        	'region_name' => 'REGION VII (Central Visayas)',
        ]);

        $region8 = Region::create([
        	'region_name' => 'REGION VIII (Eastern Visayas)',
        ]);

        $region9 = Region::create([
        	'region_name' => 'REGION IX (Zamboanga Peninsula)',
        ]);

        $region10= Region::create([
        	'region_name' => 'REGION X (Northern Mindanao)',
        ]);

        $region11= Region::create([
        	'region_name' => 'REGION XI (Davao Region)',
        ]);

        $region12= Region::create([
        	'region_name' => 'REGION XII (SOCCSKSARGEN)',
        ]);

        $region13= Region::create([
        	'region_name' => 'REGION XIII (CARAGA)',
        ]);

        $armm= Region::create([
        	'region_name' => 'ARMM - Autonomous Region in Muslim Mindanao',
        ]);

    }
}