<?php
use App\Models\User;
use App\Models\Role_user;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $user=User::create([
            'name' => 'PHREB-AS Admin',
            'email' => 'phrebaccreditationsystem@gmail.com',
            'password' => bcrypt('1231235252'),
            'verified' => '1',
            'email_token' => base64_encode('phrebaccreditationsystem@gmail.com'),
        ]);
        $user->roles()->attach('1');
        return $user;
    }
}
