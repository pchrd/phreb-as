<?php
use App\Models\DocumentTypes;
use Illuminate\Database\Seeder;

class DocumentTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $application_letter = DocumentTypes::create([
        	'document_types_name' => 'Application Letter',
        ]);

        $phrebform_no1_1 = DocumentTypes::create([
        	'document_types_name' => 'Phreb Form No 1.1 (Application for Accredtation)',
        ]);

        $phrebform_no1_2 = DocumentTypes::create([
        	'document_types_name' => 'Phreb Form No. 1.2 (Annual Report, for previous year)',
        ]);

        $phrebform_no1_3 = DocumentTypes::create([
        	'document_types_name' => 'Phreb Form No. 1.3 (Protocol Summary, in the past year/s)',
        ]);

        $phrebform_no1_4 = DocumentTypes::create([
        	'document_types_name' => 'Phreb Form No. 1.4 (Self-Assessment)',
        ]);

        $rec_ioc = DocumentTypes::create([
        	'document_types_name' => 'Institutional Organization Chart showing the location of REC in relation to other institutional units',
        ]);

        $rec_torandappointment = DocumentTypes::create([
        	'document_types_name' => 'Membership TOR and Appointment Letters',
        ]);

        $rec_training_records = DocumentTypes::create([
        	'document_types_name' => 'REC Members and Staff Training Records',
        ]);

        $rec_cv = DocumentTypes::create([
        	'document_types_name' => 'REC Members and Staff CV',
        ]);

        $rec_latest_meeting = DocumentTypes::create([
        	'document_types_name' => 'Meeting Agenda and Minutes of the last three (3) meetings',
        ]);

		$rec_pictures = DocumentTypes::create([
        	'document_types_name' => 'REC Office Picture',
        ]);

        $rec_manual_sop = DocumentTypes::create([
        	'document_types_name' => 'SOP Manual',
        ]);

        $sop_related_forms = DocumentTypes::create([
        	'document_types_name' => 'SOP-related Forms and Letter Templates',
        ]);

        // --------------------------------------------------

        $accreditorsassessment = DocumentTypes::create([
            'document_types_name' => 'Accreditors Assessment',
        ]);

        $actionplan = DocumentTypes::create([
            'document_types_name' => 'Action Plan',
        ]);

        $complianceevidences = DocumentTypes::create([
            'document_types_name' => 'Compliance Evidences',
        ]);


        //---------------------------------------------------
        $initialevaluation_ap = DocumentTypes::create([
            'document_types_name' => 'Initial Evaluation on Action Plan',
        ]);

        $finalevaluation_ap = DocumentTypes::create([
            'document_types_name' => 'Final Evaluation on Action Plan',
        ]);

        $initialevaluation_ce = DocumentTypes::create([
            'document_types_name' => 'Initial Evaluation on Compliance Evidence',
        ]);

        $finalevaluation_ce = DocumentTypes::create([
            'document_types_name' => 'Final Evaluation on Compliance Evidence',
        ]);


        //Awarding year categorization
        $oneyear = DocumentTypes::create([
            'document_types_name' => '1 Year Provisional',
        ]);

        $twoyear = DocumentTypes::create([
            'document_types_name' => '2 Years Extension of Accreditation',
        ]);

        $threeyear = DocumentTypes::create([
            'document_types_name' => '3 Years Accreditation',
        ]);

        $threeyear_fa = DocumentTypes::create([
            'document_types_name' => '3 Years Full Accreditation',
        ]);

        $fouryear = DocumentTypes::create([
            'document_types_name' => '4 Years Full Accreditation',
        ]);

        //Msc files for REC
        $msc = DocumentTypes::create([
            'document_types_name' => 'Miscellaneous Files',
        ]);


    }
}
