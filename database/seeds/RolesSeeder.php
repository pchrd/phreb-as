<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){

        //inserting in database for super admin
        $superadmin = Role::create([
        	'role_name' => 'SuperAdmin',
        	'slug' => 'superadmin', 
        	'permissions' => json_encode([
        		'overall-functions' => true, 
        	]),
        ]);

        //for sec-incharge
        $incharge = Role::create([
        	'role_name' => 'Secretariat Incharge',
        	'slug' => 'secretariatincharge',
        	'permissions' =>json_encode([
        		'incharge-functions' => true,
        	]),
        ]);

        //for REC
		$rec = Role::create([
        	'role_name' => 'Research Ethics Commitee',
        	'slug' => 'rec',
        	'permissions' =>json_encode([
        		'rec-functions' => true,
        	]),
        ]);

        //for Accreditors
        $accreditors = Role::create([
            'role_name' => 'Accreditor',
            'slug' => 'accreditor',
            'permissions' =>json_encode([
                'accreditor-functions' => true,
            ]),
        ]);

         //for CSA Chair
        $csachair = Role::create([
            'role_name' => 'CSA Chair',
            'slug' => 'csachair',
            'permissions' =>json_encode([
                'csachair-functions' => true,
            ]),
        ]);

    }
}