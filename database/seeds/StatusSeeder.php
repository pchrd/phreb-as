<?php

use App\Models\Status;
use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pending_req = Status::create([
            'status_name' => 'Pending',
            'status_color' => '#132c84',
        ]);

        $completed_req = Status::create([
        	'status_name' => 'Completed Requirements',
            'status_color' => '#00ff04',
        ]);

        $incomplete_req = Status::create([
        	'status_name' => 'Incomplete Requirements',
            'status_color' => '#ffe500',
        ]);

        $accredited = Status::create([
        	'status_name' => 'Accredited',
            'status_color' => '#ffa100',
        ]);

        $withdrawn = Status::create([
            'status_name' => 'Withdrawn',
            'status_color' => '#ff00d8',
        ]);

        $provisional = Status::create([
            'status_name' => 'Provisional',
            'status_color' => '#d0ff00',
        ]);

        $submission_received = Status::create([
            'status_name' => 'Submission Received',
            'status_color' => '#00ffe1',
        ]);

        $re_accredited = Status::create([
            'status_name' => 'Re-Accreditation',
            'status_color' => '#00c3ff',
        ]);

        $re_submission_of_requirements = Status::create([
            'status_name' => 'Re-submission of Requirements',
            'status_color' => '#e25f5f',
        ]);

        $submissionofactionpan = Status::create([
            'status_name' => 'Submission of Action Plan and Compliance Evidences',
            'status_color' => '#e22f52',
        ]);

        $deferred = Status::create([
            'status_name' => 'Deferred',
            'status_color' => '#ff0000',
        ]);

        // application types
        $new = Status::create([
            'status_name' => 'New',
            'status_color' => '#ff0000',
        ]);

        $renewal = Status::create([
            'status_name' => 'Renewal',
            'status_color' => '#ff0000',
        ]);

        $upgrade = Status::create([
            'status_name' => 'Upgrade',
            'status_color' => '#ff0000',
        ]);

        $reaccreditation = Status::create([
            'status_name' => 'Re-accreditation',
            'status_color' => '#ff0000',
        ]);

        $existing_accredited = Status::create([
            'status_name' => 'Accredited (Already Accredited)',
            'status_color' => '#ff0000',
        ]);

        $accreditationexpired = Status::create([
            'status_name' => 'Accreditation Expired',
            'status_color' => '#ff0000',
        ]);

        $ongoingapplication = Status::create([
            'status_name' => 'On-going Application',
            'status_color' => '#ff0000',
        ]);


        //Usually for Phrebforms//
        $submitted = Status::create([
            'status_name' => 'Submitted',
            'status_color' => '#ff0000',
        ]);

        $returned = Status::create([
            'status_name' => 'Return to REC',
            'status_color' => '#ff0000',
        ]);

        $approved = Status::create([
            'status_name' => 'Approved',
            'status_color' => '#ff0000',
        ]);

        $declined = Status::create([
            'status_name' => 'Declined',
            'status_color' => '#ff0000',
        ]);

        $approved_accreditor = Status::create([
            'status_name' => 'Approved by Accreditor',
            'status_color' => '#ff0000',
        ]);

        $declined_accreditor = Status::create([
            'status_name' => 'Declined by Accreditor',
            'status_color' => '#ff0000',
        ]);

        $returntosecretariat = Status::create([
            'status_name' => 'Return to PHREB Secretariat-in-Charge',
            'status_color' => '#ff0000',
        ]);

        $forwardtoCSACHAIR = Status::create([
            'status_name' => 'Forwarded to CSA-Chair',
            'status_color' => '#ff0000',
        ]);

    }
}