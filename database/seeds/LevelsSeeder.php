<?php

use App\Models\Level;
use Illuminate\Database\Seeder;

class LevelsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$level1 = Level::create([
        	'level_name' => 'Level 1',
        	'level_description' => 'Level-I', 
        ]);

        $level2 = Level::create([
        	'level_name' => 'Level 2',
        	'level_description' => 'Level-II', 
        ]);

        $level3 = Level::create([
        	'level_name' => 'Level 3',
        	'level_description' => 'Level-III', 
        ]);
    
        $all_level = Level::create([
            'level_name' => 'All Levels',
            'level_description' => '---', 
        ]);

        $no_level = Level::create([
            'level_name' => 'No Levels',
            'level_description' => '---', 
        ]);

	}
}