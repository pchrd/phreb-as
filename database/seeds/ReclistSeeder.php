<?php
use App\Models\Reclist;
use Illuminate\Database\Seeder;

class ReclistSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rec = Reclist::create([
        	'reclist_name' => 'Research Ethics Committee (REC)',
        	'reclist_description' => '---', 
        ]);

        $erc = Reclist::create([
        	'reclist_name' => 'Ethics Review Committee (ERC)',
        	'reclist_description' => '---', 
        ]);

        $irb = Reclist::create([
        	'reclist_name' => 'Institutional Review Board (IRB)',
        	'reclist_description' => '---', 
        ]);

        $ierc = Reclist::create([
        	'reclist_name' => 'Institutional Ethics Review Committee (IERC)',
        	'reclist_description' => '---', 
        ]);

        $ierb = Reclist::create([
        	'reclist_name' => 'Institutional Ethics Review Board (IERB)',
        	'reclist_description' => '---', 
        ]);

        $reb = Reclist::create([
        	'reclist_name' => 'Research Ethics Board (REB)',
        	'reclist_description' => '---', 
        ]);

        $rerb = Reclist::create([
        	'reclist_name' => 'Research Ethics Review Board (RERB)',
        	'reclist_description' => '---', 
        ]);

        $reoc = Reclist::create([
        	'reclist_name' => 'Research Ethics Oversight Committee (REOC)',
        	'reclist_description' => '---', 
        ]);

        $hec = Reclist::create([
        	'reclist_name' => 'Hospital Ethics Committee (HEC)',
        	'reclist_description' => '---', 
        ]);

        $uerc = Reclist::create([
        	'reclist_name' => 'University Ethics Research Committee (UERC)',
        	'reclist_description' => '---', 
        ]);

        $rerc = Reclist::create([
        	'reclist_name' => 'Research Ethics Review Committee (RERC)',
        	'reclist_description' => '---', 
        ]);

        $crec = Reclist::create([
        	'reclist_name' => 'Cluster Research Ethics Committee (CREC)',
        	'reclist_description' => '---', 
        ]);

        $crerc = Reclist::create([
        	'reclist_name' => 'Cluster Research Ethics Review Committee (CRERC)',
        	'reclist_description' => '---', 
        ]);

        $iec = Reclist::create([
            'reclist_name' => 'Independent Ethics Committee (IEC)',
            'reclist_description' => '---', 
        ]);

        $reru = Reclist::create([
            'reclist_name' => 'Research Ethics Review Unit (RERU)',
            'reclist_description' => '---', 
        ]);

    }
}   