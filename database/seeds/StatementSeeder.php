<?php
use App\Models\Statement;
use Illuminate\Database\Seeder;

class StatementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $st1 = Status::create([
            'statement_title' => 'Consent 1',
            'statement_description' => '
            	I agree to provide soft copies of all the documentary requirements for PHREB accreditation for paperless review, and submit all the required documents or reports as requested by the Philippine Health Research Ethics Board (PHREB) in the PHREB Accreditation Portal and the PHREB website for accredited Research Ethics Committees (RECs);
            ',

        ]);

        $st2 = Status::create([
            'statement_title' => 'Consent 2',
            'statement_description' => '
            	I understand that this submission and all it contains will be forwarded to the PHREB Accreditors for the evaluation of our accreditation application and that communications regarding this application will be strictly between me, the PHREB, and its Secretariat, and the assigned PHREB accreditors.
            ',

        ]);

        $st3 = Status::create([
            'statement_title' => 'Consent 3',
            'statement_description' => '
            	I understand that my accreditation application will be indexed in PHREB Accreditation Portal and will be treated with confidentiality at all times.
            ',

        ]);

        $st4 = Status::create([
            'statement_title' => 'Consent 4',
            'statement_description' => '
            	I understand that PHREB may make available the general information and brief description of the REC to the general public once the Rec is accredited. In no case however, shall public disclosure of confidential information about the REC be made, by me or by PCHRD, which would constitute a prejudicial disclosure. 
            ',

        ]);


    }
}
