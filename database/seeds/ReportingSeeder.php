<?php
use App\Models\Reporting;
use Illuminate\Database\Seeder;

class ReportingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $reporting = Reporting::create([
        	'report_user_id' => '1',
        	'report_start_date' => '2018-01-01', 
        	'report_end_date' => '2018-12-31', 
        	]);
    }
}
