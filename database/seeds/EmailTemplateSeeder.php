<?php
use App\Models\EmailTemplate;
use Illuminate\Database\Seeder;

class EmailTemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $newapplication = EmailTemplate::create([
        	'email_name' => 'New Application ',
        	'email_subject' => 'New Application',
        	
        	'email_body' => '
        		<p>Dear PHREB Secretariat, </p>
				<p>$rec_name has applied for $level_name through the PHREB Online Accreditation System. Please see application details below:<br><br>
						REC NAME: <b>$rec_name</b><br>
						INSTITUTION: <b>$rec_institution</b><br>
						ADDRESS: <b>$rec_address</b><br>
						LEVEL: <b>$level_name</b><br>
						REGION: <b>$region_name</b><br>
						STATUS: <b>$status_name</b><br>
						DATE SUBMITTED: <b>$created_at</b><br>
				</p>	
        	'
        ]);

        $incomplete = EmailTemplate::create([
        	'email_name' => 'Incomplete',
        	'email_subject' => 'Incomplete',
        	
        	'email_body' => '
        		<p>Dear <b>$rec_name</b>,</p>
					<p>
						Your <b>$level_name</b> application has been tagged as <b><i style=\"color: brown;\">$status_name</i></b> in Online Accreditation System. <b>Kindly complete all the requirements on or before $deadline</b>.
					</p>
        	'
        ]);

        $resubmission = EmailTemplate::create([
        	'email_name' => 'Re-submission',
        	'email_subject' => 'Re-submission',
        	
        	'email_body' => '
        		<p>Dear <b>$rec_name</b>,</p>
					<p>
						Your <b>$level_name</b> application has been tagged as <b><i style=\"color: brown;\">$status_name</i></b> in Online Accreditation System. <b>Kindly furnish PHREB with all the deficiencies on or before $deadline</b>.
					</p>
        	'
        ]);

        $complete = EmailTemplate::create([
        	'email_name' => 'Complete ',
        	'email_subject' => 'Complete',
        	
        	'email_body' => '
        		<p>Dear <b>$rec_name</b>,</p>
					<p>
						Your <b>$level_name</b> application has been tagged as <b><i style=\"color: brown;\">$status_name</i></b> in Online Accreditation System. <b>Kindly submit two (2) sets of hard copies of your application on or before $deadline. Also please ensure to pay the accreditation fee of Php 3,000 either in cash or cheque</b> (payable to <b>Philippine Council for Health Research and Development</b>).
					</p>
				<p>Regards,<br>
				$sender<br>
				PHREB Secretariat</p>
				<p>_____________________</p>
				<p><small>*Requirements for Hardbound:<br>
				<i>Font: Times New Roman<br>
				   Font size: 12<br>
				   Must be labelled and bound accordingly
				</i>
				</small></p>
        	'
        ]);

        $resubmit_req = EmailTemplate::create([
        	'email_name' => 'Re-submit Requirements ',
        	'email_subject' => 'Resubmission of Requirements',
        	
        	'email_body' => '
        		<p>Dear <b>PHREB Secretariat</b>,</p>
					<p>
						The $rec_name has submitted the following requirements to the PHREB Online Accreditation System. Please review the following requirements by clicking the link below.
					</p>
        	'
        ]);

        $sendtoaccreditor = EmailTemplate::create([
        	'email_name' => 'Invitation to be Accreditor',
        	'email_subject' => 'Invitation to be Accreditor',
        	
        	'email_body' => '
        		Dear <b>$name ,</b>
            		<p>
            		The <b>$rec_name</b> has applied for a $level_name Accreditation ($status_name). With this, may we respectfully ask if you can be the accreditor/reviewer for this REC?
            		</p>
            <p>Thank you very much and we are looking forward to your response. </p><br>
            <p>Respectfully,<br>
            <b>$secname</b><br>
            <b>Secretariat</b> <br><br>
        	'
        ]);

        
        $sendassessmentform = EmailTemplate::create([
        	'email_name' => 'Accreditors Assessment Evaluation ',
        	'email_subject' => 'Accreditors Assessment Evaluation ',
        	
        	'email_body' => '
        		<p>
                Mr/Ms <b>$acc_name</b> ($role_name) has uploaded his/her assessment on $rec_name applied for Level $level_id ($status_name) on the PHREB Online Accreditation System.  
                </p>
        	'
        ]);

        $sendtocsachair = EmailTemplate::create([
        	'email_name' => 'Assessment - Evaluation of Accreditors',
        	'email_subject' => 'Assessment - Evaluation of Accreditors',
        	
        	'email_body' => '
        		<p>Good Day!</p>
        		<p>
        			 Ms/Mr $acc_name (Accreditor) have uploaded his/her Assessment on <b>$rec_name($status_name) applied for Level $level_id</b> to the PHREB Online Accreditation System. Please click the button below to view and download accreditors evaluation.<br>
        		</p>
        		<p>Thank you. <br><br>

        		Respectfully,<br>

        		<b>
        		$secname <br>
        		PHREB Secretariat	
        		</b>

        		</p> 
        	'
        ]);

        // ASSESSMENT--------------
        $notifiyaccreditors = EmailTemplate::create([
            'email_name' => 'Reminder for Evaluation of Accreditation Application',
            'email_subject' => 'Reminder for Evaluation of Accreditation Application',
            
            'email_body' => '
                Dear <b>$name</b>,<br>
                <p> 
                We would like to remind you regarding the submission of your assessment on the Level $level_id application of <b>$rec_name</b> due on <b>$expirydate</b>.
                </p> 
                <p>
                Respectfully,<br>
                PHREB Online Accreditation System</b>
                </p>
            '
        ]);

        $notifiycsachairs = EmailTemplate::create([
            'email_name' => 'Deadline of Submission of Final Assessment',
            'email_subject' => 'Deadline of Submission of Final Assessment',
            
            'email_body' => '
                Dear <b>$name</b>,<br>
                <p> 
                    May we  remind you that the deadline for the release of the  final assessment on <b>$rec_name Level $level_id <i>($status_name)</i></b> accreditation application is due this <b>$deadline</b>. May we respectfully request for the final assessment to be submitted on or before the said deadline. Thank you. 
                </p> 
                <p>
                Respectfully,<br>
                PHREB Online Accreditation System</b>
                </p>
            '
        ]);

        //Action Plan-------------------
        //within 30 days only 
        $ap_notifyrec = EmailTemplate::create([
            'email_name' => 'Dealine of REC Action Plan',
            'email_subject' => 'Deadline of REC Action Plan',
            
            'email_body' => '
                Dear <b>$rec_chairname</b>,<br>
                <p> 
                    This is to remind you that the deadline for the submission of Action Plan is on <b>$deadline</b>. Thank you.
                </p> 
                <p>
                Regards,<br>
                PHREB Online Accreditation System</b>
                </p>
            '
        ]);

        //Compliance Evidences---------------
        //within 6 months only 
        $ce_notifyrec = EmailTemplate::create([
            'email_name' => 'Dealine of REC Compliance Evidences',
            'email_subject' => 'Dealine of REC Compliance Evidences',
            
            'email_body' => '
                Dear <b>$rec_chairname</b>,<br>
                <p> 
                    This is to remind you that the deadline for the submission of your Compliance Evidences is on <b>$deadline</b>. Please submit your documents/requirements on or before the said deadline. Thank you.
                </p> 
                <p>
                Regards,<br>
                PHREB Online Accreditation System</b>
                </p>
            '
        ]);


        //Send OR to REC
         $sendOR = EmailTemplate::create([
            'email_name' => 'REC Order of Payment',
            'email_subject' => 'REC Order of Payment',
            
            'email_body' => '
                Dear <b>$rec_chairname</b>,<br>
                <p> 
                   Please see the attached <b>Order of Payment</b>. Thank you!
                </p> 
                <p>
                Best,<br><br>

                <b>$secname</b><br>
                PHREB Secretariat</b>
                </p>
            '
        ]);


         // Action plan & Comp Evi  when REC submits
        $actionplan_rec = EmailTemplate::create([
            'email_name' => 'REC Action Plan and Compliance Evidences',
            'email_subject' => 'REC Action Plan and Compliance Evidences',
            
            'email_body' => '
                <p><b>$date</b></p>
                <p>Dear <b>PHREB</b>,</p>
    
                <p>The REC <b>$rec_name - Level $level_id</b> has uploaded their $doctype_name to the PCHRD PHREB Online Accreditation System. Please see the attached files below. Thank you.
                </p>
            '
        ]);

        // Action plan & Comp Evi  when Accreditors/CSAChair submits
        $actionplan_rec = EmailTemplate::create([
            'email_name' => 'Accreditors/CSA Chairs Action Plan and Compliance Evidences',
            'email_subject' => 'Accreditors/CSA Chairs Action Plan and Compliance Evidences',
            
            'email_body' => '
                <p><b>$date</b></p>
                <p>Dear <b>PHREB</b>,</p>
    
                <p>The $role_name - <b>$name</b> has uploaded his/her $doctype_name for the REC <b>$rec_name</b>. Please see the attached files below. Thank you.
                </p>
            '
        ]);

        //when Secretariat forwarded to CSA Chair
        $actionplan_csa = EmailTemplate::create([
            'email_name' => 'Forwarding Accreditors Evaluation to CSA Chair',
            'email_subject' => 'Forwarding Accreditors Evaluation to CSA Chair',
            
            'email_body' => '
                <p>$date<br>
                $csachair<br>
                CSA Chair<br>
                </p>

                <p>Dear <b>$csachair</b>,</p>
                <p>
                    Please see the attached file of <i>$doctype_name</i> of <b>$rec_name - level $level_id</b> ($status_name). Sent from Accreditor $accname. Thank you. 
                </p>

                <p>
                    Regards,<br>
                    <b>$secname</b><br>
                    PHREB Secretariat
                </p>
            '
        ]);

        //when Secretariat forwarded the CSA chair's evel on apce with attached letter to REC
        $actionplan_csa = EmailTemplate::create([
            'email_name' => 'Forwarding Accreditors Evaluation to REC',
            'email_subject' => 'Forwarding Accreditors Evaluation to REC',
            
            'email_body' => '
                <p>$date<br>
                $rec_name<br> 
                $rec_address<br>
                $rec_chairname<br>
                Chair
                </p>

                <p>Dear <b>$rec_chairname</b>,<br>
                    Please see the attached files of $doctype_name on <b>$rec_name - Level $level_id</b> ($status_name). Thank you. 
                </p>

                <p>
                    Regards,<br>
                    <b>$secname</b><br>
                    PHREB Secretariat
                </p>
            '
        ]);

        //send OR and Receipt softcopy to REC
        $sendorreceipt = EmailTemplate::create([
            'email_name' => 'Official Receipt and Order Payment',
            'email_subject' => 'Official Receipt and Order Payment',
            
            'email_body' => '
                <p>$date<br>
                $rec_name - $rec_institution<br> 
                $rec_address<br>
                $rec_chairname<br>
                Chair
                </p><br>

                <p>Dear <b>$rec_chairname</b>,<br>
                    Forwarding you the soft copy of your OR and Order of Payment for your <b>$rec_name - $rec_institution : Level $level_id</b> Accreditation Application. We have sent the hard copies through LIBCAP. Kindly expect for these to arrive by the end of the week. Thank you. 
                </p>

                <p>
                    Respectfully,<br>
                    <b>$secname</b><br>
                    PHREB Secretariat
                </p>
            '
        ]);

        $awardingletter = EmailTemplate::create([
            'email_name' => 'Awarding Letter',
            'email_subject' => 'Awarding Letter - Congratulations, ',
            
            'email_body' => '
                <p>Dear <b>$rec_chairname</b>, <br><br>
                Please see the attached letter. Kindly acknowledge receipt. Thank you.<br>
                <p style="color: blue;">Congratulations and warm regards.</p>
                </p> 
                <b>$secname</b>,<br>
                Secretariat
            '
        ]);

        $accredited = EmailTemplate::create([   
            'email_name' => 'Accredited',
            'email_subject' => 'Accredited',
            
            'email_body' => '
                <p>Dear <b>$rec_name</b>,</p>
                    <p>
                        Your <b>$level_name</b> application has been tagged as <b><i style=\"color: brown;\">$status_name</i></b>  at <b>$date_accreditation </b> in PHREB Online Accreditation System.</b>. Please click below to see status of your application. Thank you.
                    </p>
            '
        ]);

        $accredited = EmailTemplate::create([   
            'email_name' => 'PHREB Accreditation System - New User',
            'email_subject' => 'New User Registration in PHREB - Accreditation System',
            
            'email_body' => '
                <p>Dear PHREB,</p>
                    <p>
                        Congratulations! New user has successfully registered as verified in PHREB - Accreditation System. 
                    </p>
                <h4>User Details</h4>
                <p>
                NAME: <b>$user</b><br>
                EMAIL: <b>$email</b><br>
                ROLE: <b>$role</b><br>
                DATE: <b>$date</b></br>
                </p>
            '
        ]);

        $accredited = EmailTemplate::create([   
            'email_name' => 'PHREB Annual Report',
            'email_subject' => 'PHREB Acreditation Annual Report',
            
            'email_body' => '
                <p>Dear PHREB,</p>
                    <p>
                        The $rec_name ($level_name) has uploaded the $document_types <i>($ar_name)</i> in the system. Thank you.
                    </p>
                </p>
            '
        ]);

        $arps = EmailTemplate::create([   
            'email_name' => 'PHREB Annual Report & Protocol Summary',
            'email_subject' => 'Reminders for PHREB Annual Report & Protocol Summary',
            
            'email_body' => '
                <p>Dear <b>$rec_institution</b>,</p><br>
                    <p>
                        This is to remind you that the deadline of the Annual report and Protocol Summary of <i>$rec_name - $rec_institution ($level) </i> for year <b>$year</b> is on <b>$deadline</b>. Please submit the following requirements on or before the said deadline. Thank you.
                    </p><br>
               <p>
                    Respectfully,<br>
                    <b>PHREB</b><br>
               </p>
            '
        ]);

        $acc_expiry = EmailTemplate::create([   
            'email_name' => 'PHREB Accreditation Reminder',
            'email_subject' => 'PHREB Accreditation Expiry Reminder',
            
            'email_body' => '
                <p>Dear <b>$rec_institution</b>,</p>
                    <p>
                        This is to remind you that the Expiration of Accreditation of <i>$rec_name - $rec_institution ($level)</i> is due on <b>$date_accreditation_expiry</b>. Thank you.
                    </p>
               <p>
                    Respectfully,<br>
                    <b>$secname</b><br>
                    Secretariat
               </p>
            '
        ]);

        $acc_expiry_system = EmailTemplate::create([   
            'email_name' => 'PHREB Accreditation Reminder',
            'email_subject' => 'PHREB Expiration of Accreditation',
            
            'email_body' => '
                <p>Dear <b>$rec_institution</b>,</p>
                    <p>
                        This is to remind you that the Expiration of Accreditation of <i>$rec_name - $rec_institution ($level)</i> is due on <b>$date_accreditation_expiry</b>. Thank you.
                    </p>
               <p>
                    Respectfully,<br>
                    <b>PHREB Accreditation</b><br>
               </p>
            '
        ]);

        // start of phrebform // ------------------------- another version kunno

        $notify_csa_chair_for_acc_assessments = EmailTemplate::create([   
            'email_name' => 'PHREB Accreditors Assessment Matrix Form',
            'email_subject' => 'PHREB Accreditors Assessment Matrix Form',
            
            'email_body' => '
                <p>Dear <b>$csachair_name</b>,</p>
                    <p>
                        The Assessment Maxtrix form for $rec_institution - $recname (<i>$level</i>) from the Accreditors $accreditor_name has been forwarded to your account. Please [<a href="$link">Click here to view </a>].
                    </p>
               <p>
                    Respectfully,<br>
                    <b>$secretariat</b><br>
               </p>
            '
        ]);

        $notify_rec_for_assessments = EmailTemplate::create([   
            'email_name' => 'PHREB REC Assessment Matrix Form',
            'email_subject' => 'PHREB REC Assessment Matrix Form',
            
            'email_body' => '
                <p>Dear <b>$recchair</b>,</p>
                    <p>
                        The Assessment Maxtrix form for your REC application; $rec_institution - $recname (<i>$level</i>) has been forwarded to your account. Please [<a href="$link">Click here to view </a>].
                    </p>
               <p>
                    Respectfully,<br>
                    <b>$secretariat</b><br>
               </p>
            '
        ]);

        $attendanceNotify = EmailTemplate::create([   
            'email_name' => 'PHREB Accreditation Visit & Orientation Notification',
            'email_subject' => 'PHREB Accreditation Visit & Orientation Notification',
            
            'email_body' => '

                    <p>Hi! <b>$fname $mname $lname</b>!</p>
                        <p>
                            Thank you for attending <b>$trainingtype ($start_date - $end_date)</b>. 
                        </p><br>

                    <p>
                        <small>--- <u>This message is auto-generated by the system</u> ---</small>
                    </p>
                    
                    '
        ]);


        //notify phreb sec and accreditors when rec submitted action plan & comp. evidences
        $rec_apcp_notifysecretariat = EmailTemplate::create([   
            'email_name' => 'PHREB Action Plan & Compliance of Evidences',
            'email_subject' => 'PHREB Action Plan & Compliance of Evidences',
            
            'email_body' => '

                    <p>Good Day!</p>
                        <p>
                            This message is to remind you that the <b>$rec_institution - $rec_name $level_name </b> has submitted their <b> $phrebformname at $dateofsubmission</b>. <u>[<a href="$link">Please Click here to view </a>]</u>. Thank you.
                        </p><br>

                    <p>
                        <small>--- <u>This message is auto-generated by the system</u> ---</small>
                    </p>
                    
                    '
        ]);

        //notify REC when PHREB SEC DO ACTIONS 
        $actions = EmailTemplate::create([   
            'email_name' => 'Reminder Status for PHREB Action Plan & Compliance of Evidences',
            'email_subject' => 'Reminder Status for PHREB Action Plan & Compliance of Evidences',
            
            'email_body' => '

                    <p>Good Day!</p>
                        <p>
                            This message is to remind you that the status of your <b>$actionplan</b> for <b>$rec_institution - $rec_name $level_name </b> is <b><u  style=\"color: red;\">"$status"</u></b>. Please check your PHREB Remarks/Comments under your form for other requirements needed by PHREB. Thank you. [<a href="$link">Please Click here to view </a>]
                        </p><br>

                    <p>
                        <small>--- <u>This message is auto-generated by the system</u> ---</small>
                    </p>
                    
                    '
        ]);

        $actionsNotifyPHREBSEC = EmailTemplate::create([   
            'email_name' => 'From Accreditors - Reminder Status for PHREB Action Plan & Compliance of Evidences',
            'email_subject' => 'From Accreditors - Reminder Status for PHREB Action Plan & Compliance of Evidences',
            
            'email_body' => '

                    <p>Good Day!</p>
                        <p>
                            This message is to remind you that the status given by $accreditor is <b><u  style=\"color: red;\">"$status"</u></b> for <b>$rec_institution - $rec_name $level_name </b> `s <b>$actionplan</b>. Please check PHREB Remarks/Comments under the form for other requirements needed. Thank you. [<a href="$link">Please Click here to view </a>]
                        </p><br>

                    <p>
                        <small>--- <u>This message is auto-generated by the system</u> ---</small>
                    </p>
                    
                    '
        ]);

        //new----Aug 26, 2019//
        $initialAssessment = EmailTemplate::create([   
            'email_name' => 'Submission: Initial Assessment Matrix Form',
            'email_subject' => 'Submission: Initial Assessment Matrix Form',
            
            'email_body' => '

                    <p>Good Day!</p>
                        <p>
                            The <b>$accreditorName</b> has successfully submitted his/her Initial $phrebformTemplate in the system for REC $recname. [<a href="$link">Please Click here to view </a>].
                        </p><br>

                        <h6>Other Messages/Comments:</h6>
                        <p>
                            $assessmentMessage
                        </p>

                    <p>
                        <small>--- <u>This message is auto-generated by the system</u> ---</small>
                    </p>
                    
                    '
        ]);

        $consolidatedAssessment = EmailTemplate::create([   
            'email_name' => 'Submission: Consolidated Assessment Matrix Form',
            'email_subject' => 'Submission: Consolidated Assessment Matrix Form',
            
            'email_body' => '
                        <p>
                           Hello, $accreditors. The $name successfully submitted the $phrebformTemplate in the system for REC <b></b> [<a href="$link">Please Click here to view </a>].
                        </p><br>

                        <h6>Other Messages/Comments:</h6>
                        <p>
                            $assessmentMessage
                        </p>

                        <p>
                            <small>--- <u>This message is auto-generated by the system</u> ---</small>
                        </p>
                    '
        ]);



        //messageschat
        $chatmessageNtification = EmailTemplate::create([   
            'email_name' => 'Mail Notification - PHREB Accreditation ',
            'email_subject' => 'Mail Notification - PHREB Accreditation ',
            
            'email_body' => '

                    <p>To: $name</p>
                        <p>
                            You have a received new message from <b><u  style=\"color: red;\">"$sender"</u></b>.Please check the following link to view. [<a href="$link"> View Message </a>]
                        </p><br>

                    <p>
                        <small>--- <u>This message is auto-generated by the system</u> ---</small>
                    </p>
                    
                    '
        ]);


    }
}


 