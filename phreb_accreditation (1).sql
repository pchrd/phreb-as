-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 13, 2019 at 10:17 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `phreb_accreditation`
--

-- --------------------------------------------------------

--
-- Table structure for table `accreditorslists`
--

CREATE TABLE `accreditorslists` (
  `id` int(10) UNSIGNED NOT NULL,
  `accreditors_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `accreditors_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `accreditors_designation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `accreditors_gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `accreditors_specialization` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `accreditors_office` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `accreditors_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `accreditors_contactnumber` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `accreditors_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `accreditorslists`
--

INSERT INTO `accreditorslists` (`id`, `accreditors_name`, `accreditors_status`, `accreditors_designation`, `accreditors_gender`, `accreditors_specialization`, `accreditors_office`, `accreditors_address`, `accreditors_contactnumber`, `accreditors_email`, `created_at`, `updated_at`) VALUES
(1, 'Dr. Marita Reyes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-02-13 05:33:48'),
(2, 'Dr. Cecilia Tomas', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:22:56', '2019-01-14 00:03:52'),
(3, 'Dr. Ma. Salome Vios', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:23:38', '2019-01-13 23:23:38'),
(4, 'Prof. Edlyn Jimenez', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-14 00:04:30', '2019-01-14 00:04:30'),
(5, 'Dr. Angelica Francisco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-14 00:04:52', '2019-01-14 00:04:52'),
(6, 'Dr. Doris Benavides\r\n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(7, 'Dr. Leslie Dalmacio', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(8, 'Dr. Rhodora Estacio', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(9, 'Dr. Evangeline Santos', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(10, 'Prof. Michael George Peralta', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(11, 'Dr. Froilan Jacinto Obillo', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(12, 'Dr. Marcelito Durante', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(13, 'Dr. Concesa Padilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(14, 'Dr. Maramba', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(15, 'Dr. Ana Liza Duran', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(16, 'Dr. Saturnino Javier', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(17, 'Dr. Sonia Bongala', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(18, 'Dr. Angela Du', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(19, 'Dr. Evelyn Lacson', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(20, 'Dr. Fred Guillergan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(21, 'Dr. Emerson Donaldo', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(22, 'Dr. Virginia de Jesus', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(23, 'Dr. Marissa Orillaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(24, 'Dr. Eden Latosa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(25, 'Dr. Veronica Tallo', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(26, 'Dr. Prospero Ma. Tuaño', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(27, 'Ms. Cynthia Mapua', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(28, 'Dr. Carlo Emmanuel Sumpaico', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(29, 'Dr. Blanquita de Guzman', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(30, 'Dr. Guia Tan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(31, 'Dr. Jennifer Nailes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(32, 'Dr. Gemiliano Aligui', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(33, 'Dr. Jacinto Blas Mantaring III', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(34, 'Dr. Patricia Khu', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(35, 'Prof. Teresita de Guzman', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(36, 'Mrs. Felicidad Romualdez', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(37, 'Dr. Tito Atienza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(38, 'Dr. Roberto Villanueva', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(39, 'Dr. Ma. Stelle Paspe', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(40, 'Dr. Alvin Concha', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(41, 'Dr. Ma. Elinore Concha', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(42, 'Dr. Nimfa Baria', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(43, 'Dr. Pura Caisip', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(44, 'Dr. Anita Matilde Poblete', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(45, 'Dr. Milagros Neri', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(46, 'Dr. Victoria Idolor', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(47, 'Dr. Lucila Perez', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(48, 'Dr. Marsha Tolentino', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-01-14 00:04:52'),
(49, 'Dr. Milagros Viacrusis II', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 23:13:38', '2019-02-11 04:56:25');

-- --------------------------------------------------------

--
-- Table structure for table `accreditors_evaluations`
--

CREATE TABLE `accreditors_evaluations` (
  `id` int(10) UNSIGNED NOT NULL,
  `accreditors_id` int(10) UNSIGNED NOT NULL,
  `evaluatedby_user_id` int(10) UNSIGNED NOT NULL,
  `title_award` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `field1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `accreditors_evaluations`
--

INSERT INTO `accreditors_evaluations` (`id`, `accreditors_id`, `evaluatedby_user_id`, `title_award`, `field1`, `created_at`, `updated_at`) VALUES
(168, 1, 2, 'PHREB Awards', NULL, '2019-02-13 09:09:49', '2019-02-13 09:09:49');

-- --------------------------------------------------------

--
-- Table structure for table `accreditors_evaluation_criterias`
--

CREATE TABLE `accreditors_evaluation_criterias` (
  `id` int(10) UNSIGNED NOT NULL,
  `criteria_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `criteria_description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `criteria_percentage` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `criteria_field1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `criteria_field2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `criteria_field3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `accreditors_evaluation_criterias`
--

INSERT INTO `accreditors_evaluation_criterias` (`id`, `criteria_name`, `criteria_description`, `criteria_percentage`, `criteria_field1`, `criteria_field2`, `criteria_field3`, `created_at`, `updated_at`) VALUES
(1, 'Ethical expertise', '- Has high level knowledge and skill on ethical review', '40', NULL, NULL, NULL, '2019-02-11 06:45:56', '2019-02-11 06:45:56'),
(2, 'Technical expertise', '- Has conducted and published several studies related to the topic', '25', NULL, NULL, NULL, '2019-02-11 06:51:35', '2019-02-11 06:51:35'),
(3, 'Critical thinking ability', '- Conducts an objective analysis and evaluation of an issue in order to form a judgement', '25', NULL, NULL, NULL, '2019-02-11 06:51:58', '2019-02-11 06:51:58'),
(4, 'Timeliness', '- Submits evaluation reports on or before scheduled time', '10', NULL, NULL, NULL, '2019-02-11 06:52:19', '2019-02-11 06:52:19');

-- --------------------------------------------------------

--
-- Table structure for table `accreditors_eval_tables`
--

CREATE TABLE `accreditors_eval_tables` (
  `id` int(10) UNSIGNED NOT NULL,
  `accreditors_evaluations_id` int(10) UNSIGNED DEFAULT NULL,
  `criteria_id` int(10) UNSIGNED NOT NULL,
  `acc_rate` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `field1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `field2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `accreditors_eval_tables`
--

INSERT INTO `accreditors_eval_tables` (`id`, `accreditors_evaluations_id`, `criteria_id`, `acc_rate`, `field1`, `field2`) VALUES
(79, 168, 1, '30', NULL, NULL),
(80, 168, 2, '20', NULL, NULL),
(83, 168, 3, '22', NULL, NULL),
(84, 168, 4, '9', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `annual_reports`
--

CREATE TABLE `annual_reports` (
  `id` int(10) UNSIGNED NOT NULL,
  `ar_recdetails_id` int(10) UNSIGNED NOT NULL,
  `ar_recdocuments_id` int(10) UNSIGNED NOT NULL,
  `ar_title_id` int(10) UNSIGNED NOT NULL,
  `ar_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `annual_report_titles`
--

CREATE TABLE `annual_report_titles` (
  `id` int(10) UNSIGNED NOT NULL,
  `ar_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ar_desc` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ar_start_year` date DEFAULT NULL,
  `ar_end_year` date DEFAULT NULL,
  `ar_sobrang_field` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `applicationlevel`
--

CREATE TABLE `applicationlevel` (
  `id` int(10) UNSIGNED NOT NULL,
  `recdetails_id` int(10) UNSIGNED NOT NULL,
  `level_id` int(10) UNSIGNED NOT NULL,
  `region_id` int(10) UNSIGNED NOT NULL,
  `statuses_id` int(10) UNSIGNED DEFAULT '1',
  `recdetails_submission_status` bigint(20) NOT NULL DEFAULT '0',
  `recdetails_lock_fields` bigint(20) NOT NULL DEFAULT '0',
  `recdetails_resubmitreq` bigint(20) NOT NULL DEFAULT '0',
  `date_completed` date DEFAULT NULL,
  `date_accreditation` date DEFAULT NULL,
  `date_accreditation_expiry` date DEFAULT NULL,
  `accreditation_number` longtext COLLATE utf8mb4_unicode_ci,
  `rec_apce` int(11) NOT NULL DEFAULT '0',
  `rec_accreditation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rec_change_of_acc` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `applicationlevel`
--

INSERT INTO `applicationlevel` (`id`, `recdetails_id`, `level_id`, `region_id`, `statuses_id`, `recdetails_submission_status`, `recdetails_lock_fields`, `recdetails_resubmitreq`, `date_completed`, `date_accreditation`, `date_accreditation_expiry`, `accreditation_number`, `rec_apce`, `rec_accreditation`, `rec_change_of_acc`, `created_at`, `updated_at`) VALUES
(2, 2, 1, 1, 17, 1, 1, 0, '2017-06-19', '2017-07-01', '2018-07-02', 'L1-2017-014-01', 0, '1 Year Provisional', NULL, '2019-01-16 05:11:45', '2019-01-16 05:11:45'),
(3, 3, 1, 1, 17, 1, 1, 0, '0001-01-01', '2017-03-01', '2018-02-28', 'L1-2017-009-01', 0, '', NULL, '2019-01-16 05:51:52', '2019-01-17 01:01:53'),
(5, 5, 1, 5, 4, 1, 1, 0, '0001-01-01', '2017-05-30', '2020-05-29', 'L1-2017-012-01', 0, '3 Years Full Accreditation', NULL, '2019-01-16 06:40:47', '2019-01-16 06:40:47'),
(6, 6, 1, 2, 4, 1, 1, 0, NULL, '2017-05-30', '2020-05-29', 'L1-2017-007-01', 0, '3 Years Accreditation', NULL, '2019-01-16 08:20:19', '2019-01-16 08:20:19'),
(9, 9, 1, 12, 17, 1, 1, 0, '2016-01-22', '2017-09-01', '2018-08-31', NULL, 0, '1 Year Provisional', NULL, '2019-01-17 03:23:54', '2019-01-17 03:23:54'),
(10, 10, 1, 6, 4, 1, 1, 0, '2017-10-13', '2018-06-06', '2019-06-06', 'L1-2018-017-01', 0, '1 Year Provisional', NULL, '2019-01-17 05:16:12', '2019-01-17 05:16:12'),
(12, 12, 1, 5, 4, 1, 1, 0, NULL, '2018-04-19', '2019-04-18', 'L1-2018-015-01', 0, '1 Year Provisional', NULL, '2019-01-17 08:34:53', '2019-01-17 08:34:53'),
(13, 13, 1, 11, 4, 1, 1, 0, NULL, '2018-03-08', '2019-03-07', NULL, 0, '1 Year Provisional', NULL, '2019-01-18 06:14:42', '2019-01-18 06:14:43'),
(14, 14, 1, 15, 4, 1, 1, 0, NULL, '2018-09-24', '2019-09-23', NULL, 0, '1 Year Provisional', NULL, '2019-01-18 06:25:14', '2019-01-18 06:25:14'),
(15, 15, 1, 5, 4, 1, 1, 0, NULL, '2018-06-09', '2019-06-06', 'L1-2018-016-01', 0, '1 Year Provisional', NULL, '2019-01-18 07:09:25', '2019-01-18 07:09:25'),
(17, 17, 1, 9, 17, 1, 1, 0, '2017-12-07', '0001-01-01', '0001-01-01', '00000000000', 0, '', NULL, '2019-01-21 02:22:13', '2019-01-21 02:22:13'),
(18, 18, 1, 9, 4, 1, 1, 0, NULL, '2017-10-11', '2019-10-10', NULL, 0, '2 Years Extension of Accreditation', NULL, '2019-01-21 05:55:44', '2019-01-21 05:55:44'),
(20, 20, 2, 1, 4, 1, 1, 0, NULL, '2017-03-16', '2019-03-15', NULL, 0, '2 Years Extension of Accreditation', NULL, '2019-01-21 07:01:06', '2019-01-21 07:01:06'),
(21, 21, 2, 1, 4, 1, 1, 0, NULL, '2017-08-09', '2019-07-08', NULL, 0, '2 Years Extension of Accreditation', NULL, '2019-01-21 07:09:04', '2019-01-21 07:09:04'),
(22, 22, 2, 1, 4, 1, 1, 0, NULL, '2018-06-25', '2019-06-24', NULL, 0, '1 Year Provisional Extension of Accreditation', NULL, '2019-01-21 07:18:55', '2019-01-21 07:18:55'),
(23, 23, 2, 1, 4, 1, 1, 0, NULL, '2017-10-26', '2020-10-25', NULL, 0, '3 Years Full Accreditation', NULL, '2019-01-21 07:29:41', '2019-01-21 07:29:41'),
(24, 24, 2, 4, 4, 1, 1, 0, NULL, '2018-09-13', '2020-09-12', NULL, 0, '2 Years Extension of Accreditation', NULL, '2019-01-21 08:20:49', '2019-01-21 08:20:49'),
(25, 25, 2, 6, 4, 1, 1, 0, NULL, '2017-09-15', '2019-09-14', NULL, 0, '2 Years Extension of Accreditation', NULL, '2019-01-21 08:25:57', '2019-01-21 08:25:57'),
(26, 26, 2, 11, 4, 1, 1, 0, NULL, '2016-08-01', '2019-07-31', NULL, 0, '3 Years Full Accreditation', NULL, '2019-01-21 08:32:11', '2019-01-21 08:32:11'),
(27, 27, 2, 12, 4, 1, 1, 0, NULL, '2018-07-07', '2019-07-06', NULL, 0, '1 Year Extension of Provisional Accreditation', NULL, '2019-01-22 00:11:11', '2019-01-22 00:11:11'),
(28, 28, 2, 12, 4, 1, 1, 0, NULL, '2017-07-17', '2019-07-16', NULL, 0, '2 Years Extension of Accreditation', NULL, '2019-01-22 00:33:03', '2019-01-22 00:33:03'),
(29, 29, 2, 12, 4, 1, 1, 0, NULL, '2017-12-11', '2019-12-10', NULL, 0, '2 Years Accreditation', NULL, '2019-01-22 00:38:20', '2019-01-22 00:38:20'),
(30, 30, 2, 1, 4, 1, 1, 0, NULL, '2018-03-02', '2020-03-01', NULL, 0, '2 Years Extension of Accreditation', NULL, '2019-01-22 00:47:09', '2019-01-22 00:47:09'),
(31, 31, 2, 14, 4, 1, 1, 0, NULL, '2018-03-16', '2019-03-15', 'Reg. No.: 08-062', 0, '1 Year Provisional', NULL, '2019-01-22 00:57:00', '2019-01-22 00:57:00'),
(32, 32, 2, 10, 4, 1, 1, 0, NULL, '2018-04-10', '2019-03-09', NULL, 0, '1 Year Provisional', NULL, '2019-01-22 01:03:55', '2019-01-22 01:03:55'),
(33, 33, 2, 11, 4, 1, 1, 0, NULL, '2016-08-12', '2019-08-11', NULL, 0, '3 Years Full Accreditation', NULL, '2019-01-22 01:13:36', '2019-01-22 01:13:36'),
(34, 34, 2, 10, 4, 1, 1, 0, NULL, '2018-06-04', '2019-06-03', NULL, 0, '1 Year Provisional', NULL, '2019-01-22 01:19:23', '2019-01-22 01:19:23'),
(35, 35, 2, 1, 4, 1, 1, 0, NULL, '2018-08-23', '2019-08-22', NULL, 0, '1 Year Provisional', NULL, '2019-01-22 01:34:56', '2019-01-22 01:34:56'),
(36, 36, 2, 10, 4, 1, 1, 0, NULL, '2018-04-27', '2019-03-26', NULL, 0, '1 Year Provisional', NULL, '2019-01-22 06:13:02', '2019-01-22 06:13:02'),
(37, 37, 2, 14, 4, 1, 1, 0, NULL, '2019-01-14', '2019-07-13', NULL, 0, '6 Months Provisional Accreditation', NULL, '2019-01-22 07:18:18', '2019-01-22 07:18:18'),
(38, 38, 3, 2, 4, 1, 1, 0, NULL, '2017-11-26', '2021-11-25', NULL, 0, '4 Years Full Accreditation', NULL, '2019-01-22 08:11:26', '2019-01-22 08:11:26'),
(39, 39, 2, 1, 17, 1, 1, 0, NULL, '2018-09-21', '2019-01-30', NULL, 0, 'Extension of Accreditation', NULL, '2019-01-23 01:24:00', '2019-01-23 01:24:00'),
(40, 40, 2, 4, 17, 1, 1, 0, NULL, '2017-02-21', '2018-08-31', NULL, 0, 'One (1) year provisional accreditation, with approved six (6) months extended accreditation', NULL, '2019-01-25 01:18:26', '2019-01-25 01:18:26'),
(41, 41, 1, 5, 4, 1, 1, 0, NULL, '2018-07-16', '2020-07-15', NULL, 0, '2 Years Full Accreditation', NULL, '2019-01-31 02:35:43', '2019-01-31 02:35:43'),
(42, 42, 1, 14, 4, 1, 1, 0, NULL, '2018-10-08', '2020-10-07', NULL, 0, '2 Years Full Accreditation', NULL, '2019-01-31 02:44:22', '2019-01-31 02:44:22'),
(43, 43, 1, 3, 4, 1, 1, 0, NULL, '2018-06-15', '2019-06-14', NULL, 0, '1 Year Provisional', NULL, '2019-01-31 02:58:29', '2019-01-31 02:58:29'),
(44, 44, 2, 14, 4, 1, 1, 0, NULL, '2019-01-30', '2019-07-29', NULL, 0, '6 Months Provisional', NULL, '2019-01-31 07:03:32', '2019-01-31 07:03:33'),
(45, 45, 3, 1, 4, 1, 1, 0, NULL, '2017-11-09', '2021-11-08', NULL, 0, ', 4 Years Full Accreditation', NULL, '2019-02-01 01:17:21', '2019-02-01 01:17:21'),
(46, 46, 3, 1, 4, 1, 1, 0, '0001-01-01', '2017-05-18', '2019-05-17', '00000000000', 0, '2 Years Accreditation', NULL, '2019-02-01 01:26:52', '2019-02-01 01:26:52'),
(47, 47, 3, 1, 4, 1, 1, 0, '0001-01-01', '2016-03-03', '2019-03-03', '00000000000', 0, '3 Years Full Accreditation', NULL, '2019-02-01 01:37:48', '2019-02-01 01:37:48'),
(48, 48, 3, 1, 4, 1, 1, 0, '0001-01-01', '2018-03-05', '2022-03-05', '00000000000', 0, '4 Years Full Accreditation', NULL, '2019-02-01 06:29:53', '2019-02-01 06:29:53'),
(49, 49, 3, 1, 4, 1, 1, 0, NULL, '2017-03-14', '2021-03-14', 'L3-2014-001-01', 0, '4 Years Full Accreditation', NULL, '2019-02-01 06:35:30', '2019-02-01 06:35:30'),
(50, 50, 3, 1, 4, 1, 1, 0, NULL, '2016-03-07', '2019-03-07', NULL, 0, '3 Years Full Accreditation', NULL, '2019-02-01 07:37:34', '2019-02-01 07:37:34'),
(51, 51, 3, 1, 4, 1, 1, 0, '0001-01-01', '2017-03-14', '2022-03-14', 'L3-2014-002-01', 0, '4 Years Full Accreditation', NULL, '2019-02-01 07:46:31', '2019-02-01 07:46:31'),
(52, 52, 2, 1, 17, 1, 1, 0, NULL, '2016-03-16', '2017-03-17', NULL, 0, '1 Year Provisional', NULL, '2019-02-01 07:54:31', '2019-02-01 07:54:31'),
(53, 53, 3, 1, 4, 1, 1, 0, NULL, '2018-07-23', '2022-07-22', NULL, 0, '4 Years Full Accreditation', NULL, '2019-02-01 07:55:59', '2019-02-01 07:56:00'),
(54, 54, 3, 1, 4, 1, 1, 0, '0001-01-01', '2016-02-01', '2019-02-01', '00000000000', 0, '3 Years Full Accreditation', NULL, '2019-02-01 08:49:41', '2019-02-01 08:49:41'),
(55, 55, 3, 1, 4, 1, 1, 0, NULL, '2017-10-30', '2021-10-29', NULL, 0, '4 Years Full Accreditation', NULL, '2019-02-01 08:55:07', '2019-02-01 08:55:07'),
(56, 56, 3, 1, 4, 1, 1, 0, '0001-01-01', '2018-07-16', '2021-07-15', '00000000000', 0, '3 Years Full Accreditation', NULL, '2019-02-01 09:05:55', '2019-02-01 09:05:55'),
(57, 57, 3, 1, 4, 1, 1, 0, '0001-01-01', '2016-05-04', '2019-05-04', '00000000000', 0, '3 Years Full Accreditation', NULL, '2019-02-04 01:13:44', '2019-02-04 01:13:44'),
(58, 58, 3, 1, 4, 1, 1, 0, '0001-01-01', '2016-03-10', '2019-03-10', '12-001', 0, '3 Years Full Accreditation', NULL, '2019-02-04 01:19:28', '2019-02-04 01:19:29'),
(59, 59, 3, 1, 4, 1, 1, 0, '0001-01-01', '2016-05-04', '2019-05-04', '00000000000', 0, '3 Years Full Accreditation', NULL, '2019-02-04 01:59:35', '2019-02-04 01:59:35'),
(60, 60, 3, 1, 4, 1, 1, 0, '0001-01-01', '2016-03-03', '2019-03-03', '00000000000', 0, '3 Years Full Accreditation', NULL, '2019-02-04 02:03:00', '2019-02-04 02:03:00'),
(61, 61, 3, 1, 4, 1, 1, 0, '0001-01-01', '2017-10-03', '2021-10-02', 'L3-2014-003-02', 0, '4 Years Full Accreditation', NULL, '2019-02-04 03:34:24', '2019-02-04 03:34:24'),
(62, 62, 3, 1, 4, 1, 1, 0, '0001-01-01', '2016-02-12', '2019-02-12', '00000000000', 0, '3 Years Full Accreditation', NULL, '2019-02-04 05:28:41', '2019-02-04 05:28:41'),
(63, 63, 3, 1, 4, 1, 1, 0, '0001-01-01', '2017-03-14', '2021-03-14', 'L3-2014-005-01', 0, '4 Years Full Accreditation', NULL, '2019-02-04 05:33:08', '2019-02-04 05:33:08'),
(64, 64, 3, 9, 4, 1, 1, 0, '0001-01-01', '2016-03-03', '2019-03-03', NULL, 0, '3 Years Full Accreditation', NULL, '2019-02-04 05:42:46', '2019-02-04 05:42:46'),
(65, 65, 3, 9, 4, 1, 1, 0, NULL, '2016-03-11', '2019-03-11', '00000000000', 0, '3 Years Full Accreditation', NULL, '2019-02-04 05:52:10', '2019-02-04 05:52:10'),
(66, 66, 3, 9, 4, 1, 1, 0, NULL, '2017-08-09', '2019-08-08', NULL, 0, '2 Years Accreditation', NULL, '2019-02-04 07:23:44', '2019-02-04 07:23:44'),
(67, 67, 3, 9, 4, 1, 1, 0, NULL, '2016-02-26', '2019-02-26', NULL, 0, '3 Years Full Accreditation', NULL, '2019-02-04 07:30:41', '2019-02-04 07:30:41'),
(68, 68, 3, 9, 4, 1, 1, 0, '0001-01-01', '2017-01-04', '2020-01-03', '00000000000', 0, '3 Years Full Accreditation', NULL, '2019-02-04 07:37:08', '2019-02-04 07:37:08'),
(69, 69, 3, 10, 4, 1, 1, 0, '0001-01-01', '2017-03-16', '2019-03-15', NULL, 0, '2 Years Full Accreditation', NULL, '2019-02-04 07:50:14', '2019-02-04 07:50:14'),
(70, 70, 3, 10, 4, 1, 1, 0, NULL, '2016-02-26', '2019-02-26', NULL, 0, '3 Years Full Accreditation', NULL, '2019-02-04 07:55:26', '2019-02-04 07:55:26'),
(71, 71, 3, 5, 4, 1, 1, 0, NULL, '2016-08-01', '2019-08-01', NULL, 0, '3 Years Accreditation', NULL, '2019-02-06 01:50:58', '2019-02-06 01:50:58'),
(72, 72, 3, 6, 4, 1, 1, 0, NULL, '2016-06-15', '2019-06-15', NULL, 0, '3 Years Full Accreditation', NULL, '2019-02-06 02:04:14', '2019-02-06 02:04:14'),
(73, 73, 3, 6, 4, 1, 1, 0, NULL, '2016-02-01', '2019-02-01', NULL, 0, '3 Years Full Accreditation', NULL, '2019-02-06 02:20:45', '2019-02-06 02:20:45'),
(74, 74, 3, 6, 4, 1, 1, 0, NULL, '2018-03-07', '2021-03-07', NULL, 0, '3 Years Full Accreditation', NULL, '2019-02-06 02:26:14', '2019-02-06 02:26:14'),
(75, 75, 3, 1, 4, 1, 1, 0, NULL, '2016-03-17', '2019-03-17', NULL, 0, '3 Years Full Accreditation', NULL, '2019-02-06 02:32:26', '2019-02-06 02:32:26'),
(76, 76, 3, 1, 4, 1, 1, 0, NULL, '2016-02-01', '2019-02-01', NULL, 0, '3 Years Full Accreditation', NULL, '2019-02-06 02:36:24', '2019-02-06 02:36:24'),
(77, 77, 3, 1, 4, 1, 1, 0, NULL, '2016-02-26', '2019-02-26', NULL, 0, '3 Years Full Accreditation', NULL, '2019-02-06 02:41:20', '2019-02-06 02:41:20'),
(78, 78, 3, 1, 4, 1, 1, 0, NULL, '2016-03-03', '2019-03-03', NULL, 0, '3 Years Full Accreditation', NULL, '2019-02-06 02:44:57', '2019-02-06 02:44:58'),
(79, 79, 3, 14, 4, 1, 1, 0, NULL, '2017-03-14', '2021-03-14', NULL, 0, '4 Years Full Accreditation', NULL, '2019-02-07 01:01:04', '2019-02-07 01:01:04'),
(80, 80, 3, 3, 4, 1, 1, 0, NULL, '2014-11-25', '2017-11-25', NULL, 0, '3 Years Full Accreditation', NULL, '2019-02-07 01:06:13', '2019-02-07 01:06:13'),
(81, 81, 3, 10, 4, 1, 1, 0, NULL, '2018-09-24', '2022-09-23', NULL, 0, '4 Years Full Accreditation', NULL, '2019-02-07 01:09:43', '2019-02-07 01:09:43'),
(82, 82, 3, 14, 4, 1, 1, 0, NULL, '2018-07-11', '2019-07-10', NULL, 0, '4 Years Full Accreditation', NULL, '2019-02-07 01:12:13', '2019-02-07 01:12:13'),
(83, 83, 3, 1, 4, 1, 1, 0, NULL, '2018-06-01', '2022-05-31', NULL, 0, '4 Years Full Accreditation', NULL, '2019-02-07 01:15:56', '2019-02-07 01:15:56'),
(84, 84, 3, 1, 4, 1, 1, 0, NULL, '2018-05-03', '2022-05-22', NULL, 0, '4 Years Full Accreditation', NULL, '2019-02-07 01:20:52', '2019-02-07 01:20:52'),
(85, 85, 3, 1, 4, 1, 1, 0, NULL, '2018-05-21', '2022-05-20', NULL, 0, '4 Years Full Accreditation', NULL, '2019-02-07 01:26:24', '2019-02-07 01:26:24'),
(86, 86, 3, 1, 4, 1, 1, 0, NULL, '2017-05-31', '2018-05-30', NULL, 0, '1 Year Provisional', NULL, '2019-02-07 01:30:56', '2019-02-07 01:30:56'),
(87, 87, 3, 1, 4, 1, 1, 0, NULL, '2017-07-01', '2018-01-31', NULL, 0, '6 Months Extended Accreditation', NULL, '2019-02-07 01:35:04', '2019-02-07 01:35:04'),
(88, 88, 3, 14, 4, 1, 1, 0, NULL, '2018-07-11', '2019-07-10', NULL, 0, '1 Year Provisional', NULL, '2019-02-07 01:48:37', '2019-02-07 01:48:37'),
(89, 89, 3, 14, 4, 1, 1, 0, NULL, '2018-07-11', '2019-07-10', NULL, 0, '1 Year Provisional', NULL, '2019-02-07 01:54:31', '2019-02-07 01:54:31'),
(90, 90, 3, 1, 4, 1, 1, 0, NULL, '2018-08-01', '2019-07-31', NULL, 0, '1 Year Provisional', NULL, '2019-02-07 02:02:45', '2019-02-07 02:02:45'),
(91, 91, 3, 8, 4, 1, 1, 0, NULL, '2018-07-11', '2019-07-10', NULL, 0, '1 Year Provisional', NULL, '2019-02-07 02:13:01', '2019-02-07 02:13:01');

-- --------------------------------------------------------

--
-- Table structure for table `applicationlevel_reminders`
--

CREATE TABLE `applicationlevel_reminders` (
  `id` int(10) UNSIGNED NOT NULL,
  `recdetails_id` int(10) UNSIGNED DEFAULT NULL,
  `deadline_reminder_1st` date DEFAULT NULL,
  `deadline_reminder_2nd` date DEFAULT NULL,
  `deadline_reminder_3rd` date DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `applicationlevel_reminders`
--

INSERT INTO `applicationlevel_reminders` (`id`, `recdetails_id`, `deadline_reminder_1st`, `deadline_reminder_2nd`, `deadline_reminder_3rd`, `status`) VALUES
(2, 2, NULL, NULL, NULL, NULL),
(3, 3, NULL, NULL, NULL, NULL),
(5, 5, NULL, NULL, NULL, NULL),
(6, 6, NULL, NULL, NULL, NULL),
(9, 9, NULL, NULL, NULL, NULL),
(10, 10, NULL, NULL, NULL, NULL),
(12, 12, NULL, NULL, NULL, NULL),
(13, 13, NULL, NULL, NULL, NULL),
(14, 14, NULL, NULL, NULL, NULL),
(15, 15, NULL, NULL, NULL, NULL),
(17, 17, NULL, NULL, NULL, NULL),
(18, 18, NULL, NULL, NULL, NULL),
(20, 20, NULL, NULL, NULL, NULL),
(21, 21, NULL, NULL, NULL, NULL),
(22, 22, NULL, NULL, NULL, NULL),
(23, 23, NULL, NULL, NULL, NULL),
(24, 24, NULL, NULL, NULL, NULL),
(25, 25, NULL, NULL, NULL, NULL),
(26, 26, NULL, NULL, NULL, NULL),
(27, 27, NULL, NULL, NULL, NULL),
(28, 28, NULL, NULL, NULL, NULL),
(29, 29, NULL, NULL, NULL, NULL),
(30, 30, NULL, NULL, NULL, NULL),
(31, 31, NULL, NULL, NULL, NULL),
(32, 32, NULL, NULL, NULL, NULL),
(33, 33, NULL, NULL, NULL, NULL),
(34, 34, NULL, NULL, NULL, NULL),
(35, 35, NULL, NULL, NULL, NULL),
(36, 36, NULL, NULL, NULL, NULL),
(37, 37, NULL, NULL, NULL, NULL),
(38, 38, NULL, NULL, NULL, NULL),
(39, 39, NULL, NULL, NULL, NULL),
(40, 40, NULL, NULL, NULL, NULL),
(41, 41, NULL, NULL, NULL, NULL),
(42, 42, NULL, NULL, NULL, NULL),
(43, 43, NULL, NULL, NULL, NULL),
(44, 44, NULL, NULL, NULL, NULL),
(45, 45, NULL, NULL, NULL, NULL),
(46, 46, NULL, NULL, NULL, NULL),
(47, 47, NULL, NULL, NULL, NULL),
(48, 48, NULL, NULL, NULL, NULL),
(49, 49, NULL, NULL, NULL, NULL),
(50, 50, NULL, NULL, NULL, NULL),
(51, 51, NULL, NULL, NULL, NULL),
(52, 52, NULL, NULL, NULL, NULL),
(53, 53, NULL, NULL, NULL, NULL),
(54, 54, NULL, NULL, NULL, NULL),
(55, 55, NULL, NULL, NULL, NULL),
(56, 56, NULL, NULL, NULL, NULL),
(57, 57, NULL, NULL, NULL, NULL),
(58, 58, NULL, NULL, NULL, NULL),
(59, 59, NULL, NULL, NULL, NULL),
(60, 60, NULL, NULL, NULL, NULL),
(61, 61, NULL, NULL, NULL, NULL),
(62, 62, NULL, NULL, NULL, NULL),
(63, 63, NULL, NULL, NULL, NULL),
(64, 64, NULL, NULL, NULL, NULL),
(65, 65, NULL, NULL, NULL, NULL),
(66, 66, NULL, NULL, NULL, NULL),
(67, 67, NULL, NULL, NULL, NULL),
(68, 68, NULL, NULL, NULL, NULL),
(69, 69, NULL, NULL, NULL, NULL),
(70, 70, NULL, NULL, NULL, NULL),
(71, 71, NULL, NULL, NULL, NULL),
(72, 72, NULL, NULL, NULL, NULL),
(73, 73, NULL, NULL, NULL, NULL),
(74, 74, NULL, NULL, NULL, NULL),
(75, 75, NULL, NULL, NULL, NULL),
(76, 76, NULL, NULL, NULL, NULL),
(77, 77, NULL, NULL, NULL, NULL),
(78, 78, NULL, NULL, NULL, NULL),
(79, 79, NULL, NULL, NULL, NULL),
(80, 80, NULL, NULL, NULL, NULL),
(81, 81, NULL, NULL, NULL, NULL),
(82, 82, NULL, NULL, NULL, NULL),
(83, 83, NULL, NULL, NULL, NULL),
(84, 84, NULL, NULL, NULL, NULL),
(85, 85, NULL, NULL, NULL, NULL),
(86, 86, NULL, NULL, NULL, NULL),
(87, 87, NULL, NULL, NULL, NULL),
(88, 88, NULL, NULL, NULL, NULL),
(89, 89, NULL, NULL, NULL, NULL),
(90, 90, NULL, NULL, NULL, NULL),
(91, 91, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `awardingletters`
--

CREATE TABLE `awardingletters` (
  `id` int(10) UNSIGNED NOT NULL,
  `a_letter_recdetails_id` int(10) UNSIGNED NOT NULL,
  `a_letter_saveduser_id` int(10) UNSIGNED NOT NULL,
  `a_letter_doctypes` int(10) UNSIGNED NOT NULL,
  `a_letter_years` int(10) UNSIGNED DEFAULT NULL,
  `a_letter_body` longtext COLLATE utf8mb4_unicode_ci,
  `a_letter_datesend` datetime DEFAULT NULL,
  `a_letter_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `chatmessages`
--

CREATE TABLE `chatmessages` (
  `id` int(10) UNSIGNED NOT NULL,
  `send_user_id` int(10) UNSIGNED NOT NULL,
  `reciever_user_id` int(10) UNSIGNED NOT NULL,
  `messages` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `seen_status` datetime DEFAULT NULL,
  `message_status` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `document_types`
--

CREATE TABLE `document_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `document_types_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `document_types`
--

INSERT INTO `document_types` (`id`, `document_types_name`) VALUES
(1, 'Application Letter'),
(2, 'Phreb Form No 1.1 (Application for Accredtation)'),
(3, 'Phreb Form No. 1.2 (Annual Report, for previous year)'),
(4, 'Phreb Form No. 1.3 (Protocol Summary, in the past year/s)'),
(5, 'Phreb Form No. 1.4 (Self-Assessment)'),
(6, 'Institutional Organization Chart showing the location of REC in relation to other institutional units'),
(7, 'Membership TOR and Appointment Letters'),
(8, 'REC Members and Staff Training Records'),
(9, 'REC Members and Staff CV'),
(10, 'Meeting Agenda and Minutes of the last three (3) meetings'),
(11, 'REC Office Picture'),
(12, 'SOP Manual'),
(13, 'SOP-related Forms and Letter Templates'),
(14, 'Accreditors Assessment'),
(15, 'Action Plan'),
(16, 'Compliance Evidences'),
(17, 'Initial Evaluation on Action Plan'),
(18, 'Final Evaluation on Action Plan'),
(19, 'Initial Evaluation on Compliance Evidence'),
(20, 'Final Evaluation on Compliance Evidence'),
(21, '1 Year Provisional'),
(22, '2 Years Extension of Accreditation'),
(23, '3 Years Accreditation'),
(24, '3 Years Full Accreditation'),
(25, '4 Years Full Accreditation'),
(26, 'Miscellaneous Files');

-- --------------------------------------------------------

--
-- Table structure for table `emaillogs`
--

CREATE TABLE `emaillogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `e_sender_user_id` int(10) UNSIGNED NOT NULL,
  `e_recdetails_id` int(10) UNSIGNED NOT NULL,
  `e_from` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `e_to` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `e_cc` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `e_bcc` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `e_subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `e_body` longtext COLLATE utf8mb4_unicode_ci,
  `e_status` int(11) NOT NULL DEFAULT '0',
  `e_created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `emaillogs`
--

INSERT INTO `emaillogs` (`id`, `e_sender_user_id`, `e_recdetails_id`, `e_from`, `e_to`, `e_cc`, `e_bcc`, `e_subject`, `e_body`, `e_status`, `e_created_at`) VALUES
(6, 1, 5, 'jrcambonga@pchrd.dost.gov.ph', 'clhrdc.region3@yahoo.com', '', 'phrebaccreditationsystem@gmail.com', 'PHREB Accreditation Expiry Reminder for Ethics Review Committee (ERC)-Central Luzon Health Research and Development Consortium', '\n                <p>Dear <b>Central Luzon Health Research and Development Consortium</b>,</p>\n                    <p>\n                        This is to remind you that the Expiration of Accreditation of <i>Ethics Review Committee (ERC) - Central Luzon Health Research and Development Consortium (Level 1)</i> is due on <b>May 29, 2020</b>. Thank you.\n                    </p>\n               <p>\n                    Respectfully,<br>\n                    <b>John Marc Cambonga</b><br>\n                    Secretariat\n               </p>\n            ', 0, '2019-01-17 09:24:28'),
(7, 1, 2, 'jrcambonga@pchrd.dost.gov.ph', 'abscabal@adamson.edu.ph', '', 'phrebaccreditationsystem@gmail.com', 'PHREB Accreditation Expiry Reminder for University Ethics Research Committee (UERC)-Adamson University (2017)', '\n                <p>Dear <b>Adamson University (2017)</b>,</p>\n                    <p>\n                        This is to remind you that the Expiration of Accreditation of <i>University Ethics Research Committee (UERC) - Adamson University (2017) (Level 1)</i> is due on <b>July 2, 2018</b>. Thank you.\n                    </p>\n               <p>\n                    Respectfully,<br>\n                    <b>John Marc Cambonga</b><br>\n                    Secretariat\n               </p>\n            ', 0, '2019-01-18 08:37:13'),
(8, 1, 2, 'jrcambonga@pchrd.dost.gov.ph', 'abscabal@adamson.edu.ph', '', 'phrebaccreditationsystem@gmail.com', 'PHREB Accreditation Expiry Reminder for University Ethics Research Committee (UERC)-Adamson University (2017)', '\n                <p>Dear <b>Adamson University (2017)</b>,</p>\n                    <p>\n                        This is to remind you that the Expiration of Accreditation of <i>University Ethics Research Committee (UERC) - Adamson University (2017) (Level 1)</i> is due on <b>July 2, 2018</b>. Thank you.\n                    </p>\n               <p>\n                    Respectfully,<br>\n                    <b>John Marc Cambonga</b><br>\n                    Secretariat\n               </p>\n            ', 0, '2019-01-18 08:38:19'),
(9, 1, 2, 'jrcambonga@pchrd.dost.gov.ph', 'abscabal@adamson.edu.ph', '', 'phrebaccreditationsystem@gmail.com', 'PHREB Accreditation Expiry Reminder for University Ethics Research Committee (UERC)-Adamson University (2017)', '\n                <p>Dear <b>Adamson University (2017)</b>,</p>\n                    <p>\n                        This is to remind you that the Expiration of Accreditation of <i>University Ethics Research Committee (UERC) - Adamson University (2017) (Level 1)</i> is due on <b>July 2, 2018</b>. Thank you.\n                    </p>\n               <p>\n                    Respectfully,<br>\n                    <b>John Marc Cambonga</b><br>\n                    Secretariat\n               </p>\n            ', 0, '2019-01-18 08:43:02'),
(10, 1, 2, 'jrcambonga@pchrd.dost.gov.ph', 'abscabal@adamson.edu.ph', '', 'phrebaccreditationsystem@gmail.com', 'PHREB Accreditation Expiry Reminder for University Ethics Research Committee (UERC)-Adamson University (2017)', '\n                <p>Dear <b>Adamson University (2017)</b>,</p>\n                    <p>\n                        This is to remind you that the Expiration of Accreditation of <i>University Ethics Research Committee (UERC) - Adamson University (2017) (Level 1)</i> is due on <b>July 2, 2018</b>. Thank you.\n                    </p>\n               <p>\n                    Respectfully,<br>\n                    <b>John Marc Cambonga</b><br>\n                    Secretariat\n               </p>\n            ', 0, '2019-01-18 08:44:21'),
(11, 1, 2, 'jrcambonga@pchrd.dost.gov.ph', 'abscabal@adamson.edu.ph', '', 'phrebaccreditationsystem@gmail.com', 'PHREB Accreditation Expiry Reminder for University Ethics Research Committee (UERC)-Adamson University (2017)', '\n                <p>Dear <b>Adamson University (2017)</b>,</p>\n                    <p>\n                        This is to remind you that the Expiration of Accreditation of <i>University Ethics Research Committee (UERC) - Adamson University (2017) (Level 1)</i> is due on <b>July 2, 2018</b>. Thank you.\n                    </p>\n               <p>\n                    Respectfully,<br>\n                    <b>John Marc Cambonga</b><br>\n                    Secretariat\n               </p>\n            ', 0, '2019-01-18 08:48:09'),
(12, 1, 2, 'jrcambonga@pchrd.dost.gov.ph', 'abscabal@adamson.edu.ph', '', 'phrebaccreditationsystem@gmail.com', 'PHREB Accreditation Expiry Reminder for University Ethics Research Committee (UERC)-Adamson University (2017)', '\n                <p>Dear <b>Adamson University (2017)</b>,</p>\n                    <p>\n                        This is to remind you that the Expiration of Accreditation of <i>University Ethics Research Committee (UERC) - Adamson University (2017) (Level 1)</i> is due on <b>July 2, 2018</b>. Thank you.\n                    </p>\n               <p>\n                    Respectfully,<br>\n                    <b>John Marc Cambonga</b><br>\n                    Secretariat\n               </p>\n            ', 0, '2019-01-18 08:49:02'),
(13, 1, 2, 'jrcambonga@pchrd.dost.gov.ph', 'abscabal@adamson.edu.ph', '', 'phrebaccreditationsystem@gmail.com', 'PHREB Accreditation Expiry Reminder for University Ethics Research Committee (UERC)-Adamson University (2017)', '\n                <p>Dear <b>Adamson University (2017)</b>,</p>\n                    <p>\n                        This is to remind you that the Expiration of Accreditation of <i>University Ethics Research Committee (UERC) - Adamson University (2017) (Level 1)</i> is due on <b>July 2, 2018</b>. Thank you.\n                    </p>\n               <p>\n                    Respectfully,<br>\n                    <b>John Marc Cambonga</b><br>\n                    Secretariat\n               </p>\n            ', 0, '2019-01-18 08:50:31'),
(14, 1, 2, 'jrcambonga@pchrd.dost.gov.ph', 'abscabal@adamson.edu.ph', '', 'phrebaccreditationsystem@gmail.com', 'PHREB Accreditation Expiry Reminder for University Ethics Research Committee (UERC)-Adamson University (2017)', '\n                <p>Dear <b>Adamson University (2017)</b>,</p>\n                    <p>\n                        This is to remind you that the Expiration of Accreditation of <i>University Ethics Research Committee (UERC) - Adamson University (2017) (Level 1)</i> is due on <b>July 2, 2018</b>. Thank you.\n                    </p>\n               <p>\n                    Respectfully,<br>\n                    <b>John Marc Cambonga</b><br>\n                    Secretariat\n               </p>\n            ', 0, '2019-01-18 08:52:50'),
(15, 1, 2, 'jrcambonga@pchrd.dost.gov.ph', 'abscabal@adamson.edu.ph', '', 'phrebaccreditationsystem@gmail.com', 'PHREB Accreditation Expiry Reminder for University Ethics Research Committee (UERC)-Adamson University (2017)', '\n                <p>Dear <b>Adamson University (2017)</b>,</p>\n                    <p>\n                        This is to remind you that the Expiration of Accreditation of <i>University Ethics Research Committee (UERC) - Adamson University (2017) (Level 1)</i> is due on <b>July 2, 2018</b>. Thank you.\n                    </p>\n               <p>\n                    Respectfully,<br>\n                    <b>John Marc Cambonga</b><br>\n                    Secretariat\n               </p>\n            ', 0, '2019-01-18 08:55:37'),
(16, 1, 2, 'jrcambonga@pchrd.dost.gov.ph', 'abscabal@adamson.edu.ph', '', 'phrebaccreditationsystem@gmail.com', 'PHREB Accreditation Expiry Reminder for University Ethics Research Committee (UERC)-Adamson University (2017)', '\n                <p>Dear <b>Adamson University (2017)</b>,</p>\n                    <p>\n                        This is to remind you that the Expiration of Accreditation of <i>University Ethics Research Committee (UERC) - Adamson University (2017) (Level 1)</i> is due on <b>July 2, 2018</b>. Thank you.\n                    </p>\n               <p>\n                    Respectfully,<br>\n                    <b>John Marc Cambonga</b><br>\n                    Secretariat\n               </p>\n            ', 0, '2019-01-18 08:56:24'),
(17, 1, 2, 'jrcambonga@pchrd.dost.gov.ph', 'abscabal@adamson.edu.ph', '', 'phrebaccreditationsystem@gmail.com', 'PHREB Accreditation Expiry Reminder for University Ethics Research Committee (UERC)-Adamson University (2017)', '\n                <p>Dear <b>Adamson University (2017)</b>,</p>\n                    <p>\n                        This is to remind you that the Expiration of Accreditation of <i>University Ethics Research Committee (UERC) - Adamson University (2017) (Level 1)</i> is due on <b>July 2, 2018</b>. Thank you.\n                    </p>\n               <p>\n                    Respectfully,<br>\n                    <b>John Marc Cambonga</b><br>\n                    Secretariat\n               </p>\n            ', 0, '2019-01-18 09:05:52'),
(18, 1, 2, 'jrcambonga@pchrd.dost.gov.ph', 'abscabal@adamson.edu.ph', '', 'phrebaccreditationsystem@gmail.com', 'PHREB Accreditation Expiry Reminder for University Ethics Research Committee (UERC)-Adamson University (2017)', '\n                <p>Dear <b>Adamson University (2017)</b>,</p>\n                    <p>\n                        This is to remind you that the Expiration of Accreditation of <i>University Ethics Research Committee (UERC) - Adamson University (2017) (Level 1)</i> is due on <b>July 2, 2018</b>. Thank you.\n                    </p>\n               <p>\n                    Respectfully,<br>\n                    <b>John Marc Cambonga</b><br>\n                    Secretariat\n               </p>\n            ', 0, '2019-01-18 09:17:35'),
(19, 1, 2, 'jrcambonga@pchrd.dost.gov.ph', 'abscabal@adamson.edu.ph', '', 'phrebaccreditationsystem@gmail.com', 'PHREB Accreditation Expiry Reminder for University Ethics Research Committee (UERC)-Adamson University (2017)', '\n                <p>Dear <b>Adamson University (2017)</b>,</p>\n                    <p>\n                        This is to remind you that the Expiration of Accreditation of <i>University Ethics Research Committee (UERC) - Adamson University (2017) (Level 1)</i> is due on <b>July 2, 2018</b>. Thank you.\n                    </p>\n               <p>\n                    Respectfully,<br>\n                    <b>John Marc Cambonga</b><br>\n                    Secretariat\n               </p>\n            ', 0, '2019-01-18 09:18:33'),
(20, 1, 2, 'jrcambonga@pchrd.dost.gov.ph', 'abscabal@adamson.edu.ph', '', 'phrebaccreditationsystem@gmail.com', 'PHREB Accreditation Expiry Reminder for University Ethics Research Committee (UERC)-Adamson University (2017)', '\n                <p>Dear <b>Adamson University (2017)</b>,</p>\n                    <p>\n                        This is to remind you that the Expiration of Accreditation of <i>University Ethics Research Committee (UERC) - Adamson University (2017) (Level 1)</i> is due on <b>July 2, 2018</b>. Thank you.\n                    </p>\n               <p>\n                    Respectfully,<br>\n                    <b>John Marc Cambonga</b><br>\n                    Secretariat\n               </p>\n            ', 0, '2019-01-18 09:20:29'),
(21, 1, 2, 'jrcambonga@pchrd.dost.gov.ph', 'abscabal@adamson.edu.ph', '', 'phrebaccreditationsystem@gmail.com', 'PHREB Accreditation Expiry Reminder for University Ethics Research Committee (UERC)-Adamson University (2017)', '\n                <p>Dear <b>Adamson University (2017)</b>,</p>\n                    <p>\n                        This is to remind you that the Expiration of Accreditation of <i>University Ethics Research Committee (UERC) - Adamson University (2017) (Level 1)</i> is due on <b>July 2, 2018</b>. Thank you.\n                    </p>\n               <p>\n                    Respectfully,<br>\n                    <b>John Marc Cambonga</b><br>\n                    Secretariat\n               </p>\n            ', 0, '2019-01-18 09:21:29'),
(22, 1, 2, 'jrcambonga@pchrd.dost.gov.ph', 'abscabal@adamson.edu.ph', '', 'phrebaccreditationsystem@gmail.com', 'PHREB Accreditation Expiry Reminder for University Ethics Research Committee (UERC)-Adamson University (2017)', '\n                <p>Dear <b>Adamson University (2017)</b>,</p>\n                    <p>\n                        This is to remind you that the Expiration of Accreditation of <i>University Ethics Research Committee (UERC) - Adamson University (2017) (Level 1)</i> is due on <b>July 2, 2018</b>. Thank you.\n                    </p>\n               <p>\n                    Respectfully,<br>\n                    <b>John Marc Cambonga</b><br>\n                    Secretariat\n               </p>\n            ', 0, '2019-01-18 09:24:15'),
(23, 1, 2, 'jrcambonga@pchrd.dost.gov.ph', 'abscabal@adamson.edu.ph', '', 'phrebaccreditationsystem@gmail.com', 'PHREB Accreditation Expiry Reminder for University Ethics Research Committee (UERC)-Adamson University (2017)', '\n                <p>Dear <b>Adamson University (2017)</b>,</p>\n                    <p>\n                        This is to remind you that the Expiration of Accreditation of <i>University Ethics Research Committee (UERC) - Adamson University (2017) (Level 1)</i> is due on <b>July 2, 2018</b>. Thank you.\n                    </p>\n               <p>\n                    Respectfully,<br>\n                    <b>John Marc Cambonga</b><br>\n                    Secretariat\n               </p>\n            ', 0, '2019-01-18 09:24:37'),
(24, 1, 2, 'jrcambonga@pchrd.dost.gov.ph', 'abscabal@adamson.edu.ph', '', 'phrebaccreditationsystem@gmail.com', 'PHREB Accreditation Expiry Reminder for University Ethics Research Committee (UERC)-Adamson University (2017)', '\n                <p>Dear <b>Adamson University (2017)</b>,</p>\n                    <p>\n                        This is to remind you that the Expiration of Accreditation of <i>University Ethics Research Committee (UERC) - Adamson University (2017) (Level 1)</i> is due on <b>July 2, 2018</b>. Thank you.\n                    </p>\n               <p>\n                    Respectfully,<br>\n                    <b>John Marc Cambonga</b><br>\n                    Secretariat\n               </p>\n            ', 0, '2019-01-18 09:24:39'),
(25, 1, 2, 'jrcambonga@pchrd.dost.gov.ph', 'abscabal@adamson.edu.ph', '', 'phrebaccreditationsystem@gmail.com', 'PHREB Accreditation Expiry Reminder for University Ethics Research Committee (UERC)-Adamson University (2017)', '\n                <p>Dear <b>Adamson University (2017)</b>,</p>\n                    <p>\n                        This is to remind you that the Expiration of Accreditation of <i>University Ethics Research Committee (UERC) - Adamson University (2017) (Level 1)</i> is due on <b>July 2, 2018</b>. Thank you.\n                    </p>\n               <p>\n                    Respectfully,<br>\n                    <b>John Marc Cambonga</b><br>\n                    Secretariat\n               </p>\n            ', 0, '2019-01-18 09:24:40'),
(26, 1, 2, 'jrcambonga@pchrd.dost.gov.ph', 'abscabal@adamson.edu.ph', '', 'phrebaccreditationsystem@gmail.com', 'PHREB Accreditation Expiry Reminder for University Ethics Research Committee (UERC)-Adamson University (2017)', '\n                <p>Dear <b>Adamson University (2017)</b>,</p>\n                    <p>\n                        This is to remind you that the Expiration of Accreditation of <i>University Ethics Research Committee (UERC) - Adamson University (2017) (Level 1)</i> is due on <b>July 2, 2018</b>. Thank you.\n                    </p>\n               <p>\n                    Respectfully,<br>\n                    <b>John Marc Cambonga</b><br>\n                    Secretariat\n               </p>\n            ', 0, '2019-01-18 09:26:42'),
(27, 1, 2, 'jrcambonga@pchrd.dost.gov.ph', 'abscabal@adamson.edu.ph', '', 'phrebaccreditationsystem@gmail.com', 'PHREB Accreditation Expiry Reminder for University Ethics Research Committee (UERC)-Adamson University (2017)', '\n                <p>Dear <b>Adamson University (2017)</b>,</p>\n                    <p>\n                        This is to remind you that the Expiration of Accreditation of <i>University Ethics Research Committee (UERC) - Adamson University (2017) (Level 1)</i> is due on <b>July 2, 2018</b>. Thank you.\n                    </p>\n               <p>\n                    Respectfully,<br>\n                    <b>John Marc Cambonga</b><br>\n                    Secretariat\n               </p>\n            ', 0, '2019-01-18 09:27:49'),
(28, 1, 2, 'jrcambonga@pchrd.dost.gov.ph', 'abscabal@adamson.edu.ph', '', 'phrebaccreditationsystem@gmail.com', 'PHREB Accreditation Expiry Reminder for University Ethics Research Committee (UERC)-Adamson University (2017)', '\n                <p>Dear <b>Adamson University (2017)</b>,</p>\n                    <p>\n                        This is to remind you that the Expiration of Accreditation of <i>University Ethics Research Committee (UERC) - Adamson University (2017) (Level 1)</i> is due on <b>July 2, 2018</b>. Thank you.\n                    </p>\n               <p>\n                    Respectfully,<br>\n                    <b>John Marc Cambonga</b><br>\n                    Secretariat\n               </p>\n            ', 0, '2019-01-24 07:58:55');

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE `email_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `email_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_body` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `email_templates`
--

INSERT INTO `email_templates` (`id`, `email_name`, `email_subject`, `email_body`) VALUES
(1, 'New Application ', 'New Application', '\n        		<p>Dear PHREB Secretariat, </p>\n				<p>$rec_name has applied for $level_name through the PHREB Online Accreditation System. Please see application details below:<br><br>\n						REC NAME: <b>$rec_name</b><br>\n						INSTITUTION: <b>$rec_institution</b><br>\n						ADDRESS: <b>$rec_address</b><br>\n						LEVEL: <b>$level_name</b><br>\n						REGION: <b>$region_name</b><br>\n						STATUS: <b>$status_name</b><br>\n						DATE SUBMITTED: <b>$created_at</b><br>\n				</p>	\n        	'),
(2, 'Incomplete', 'Incomplete', '\n        		<p>Dear <b>$rec_name</b>,</p>\n					<p>\n						Your <b>$level_name</b> application has been tagged as <b><i style=\\\"color: brown;\\\">$status_name</i></b> in Online Accreditation System. <b>Kindly complete all the requirements on or before $deadline</b>.\n					</p>\n        	'),
(3, 'Re-submission', 'Re-submission', '\n        		<p>Dear <b>$rec_name</b>,</p>\n					<p>\n						Your <b>$level_name</b> application has been tagged as <b><i style=\\\"color: brown;\\\">$status_name</i></b> in Online Accreditation System. <b>Kindly furnish PHREB with all the deficiencies on or before $deadline</b>.\n					</p>\n        	'),
(4, 'Complete ', 'Complete', '\n        		<p>Dear <b>$rec_name</b>,</p>\n					<p>\n						Your <b>$level_name</b> application has been tagged as <b><i style=\\\"color: brown;\\\">$status_name</i></b> in Online Accreditation System. <b>Kindly submit two (2) sets of hard copies of your application on or before $deadline. Also please ensure to pay the accreditation fee of Php 3,000 either in cash or cheque</b> (payable to <b>Philippine Council for Health Research and Development</b>).\n					</p>\n				<p>Regards,<br>\n				$sender<br>\n				PHREB Secretariat</p>\n				<p>_____________________</p>\n				<p><small>*Requirements for Hardbound:<br>\n				<i>Font: Times New Roman<br>\n				   Font size: 12<br>\n				   Must be labelled and bound accordingly\n				</i>\n				</small></p>\n        	'),
(5, 'Re-submit Requirements ', 'Resubmission of Requirements', '\n        		<p>Dear <b>PHREB Secretariat</b>,</p>\n					<p>\n						The $rec_name has submitted the following requirements to the PHREB Online Accreditation System. Please review the following requirements by clicking the link below.\n					</p>\n        	'),
(6, 'Invitation to be Accreditor', 'Invitation to be Accreditor', '\n        		Dear <b>$name ,</b>\n            		<p>\n            		The <b>$rec_name</b> has applied for a $level_name Accreditation ($status_name). With this, may we respectfully ask if you can be the accreditor/reviewer for this REC?\n            		</p>\n            <p>Thank you very much and we are looking forward to your response. </p><br>\n            <p>Respectfully,<br>\n            <b>$secname</b><br>\n            <b>Secretariat</b> <br><br>\n        	'),
(7, 'Accreditors Assessment Evaluation ', 'Accreditors Assessment Evaluation ', '\n        		<p>\n                Mr/Ms <b>$acc_name</b> ($role_name) has uploaded his/her assessment on $rec_name applied for Level $level_id ($status_name) on the PHREB Online Accreditation System.  \n                </p>\n        	'),
(8, 'Assessment - Evaluation of Accreditors', 'Assessment - Evaluation of Accreditors', '\n        		<p>Good Day!</p>\n        		<p>\n        			 Ms/Mr $acc_name (Accreditor) have uploaded his/her Assessment on <b>$rec_name($status_name) applied for Level $level_id</b> to the PHREB Online Accreditation System. Please click the button below to view and download accreditors evaluation.<br>\n        		</p>\n        		<p>Thank you. <br><br>\n\n        		Respectfully,<br>\n\n        		<b>\n        		$secname <br>\n        		PHREB Secretariat	\n        		</b>\n\n        		</p> \n        	'),
(9, 'Reminder for Evaluation of Accreditation Application', 'Reminder for Evaluation of Accreditation Application', '\n                Dear <b>$name</b>,<br>\n                <p> \n                We would like to remind you regarding the submission of your assessment on the Level $level_id application of <b>$rec_name</b> due on <b>$expirydate</b>.\n                </p> \n                <p>\n                Respectfully,<br>\n                PHREB Online Accreditation System</b>\n                </p>\n            '),
(10, 'Deadline of Submission of Final Assessment', 'Deadline of Submission of Final Assessment', '\n                Dear <b>$name</b>,<br>\n                <p> \n                    May we  remind you that the deadline for the release of the  final assessment on <b>$rec_name Level $level_id <i>($status_name)</i></b> accreditation application is due this <b>$deadline</b>. May we respectfully request for the final assessment to be submitted on or before the said deadline. Thank you. \n                </p> \n                <p>\n                Respectfully,<br>\n                PHREB Online Accreditation System</b>\n                </p>\n            '),
(11, 'Dealine of REC Action Plan', 'Deadline of REC Action Plan', '\n                Dear <b>$rec_chairname</b>,<br>\n                <p> \n                    This is to remind you that the deadline for the submission of Action Plan is on <b>$deadline</b>. Thank you.\n                </p> \n                <p>\n                Regards,<br>\n                PHREB Online Accreditation System</b>\n                </p>\n            '),
(12, 'Dealine of REC Compliance Evidences', 'Dealine of REC Compliance Evidences', '\n                Dear <b>$rec_chairname</b>,<br>\n                <p> \n                    This is to remind you that the deadline for the submission of your Compliance Evidences is on <b>$deadline</b>. Please submit your documents/requirements on or before the said deadline. Thank you.\n                </p> \n                <p>\n                Regards,<br>\n                PHREB Online Accreditation System</b>\n                </p>\n            '),
(13, 'REC Order of Payment', 'REC Order of Payment', '\n                Dear <b>$rec_chairname</b>,<br>\n                <p> \n                   Please see the attached <b>Order of Payment</b>. Thank you!\n                </p> \n                <p>\n                Best,<br><br>\n\n                <b>$secname</b><br>\n                PHREB Secretariat</b>\n                </p>\n            '),
(14, 'REC Action Plan and Compliance Evidences', 'REC Action Plan and Compliance Evidences', '\n                <p><b>$date</b></p>\n                <p>Dear <b>PHREB</b>,</p>\n    \n                <p>The REC <b>$rec_name - Level $level_id</b> has uploaded their $doctype_name to the PCHRD PHREB Online Accreditation System. Please see the attached files below. Thank you.\n                </p>\n            '),
(15, 'Accreditors/CSA Chairs Action Plan and Compliance Evidences', 'Accreditors/CSA Chairs Action Plan and Compliance Evidences', '\n                <p><b>$date</b></p>\n                <p>Dear <b>PHREB</b>,</p>\n    \n                <p>The $role_name - <b>$name</b> has uploaded his/her $doctype_name for the REC <b>$rec_name</b>. Please see the attached files below. Thank you.\n                </p>\n            '),
(16, 'Forwarding Accreditors Evaluation to CSA Chair', 'Forwarding Accreditors Evaluation to CSA Chair', '\n                <p>$date<br>\n                $csachair<br>\n                CSA Chair<br>\n                </p>\n\n                <p>Dear <b>$csachair</b>,</p>\n                <p>\n                    Please see the attached file of <i>$doctype_name</i> of <b>$rec_name - level $level_id</b> ($status_name). Sent from Accreditor $accname. Thank you. \n                </p>\n\n                <p>\n                    Regards,<br>\n                    <b>$secname</b><br>\n                    PHREB Secretariat\n                </p>\n            '),
(17, 'Forwarding Accreditors Evaluation to REC', 'Forwarding Accreditors Evaluation to REC', '\n                <p>$date<br>\n                $rec_name<br> \n                $rec_address<br>\n                $rec_chairname<br>\n                Chair\n                </p>\n\n                <p>Dear <b>$rec_chairname</b>,<br>\n                    Please see the attached files of $doctype_name on <b>$rec_name - Level $level_id</b> ($status_name). Thank you. \n                </p>\n\n                <p>\n                    Regards,<br>\n                    <b>$secname</b><br>\n                    PHREB Secretariat\n                </p>\n            '),
(18, 'Official Receipt and Order Payment', 'Official Receipt and Order Payment', '\n                <p>$date<br>\n                $rec_name<br> \n                $rec_address<br>\n                $rec_chairname<br>\n                Chair\n                </p><br>\n\n                <p>Dear <b>$rec_chairname</b>,<br>\n                    Forwarding you the soft copy of your OR and Order of Payment for your <b>$rec_name- Level $level_id</b> Accreditation Application. We have sent the hard copies through LIBCAP. Kindly expect for these to arrive by the end of the week. Thank you. \n                </p>\n\n                <p>\n                    Respectfully,<br>\n                    <b>$secname</b><br>\n                    PHREB Secretariat\n                </p>\n            '),
(19, 'Awarding Letter', 'Awarding Letter - Congratulations, ', '\n                <p>Dear <b>$rec_chairname</b>, <br><br>\n                Please see the attached letter. Kindly acknowledge receipt. Thank you.<br>\n                <p style=\"color: blue;\">Congratulations and warm regards.</p>\n                </p> \n                <b>$secname</b>,<br>\n                Secretariat\n            '),
(20, 'Accredited', 'Accredited', '\n                <p>Dear <b>$rec_name</b>,</p>\n                    <p>\n                        Your <b>$level_name</b> application has been tagged as <b><i style=\\\"color: brown;\\\">$status_name</i></b>  at <b>$date_accreditation </b> in PHREB Online Accreditation System.</b>. Please click below to see status of your application. Thank you.\n                    </p>\n            '),
(21, 'PHREB Accreditation System - New User', 'New User Registration in PHREB - Accreditation System', '\n                <p>Dear PHREB,</p>\n                    <p>\n                        Congratulations! New user has successfully registered as verified in PHREB - Accreditation System. \n                    </p>\n                <h4>User Details</h4>\n                <p>\n                NAME: <b>$user</b><br>\n                EMAIL: <b>$email</b><br>\n                ROLE: <b>$role</b><br>\n                DATE: <b>$date</b></br>\n                </p>\n            '),
(22, 'PHREB Annual Report', 'PHREB Acreditation Annual Report', '\n                <p>Dear PHREB,</p>\n                    <p>\n                        The $rec_name ($level_name) has uploaded the $document_types <i>($ar_name)</i> in the system. Thank you.\n                    </p>\n                </p>\n            '),
(23, 'PHREB Annual Report & Protocol Summary', 'Reminders for PHREB Annual Report & Protocol Summary', '\n                <p>Dear <b>$rec_institution</b>,</p><br>\n                    <p>\n                        This is to remind you that the deadline of the Annual report and Protocol Summary of <i>$rec_name - $rec_institution ($level) </i> for year <b>$year</b> is on <b>$deadline</b>. Please submit the following requirements on or before the said deadline. Thank you.\n                    </p><br>\n               <p>\n                    Respectfully,<br>\n                    <b>PHREB</b><br>\n               </p>\n            '),
(24, 'PHREB Accreditation Reminder', 'PHREB Accreditation Expiry Reminder', '\n                <p>Dear <b>$rec_institution</b>,</p>\n                    <p>\n                        This is to remind you that the Expiration of Accreditation of <i>$rec_name - $rec_institution ($level)</i> is due on <b>$date_accreditation_expiry</b>. Thank you.\n                    </p>\n               <p>\n                    Respectfully,<br>\n                    <b>$secname</b><br>\n                    Secretariat\n               </p>\n            '),
(25, 'PHREB Accreditation Reminder', 'PHREB Expiration of Accreditation', '\n                <p>Dear <b>$rec_institution</b>,</p>\n                    <p>\n                        This is to remind you that the Expiration of Accreditation of <i>$rec_name - $rec_institution ($level)</i> is due on <b>$date_accreditation_expiry</b>. Thank you.\n                    </p>\n               <p>\n                    Respectfully,<br>\n                    <b>PHREB Accreditation</b><br>\n               </p>\n            ');

-- --------------------------------------------------------

--
-- Table structure for table `esigs`
--

CREATE TABLE `esigs` (
  `id` int(10) UNSIGNED NOT NULL,
  `esig_user_id` int(10) UNSIGNED NOT NULL,
  `esig_addedby` int(10) UNSIGNED NOT NULL,
  `esig_file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `esig_file_generated` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `esig_datecreated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `existing_arps`
--

CREATE TABLE `existing_arps` (
  `id` int(10) UNSIGNED NOT NULL,
  `recdetails_id` int(10) UNSIGNED NOT NULL,
  `selectarps` int(10) UNSIGNED NOT NULL,
  `foryear` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `arps_submission` date NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_uniqid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_extension` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `existing_arps`
--

INSERT INTO `existing_arps` (`id`, `recdetails_id`, `selectarps`, `foryear`, `arps_submission`, `file`, `file_uniqid`, `file_extension`, `created_at`, `updated_at`) VALUES
(6, 6, 3, '2017', '2018-04-11', NULL, NULL, NULL, '2019-01-16 08:23:49', '2019-01-16 08:23:49'),
(7, 6, 4, '2017', '2018-04-11', NULL, NULL, NULL, '2019-01-16 08:24:15', '2019-01-16 08:24:15'),
(10, 33, 3, '2017', '2018-01-11', 'Eastern Visayas Regional Medical Center.zip', '5c48180d0a655', 'zip', '2019-01-23 07:30:21', '2019-01-23 07:30:21'),
(11, 3, 3, '2017', '2018-01-31', 'PHREB Form 1.2_Annual Report 2017_AU.doc', '5c490607058c3', 'doc', '2019-01-24 00:25:43', '2019-01-24 00:25:43'),
(12, 3, 4, '2017', '2918-01-31', 'PHREB Form 1.3_Protocol Summary 2017_AU.doc', '5c49066b77f60', 'doc', '2019-01-24 00:27:23', '2019-01-24 00:27:23'),
(13, 5, 3, '2017', '2018-01-22', 'PHREB Form No. 1.2 Annual Report 2017_CLHRDC.pdf', '5c4907d347980', 'pdf', '2019-01-24 00:33:23', '2019-01-24 00:33:23'),
(14, 5, 4, '2017', '2018-01-22', 'PHREB Form No. 1.3 Protocol Summary 2017_CLHRDC.pdf', '5c4909d5af92e', 'pdf', '2019-01-24 00:41:57', '2019-01-24 00:41:57'),
(15, 2, 3, '2017', '2018-01-31', 'PHREB Form No. 1.2 Annual Report 2017_UERC.zip', '5c490ee42a703', 'zip', '2019-01-24 01:03:32', '2019-01-24 01:03:32'),
(16, 2, 4, '2017', '2018-01-31', 'PHREB Form No. 1.3 Protocol Summary 2017_UERC.pdf', '5c491071a85f9', 'pdf', '2019-01-24 01:10:09', '2019-01-24 01:10:09'),
(17, 18, 3, '2017', '2018-01-19', 'PHREB Form No. 1.2 Annual Report 2017_USA RERC.pdf', '5c4911809b9d1', 'pdf', '2019-01-24 01:14:40', '2019-01-24 01:14:40'),
(18, 18, 4, '2017', '2018-01-19', 'PHREB Form No. 1.3 Protocol Summary 2017_USA RERC.pdf', '5c4911a72a2f3', 'pdf', '2019-01-24 01:15:19', '2019-01-24 01:15:19'),
(19, 25, 3, '2017', '2018-01-31', 'PHREB Forms No. 1.2 and 1.3 Annual Report and Protocol Summary 2017_BMC RERC.pdf', '5c491f5c13ae1', 'pdf', '2019-01-24 02:13:48', '2019-01-24 02:13:48'),
(20, 23, 3, '2017', '2018-01-31', 'PHREB Form No. 1.2 Annual Report 2017_DLSU REC.pdf', '5c49211b47dad', 'pdf', '2019-01-24 02:21:15', '2019-01-24 02:21:15'),
(21, 23, 4, '2017', '2018-01-31', 'PHREB Form No. 1.3 Protocol Summary 2017_DLSU REC.docx', '5c492135ed910', 'docx', '2019-01-24 02:21:41', '2019-01-24 02:21:41'),
(22, 29, 3, '2017', '2018-01-30', 'PHREB Form No. 1.2 Annual Report 2017_DMSFH REC.zip', '5c4926ee38139', 'zip', '2019-01-24 02:46:06', '2019-01-24 02:46:06'),
(23, 29, 4, '2017', '2018-01-03', 'PHREB Form No. 1.3 Protocol Summary 2017_DMSFH REC.zip', '5c4927af83bf0', 'zip', '2019-01-24 02:49:19', '2019-01-24 02:49:19'),
(24, 39, 3, '2017', '2018-01-18', 'PHREB Form No. 1.2 Annual Report 2017_OLFU IERC.pdf', '5c492ab62713f', 'pdf', '2019-01-24 03:02:14', '2019-01-24 03:02:14'),
(25, 39, 4, '2017', '2018-01-18', 'PHREB Form No. 1.3 Protocol Summary 2017_OLFU IERC.pdf', '5c492ad289998', 'pdf', '2019-01-24 03:02:42', '2019-01-24 03:02:42'),
(26, 24, 3, '2017', '2018-02-12', 'PHREB Form No. 1.2 Annual Report 2017_VRH IRB.zip', '5c492e783ba71', 'zip', '2019-01-24 03:18:16', '2019-01-24 03:18:16'),
(27, 24, 4, '2017', '2018-02-12', 'PHREB Form No. 1.3 Protocol Summary 2017_VRH IRB.docx', '5c492e8fd39e6', 'docx', '2019-01-24 03:18:39', '2019-01-24 03:18:39'),
(28, 27, 3, '2017', '2018-01-10', 'PHREB Form No. 1.2 Annual Report 2017_WMSU REOC.docx', '5c492fb807394', 'docx', '2019-01-24 03:23:36', '2019-01-24 03:23:36'),
(29, 28, 3, '2017', '2017-12-19', 'PHREB Form No. 1.2 Annual Report 2017_ZCMC ERB.pdf', '5c4930a60de51', 'pdf', '2019-01-24 03:27:34', '2019-01-24 03:27:34'),
(30, 20, 3, '2017', '2018-01-15', 'PHREB Form No. 1.2 Annual Report 2017_UST-CRS ERC.pdf', '5c49334616575', 'pdf', '2019-01-24 03:38:46', '2019-01-24 03:38:46'),
(31, 20, 4, '2017', '2018-01-15', 'PHREB Form No. 1.3 Protocol Summary 2017_UST-CRS ERC.pdf', '5c493359e0080', 'pdf', '2019-01-24 03:39:05', '2019-01-24 03:39:05');

-- --------------------------------------------------------

--
-- Table structure for table `existing_recs`
--

CREATE TABLE `existing_recs` (
  `id` int(10) UNSIGNED NOT NULL,
  `recdetails_id` int(10) UNSIGNED NOT NULL,
  `name_of_accreditors` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_req_deck_accreditors` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_rec_received_awarding_letter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_deadline_accreditors_ass` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_accreditors_send_ass` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_rec_recieved_acc_ass` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_rec_sent_apce` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_accreditors_sent_apce` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_forward_to_csachair` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `existingrec_remarks` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `existing_recs`
--

INSERT INTO `existing_recs` (`id`, `recdetails_id`, `name_of_accreditors`, `date_req_deck_accreditors`, `date_rec_received_awarding_letter`, `date_deadline_accreditors_ass`, `date_accreditors_send_ass`, `date_rec_recieved_acc_ass`, `date_rec_sent_apce`, `date_accreditors_sent_apce`, `date_forward_to_csachair`, `existingrec_remarks`) VALUES
(2, 2, 'Dr. Evangeline Santos', NULL, '2017-07-27', NULL, NULL, NULL, NULL, NULL, NULL, '1) new composition of AdZU UREC members (non-affiliated), with supporting documents of appointment\r\n2) minutes of REC meeting for August, September and October with full-board discussion of protocols\r\n3) photo of REC office showing telephone line and cabinet bearing label outside and the protocol filing inside\r\n4) revised SOP reflecting CSA recommendations, and 5) Action Plan and evidence of compliance for the requested documents'),
(3, 3, '', NULL, '2017-03-01', NULL, NULL, NULL, NULL, NULL, NULL, '1) Picture of collection of ethical guidelines, \r\n2) copy of program/s, date and venue and copy of attendance sheet of trainers and trainers\' credentials, \r\n3) complete copy of revised SOP with the forms,\r\n 4) copy of 2 recent letters to the researchers regarding approval of their research, \r\n5) minutes of 2 most recent meetings, with discussion of review of at least 1 new protocol, and \r\n6) pictures of the contents of 2 protocol files'),
(4, 5, 'Dr. Sonia Bongala', NULL, NULL, NULL, NULL, 'September 9, 2016', 'November 11, 2016', 'February 2, 2017', NULL, '1) revised SOP, a) Item 3.1 Review of Progress, Final, and Early Termination Reports and Protocol Amendments, adding Step 7: Filling of all related documents, and b) Item 3.2 Review of Protocol Deviations and Violations, changing the Step 7 to Step 8'),
(5, 6, 'Dr. Rhodora Estacio', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1) copy of minutes of 2 most recent full board REC meetings, reflecting post approval review of resubmissions, amendments, progress reports, and final reports'),
(8, 9, 'Dr. Ma. Salome Vios, Dr. Sonia Bongala', 'June 29 2016', '2017-08-30', NULL, 'August 20, 2017', 'August 22, 2016', 'September 21, 2016', NULL, NULL, '1) revised SOP, reflecting CSA recommendations, and 2) copy of minutes of REC meeting for the period of September 2017 - July 2018'),
(9, 10, 'Dr. Angelica Francisco, Dr. Gemma N. Balein', 'February 02 2018', 'July 6, 2018', 'February 22, 2018', 'March 6, 2018', 'March 19, 2018', 'April 25, 2018', 'April 25, 2018', 'May 15, 2018', 'March 6 2018 (GNB); March 7 2018 (ADF)- Date phreb sec. received Accreditors Assessment\r\nMay 15 2018 - (ADF) | May 23, 2018 - GNB- Date of PHREB sec. received accreditors evaluation on action plan & compliance evidences'),
(11, 12, 'Dr. Sonia Bongala', NULL, NULL, NULL, 'September 2, 2016', 'September 9, 2016', NULL, 'February 20, 2018', NULL, '1) COI forms signed by REC members, 2) Institutional organizational chart showing location of REC in relation to other institutional units, 3) revised SOP (kindly see accreditation letter), and 4) copy of minutes of REC meeting for the period April - September 2018'),
(12, 13, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 14, 'Dr. Alvin Concha, REM XI:', NULL, NULL, NULL, 'April 16, 2018', 'June 4, 2018', 'July 17, 2018', 'July 31, 2018', NULL, '1) sample certificate of ethics training of one REC member\r\n2) e-copy of SOP where scope of review is stated\r\n3) e-copy of SOP with revised section 11, and \r\n4) revised agenda and minutes template'),
(14, 15, 'Dr. Cecilia Tomas', NULL, NULL, NULL, 'July 4, 2017', 'July 16, 2018', NULL, NULL, NULL, 'Too Many To Mention (kindly see accreditation letter and CSA recommendations, c/o Dr. Tomas)'),
(16, 17, 'Dr. Fred Guillergan', 'December 08 2017', '0001-01-01', 'January 21, 2019', 'May 30, 2018', 'May 31, 2018', 'July 25, 2018', NULL, 'July 25, 2018', NULL),
(17, 18, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, 20, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, 21, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, 22, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, 23, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, 24, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, 25, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, 26, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, 27, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(27, 28, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, 29, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(29, 30, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(30, 31, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(31, 32, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(32, 33, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(33, 34, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(34, 35, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(35, 36, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(36, 37, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(37, 38, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(38, 39, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(39, 40, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(40, 41, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(41, 42, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(42, 43, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(43, 44, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(44, 45, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(45, 46, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(46, 47, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(47, 48, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(48, 49, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(49, 50, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(50, 51, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(51, 52, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(52, 53, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(53, 54, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(54, 55, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(55, 56, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(56, 57, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(57, 58, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(58, 59, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(59, 60, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(60, 61, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(61, 62, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(62, 63, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(63, 64, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(64, 65, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(65, 66, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(66, 67, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(67, 68, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(68, 69, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(69, 70, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(70, 71, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(71, 72, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(72, 73, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(73, 74, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(74, 75, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(75, 76, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(76, 77, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(77, 78, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(78, 79, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(79, 80, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(80, 81, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(81, 82, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(82, 83, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(83, 84, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(84, 85, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(85, 86, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(86, 87, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(87, 88, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(88, 89, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(89, 90, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(90, 91, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `levels`
--

CREATE TABLE `levels` (
  `id` int(10) UNSIGNED NOT NULL,
  `level_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level_description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `levels`
--

INSERT INTO `levels` (`id`, `level_name`, `level_description`) VALUES
(1, 'Level 1', 'Level-I'),
(2, 'Level 2', 'Level-II'),
(3, 'Level 3', 'Level-III'),
(4, 'All Levels', '---'),
(5, 'No Levels', '---');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(66, '2014_10_12_000000_create_users_table', 1),
(67, '2014_10_12_100000_create_password_resets_table', 1),
(68, '2018_07_10_083620_create_levels_table', 1),
(69, '2018_07_10_084619_create_roles_table', 1),
(70, '2018_07_10_084621_create_role_users_table', 1),
(71, '2018_07_12_023051_create_regions_table', 1),
(72, '2018_07_12_023714_create_statuses_table', 1),
(73, '2018_07_12_023715_create_reclists_table', 1),
(74, '2018_07_13_060136_create_recdetails_table', 1),
(75, '2018_07_16_053506_create_applicationlevel_table', 1),
(76, '2018_07_23_064056_create_recchairs_table', 1),
(77, '2018_07_27_005802_create_jobs_table', 1),
(78, '2018_07_27_010219_create_failed_jobs_table', 1),
(79, '2018_07_28_072733_create_document_types_table', 1),
(80, '2018_08_08_024532_create_recdocuments_table', 1),
(81, '2018_08_24_071452_create_submissionstatuses_table', 1),
(82, '2018_08_29_020438_create_recdocumentsremarks_table', 1),
(83, '2018_09_03_011530_create_sendtoaccreditors_table', 1),
(84, '2018_09_17_030635_create_chatmessages_table', 1),
(85, '2018_09_18_062248_create_recassessments_table', 1),
(86, '2018_09_18_062325_create_recassessmentscsas_table', 1),
(87, '2018_09_19_011117_create_recassessmentrecs_table', 1),
(88, '2018_09_20_012352_create_recactionplans_table', 1),
(89, '2018_09_20_074418_create_recactionplancsas_table', 1),
(90, '2018_09_24_042905_create_emaillogs_table', 1),
(91, '2018_10_02_001234_create_email_templates_table', 1),
(92, '2018_10_08_111706_create_esigs_table', 1),
(93, '2018_10_10_150713_create_send_orderpayments_table', 1),
(94, '2018_11_05_151328_create_awardingletters_table', 1),
(95, '2018_11_23_134541_create_reportings_table', 1),
(96, '2018_12_12_140512_create_annual_report_titles_table', 1),
(97, '2018_12_12_180119_create_annual_reports_table', 1),
(98, '2019_01_03_164816_create_existing_recs_table', 1),
(99, '2019_01_06_155916_create_existing_arps_table', 1),
(100, '2019_01_07_154338_create_applicationlevel_reminders_table', 1),
(101, '2019_01_14_143444_create_accreditorslists_table', 1),
(102, '2019_02_11_132021_create_accreditors_evaluations_table', 2),
(103, '2019_02_11_135904_create_accreditors_evaluation_criterias_table', 2),
(104, '2019_02_11_162354_create_accreditors_eval_tables_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `recactionplancsas`
--

CREATE TABLE `recactionplancsas` (
  `id` int(10) UNSIGNED NOT NULL,
  `ap_csa_ap_id` int(10) UNSIGNED NOT NULL,
  `ap_csa_recdetails_id` int(10) UNSIGNED NOT NULL,
  `ap_csa_sender_id` int(10) UNSIGNED NOT NULL,
  `ap_csa_csachair_id` int(10) UNSIGNED DEFAULT NULL,
  `ap_csa_rec_id` int(10) UNSIGNED DEFAULT NULL,
  `ap_csa_status` datetime DEFAULT NULL,
  `ap_csa_datecreated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `recactionplans`
--

CREATE TABLE `recactionplans` (
  `id` int(10) UNSIGNED NOT NULL,
  `ap_recdetails_id` int(10) UNSIGNED NOT NULL,
  `ap_sender_id` int(10) UNSIGNED NOT NULL,
  `ap_document_types` int(10) UNSIGNED NOT NULL,
  `ap_recactionplan_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ap_recactionplan_uniqid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ap_status` datetime DEFAULT NULL,
  `ap_submittedto_rec` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `recassessmentrecs`
--

CREATE TABLE `recassessmentrecs` (
  `id` int(10) UNSIGNED NOT NULL,
  `ra_rec_ra_id` int(10) UNSIGNED NOT NULL,
  `ra_rec_recdetails_id` int(10) UNSIGNED NOT NULL,
  `ra_rec_sender_id` int(10) UNSIGNED NOT NULL,
  `ra_rec_csachair_id` int(10) UNSIGNED NOT NULL,
  `ra_rec_status` datetime DEFAULT NULL,
  `ra_rec_datecreated_at` datetime NOT NULL,
  `ra_rec_ap_dateexpiry` date NOT NULL,
  `ra_rec_ap_reminder_status` datetime DEFAULT NULL,
  `ra_rec_ap_reminder_status_deadline` datetime DEFAULT NULL,
  `ra_rec_ce_dateexpiry` date NOT NULL,
  `ra_rec_ce_reminder_status` datetime DEFAULT NULL,
  `ra_rec_ce_reminder_status_deadline` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `recassessments`
--

CREATE TABLE `recassessments` (
  `id` int(10) UNSIGNED NOT NULL,
  `ra_user_id` int(10) UNSIGNED NOT NULL,
  `ra_recdetails_id` int(10) UNSIGNED NOT NULL,
  `ra_documents_types_id` int(10) UNSIGNED NOT NULL,
  `ra_check` int(11) DEFAULT NULL,
  `ra_recdocuments_file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ra_recdocuments_file_generated` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ra_recdocuments_file_extension` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ra_dateuploaded` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `recassessmentscsas`
--

CREATE TABLE `recassessmentscsas` (
  `id` int(10) UNSIGNED NOT NULL,
  `ra_csa_ra_id` int(10) UNSIGNED NOT NULL,
  `ra_csa_sender_id` int(10) UNSIGNED NOT NULL,
  `ra_csa_csachair_id` int(10) UNSIGNED NOT NULL,
  `ra_csa_recdetails_id` int(10) UNSIGNED NOT NULL,
  `ra_csa_status` int(10) UNSIGNED DEFAULT NULL,
  `ra_csa_datecreated_at` datetime NOT NULL,
  `ra_csa_dateexpiry` date NOT NULL,
  `ra_csa_reminder_status` datetime DEFAULT NULL,
  `ra_csa_reminder_status_deadline` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `recchairs`
--

CREATE TABLE `recchairs` (
  `id` int(10) UNSIGNED NOT NULL,
  `recdetails_id` int(10) UNSIGNED NOT NULL,
  `rec_chairname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rec_chairemail` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rec_chairmobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `recchairs`
--

INSERT INTO `recchairs` (`id`, `recdetails_id`, `rec_chairname`, `rec_chairemail`, `rec_chairmobile`) VALUES
(2, 2, 'Prof. Ace Bryan S. Cabal', 'abscabal@adamson.edu.ph', '9950897078'),
(3, 3, 'Remedios L. Fernandez, RN, PhD', 'rlfernandez_ph@yahoo.com', '09177955842'),
(5, 5, 'Al D. Biag, MAN, MEd, EdD', 'clhrdc.region3@yahoo.com', '9278189123'),
(6, 6, 'Eleanor D. Cuarte, MD', 'edrcuarte@gmail.com', '9176070992'),
(9, 9, 'Servando D. Halili, Jr., PhD', 'arc@adzu.edu.ph', '9061078241'),
(10, 10, 'Norbel A. Tabo, Ph D', 'natabo@dlsud.edu.ph', '9175818981'),
(12, 12, 'Jean N. Guillasper, PhD, RN', 'jeanguillasper@neust.edu.ph', '09155048323 / 09328530867'),
(13, 13, 'Dr. Ronnel D. Dela Rosa', 'drronnelldelarosa@gmail.com', '---'),
(14, 14, 'Geraldine Hope D. Domines, MD', 'ghdd_doc@yahoo.com', '09172740095'),
(15, 15, 'Al D. Biag, MAN, MEd, EdD', 'abiag@hau.edu.ph', '---'),
(17, 17, 'Diadem Pearl S. Equiña, MD', 'diademequina@gmail.com', '09323441979'),
(18, 18, 'Christine Villanueva', 'ethicscommittee@usa.edu.ph', '(033) 3374841 to 44 loc 260 or 600'),
(20, 20, 'Anna Leah L. Enriquez, MD, DPBA, FPSA', 'ustcrs.erc@gmail.com', '---'),
(21, 21, 'Prof. Fernando R. Pedrosa, Ph.D.', 'erc.ustgs@gmail.com', '---'),
(22, 22, 'Jacklyn A. Cleofas, PhD', 'jcleofas@ateneo.edu', '0920-946-6242'),
(23, 23, 'Dr. Elenita Garcia', 'madelene.stamaria@dlsu.edu.ph', '---'),
(24, 24, 'Daisy J. Dulnuan, MD, FPOGS, FPSMFM, PSUCMI, MSII', 'daisy_jara_md@yahoo.com', '0917-322-0263'),
(25, 25, 'Dr. Rhodora M. Reyes, MD', 'rmrcns@yahoo.com.ph', '0918-945-6789'),
(26, 26, 'Dr. Jane R. Borrinaga', 'dmsfh.ec@gmail.com', '0922-842-5593 / 09497408313'),
(27, 27, 'Jocelyn P. Pedroso, PhD', 'pedrosojocelyn@yahoo.com', '0915-924-9941'),
(28, 28, 'Dr. Myrna P. Angeles, MD', 'angeles_myrna@yahoo.com', '0917-797-6284'),
(29, 29, 'Agnes B. Padilla, MD, DPBP, FPPA', 'psycdrpadilla@yahoo.com', '0917 704 2425'),
(30, 30, 'Roberta C. Romero, MD', 'taxromero715@gmail.com', '0918-933-4427'),
(31, 31, 'Renan P. Limjuco, PhD', 'rlimjuco@uic.edu.ph', '0918-318-7524'),
(32, 32, 'Johnny J. Yao, Jr., BSN, MN, DM', 'velezerc@gmail.com', '0922-703-3938'),
(33, 33, 'Jane R. Borrinaga, MD, FPCP', 'janeragbor@yahoo.com', '0949-740-8313'),
(34, 34, 'Marites G. Arcilla', 'marytes1974@gmail.com', '0942-241-0449'),
(35, 35, 'Frederick R. Supan, MD', 'fredgimed@yahoo.com', '0916-467-8584'),
(36, 36, 'Dr. Maria Philina P. Villamor', 'p_squared20052@yahoo.com', '09228482938'),
(37, 37, 'Glen A. Pono', 'drmc.ethics@gmail.com', '(084) 216-9127 loc. 811'),
(38, 38, 'Frederick Mars B. Untalan, MD, MBAH', 'peto.bghmc@gmail.com', '---'),
(39, 39, 'Jessica Ana A. Rivero, RN, MAN', 'ayka36@yahoo.com', '0917-822-4327'),
(40, 40, 'Cecil A. Carag, MD, FPSMS', 'cecilcarag@yahoo.com', '---'),
(41, 41, 'Dr. Lucila S. Sunga RN.', 'lucillesunga.ph@gmail.com', '09285008682'),
(42, 42, 'Dr. Gilbert B. Arendain', 'rhrdc_davaoregion@yahoo.com', '---'),
(43, 43, 'Dr. Hilarion Maramba, Jr.', 'ronimarambareturns@gmail.com', '09993870888'),
(44, 44, 'Vicente Salvador E. Montano', 'vicente_montano@umindanao.edu.ph', '(082) 2275456 / 09094177626'),
(45, 45, 'Milagros F. Neri, MD', 'feunrmf_ierc@yahoo.com', '427 0213 local 1236'),
(46, 46, 'Dr. Cecilia S. Acuin', 'fierc.fnri@gmail.com', '09302681966'),
(47, 47, 'CRISMELITA M. BAÑEZ, MD', 'jrrmmc.irb@gmail.com', '09286881307'),
(48, 48, 'Dr. Victoria C. Idolor', 'lcpierb@gmail.com', '9246101 loc. 568'),
(49, 49, 'Dr. Saturnino Javier', 'paula.marie.limbo@gmail.com', '8888-999 loc 2383'),
(50, 50, 'Ma. Rosario Bonagua, MD', 'mcufdtirb@yahoo.com', '09392585751'),
(51, 51, 'Angela Abanilla-Du, M.D., FPOGS', 'info@maniladoctors.com.ph', '5580888 local 4728'),
(52, 52, 'Marita V.T. Reyes, M.D.', 'nationalethicscommittee.ph@gmail.com', '---'),
(53, 53, 'Virginia R. De Jesus, MD', 'ethicsreviewcommittee@manilamed.com.ph', '523-8131  Fax: (632) 524-3340'),
(54, 54, 'Dr. Eva I. Bautista', 'nch_irb@yahoo.com.ph', '7240656 to 59 local 102'),
(55, 55, 'Dr. Teresita Tuazon', 'nktiresearchethics@gmail.com', '09328667779'),
(56, 56, 'Sheri Ann Liquete, MD', 'info@peregrineeye.com', '511-8506  Fax: 511-8505'),
(57, 57, 'MA.LUCILA M. PEREZ, MD', 'pcmc.irbec@yahoo.com.ph', '924-6601 TO 25 LOC. 356   Fax: 924-0840'),
(58, 58, 'Marcelito L. Durante, MD', 'irbphc@gmail.com', '925-2401 LOC.3899     Fax  9252401 LOC.3899'),
(59, 59, 'Prospero Ma. C. Tuaño, MD', 'ethicsreview@stluke.com.ph', '727-5562 /723-0101 loc. 7391'),
(60, 60, 'Dr. Carlo Emmanuel J. Sumpaico', 'irb@themedicalcity.com', '635-6789 loc. 6525'),
(61, 61, 'Dr. Jacinto Blas V. Mantaring III', 'upmreb@post.upm.edu.ph', '+63 2 5222684 Fax: +63 2 5222684'),
(62, 62, 'Dr. Wilson L. Tan De Guzman', 'usth_irb@yahoo.com.ph', '0922-8586039'),
(63, 63, 'Tito, C. Atienza, MD', 'drtito104@yahoo.com', '632-4269653'),
(64, 64, 'Fred P. Guillergan, M.D.', 'ubrerc_wvsu@yahoo.com.ph', '09101848890'),
(65, 65, 'Ma. Stella G. Paspe, MD', 'wvmcerc2015@gmail.com', '(033)337-28-46/(033)333-28-41'),
(66, 66, 'Dr. Jaime Manila', 'sphirbresearch@gmail.com', '09186838715'),
(67, 67, 'Evelyn R. Lacson', 'lacsonevelyn@yahoo.com', '+63 920 927 7982'),
(68, 68, 'Dr. Evelyn R . Lacson', 'ethics.clmmrh@gmail.com', '+63 920 927 7982'),
(69, 69, 'Enrico B. Gruet, MD', 'cduhrec@gmail.com', '+63923 718 8121'),
(70, 70, 'Marsha C. Tolentino, MD', 'PSHIERB@yahoo.com', '09236436685'),
(71, 71, 'Rev. Dr. Marvin M. Sinlao', 'marvs73@icloud.com', '09178485818'),
(72, 72, 'Madeleine M. Sosa, MD', 'iec@dlshsi.edu.ph', '---'),
(73, 73, 'Dr. Narcisa Sonia Comia', 'mmmc_rerc@yahoo.com.ph', '---'),
(74, 74, 'Dr. Emerson M. Cruz, MD', 'scmcaeierc@gmail.com', '---'),
(75, 75, 'Dr. Joanna V. Remo, MD', 'armmc_erc2012@yahoo.com', '---'),
(76, 76, 'Pura R. Caisip, MD', 'cesmao@csmc.ph', '---'),
(77, 77, 'Bernice Ong-Dela Cruz, MD', 'edenandulan21@yahoo.com', '---'),
(78, 78, 'Ana Liza Duran, MD', 'ierb.eamc@gmail.com', '---'),
(79, 79, 'Alvin S. Concha, M.D.', 'rxiirb@gmail.com', '---'),
(80, 80, 'Dr. Michael Martin Baccay', 'docmikebac@yahoo.com', '---'),
(81, 81, 'Manuel Emerson S. Donaldo, M.D.', 'sambagii2000@gmail.com', '---'),
(82, 82, 'Joyce S.A. Custodio, M.D.', 'joycecustodio22@yahoo.com', '---'),
(83, 83, 'Roy J. Cuison, M.D., MBA', 'cor_uphs@yahoo.com.ph', '---'),
(84, 84, 'Jose Ronilo G. Juangco, MD, MPH', 'research@uerm.edu.ph', '---'),
(85, 85, 'Veronica L Tallo, PhD', 'ritmirb@gmail.com', '---'),
(86, 86, 'Evelyn Victoria E. Reside, MD', 'reside_ivy@yahoo.com.ph', '09175304439'),
(87, 87, 'Rosalinda DS Pulido, MD', 'roseedoc@gmail.com', '09178971702'),
(88, 88, 'Chita I. Nazal-Matunog, MD', 'chita@matunog.com', '09176221025'),
(89, 89, 'Alisa Bernan, MD', 'mdmrcdavao@yahoo.com', '---'),
(90, 90, 'Elizabeth Freda O. Telan, MD, PhD', 'slh.iso.reru@gmail.com', '---'),
(91, 91, 'Vincent B. Aguilar, MD, DPPS, DPSHBT, DPSPH, FPSPO', 'brtth.ord@gmail.com', '09399106822');

-- --------------------------------------------------------

--
-- Table structure for table `recdetails`
--

CREATE TABLE `recdetails` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `rec_apptype_id` int(10) UNSIGNED NOT NULL,
  `rec_existing_record` int(10) UNSIGNED DEFAULT NULL,
  `rec_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rec_institution` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rec_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rec_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rec_telno` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rec_faxno` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rec_contactperson` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rec_contactposition` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rec_contactmobileno` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rec_contactemail` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rec_dateestablished` date NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `recdetails`
--

INSERT INTO `recdetails` (`id`, `user_id`, `rec_apptype_id`, `rec_existing_record`, `rec_name`, `rec_institution`, `rec_address`, `rec_email`, `rec_telno`, `rec_faxno`, `rec_contactperson`, `rec_contactposition`, `rec_contactmobileno`, `rec_contactemail`, `rec_dateestablished`, `deleted_at`) VALUES
(2, 2, 12, 1, 'University Ethics Research Committee (UERC)', 'Adamson University (2017)', '900 San Marcelinio St. Ermita Manila 1000', 'abscabal@adamson.edu.ph', '---', '---', 'Joseph G. Balaoing, PhD', 'Secretariat', '09256141218', 'uerc-secretariat@adamson.edu.ph', '2015-11-04', NULL),
(3, 7, 12, 1, 'Ethics Review Board(ERB)', 'Arellano University', '2600 Legarda St., Sampaloc, Manila 1008', 'rlfernandez_ph@yahoo.com', '09177955842', '09177955842', 'Remedios L. Fernandez, RN, PhD', 'RN, PhD', '09177955842', 'rlfernandez_ph@yahoo.com', '2014-11-20', NULL),
(5, 7, 12, 1, 'Ethics Review Committee (ERC)', 'Central Luzon Health Research and Development Consortium', 'DOST Regional Office No. 3 DM Government Center Maimpis San Fernando City Pampanga 2000', 'clhrdc.region3@yahoo.com', '09278189123', '09278189123', 'Camille Louise S. Sunglao', 'Secretariat', '09278189123', 'ethics.clhrdc@gmail.com', '2008-05-01', NULL),
(6, 2, 12, 1, 'Ethics Review Committee (ERC)', 'Cordillera Administrative Region (CAR) Regional Health Research and Development Consortium', 'DOST-CAR Km. 6 La Trinidad Benguet 2601', 'edrcuarte@gmail.com', '---', '---', 'Jake S. Olsim', 'Secretariat', '09480777825', 'crhrdccar@gmail.com', '2008-01-01', NULL),
(9, 2, 12, 1, 'Ethics Review Committee (ERC)', 'Ateneo de Zamboanga University', 'c/o Ateneo Research Center Ateneo de Zamboanga University La Purisima St. Zamboanga City Zamboanga del Sur 7000', 'montanoterg@adzu.edu.ph', '---', '---', 'Aleli J. Ramillano & Precious T. Opinion', 'Secretary', '09361692687', 'arc@adzu.edu.ph', '2013-06-01', NULL),
(10, 2, 12, 1, 'Ethics Review Committee (ERC)', 'De La Salle University Dasmariñas', 'Dasmariñas Bagong Bayan-B Dasmariñas City Cavite 4114', 'dlsuderc@dlsud.edu.ph', '---', '---', 'Melanie P. Medecilo', 'Secretary', '09192659432', 'dlsuderc@dlsud.edu.ph', '0001-01-01', NULL),
(12, 2, 12, 1, 'University Ethics Research Committee (UERC)', 'Nueva Ecija University of Science and Technology', 'General Tinio St. Cabanatuan City Nueva Ecija 3100', 'jeanguillasper@neust.edu.ph', '---', '---', 'Jean N. Guillasper, PhD, RN', 'Secretariat', '09155048323 / 09328530867', 'urecsecretariat@neust.edu.ph', '2016-06-16', NULL),
(13, 2, 12, 1, 'Research Ethics Committee (REC)', 'Bataan Peninsula State University', '3rd Floor Admin. Building BPSU Main Campus Capitol Drive Balanga City Bataan 2100', 'bpsuprec2017@gmail.com', '---', '---', 'Dr. Ronnel D. Dela Rosa', 'REC Chair', '09298186024', 'bpsuprec2017@gmail.com', '2017-02-08', NULL),
(14, 2, 12, 1, 'Research Ethics Committee (REC)', 'Cotabato Regional and Medical Center', 'Sinsuat Avenue Cotabato City Maguindanao 9600', 'uzumaki21793@gmail.com', '---', '---', 'Edward B. Cabalida', 'Secretariat', '09172740095', 'ghdd_doc@yahoo.com', '2017-02-01', NULL),
(15, 2, 12, 1, 'Institutional Review Board (IRB)', 'Holy Angel University', '#1 Holy Angel Avenue Santo Rosario Angeles City Pampanga 2009', 'abiag@hau.edu.ph', '---', '---', 'Julie Anne Mary C. Cruz', 'Secretariat', '09088878187', 'jcruz@hau.edu.ph', '0001-01-01', NULL),
(17, 7, 12, 1, 'Research Ethics Committee (REC)', 'Central Philippines University', 'Lopez Jaena St. Jaro Iloilo City Iloilo 5000', 'diademequina@gmail.com', '09323441979', '09323441979', 'Melba C. Sale / Atty. Salex E. Alibogha, LLM', 'Secretariat', '09506349832', 'kabalakacpu@gmail.com', '2014-04-21', NULL),
(18, 2, 12, 1, 'Research Ethics Review Committee (RERC)', 'University of San Agustin', 'General Luna St., Iloilo City', 'ethicscommittee@usa.edu.ph', '(033) 3374841 to 44 loc 260 or 600', '---', 'Christine Villanuev', 'Chair', '(033) 3374841 to 44 loc 260 or 600', 'ethicscommittee@usa.edu.ph', '0001-01-01', NULL),
(20, 2, 12, 1, 'Ethics Review Committee (ERC)', 'University of Santo Tomas - College of Rehabilitation Sciences', 'España Boulevard, Sampaloc Manila, Metro Manila', 'ustcrs.erc@gmail.com', '406-1611 loc. 8443', '406-1611 loc. 8443', 'Catherine Joy T. Escuadra', 'Secretariat', '09167329643', 'ustcrs.erc@gmail.com', '0001-01-01', NULL),
(21, 2, 12, 1, 'Ethics Review Committee (ERC)', 'University of Sto. Tomas – Graduate School', 'Rm. 301, TARC, UST, España, Manila', 'erc.ustgs@gmail.com', '406-1611 loc. 4029', '---', 'Angelo Brian T. Castro', 'Office Secretariat', '0915-149-1698', 'erc.ustgs@gmail.com', '0001-01-01', NULL),
(22, 2, 12, 1, 'Research Ethics Committee (REC)', 'Ateneo de Manila University', 'Ateneo de Manila University Loyola Heights,  Katipunan Avenue, Quezon City 1108 Metro Manila', 'univresearchethics@ateneo.edu', '426-6001 ext. 4030-32', '---', 'Edith Liane Peña Alampay, PhD', 'Director of the University Research Ethics Office (UREO)', '0917-891-7618', 'lpalampay@ateneo.edu', '2015-11-03', NULL),
(23, 2, 12, 1, 'Research Ethics Committee (REC)', 'De La Salle University Manila', '2401 Taft Avenue Malate Manila', 'madelene.stamaria@dlsu.edu.ph', '---', '---', 'Dr. Madelene A. Sta. Maria', 'Assistant Chair', '0917-516-2112', 'madelene.stamaria@dlsu.edu.ph', '2008-01-01', NULL),
(24, 2, 12, 1, 'Institutional Review Board (IRB)', 'Veterans Regional Hospital (2015/2016)', 'AH26 (Maharlika Highway) Barangay Magsaysay Bayombong Nueva Vizcaya', 'vrhirb@yahoo.com.ph', '(078) 805-35-60', 'VRH-IRB Chair', 'Daisy J. Dulnuan, MD, FPOGS, FPSMFM, PSUCMI, MSII', 'Chair', '0917-322-0263', 'daisy_jara_md@yahoo.com', '2015-08-15', NULL),
(25, 2, 12, 1, 'Research Ethics Review Committee (RERC)', 'Batangas Medical Center', 'Bihi Road Kumintang Ibaba Batangas', 'rmrcns@yahoo.com.ph', '043-7272103', '0918-945-6789', 'Dr. Rhodora M. Reyes, MD', 'Chair', '0918-945-6789', 'rmrcns@yahoo.com.ph', '2014-01-01', NULL),
(26, 2, 12, 1, 'Ethics Review Committee (ERC)', 'Eastern Visayas Health Research and Development Consortium', 'Medical School Drive Bajada Davao City Davao del Sur', 'dmsfh.ec@gmail.com', '(053) 323 7110', '0922-842-5593', 'Ilra Valmonte', 'Secretariat', '0922-842-5593', 'dmsfh.ec@gmail.com', '2010-03-24', NULL),
(27, 2, 12, 1, 'Research Ethics Oversight Committee (REOC)', 'Western Mindanao State University (2015)', '2nd Floor Research Building Western Mindanao State University Normal Road Baliwasan Zamboanga City Zamboanga del Sur', 'pedrosojocelyn@yahoo.com', '---', '0915-924-9941', 'Jovelyn I. Legaspi', 'Secretariat', '0917-729-6959', 'jaslyn_joi@yahoo.com', '2016-04-06', NULL),
(28, 2, 12, 1, 'Ethics Review Board(ERB)', 'Zamboanga City Medical Center (2015)', 'Dr. D. Evangelista Street Barangay Sta. Catalina Zamboanga City Zamboanga del Sur', 'zcmcru@gmail.com', '(062) 991-2934/ 991-0573 loc 113', '09175199629', 'Norvie T. Jalani, MD, MSII', 'Medical Specialist II', '0977-317-6757', 'zcmcru@gmail.com', '2012-01-01', NULL),
(29, 2, 12, 1, 'Hospital Ethics Committee (HEC)', 'Davao Medical School Foundation Incorporated (2015)', 'Medical School Drive Bajada Davao City Davao del Sur', 'dmsfh.ec@gmail.com', '(082) 305 0931', '0922-842-5593', 'Ilra Valmonte', 'Secretariat', '0922-842-5593', 'dmsfh.ec@gmail.com', '2010-03-24', NULL),
(30, 2, 12, 1, 'Institutional Review Board (IRB)', 'Tropical Disease Foundation, Incorporated (2015)', '4th Floor Philippine Institute of Tuberculosis Building Amorsolo Street corner Urban Avenue Makati City', 'mlpvillanueva@tdf.org.ph', '8940741/43 locals  107,108', '09058687826/09275337283', 'Lisette Villanueva/Evelyn Joson', 'IRB Secretariat', '09058687826/09275337283', 'esjoson@tdf.org.ph', '1984-08-01', NULL),
(31, 2, 12, 1, 'Research Ethics Committee (REC)', 'University of Immaculate Conception (2012)', 'Andres Bonifacio Street Bajada Davao City Davao del Sur', 'rlimjuco@uic.edu.ph', '---', '---', 'Renan P. Limjuco, PhD', 'Chair', '0918-318-7524', 'rpic@uic.edu.ph', '2015-06-01', NULL),
(32, 2, 12, 1, 'Ethics Review Committee (ERC)', 'Velez College', '79 Fructuoso Ramos Street Barangay Cogon Central Cebu City Cebu', 'velezerc@gmail.com', '---', '0922-703-3938', 'Johnny J. Yao, Jr., BSN, MN, DM', 'Chair', '0922-703-3938', 'velezerc@gmail.com', '0001-01-01', NULL),
(33, 2, 12, 1, 'Institutional Ethics Review Committee (IERC)', 'Eastern Visayas Regional Medical Center', 'DOST RO VIII Government Center Cadahug Palo Leyte', 'evhrdc@yahoo.com', '---', '0916-542-4682/0917-318-2265', 'Juliet M. Aguirre', 'Secretariat', '0916-542-4682/0917-318-2265', 'evhrdc@yahoo.com', '1982-01-01', NULL),
(34, 2, 12, 1, 'Institutional Review Board (IRB)', 'University of the Visayas', 'Corner Colon and Dionisio Jakosalem Streets Cebu City Cebu', 'marytes1974@gmail.com', '---', '---', 'Brian A. Vasquez, PhD', 'IRB Contact Person', '0922-825-3404', 'brianquez@gmail.com', '2015-04-30', NULL),
(35, 2, 12, 1, 'Ethics Review Committee (ERC)', 'Mary Johnston Hospital', '1221 Juan Nolasco Street Barangay 37 Tondo Manila', 'mjh_erc@yahoo.com', '---', '---', 'Arlyn J. Espiritu', 'Secretariat', '0906-167-5338', 'espirituarlyn1@gmail.com', '2002-01-01', NULL),
(36, 2, 12, 1, 'Research Ethics Committee (REC)', 'Vicente Sotto Memorial Medical Center', '---', 'vsmmcethicsreviewcommittee@yahoo.com', '263-7497', '09228482938', 'Dr. Maria Philina P. Villamor', 'Chair', '09228482938', 'p_squared20052@yahoo.com', '2016-04-26', NULL),
(37, 2, 12, 1, 'Research Ethics Committee (REC)', 'Davao Regional Medical Center', 'Apokon Rd. Tagum City, Davao Del Norte', 'drmc.ethics@gmail.com', '(084) 216-9127 loc. 811', '---', 'Dorothy M. Dimaandal', 'Member-secretariat', '09499952504', 'dorothydimaandal@gmail', '2018-05-29', NULL),
(38, 2, 12, 1, 'Ethics Review Committee (ERC)', 'Baguio General Hospital and Medical Center', 'Governor Pack Road, Baguio City', 'peto.bghmc@gmail.com', '074-661-7910', '---', 'Joy R. Bautista', 'Training Assistant/Staff Secretary', '661-7910 loc 355', 'bghmc.erc@gmail.com', '2003-10-27', NULL),
(39, 2, 12, 1, 'Institutional Ethics Review Committee (IERC)', 'Our Lady of Fatima University', '#120 MacArthur Highway Barangay Marulas Valenzuela City', 'olfu.rdic@yahoo.com', '291-65-38 loc 808', '---', 'Jueliand Peter A. Perez', 'Member', '0917-822-4327', 'olfu.rdic@yahoo.com', '2014-01-01', NULL),
(40, 2, 12, 1, 'Research Ethics Review Committee (RERC)', 'Cagayan Valley Medical Center', 'Pan-Philippine Highway Carig Regional Center Carig Sur Tuguegarao City Cagayan', 'cecilcarag@yahoo.com', '---', '---', 'Jenalyn C. Geronimo / Jeff Christopher T. Bunagan, RN', 'Member-Secretariat', '0917-116-9442', 'jen_internalmed@yahoo.com', '2014-10-01', NULL),
(41, 2, 12, 1, 'Research Ethics Review Committee (RERC)', 'Tarlac State University', 'Romulo Boulevard, San Vicente, Tarlac City', 'tsu.rerc@gmail.com', '(045) 982-1624', '09285008682', 'Dr. Lucila S. Sunga RN.', 'Chaiman', '09285008682', 'lucillesunga.ph@gmail.com', '2016-10-21', NULL),
(42, 2, 12, 1, 'Ethics Review Committee (ERC)', 'Regional Health Research and Development Consortium XI', 'c/o DOST XI office, cor. Friendship and Dumanlas Roads, Bajada, Davao City', 'rhrdc_davaoregion@yahoo.com', '(082) 221-5428/227-1313', '+63 999 3094674', 'Leslie Pearl M. Cancio / Richell Mae Ruyeras', 'RHRDC XI Project Staff', '+63 999 3094674', 'lpcancio_dostxi@yahoo.com', '1985-01-01', NULL),
(43, 2, 12, 1, 'Ethics Review Committee (ERC)', 'Region I Health Research and Development Consortium', 'C/o Mariano Marcos Memorial Hospital & Medical Center, Batac, Ilocos Norte', 'ronimarambareturns@gmail.com', '0777923133', '09258861180', 'Dr. Hilarion Maramba, Jr.', 'Chair', '09993870888', 'ronimarambareturns@gmail.com', '2011-01-01', NULL),
(44, 2, 12, 1, 'Ethics Review Committee (ERC)', 'University of Mindanao', 'Maa, Davao City, Region XI', 'umethicsreviewer@umindanao.edu.ph', '(082) 2976115', '09322418500', 'Hansel Roy R. Nalla', 'Secretariat', '09322418500', 'umethicsreviewer@umindanao.edu.ph', '0001-01-01', NULL),
(45, 7, 12, 1, 'Institutional Ethics Review Committee (IERC)', 'Far Eastern University - Nicanor Reyes Medical Foundation', 'Regalado Avenue, near Dahlia Street, West Fairview, Quezon City', 'feunrmf_ierc@yahoo.com', '427 0213 local 1236', '427 0213 local 1236', 'Ms. Dianne C. Rocha', 'Secretariat', '427 0213 local 1236', 'feunrmf_ierc@yahoo.com', '0001-01-01', NULL),
(46, 7, 12, 1, 'Institutional Ethics Review Committee (IERC)', 'Food and Nutrition Research Institute', 'Gen. Santos Ave., Bicutan, Taguig City, Philippines 1631', 'fierc.fnri@gmail.com', '837-20-71 loc. 2297', '09302681966', 'Marlita A. Aguilos', 'Head, Member Secretary', '09302681966', 'fierc.fnri@gmail.com', '0001-01-01', NULL),
(47, 7, 12, 1, 'Institutional Review Board (IRB)', 'Jose R. Reyes Memorial Medical Center', 'Rizal Avenue, Sta. Cruz, Manila', 'jrrmmc.irb@gmail.com', '732 1071-76 loc. 296', '09286881307', 'Ms. Lot M. Dorado', 'IRB Secretary', '09286881307', 'jrrmmc.irb@gmail.com', '0001-01-01', NULL),
(48, 7, 12, 1, 'Institutional Ethics Review Committee (IERC)', 'Lung Center of the Philippines', '4th floor Rm. 4013 Lung center of the Philippines Quezon Avenue, Diliman, Quezon City 1100', 'lcpierb@gmail.com', '9246101 loc. 568', '9246101 loc. 568', 'Jennifer F. Juliano', 'Secretariat', '9246101 loc. 568', 'lcpierb@gmail.com', '0001-01-01', NULL),
(49, 7, 12, 1, 'Institutional Review Board (IRB)', 'Makati Medical Center', '7th floor, Keyland Center, Makati Medical Center Tower 3 #143 Dela Rosa cor. Adelantado St., Legazpi Village, Makati City', 'paula.marie.limbo@gmail.com', '8888-999 loc 2383 Fax: 8888-999 loc 7182', '8888-999 loc 2383', 'Paula Limbo', 'Secretariat', '8888-999 loc 2383', 'paula.marie.limbo@gmail.com', '0001-01-01', NULL),
(50, 7, 12, 1, 'Institutional Review Board (IRB)', 'Manila Central University Filemon D. Tanchoco Medical Foundation', 'Samson Road, EDSA, Caloocan City', 'mcufdtirb@yahoo.com', '367-2031 loc. 1226', '09392585751', 'Marilyn G. Salvador', 'Secretariat', '09392585751', 'mcufdtirb@yahoo.com', '0001-01-01', NULL),
(51, 7, 12, 1, 'Institutional Review Board (IRB)', 'Manila Doctors Hospital', '8th Floor Norberto Ty Medical Tower 2, Manila Doctors Hospital, T.M. Kalaw St., Ermita, Manila, 1000', 'info@maniladoctors.com.ph', '5580888 local 4728', '5580888 local 4728', 'Myrna Buenaluz-Sedurante, MD', 'Secretariat', '5580888 local 4728', 'info@maniladoctors.com.ph', '0001-01-01', NULL),
(52, 2, 12, 1, 'Research Ethics Committee (REC)', 'National Ethics Committee (NEC)', '3/F DOST Main Building, Gen. Santos Ave., Bicutan, Taguig City 1631', 'nationalethicscommittee.ph@gmail.com', '(02) 837 7537', '(02) 837 7537', 'Dean Ryan C. Aguila/ Marie Jeanne B. Berroya', 'Secretariat', '(02) 837 7537', 'nationalethicscommittee.ph@gmail.com', '1984-01-01', NULL),
(53, 7, 12, 1, 'Ethics Review Committee (ERC)', 'Manilamed Ethics Review Committee (MMERC)', 'U.N Avenue Corner Taft Avenue Ermita Manila', 'ethicsreviewcommittee@manilamed.com.ph', '523-8131  Fax: (632) 524-3340', '523-8131', 'Virginia R. De Jesus, MD', 'Secretariat', '523-8131  Fax: (632) 524-3340', 'ethicsreviewcommittee@manilamed.com.ph', '0001-01-01', NULL),
(54, 7, 12, 1, 'Institutional Review Board (IRB)', 'National Children\'s Hospital', '264 E. Rodriguez Sr. Avenue, Quezon City', 'nch_irb@yahoo.com.ph', '7240656 to 59 local 102', '7240656 to 59 local 102', 'Joey Lopez', 'Secretariat', '7240656 to 59 local 102', 'nch_irb@yahoo.com.ph', '0001-01-01', NULL),
(55, 7, 12, 1, 'Ethics Review Committee (ERC)', 'National Kidney and Transplant Institute', 'East Avenue, Quezon City', 'nktiresearchethics@gmail.com', '9810300 loc 4408', '09328667779', 'Avegaine S. Padua', 'Secretariat', '09328667779', 'nktiresearchethics@gmail.com', '0001-01-01', NULL),
(56, 7, 12, 1, 'Institutional Review Board (IRB)', 'Peregrine Eye and Laser Institute, Inc.', '50 Jupiter St. Bel Air Makati City 1209', 'info@peregrineeye.com', '511-8506  Fax: 511-8505', '511-8506  Fax: 511-8505', 'Ma. Angela Felicia C. Arregalo-Dumlao', 'Secretariat', '511-8506  Fax: 511-8505', 'info@peregrineeye.com', '0001-01-01', NULL),
(57, 7, 12, 1, 'Institutional Review Board (IRB)', 'Philippine Children\'s Medical Center', 'QUEZON AVENUE, CORNER AGHAM ROAD, QUEZON CITY', 'pcmc.irbec@yahoo.com.ph', '924-6601 TO 25 LOC. 356   Fax: 924-0840', '924-6601 TO 25 LOC. 356   Fax: 924-0840', 'MA. LUCILA M. PEREZ, MD', 'CHAIR, IRB-EC', '924-6601 TO 25 LOC. 356   Fax: 924-0840', 'pcmc.irbec@yahoo.com.ph', '0001-01-01', NULL),
(58, 7, 12, 1, 'Institutional Ethics Review Board (IERB)', 'Philippine Heart Center', '8F MAB PHC EAST AVENUE, QUEZON CITY', 'irbphc@gmail.com', '925-2401 LOC.3899     Fax  9252401 LOC.3899', '925-2401 LOC.3899     Fax  9252401 LOC.3899', 'Rachelle B. Trinidad', 'Secretariat', '925-2401 LOC.3899     Fax  9252401 LOC.3899', 'irbphc@gmail.com', '1986-01-01', NULL),
(59, 7, 12, 1, 'Ethics Review Committee (ERC)', 'St. Luke\'s Medical Center', '279 E. Rodriguez Sr. Blvd., Quezon City', 'ethicsreview@stluke.com.ph', '727-5562 /723-0101 loc. 7391', '727-5562 /723-0101 loc. 7391', 'Heidi A. Cabal', 'Secretariat', '727-5562 /723-0101 loc. 7391', 'ethicsreview@stluke.com.ph', '1996-01-01', NULL),
(60, 7, 12, 1, 'Institutional Review Board (IRB)', 'The Medical City', 'Ortigas Avenue, Pasig City, Metro Manila', 'irb@themedicalcity.com', '635-6789 loc. 6525', '635-6789 loc. 6525', 'Johann Fabrian Q. Bolinao', 'Secretariat', '635-6789 loc. 6525', 'irb@themedicalcity.com', '1997-01-01', NULL),
(61, 7, 12, 1, 'Research Ethics Board (REB)', 'University of the Philippines Manila', '2nd Floor Paz Mendoza Building, College of Medicine UP Manila, 547 Pedro Gil Street, Ermita, 1000 Manila', 'upmreb@post.upm.edu.ph', '+63 2 5222684 Fax: +63 2 5222684', '+63 2 5222684 Fax: +63 2 5222684', 'Jacinto Blas V. Mantaring III, MD,', 'MSc Chair, UPMREB', '+63 2 5222684 Fax: +63 2 5222684', 'upmreb@post.upm.edu.ph', '2011-01-01', NULL),
(62, 7, 12, 1, 'Institutional Review Board (IRB)', 'University of  Santo Tomas Hospital', 'USTH-IRB Office 6th Floor Clinical Division Building, España Blvd, Manila Philippines 1015', 'naranjilla.ust.irb@gmail.com', '02-731-2001 local 2610 	 Fax: 02 – 732-8189  c/o Office of the Medical Director', '0922-8586039', 'Dr. Raymund Gabriel A. Naranjilla', 'Secretariat', '0923-7371536', 'naranjilla.ust.irb@gmail.com', '0001-01-01', NULL),
(63, 7, 12, 1, 'Institutional Review Board (IRB)', 'Veterans Memorial Medical Center', 'North Avenue, Diliman, Quezon City', 'drtito104@yahoo.com', '9276426 to 30 local 1368', '632-4269653', 'Tito C. Atienza, MD', 'Secretariat', '632-4269653', 'drtito104@yahoo.com', '2010-11-02', NULL),
(64, 7, 12, 1, 'Ethics Review Committee (ERC)', 'West Visayas State University - Unified Biomedical Research', 'LUNA ST. LAPAZ, ILOILO CITY, REGION 6', 'ubrerc_wvsu@yahoo.com.ph', '320-0877 LOC. 1506   Fax:  (033) 320-0881', '09101848890', 'FRED P. GUILLERGAN, M.D.', 'Chair', '09101848890', 'ubrerc_wvsu@yahoo.com.ph', '0001-01-01', NULL),
(65, 7, 12, 1, 'Ethics Review Committee (ERC)', 'Western Visayas Medical Center', 'Q. ABETO ST., MANDURRIAO, ILOILO CITY 5000', 'wvmcerc2015@gmail.com', '(033)337-28-46/(033)333-28-41', '(033)337-28-46/(033)333-28-41', 'Eden Shiz Parpa', 'Secretariat', '(033)337-28-46/(033)333-28-41', 'wvmcerc2015@gmail.com', '0001-01-01', NULL),
(66, 7, 12, 1, 'Institutional Review Board (IRB)', 'St. Paul’s Hospital Iloilo', 'Gen. Luna Street, Iloilo City', 'sphirbresearch@gmail.com', '337 27 41 local 8255', '09186838715', 'Sr. Maria Kristina Bergonia, SPC', 'Secretariat', '09186838715', 'sphirbresearch@gmail.com', '0001-01-01', NULL),
(67, 7, 12, 1, 'Ethics Review Committee (ERC)', 'Dr. Pablo O. Torre Memorial Hospital', 'BS Aquino Drive, Bacolod City Negros Occidental', 'lacsonevelyn@yahoo.com', '(034) 432 2719', '+63 920 927 7982', 'Evelyn R. Lacson, MD', 'Secretariat', '+63 920 927 7982', 'lacsonevelyn@yahoo.com', '0001-01-01', NULL),
(68, 7, 12, 1, 'Ethics Review Committee (ERC)', 'Corazon Locsin Montelibano Memorial Regional Hospital', 'Lacson St., Bacolod City, Negros Occidental, Philippines 6100', 'ethics.clmmrh@gmail.com', '(034) 432 2719', '+63 920 927 7982', 'Mr. Fel Jan Conrad L. Famoso', 'Secretariat', 'Mr. Fel Jan Conrad L. Famoso', 'ethics.clmmrh@gmail.com', '0001-01-01', NULL),
(69, 7, 12, 1, 'Research Ethics Committee (REC)', 'Cebu Doctors’ University Hospital', 'Cebu Doctors’ University Hospital, Administrative Offices  Gov. M. Roa St. Corner Don Jose Avila St.  Cebu City, Region Vii', 'cduhrec@gmail.com', '+6332 4169341', '+63923 718 8121', 'Dr. Enrico B. Gruet', 'Chair', '+63923 718 8121', 'cduhrec@gmail.com', '2018-01-01', NULL),
(70, 7, 12, 1, 'Ethics Review Board(ERB)', 'Perpetual Succour Hospital', 'Gorordo Avenue, Lahug, Cebu City, Cebu, Region Vii', 'PSHIERB@yahoo.com', '233-8620 (local 375)', '09236436685', 'Kathleen F. Banua', 'Secretariat', '09236436685', 'PSHIERB@yahoo.com', '0001-01-01', NULL),
(71, 2, 12, 1, 'Institutional Ethics Review Committee (IERC)', 'Angeles University Foundation', 'Mcarthur Highway, Angeles City, Pampanga', 'aufierc@auf.edu.ph', '045-625-2888 local 709', '09295217900', 'Dr. Maria Emilia Deogracias T. Bayubay', 'Committee Secretary', '09295217900', 'aufierc@auf.edu.ph', '0001-01-01', NULL),
(72, 2, 12, 1, 'Independent Ethics Committee(IEC)', 'De La Salle Health Sciences Institute', 'Ground Floor, De La Salle Angelo King Medical Research Center Congressional Avenue Dasmariñas, Cavite 4114 Philippines', 'iec@dlshsi.edu.ph', '(046) 481 8000 loc. 8042', '---', 'Ms. Genevieve V. Bayas', 'Member Secretary', '---', 'iec@dlshsi.edu.ph', '0001-01-01', NULL),
(73, 2, 12, 1, 'Ethics Review Committee (ERC)', 'Mary Mediatrix Medical Center', 'Jp Laurel Highway, Mataas Na Lupa. Lipa City, Batangas', 'mmmc_rerc@yahoo.com.ph', '(043) 773 6800 local 1194', '---', 'Ms. Elaine Natividad', 'Secretariat', '09753904183', 'mmmc_rerc@yahoo.com.ph', '0001-01-01', NULL),
(74, 2, 12, 1, 'Ethics Review Committee (ERC)', 'St. Cabrini Medical Center - Asian Eye Institute', '8th Floor, Phinma Plaza Building, Rockwell Center Makati City 1200 Philippines', 'scmcaeierc@gmail.com', '(02) 898-2020 loc. 815', '---', 'Ms. Juliet Falguera', 'Secretariat', '(02) 898-2020 loc. 815', 'scmcaeierc@gmail.com', '0001-01-01', NULL),
(75, 2, 12, 1, 'Ethics Review Board(ERB)', 'Amang Rodriguez Memorial Medical Center', 'Sumulong Highway, Sto. Niño Marikina City', 'armmc_erc2012@yahoo.com', '941-6638', '---', 'Thelma A. Fadallan', 'Secretariat', '0929-4018249', 'armmc_erc2012@yahoo.com', '0001-01-01', NULL),
(76, 2, 12, 1, 'Research Ethics Review Committee (RERC)', 'Cardinal Santos Medical Center', '10 Wilson St., Greenhills San Juan City 1502', 'cesmao@csmc.ph', '02 – 7270001-17 local 3799; 02-7210334', '---', 'Conchita Esmao / Janice Bacarisas', 'Secretariat', '02 – 7270001-17 local 3799; 02-7210334', 'jbacarisas@csmc.ph', '0001-01-01', NULL),
(77, 2, 12, 1, 'Research Ethics Review Board (RERB)', 'Chinese General Hospital And Medical Center', '286 Blumentritt St. Sta Cruz Manila', 'edenandulan21@yahoo.com', '7114141 loc. 435', '---', 'Eden Andulan', 'Secretariat', '7114141 loc. 435', 'edenandulan21@yahoo.com', '0001-01-01', NULL),
(78, 2, 12, 1, 'Institutional Ethics Review Board (IERB)', 'East Avenue Medical Center', 'East Avenue, Diliman, Quezon City, Metro Manila', 'ierb.eamc@gmail.com', '(632) 928 0611 local 739', '---', 'Christine D. Montegrejo, Rpm', 'Secretariat', '(63) 999 889 4696', 'ierb.eamc@gmail.com', '0001-01-01', NULL),
(79, 2, 12, 1, 'Cluster Research Ethics Review Committee (CRERC)', 'Department Of Health Xi', '\"hospital Research Office  First Level, Jica Building  Southern Philippines Medical Center  Bajada, Davao City 8000\"', 'rxiirb@gmail.com', '(082) 2272731 local 4615', '---', 'Alvin S. Concha, M.d.', 'Chair', '(082) 2272731 local 4615', 'rxiirb@gmail.com', '0001-01-01', NULL),
(80, 2, 12, 1, 'Ethics Review Committee (ERC)', 'Mariano Marcos Memorial Hospital And Medical Center', '#6 San Julian Barangay, Batac City, Ilocos Norte', 'mmmhrerc@gmail.com', '---', '---', 'Dr. Elyzel Puguon', 'Chairman', '09175682919 / 09778257125', 'yzelpuguon@gmail.com', '2010-01-01', NULL),
(81, 2, 12, 1, 'Institutional Review Board (IRB)', 'Chong Hua Hospital', 'Don Mariano Cui Cor. J. Llorente Sts., Cebu City', 'sambagii2000@gmail.com', '(032) 255-8000 local 7434', '---', 'Manuel Emerson S. Donaldo, M.d.', 'Chair', '---', 'sambagii2000@gmail.com', '0001-01-01', NULL),
(82, 2, 12, 1, 'Institutional Review Board (IRB)', 'Davao Doctors Hospital', '4th flr. Oncology bldg. 118 E. Quirino Avenue, Davao City, Davao Del Sur', 'IERC@ddh.com.ph', '222 8000 loc 1409', '---', 'Isobel Joy D. Peligro', 'Staff Assistant', '09258260453', 'idpeligro@ddh.com.ph', '0001-01-01', NULL),
(83, 2, 12, 1, 'Institutional Ethics Review Board (IERB)', 'University Of Perpetual Help System', 'Alabang-zapote Road, Pampalona, Las Piñas City', 'cor_uphs@yahoo.com.ph', '8733772', '---', 'Roy J. Cuison, M.d., Mba', 'Chair', '---', 'cor_uphs@yahoo.com.ph', '0001-01-01', NULL),
(84, 2, 12, 1, 'Institutional Ethics Review Board (IERB)', 'University Of The East Ramon Magsaysay Memorial Medical Center, Inc. Research Institute For Health', '64 Aurora Boulevard, Brgy. Doña Imelda, Quezon City 1113', 'research@uerm.edu.ph', '7161843', '---', 'Jose Ronilo G. Juangco, Md, Mph', 'Chair', '---', 'research@uerm.edu.ph', '0001-01-01', NULL),
(85, 2, 12, 1, 'Institutional Review Board (IRB)', 'Research Institute For Tropical Medicine', '9002 Research Drive, Filinvest Corporate City, Alabang, Muntinlupa City', 'ritmirb@gmail.com', '80726-28 loc 418 or 225', '---', 'Jeniffer M. Landicho', 'Secretariat', '0927935122', 'jenski_08@yahoo.com', '1983-02-07', NULL),
(86, 2, 12, 1, 'Hospital Ethics Committee (HEC)', 'Quirino Memorial Medical Center', 'Katipunan Road, Project 4, Quezon City', 'reside_ivy@yahoo.com.ph', '421-2250(trunk line)', '09175304439', 'Evelyn Victoria E. Reside, Md', 'Medical Specialist Iv, Chairman', '09175304439', 'reside_ivy@yahoo.com.ph', '2004-01-01', NULL),
(87, 2, 12, 1, 'Institutional Review Board (IRB)', 'San Juan De Dios Educational Foundation, Inc. - Hospital', '2772 Roxas Blvd, Pasay City', 'irb_sanjuandedios@gmail.com', '(02) 831-9731 loc.1104', '---', 'Rosalinda Ds Pulido, Md', 'Chair', '09178971702', 'roseedoc@gmail.com', '2010-04-03', NULL),
(88, 2, 12, 1, 'Institutional Ethics Review Committee (IERC)', 'San Pedro Hospital', 'C. Guzman St. Davao City', 'sphierc@yahoo.com', '082-2255934', '---', 'Chita I. Nazal-matunog, Md', 'Chair', '09176221025', 'chita@matunog.com', '2007-12-03', NULL),
(89, 2, 12, 1, 'Cluster Research Ethics Review Committee (CRERC)', 'Metro Davao Medical And Research Center Inc. - Anda Riverview Medical Center Inc.', 'Km. 4, J.p Laurel Ave., Bajada, Davao City', 'mdmrcdavao@yahoo.com', '(082) 287-7777', '09177054376', 'Dr. Yvette Y. Tan', 'Member', '09177054376', 'mdmrcdavao@yahoo.com', '2015-01-05', NULL),
(90, 2, 12, 1, 'Research Ethics Review Unit (RERU)', 'San Lazaro Hospital', 'Quiricada St. Sta Cruz, Manila', 'slh.iso.reru@gmail.com', '310-3211', '---', 'Khristine Dell G. Yap, Rn, Man', 'Reru Secretariat', '310-3211', 'slh.iso.reru@gmail.com', '1983-01-01', NULL),
(91, 2, 12, 1, 'Institutional Review Board (IRB)', 'Bicol Regional Training And Teaching Hospital', 'Rizal St. Legazpi City, Province Of Albay, Region V', 'brtth.ord@gmail.com', '(052) 483-0017 loc. 4205', '09399106822', 'Vincent B. Aguilar, Md, Dpps, Dpshbt, Dpsph, Fpspo', 'Chair', '09399106822', 'brtth.ord@gmail.com', '2007-01-01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `recdocuments`
--

CREATE TABLE `recdocuments` (
  `id` int(10) UNSIGNED NOT NULL,
  `recdocuments_user_id` int(10) UNSIGNED NOT NULL,
  `recdocuments_recdetails_id` int(10) UNSIGNED NOT NULL,
  `recdocuments_document_types_id` int(10) UNSIGNED NOT NULL,
  `recdocuments_document_types_id_check` int(10) UNSIGNED DEFAULT NULL,
  `recdocuments_submittophreb` datetime DEFAULT NULL,
  `recdocuments_file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `recdocuments_file_generated` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `recdocuments_fileextension` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `recdocuments_filesize` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rec_dateuploaded` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `recdocuments`
--

INSERT INTO `recdocuments` (`id`, `recdocuments_user_id`, `recdocuments_recdetails_id`, `recdocuments_document_types_id`, `recdocuments_document_types_id_check`, `recdocuments_submittophreb`, `recdocuments_file`, `recdocuments_file_generated`, `recdocuments_fileextension`, `recdocuments_filesize`, `rec_dateuploaded`) VALUES
(5, 2, 36, 2, NULL, NULL, 'VSCMMC Application for Accreditation.docx', '5c46b7272469b', NULL, NULL, '2019-01-22 14:24:39'),
(8, 2, 37, 2, NULL, NULL, 'PHREB ACCREDITATION FORMS 1.1 DRMC.pdf', '5c46c864bb21a', NULL, NULL, '2019-01-22 15:38:12'),
(9, 2, 39, 2, NULL, NULL, 'OLFU-IERC PHREB FORM 1.1.pdf', '5c47c3a23c76f', NULL, NULL, '2019-01-23 09:30:10'),
(10, 2, 44, 2, NULL, NULL, 'Forms 1.5, 1.1, _ 1.3.pdf', '5c529f870a337', NULL, NULL, '2019-01-31 15:11:03'),
(12, 2, 2, 26, NULL, NULL, 'AdU ERC_Appointment Letter.pdf', '5c53a124a353d', NULL, NULL, '2019-02-01 09:30:12'),
(13, 2, 2, 26, NULL, NULL, 'AdU ERC_Letter of Intent.JPG', '5c53a124a5f3e', NULL, NULL, '2019-02-01 09:30:12'),
(14, 2, 2, 26, NULL, NULL, 'AdU ERC_Members CV.zip', '5c53a124a76bc', NULL, NULL, '2019-02-01 09:30:12'),
(15, 2, 2, 26, NULL, NULL, 'AdU ERC_Training Records.zip', '5c53a27086614', NULL, NULL, '2019-02-01 09:35:44'),
(16, 2, 2, 26, NULL, NULL, 'PHREB Form No. 1.2 Annual Report 2017_UERC.zip', '5c53a2708a333', NULL, NULL, '2019-02-01 09:35:44'),
(17, 2, 2, 26, NULL, NULL, 'PHREB Form No. 1.3 Protocol Summary.pdf', '5c53a270a343b', NULL, NULL, '2019-02-01 09:35:44'),
(19, 2, 9, 26, NULL, NULL, 'AdZU ERC_Annual Report.zip', '5c53fda7c65db', NULL, NULL, '2019-02-01 16:04:55'),
(20, 2, 9, 26, NULL, NULL, 'AdZU ERC_Appointment.jpg', '5c53fda7caca4', NULL, NULL, '2019-02-01 16:04:55'),
(21, 2, 9, 26, NULL, NULL, 'AdZU ERC_Letter of Intent.pdf', '5c53fda7e119f', NULL, NULL, '2019-02-01 16:04:55'),
(22, 2, 9, 26, NULL, NULL, 'AdZU ERC_Members CV and Training Records.zip', '5c53fda7e27d1', NULL, NULL, '2019-02-01 16:04:55'),
(23, 2, 9, 26, NULL, NULL, 'AdZU ERC_Minutes of the Meeting.zip', '5c53fda7e6ad5', NULL, NULL, '2019-02-01 16:04:55'),
(24, 2, 9, 26, NULL, NULL, 'AdZU ERC_Organizational Chart.docx', '5c53fda7e97a8', NULL, NULL, '2019-02-01 16:04:55'),
(25, 2, 9, 26, NULL, NULL, 'AdZU ERC_PHREB Form 001.pdf', '5c53fda7eaddc', NULL, NULL, '2019-02-01 16:04:55'),
(26, 2, 9, 26, NULL, NULL, 'AdZU ERC_Protocol Summary.pdf', '5c53fda7ec416', NULL, NULL, '2019-02-01 16:04:55'),
(27, 2, 9, 26, NULL, NULL, 'AdZU ERC_SOP Manual and Forms.pdf', '5c53fda7edb4a', NULL, NULL, '2019-02-01 16:04:55'),
(28, 2, 3, 26, NULL, NULL, 'AU ERB_Annual Report.doc', '5c5406370d823', NULL, NULL, '2019-02-01 16:41:27'),
(29, 2, 3, 26, NULL, NULL, 'AU ERB_Application for Accreditation.pdf', '5c54063710562', NULL, NULL, '2019-02-01 16:41:27'),
(30, 2, 3, 26, NULL, NULL, 'AU ERB_Application Letter.JPG', '5c540637261c9', NULL, NULL, '2019-02-01 16:41:27'),
(31, 2, 3, 26, NULL, NULL, 'AU ERB_Appointment Letter.docx', '5c540637280f4', NULL, NULL, '2019-02-01 16:41:27'),
(32, 2, 3, 26, NULL, NULL, 'AU ERB_Forms.zip', '5c5406372a4d2', NULL, NULL, '2019-02-01 16:41:27'),
(33, 2, 3, 26, NULL, NULL, 'AU ERB_Members CV.zip', '5c5406372c3fb', NULL, NULL, '2019-02-01 16:41:27'),
(34, 2, 3, 26, NULL, NULL, 'AU ERB_Minutes of Meeting.docx', '5c5406372da41', NULL, NULL, '2019-02-01 16:41:27'),
(35, 2, 3, 26, NULL, NULL, 'AU ERB_Organizational Chart.doc', '5c5406372f052', NULL, NULL, '2019-02-01 16:41:27'),
(36, 2, 3, 26, NULL, NULL, 'AU ERB_Picture of Office.JPG', '5c5406373087d', NULL, NULL, '2019-02-01 16:41:27'),
(37, 2, 3, 26, NULL, NULL, 'AU ERB_Protocol Summary.doc', '5c54063732a9a', NULL, NULL, '2019-02-01 16:41:27'),
(38, 2, 3, 26, NULL, NULL, 'AU ERB_SOP.zip', '5c540637349db', NULL, NULL, '2019-02-01 16:41:27'),
(39, 2, 3, 26, NULL, NULL, 'AU ERB_Training Records.zip', '5c5406373605a', NULL, NULL, '2019-02-01 16:41:27'),
(40, 2, 13, 26, NULL, NULL, 'BPSU PREC_ Annual Report.pdf', '5c540961da151', NULL, NULL, '2019-02-01 16:54:57'),
(41, 2, 13, 26, NULL, NULL, 'BPSU PREC_Application for Accreditation.pdf', '5c540961dcd6f', NULL, NULL, '2019-02-01 16:54:57'),
(42, 2, 13, 26, NULL, NULL, 'BPSU PREC_CV and Training Records.zip', '5c540962006ae', NULL, NULL, '2019-02-01 16:54:57'),
(43, 2, 13, 26, NULL, NULL, 'BPSU PREC_Letter of Intent.jpg', '5c54096205fd8', NULL, NULL, '2019-02-01 16:54:57'),
(44, 2, 13, 26, NULL, NULL, 'BPSU PREC_SOP Manual.zip', '5c54096208584', NULL, NULL, '2019-02-01 16:54:57'),
(45, 2, 13, 26, NULL, NULL, 'BPSU PREC_SOP Related Forms.zip', '5c5409620cfa0', NULL, NULL, '2019-02-01 16:54:57'),
(46, 2, 13, 26, NULL, NULL, 'BPSU PREC_TOR and Appointment Letter.zip', '5c5409620f4d9', NULL, NULL, '2019-02-01 16:54:57'),
(47, 2, 13, 26, NULL, NULL, 'BSPU PREC_ Ogranizational Chart.jpg', '5c540962128c3', NULL, NULL, '2019-02-01 16:54:57'),
(48, 2, 5, 26, NULL, NULL, 'CLHRDC ERC_Annual Report.zip', '5c5409dc4f229', NULL, NULL, '2019-02-01 16:57:00'),
(49, 2, 5, 26, NULL, NULL, 'CLHRDC ERC_Application for Accreditation.pdf', '5c5409dc51592', NULL, NULL, '2019-02-01 16:57:00'),
(50, 2, 5, 26, NULL, NULL, 'CLHRDC ERC_Application Letter.jpg', '5c5409dc690ce', NULL, NULL, '2019-02-01 16:57:00'),
(51, 2, 5, 26, NULL, NULL, 'CLHRDC ERC_Minutes of the Meeting.zip', '5c5409dc6a715', NULL, NULL, '2019-02-01 16:57:00'),
(52, 2, 5, 26, NULL, NULL, 'CLHRDC ERC_Protocol Summary.zip', '5c5409dc716ae', NULL, NULL, '2019-02-01 16:57:00'),
(53, 2, 5, 26, NULL, NULL, 'CLHRDC ERC_SOP Related Forms.pdf', '5c5409dc72d09', NULL, NULL, '2019-02-01 16:57:00'),
(54, 2, 5, 26, NULL, NULL, 'CLHRDC ERC_SOP.zip', '5c5409dc7435e', NULL, NULL, '2019-02-01 16:57:00'),
(55, 2, 5, 26, NULL, NULL, 'CLHRDC ERC_TOR and Appointment Letter.zip', '5c5409dc796a8', NULL, NULL, '2019-02-01 16:57:00'),
(56, 2, 5, 26, NULL, NULL, 'CLHRDC ERC_Training Records.zip', '5c5409dc7b2c0', NULL, NULL, '2019-02-01 16:57:00'),
(57, 2, 18, 26, NULL, NULL, 'PHREB Form No. 1.3 Protocol Summary 2017_ USA RERC.pdf', '5c540c77c8ac1', NULL, NULL, '2019-02-01 17:08:07'),
(58, 2, 18, 26, NULL, NULL, 'USA RERC_Annual Report.zip', '5c540c77cd7a0', NULL, NULL, '2019-02-01 17:08:07'),
(59, 2, 18, 26, NULL, NULL, 'USA RERC_Application Letter.pdf', '5c540c77d0426', NULL, NULL, '2019-02-01 17:08:07'),
(60, 2, 18, 26, NULL, NULL, 'USA RERC_Members CV.zip', '5c540c77d1a23', NULL, NULL, '2019-02-01 17:08:07'),
(61, 2, 18, 26, NULL, NULL, 'USA RERC_Minutes of the Meeting.zip', '5c540c77d73c0', NULL, NULL, '2019-02-01 17:08:07'),
(62, 2, 18, 26, NULL, NULL, 'USA RERC_Office Picture.zip', '5c540c77db6ac', NULL, NULL, '2019-02-01 17:08:07'),
(63, 2, 18, 26, NULL, NULL, 'USA RERC_Organizational Chart.pdf', '5c540c77de374', NULL, NULL, '2019-02-01 17:08:07'),
(64, 2, 18, 26, NULL, NULL, 'USA RERC_PHREB FOrm 001.pdf', '5c540c77df95b', NULL, NULL, '2019-02-01 17:08:07'),
(65, 2, 18, 26, NULL, NULL, 'USA RERC_SOP Manual and Forms.zip', '5c540c77e0fbc', NULL, NULL, '2019-02-01 17:08:07'),
(66, 2, 18, 26, NULL, NULL, 'USA RERC_Training Records.pdf', '5c540c77e5295', NULL, NULL, '2019-02-01 17:08:07'),
(67, 2, 18, 26, NULL, NULL, 'USA-RERC TOR and Appointment Letter.zip', '5c540c77e68d5', NULL, NULL, '2019-02-01 17:08:07'),
(68, 2, 14, 26, NULL, NULL, 'CRMC REC_SOP and Forms.zip', '5c540e2177487', NULL, NULL, '2019-02-01 17:15:13'),
(69, 2, 14, 26, NULL, NULL, 'CRMC REC_Application for Accreditation.docx', '5c540e2179e67', NULL, NULL, '2019-02-01 17:15:13'),
(70, 2, 14, 26, NULL, NULL, 'CRMC REC_Letter of Appointment.pdf', '5c540e21919ed', NULL, NULL, '2019-02-01 17:15:13'),
(71, 2, 14, 26, NULL, NULL, 'CRMC REC_Letter of Intent.pdf', '5c540e2193051', NULL, NULL, '2019-02-01 17:15:13'),
(72, 2, 14, 26, NULL, NULL, 'CRMC REC_Members CV.zip', '5c540e2194689', NULL, NULL, '2019-02-01 17:15:13'),
(73, 2, 14, 26, NULL, NULL, 'CRMC REC_Members Training Records.pdf', '5c540e2197343', NULL, NULL, '2019-02-01 17:15:13'),
(74, 2, 14, 26, NULL, NULL, 'CRMC REC_Protocol Summary.docx', '5c540e219897d', NULL, NULL, '2019-02-01 17:15:13'),
(75, 2, 14, 26, NULL, NULL, 'CRMC REC_Office Picures.zip', '5c5411b8dee45', NULL, NULL, '2019-02-01 17:30:32'),
(76, 2, 14, 26, NULL, NULL, 'CRMC REC_Organizational Chart.zip', '5c54120939095', NULL, NULL, '2019-02-01 17:31:53'),
(77, 2, 10, 26, NULL, NULL, 'DLSUD ERC_Minutes of the Meeting.zip', '5c5412635d5a3', NULL, NULL, '2019-02-01 17:33:23'),
(78, 2, 10, 26, NULL, NULL, 'DLSUD ERC_Office Pictures.docx', '5c5412635fe3c', NULL, NULL, '2019-02-01 17:33:23'),
(79, 2, 10, 26, NULL, NULL, 'DLSUD ERC_Organizational Chart.pdf', '5c54126376375', NULL, NULL, '2019-02-01 17:33:23'),
(80, 2, 10, 26, NULL, NULL, 'DLSUD ERC_PHREB Form 1.1.pdf', '5c541263779cd', NULL, NULL, '2019-02-01 17:33:23'),
(81, 2, 10, 26, NULL, NULL, 'DLSUD ERC_Protocol Summary.pdf', '5c5412637a6a5', NULL, NULL, '2019-02-01 17:33:23'),
(82, 2, 10, 26, NULL, NULL, 'DLSUD ERC_Letter of Intent.jpg', '5c5412637bcd0', NULL, NULL, '2019-02-01 17:33:23'),
(83, 2, 10, 26, NULL, NULL, 'DLSUD ERC_Members CV.pdf', '5c5412637dea2', NULL, NULL, '2019-02-01 17:33:23'),
(84, 2, 10, 26, NULL, NULL, 'DLSUD ERC_Appointment Letters.pdf', '5c541296ad185', NULL, NULL, '2019-02-01 17:34:14'),
(85, 2, 10, 26, NULL, NULL, 'DLSUD ERC_Requirements.zip', '5c5412fdd966e', NULL, NULL, '2019-02-01 17:35:57'),
(86, 2, 10, 26, NULL, NULL, 'DLSUD ERC_SOP Manual and Forms.zip', '5c5412fdee480', NULL, NULL, '2019-02-01 17:35:57'),
(87, 2, 10, 26, NULL, NULL, 'DLSUD ERC_Training Records.zip', '5c54132575586', NULL, NULL, '2019-02-01 17:36:37'),
(90, 2, 15, 26, NULL, NULL, 'HAU IRB_Annual Report.zip', '5c5414816b4d7', NULL, NULL, '2019-02-01 17:42:25'),
(91, 2, 15, 26, NULL, NULL, 'HAU IRB_Application for Accreditation.zip', '5c5414816e887', NULL, NULL, '2019-02-01 17:42:25'),
(92, 2, 15, 26, NULL, NULL, 'HAU IRB_Application Letter.JPG', '5c54148172b88', NULL, NULL, '2019-02-01 17:42:25'),
(93, 2, 15, 26, NULL, NULL, 'HAU IRB_Appointment Letter.JPG', '5c5414817420d', NULL, NULL, '2019-02-01 17:42:25'),
(94, 2, 15, 26, NULL, NULL, 'HAU IRB_Members CV and Training Records.zip', '5c54148175844', NULL, NULL, '2019-02-01 17:42:25'),
(95, 2, 15, 26, NULL, NULL, 'HAU IRB_Minutes of the Meeting.zip', '5c54148176ebe', NULL, NULL, '2019-02-01 17:42:25'),
(96, 2, 15, 26, NULL, NULL, 'HAU IRB_Organizational Chart.jpg', '5c5414817d74c', NULL, NULL, '2019-02-01 17:42:25'),
(97, 2, 15, 26, NULL, NULL, 'HAU IRB_Protocol Summary.zip', '5c5414817f4b0', NULL, NULL, '2019-02-01 17:42:25'),
(98, 2, 15, 26, NULL, NULL, 'HAU IRB_SOP Manual.zip', '5c54148183048', NULL, NULL, '2019-02-01 17:42:25'),
(99, 2, 15, 26, NULL, NULL, 'HAU IRB_SOP Related Forms and Letter Templates.zip', '5c5414818738a', NULL, NULL, '2019-02-01 17:42:25'),
(100, 2, 12, 26, NULL, NULL, 'NEUST UERC_Annual Report.zip', '5c5414dc276ae', NULL, NULL, '2019-02-01 17:43:56'),
(101, 2, 12, 26, NULL, NULL, 'NEUST UERC_Application for Accreditation.zip', '5c5414dc2b949', NULL, NULL, '2019-02-01 17:43:56'),
(102, 2, 12, 26, NULL, NULL, 'NEUST UERC_Appointment Letters.zip', '5c5414dc2e633', NULL, NULL, '2019-02-01 17:43:56'),
(103, 2, 12, 26, NULL, NULL, 'NEUST UERC_Letter of Intent.jpg', '5c5414dc312a7', NULL, NULL, '2019-02-01 17:43:56'),
(104, 2, 12, 26, NULL, NULL, 'NEUST UERC_Members Profile.jpg', '5c5414dc32921', NULL, NULL, '2019-02-01 17:43:56'),
(105, 2, 12, 26, NULL, NULL, 'NEUST UERC_Minutes of the Meeting.zip', '5c5414dc34f3d', NULL, NULL, '2019-02-01 17:43:56'),
(106, 2, 12, 26, NULL, NULL, 'NEUST UERC_Office Pictures.docx', '5c5414dc37be6', NULL, NULL, '2019-02-01 17:43:56'),
(107, 2, 12, 26, NULL, NULL, 'NEUST UERC_Organizational Chart.JPG', '5c5414dc3986d', NULL, NULL, '2019-02-01 17:43:56'),
(108, 2, 12, 26, NULL, NULL, 'NEUST UERC_Protocol Summary.jpg', '5c5414dc3af10', NULL, NULL, '2019-02-01 17:43:56'),
(109, 2, 12, 26, NULL, NULL, 'NEUST UERC_SOP Manual and Forms.zip', '5c5414dc3c586', NULL, NULL, '2019-02-01 17:43:56'),
(110, 2, 12, 26, NULL, NULL, 'NEUST UERC_Training Records.zip', '5c5414dc434d1', NULL, NULL, '2019-02-01 17:43:56'),
(111, 2, 22, 26, NULL, NULL, 'ADMU REC_Annual Report.zip', '5c5a984083d8d', 'zip', '1242455', '2019-02-06 16:18:08'),
(112, 2, 22, 26, NULL, NULL, 'ADMU REC_Application for Accreditation.pdf', '5c5a984092fcb', 'pdf', '10636470', '2019-02-06 16:18:08'),
(113, 2, 22, 26, NULL, NULL, 'ADMU REC_Appointment Letter.zip', '5c5a984098576', 'zip', '3737140', '2019-02-06 16:18:08'),
(114, 2, 22, 26, NULL, NULL, 'ADMU REC_Letter of Intent.pdf', '5c5a98409b5a6', 'pdf', '560003', '2019-02-06 16:18:08'),
(115, 2, 22, 26, NULL, NULL, 'ADMU REC_Members Profile.zip', '5c5a98409de9a', 'zip', '510136', '2019-02-06 16:18:08'),
(116, 2, 22, 26, NULL, NULL, 'ADMU REC_Minutes of the Meeting.zip', '5c5a98409f88b', 'zip', '318564', '2019-02-06 16:18:08'),
(117, 2, 22, 26, NULL, NULL, 'ADMU REC_Office Pictures.zip', '5c5a9840a0ef9', 'zip', '356311', '2019-02-06 16:18:08'),
(118, 2, 22, 26, NULL, NULL, 'ADMU REC_Protocol Summary.pdf', '5c5a9840a2543', 'pdf', '468148', '2019-02-06 16:18:08'),
(119, 2, 22, 26, NULL, NULL, 'ADMU REC_SOP Manual and Forms.zip', '5c5a9840a3b61', 'zip', '7337481', '2019-02-06 16:18:08'),
(120, 2, 25, 26, NULL, NULL, 'BATMC RERC_Annual Report.pdf', '5c5a9dac94920', 'pdf', '3128833', '2019-02-06 16:41:16'),
(121, 2, 25, 26, NULL, NULL, 'BATMC RERC_Application for Accreditation.docx', '5c5a9dac9885b', 'docx', '220787', '2019-02-06 16:41:16'),
(122, 2, 25, 26, NULL, NULL, 'BATMC RERC_Application Letter.docx', '5c5a9dac99e64', 'docx', '550763', '2019-02-06 16:41:16'),
(123, 2, 25, 26, NULL, NULL, 'BATMC RERC_Appointment Letters.zip', '5c5a9dac9b4c3', 'zip', '926273', '2019-02-06 16:41:16'),
(124, 2, 25, 26, NULL, NULL, 'BATMC RERC_Members CV.zip', '5c5a9dac9caf1', 'zip', '3480424', '2019-02-06 16:41:16'),
(125, 2, 25, 26, NULL, NULL, 'BATMC RERC_Minutes of the Meeting.zip', '5c5a9dac9f7d5', 'zip', '1555195', '2019-02-06 16:41:16'),
(126, 2, 25, 26, NULL, NULL, 'BATMC RERC_Office Pictures.zip', '5c5a9daca1b8c', 'zip', '362655', '2019-02-06 16:41:16'),
(127, 2, 25, 26, NULL, NULL, 'BATMC RERC_Organizational Chart.JPG', '5c5a9daca3b42', 'JPG', '2086317', '2019-02-06 16:41:16'),
(128, 2, 25, 26, NULL, NULL, 'BATMC RERC_Protocol Summary.zip', '5c5a9daca5e48', 'zip', '584779', '2019-02-06 16:41:16'),
(129, 2, 25, 26, NULL, NULL, 'BATMC RERC_SOP Manual and Forms.pdf', '5c5a9daca943b', 'pdf', '2533175', '2019-02-06 16:41:16'),
(130, 2, 25, 26, NULL, NULL, 'BATMC RERC_Training Records.zip', '5c5a9dacab7ba', 'zip', '14193035', '2019-02-06 16:41:16'),
(131, 2, 40, 26, NULL, NULL, 'CVMC RERC_Annual Report.docx', '5c5a9e3502dee', 'docx', '95579', '2019-02-06 16:43:33'),
(132, 2, 40, 26, NULL, NULL, 'CVMC RERC_Application for Accreditation.zip', '5c5a9e3504a8a', 'zip', '388431', '2019-02-06 16:43:33'),
(133, 2, 40, 26, NULL, NULL, 'CVMC RERC_Appointment Letters.zip', '5c5a9e351c621', 'zip', '1170083', '2019-02-06 16:43:33'),
(134, 2, 40, 26, NULL, NULL, 'CVMC RERC_Letter of Intent.JPG', '5c5a9e351e884', 'JPG', '1441838', '2019-02-06 16:43:33'),
(135, 2, 40, 26, NULL, NULL, 'CVMC RERC_Members CV.zip', '5c5a9e35208ff', 'zip', '6564669', '2019-02-06 16:43:33'),
(136, 2, 40, 26, NULL, NULL, 'CVMC RERC_Minutes of the Meeting.zip', '5c5a9e35241d0', 'zip', '270456', '2019-02-06 16:43:33'),
(137, 2, 40, 26, NULL, NULL, 'CVMC RERC_Office Pictures.zip', '5c5a9e35257c9', 'zip', '11318798', '2019-02-06 16:43:33'),
(138, 2, 40, 26, NULL, NULL, 'CVMC RERC_Organizational CHart.docx', '5c5a9e352a553', 'docx', '126745', '2019-02-06 16:43:33'),
(139, 2, 40, 26, NULL, NULL, 'CVMC RERC_Protocol Summary.pdf', '5c5a9e352bb8e', 'pdf', '6094742', '2019-02-06 16:43:33'),
(140, 2, 40, 26, NULL, NULL, 'CVMC RERC_SOP Forms.zip', '5c5a9e352f446', 'zip', '1308772', '2019-02-06 16:43:33'),
(141, 2, 40, 26, NULL, NULL, 'CVMC RERC_SOP Manual.zip', '5c5a9e35314e4', 'zip', '2798955', '2019-02-06 16:43:33'),
(142, 2, 40, 26, NULL, NULL, 'CVMC RERC_Training Certificates.zip', '5c5a9e3533735', 'zip', '1903095', '2019-02-06 16:43:33'),
(143, 2, 29, 26, NULL, NULL, 'DMSFH REC_Office Pictures.zip', '5c5a9e956f16f', 'zip', '1571645', '2019-02-06 16:45:09'),
(144, 2, 29, 26, NULL, NULL, 'DMSFH REC_SOP Manual.pdf', '5c5a9e95717ca', 'pdf', '2355839', '2019-02-06 16:45:09'),
(145, 2, 29, 26, NULL, NULL, 'DMSFH REC_SOP Related Forms.zip', '5c5a9e957393e', 'zip', '664400', '2019-02-06 16:45:09'),
(146, 2, 29, 26, NULL, NULL, 'DMSFH REC_Training Records.zip', '5c5a9e9574f83', 'zip', '1330609', '2019-02-06 16:45:09'),
(147, 2, 29, 26, NULL, NULL, 'DMSFH REC_Application for Accreditation.docx', '5c5a9e9577100', 'docx', '107927', '2019-02-06 16:45:09'),
(148, 2, 29, 26, NULL, NULL, 'DMSFH REC_Appointment Letters.zip', '5c5a9e957872c', 'zip', '278500', '2019-02-06 16:45:09'),
(149, 2, 29, 26, NULL, NULL, 'DMSFH REC_Letter of Intent.docx', '5c5a9e9579d8b', 'docx', '42437', '2019-02-06 16:45:09'),
(150, 2, 29, 26, NULL, NULL, 'DMSFH REC_Members CV.zip', '5c5a9e957b3ff', 'zip', '1174407', '2019-02-06 16:45:09'),
(151, 2, 29, 26, NULL, NULL, 'DMSFH REC_Minutes of the Meeting.zip', '5c5a9e957d583', 'zip', '397084', '2019-02-06 16:45:09'),
(152, 2, 29, 26, NULL, NULL, 'DMSFH REC_Protocol Summary.pdf', '5c5a9fff008a5', 'pdf', '1828288', '2019-02-06 16:51:11'),
(153, 2, 29, 26, NULL, NULL, 'DMSFH REC_Annual Report.pdf', '5c5a9fff03cbc', 'pdf', '5677657', '2019-02-06 16:51:11'),
(154, 2, 37, 26, NULL, NULL, 'DRMC REC_Letter of Intent.JPG', '5c5aa07f14f25', 'JPG', '155252', '2019-02-06 16:53:19'),
(155, 2, 37, 26, NULL, NULL, 'DRMC REC_Agenda and Minutes of the Meeting.zip', '5c5aa07f1747a', 'zip', '27914305', '2019-02-06 16:53:19'),
(156, 2, 37, 26, NULL, NULL, 'DRMC REC_Application for Accreditation.pdf', '5c5aa07f20a21', 'pdf', '2880490', '2019-02-06 16:53:19'),
(157, 2, 37, 26, NULL, NULL, 'DRMC REC_Appointment Letters.zip', '5c5aa07f23707', 'zip', '19287495', '2019-02-06 16:53:19'),
(158, 2, 37, 26, NULL, NULL, 'DRMC REC_Members CV.zip', '5c5aa091f1ce7', 'zip', '12532728', '2019-02-06 16:53:37'),
(159, 2, 37, 26, NULL, NULL, 'DRMC REC_Office Pictures.pdf', '5c5aa09203bfb', 'pdf', '1529308', '2019-02-06 16:53:37'),
(160, 2, 37, 26, NULL, NULL, 'DRMC REC_Organizational Chart.JPG', '5c5aa09205224', 'JPG', '181252', '2019-02-06 16:53:37'),
(161, 2, 37, 26, NULL, NULL, 'DRMC REC_Protocol Summary.docx', '5c5aa09206851', 'docx', '198894', '2019-02-06 16:53:37'),
(162, 2, 37, 26, NULL, NULL, 'DRMC REC_SOP Manual and Forms.zip', '5c5aa09207e8c', 'zip', '17209945', '2019-02-06 16:53:37'),
(163, 2, 37, 26, NULL, NULL, 'DRMC REC_Training Records.zip', '5c5aa0920d820', 'zip', '41045137', '2019-02-06 16:53:37'),
(164, 2, 23, 26, NULL, NULL, 'DLSU REC_ Minutes of the Meeting.zip', '5c5aa112587f0', 'zip', '1411882', '2019-02-06 16:55:46'),
(165, 2, 23, 26, NULL, NULL, 'DLSU REC_Annual Report.zip', '5c5aa1125b8ca', 'zip', '755016', '2019-02-06 16:55:46'),
(166, 2, 23, 26, NULL, NULL, 'DLSU REC_Application for Accreditation.pdf', '5c5aa1125cf4a', 'pdf', '314859', '2019-02-06 16:55:46'),
(167, 2, 23, 26, NULL, NULL, 'DLSU REC_Application Letter.pdf', '5c5aa1125e55d', 'pdf', '182557', '2019-02-06 16:55:46'),
(168, 2, 23, 26, NULL, NULL, 'DLSU REC_Appointment Letters.zip', '5c5aa1125fc05', 'zip', '2194328', '2019-02-06 16:55:46'),
(169, 2, 23, 26, NULL, NULL, 'DLSU REC_Members CV.zip', '5c5aa11261237', 'zip', '16081275', '2019-02-06 16:55:46'),
(170, 2, 23, 26, NULL, NULL, 'DLSU REC_Organizational Structure.pdf', '5c5aa11266ba8', 'pdf', '87434', '2019-02-06 16:55:46'),
(171, 2, 23, 26, NULL, NULL, 'DLSU REC_Protocol Summary.zip', '5c5aa1126933f', 'zip', '2689858', '2019-02-06 16:55:46'),
(172, 2, 23, 26, NULL, NULL, 'DLSU REC_SOP Manual and Forms.zip', '5c5aa1126af3e', 'zip', '4668245', '2019-02-06 16:55:46'),
(173, 2, 23, 26, NULL, NULL, 'DLSU REC_Training Records.zip', '5c5aa1126dbc9', 'zip', '5914273', '2019-02-06 16:55:46'),
(174, 2, 26, 26, NULL, NULL, 'EVHRDC ERC_Annual Report.doc', '5c5aa15bc6364', 'doc', '286720', '2019-02-06 16:56:59'),
(175, 2, 26, 26, NULL, NULL, 'EVHRDC ERC_Application for Accreditation.zip', '5c5aa15bc8656', 'zip', '371377', '2019-02-06 16:56:59'),
(176, 2, 26, 26, NULL, NULL, 'EVHRDC ERC_Letter of Intent.PDF', '5c5aa15bc9c67', 'PDF', '285251', '2019-02-06 16:56:59'),
(177, 2, 26, 26, NULL, NULL, 'EVHRDC ERC_Members CV.zip', '5c5aa15bcb2ce', 'zip', '1483216', '2019-02-06 16:56:59'),
(178, 2, 26, 26, NULL, NULL, 'EVHRDC ERC_Minutes of the Meeting.zip', '5c5aa15bcc940', 'zip', '4557906', '2019-02-06 16:56:59'),
(179, 2, 26, 26, NULL, NULL, 'EVHRDC ERC_Office Pictures.zip', '5c5aa15bcf633', 'zip', '237144', '2019-02-06 16:56:59'),
(180, 2, 26, 26, NULL, NULL, 'EVHRDC ERC_Organizational Chart.pdf', '5c5aa15bd0c35', 'pdf', '137312', '2019-02-06 16:56:59'),
(181, 2, 26, 26, NULL, NULL, 'EVHRDC ERC_Protocol Summary.zip', '5c5aa15bd226e', 'zip', '335486', '2019-02-06 16:56:59'),
(182, 2, 26, 26, NULL, NULL, 'EVHRDC ERC_TOR.zip', '5c5aa15bd38a9', 'zip', '3520003', '2019-02-06 16:56:59'),
(183, 2, 26, 26, NULL, NULL, 'EVHRDC ERC_Training Records.zip', '5c5aa15bd6590', 'zip', '2064351', '2019-02-06 16:56:59'),
(184, 2, 26, 26, NULL, NULL, 'EVHRDS ERC_SOP Manual and Forms.zip', '5c5aa15bd8e11', 'zip', '2754281', '2019-02-06 16:56:59'),
(185, 2, 33, 26, NULL, NULL, 'EVRMC IERC_Organizational Chart.pdf', '5c5aa1b8d8908', 'pdf', '268532', '2019-02-06 16:58:32'),
(186, 2, 33, 26, NULL, NULL, 'EVRMC IERC_SOP Manual and Forms.zip', '5c5aa1b8dbdf9', 'zip', '945338', '2019-02-06 16:58:32'),
(187, 2, 33, 26, NULL, NULL, 'EVRMC IERC_Application for Accreditation.zip', '5c5aa1b8dd494', 'zip', '475487', '2019-02-06 16:58:32'),
(188, 2, 33, 26, NULL, NULL, 'EVRMC IERC_Letter of Intent.JPG', '5c5aa1b8deb05', 'JPG', '173972', '2019-02-06 16:58:32'),
(189, 2, 33, 26, NULL, NULL, 'EVRMC IERC_Members CV and Training Records.zip', '5c5aa1b8e0134', 'zip', '8456947', '2019-02-06 16:58:32'),
(190, 2, 33, 26, NULL, NULL, 'EVRMC IERC_Minutes of the Meeting.zip', '5c5aa1b8e443e', 'zip', '1911549', '2019-02-06 16:58:32'),
(191, 2, 33, 26, NULL, NULL, 'EVRMC IERC_Office Pictures.docx', '5c5aa1b8e5a98', 'docx', '2086211', '2019-02-06 16:58:32'),
(192, 2, 33, 26, NULL, NULL, 'EVRMC IERC_Annual Report.zip', '5c5aa208180e0', 'zip', '4573155', '2019-02-06 16:59:52'),
(193, 2, 33, 26, NULL, NULL, 'EVRMC IERC_Protocol Summary.zip', '5c5aa2081b599', 'zip', '4867366', '2019-02-06 16:59:52'),
(194, 2, 35, 26, NULL, NULL, 'MJH ERC_Annual Report.zip', '5c5aa24016d01', 'zip', '779051', '2019-02-06 17:00:48'),
(195, 2, 35, 26, NULL, NULL, 'MJH ERC_Application for Accreditation.pdf', '5c5aa24019e62', 'pdf', '1858078', '2019-02-06 17:00:48'),
(196, 2, 35, 26, NULL, NULL, 'MJH ERC_Appointment Letter.pdf', '5c5aa2401b4c2', 'pdf', '3744035', '2019-02-06 17:00:48'),
(197, 2, 35, 26, NULL, NULL, 'MJH ERC_Members CV.zip', '5c5aa2401def4', 'zip', '2956340', '2019-02-06 17:00:48'),
(198, 2, 35, 26, NULL, NULL, 'MJH ERC_Minutes of the Meeting.zip', '5c5aa2402248e', 'zip', '6775713', '2019-02-06 17:00:48'),
(199, 2, 35, 26, NULL, NULL, 'MJH ERC_Office Pictures.pdf', '5c5aa2402513f', 'pdf', '2842499', '2019-02-06 17:00:48'),
(200, 2, 35, 26, NULL, NULL, 'MJH ERC_Organizational Structure.pdf', '5c5aa2402674c', 'pdf', '696762', '2019-02-06 17:00:48'),
(201, 2, 35, 26, NULL, NULL, 'MJH ERC_Protocol Summary.pdf', '5c5aa24027de7', 'pdf', '719522', '2019-02-06 17:00:48'),
(202, 2, 35, 26, NULL, NULL, 'MJH ERC_SOP Manual and Forms.pdf', '5c5aa24029435', 'pdf', '22646962', '2019-02-06 17:00:48'),
(203, 2, 35, 26, NULL, NULL, 'MJH ERC_Training Records.pdf', '5c5aa240303fb', 'pdf', '8935240', '2019-02-06 17:00:48'),
(204, 2, 39, 26, NULL, NULL, 'OLGU IERC_SOP Forms.zip', '5c5aa2c49a9a6', 'zip', '2979765', '2019-02-06 17:03:00'),
(205, 2, 39, 26, NULL, NULL, 'OLFU IERC_Annual Report.docx', '5c5aa2c49d03d', 'docx', '223144', '2019-02-06 17:03:00'),
(206, 2, 39, 26, NULL, NULL, 'OLFU IERC_Application for Accreditation.pdf', '5c5aa2c4b357e', 'pdf', '7388981', '2019-02-06 17:03:00'),
(207, 2, 39, 26, NULL, NULL, 'OLFU IERC_Appointment Letters.pdf', '5c5aa2c4b7861', 'pdf', '10073095', '2019-02-06 17:03:00'),
(208, 2, 39, 26, NULL, NULL, 'OLFU IERC_Letter for Extension.pdf', '5c5aa2c4bd1bb', 'pdf', '353514', '2019-02-06 17:03:00'),
(209, 2, 39, 26, NULL, NULL, 'OLFU IERC_Members CV.pdf', '5c5aa2c4be818', 'pdf', '24862797', '2019-02-06 17:03:00'),
(210, 2, 39, 26, NULL, NULL, 'OLFU IERC_Minutes of the Meeting.zip', '5c5aa2c4c6dab', 'zip', '49342859', '2019-02-06 17:03:00'),
(211, 2, 39, 26, NULL, NULL, 'OLFU IERC_Organizational Chart.pdf', '5c5aa2c4d36da', 'pdf', '814468', '2019-02-06 17:03:00'),
(212, 2, 39, 26, NULL, NULL, 'OLFU IERC_Protocol Summary.pdf', '5c5aa2c4d79f8', 'pdf', '1309838', '2019-02-06 17:03:00'),
(213, 2, 39, 26, NULL, NULL, 'OLFU IERC_SOP Manual.pdf', '5c5aa2c4d900c', 'pdf', '1766692', '2019-02-06 17:03:00'),
(214, 2, 39, 26, NULL, NULL, 'OLFU IERC_Training Records.zip', '5c5aa2c4da62a', 'zip', '1676009', '2019-02-06 17:03:00'),
(215, 2, 30, 26, NULL, NULL, 'TDF IRB_Training Records.zip', '5c5aa3245f9c3', 'zip', '1480751', '2019-02-06 17:04:36'),
(216, 2, 30, 26, NULL, NULL, 'TDF IRB_Annual Report.pdf', '5c5aa32462623', 'pdf', '371846', '2019-02-06 17:04:36'),
(217, 2, 30, 26, NULL, NULL, 'TDF IRB_Application for Accreditation.pdf', '5c5aa3247752b', 'pdf', '482205', '2019-02-06 17:04:36'),
(218, 2, 30, 26, NULL, NULL, 'TDF IRB_Application Letter.pdf', '5c5aa32478b5c', 'pdf', '132815', '2019-02-06 17:04:36'),
(219, 2, 30, 26, NULL, NULL, 'TDF IRB_Appointment Letters.zip', '5c5aa3247a1d1', 'zip', '2185489', '2019-02-06 17:04:36'),
(220, 2, 30, 26, NULL, NULL, 'TDF IRB_Members CV.zip', '5c5aa3247b80c', 'zip', '13107331', '2019-02-06 17:04:36'),
(221, 2, 30, 26, NULL, NULL, 'TDF IRB_Minutes of the Meeting.pdf', '5c5aa32481147', 'pdf', '441563', '2019-02-06 17:04:36'),
(222, 2, 30, 26, NULL, NULL, 'TDF IRB_Office Pictures.zip', '5c5aa32482786', 'zip', '5472090', '2019-02-06 17:04:36'),
(223, 2, 30, 26, NULL, NULL, 'TDF IRB_Organizational Chart.jpg', '5c5aa3248543e', 'jpg', '61462', '2019-02-06 17:04:36'),
(224, 2, 30, 26, NULL, NULL, 'TDF IRB_Protocol Summary.docx', '5c5aa32486a61', 'docx', '99336', '2019-02-06 17:04:36'),
(225, 2, 30, 26, NULL, NULL, 'TDF IRB_SOP Forms.zip', '5c5aa324880d8', 'zip', '231698', '2019-02-06 17:04:36'),
(226, 2, 30, 26, NULL, NULL, 'TDF IRB_SOP Manual.pdf', '5c5aa3248adb7', 'pdf', '784598', '2019-02-06 17:04:36'),
(227, 2, 31, 26, NULL, NULL, 'UIC REC_Training Records.zip', '5c5aa362538be', 'zip', '35460604', '2019-02-06 17:05:38'),
(228, 2, 31, 26, NULL, NULL, 'UIC REC_Application for Accreditation.pdf', '5c5aa3625cd06', 'pdf', '464580', '2019-02-06 17:05:38'),
(229, 2, 31, 26, NULL, NULL, 'UIC REC_Minutes of the Meeting.zip', '5c5aa3627321b', 'zip', '599518', '2019-02-06 17:05:38'),
(230, 2, 31, 26, NULL, NULL, 'UIC REC_Office Pictures.pdf', '5c5aa3627486d', 'pdf', '823718', '2019-02-06 17:05:38'),
(231, 2, 31, 26, NULL, NULL, 'UIC REC_Organizatonal Chart.png', '5c5aa36275eac', 'png', '41163', '2019-02-06 17:05:38'),
(232, 2, 31, 26, NULL, NULL, 'UIC REC_Protocol Summary.pdf', '5c5aa36277500', 'pdf', '957363', '2019-02-06 17:05:38'),
(233, 2, 31, 26, NULL, NULL, 'UIC REC_SOP Manual.zip', '5c5aa36278b43', 'zip', '6233108', '2019-02-06 17:05:38'),
(234, 2, 44, 26, NULL, NULL, 'UM ERC_Training Records.zip', '5c5aa3c78c523', 'zip', '6187982', '2019-02-06 17:07:19'),
(235, 2, 44, 26, NULL, NULL, 'UM ERC_Agenda and Minutes of the Meeting.pdf', '5c5aa3c790775', 'pdf', '9688908', '2019-02-06 17:07:19'),
(236, 2, 44, 26, NULL, NULL, 'UM ERC_Application for Accreditation and Protocol Summary.pdf', '5c5aa3c794a30', 'pdf', '23566313', '2019-02-06 17:07:19'),
(237, 2, 44, 26, NULL, NULL, 'UM ERC_Appointment Letters.zip', '5c5aa3c79e533', 'zip', '327446', '2019-02-06 17:07:19'),
(238, 2, 44, 26, NULL, NULL, 'UM ERC_Letter of Intent.jpg', '5c5aa3c79fc8b', 'jpg', '114382', '2019-02-06 17:07:19'),
(239, 2, 44, 26, NULL, NULL, 'UM ERC_Members CV.zip', '5c5aa3c7a1328', 'zip', '7070714', '2019-02-06 17:07:19'),
(240, 2, 44, 26, NULL, NULL, 'UM ERC_Office Pictures.pdf', '5c5aa3c7a560b', 'pdf', '3197604', '2019-02-06 17:07:19'),
(241, 2, 44, 26, NULL, NULL, 'UM ERC_SOP Manual and Forms.zip', '5c5aa3c7a82b3', 'zip', '5463851', '2019-02-06 17:07:19'),
(253, 2, 20, 26, NULL, NULL, 'UST-CRS ERC_Training Certificates.zip', '5c5aa537f08fd', 'zip', '2670803', '2019-02-06 17:13:27'),
(254, 2, 20, 26, NULL, NULL, 'UST-CRS ERC_Annual Report.pdf', '5c5aa537f3903', 'pdf', '366770', '2019-02-06 17:13:27'),
(255, 2, 20, 26, NULL, NULL, 'UST-CRS ERC_Application for Accreditation.pdf', '5c5aa53800cf9', 'pdf', '1473917', '2019-02-06 17:13:27'),
(256, 2, 20, 26, NULL, NULL, 'UST-CRS ERC_Application Letter.pdf', '5c5aa53802348', 'pdf', '82975', '2019-02-06 17:13:27'),
(257, 2, 20, 26, NULL, NULL, 'UST-CRS ERC_Appointment Letters.zip', '5c5aa53803967', 'zip', '929725', '2019-02-06 17:13:27'),
(258, 2, 20, 26, NULL, NULL, 'UST-CRS ERC_Members CV.zip', '5c5aa53804fc5', 'zip', '8891490', '2019-02-06 17:13:27'),
(259, 2, 20, 26, NULL, NULL, 'UST-CRS ERC_Minutes of the Meeting.zip', '5c5aa538092c6', 'zip', '423564', '2019-02-06 17:13:27'),
(260, 2, 20, 26, NULL, NULL, 'UST-CRS ERC_Office Pictures.pdf', '5c5aa5380a944', 'pdf', '572389', '2019-02-06 17:13:27'),
(261, 2, 20, 26, NULL, NULL, 'UST-CRS ERC_Organizational Chart.JPG', '5c5aa5380bf95', 'JPG', '137959', '2019-02-06 17:13:27'),
(262, 2, 20, 26, NULL, NULL, 'UST-CRS ERC_Protocol Summary.pdf', '5c5aa5380d5c0', 'pdf', '525244', '2019-02-06 17:13:27'),
(263, 2, 20, 26, NULL, NULL, 'UST-CRS ERC_SOP Manual and Forms.pdf', '5c5aa5380ec23', 'pdf', '41316886', '2019-02-06 17:13:27'),
(264, 2, 21, 26, NULL, NULL, 'UST-GS ERC_Training Certificates.zip', '5c5aa5ad94e6b', 'zip', '4186351', '2019-02-06 17:15:25'),
(265, 2, 21, 26, NULL, NULL, 'UST-GS ERC_Annual Report.zip', '5c5aa5ad98e8d', 'zip', '805652', '2019-02-06 17:15:25'),
(266, 2, 21, 26, NULL, NULL, 'UST-GS ERC_Application for Accreditation.pdf', '5c5aa5ad9a4ca', 'pdf', '319705', '2019-02-06 17:15:25'),
(267, 2, 21, 26, NULL, NULL, 'UST-GS ERC_Appointment Letters.zip', '5c5aa5ad9badb', 'zip', '4154670', '2019-02-06 17:15:25'),
(268, 2, 21, 26, NULL, NULL, 'UST-GS ERC_Letter of Intent.pdf', '5c5aa5ad9e7ae', 'pdf', '79249', '2019-02-06 17:15:25'),
(269, 2, 21, 26, NULL, NULL, 'UST-GS ERC_Members CV.zip', '5c5aa5ad9fe0f', 'zip', '3948052', '2019-02-06 17:15:25'),
(270, 2, 21, 26, NULL, NULL, 'UST-GS ERC_Minutes of the Meeting.zip', '5c5aa5ada4133', 'zip', '766216', '2019-02-06 17:15:25'),
(271, 2, 21, 26, NULL, NULL, 'UST-GS ERC_Office Pictures.pdf', '5c5aa5ada574c', 'pdf', '262335', '2019-02-06 17:15:25'),
(272, 2, 21, 26, NULL, NULL, 'UST-GS ERC_Organizational Chart.jpg', '5c5aa5ada6db1', 'jpg', '172303', '2019-02-06 17:15:25'),
(273, 2, 21, 26, NULL, NULL, 'UST-GS ERC_Protocol Summary.zip', '5c5aa5ada8432', 'zip', '1106799', '2019-02-06 17:15:25'),
(274, 2, 21, 26, NULL, NULL, 'UST-GS ERC_SOP Forms.zip', '5c5aa5ada9a79', 'zip', '486425', '2019-02-06 17:15:25'),
(275, 2, 21, 26, NULL, NULL, 'UST-GS ERC_SOP Manual.pdf', '5c5aa5adab08f', 'pdf', '3019870', '2019-02-06 17:15:25'),
(276, 2, 34, 26, NULL, NULL, 'UV IRB_Training Records.zip', '5c5aa6e97153c', 'zip', '2070926', '2019-02-06 17:20:41'),
(277, 2, 34, 26, NULL, NULL, 'UV IRB_Application for Accreditation.docx', '5c5aa6e974165', 'docx', '58043', '2019-02-06 17:20:41'),
(278, 2, 34, 26, NULL, NULL, 'UV IRB_Appointment Certificate.docx', '5c5aa6e98b045', 'docx', '55309', '2019-02-06 17:20:41'),
(279, 2, 34, 26, NULL, NULL, 'UV IRB_Letter of Intent.docx', '5c5aa6e98c669', 'docx', '42761', '2019-02-06 17:20:41'),
(280, 2, 34, 26, NULL, NULL, 'UV IRB_Members CV.zip', '5c5aa6e98dcc5', 'zip', '709708', '2019-02-06 17:20:41'),
(281, 2, 34, 26, NULL, NULL, 'UV IRB_Minutes of the Meeting.zip', '5c5aa6e98f2bb', 'zip', '6819545', '2019-02-06 17:20:41'),
(282, 2, 34, 26, NULL, NULL, 'UV IRB_Office Pictures.docx', '5c5aa6e992c98', 'docx', '6182712', '2019-02-06 17:20:41'),
(283, 2, 34, 26, NULL, NULL, 'UV IRB_Organizational Chart.zip', '5c5aa6e996282', 'zip', '487103', '2019-02-06 17:20:41'),
(284, 2, 34, 26, NULL, NULL, 'UV IRB_Protocol Summary.pdf', '5c5aa6e997914', 'pdf', '531598', '2019-02-06 17:20:41'),
(285, 2, 34, 26, NULL, NULL, 'UV IRB_SOP Manual and Forms.zip', '5c5aa6e998f27', 'zip', '14301252', '2019-02-06 17:20:41'),
(286, 2, 32, 26, NULL, NULL, 'VC ERC_Training Records.zip', '5c5aa71c3f5bb', 'zip', '162483', '2019-02-06 17:21:32'),
(287, 2, 32, 26, NULL, NULL, 'VC ERC_Annual Report.zip', '5c5aa71c41db5', 'zip', '131448', '2019-02-06 17:21:32'),
(288, 2, 32, 26, NULL, NULL, 'VC ERC_Application for Accreditation.DOCX', '5c5aa71c59947', 'DOCX', '52914', '2019-02-06 17:21:32'),
(289, 2, 32, 26, NULL, NULL, 'VC ERC_Application Letter.DOCX', '5c5aa71c5af57', 'DOCX', '51174', '2019-02-06 17:21:32'),
(290, 2, 32, 26, NULL, NULL, 'VC ERC_Appointment Letter.pdf', '5c5aa71c5c5ce', 'pdf', '603673', '2019-02-06 17:21:32'),
(291, 2, 32, 26, NULL, NULL, 'VC ERC_Members CV.zip', '5c5aa71c5e5ea', 'zip', '318962', '2019-02-06 17:21:32'),
(292, 2, 32, 26, NULL, NULL, 'VC ERC_Minutes of the Meeting.zip', '5c5aa71c5fc7f', 'zip', '188945', '2019-02-06 17:21:32'),
(293, 2, 32, 26, NULL, NULL, 'VC ERC_Office Pictures.pdf', '5c5aa71c612c2', 'pdf', '1432191', '2019-02-06 17:21:32'),
(294, 2, 32, 26, NULL, NULL, 'VC ERC_Organizational Chart.pdf', '5c5aa71c63575', 'pdf', '478442', '2019-02-06 17:21:32'),
(295, 2, 32, 26, NULL, NULL, 'VC ERC_Protocol Summary.DOCX', '5c5aa71c655b2', 'DOCX', '150915', '2019-02-06 17:21:32'),
(296, 2, 32, 26, NULL, NULL, 'VC ERC_SOP Manual and Forms.zip', '5c5aa71c66c0c', 'zip', '764597', '2019-02-06 17:21:32'),
(297, 2, 24, 26, NULL, NULL, 'VRH IRB_Training Records.zip', '5c5aaa4e31889', 'zip', '2497039', '2019-02-06 17:35:10'),
(298, 2, 24, 26, NULL, NULL, 'VRH IRB_Annual Report.zip', '5c5aaa4e34e74', 'zip', '4798510', '2019-02-06 17:35:10'),
(299, 2, 24, 26, NULL, NULL, 'VRH IRB_Application for Accreditation.docx', '5c5aaa4e37ab6', 'docx', '56702', '2019-02-06 17:35:10'),
(300, 2, 24, 26, NULL, NULL, 'VRH IRB_Appointment Letter.zip', '5c5aaa4e390fe', 'zip', '3477196', '2019-02-06 17:35:10'),
(301, 2, 24, 26, NULL, NULL, 'VRH IRB_Letter of Intent.png', '5c5aaa4e3a731', 'png', '18631', '2019-02-06 17:35:10'),
(302, 2, 24, 26, NULL, NULL, 'VRH IRB_Members CV.zip', '5c5aaa4e3be00', 'zip', '1380064', '2019-02-06 17:35:10'),
(303, 2, 24, 26, NULL, NULL, 'VRH IRB_Minutes of the Meeting.zip', '5c5aaa4e3d3f6', 'zip', '9615362', '2019-02-06 17:35:10'),
(304, 2, 24, 26, NULL, NULL, 'VRH IRB_Office Pictures.zip', '5c5aaa4e400c4', 'zip', '12959750', '2019-02-06 17:35:10'),
(305, 2, 24, 26, NULL, NULL, 'VRH IRB_Organizational Chart.docx', '5c5aaa4e44392', 'docx', '68750', '2019-02-06 17:35:10'),
(306, 2, 24, 26, NULL, NULL, 'VRH IRB_Protocol Summary.docx', '5c5aaa4e45a0b', 'docx', '139737', '2019-02-06 17:35:10'),
(307, 2, 24, 26, NULL, NULL, 'VRH IRB_SOP Manual and Forms.zip', '5c5aaa4e47066', 'zip', '4312905', '2019-02-06 17:35:10'),
(308, 2, 36, 26, NULL, NULL, 'VSMMC REC_Organizational Structure.pdf', '5c5aaab2ba9cc', 'pdf', '36884', '2019-02-06 17:36:50'),
(309, 2, 36, 26, NULL, NULL, 'VSMMC REC_Annual Report.docx', '5c5aaab2bc268', 'docx', '116833', '2019-02-06 17:36:50'),
(310, 2, 36, 26, NULL, NULL, 'VSMMC REC_Application for Accreditation.docx', '5c5aaab2bd8a1', 'docx', '58523', '2019-02-06 17:36:50'),
(311, 2, 36, 26, NULL, NULL, 'VSMMC REC_Letter of Intent.pdf', '5c5aaab2bef0b', 'pdf', '657645', '2019-02-06 17:36:50'),
(312, 2, 36, 26, NULL, NULL, 'VSMMC REC_Protocol Summary.docx', '5c5aaab2c0570', 'docx', '275124', '2019-02-06 17:36:50'),
(313, 2, 36, 26, NULL, NULL, 'VSMMC REC_SOP Manual and Forms.zip', '5c5aaab2c1bab', 'zip', '1966239', '2019-02-06 17:36:50'),
(314, 2, 27, 26, NULL, NULL, 'WMSU REOC_Training Records.pdf', '5c5aaaebd40c8', 'pdf', '3732207', '2019-02-06 17:37:47'),
(315, 2, 27, 26, NULL, NULL, 'WMSU REOC_Annual Report.doc', '5c5aaaebd6a6a', 'doc', '752128', '2019-02-06 17:37:47'),
(316, 2, 27, 26, NULL, NULL, 'WMSU REOC_Application for Accreditation.doc', '5c5aaaebecf6d', 'doc', '683520', '2019-02-06 17:37:47'),
(317, 2, 27, 26, NULL, NULL, 'WMSU REOC_Application Letter.docx', '5c5aaaebee5e6', 'docx', '405788', '2019-02-06 17:37:47'),
(318, 2, 27, 26, NULL, NULL, 'WMSU REOC_Appointment Letter.doc', '5c5aaaebefc25', 'doc', '5144064', '2019-02-06 17:37:47'),
(319, 2, 27, 26, NULL, NULL, 'WMSU REOC_Members CV.doc', '5c5aaaebf28a1', 'doc', '1886720', '2019-02-06 17:37:47'),
(320, 2, 27, 26, NULL, NULL, 'WMSU REOC_Minutes of the Meeting.zip', '5c5aaaebf3f27', 'zip', '183587', '2019-02-06 17:37:47'),
(321, 2, 27, 26, NULL, NULL, 'WMSU REOC_Office Pictures.zip', '5c5aaaec0132b', 'zip', '1409648', '2019-02-06 17:37:47'),
(322, 2, 27, 26, NULL, NULL, 'WMSU REOC_Organizational Chart.docx', '5c5aaaec0294a', 'docx', '114075', '2019-02-06 17:37:47'),
(323, 2, 27, 26, NULL, NULL, 'WMSU REOC_Protocol Summary.zip', '5c5aaaec03fcf', 'zip', '385363', '2019-02-06 17:37:47'),
(324, 2, 27, 26, NULL, NULL, 'WMSU REOC_SOP Manual and Forms.pdf', '5c5aaaec05621', 'pdf', '5665796', '2019-02-06 17:37:47'),
(325, 2, 28, 26, NULL, NULL, 'ZCMC ERB_Training Records.zip', '5c5aab2ac7696', 'zip', '4809438', '2019-02-06 17:38:50'),
(326, 2, 28, 26, NULL, NULL, 'ZCMC ERB_Annual Report.docx', '5c5aab2acb776', 'docx', '93668', '2019-02-06 17:38:50'),
(327, 2, 28, 26, NULL, NULL, 'ZCMC ERB_Application for Accreditation.pdf', '5c5aab2accdc8', 'pdf', '289862', '2019-02-06 17:38:50'),
(328, 2, 28, 26, NULL, NULL, 'ZCMC ERB_Appointment Letter.jpg', '5c5aab2ace3dd', 'jpg', '857141', '2019-02-06 17:38:50'),
(329, 2, 28, 26, NULL, NULL, 'ZCMC ERB_Letter of Intent.pdf', '5c5aab2acfa30', 'pdf', '198020', '2019-02-06 17:38:50'),
(330, 2, 28, 26, NULL, NULL, 'ZCMC ERB_Members CV.zip', '5c5aab2ad10ab', 'zip', '3274605', '2019-02-06 17:38:50'),
(331, 2, 28, 26, NULL, NULL, 'ZCMC ERB_Minutes of the Meeting.zip', '5c5aab2ad3cf5', 'zip', '4106316', '2019-02-06 17:38:50'),
(332, 2, 28, 26, NULL, NULL, 'ZCMC ERB_Office Pictures.zip', '5c5aab2ad69d0', 'zip', '317124', '2019-02-06 17:38:50'),
(333, 2, 28, 26, NULL, NULL, 'ZCMC ERB_Organizational Chart.zip', '5c5aab2ad801b', 'zip', '267966', '2019-02-06 17:38:50'),
(334, 2, 28, 26, NULL, NULL, 'ZCMC ERB_Protocol Summary.docx', '5c5aab2ad9671', 'docx', '139807', '2019-02-06 17:38:50'),
(335, 2, 28, 26, NULL, NULL, 'ZCMC ERB_SOP Manual and Forms.zip', '5c5aab2adacdf', 'zip', '12847760', '2019-02-06 17:38:50'),
(336, 2, 52, 26, NULL, NULL, 'NEC_Training Records.zip', '5c5aac049c3dc', 'zip', '31958385', '2019-02-06 17:42:28'),
(337, 2, 52, 26, NULL, NULL, 'NEC_Annual Reports.zip', '5c5aac04a5b3d', 'zip', '1222450', '2019-02-06 17:42:28'),
(338, 2, 52, 26, NULL, NULL, 'NEC_Application for Accreditation.pdf', '5c5aac04bbf8d', 'pdf', '334733', '2019-02-06 17:42:28'),
(339, 2, 52, 26, NULL, NULL, 'NEC_Letter of Intent.pdf', '5c5aac04bd6dc', 'pdf', '166893', '2019-02-06 17:42:28'),
(340, 2, 52, 26, NULL, NULL, 'NEC_Members CV.zip', '5c5aac04bed00', 'zip', '2412364', '2019-02-06 17:42:28'),
(341, 2, 52, 26, NULL, NULL, 'NEC_Minutes of the Meeting.zip', '5c5aac04c044d', 'zip', '436911', '2019-02-06 17:42:28'),
(342, 2, 52, 26, NULL, NULL, 'NEC_Office Picture.zip', '5c5aac04c19db', 'zip', '472005', '2019-02-06 17:42:28'),
(343, 2, 52, 26, NULL, NULL, 'NEC_Protocol Summary.zip', '5c5aac04c3004', 'zip', '729394', '2019-02-06 17:42:28'),
(344, 2, 52, 26, NULL, NULL, 'NEC_SOP Manual and Forms.zip', '5c5aac04c4647', 'zip', '1564972', '2019-02-06 17:42:28');

-- --------------------------------------------------------

--
-- Table structure for table `recdocumentsremarks`
--

CREATE TABLE `recdocumentsremarks` (
  `id` int(10) UNSIGNED NOT NULL,
  `remarks_user_id` int(10) UNSIGNED NOT NULL,
  `remarks_recdetails_id` int(10) UNSIGNED NOT NULL,
  `remarks_recdocuments_types_id` int(10) UNSIGNED NOT NULL,
  `remarks_message_recdocuments` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reclists`
--

CREATE TABLE `reclists` (
  `id` int(10) UNSIGNED NOT NULL,
  `reclist_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reclist_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reclists`
--

INSERT INTO `reclists` (`id`, `reclist_name`, `reclist_description`) VALUES
(1, 'Research Ethics Committee (REC)', '---'),
(2, 'Ethics Review Committee (ERC)', '---'),
(3, 'Institutional Review Board (IRB)', '---'),
(4, 'Institutional Ethics Review Committee (IERC)', '---'),
(5, 'Institutional Ethics Review Board (IERB)', '---'),
(6, 'Research Ethics Board (REB)', '---'),
(7, 'Research Ethics Review Board (RERB)', '---'),
(8, 'Research Ethics Oversight Committee (REOC)', '---'),
(9, 'Hospital Ethics Committee (HEC)', '---'),
(10, 'University Ethics Research Committee (UERC)', '---'),
(11, 'Research Ethics Review Committee (RERC)', '---'),
(12, 'Cluster Research Ethics Committee (CREC)', '---'),
(13, 'Cluster Research Ethics Review Committee (CRERC)', '---'),
(14, 'Ethics Review Board (ERB)', '---'),
(15, 'Independent Ethics Committee (IEC)', '---'),
(16, 'Research Ethics Review Unit (RERU)', '---');

-- --------------------------------------------------------

--
-- Table structure for table `regions`
--

CREATE TABLE `regions` (
  `id` int(10) UNSIGNED NOT NULL,
  `region_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `regions`
--

INSERT INTO `regions` (`id`, `region_name`) VALUES
(1, 'NCR - National Capital Region'),
(2, 'CAR - Cordillera Administrative Region'),
(3, 'REGION I (Ilocos Region)'),
(4, 'REGION II (Cagayan Valley)'),
(5, 'REGION III (Central Luzon)'),
(6, 'REGION IV-A (CALABARZON)'),
(7, 'REGION IV-B (MIMAROPA Region)'),
(8, 'REGION V (Bicol Region)'),
(9, 'REGION VI (Western Visayas)'),
(10, 'REGION VII (Central Visayas)'),
(11, 'REGION VIII (Eastern Visayas)'),
(12, 'REGION IX (Zamboanga Peninsula)'),
(13, 'REGION X (Northern Mindanao)'),
(14, 'REGION XI (Davao Region)'),
(15, 'REGION XII (SOCCSKSARGEN)'),
(16, 'REGION XIII (CARAGA)'),
(17, 'ARMM - Autonomous Region in Muslim Mindanao');

-- --------------------------------------------------------

--
-- Table structure for table `reportings`
--

CREATE TABLE `reportings` (
  `id` int(10) UNSIGNED NOT NULL,
  `report_user_id` int(10) UNSIGNED NOT NULL,
  `report_start_date` date NOT NULL,
  `report_end_date` date NOT NULL,
  `report_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reportings`
--

INSERT INTO `reportings` (`id`, `report_user_id`, `report_start_date`, `report_end_date`, `report_status`, `created_at`, `updated_at`) VALUES
(1, 1, '2010-01-01', '2019-01-31', NULL, '2019-01-15 06:35:51', '2019-01-15 06:35:51');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role_name`, `slug`, `permissions`) VALUES
(1, 'SuperAdmin', 'superadmin', '{\"overall-functions\":true}'),
(2, 'Secretariat Incharge', 'secretariatincharge', '{\"incharge-functions\":true}'),
(3, 'Research Ethics Commitee', 'rec', '{\"rec-functions\":true}'),
(4, 'Accreditor', 'accreditor', '{\"accreditor-functions\":true}'),
(5, 'CSA Chair', 'csachair', '{\"csachair-functions\":true}');

-- --------------------------------------------------------

--
-- Table structure for table `role_users`
--

CREATE TABLE `role_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_assigned_level_id` int(10) UNSIGNED NOT NULL DEFAULT '5'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_users`
--

INSERT INTO `role_users` (`id`, `user_id`, `role_id`, `user_assigned_level_id`) VALUES
(1, 1, 1, 5),
(2, 2, 2, 1),
(4, 4, 2, 1),
(5, 5, 2, 3),
(6, 6, 2, 4),
(7, 7, 2, 4),
(8, 8, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sendtoaccreditors`
--

CREATE TABLE `sendtoaccreditors` (
  `sendacc_tbl_id` int(10) UNSIGNED NOT NULL,
  `a_recdetails_id` int(10) UNSIGNED NOT NULL,
  `a_sender_user_id` int(10) UNSIGNED NOT NULL,
  `a_accreditors_user_id` int(10) UNSIGNED NOT NULL,
  `sendaccreditors_status` bigint(20) NOT NULL DEFAULT '0',
  `a_datesubmitted` datetime NOT NULL,
  `a_dateofexpiry` date NOT NULL,
  `a_reminder_status` datetime DEFAULT NULL,
  `a_reminder_status_deadline` datetime DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `send_orderpayments`
--

CREATE TABLE `send_orderpayments` (
  `id` int(10) UNSIGNED NOT NULL,
  `or_sender_id` int(10) UNSIGNED NOT NULL,
  `or_recdetails_id` int(10) UNSIGNED NOT NULL,
  `or_serialno` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `or_file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `or_file_generated` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `or_datecreated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `statuses`
--

CREATE TABLE `statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `status_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_color` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `statuses`
--

INSERT INTO `statuses` (`id`, `status_name`, `status_color`) VALUES
(1, 'Pending', '#132c84'),
(2, 'Completed Requirements', '#00ff04'),
(3, 'Incomplete Requirements', '#ffe500'),
(4, 'Accredited', '#ffa100'),
(5, 'Withdrawn', '#ff00d8'),
(6, 'Provisional', '#d0ff00'),
(7, 'Submission Received', '#00ffe1'),
(8, 'Re-Accreditation', '#00c3ff'),
(9, 'Re-submission of Requirements', '#e25f5f'),
(10, 'Submission of Action Plan and Compliance Evidences', '#e22f52'),
(11, 'Deferred', '#ff0000'),
(12, 'New', '#ff0000'),
(13, 'Renewal', '#ff0000'),
(14, 'Upgrade', '#ff0000'),
(15, 'Re-accreditation', '#ff0000'),
(16, 'Accredited (Already Accredited)', '#ff0000'),
(17, 'Accreditation Expired', '#ff0000'),
(18, 'REC Application has been changed', '#ff0000'),
(19, 'On-going Application', '#ff0000');

-- --------------------------------------------------------

--
-- Table structure for table `submissionstatuses`
--

CREATE TABLE `submissionstatuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `submissionstatuses_recdetails_id` int(10) UNSIGNED NOT NULL,
  `submissionstatuses_user_id` int(10) UNSIGNED NOT NULL,
  `submissionstatuses_statuses_id` int(10) UNSIGNED NOT NULL,
  `submissionstatuses_datecreated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `verified` tinyint(4) NOT NULL DEFAULT '0',
  `email_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `verified`, `email_token`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'PHREB-AS Admin', 'phrebaccreditationsystem@gmail.com', '$2y$10$aSF6t2hCkxEXqzj1lx/Y.eY0NHKGhquLpaamekAXVsIOCEqQP26iK', 1, 'cGhyZWJhY2NyZWRpdGF0aW9uc3lzdGVtQGdtYWlsLmNvbQ==', 'm3sPrmL2hWzdAcRbjCgznWLUJ47mAeeIRjc44Va63J1fPlEX14Z2OOT1csOb', '2019-01-15 06:35:51', '2019-01-15 06:35:51', NULL),
(2, 'John Marc Cambonga', 'jrcambonga@pchrd.dost.gov.ph', '$2y$10$r2gabOh0I0KZlRASWPaPGOhMZfEbGWqcP7gQ4zE3JU0L8afi1.iGu', 1, 'anJjYW1ib25nYUBwY2hyZC5kb3N0Lmdvdi5waA==', 'bJpOHMChWhyAuEmAEWDCeQb2kXDg5L3oGrkW9Uyn1gjLQ4GEDMn4d0FFd8UH', '2019-01-15 07:26:11', '2019-01-15 07:26:11', NULL),
(4, 'Rochelle G. Tabi', 'rgtabi@pchrd.dost.gov.ph', '$2y$10$Tlr72etlfmcJuV18EXGCEOyusq3SRz1Wqs8Uah6DU6MDUAvk4/V9G', 1, 'cmd0YWJpQHBjaHJkLmRvc3QuZ292LnBo', '0kquf8ovwx5KclKFkvi4VeywqlXuQsvUD8yPXFN9vxBqqMCZOcvsGY2Db9rz', '2019-01-16 01:00:41', '2019-01-16 01:00:41', NULL),
(5, 'Joyce Cordon', 'jccordon@pchrd.dost.gov.ph', '$2y$10$x.3CyLk669KjESIz0ArRUe2lTrNDP60p50ZU1z9QmRkSANbCBH2W2', 1, 'amNjb3Jkb25AcGNocmQuZG9zdC5nb3YucGg=', NULL, '2019-01-16 01:01:54', '2019-01-16 01:01:54', NULL),
(6, 'Marie Jeanne Berroya', 'mbberroya@pchrd.dost.gov.ph', '$2y$10$gjOsVbFFBRKtFoh.tgxfQOPlbDNVCWvSGOFc0AZOr1zKq6u7vLd6i', 1, 'bWJiZXJyb3lhQHBjaHJkLmRvc3QuZ292LnBo', 'bnG82n9ZNzLafVayZ8mvj1wV3XqT0QopJwulC89fQLF4Ltzv8kvMNVFQ4qG8', '2019-01-16 01:03:57', '2019-01-16 01:03:57', NULL),
(7, 'Mariel R. Cervero', 'marielrcervero1988@gmail.com', '$2y$10$WyStpqko9.aJow1GFIUIQOBn4KhgBdatjUiKavgnBwdS0lqLd3fn.', 1, 'bWFyaWVscmNlcnZlcm8xOTg4QGdtYWlsLmNvbQ==', 'AwlndYzqaxgEu4G0hufhcreV0gDHH4hlpzYohw19UvE6kABXfiqYPxhepRyx', '2019-01-16 05:39:06', '2019-01-16 05:39:06', NULL),
(8, 'Julie Ann P. Para', 'jap.pmeu@gmail.com', '$2y$10$5VCUeFlkn/eJv6MV7TW40udK7XZs9uhfvSGDK2/7jhJtq6JEKb7GS', 1, 'amFwLnBtZXVAZ21haWw=', 'qfCWakBNWtAMxA3S6ora9WLYVjYAiBaV4ahYbfmOacwMKOyyZJKt0RLYz7cE', '2019-01-23 06:07:42', '2019-01-23 06:09:41', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accreditorslists`
--
ALTER TABLE `accreditorslists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `accreditors_evaluations`
--
ALTER TABLE `accreditors_evaluations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `accreditors_id` (`accreditors_id`),
  ADD KEY `evaluatedby_user_id` (`evaluatedby_user_id`);

--
-- Indexes for table `accreditors_evaluation_criterias`
--
ALTER TABLE `accreditors_evaluation_criterias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `accreditors_eval_tables`
--
ALTER TABLE `accreditors_eval_tables`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `annual_reports`
--
ALTER TABLE `annual_reports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `annual_reports_ar_recdetails_id_foreign` (`ar_recdetails_id`),
  ADD KEY `annual_reports_ar_recdocuments_id_foreign` (`ar_recdocuments_id`),
  ADD KEY `annual_reports_ar_title_id_foreign` (`ar_title_id`);

--
-- Indexes for table `annual_report_titles`
--
ALTER TABLE `annual_report_titles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `applicationlevel`
--
ALTER TABLE `applicationlevel`
  ADD PRIMARY KEY (`id`),
  ADD KEY `applicationlevel_recdetails_id_foreign` (`recdetails_id`),
  ADD KEY `applicationlevel_level_id_foreign` (`level_id`),
  ADD KEY `applicationlevel_statuses_id_foreign` (`statuses_id`),
  ADD KEY `applicationlevel_region_id_foreign` (`region_id`);

--
-- Indexes for table `applicationlevel_reminders`
--
ALTER TABLE `applicationlevel_reminders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `applicationlevel_reminders_recdetails_id_foreign` (`recdetails_id`);

--
-- Indexes for table `awardingletters`
--
ALTER TABLE `awardingletters`
  ADD PRIMARY KEY (`id`),
  ADD KEY `awardingletters_a_letter_recdetails_id_foreign` (`a_letter_recdetails_id`),
  ADD KEY `awardingletters_a_letter_saveduser_id_foreign` (`a_letter_saveduser_id`),
  ADD KEY `awardingletters_a_letter_doctypes_foreign` (`a_letter_doctypes`);

--
-- Indexes for table `chatmessages`
--
ALTER TABLE `chatmessages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `chatmessages_send_user_id_foreign` (`send_user_id`),
  ADD KEY `chatmessages_reciever_user_id_foreign` (`reciever_user_id`);

--
-- Indexes for table `document_types`
--
ALTER TABLE `document_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emaillogs`
--
ALTER TABLE `emaillogs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `emaillogs_e_sender_user_id_foreign` (`e_sender_user_id`),
  ADD KEY `emaillogs_e_recdetails_id_foreign` (`e_recdetails_id`);

--
-- Indexes for table `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `esigs`
--
ALTER TABLE `esigs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `esigs_esig_user_id_foreign` (`esig_user_id`),
  ADD KEY `esigs_esig_addedby_foreign` (`esig_addedby`);

--
-- Indexes for table `existing_arps`
--
ALTER TABLE `existing_arps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `existing_arps_recdetails_id_foreign` (`recdetails_id`),
  ADD KEY `existing_arps_selectarps_foreign` (`selectarps`);

--
-- Indexes for table `existing_recs`
--
ALTER TABLE `existing_recs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `existing_recs_recdetails_id_foreign` (`recdetails_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `levels`
--
ALTER TABLE `levels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `recactionplancsas`
--
ALTER TABLE `recactionplancsas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `recactionplancsas_ap_csa_ap_id_foreign` (`ap_csa_ap_id`),
  ADD KEY `recactionplancsas_ap_csa_recdetails_id_foreign` (`ap_csa_recdetails_id`),
  ADD KEY `recactionplancsas_ap_csa_sender_id_foreign` (`ap_csa_sender_id`),
  ADD KEY `recactionplancsas_ap_csa_csachair_id_foreign` (`ap_csa_csachair_id`),
  ADD KEY `recactionplancsas_ap_csa_rec_id_foreign` (`ap_csa_rec_id`);

--
-- Indexes for table `recactionplans`
--
ALTER TABLE `recactionplans`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `recactionplans_ap_recactionplan_uniqid_unique` (`ap_recactionplan_uniqid`),
  ADD KEY `recactionplans_ap_recdetails_id_foreign` (`ap_recdetails_id`),
  ADD KEY `recactionplans_ap_sender_id_foreign` (`ap_sender_id`),
  ADD KEY `recactionplans_ap_document_types_foreign` (`ap_document_types`);

--
-- Indexes for table `recassessmentrecs`
--
ALTER TABLE `recassessmentrecs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `recassessmentrecs_ra_rec_ra_id_foreign` (`ra_rec_ra_id`),
  ADD KEY `recassessmentrecs_ra_rec_recdetails_id_foreign` (`ra_rec_recdetails_id`),
  ADD KEY `recassessmentrecs_ra_rec_sender_id_foreign` (`ra_rec_sender_id`),
  ADD KEY `recassessmentrecs_ra_rec_csachair_id_foreign` (`ra_rec_csachair_id`);

--
-- Indexes for table `recassessments`
--
ALTER TABLE `recassessments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `recassessments_ra_user_id_foreign` (`ra_user_id`),
  ADD KEY `recassessments_ra_recdetails_id_foreign` (`ra_recdetails_id`),
  ADD KEY `recassessments_ra_documents_types_id_foreign` (`ra_documents_types_id`);

--
-- Indexes for table `recassessmentscsas`
--
ALTER TABLE `recassessmentscsas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `recassessmentscsas_ra_csa_ra_id_foreign` (`ra_csa_ra_id`),
  ADD KEY `recassessmentscsas_ra_csa_sender_id_foreign` (`ra_csa_sender_id`),
  ADD KEY `recassessmentscsas_ra_csa_csachair_id_foreign` (`ra_csa_csachair_id`),
  ADD KEY `recassessmentscsas_ra_csa_recdetails_id_foreign` (`ra_csa_recdetails_id`);

--
-- Indexes for table `recchairs`
--
ALTER TABLE `recchairs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `recchairs_recdetails_id_foreign` (`recdetails_id`);

--
-- Indexes for table `recdetails`
--
ALTER TABLE `recdetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `recdetails_user_id_foreign` (`user_id`),
  ADD KEY `recdetails_rec_apptype_id_foreign` (`rec_apptype_id`);

--
-- Indexes for table `recdocuments`
--
ALTER TABLE `recdocuments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `recdocuments_recdocuments_user_id_foreign` (`recdocuments_user_id`),
  ADD KEY `recdocuments_recdocuments_recdetails_id_foreign` (`recdocuments_recdetails_id`),
  ADD KEY `recdocuments_recdocuments_document_types_id_foreign` (`recdocuments_document_types_id`);

--
-- Indexes for table `recdocumentsremarks`
--
ALTER TABLE `recdocumentsremarks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `recdocumentsremarks_remarks_user_id_foreign` (`remarks_user_id`),
  ADD KEY `recdocumentsremarks_remarks_recdetails_id_foreign` (`remarks_recdetails_id`),
  ADD KEY `recdocumentsremarks_remarks_recdocuments_types_id_foreign` (`remarks_recdocuments_types_id`);

--
-- Indexes for table `reclists`
--
ALTER TABLE `reclists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `regions`
--
ALTER TABLE `regions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reportings`
--
ALTER TABLE `reportings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reportings_report_user_id_foreign` (`report_user_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`);

--
-- Indexes for table `role_users`
--
ALTER TABLE `role_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `role_id` (`user_id`),
  ADD KEY `role_users_role_id_foreign` (`role_id`),
  ADD KEY `role_users_user_assigned_level_id_foreign` (`user_assigned_level_id`);

--
-- Indexes for table `sendtoaccreditors`
--
ALTER TABLE `sendtoaccreditors`
  ADD PRIMARY KEY (`sendacc_tbl_id`),
  ADD KEY `sendtoaccreditors_a_recdetails_id_foreign` (`a_recdetails_id`),
  ADD KEY `sendtoaccreditors_a_sender_user_id_foreign` (`a_sender_user_id`),
  ADD KEY `sendtoaccreditors_a_accreditors_user_id_foreign` (`a_accreditors_user_id`);

--
-- Indexes for table `send_orderpayments`
--
ALTER TABLE `send_orderpayments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `send_orderpayments_or_sender_id_foreign` (`or_sender_id`),
  ADD KEY `send_orderpayments_or_recdetails_id_foreign` (`or_recdetails_id`);

--
-- Indexes for table `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `submissionstatuses`
--
ALTER TABLE `submissionstatuses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `submissionstatuses_submissionstatuses_recdetails_id_foreign` (`submissionstatuses_recdetails_id`),
  ADD KEY `submissionstatuses_submissionstatuses_user_id_foreign` (`submissionstatuses_user_id`),
  ADD KEY `submissionstatuses_submissionstatuses_statuses_id_foreign` (`submissionstatuses_statuses_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accreditorslists`
--
ALTER TABLE `accreditorslists`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `accreditors_evaluations`
--
ALTER TABLE `accreditors_evaluations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=169;

--
-- AUTO_INCREMENT for table `accreditors_evaluation_criterias`
--
ALTER TABLE `accreditors_evaluation_criterias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `accreditors_eval_tables`
--
ALTER TABLE `accreditors_eval_tables`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT for table `annual_reports`
--
ALTER TABLE `annual_reports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `annual_report_titles`
--
ALTER TABLE `annual_report_titles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `applicationlevel`
--
ALTER TABLE `applicationlevel`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `applicationlevel_reminders`
--
ALTER TABLE `applicationlevel_reminders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `awardingletters`
--
ALTER TABLE `awardingletters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chatmessages`
--
ALTER TABLE `chatmessages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `document_types`
--
ALTER TABLE `document_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `emaillogs`
--
ALTER TABLE `emaillogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `esigs`
--
ALTER TABLE `esigs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `existing_arps`
--
ALTER TABLE `existing_arps`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `existing_recs`
--
ALTER TABLE `existing_recs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `levels`
--
ALTER TABLE `levels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;

--
-- AUTO_INCREMENT for table `recactionplancsas`
--
ALTER TABLE `recactionplancsas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `recactionplans`
--
ALTER TABLE `recactionplans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `recassessmentrecs`
--
ALTER TABLE `recassessmentrecs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `recassessments`
--
ALTER TABLE `recassessments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `recassessmentscsas`
--
ALTER TABLE `recassessmentscsas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `recchairs`
--
ALTER TABLE `recchairs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `recdetails`
--
ALTER TABLE `recdetails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `recdocuments`
--
ALTER TABLE `recdocuments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=345;

--
-- AUTO_INCREMENT for table `recdocumentsremarks`
--
ALTER TABLE `recdocumentsremarks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reclists`
--
ALTER TABLE `reclists`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `regions`
--
ALTER TABLE `regions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `reportings`
--
ALTER TABLE `reportings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `role_users`
--
ALTER TABLE `role_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `sendtoaccreditors`
--
ALTER TABLE `sendtoaccreditors`
  MODIFY `sendacc_tbl_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `send_orderpayments`
--
ALTER TABLE `send_orderpayments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `statuses`
--
ALTER TABLE `statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `submissionstatuses`
--
ALTER TABLE `submissionstatuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `accreditors_evaluations`
--
ALTER TABLE `accreditors_evaluations`
  ADD CONSTRAINT `accreditors_evaluations_ibfk_1` FOREIGN KEY (`accreditors_id`) REFERENCES `accreditorslists` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `accreditors_evaluations_ibfk_2` FOREIGN KEY (`evaluatedby_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `annual_reports`
--
ALTER TABLE `annual_reports`
  ADD CONSTRAINT `annual_reports_ar_recdetails_id_foreign` FOREIGN KEY (`ar_recdetails_id`) REFERENCES `recdetails` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `annual_reports_ar_recdocuments_id_foreign` FOREIGN KEY (`ar_recdocuments_id`) REFERENCES `recdocuments` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `annual_reports_ar_title_id_foreign` FOREIGN KEY (`ar_title_id`) REFERENCES `annual_report_titles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `applicationlevel`
--
ALTER TABLE `applicationlevel`
  ADD CONSTRAINT `applicationlevel_level_id_foreign` FOREIGN KEY (`level_id`) REFERENCES `levels` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `applicationlevel_recdetails_id_foreign` FOREIGN KEY (`recdetails_id`) REFERENCES `recdetails` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `applicationlevel_region_id_foreign` FOREIGN KEY (`region_id`) REFERENCES `regions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `applicationlevel_statuses_id_foreign` FOREIGN KEY (`statuses_id`) REFERENCES `statuses` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `applicationlevel_reminders`
--
ALTER TABLE `applicationlevel_reminders`
  ADD CONSTRAINT `applicationlevel_reminders_recdetails_id_foreign` FOREIGN KEY (`recdetails_id`) REFERENCES `recdetails` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `awardingletters`
--
ALTER TABLE `awardingletters`
  ADD CONSTRAINT `awardingletters_a_letter_doctypes_foreign` FOREIGN KEY (`a_letter_doctypes`) REFERENCES `document_types` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `awardingletters_a_letter_recdetails_id_foreign` FOREIGN KEY (`a_letter_recdetails_id`) REFERENCES `recdetails` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `awardingletters_a_letter_saveduser_id_foreign` FOREIGN KEY (`a_letter_saveduser_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `chatmessages`
--
ALTER TABLE `chatmessages`
  ADD CONSTRAINT `chatmessages_reciever_user_id_foreign` FOREIGN KEY (`reciever_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `chatmessages_send_user_id_foreign` FOREIGN KEY (`send_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `emaillogs`
--
ALTER TABLE `emaillogs`
  ADD CONSTRAINT `emaillogs_e_recdetails_id_foreign` FOREIGN KEY (`e_recdetails_id`) REFERENCES `recdetails` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `emaillogs_e_sender_user_id_foreign` FOREIGN KEY (`e_sender_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `esigs`
--
ALTER TABLE `esigs`
  ADD CONSTRAINT `esigs_esig_addedby_foreign` FOREIGN KEY (`esig_addedby`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `esigs_esig_user_id_foreign` FOREIGN KEY (`esig_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `existing_arps`
--
ALTER TABLE `existing_arps`
  ADD CONSTRAINT `existing_arps_recdetails_id_foreign` FOREIGN KEY (`recdetails_id`) REFERENCES `recdetails` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `existing_arps_selectarps_foreign` FOREIGN KEY (`selectarps`) REFERENCES `document_types` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `existing_recs`
--
ALTER TABLE `existing_recs`
  ADD CONSTRAINT `existing_recs_recdetails_id_foreign` FOREIGN KEY (`recdetails_id`) REFERENCES `recdetails` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `recactionplancsas`
--
ALTER TABLE `recactionplancsas`
  ADD CONSTRAINT `recactionplancsas_ap_csa_ap_id_foreign` FOREIGN KEY (`ap_csa_ap_id`) REFERENCES `recactionplans` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `recactionplancsas_ap_csa_csachair_id_foreign` FOREIGN KEY (`ap_csa_csachair_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `recactionplancsas_ap_csa_rec_id_foreign` FOREIGN KEY (`ap_csa_rec_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `recactionplancsas_ap_csa_recdetails_id_foreign` FOREIGN KEY (`ap_csa_recdetails_id`) REFERENCES `recdetails` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `recactionplancsas_ap_csa_sender_id_foreign` FOREIGN KEY (`ap_csa_sender_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `recactionplans`
--
ALTER TABLE `recactionplans`
  ADD CONSTRAINT `recactionplans_ap_document_types_foreign` FOREIGN KEY (`ap_document_types`) REFERENCES `document_types` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `recactionplans_ap_recdetails_id_foreign` FOREIGN KEY (`ap_recdetails_id`) REFERENCES `recdetails` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `recactionplans_ap_sender_id_foreign` FOREIGN KEY (`ap_sender_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `recassessmentrecs`
--
ALTER TABLE `recassessmentrecs`
  ADD CONSTRAINT `recassessmentrecs_ra_rec_csachair_id_foreign` FOREIGN KEY (`ra_rec_csachair_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `recassessmentrecs_ra_rec_ra_id_foreign` FOREIGN KEY (`ra_rec_ra_id`) REFERENCES `recassessments` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `recassessmentrecs_ra_rec_recdetails_id_foreign` FOREIGN KEY (`ra_rec_recdetails_id`) REFERENCES `recdetails` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `recassessmentrecs_ra_rec_sender_id_foreign` FOREIGN KEY (`ra_rec_sender_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `recassessments`
--
ALTER TABLE `recassessments`
  ADD CONSTRAINT `recassessments_ra_documents_types_id_foreign` FOREIGN KEY (`ra_documents_types_id`) REFERENCES `document_types` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `recassessments_ra_recdetails_id_foreign` FOREIGN KEY (`ra_recdetails_id`) REFERENCES `recdetails` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `recassessments_ra_user_id_foreign` FOREIGN KEY (`ra_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `recassessmentscsas`
--
ALTER TABLE `recassessmentscsas`
  ADD CONSTRAINT `recassessmentscsas_ra_csa_csachair_id_foreign` FOREIGN KEY (`ra_csa_csachair_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `recassessmentscsas_ra_csa_ra_id_foreign` FOREIGN KEY (`ra_csa_ra_id`) REFERENCES `recassessments` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `recassessmentscsas_ra_csa_recdetails_id_foreign` FOREIGN KEY (`ra_csa_recdetails_id`) REFERENCES `recdetails` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `recassessmentscsas_ra_csa_sender_id_foreign` FOREIGN KEY (`ra_csa_sender_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `recchairs`
--
ALTER TABLE `recchairs`
  ADD CONSTRAINT `recchairs_recdetails_id_foreign` FOREIGN KEY (`recdetails_id`) REFERENCES `recdetails` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `recdetails`
--
ALTER TABLE `recdetails`
  ADD CONSTRAINT `recdetails_rec_apptype_id_foreign` FOREIGN KEY (`rec_apptype_id`) REFERENCES `statuses` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `recdetails_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `recdocuments`
--
ALTER TABLE `recdocuments`
  ADD CONSTRAINT `recdocuments_recdocuments_document_types_id_foreign` FOREIGN KEY (`recdocuments_document_types_id`) REFERENCES `document_types` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `recdocuments_recdocuments_recdetails_id_foreign` FOREIGN KEY (`recdocuments_recdetails_id`) REFERENCES `recdetails` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `recdocuments_recdocuments_user_id_foreign` FOREIGN KEY (`recdocuments_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `recdocumentsremarks`
--
ALTER TABLE `recdocumentsremarks`
  ADD CONSTRAINT `recdocumentsremarks_remarks_recdetails_id_foreign` FOREIGN KEY (`remarks_recdetails_id`) REFERENCES `recdetails` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `recdocumentsremarks_remarks_recdocuments_types_id_foreign` FOREIGN KEY (`remarks_recdocuments_types_id`) REFERENCES `document_types` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `recdocumentsremarks_remarks_user_id_foreign` FOREIGN KEY (`remarks_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `reportings`
--
ALTER TABLE `reportings`
  ADD CONSTRAINT `reportings_report_user_id_foreign` FOREIGN KEY (`report_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_users`
--
ALTER TABLE `role_users`
  ADD CONSTRAINT `role_users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_users_user_assigned_level_id_foreign` FOREIGN KEY (`user_assigned_level_id`) REFERENCES `levels` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sendtoaccreditors`
--
ALTER TABLE `sendtoaccreditors`
  ADD CONSTRAINT `sendtoaccreditors_a_accreditors_user_id_foreign` FOREIGN KEY (`a_accreditors_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sendtoaccreditors_a_recdetails_id_foreign` FOREIGN KEY (`a_recdetails_id`) REFERENCES `recdetails` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sendtoaccreditors_a_sender_user_id_foreign` FOREIGN KEY (`a_sender_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `send_orderpayments`
--
ALTER TABLE `send_orderpayments`
  ADD CONSTRAINT `send_orderpayments_or_recdetails_id_foreign` FOREIGN KEY (`or_recdetails_id`) REFERENCES `recdetails` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `send_orderpayments_or_sender_id_foreign` FOREIGN KEY (`or_sender_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `submissionstatuses`
--
ALTER TABLE `submissionstatuses`
  ADD CONSTRAINT `submissionstatuses_submissionstatuses_recdetails_id_foreign` FOREIGN KEY (`submissionstatuses_recdetails_id`) REFERENCES `recdetails` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `submissionstatuses_submissionstatuses_statuses_id_foreign` FOREIGN KEY (`submissionstatuses_statuses_id`) REFERENCES `statuses` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `submissionstatuses_submissionstatuses_user_id_foreign` FOREIGN KEY (`submissionstatuses_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
